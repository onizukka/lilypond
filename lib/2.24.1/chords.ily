\layout {
  \context { 
    \ChordNames
    minorChordModifier = "-"

    \override ChordName.font-size = #2
    \override ChordName.font-name = #"lilyjazz-chord"
  }
}
