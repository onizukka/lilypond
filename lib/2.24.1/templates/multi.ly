\version "2.24.1"
\language "english"

#(set-global-staff-size 20)

\include "main.ily"
\include "fonts/lilyjazz.ily"

\header {
  title = ""
  instrument = ""
  composer = ""
  arranger = ""
  poet = ""
  tagline = ""
}

marks = {
  \mark \markup "Score mark"
}


harmony = \chords {
  c
}

% Música
partEb = \relative c' {
  \key c \major
  \time 4/4
  
  c
}

partBb = \relative c' {
  \key c \major
  \time 4/4

  c'
}

sheet = <<
  \harmony
  \new Staff \with { \RemoveAllEmptyStaves } { \marks }

  \new StaffGroup <<
    \new Staff \with 
    { instrumentName = "Eb" }
    { \transpose ef c' \partEb } 

    \new Staff \with 
    { instrumentName = "Bb" }
    { \transpose bf c'' \partBb } 
  >>
>>

\book {
  \score { \sheet }
}
