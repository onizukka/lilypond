% ---------------------------------------------------------------------
eolMark = #(define-scheme-function (parser location text) (markup?)
#{
  \textEndMark { \hspace #1.25 \raise #1.25 $text }
#})

% ---------------------------------------------------------------------
segno = #(define-scheme-function (parser location) ()
#{
  \markup \musicglyph #"scripts.segno" 
#})

% ---------------------------------------------------------------------
coda = #(define-scheme-function (parser location) ()
#{
  \markup \musicglyph #"scripts.coda" 
#})

% ---------------------------------------------------------------------
rehearsal = #(define-scheme-function (parser location text) (markup?)
#{
  \markup \underline \line { "(" $text ")" }
#})

% ---------------------------------------------------------------------
alSegno = #(define-scheme-function (parser location) ()
#{
  \markup \underline {
    \line {
      "(D.S. "
      \fontsize #-4 \raise #1 { \musicglyph #"scripts.varsegno" }
      ")"
    } 
  }
#})

% ---------------------------------------------------------------------
dalSegnoAlCoda = #(define-scheme-function (parser location) ()
#{
  \markup \underline \line {
      "(D.S. "
      \fontsize #-4 \raise #1 { \musicglyph #"scripts.varsegno" }
      " al Coda "
      \fontsize #-4 \raise #1 { \musicglyph #"scripts.varcoda" }
      \hspace #-.5
      ")"
  }
#})

% ---------------------------------------------------------------------
segnoAlCodaRepeats = #(define-scheme-function (parser location) ()
#{
  \markup \underline \line {
      "(D.S. "
      \fontsize #-4 \raise #1 { \musicglyph #"scripts.varsegno" }
      " al Coda "
      \fontsize #-4 \raise #1 { \musicglyph #"scripts.varcoda" }
      " with repetitions)"
  }
#})

% ---------------------------------------------------------------------
segnoAlCodaNoRepeats = #(define-scheme-function (parser location) ()
#{
  \markup \underline \line {
      "(D.S. "
      \fontsize #-4 \raise #1 { \musicglyph #"scripts.varsegno" }
      " al Coda "
      \fontsize #-4 \raise #1 { \musicglyph #"scripts.varcoda" }
      " without repetitions)"
  }
#})

% ---------------------------------------------------------------------
daCapoAlCoda = #(define-scheme-function (parser location) ()
#{
  \markup \underline {
    \line {
      \fontsize #3 "(D.C. al Coda "
      \hspace #-1
      \fontsize #-2 \raise #1.5 { \musicglyph #"scripts.varcoda" }
      \hspace #-1
      \fontsize #3 ")"
    } 
  }
#})

% ---------------------------------------------------------------------
alCoda = #(define-scheme-function (parser location) ()
#{
  \markup \underline \line { 
    "(to" \fontsize #-4 \raise #1 { \musicglyph #"scripts.varcoda" } \hspace #-.5 ")"
  }
#})

% ---------------------------------------------------------------------
daCapo = #(define-scheme-function (parser location) ()
#{
  \markup { \column { \fontsize #3 "(D. C.)" } } 
#})


% ---------------------------------------------------------------------
mpsubito = #(define-scheme-function (parser location) ()
#{
  \markup { \dynamic "mp" \italic "súbito" }
#})

% ---------------------------------------------------------------------
ppsubito = #(define-scheme-function (parser location) ()
#{
  \markup { \dynamic "pp" \italic "súbito" }
#})


% Añade borde a un texto con un cómodo espacio interior (y no el que viene por defecto)
% ---------------------------------------------------------------------
bmark = #(define-scheme-function (parser location text) (markup?)
#{
  \mark \markup {
    \override #'((thickness . 2) (corner-radius . 0.75))
    \rounded-box $text
  }
#})

% boxed = #(define-scheme-function (parser location) ()
% #{
%   \markup \rounded-box 
% #})
% #(define-markup-command (boxed layout props text) (markup?)
%   interpret-markup layout props
%     (markup #:huge #:override '(box-padding . 0.9) #:override '(thickness . 4) #:parenthesize text)
%   )
