\paper {
  % Distancia entre la parte superior de la página y el título
  top-markup-spacing.basic-distance = 5

  % Distancia entre el título y el primer sistema
  markup-system-spacing.basic-distance = 20

  % Distancia entre el texto y el primer sistema 
  top-system-spacing.basic-distance = 15

  last-bottom-spacing.basic-distance = 10

  % Margen inferior
  bottom-margin = 5

  % Intentado del primer sistema
  indent = 15

  % Número de páginas
  % page-count = 1

  ragged-bottom = ##f

  ragged-last-bottom = ##f

  oddHeaderMarkup = \markup {
    \unless \on-first-page \fill-line {
      \fromproperty #'page:page-number-string
      \fromproperty #'header:title
      \fromproperty #'header:instrument
    }
  }

  evenHeaderMarkup = \markup {
    \unless \on-first-page \fill-line {
      \fromproperty #'header:instrument
      \fromproperty #'header:title
      \fromproperty #'page:page-number-string
    }
  }
}

