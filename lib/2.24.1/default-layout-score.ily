\layout {
  \context {
    \Score

    % Aspecto de los silencios multicompás
    \override MultiMeasureRest.expand-limit = #1

    % Margen extra de las articulaciones para evitar la colisión con las ligaduras
    \override Script.padding = #0.5
    \override Script.avoid-slur = #'inside

    % Glissando bonito
    \override Glissando.style = #'trill
    \override Glissando.bound-details.left.padding = #1.7

    % Dinámicas un poco mas gruesas
    \override Hairpin.thickness = #1.75
    \override Hairpin.bound-padding = #2

    % reduce el espacio entre pentagramas en los sistemas
    \override StaffGrouper.staff-staff-spacing.padding = #1
    \override StaffGrouper.staff-staff-spacing.basic-distance = #0

    % Establece un ancho mínimo para los bendAfter
    \override BendAfter.minimum-length = #2.0
    \override BendAfter.thickness = #2.5
    \override BendAfter.springs-and-rods = #ly:spanner::set-spacing-rods

    % Alinea las marcas de ensayo a la izquierda
    \override RehearsalMark.self-alignment-X = #LEFT
    \override RehearsalMark.font-size = #5

    % Ajusta el tamaño de los paréntesis de nota
    \override ParenthesesItem.font-size = #0

    % Esconde la cancelacion de accidentales en el cambio de armadura
    % \override KeyCancellation.break-visibility = #'#(#f #f #f)

  }
}
