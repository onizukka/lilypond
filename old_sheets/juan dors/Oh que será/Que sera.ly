﻿\version "2.12.3"

\include "Que sera - Piano.ly"

\header {
	title = "Qué será"
	composer = "Juan d'Ors"
}

\score
{
	\piano
}

\markuplines
{
     "A: dreamer, melodías octavadas"
     "B: dreamer"
     "C: rítmico, melodías octavadas"
     "D: rock"
     "E: rítmico, melodías octavadas"
     "F: rítmico, melodías octavadas"
     "G: final"
}