﻿introA = 
<<
   \relative c'' {
      r8 cis4 cis8 cis4 cis 
      r8 cis4 cis8 cis4 cis 
      r8 bis4 bis8 \times 2/3 { bis4 bis bis }
      bis1
      r8 bis4 bis8 bis4 bis 
      r8 bis4 bis8 bis4 bis 
      r8 cis4 cis8 \times 2/3 { cis4 cis cis }
      cis1
   } \\
	\relative c'{
      r8 cis4 cis8 cis4 cis 
      r8 cis4 cis8 cis4 cis 
      r8 gis4 gis8 \times 2/3 { gis4 gis gis }
      gis ais bis ais
      gis2. ~ gis8 ais
      bis2. ~ bis8 gis
      cis1 ~ 
      cis4 gis ais bis
   }
>>

chordA = 
{
   r1 r1 r1 r1
   r1 r1 r1 r1
}

chordAtoBrA = 
{
   r1 r1 r1 r1
   r1 r1 r1
}

chordB = 
{
   r1 r1 r1 r1
}

bridgA = \relative c''
{
   r1
   r4 r8 <gis gis'>8 <bis bis'>4 <cis cis'> ~
   <cis cis'>2 r2
   r4 r8 <gis gis'>8 <bis bis'>4 <cis cis'>4 ~
   <cis cis'>1 ~ <cis cis'>
}

bridgB = \relative c''
{
	\times 2/3 { r4 <cis cis'> <bis bis'> } \times 2/3 { <b b'> <ais ais'> <a a'>}
	<gis gis'>1
	\times 2/3 { r4 <cis cis'> <bis bis'> } \times 2/3 { <b b'> <ais ais'> <a a'>}
	<gis gis'>1
	r1 r1
}

introAh = \chordmode
{
   cis1:m cis:m gis gis
   gis gis cis:m gis
}

verseAh = \chordmode
{
   cis1:m c:aug e/b fis:9^1
   fis:m f:aug a/e gis:7
}

verseAtoBrAh = \chordmode
{
   cis1:m c:aug e/b fis:9^1
   a gis cis:m
}

verseBh = \chordmode
{
   cis1:m cis:m gis gis
   gis gis cis:m cis:m
}

verseBtoBrAh = \chordmode
{
   cis1:m cis:m gis gis
   gis gis cis:m
}

verseCh = \chordmode
{
   cis1:m cis1:m b b
   a a gis gis
}

bridgAh = \chordmode
{
   cis:m gis cis:m gis:m 
   cis:m cis:m
}

endh = \chordmode
{

}

piano = 
<<
	\new ChordNames {
		\introAh
		\introAh
      
      \verseAh
      \verseAh
      \verseAtoBrAh
      \bridgAh
      
      \verseBh
      \verseBh
      \verseBtoBrAh
      \bridgAh
      
      \verseBh
      \verseBh
      \verseBh
      
      \verseCh
      \verseBh
      
      \verseBh
      \verseBh
      \verseBh
      
      \verseBh
	}
   
	\new Staff
	{
		\key cis \minor
      
		\introA
      \introA
      
      \mark \markup {A \super 1}
      \chordA
      \mark \markup {A \super 2}
      \chordA
      \mark \markup {A \super 3}
      \chordAtoBrA
      \mark \markup {\small {contestando al saxo}}
      \bridgA
      
      \mark \markup {B \super 1}
      \chordA
      \mark \markup {B \super 2}
      \chordA
      \mark \markup {B \super 3}
      \chordAtoBrA
      \bridgB
      
      \mark \markup {C \super 1}
      \chordA
      \mark \markup {C \super 2}
      \chordA
      \mark \markup {C \super 3}
      \chordA
      
      \mark \markup {D}
      \chordA
      \mark \markup {E}
      \chordA
      
      \mark \markup {F \super 1}
      \chordA
      \mark \markup {F \super 2}
      \chordA
      \mark \markup {F \super 3}
      \chordA
      
      \mark \markup {G}
      \chordA
	}
>> 



rightBase = 
{
	R1^\markup { \small { "C#m" } }
	R1^\markup { \small { "C#m" } }
	R1^\markup { \small { "G#" } }
	R1^\markup { \small { "G#" } }
	
	R1^\markup { \small { "G#" } }
	R1^\markup { \small { "G#" } }
	R1^\markup { \small { "C#m" } }
	R1^\markup { \small { "C#m" } }
}

rightBaseB = 
{
	R1^\markup { \small { "C#m" } }
	R1^\markup { \small { "C#m" } }
	R1^\markup { \small { B } }
	R1^\markup { \small { B } }
	
	R1^\markup { \small { A } }
	R1^\markup { \small { A } }
	R1^\markup { \small { "G#m" } }
	R1^\markup { \small { "G#m" } }
}

rightVerse = 
{
	R1^\markup { \small { "C#m" } }
	R1^\markup { \small { "C5#" } }
	R1^\markup { \small { B/E } }
	R1^\markup { \small { "A#dim7M" } }
	
	R1^\markup { \small { "F#m" } }
	R1^\markup { \small { "F5#" } }
	R1^\markup { \small { E/A } }
	R1^\markup { \small { "G#7" } }
}

rightVerseOut = 
{
	R1^\markup { \small { "C#m" } }
	R1^\markup { \small { "C5#" } }
	R1^\markup { \small { B/E } }
	R1^\markup { \small { "A#dim7M" } }
	
	R1^\markup { \small { A } }
	R1^\markup { \small { "G#7" } }
	R1^\markup { \small { "C#m" } }
	R1^\markup { \small { "C#m" } }
}

bridgeA = 
{
	R1^\markup { \small { "G#" } }
	R1^\markup { \small { "C#m" } }
	R1^\markup { \small { "G#" } }
	R1^\markup { \small { "C#m" } }
}

bridgeB = 
{
	\times 2/3 { r4^\markup { "C#m" } <cis cis'> <bis bis'> }\times 2/3 { <b b'> <ais ais'> <a a'>}
	<gis gis'>1^\markup { "G#" }
	\times 2/3 { r4^\markup { "C#m" } <cis cis'> <bis bis'> }\times 2/3 { <b b'> <ais ais'> <a a'>}
	<gis gis'>1^\markup { "G#" }
	R1^\markup { "C#m" }
	R1^\markup { "C#m" }
}

manoDerecha = 
{
	\relative c''
	{
		\key cis \minor
		
		\mark \markup { \small { Introdución marcatto } }
		\rightBase
		\rightBase
		
		\mark \markup { \small { Olas. primer tiempo marcado } }
		\rightVerse
		\rightVerse
		\rightVerseOut
		
		\mark \markup { \small { Saxo. Coletilla agudas al final } }
		\bridgeA
		
		\mark \markup { \small { Olas. primer tiempo marcado (octavas) } }
		\rightBase
		\rightBase
		\rightBase
		
		\mark \markup { \small { Coletilla agudas al final } }
		\bridgeB
		
		\mark \markup { \small { Tango, bajos revueltos } }
		\rightBase
		\rightBase
		
		\mark \markup { \small { Melodías octavadas } }
		\rightBaseB
		\rightBase
		
		\mark \markup { \small { Tango, bajos revueltos } }
		\rightBase
		\rightBase
		
		\mark \markup { \small { Va bajando. } }
		\rightBase
		\rightBase
		
		\rightBase
	}
}

leftIntro  = 
{
	r8 <cis cis'>4 <cis cis'>8 <cis cis'>4 <cis cis'>4 
	r8 <cis cis'>4 <cis cis'>8 <cis cis'>4 <cis cis'>4 
	r8 <gis gis'>4 <gis gis'>8 \times 2/3 { <gis gis'>4 <gis gis'> <gis gis'> }
	<gis gis'> <ais ais'> <bis bis'> <ais ais'> 
	<gis gis'>2. ~ <gis gis'>8 <ais ais'> 
	<bis bis'>2. ~ <bis bis'>8 <gis gis'>
	<cis cis'>1 ~ 
	<cis cis'>4 <gis gis'> <ais ais'> <bis bis'>
}

manoIzquierda = 
{
	\relative c
	{
		\clef bass
		\key cis \minor
		
		\leftIntro
		\leftIntro
		R1
	}
}

pianodos = \new PianoStaff
<<
	\new Staff { \manoDerecha }
	\new Staff { \manoIzquierda }
>>

