\version "2.16.1"
% \include "imports\main.ly"

slides =
#(make-music 'CrescendoEvent
             'span-direction START
             'span-type 'text
             'span-text "slides")

% Instrument Music
% -------------------------------------------------
altosaxmidi = {

  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0	
  
  \key b \minor
  
  \partial 4
  r4
  
  R1*12
  
  R1*4
  
  R1*3
  
  R1*4
  R1*3
  
  R1*12
  R1*12
  
  r8 fis \p \< g a b cis d e
  fis2 \mf r2
  r8. \appoggiatura g8 fis16 \< ~ fis8 g a8. b16 ~ b8 cis
  d8. \f \> cis16 ~ cis8 b a2 \mf
  
  \acciaccatura {b8 a} g4. fis8 e8. d16 ~ d8 cis
  d8. e16 ~ e8 d b8. a16 ~ a8 g
  fis8. fis16 ~ fis8 fis \> g4 a \p
  r2 r4 r8 d
  
  cis \< d e fis g a b cis
  d2 \f \> \acciaccatura {a8 g} fis8. a16 ~ a8 g
  fis1 \mf 
  R1
  
  R1*12
  
  r1
  r2 r4 r8 fis, \p \<
  g a b cis d8. e16 ~ e8 fis
  g4. fis8 ~ fis2
  
  r8. d'16 \f ~ d8 cis d8. d16 ~ d8 cis
  b2 \acciaccatura {b8 a} g \> fis e d
  cis1 \p
  \acciaccatura {b8 a} g \< a b cis d e fis g
  
  ais b cis d cis2 \f
  r8. cis16 ~ cis8 d8 \appoggiatura d8 cis4. b8
  b4 \> ais8 ais ~ ais4 \acciaccatura {g8 a} g fis
  
  g4 \mf fis2.
  R1*2
  
  R1*8
  
  R1*8
  R1*2
  
  \unfoldRepeats \repeat volta 8 {
    R1*2
  }
  
  R1*12
  
  R1*4
  
  R1*2
  
  R1*12
  
  R1
  R1*2
  
  fis8. fis16 ~ fis8 fis fis8. fis16 ~ fis8 fis
  fis8 r r4 r2
   
   \bar "|."
   
}