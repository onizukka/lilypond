scoreMarkup = \new Staff \with {
   \RemoveEmptyStaves
}
{
  % Oculta el primer pentagrama del markup
  \override Staff.VerticalAxisGroup #'remove-first = ##t
  
  \mark \markup \boxed "Introducción"
  
  s1*14
  
  \mark \markup \boxed "Estrofa"
  
  s1*20
  
  \mark \markup \boxed "Estribillo"
  
  s1*12
  
  \mark \markup \boxed "Estrofa"
  
  s1*12
  
  \mark \markup \boxed "Estribillo"
  
  s1*22
  
  \mark \markup \boxed "Estribillo"
}