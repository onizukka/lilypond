\version "2.16.1"
% \include "imports\main.ly"

slides =
#(make-music 'CrescendoEvent
             'span-direction START
             'span-type 'text
             'span-text "slides")

% Instrument Music
% -------------------------------------------------
eguitarmidi = {

  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0	
  
  \key b \minor
  
  \partial 4
  r4
  
  R1*8
  
  r8. <b d>16 \p ~ <b d>8 <b d>8 <b d>8. <cis e>16 ~ <cis e>8 <b d>
  <ais cis>1
  r8. <b d>16 ~ <b d>8 <b d>8 <b d>8. <cis e>16 ~ <cis e>8 <b d>
  <ais cis>1
  
  
  b4 a g fis
  a g fis e
  g fis e d
  fis cis2.
  
  b''4 a g fis
  \break
  a g fis e
  g fis e d
  cis1
  
  r8. b16 \< ~ b8 b b8.-> \f \> cis16 ~ cis8 d8
  cis1 \p
  \break
  r8. b16 \< ~ b8 b b8.-> \f \> cis16 ~ cis8 d
  cis1 \p
  
  cis8. \< cis16 ~ cis8 cis cis8. cis16 ~ cis8 cis
  cis8-^ \f r8 r4 r2
  
  
  fis,1 \p
  \appoggiatura fis8 g8. fis16 ~ fis8 e8 d8. e16 ~ e8 fis
  cis1
  \appoggiatura fis8 g8. fis16 ~ fis8 e8 d8. e16 ~ e8 fis
  
  cis1 
  \appoggiatura fis8 g8. fis16 ~ fis8 e8 d8. e16 ~ e8 fis
  cis1
  r8. e16 ~ e8 e e8. fis16 ~ fis8 g
  
  fis1
  r8. e16 ~ e8 e e8. fis16 ~ fis8 g
  fis1
  R1
  
  fis1
  \appoggiatura fis8 g8. fis16 ~ fis8 e8 d8. e16 ~ e8 fis
  cis1
  \appoggiatura fis8 g8. fis16 ~ fis8 e8 d8. e16 ~ e8 fis

  cis1
  d'4 cis b a8 g
  fis1
  g'4 d b g
  
  fis1
  r8. b16 ~ b8 b8 b8. cis16 ~ cis8 d
  cis1 ~ 
  cis1
  
  \break
  b1 ~ \f
  b8. b16 ~ b8 cis8 d8. e16 ~ e8 fis
  e8. d16 ~ d8 cis8 e2 ~ 
  e4. e16 e e8. fis16 ~ fis8 e
  
  \break
  d8. cis16 ~ cis8 b8 d2 ~ 
  d4. d16 d d8. e16 ~ e8 d
  cis1
  r8. b16 \> ~ b8 b8 b8. cis16 ~ cis8 d
  
  \break
  cis1 \mf 
  r8. b16 \> ~ b8 b8 b8. cis16 ~ cis8 d
  cis1 \p
  R1
  
  \break
  fis1 \p
  \appoggiatura fis8 g8. fis16 ~ fis8 e8 d8. e16 ~ e8 fis
  cis1
  \appoggiatura fis8 g8. fis16 ~ fis8 e8 d8. e16 ~ e8 fis
  
  \break
  cis1
  d4 cis b a8 g
  fis1
  g'4 d b g
  
  \break
  fis1
  r8. b16 ~ b8 b8 b8. cis16 ~ cis8 d
  cis1 ~ 
  cis1
  
  \break
  b1 \f ~ 
  b8 fis b cis d8. e16 ~ e8 fis
  e8. d16 ~ d8 cis8 e2 ~
  e4. e16 e e8. fis16 ~ fis8 e
  
  \break
  d8. cis16 ~ cis8 b8 d2 ~ 
  d4. d16 d d8. e16 ~ e8 d
  cis1
  r8. d16 \> ~ d8 d8 d8. e16 ~ e8 d
   
   \break
   fis1 \mf
   r8. g16 \> ~ g8 fis8 e8. d16 ~ d8 e
   cis1 \p
   
   <b d>2 ~ <b d>4. d8
   <a cis>2 ~ <a cis>4. cis8
   <g b>1
   
   \break
   <fis ais>1
   <g b>8. <g b>16 ~ <g b>8 <g b>8 <g b>8. <g b>16 ~ <g b>8 <g b>
   <ais cis>1
   <g b>8. <g b>16 ~ <g b>8 <g b>8 <g b>8. <g b>16 ~ <g b>8 <g b>
   
   \break
   <fis ais>1
   <g b>8. <g b>16 ~ <g b>8 <g b>8 <g b>8. <g b>16 ~ <g b>8 <g b>
   <fis ais>1 ~ 
   <fis ais>1
   
   \break
   b1 \f ~ 
   b8 fis b cis d8. e16 ~ e8 fis
   e8. d16 ~ d8 cis8 e2 ~
   e4. e16 e e8. fis16 ~ fis8 e
   
   \break
   d8. cis16 ~ cis8 b8 d2 ~ 
   d4. d16 d d8. e16 ~ e8 d
   cis1
   R1
   
   \break
   R1*3 
   d'8 \bendBefore \p g ~ g2. 
   %{
   R1
   d'8 \bendBefore \p g ~ g2. %}
   
   
   \unfoldRepeats \repeat volta 7 {
   % \unfoldRepeats \repeat volta 8 {
     cis,8 \bendBefore fis ~ fis2.
     d8 \bendBefore  g ~ g2.
   }
   
   fis1 \bendBefore \p
   R1
   R1
   R1
   
   R1
   R1
   R1
   R1
   
   R1
   R1
   R1
   R1
   
   R1
   R1
   R1
   R1
   
   R1
   R1
   
   \break
   fis,1 \p
   \appoggiatura fis8 g8. fis16 ~ fis8 e8 d8. e16 ~ e8 fis
   cis1
   \appoggiatura fis8 g8. fis16 ~ fis8 e8 d8. e16 ~ e8 fis
   
   \break
   cis1
   \appoggiatura fis8 g8. fis16 ~ fis8 e8 d8. e16 ~ e8 fis
   cis1
   g'4 \f d b g
   
   fis1
   r8. g'16 \mf ~ g8 g8 g8. a16 ~ a8 g
   fis1
   r8. g16 ~ g8 g8 g8. a16 ~ a8 g
   
   fis1 
   r8. g16 \> ~ g8 g8 g8. a16 ~ a8 g
   fis1 \p
   <cis fis>8. \< <cis fis>16 ~ <cis fis>8 <cis fis>8 <cis fis>8. <cis fis>16 ~ <cis fis>8 <cis fis>
   
   <cis fis>8-^ \f r8 r4 r2
   
   \bar "|."
   
}