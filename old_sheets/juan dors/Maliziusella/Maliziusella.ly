\version "2.12.3"

\include "Maliziusella - Piano.ly"

\header {
	title = "Maliziusella"
	subtitle = "Tiempos"
	composer = "Juan d'Ors"
	instrument = "Piano"
}

\score
{
	\piano
}