﻿intro = \relative c'
{
   e8. g16 e8. g16 e8. g16 e8. g16 
   e8. g16 e8. g16 e8. g16 e8. g16 
   fis8. a16 fis8. a16 fis8. a16 g8. fis16
   e8. g16 e8. g16 e8. g16 e8. g16 
   
   r1 r1 r1 r1
   r1 r1 r1 r1
   r1 r1 r1 r1
}

verseSA =
{
   r1 r1 r1 r1 
   r1 r1 r1 r1
}

verseSB =
{
   r1 r1 r1 r1 
   r1 r1 r1 r1 
   r1 r1 r1 r1
}

verseE = 
{
   r1 r1 r1
}

introH = \chordmode
{
   e1:m e1:m 
   e2.:m a4:m7 e1:m
   a2:m e2:m a2:m e2:m 
   a2:m fis2:7 b1:7
   
   d1:7 g:7 d1:7 g:7
   b2:7 e2:m b2:7 e2:m
   a2:m fis2:7 b1:7
}

verseAH = \chordmode
{
   e1:m e1:m e1:m b1:7
   b1:7 b1:7 b1:7 e1:m
}

verseBH = \chordmode
{
   e1:m e1:m e1:7 a1:m
   a1:m e1:m b1:7 e1:7
   a1:m e1:m b1:7 e1:m
}

verseEH = \chordmode
{
   b1:7 b1:7 e1:m
}

piano = \new StaffGroup
<<
	\new ChordNames {
		\introH
      
      \verseAH
      \verseBH
      
      \verseAH
      \verseBH
      
      \verseAH
      \verseBH
      
      \verseEH
	}
	\new Staff
	{
		\key e \minor
      
      \intro
      
      \mark \markup {A}
      \verseSA
      \mark \markup {B}
      \verseSB
      
      \mark \markup {C}
      \verseSA
      \mark \markup {D}
      \verseSB
      
      \mark \markup {E}
      \verseSA
      \mark \markup {F}
      \verseSB
      
      \mark \markup {G}
      \verseE
	}
>> 