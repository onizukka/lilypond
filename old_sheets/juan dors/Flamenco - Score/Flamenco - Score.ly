\version "2.18.2"

\include "lily-jazz/ignatzek-jazz-chords.ily"
\include "lily-jazz/lily-jazz.ily"





% PLUGINS 1.2
% -----------
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~





% multi-mark-engraver
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
% Aporta la posibilidad de pintar un rehearsal mark al final de una línea y 
% al comienzo de la siguiente.
% http://lists.gnu.org/archive/html/lilypond-user/2011-08/msg00157.html
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
#(define (multi-mark-engraver ctx)
   (let ((texts '())
         (final-texts '())
         (events '()))

     `((start-translation-timestep
        . ,(lambda (trans)
             (set! final-texts '())))

       (listeners
        (mark-event
         . ,(lambda (trans ev)
              (set! events (cons ev events)))))

       (acknowledgers
        (break-alignment-interface
         . ,(lambda (trans grob source)
              (for-each (lambda (mark)
                          (set! (ly:grob-parent mark X) grob))
                        texts))))

       (process-music
        . ,(lambda (trans)
             (for-each
              (lambda (ev)
                (let* ((mark-grob
                        (ly:engraver-make-grob trans 'RehearsalMark ev))
                       (label (ly:event-property ev 'label))
                       (formatter (ly:context-property ctx 'markFormatter)))

                  (if (and (procedure? formatter)
                           (not (markup? label)))
                      (begin
                       (if (not (number? label))
                           (set! label
                                 (ly:context-property ctx 'rehearsalMark)))

                       (if (and (integer? label)
                                (exact? label))
                           (set! (ly:context-property ctx 'rehearsalMark)
                                 (1+ label)))

                       (if (number? label)
                           (set! label (apply formatter (list label ctx)))
                           (ly:warning "rehearsalMark must have
integer value"))))

                  (if (markup? label)
                      (begin
                       (set! (ly:grob-property mark-grob 'text) label)
                       (let ((dir (ly:event-property ev 'direction)))
                         (and (ly:dir? dir)
                              (set! (ly:grob-property mark-grob 'direction)
                                    dir))))
                      (ly:warning "mark label must be a markup object"))

                  (set! texts (cons mark-grob texts))))
              (reverse events))))

       (stop-translation-timestep
        . ,(lambda (trans)
             (if (pair? texts)
                 (let ((staves (ly:context-property ctx 'stavesFound))
                       (priority-index 0))
                   (for-each (lambda (grob)
                               (let ((my-priority (ly:grob-property
grob 'outside-staff-priority 1500)))
                                 (for-each (lambda (stave)

(ly:pointer-group-interface::add-grob grob 'side-support-elements
                                               stave))
                                           staves)
                                 (set! (ly:grob-property grob
'outside-staff-priority) (+ my-priority priority-index))
                                 (set! priority-index (1+ priority-index))
                                 (set! final-texts (cons grob final-texts))))
                             (reverse texts))
                     (set! texts '())
                     (set! events '())))))

        (finalize
         . ,(lambda (trans)
              (and (pair? final-texts)
                   (for-each (lambda (grob)
                               (set! (ly:grob-property grob 'break-visibility)
                                     end-of-line-visible))
                             final-texts)))))))


% Funciones originales
%{
myMark =
#(define-music-function (parser location text) (markup?)
  (make-music 'MarkEvent
              'label text))
			  
toCoda = {
	\tweak #'self-alignment-X #RIGHT
	\tweak #'break-visibility #begin-of-line-invisible
	\myMark \markup { \hspace #1.25 \raise #1.25 \musicglyph #"scripts.coda" }
}
%}

% Función maqueada.
% Se apoya en el código obtenido por consola al ejecutar { \displayMusic { \toCoda }}
eolMarkup =
#(define-music-function (parser location text) (markup?)
	(make-music 
		'MarkEvent 
		'tweaks (list 
			(cons (quote self-alignment-X) 1)
			(cons (quote break-visibility) #(#t #t #f))
		) 
		'label (markup
			#:line (
				#:hspace 1.25
				#:raise 1.25
				text
			)
		)
	)
)










% rest-score y merge-rests-on-positioning
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
% Permite realizar la fusión de los silencios entre dos voces en un mismo pentagrama.
% https://code.google.com/p/lilypond/issues/detail?id=1228 (fichero merge-rests.ily)
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]

#(define (rest-score r)
  (let ((score 0)
	(yoff (ly:grob-property-data r 'Y-offset))
	(sp (ly:grob-property-data r 'staff-position)))
    (if (number? yoff)
	(set! score (+ score 2))
	(if (eq? yoff 'calculation-in-progress)
	    (set! score (- score 3))))
    (and (number? sp)
	 (<= 0 2 sp)
	 (set! score (+ score 2))
	 (set! score (- score (abs (- 1 sp)))))
    score))

#(define (merge-rests-on-positioning grob)
  (let* ((can-merge #f)
	 (elts (ly:grob-object grob 'elements))
	 (num-elts (and (ly:grob-array? elts)
			(ly:grob-array-length elts)))
	 (two-voice? (= num-elts 2)))
    (if two-voice?
	(let* ((v1-grob (ly:grob-array-ref elts 0))
	       (v2-grob (ly:grob-array-ref elts 1))
	       (v1-rest (ly:grob-object v1-grob 'rest))
	       (v2-rest (ly:grob-object v2-grob 'rest)))
	  (and
	   (ly:grob? v1-rest)
	   (ly:grob? v2-rest)
	   (let* ((v1-duration-log (ly:grob-property v1-rest 'duration-log))
		  (v2-duration-log (ly:grob-property v2-rest 'duration-log))
		  (v1-dot (ly:grob-object v1-rest 'dot))
		  (v2-dot (ly:grob-object v2-rest 'dot))
		  (v1-dot-count (and (ly:grob? v1-dot)
				     (ly:grob-property v1-dot 'dot-count -1)))
		  (v2-dot-count (and (ly:grob? v2-dot)
				     (ly:grob-property v2-dot 'dot-count -1))))
	     (set! can-merge
		   (and
		    (number? v1-duration-log)
		    (number? v2-duration-log)
		    (= v1-duration-log v2-duration-log)
		    (eq? v1-dot-count v2-dot-count)))
	     (if can-merge
		 ;; keep the rest that looks best:
		 (let* ((keep-v1? (>= (rest-score v1-rest)
				      (rest-score v2-rest)))
			(rest-to-keep (if keep-v1? v1-rest v2-rest))
			(dot-to-kill (if keep-v1? v2-dot v1-dot)))
		   ;; uncomment if you're curious of which rest was chosen:
		   ;;(ly:grob-set-property! v1-rest 'color green)
		   ;;(ly:grob-set-property! v2-rest 'color blue)
		   (ly:grob-suicide! (if keep-v1? v2-rest v1-rest))
		   (if (ly:grob? dot-to-kill)
		       (ly:grob-suicide! dot-to-kill))
		   (ly:grob-set-property! rest-to-keep 'direction 0)
		   (ly:rest::y-offset-callback rest-to-keep)))))))
    (if can-merge
	#t
	(ly:rest-collision::calc-positioning-done grob))))










% startParenthesis y endParenthesis
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
% http://comments.gmane.org/gmane.comp.gnu.lilypond.general/64087
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]

% ELIMINADO










% bendBefore
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
% Adaptación
% http://lilypondblog.org/2013/08/adding-ornamentations-to-note-heads-part-1/
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]

%%%
%%% Utilities for defining new grobs, grob properties and music event types
%%% (there should be built-in commands to do that in LilyPond)
%%%
#(define (define-grob-type grob-name grob-entry)
   (let* ((meta-entry   (assoc-get 'meta grob-entry))
          (class        (assoc-get 'class meta-entry))
          (ifaces-entry (assoc-get 'interfaces meta-entry)))
     (set-object-property! grob-name 'translation-type? list?)
     (set-object-property! grob-name 'is-grob? #t)
     (set! ifaces-entry (append (case class
                                  ((Item) '(item-interface))
                                  ((Spanner) '(spanner-interface))
                                  ((Paper_column) '((item-interface
                                                     paper-column-interface)))
                                  ((System) '((system-interface
                                               spanner-interface)))
                                  (else '(unknown-interface)))
                                ifaces-entry))
     (set! ifaces-entry (uniq-list (sort ifaces-entry symbol<?)))
     (set! ifaces-entry (cons 'grob-interface ifaces-entry))
     (set! meta-entry (assoc-set! meta-entry 'name grob-name))
     (set! meta-entry (assoc-set! meta-entry 'interfaces
                                  ifaces-entry))
     (set! grob-entry (assoc-set! grob-entry 'meta meta-entry))
     (set! all-grob-descriptions
           (cons (cons grob-name grob-entry)
                 all-grob-descriptions))))

#(define-public (define-grob-property symbol type? description)
  (set-object-property! symbol 'backend-type? type?)
  (set-object-property! symbol 'backend-doc description)
  symbol)

#(defmacro*-public define-music-type (type-name parents #:rest properties)
   (let ((gproperties (gensym "properties")))
     `(let ((,gproperties (list-copy ',properties)))
        ,@(map (lambda (parent)
                `(define-event-class ',(ly:camel-case->lisp-identifier type-name)
                   ',parent))
               parents)
        (set-object-property! ',type-name
                              'music-description
                              (cdr (assq 'description ,gproperties)))
        (set! ,gproperties (assoc-set! ,gproperties 'name ',type-name))
        (set! ,gproperties (assq-remove! ,gproperties 'description))
        (hashq-set! music-name-to-property-table ',type-name ,gproperties)
        (set! music-descriptions
              (cons (cons ',type-name ,gproperties)
                    music-descriptions)))))

%%%
%%% Grob definition
%%%
#(define (head-ornamentation::print me)
   (let* ((notes (ly:grob-object me 'elements))
          (y-ref (ly:grob-common-refpoint-of-array me notes Y))
          (x-ref (ly:grob-common-refpoint-of-array me notes X))
          (x-ext (ly:relative-group-extent notes x-ref X))
          (y-ext (ly:relative-group-extent notes y-ref Y))
          (y-coord (interval-center y-ext))
          (text (ly:text-interface::print me))
          (width (/ (interval-length (ly:stencil-extent text X)) 2.0))
          (x-coord (- (car x-ext) width)))
     (ly:stencil-translate
      text
      (cons
       (- (- x-coord (ly:grob-relative-coordinate me x-ref X)) .7)
       (- (- y-coord (ly:grob-relative-coordinate me y-ref Y)) 1.5)))))

#(define-grob-type 'HeadOrnamentation
  `((text . ,#{ \markup \fontsize #-4 \musicglyph #"brackettips.up" #})
    (stencil . ,head-ornamentation::print)
    (meta . ((class . Item)
             (interfaces . (font-interface))))))

\layout {
  \context {
    \Global
    \grobdescriptions #all-grob-descriptions
  }
}

%%%
%%% Engraver
%%%
%%% The head-ornamentation engraver, with its note-head acknowledger
%%% (which add HeadOrnamentation grobs to note heads)
#(define head-ornamentation-engraver
   (make-engraver
    (acknowledgers
     ((note-head-interface engraver note-grob source)
      ;; When the note-head event attached to the note-head grob has
      ;; ornamentation events among its articulations, then create a
      ;; HeadOrnamentation grob
      (let* ((note-event (ly:grob-property note-grob 'cause)))
        (for-each
         (lambda (articulation)
           (if (memq 'head-ornamentation-event
                     (ly:event-property articulation 'class))
               (let ((ornament-grob (ly:engraver-make-grob engraver
                                                           'HeadOrnamentation
                                                           note-grob)))
                 (ly:pointer-group-interface::add-grob ornament-grob
                                                       'elements
                                                       note-grob)
                 (set! (ly:grob-parent ornament-grob Y) note-grob)
                 (set! (ly:grob-property ornament-grob 'font-size)
                       (+ (ly:grob-property ornament-grob 'font-size 0.0)
                          (ly:grob-property note-grob 'font-size 0.0))))))
         (ly:event-property note-event 'articulations)))))))

\layout {
  \context {
    \Voice
    \consists #head-ornamentation-engraver
  }
}

%%%
%%% Event type definition
%%%
#(define-music-type HeadOrnamentationEvent (music-event)
   (description . "Print an ornamentation at a note head side")
   (types . (general-music post-event event head-ornamentation-event)))

bendBefore = #(make-music 'HeadOrnamentationEvent)











% Índice con borde circular y un cómodo espacio interior (y no el que viene por defecto)
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
#(define-markup-command (boxindex layout props text) (markup?)
	(interpret-markup layout props
		(markup #:huge #:override '(box-padding . 0.5) #:box text)
	)
)

#(define-markup-command (boxed layout props text) (markup?)
	(interpret-markup layout props
		(markup #:huge #:override '(box-padding . 0.9) #:box text)
	)
)





% Ritardando
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]

rit = #(make-music 'CrescendoEvent
         'span-direction START
         'span-type 'text
         'span-text "rit.")





% Negra oblicua
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]

% {
rs = {
  \once \override Rest #'stencil = #ly:percent-repeat-item-interface::beat-slash
  \once \override Rest #'thickness = #0.48
  \once \override Rest #'slope = #1.7
  r4
}

rrs = {
  \once \override Rest #'stencil = #ly:percent-repeat-item-interface::beat-slash
  \once \override Rest #'thickness = #0.48
  \once \override Rest #'slope = #1.7
  r2
}

rsMeasure = {
  \rrs \rrs
}





% Añadido de funciones al contexto
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]

\layout {
  \context {
    \Score		
    
    % multi-mark-engraver
    \remove "Mark_engraver"
    \consists #multi-mark-engraver
    \consists "Tweak_engraver"
    
    % merge-rests-on-positioning
    \override RestCollision #'positioning-done = #merge-rests-on-positioning
  }
}












% LAYOUT
% ------
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

\header {
  title = "Flamenco - Una producción de Juan d'Ors"
  instrument = ""
  composer = \markup \center-column {
      \line { "Versión libre, arreglos y producción" }
      \line { "JUAN D'ORS" }
    }
  poet = \markup \center-column {
      \line { "Música y letra" }
      \line { "Copyright by F. Arbex Miro, M. Callejo Martínez Losa, " }
      \line { "M. González Carot, A. Morales Barretto," }
      \line { "J. Pardo Suárez / Arreglista: Juan d'Ors." }
      \line { "Universal Music Publishing SL" }
    }
  tagline = \markup \center-column {
      \line { " " }
      \line { " " }
      \line { "Transcripción y edición: GABRIEL PALACIOS" }
      \line { "gabriel.ps.ms@gmail.com" }
    }
}

\paper {
  % El definir otro tipo de fuente no permite cambiar el tamaño global del staff. Es un bug conocigo
  #(define fonts (make-pango-font-tree "LilyJAZZText" "Nimbus Sans" "Luxi Mono" (/ 20 20)))
    
  #(set-paper-size "a4" 'landscape)
    
  % distancia entre sistemas
  system-system-spacing #'basic-distance = #20
  
  % distancia entre el título y el primer sistema
  markup-system-spacing #'basic-distance = #35
  
  top-markup-spacing #'basic-distance = #5
  
  top-system-spacing #'basic-distance = #15
  
  bottom-margin = #20
  
  indent = #20
  
  oddHeaderMarkup = \markup {
    \on-the-fly #not-first-page \fill-line {
      \fromproperty #'page:page-number-string
      \fromproperty #'header:title
      \fromproperty #'header:instrument
    }
  }
  
  evenHeaderMarkup = \markup {
    \on-the-fly #not-first-page \fill-line {
      \fromproperty #'header:instrument
      \fromproperty #'header:title
      \fromproperty #'page:page-number-string
    }
  }
}










% MUSIC PARTS
% -----------
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~





% scoreMarkup
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]

scoreMarkup = \new Staff \with {
   \RemoveEmptyStaves
}
{
  % Oculta el primer pentagrama del markup
  \override Staff.VerticalAxisGroup #'remove-first = ##t
  
  % s1*2
  % \mark \markup \boxindex A
}





% scoreChords
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]

scoreChords = \chords {
  % c1:m f2:m g2:m7
}





% music
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]

globalMusic = {
  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0
  
  \key b \minor 
}

lead = \relative c'' {
  \globalMusic
  
  \partial 2.
  r4 r2
  
  R1*26
  
  \break
  \bar "||"
  cis8. cis16 ~ cis8 cis cis8. cis16 ~ cis8 cis8
  d8. d16 ~ d8 d8 ~ d4 r
  cis8. cis16 ~ cis8 cis cis8. cis16 ~ cis8 cis8
  d8. d16 ~ d8 d8 ~ d4 r
  
  cis8. cis16 ~ cis8 cis cis8. cis16 ~ cis8 cis8
  d8. d16 ~ d8 d8 ~ d d b g
  fis1
  g8. g16 ~ g8 g g8. b16 ~ b8 b8
  
  fis1
  R1*3
  
  \break
  \bar "||"
  cis'8. ais16 ~ ais8 b cis8. ais16 ~ ais8 b8
  cis8. d16 ~ d8 b8 ~ b2
  cis8. ais16 ~ ais8 b cis8. ais16 ~ ais8 b8
  cis8. d16 ~ d8 b8 ~ b2
  
  cis8. ais16 ~ ais8 b cis8. ais16 ~ ais8 b8
  cis8. d16 ~ d8 b8 ~ b b a g
  fis1
  e'8. e16 ~ e8 e e8. d16 ~ d8 e8
 
  cis8. fis,16 ~ fis4 r2
  e'8. e16 ~ e8 e e8. d16 ~ d8 e8
  cis1
  R1  
  
  \break
  \bar "||"
  b1 ~ 
  b4. cis8 d4 b
  a1
  r4 cis2 b8 a
  
  b4 g2.
  r4 b4 a g
  fis1
  R1
  
  R1*4
  
  \break
  \bar "||"
  cis'8. ais16 ~ ais8 b cis8. cis16 ~ cis8 cis8
  d8. d16 ~ d8 b8 ~ b4 r
  cis8. cis16 ~ cis8 cis cis8. cis16 ~ cis8 cis8
  d8. d16 ~ d8 b8 ~ b4 r
  
  cis8. ais16 ~ ais8 b cis8. ais16 ~ ais8 b8
  cis8. d16 ~ d8 b8 ~ b b a g
  fis1 
  e'8. d16 ~ d8 d d8. e16 ~ e8 d8
  
  cis8. fis,16 ~ fis4 r2
  e'8. d16 ~ d8 cis b8. cis16 ~ cis8 g8
  fis1
  R1
  
  \break
  \bar "||"
  \once \override Rest #'stencil = #ly:percent-repeat-item-interface::beat-slash
  \once \override Rest #'thickness = #0.48
  \once \override Rest #'slope = #1.7
  r2 ^"Coros Libres"
  \once \override Rest #'stencil = #ly:percent-repeat-item-interface::beat-slash
  \once \override Rest #'thickness = #0.48
  \once \override Rest #'slope = #1.7
  r2
  \unfoldRepeats \repeat volta 9 { \rsMeasure }
  R1*4
  
  R1*8
  
  \break
  \bar "||"
  R1*6
  R1*4
  
  R1
  \once \override Rest #'stencil = #ly:percent-repeat-item-interface::beat-slash
  \once \override Rest #'thickness = #0.48
  \once \override Rest #'slope = #1.7
  r2 ^"Coros Libres"
  \once \override Rest #'stencil = #ly:percent-repeat-item-interface::beat-slash
  \once \override Rest #'thickness = #0.48
  \once \override Rest #'slope = #1.7
  r2
  \unfoldRepeats \repeat volta 15 { \rsMeasure }
  
  R1*9
  
  R1*2
  
  \break
  \bar "||"
  cis'8. cis16 ~ cis8 cis cis8. cis16 ~ cis8 cis8
  d8. d16 ~ d8 d8 ~ d4 r
  cis8. cis16 ~ cis8 cis cis8. cis16 ~ cis8 cis8
  d8. d16 ~ d8 d8 ~ d4 r
  
  cis8. cis16 ~ cis8 cis cis8. cis16 ~ cis8 cis8
  d8. d16 ~ d8 d8 ~ d4 b8 cis16 b
  fis1
  g'8. g16 ~ g8 g g8. fis16 ~ fis8 e8
  
  cis1
  g8 d' ~ d2 b8 g
  fis8 cis' ~ cis2 ais8 fis
  g8 d' ~ d2 b8 g
  
  fis8 cis' ~ cis2 ais8 fis
  g8 d' ~ d2 b8 g
  fis1
  R1
  
  R1
  \bar "|."
}
\addlyrics {
  Si me pre -- gun -- tas a don -- de voy,
  Y si tu quie -- res sa -- ber quién soy,
  Pien -- sa que_es fá -- cil de_a -- di -- vi -- nar,
  Que yo soy yo.
  Ay, ay, ay, ay, ay, ay, ay!
  
  Si te preo -- cu -- pa mi por -- ve -- nir,
  Pue -- des de -- jar de pen -- sar en mi,
  No lo -- gra -- rás ha -- cer -- me cam -- biar,
  Soy co -- mo soy.
  Ay, ay, ay, ay, ay, ay, ay, ay!
  Ay, ay, ay, ay, ay, ay, ay!
  
  A -- y,_a -- y,_a -- y,_a -- y,_ay!
  A -- y,_a __ _ y,_a -- y,_ay!
  A -- y,_a -- y,_a -- y,_ay!
  
  Pién -- sa -- lo bien y de -- cí -- de -- te,
  No ten -- go tiem -- po pa -- ra per -- der,
  Pron -- to mi no -- via tu vas a ser,
  Lo di -- go yo.
  
  Ay, ay, ay, ay, ay, ay, ay, ay!
  Ay, ay, ay, ay, ay, ay, ay!
  
  Nun -- ca ja -- más te_a -- rre -- pen -- ti -- rás,
  Por -- que si bus -- cas fe -- li -- ci -- dad,
  So -- lo_a mi la -- do la_en -- con -- tra -- rás,
  Lo di -- go yo.
  Ay, ay, ay, ay, ay, ay, ay!
  
  Le -- i,
  La -- vo -- le -- i,
  La -- vo -- le -- i,
  La -- vo -- le -- i,
  La -- vo -- le -- i,
  La -- vo -- lá!
}

violin = \relative c'' {
  \globalMusic
  
  \partial 2.
  r4 r2
  
  R1*12
  
  R1*14
  
  R1*12
  
  r4. \mf cis8( fis8. eis16 ~ eis8 fis
  g2 d8. e16 ~ e8 fis16 g
  fis8 \mordent cis4) cis8( fis8. eis16 ~ eis8 fis
  g2 d8. e16 ~ e8 fis16 g
  
  fis2.) \mordent e4(
  d \> cis b cis
  fis,2.) \! fis'4( \mf
  eis \> d cis b
  
  cis1 \!)
  R1*3
  
  R1*12
  
  r4 cis,( \p fis ais 
  b ais g d
  fis2.) ais4(
  b ais g d
  
  fis2.) g4(
  ais b cis b
  fis1)
  g4( b g fis
  
  cis1)
  d4( fis g b
  fis1)
  R1
  
  R1*15
  
  r2 r8. fis16 ~ fis8 g
  ais1
  g2 ~ g8. ais16 ~ ais8 g
  
  fis1
  g2 ~ g8. ais16 ~ ais8 g
  fis1
  R1
  
  R1*6
  
  fis'4-> ^"Enérgico" cis8-. fis-> ~ fis cis-. fis4->
  g4-> d8-. g-> ~ g d-. g4->
  fis4-> cis8-. fis-> ~ fis cis-. fis4->
  g4-> d8-. g-> ~ g d-. g4->
  
  fis2.-> fis4( ^"Cantabile"
  g8. fis16 ~ fis8 e d8. cis16 ~ cis8 b
  ais8. fis16 ~ fis2) fis'4
  g8. fis16 ~ fis8 e d8. e16 ~ e8 fis
  
  cis2. ~ cis8 cis(
  fis4. eis8 g4. cis,8
  fis8 cis8 ~ cis2 ~ cis8) cis8(
  g'4. fis8 eis8. cis16 ~ cis8 g'
  
  fis2. ~ fis8) fis(
  g4. fis,8 a'4. g8
  fis4. e8 cis4.) e'8(
  d4. cis,8 b4. ais8
  
  cis2. ~ cis8) fis,(
  \appoggiatura {d'16} g,8. a16 ~ a8 \appoggiatura {d} b ~ b4. ais8
  cis8 fis,8 ~ fis2 ~ fis8) \appoggiatura {cis'16} fis,8(
  g8. a16 ~ a8 b ~ b4. ais8
  
  fis2.) fis'4 \f ^"Marcato"
  eis fis g fis
  eis d eis d
  cis d cis b
  
  cis2. fis4( \mf ^"Cantabile"
  g fis eis d
  cis d eis d
  cis \> b ais g
  
  fis1) \p
  R1*3
    
  r2 r4 fis'( \p
  g2. fis4
  fis2.) fis4(
  g2. b4
  
  fis2 ~ fis4.) fis8(
  g4 ais b cis
  fis1)
  R1
  
  R1*6
  
  R1
  fis,8.^"Marcato" \mf \< fis16 ~ fis8 fis fis8. fis16 ~ fis8 fis
  fis8-> \sf r r4 r2
}

altoSax = \relative c' {
  \globalMusic
  
  
  \partial 2.
  r4 r2
  
  R1*12
  
  R1*4
  
  R1*3
  
  R1*4
  R1*3
  
  R1*12
  R1*12
  
  r8 fis \p \< g a b cis d e
  fis2 \mf r2
  r8. \appoggiatura g8 fis16 \< ~ fis8 g a8. b16 ~ b8 cis
  d8. \f \> cis16 ~ cis8 b a2 \mf
  
  \acciaccatura {b8 a} g4. fis8 e8. d16 ~ d8 cis
  d8. e16 ~ e8 d b8. a16 ~ a8 g
  fis8. fis16 ~ fis8 fis \> g4 a \p
  r2 r4 r8 d
  
  cis \< d e fis g a b cis
  d2 \f \> \acciaccatura {a8 g} fis8. a16 ~ a8 g
  fis1 \mf 
  R1
  
  R1*12
  
  r1
  r2 r4 r8 fis, \p \<
  g a b cis d8. e16 ~ e8 fis
  g4. fis8 ~ fis2
  
  r8. d'16 \f ~ d8 cis d8. d16 ~ d8 cis
  b2 \acciaccatura {b8 a} g \> fis e d
  cis1 \p
  \acciaccatura {b8 a} g \< a b cis d e fis g
  
  ais b cis d cis2 \f
  r8. cis16 ~ cis8 d8 \appoggiatura d8 cis4. b8
  b4 \> ais8 ais ~ ais4 \acciaccatura {g8 a} g fis
  
  g4 \mf fis2.
  R1*2
  
  R1*8
  
  R1*8
  R1*2
  
  \unfoldRepeats \repeat volta 8 {
    R1*2
  }
  
  R1*12
    
  R1*12
  
  R1
  R1*2
  
  fis8. fis16 ~ fis8 fis fis8. fis16 ~ fis8 fis
  fis8 r r4 r2
}

trumpet = \relative c'' {
  \globalMusic
  
  \partial 2.
  r4 r2
  
  R1*12
  
  R1*4
  
  R1*3
  
  R1*4
  R1*3
  
  R1*12
  R1*12
  
  R1*12
  
  R1*12
  
  R1*11
  
  R1*3
  
  R1*8
  
  r8. b'16 ~ b8 cis d8. e16 ~ e8 fis
  e8. d16 ~ d8 cis e8. a,16 ~ a8 a
  a8. b16 ~ b8 a e'8. a,16 ~ a8. a16
  a2 ~ a8. fis16 ~ fis8 a
  
  b cis d cis b a g4 ~ 
  g8. e16 ~ e8 cis cis2 ~ 
  cis2 r2
  b''2 ~ b8. a16 ~ a8 g
  b8. fis16 ~ fis4 ~ fis8. fis16 ~ fis8 g
  
  a g fis e ~ e8. fis16 ~ fis8 g
  fis cis ~ cis2 r4
  
  %{
  R1*8
  R1*2 %}
  
  R1
  \unfoldRepeats \repeat volta 7 {
    R1*2
  }
  
  R1*12
  
  R1*7
  r2 r4 r8 a,
  
  b cis d e fis g a b
  \appoggiatura {a16 b a} g2 r
  r4 r8 d' cis8. d16 ~ d8 e 
  cis2. \appoggiatura {a16} g8 fis
    
  g fis e2 r4
  r8 b d g d' e4 d8
  cis a g a ~ a4 \appoggiatura {bes16 a} g8 e
  
  fis8. fis16 ~ fis8 fis fis8. fis16 ~ fis8 fis
  fis8 r r4 r2
}

eGuitarI = \relative c'' {
  \globalMusic
  
  \partial 2.
  r4 r2
  
  R1*8 
  
  r8. <b d>16 \mf ~ <b d>8 <b d>8 <b d>8. <cis e>16 ~ <cis e>8 <b d>
  <ais cis>1
  r8. <b d>16 ~ <b d>8 <b d>8 <b d>8. <cis e>16 ~ <cis e>8 <b d>
  <ais cis>1
  
  
  b4 \p a g fis
  a g fis e
  g fis e d
  fis cis2.
  
  b''4 \mf a g fis
  a g fis e
  g fis e d
  cis1
  
  r8. b16 \< ~ b8 b b8. cis16 ~ cis8 d8
  cis1 \mf
  r8. b16 \< ~ b8 b b8. cis16 ~ cis8 d
  cis1 \mp
  
  cis8.^"Marcato" \mf \< cis16 ~ cis8 cis cis8. cis16 ~ cis8 cis
  cis8-^ \sf r8 r4 r2
    
  fis,1 \p
  \appoggiatura fis8 g8. fis16 ~ fis8 e8 d8. e16 ~ e8 fis
  cis1
  \appoggiatura fis8 g8. fis16 ~ fis8 e8 d8. e16 ~ e8 fis
  
  cis1 
  \appoggiatura fis8 g8. fis16 ~ fis8 e8 d8. e16 ~ e8 fis
  cis1
  r8. e16 ~ e8 e e8. fis16 ~ fis8 g
  
  fis1
  r8. e16 ~ e8 e e8. fis16 ~ fis8 g
  fis1
  R1
  
  fis1
  \appoggiatura fis8 g8. fis16 ~ fis8 e8 d8. e16 ~ e8 fis
  cis1
  \appoggiatura fis8 g8. fis16 ~ fis8 e8 d8. e16 ~ e8 fis
  
  cis1
  d'4 cis b a8 g
  fis1
  g'4 d b g
  
  fis1
  r8. b16 ~ b8 b8 b8. cis16 ~ cis8 d
  cis1 ~ 
  cis2. ~ cis8 d16 cis
  
  b1 ~ \f
  b8. b16 ~ b8 cis8 d8. e16 ~ e8 fis
  e8. d16 ~ d8 cis8 e2 ~ 
  e4. e16 e e8. fis16 ~ fis8 e
  
  d8. cis16 ~ cis8 b8 d2 ~ 
  d4. d16 d d8. e16 ~ e8 d
  cis1
  r8. b16 \> ~ b8 b8 b8. cis16 ~ cis8 d
  
  cis1 \mf 
  r8. b16 \> ~ b8 b8 b8. cis16 ~ cis8 d
  cis1 \p
  R1
  
  fis1 \p
  \appoggiatura fis8 g8. fis16 ~ fis8 e8 d8. e16 ~ e8 fis
  cis1
  \appoggiatura fis8 g8. fis16 ~ fis8 e8 d8. e16 ~ e8 fis
  
  cis1
  d4 cis b a8 g
  fis1
  g'4 d b g
  
  fis1
  r8. b16 ~ b8 b8 b8. cis16 ~ cis8 d
  cis1 ~ 
  cis2. ~ cis8 d16 cis
    
  b1 \f ~ 
  b8 fis b cis d8. e16 ~ e8 fis
  e8. d16 ~ d8 cis8 e2 ~
  e4. e16 e e8. fis16 ~ fis8 e
  
  d8. cis16 ~ cis8 b8 d2 ~ 
  d4. d16 d d8. e16 ~ e8 d
  cis1
  r8. d16 \> ~ d8 d8 d8. e16 ~ e8 d

  fis1 \mf
  r8. g16 \> ~ g8 fis8 e8. d16 ~ d8 e
  cis1 \p
  
  <b d>2 \mf \> ~ <b d>4. d8
  <a cis>2 ~ <a cis>4. cis8
  <g b>1
  
  <fis ais>1 \p
  <g b>8. <g b>16 ~ <g b>8 <g b>8 <g b>8. <g b>16 ~ <g b>8 <g b>
  <ais cis>1
  <g b>8. <g b>16 ~ <g b>8 <g b>8 <g b>8. <g b>16 ~ <g b>8 <g b>
  
  <fis ais>1
  <g b>8. <g b>16 ~ <g b>8 <g b>8 <g b>8. <g b>16 ~ <g b>8 <g b>
  <fis ais>1 ~ 
  <fis ais>1
  
  %{b1 \f ~ 
  b8 fis b cis d8. e16 ~ e8 fis
  e8. d16 ~ d8 cis8 e2 ~
  e4. e16 e e8. fis16 ~ fis8 e
  
  d8. cis16 ~ cis8 b8 d2 ~ 
  d4. d16 d d8. e16 ~ e8 d
  cis1
  R1
  
  R1*3%}
  
  \once \override Rest #'stencil = #ly:percent-repeat-item-interface::beat-slash
  \once \override Rest #'thickness = #0.48
  \once \override Rest #'slope = #1.7
  r2 ^"Solo Guitarra E. I"
  \once \override Rest #'stencil = #ly:percent-repeat-item-interface::beat-slash
  \once \override Rest #'thickness = #0.48
  \once \override Rest #'slope = #1.7
  r2
  \unfoldRepeats \repeat volta 10 { \rsMeasure }
  
  d''8^"Slides" \bendBefore \mf ~ <d g> ~ <d g>2. 
  cis8 \bendBefore ~ <cis fis> ~ <cis fis>2.
  \unfoldRepeats \repeat percent 6 {
    d8 \bendBefore \mf ~ <d g> ~ <d g>2. 
    cis8 \bendBefore ~ <cis fis> ~ <cis fis>2.
  }
  
  d8 \bendBefore \< ~ <d g> ~ <d g>2.
  fis1 \bendBefore \f
  
  R1*10
  
  R1*1
  
  fis,1 \p
  \appoggiatura fis8 g8. fis16 ~ fis8 e8 d8. e16 ~ e8 fis
  cis1
  \appoggiatura fis8 g8. fis16 ~ fis8 e8 d8. e16 ~ e8 fis
  
  \break
  cis1
  \appoggiatura fis8 g8. fis16 ~ fis8 e8 d8. e16 ~ e8 fis
  cis1
  g'4 \f d b g
  
  fis1
  r8. g'16 \mf ~ g8 g8 g8. a16 ~ a8 g
  fis1
  r8. g16 ~ g8 g8 g8. a16 ~ a8 g
  
  fis1 
  r8. g16 \> ~ g8 g8 g8. a16 ~ a8 g
  fis1 \p
  <cis fis>8. \mf ^"Marcato" \< <cis fis>16 ~ <cis fis>8 <cis fis>8 <cis fis>8. <cis fis>16 ~ <cis fis>8 <cis fis>
  
  <cis fis>8-^ \f r8 r4 r2
}

eGuitarIChords = \chords {
  % c1:m f2:m g2:m7
  
  \partial 2.
  s4 s2
  
  s1*26
  
  s1*12
  
  s1*12
  
  s1*12
  
  s1*12
  
  s1*12
  
  s1*2
  
  s1*8
  
  b1 b a a
  g:7 g:7 fis g
  fis g fis
  
  s1*15
    
  s1*12
  
  s1*17
}



eGuitarII = \relative c'' {
  \globalMusic
  
  \partial 2.
  r4 r2
  
  R1*26
  
  R1*12
  
  R1*12
  
  R1*12
  
  R1*12
  
  R1*12
  
  R1*2
  
  
  \once \override Rest #'stencil = #ly:percent-repeat-item-interface::beat-slash
  \once \override Rest #'thickness = #0.48
  \once \override Rest #'slope = #1.7
  r2 ^"Solo Guitarra E. II"
  \once \override Rest #'stencil = #ly:percent-repeat-item-interface::beat-slash
  \once \override Rest #'thickness = #0.48
  \once \override Rest #'slope = #1.7
  r2
  \unfoldRepeats \repeat volta 7 { \rsMeasure }
  
  R1*26
    
  R1*12
  
  R1*17
}

eGuitarIIChords = \chords {
  % c1:m f2:m g2:m7
  
  \partial 2.
  s4 s2
  
  s1*26
  
  s1*12
  
  s1*12
  
  s1*12
  
  s1*12
  
  s1*12
  
  s1*2
  
  fis1 g fis g
  fis g fis fis
  
  s1*26
    
  s1*12
  
  s1*17
}

aGuitar = \relative c' {
  \globalMusic
  
  \partial 2.
  r4 r2
  
  r2 r8 e,4. \p ~ 
  e1 ~ 
  e8 \< g8  b' \f \> g f b g \mf fis, \p \< ~ 
  fis cis' b' ais8 \mf ~ ais2  
  
  r8 g, \p f'' \f \> e b g e'4 \mf 
  fis,,8 \p \< cis' fis b \mf ~ b4 ais4\p 
  r8 e' d b g e b fis8 ~
  fis cis' fis ais ~ ais4 fis
  
  \unfoldRepeats \repeat volta 4 { \rsMeasure }
  
  b,8. \f d16 ~ d8 fis b8. fis16 ~ fis8 d8
  a8. cis16 ~ cis8 e a2
  b,8. b16 ~ b8 b b8. cis16 ~ cis8 b8
  ais16 b cis8 ~ cis2.
  
  b8. d16 ~ d8 fis b8. fis16 ~ fis8 d8
  a8. cis16 ~ cis8 e a2
  b,8. b16 ~ b8 b b8. cis16 ~ cis8 b8
  ais1
  
  \unfoldRepeats \repeat volta 4 { \rsMeasure }
  
  \jazzOff \improvisationOn
  cis'8. \< cis16 ~ cis8 cis cis8. cis16 ~ cis8 cis8
  cis8 \f r8 r4 r2
  \improvisationOff \jazzOn
  
  \unfoldRepeats \repeat volta 11 { \rsMeasure }
  \jazzOff \improvisationOn
  cis8 \f r8 r4 r2
  \improvisationOff \jazzOn
  
  \unfoldRepeats \repeat volta 12 { \rsMeasure }
  
  \unfoldRepeats \repeat volta 11 { \rsMeasure }
  \jazzOff \improvisationOn
  cis8 \f r8 r4 r2
  \improvisationOff \jazzOn
  
  \unfoldRepeats \repeat volta 12{ \rsMeasure }
  
  \unfoldRepeats \repeat volta 11 { \rsMeasure }
  <b d>2 \mf \> ~ <b d>4. d8
  <a cis>2 ~ <a cis>4. cis8
  <g b>1 \!
  
  \unfoldRepeats \repeat volta 8 { \rsMeasure }
  
  \unfoldRepeats \repeat volta 26 { \rsMeasure }
  \jazzOff \improvisationOn
  cis4 \f cis cis cis
  d8. d16 ~ d8 d d8. e16 ~ e8 d
  cis4 cis cis cis
  d8. d16 ~ d8 d d8. e16 ~ e8 d
  
  cis1 \p
  d
  cis
  d
  
  cis1
  d
  cis8. \< cis16 ~cis8 cis cis8. cis16 ~ cis8 cis8
  cis8 \f r8 r4 r2
  \improvisationOff \jazzOn
  
  \unfoldRepeats \repeat volta 15 { \rsMeasure }
  \jazzOff \improvisationOn
  cis8. \< cis16 ~ cis8 cis cis8. cis16 ~ cis8 cis8
  cis8 \f r8 r4 r2
  \improvisationOff \jazzOn
}

aGuitarChords = \chords {
  % c1:m f2:m g2:m7
  
  \partial 2.
  s4 s2
  
  s1*8
  
  g1 fis g fis
  
  s1*8
  
  g1 fis g fis 
  fis fis
  
  fis g fis g
  fis g fis g
  fis g fis fis
  
  fis g fis g
  fis g fis g
  fis g fis fis
  
  b b a a 
  g:7 g:7 fis g
  fis g fis fis
  
  fis g fis g
  fis g fis g
  fis g fis fis
  
  b b a a 
  g:7 g:7 fis g
  fis g fis
  
  s s s
  
  fis g fis g
  fis g fis fis
  
  b b a a 
  g:7 g:7 fis g
  fis g fis g
  
  fis g fis g
  fis g fis g
  fis g fis g
  
  fis g 
  fis g2 g8. a8. g8
  fis1 g2 g8. a8. g8 
  
  fis1 g
  fis1 g
  fis1 g
  
  fis1 fis
  
  fis g fis g
  fis g fis g
  fis g fis g
  fis g fis fis
  fis
}

piano = \relative c' {
  \globalMusic
  
  \partial 2.
  r4 r2
  
  R1*12
  
  R1*4
  
  R1*3
  
  <fis fis'>8-. \p r8 <ais ais'>8-. r8 <cis cis'>8-. r8 <fis fis'>8-. r8
  <g g'>8-. r8 <d d'>8-. r8 <b b'>8-. r8 <g g'>8-. r8
  <fis fis'>8-. \p r8 <ais ais'>8-. r8 <cis cis'>8-. r8 <fis fis'>8-. r8
  <g g'>8-. r8 <d d'>8-. r8 <b b'>8-. r8 <g g'>8-. r8
  
  \rsMeasure
  \jazzOff \improvisationOn
  cis8. \< cis16 ~ cis8 cis cis8. cis16 ~ cis8 cis8
  cis8 \f \improvisationOff \jazzOn r8 r8 fis'8-. fis,-. r8 r4
  
  \unfoldRepeats \repeat volta 11 { \rsMeasure }
  \jazzOff \improvisationOn
  cis8 \f \improvisationOff \jazzOn r8 r8 fis'8-. fis,-. r8 r4
  
  \unfoldRepeats \repeat volta 6 { \rsMeasure }
  <fis, fis'>8-. \p r8 <ais ais'>8-. r8 <cis cis'>8-. r8 <fis fis'>8-. r8
  <g g'>8-. r8 <d d'>8-. r8 <b b'>8-. r8 <g g'>8-. r8
  <fis fis'>8-. \p <fis fis'>8-. <ais ais'>8-. <ais ais'>8-. 
  <cis cis'>8-. <cis cis'>8-. <fis fis'>8-. <fis fis'>8-.
  <g g'>8-. <g g'>8-. <d d'>8-. <d d'>8-. <b b'>8-. <b b'>8-. <g g'>8-. <g g'>8-.
  \unfoldRepeats \repeat volta 2 { \rsMeasure }
  
  \unfoldRepeats \repeat volta 11 { \rsMeasure }
  \jazzOff \improvisationOn
  cis8 \f \improvisationOff \jazzOn r8 r8 fis'8-. fis,-. r8 r4
  
  \unfoldRepeats \repeat volta 6 { \rsMeasure }
  <fis, fis'>8-. \p r8 <ais ais'>8-. r8 <cis cis'>8-. r8 <fis fis'>8-. r8
  <g g'>8-. r8 <d d'>8-. r8 <b b'>8-. r8 <g g'>8-. r8
  <fis fis'>8-. \p <fis fis'>8-. <ais ais'>8-. <ais ais'>8-. 
  <cis cis'>8-. <cis cis'>8-. <fis fis'>8-. <fis fis'>8-.
  <g g'>8-. <g g'>8-. <d d'>8-. <d d'>8-. <b b'>8-. <b b'>8-. <g g'>8-. <g g'>8-.
  \unfoldRepeats \repeat volta 2 { \rsMeasure }
  
  \unfoldRepeats \repeat volta 11 { \rsMeasure }
  <b d>2 ~ <b d>4. d8
  <a cis>2 ~ <a cis>4. cis8
  <g b>1
  
  \unfoldRepeats \repeat volta 8 { \rsMeasure }
  
  \unfoldRepeats \repeat volta 26 { \rsMeasure }
  \jazzOff \improvisationOn
  cis4 \f cis cis cis
  d8. d16 ~ d8 d d8. e16 ~ e8 d
  cis4 cis cis cis
  d8. d16 ~ d8 d d8. e16 ~ e8 d
  
  cis1 \p
  d
  cis
  R1
  
  R1*2
  
  cis8. \< cis16 ~ cis8 cis cis8. cis16 ~ cis8 cis8
  cis8 \f \improvisationOff \jazzOn r8 r8 fis'8-. fis,-. r8 r4
  
  \unfoldRepeats \repeat volta 6 { \rsMeasure }
  <fis, fis'>8-. \p r8 <ais ais'>8-. r8 <cis cis'>8-. r8 <fis fis'>8-. r8
  <g g'>8-. r8 <d d'>8-. r8 <b b'>8-. r8 <g g'>8-. r8
  <fis fis'>8-. \p <fis fis'>8-. <ais ais'>8-. <ais ais'>8-. 
  <cis cis'>8-. <cis cis'>8-. <fis fis'>8-. <fis fis'>8-.
  <g g'>8-. <g g'>8-. <d d'>8-. <d d'>8-. <b b'>8-. <b b'>8-. <g g'>8-. <g g'>8-.
  \unfoldRepeats \repeat volta 5 { \rsMeasure }
  \jazzOff \improvisationOn
  cis8. \< cis16 ~ cis8 cis cis8. cis16 ~ cis8 cis8
  cis8 \f \improvisationOff \jazzOn r8 r4 r2
}

pianoChords = \chords {
  % c1:m f2:m g2:m7
  
  \partial 2.
  s4 s2
  
  s1*8
  
  s1*4
  
  s1*8
  
  s1 s s fis 
  fis fis
  
  fis g fis g
  fis g fis g
  fis g fis fis
  
  fis g fis g
  fis g fis g
  fis g fis fis
  
  b b a a 
  g:7 g:7 fis g
  fis g fis fis
  
  fis g fis g
  fis g fis g
  fis g fis fis
  
  b b a a 
  g:7 g:7 fis g
  fis g fis
  
  b a g
  
  fis g fis g
  fis g fis fis
  
  b b a a 
  g:7 g:7 fis g
  fis g fis g
  
  fis g fis g
  fis g fis g
  fis g fis g
  
  fis g 
  fis g2 g8. a8. g8
  fis1 g2 g8. a8. g8 
  fis1 g
  
  fis1 
  s1*3
  
  fis1 fis
  
  fis g fis g
  fis g fis g
  fis g fis g
  fis g fis fis
  fis
}

eBass = \relative c {
  \globalMusic
  \clef bass
  
  \partial 2.
  r4 r2
  
  R1*12
  
  b1 \f a g fis
  b a g 
  
  fis g fis g
  
  fis
  fis8.  fis16 ~ fis8 fis fis8. fis16 ~ fis8 fis
  fis8-^ r8 r4 r2
  
  
  fis1  g fis g
  fis g fis g 
  fis g fis fis8 r r4 r2
  
  
  fis1  g fis g
  fis1 g fis g
  fis g fis fis
  
  b b a a
  g g fis g
  fis g fis fis8 r r4 r2
  
  fis1 g fis g
  fis1 g fis g
  fis1 g fis fis
  
  b b a a
  g g fis g
  fis g fis
  b a g
  
  fis g fis g
  fis g fis fis
  
  b b a a
  g g fis g
  fis g
  
  \unfoldRepeats \repeat volta 8 {
    fis g 
  }
  
  fis4 \f fis fis fis
  g8. g16 ~ g8 g g8. a16 ~ a8 g
  fis4 fis fis fis
  g8. g16 ~ g8 g g8. a16 ~ a8 g
  
  fis1 \p g fis g
  
  fis1 
  g 
  fis8.  fis16 ~ fis8 fis fis8. fis16 ~ fis8 fis
  fis8-^ r8 r4 r2
  
  fis1 \f g fis g
  fis g fis g 
  fis g fis g 
  fis g fis fis8.  fis16 ~ fis8 fis fis8. fis16 ~ fis8 fis
  
  fis8-^ r8 r4 r2
  
  \bar "|."
}

percussion = \drummode {
  \globalMusic
  
  \partial 2.
  gui8^"Palmas" gui gui4 gui
  
  \unfoldRepeats \repeat volta 25 { r4 gui8 gui gui4 gui }
  gui4 r4 r2
  
  \once \override Rest #'stencil = #ly:percent-repeat-item-interface::beat-slash
  \once \override Rest #'thickness = #0.48
  \once \override Rest #'slope = #1.7
  r2 ^"Bongos libres"
  \once \override Rest #'stencil = #ly:percent-repeat-item-interface::beat-slash
  \once \override Rest #'thickness = #0.48
  \once \override Rest #'slope = #1.7
  r2
  \unfoldRepeats \repeat volta 11 { \rsMeasure }
  
  \once \override Rest #'stencil = #ly:percent-repeat-item-interface::beat-slash
  \once \override Rest #'thickness = #0.48
  \once \override Rest #'slope = #1.7
  r2 ^"Bongos y Cajón con escobillas libres"
  \once \override Rest #'stencil = #ly:percent-repeat-item-interface::beat-slash
  \once \override Rest #'thickness = #0.48
  \once \override Rest #'slope = #1.7
  r2
  \unfoldRepeats \repeat volta 11 { \rsMeasure }
  
  \once \override Rest #'stencil = #ly:percent-repeat-item-interface::beat-slash
  \once \override Rest #'thickness = #0.48
  \once \override Rest #'slope = #1.7
  r2 ^"Bongos y Cajón flamenco libres"
  \once \override Rest #'stencil = #ly:percent-repeat-item-interface::beat-slash
  \once \override Rest #'thickness = #0.48
  \once \override Rest #'slope = #1.7
  r2
  \unfoldRepeats \repeat volta 11 { \rsMeasure }
  \unfoldRepeats \repeat volta 12 { \rsMeasure }
  
  \unfoldRepeats \repeat volta 11 { \rsMeasure }
  \unfoldRepeats \repeat volta 3 { \rsMeasure }
  
  \unfoldRepeats \repeat volta 8 { \rsMeasure }
  \unfoldRepeats \repeat volta 26 { \rsMeasure }
  
  gui4^"Cajón flamenco y Caja" gui gui gui
  gui8. gui16 ~ gui8 gui gui8. gui16 ~ gui8 gui
  gui4 gui gui gui
  gui8. gui16 ~ gui8 gui gui8. gui16 ~ gui8 gui
  
  \jazzOff \improvisationOn
  \unfoldRepeats \repeat volta 6 { gui2 gui }
  \improvisationOff \jazzOn
  gui8. gui16 ~ gui8 gui gui8. gui16 ~ gui8 gui
  
  \once \override Rest #'stencil = #ly:percent-repeat-item-interface::beat-slash
  \once \override Rest #'thickness = #0.48
  \once \override Rest #'slope = #1.7
  r2 ^"Bongos, Cajón flamenco y Caja libres"
  \once \override Rest #'stencil = #ly:percent-repeat-item-interface::beat-slash
  \once \override Rest #'thickness = #0.48
  \once \override Rest #'slope = #1.7
  r2
  
  \unfoldRepeats \repeat volta 15 { \rsMeasure }
  
  gui8. gui16 ~ gui8 gui gui8. gui16 ~ gui8 gui
  gui8 r8 r4 r2
}



% score
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
\score {
  <<
    \scoreMarkup
    \new StaffGroup <<
      \new Staff \with { instrumentName = #"Voz"} { \lead }
      \new Staff \with { instrumentName = #"Violín"} { \violin }
      \new Staff \with { instrumentName = #"Saxo alto"} { \transpose ees c \altoSax }
      \new Staff \with { instrumentName = #"Trompeta"} { \transpose bes c \trumpet }
      
      \eGuitarIChords \new Staff \with { instrumentName = #"Guitarra E. I"} { \eGuitarI }
      \eGuitarIIChords \new Staff \with { instrumentName = #"Guitarra E. II"} { \eGuitarII }
      \aGuitarChords \new Staff \with { instrumentName = #"Guitarra A."} { \aGuitar }
      \pianoChords \new Staff \with { instrumentName = #"Piano"} { \piano }
      
      \new Staff \with { instrumentName = #"Bajo E."} { \eBass }
      \new DrumStaff \with {
        \remove "Time_signature_engraver"
        \override StaffSymbol #'line-count = #1
        drumStyleTable = #percussion-style
        instrumentName = #"Percusión"
      } { \percussion }
    >>
  >>
}


