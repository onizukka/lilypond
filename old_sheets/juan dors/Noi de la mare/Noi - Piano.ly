﻿verseSA = \relative c'
{
     r2. r2. r2. r2. 
     r2. r2. r2. r2. 
}

verseSAC = \relative c'
{
     r2.^\markup{ \small {guitarra}} r2. r2. r2.
}

end = \relative c'
{
   fis,2 d'4
   b2 d4
   fis,2 d'4
   b2 d4
   fis,2 d'4
   b2.
   r2. r2. 
}

verseAAh = \chordmode
{
   b2. b2. e2. b2.
   b2. b2. e2 fis4:7 b2.
}

verseABh = \chordmode
{
   b2. b2. fis2.:7 b2.
   b2. b2. fis2.:7 b2.
}

verseACh = \chordmode
{
   r2. r2. r2. r2.
}

verseBAh = \chordmode
{
   b2.:m b2.:m e2.:m b2.:m
   b2.:m b2.:m e2.:m b2.:m
}

verseBBh = \chordmode
{
   b2.:m b2.:m fis2.:7 b2.:m
   b2.:m b2.:m fis2.:7 b2.:m
}

endh = \chordmode
{
   r2. r2.
   r2. r2.
   r2. r2.
   r2. b2.:m
}

piano = \new StaffGroup
<<
	\new ChordNames 
   {
      \verseAAh
      \verseABh
      \verseABh
      \verseACh
      
      \verseBAh
      \verseBBh
      
      \verseBAh
      \verseBBh
      
      \verseBAh
      \verseBBh
      
      \verseBAh
      \verseBBh
      
      \endh
   }
   
	\new Staff
	{
      \time 3/4
		\key b \major
   
      \verseSA
      \verseSA
      \verseSA
      \verseSAC
      
		\key b \minor
      
      \mark \markup {A}
      \verseSA
      \verseSA
      
      \mark \markup {B}
      \verseSA
      \verseSA
      
      \mark \markup {C}
      \verseSA
      \verseSA
      
      \mark \markup {D}
      \verseSA
      \verseSA
      
      \mark \markup {E}
      \end
      
      \bar "||"
   }

>>