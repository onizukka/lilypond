\version "2.12.3"

\include "Noi - Piano.ly"

\header {
	title = "Noi de la mare"
	subtitle = "Tiempos"
	composer = "Juan d'Ors"
	instrument = "Piano"
}

\markuplines
{
     "(capella)"
     "(capella campanilla)"
     "(capella menor)"
     "motivo guitarra"
}

<<
	\piano
>>