﻿intro = \relative c'
{
   \clef F
   d8 f a,2.
   g8 bes e,2 ~ e8 d
   cis4 bes'2 a8 g
   f1
   
   \clef G
   d''8 f a,2.
   g8 bes e,2 ~ e8 d
   cis4 bes'2 a8 g
   
   r1 \fermata
}

end = \relative c'
{
   \clef F
   d8 f a,2.
   g8 bes e,2 ~ e8 d
   cis4 bes'2 a8 g
   f1
   
   \clef G
   d''8 f a,2.
   g8 bes e,2 ~ e8 d
   cis4 bes'2 a8 g
   
   r2. \fermata d'8 f
   \bar "|:" a,4^\markup{\small{ad.lib}} a a d8 f \bar ":|"
}

verseSA = \relative c'
{
   r1 r1 r1 r1
   r1 r1 r1 r1
}


introH = \chordmode
{
   d1:m g1:m a1 d1:m
   d1:m g1:m a1 r1
}

verseAAH = \chordmode
{
   d1:m f1:aug/a f1/a g1:9^1
   g1:m9 a1:7 d1:m a1:7
}

verseAAtoBAH = \chordmode
{
   d1:m f1:aug/a f1/a g1:9^1
   g1:m9 a1:7 d1:m d1:7
}

verseBAH = \chordmode
{
   g1:m7 c1:7 f1:maj bes:7
   cis1:dim7^1 a1 d1:m d1:7.9-
}

verseBAtoAAH = \chordmode
{
   g1:m7 c1:7 f1:maj bes:7
   cis1:dim7^1 a1 d1:m a1:7
}

verseBBtoAAH = \chordmode
{
   g1:m7 c1:7 f1:maj bes:7
   cis1:dim7^1 a1 r1 r1
}

endH = \chordmode
{
   d1:m g1:m a1 d1:m
   d1:m g1:m a1 r1
   r1
}

piano = \new StaffGroup
<<
	\new ChordNames {
   
		\introH
      
      \verseAAH
      \verseAAH
      
      \verseBAH
      \verseBAtoAAH
      
      \verseAAtoBAH
      \verseBAH
      \verseBAH
      \verseBBtoAAH
      
      \verseAAtoBAH
      \verseBAH
      \verseBAH
      \verseBAtoAAH
      
		\endH
	}
	\new Staff
	{
		\key d \minor
      
      \intro
      
      \mark \markup {A}
      \verseSA
      \verseSA
      
      \mark \markup {B}
      \verseSA
      \verseSA
      
      \mark \markup {C}
      \verseSA
      \verseSA
      \verseSA
      \verseSA
      
      \mark \markup {D}
      \verseSA
      \verseSA
      \verseSA
      \verseSA
      
      \end
	}
>> 