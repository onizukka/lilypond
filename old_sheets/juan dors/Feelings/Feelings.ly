\version "2.12.3"

\include "Feelings - Piano.ly"

\header {
	title = "Feelings"
	subtitle = "Tiempos"
	composer = "Juan d'Ors"
	instrument = "Piano"
}

\score
{
	\piano
}