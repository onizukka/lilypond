﻿addedA = \relative c''
{
	<c, ees g c>8\arpeggio r8 r4 r2 
}

introA = \relative c'
{
	\textLengthOff
	r1 ^\markup {\small {contestando a la guitarra}}
	
	r2 r8 g	^\markup {\italic { rall.}} c d
	<g, c ees>2 ^\markup {\musicglyph #"scripts.ufermata"} \arpeggio ~ <g c ees>4. c8
	
	b8 d8 ~ d2.
	r8 g, b d \times 2/3 {f4 ees d} 
	c1 
	
	r8 g' c d ees4. c8 
	b8 d ~ d2.
	r8 g, b d \times 2/3 {f4 ees d} 
	c1 
}

verseAA = \relative c''
{
	r4 g aes4. g8 
	g1 
	r8 g aes g \times 2/3 {b4 a aes}
	g4. ees8 g2
	
	r4 g' aes4. g8 
	b1
	r8 g, b d \times 2/3 {f4 d ees}
	c1
}

verseAB = \relative c''
{
	\textLengthOn
	c8 r8 r4 r2
	r1 ^\markup {\small {octavs}}
	r8 <g g'> <aes aes'> <g g'> \times 2/3 { <d' d'>4 <b b'> <d d'> }
	\times 2/3 { <ees ees'> <c c'> <g g'> ~ } <g g'>2
	
	r8 <g g'> <aes aes'> <g g'> \times 2/3 { <b b'>4 <g g'> <a a'> }
	\times 2/3 { <b b'> <g g'>2 ~ }  <g g'>2	
	r8 g' f g ees g d g 
	c,8 r8 r4 r2
}

verseBA = \relative c'
{
	\textLengthOff
	r1 ^\markup {\small {harm/ritm}} 
	r1 
	r1
	r1
	r1
	r8 <c c'>4 <c c'>8 <bes bes'>4 <aes aes'>
	<g g'>2 r2
	<c ees g c>8\arpeggio r8 r4 r2 
}

verseBB = \relative c'
{
	\textLengthOff
	r1 ^\markup {\small {ritm/hard}} 
	r1 
	r1
	r1
	r1
	r8 <c c'>4 <c c'>8 <bes bes'>4 <aes aes'>
	<g g'>2 r2
	<c ees g c>8\arpeggio r8 r4 r2 
}

verseBEnd = \relative c''
{
	\textLengthOff
	r1 ^\markup {\small {harm/ritm/fill}} 
	r1 
	r1 
	r1 ^\markup {\small {dulce}}
	
	r1 r1 r1 r1
	r1 r1 r1 r1
}

verseC = \relative c''
{
	r4 <g g'> <ees ees'>4. <c c'>8
	<b b'>16 <c c'> <d d'>8 ~ <d d'>2.
	r8 <g, g'> <b b'> <d d'> \times 2/3 { <f f'>4 <ees ees'> <d d'> }
	<c c'>1
	
	r4 g'' ees4. c8
	b16 c d 8 ~ d2.
	r8 g, b d \times 2/3 { f4 d ees }
	c1
}

verseDA = \relative c''
{
	r4 <g g'> <ees' ees'>4. <c c'>8
	<b b'>1
	r4 <g g'> <f' f'>4. <d d'>8
	<ees ees'>1
	
	r4 <g, g'> <ees' ees'>4. <c c'>8
	<b b'>1
	r4 b \times 2/3 { f' d ees }
	c1
}

verseDB = \relative c''
{
	r4 <g g'> \times 2/3 { <aes aes'>4 <g g'> <f f'> }
	<d d'> <b b'>2.
	r4 <g' g'> \times 2/3 { <aes aes'>4 <g g'> <g g'> }
	\times 2/3 { <f f'> <ees ees'> <d d'> } <c c'>2
	#(set-octavation 1)
	r4 <g'' g'> \times 2/3 { <aes aes'>4 <g g'> <g g'> }
	\times 2/3 { <f f'> <d d'> <b b'> ~ } <b b'>2
	#(set-octavation 0)
	r8 g' f g ees g d ees 
	r8 c ~ c2.
}

verseDC = \relative c''
{
	\textLengthOff
	r1 ^\markup {octavs}
	r1
	r1
	r1
	
	r4 <g g'> <ees ees'>4. <c c'>8
	<b b'>2 <d d'>2
	r8 g' f g ees g r8 d16 ees 
	c1
}

bridge = \relative c''
{
	\times 2/3 { r4 <c, c'> <c c'>} \times 2/3 { <c c'> <bes bes'> <aes aes'> }
	\times 2/3 { <g g'> <g g'> <g g'>} \times 2/3 { <fis fis'> <aes aes'> <fis fis'> }
	\times 2/3 { <g g'> <c ees g c> <c ees g c> } \times 2/3 { <c ees g c> <bes bes'> <aes aes'> }
	\times 2/3 { <g g'> <aes aes'> <fis fis'>} \times 2/3 { <g g'> <aes aes'> <fis fis'> }
	<g g'>2 \glissando g'''8 r g4 \glissando 
}

arpegg = \relative c'' 
{
	g8 r8 r4 r4 g''4 \glissando
	g,,8 r8 r4 r4 g''4 \glissando
	c,,,8 r8 r4 r4 g'''4 \glissando
	c,,,8 r8 r4 r4 g'''4 \glissando
}

arpeggEnd = \relative c'' 
{
	g8 r8 r4 r4 g''4 \glissando
	g,,8 r8 r4 r4 g''4 \glissando
	c,,,8 r8 r4 r4 g'''4 \glissando
	c,,,8 r8 r8 g'8 aes4 g
}

verseEA = \relative c'
{
	\textLengthOff
   \bar "|:"
   b8^\markup {\small {ad.lib.}} b r8 g'8 aes4 g
   c,4 r8 g'8 aes4 g
   b8 g r8 g aes4 g
   c4 r8 g'8 aes4 g
   \bar ":|"
}

verseEB = \relative c''
{
   \bar "|:"
   \times 2/3 {d4 b d} \times 2/3 {d b d}
   \times 2/3 {ees c ees} \times 2/3 {ees c ees}
   \times 2/3 {f d f} \times 2/3 {f d f}
   \times 2/3 {g ees g} \times 2/3 {ees c ees}
   \bar "|:"
}

verseEC = \relative c''
{
   \bar "|:"
   \times 2/3 {d8 b g} \times 2/3 {d' b d} \times 2/3 {d8 b g} \times 2/3 {d' b d}
   \times 2/3 {ees c g} \times 2/3 {ees' c ees} \times 2/3 {ees c g} \times 2/3 {ees' c ees}
   \times 2/3 {f d g,} \times 2/3 {f' d f} \times 2/3 {f d g,} \times 2/3 {f' d f}
   \times 2/3 {g ees c} \times 2/3 {g' ees g} \times 2/3 {g ees c} \times 2/3 {ees c g}
   \bar ":|"
}

verseED = \relative c''
{
	\textLengthOff
   \bar "|:"
   r1^\markup{ \small{rall. al final}} r1 r1 r1
   \bar ":|"
}

verseEEnd = \relative c''
{
   b2 r4 b
   c2 r4 c
   b2 r4 b
   c2^\markup {\small {guit.}} r2
}

end = \relative c'
{
   \times 2/3 {c4 ees g} \times 2/3 {g, b d} \times 2/3 {c4 ees g} \times 2/3 {g, b d}
   \times 2/3 {c4 ees g} \times 2/3 {g, b d}
   \times 2/3 {c4^\markup {\small{rall.}} r4 g} \times 2/3 {c4 r4 g}
   \times 2/3 {c4 r4 g}  \times 2/3 {c4 r4 g}
   c1
}

addedAh = \chordmode
{
	c1:m
}

introAh = \chordmode
{
	r1	 r1
	r1   g1:7 g1:7 c1:m
	c1:m g1:7 g1:7 c1:m
}

verseAAh = \chordmode
{
	c1:m g1:7 g1:7 c1:m
	c1:m g1:7 g1:7 c1:m
}

verseAh = \chordmode
{
	c1:m g1:7 g1:7 c1:m
	c1:m g1:7 g1:7 c1:m 
}

verseBh = \chordmode
{
	g1:7 c1:m g1:7 c1:m
	g1:7 c1:m g1:7 c1:m
}

verseBEndh = \chordmode
{
	g1:7 c1:m g1:7 c2:m c2:7
	f2:m f2:m6 c1:m g1:7 c2:m c2:7
	f2:m f2:m6 c1:m g1:7 c1:m
}

bridgeh = 
{
	r1 r1 r1 r1 r1
}

arpeggh = \chordmode
{
	g1:7 g1:7 c1:m c1:m
}

verseEh = \chordmode
{
	g1:7 c1:m g1:7 c1:m
}

endh = \chordmode
{
   c1:m g1:m c1:m g1:m
   c1:m g1:m
   c1:m c1:m c1:m
}

piano = \new StaffGroup
<<
	\new ChordNames {
		\introAh
		\verseAh
		\verseAh
		\addedAh
		
		\verseBh
		\verseBEndh
		
		\verseAh
		\addedAh
		
		\verseBh
		\verseAh
		\verseAh
		\verseAh
		
		\bridgeh
		
		\arpeggh
		\arpeggh
		
      \verseEh
      \verseEh
      \verseEh
      \verseEh
      \verseEh
      
      \verseBh
		\verseBh
		\verseBh
      
      \endh
	}
	\new Staff
	{
		\key c \minor
		
		\introA
		\verseAA
		\verseAB
		\addedA
		
		\verseBA
		\verseBEnd
		
		\verseC
		\addedA
		
		\verseBB
		\verseDA
		\verseDB
		\verseDC
		
		\bridge
		
		\arpegg
		\arpeggEnd
		
      \verseEA
      \verseEB
      \verseEC
      \verseED
      \verseEEnd
      
      \verseBB
      \verseDA
      \verseDC
      
      \end
      \bar "||"
	}
>> 