\version "2.12.3"

\include "Tiempos - Piano.ly"

\header {
	title = "Tiempos"
	subtitle = "Tiempos"
	composer = "Juan d'Ors"
	instrument = "Piano"
}

\markuplines
{
     "(cencerros)"
     "Tiempos ... Una suite de canciones"
     "Tiempos ... Suite, suite, suite [...]"
	 "(cencerros se pierden)"
}

<<
	\piano
>>