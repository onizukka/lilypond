\version "2.18"
\include "imports\main.ly"

slides =
#(make-music 'CrescendoEvent
             'span-direction START
             'span-type 'text
             'span-text "slides")

% Instrument Music
% -------------------------------------------------
guia = {

  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0	
  
  \key b \minor
  
  R1*8 _"Empiezan primero palmas y le sigue una introduccion libre de guitarra española"
  
  \break
  R1*4 _"Entra la guitarra eléctrica"
  
  R1*8 _"Obligado de guitarras"
  
  \break
  \teeny
  fis,8 _\markup \normalsize "Groove" ^"Piano" \p r ais r cis r fis r
  g r d r b r g r
  fis r ais r cis r fis r
  g r d r b r g r
  
  \normalsize
  R1
  R1*1 _"Tutti cresc."
  \teeny
  fis4-^ r8 _\markup \normalsize "Silencio" _"Terminan las palmas" fis'8 ^"Piano" fis, r8 r4
  
  \break
  \bar "||"
  \mark \markup \boxed "Estrofa"
  \normalsize
  R1*6 _"Groove" \p
  \break
  R1*5
  \teeny
  fis4-^ r8 _\markup \normalsize "Silencio" fis'8 ^"Piano" fis, r8 r4
  
  \break
  \bar "||"
  \mark \markup \boxed "Estrofa"
  \normalsize
  R1*6 _"Groove" \mf
  \break
  \teeny
  fis8 ^"Piano" r ais r cis r fis r
  g r d r b r g r
  R1*3
  R1*1 \<
  
  \break
  \bar "||"
  \mark \markup \boxed "Estribillo"
  \normalsize
  R1*6 \! \f
  \break
  R1*2
  R1*2 \>
  R1*1 \! \p
  fis4-^ r8 _\markup \normalsize "Silencio" fis'8 ^\markup \tiny "Piano" fis, r8 r4
  
  \break
  \bar "||"
  \mark \markup \boxed "Estrofa"
  \normalsize
  R1*6 _"Groove"
  \break
  \teeny
  fis8 ^"Piano" r ais r cis r fis r
  g r d r b r g r
  R1*3
  R1*1 \<
  
  \break
  \bar "||"
  \mark \markup \boxed "Estribillo"
  \normalsize
  R1*6 \! \f
  \break
  R1*2
  R1*2 
  R1*1 
  \teeny
  d'2. \> ^"Piano y Guitarras" ~ d8 d
  cis2. ~ cis8 cis
  b1
  \break
  \normalsize
  R1*7 \! \p
  R1 \< 
  
  \break
  \bar "||"
  \mark \markup \boxed "Estribillo"
  R1*6 \! \f
  \break
  R1
  R1 _"Entra solo de Violín"
  R1*2
  
  \break
  R1*1
  R1*1 _"Entran slides de guitarra"
  R1*2
  
  \break
  R1*12 _"12 compases"
  
  \break
  \teeny
  cis4 _\markup \normalsize "terminan slides y violín" _\markup \normalsize "Marcato" ^"Piano, Española y Bajo" \f cis cis cis
  d8. d16 ~ d8 d d8. e16 ~ e8 d
  cis4 cis cis cis
  d8. d16 ~ d8 d d8. e16 ~ e8 d
  
  \break
  cis2 _\markup \normalsize "menos intensidad" ^"Piano y Bajo" cis
  d d8. e16 ~ e8 d
  cis2 cis
  d d8. e16 ~ e8 d
  
  \break
  cis2 _\markup \normalsize "se va perdiendo" b
  ais g
  fis1 g ^"Solo Bajo"
  fis g
  
  fis8. \< _\markup \normalsize "Tutti creciendo" ^"Piano, Española y Bajo" fis16 ~ fis8 fis fis8. fis16 ~ fis8 fis
  fis4-^ \! r8 _\markup \normalsize "Silencio" fis'8 ^"Piano" fis, r8 r4
  
  \break
  \bar "||"
  \mark \markup \boxed "Estrofa"
  \normalsize
  R1*6 _"Groove"
  \teeny
  fis8 ^"Piano" r ais r cis r fis r
  g r d r b r g r
  \normalsize
  \break
  R1*7 _"Creciendo"
  \teeny
  fis8. _\markup \normalsize"Piano subito" _\markup \normalsize"Tutti creciendo" ^"Todos" fis16 ~ fis8 fis fis8. fis16 ~ fis8 fis
  fis8 r r4 r2
  
  \bar "|."
}



\header {
  instrument = \markup \center-column { 
    \line {"Guía para"}
    \line {"Piano, Guitarra Española, Bajo y Percusión"}
 }
}

\paper {  	
  % distancia entre sistemas
  system-system-spacing #'basic-distance = #18
  
  % distancia entre el título y el primer sistema
  markup-system-spacing #'basic-distance = #20
  
  systems-per-page = 9
}

<<
  \scoreChords
  \new Staff << \relative c'' \guia >>
>>