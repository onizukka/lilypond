\version "2.16.1"
% \include "imports\main.ly"

slides =
#(make-music 'CrescendoEvent
             'span-direction START
             'span-type 'text
             'span-text "slides")

% Instrument Music
% -------------------------------------------------
vozmidi = {

  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0	
  
  \key b \minor
  
  \partial 4
  r4
  
  R1*26
  
  cis8. cis16 ~ cis8 cis cis8. cis16 ~ cis8 cis8
  d8. d16 ~ d8 d8 ~ d4 r
  cis8. cis16 ~ cis8 cis cis8. cis16 ~ cis8 cis8
  d8. d16 ~ d8 d8 ~ d4 r
  
  cis8. cis16 ~ cis8 cis cis8. cis16 ~ cis8 cis8
  d8. d16 ~ d8 d8 d d b g
  fis1
  g8. g16 ~ g8 g g8. b16 ~ b8 b8
  
  fis1
  R1*3
  
  cis'8. ais16 ~ ais8 b cis8. ais16 ~ ais8 b8
  cis8. d16 ~ d8 b8 ~ b2
  cis8. ais16 ~ ais8 b cis8. ais16 ~ ais8 b8
  cis8. d16 ~ d8 b8 ~ b2
  
  cis8. ais16 ~ ais8 b cis8. ais16 ~ ais8 b8
  cis8. d16 ~ d8 b8 ~ b b a g
  fis1
  e'8. e16 ~ e8 e e8. d16 ~ d8 e8
 
  cis8. fis,16 ~ fis4 r2
  e'8. e16 ~ e8 e e8. d16 ~ d8 e8
  cis1
  R1  
  
  b1 ~ 
  b4. cis8 d4 b
  a1
  r4 cis2 b8 a
  
  b4 g2.
  r4 b4 a g
  fis1
  r1
  
  R1*4
  
  cis'8. ais16 ~ ais8 b cis8. cis16 ~ cis8 cis8
  d8. d16 ~ d8 b8 ~ b4 r
  cis8. cis16 ~ cis8 cis cis8. cis16 ~ cis8 cis8
  d8. d16 ~ d8 b8 ~ b4 r
  
  cis8. ais16 ~ ais8 b cis8. ais16 ~ ais8 b8
  cis8. d16 ~ d8 b8 ~ b b a g
  fis1 
  e'8. d16 ~ d8 d d8. e16 ~ e8 d8
  
  cis8. fis,16 ~ fis4 r2
  e'8. d16 ~ d8 cis b8. cis16 ~ cis8 g8
  fis1
  r1
  
  R1*6
  R1*5
  R1*3
  
  R1*8
  
  R1*6
  R1*4
  
  \unfoldRepeats \repeat volta 8 {
    R1*2 
  }
  
  R1*16
  
  R1*2
  
  cis'8. cis16 ~ cis8 cis cis8. cis16 ~ cis8 cis8
  d8. d16 ~ d8 d8 ~ d4 r
  cis8. cis16 ~ cis8 cis cis8. cis16 ~ cis8 cis8
  d8. d16 ~ d8 d8 ~ d4 r
  
  cis8. cis16 ~ cis8 cis cis8. cis16 ~ cis8 cis8
  d8. d16 ~ d8 d8 d4 b8 cis16 b
  fis1
  g'8. g16 ~ g8 g g8. fis16 ~ fis8 e8
  
  cis1
  g8 d' ~ d2 b8 g
  fis8 cis' ~ cis2 ais8 fis
  g8 d' ~ d2 b8 g
  
  fis8 cis' ~ cis2 ais8 fis
  g8 d' ~ d2 b8 g
  fis1
  r1
  
  r1
   
  \bar "|."
}