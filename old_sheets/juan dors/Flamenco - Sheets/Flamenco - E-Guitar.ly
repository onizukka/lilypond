\version "2.16.1"
\include "imports\main.ly"

slides =
#(make-music 'CrescendoEvent
             'span-direction START
             'span-type 'text
             'span-text "slides")

% Instrument Music
% -------------------------------------------------
eguitar = {

  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0	
  \set countPercentRepeats = ##t
  \set repeatCountVisibility = #(every-nth-repeat-count-visible 2)
  
  \key b \minor
  
  R1*8 ^ \markup \tiny "Introduccion de guitarra española"
  
  r8. <b d>16 \mf ~ <b d>8 <b d>8 <b d>8. <cis e>16 ~ <cis e>8 <b d>
  <ais cis>1
  r8. <b d>16 ~ <b d>8 <b d>8 <b d>8. <cis e>16 ~ <cis e>8 <b d>
  <ais cis>1
  
  
  b4 \p a g fis
  a g fis e
  g fis e d
  fis cis2.
  
  b''4 \mf a g fis
  \break
  a g fis e
  g fis e d
  cis1
  
  r8. b16 \< ~ b8 b b8. cis16 ~ cis8 d8
  cis1 \mf
  \break
  r8. b16 \< ~ b8 b b8. cis16 ~ cis8 d
  cis1 \mp
  
  cis8.^"marcato" \mf \< cis16 ~ cis8 cis cis8. cis16 ~ cis8 cis
  cis8-^ \sf r8 r4 r2
  
  \break
  \bar "||"
  \mark \markup \boxed "Estrofa"
  
  fis,1 \p
  \appoggiatura fis8 g8. fis16 ~ fis8 e8 d8. e16 ~ e8 fis
  cis1
  \appoggiatura fis8 g8. fis16 ~ fis8 e8 d8. e16 ~ e8 fis
  
  cis1 
  \appoggiatura fis8 g8. fis16 ~ fis8 e8 d8. e16 ~ e8 fis
  cis1
  r8. e16 ~ e8 e e8. fis16 ~ fis8 g
  
  fis1
  r8. e16 ~ e8 e e8. fis16 ~ fis8 g
  fis1
  R1
  
  \break
  \bar "||"
  \mark \markup \boxed "Estrofa"
  
  fis1
  \appoggiatura fis8 g8. fis16 ~ fis8 e8 d8. e16 ~ e8 fis
  cis1
  \appoggiatura fis8 g8. fis16 ~ fis8 e8 d8. e16 ~ e8 fis

  cis1
  d'4 cis b a8 g
  fis1
  g'4 d b g
  
  fis1
  r8. b16 ~ b8 b8 b8. cis16 ~ cis8 d
  cis1 ~ 
  cis1
  
  \break
  \bar "||"
  \mark \markup \boxed "Estribillo"
  
  b1 ~ \f
  b8. b16 ~ b8 cis8 d8. e16 ~ e8 fis
  e8. d16 ~ d8 cis8 e2 ~ 
  e4. e16 e e8. fis16 ~ fis8 e
  
  \break
  d8. cis16 ~ cis8 b8 d2 ~ 
  d4. d16 d d8. e16 ~ e8 d
  cis1
  r8. b16 \> ~ b8 b8 b8. cis16 ~ cis8 d
  
  \break
  cis1 \mf 
  r8. b16 \> ~ b8 b8 b8. cis16 ~ cis8 d
  cis1 \p
  R1
  
  \break
  \bar "||"
  \mark \markup \boxed "Estrofa"
  
  fis1 \p
  \appoggiatura fis8 g8. fis16 ~ fis8 e8 d8. e16 ~ e8 fis
  cis1
  \appoggiatura fis8 g8. fis16 ~ fis8 e8 d8. e16 ~ e8 fis
  
  \break
  cis1
  d4 cis b a8 g
  fis1
  g'4 d b g
  
  \break
  fis1
  r8. b16 ~ b8 b8 b8. cis16 ~ cis8 d
  cis1 ~ 
  cis1
  
  \break
  \bar "||"
  \mark \markup \boxed "Estribillo"
  
  b1 \f ~ 
  b8 fis b cis d8. e16 ~ e8 fis
  e8. d16 ~ d8 cis8 e2 ~
  e4. e16 e e8. fis16 ~ fis8 e
  
  \break
  d8. cis16 ~ cis8 b8 d2 ~ 
  d4. d16 d d8. e16 ~ e8 d
  cis1
  r8. d16 \> ~ d8 d8 d8. e16 ~ e8 d
   
   \break
   fis1 \mf
   r8. g16 \> ~ g8 fis8 e8. d16 ~ d8 e
   cis1 \p
   
   \mark \markup \boxed "Puente"
   <b d>2 \mf \> ~ <b d>4. d8
   <a cis>2 ~ <a cis>4. cis8
   <g b>1
   
   \break
   <fis ais>1 \p
   <g b>8. <g b>16 ~ <g b>8 <g b>8 <g b>8. <g b>16 ~ <g b>8 <g b>
   <ais cis>1
   <g b>8. <g b>16 ~ <g b>8 <g b>8 <g b>8. <g b>16 ~ <g b>8 <g b>
   
   \break
   <fis ais>1
   <g b>8. <g b>16 ~ <g b>8 <g b>8 <g b>8. <g b>16 ~ <g b>8 <g b>
   <fis ais>1 ~ 
   <fis ais>1
   
   \break
   \bar "||"
   \mark \markup \boxed "Estribillo"
   
   b1 \f ~ 
   b8 fis b cis d8. e16 ~ e8 fis
   e8. d16 ~ d8 cis8 e2 ~
   e4. e16 e e8. fis16 ~ fis8 e
   
   \break
   d8. cis16 ~ cis8 b8 d2 ~ 
   d4. d16 d d8. e16 ~ e8 d
   cis1
   \teeny
   g'4-> ^\markup \tiny "Violín" d8-. g-> ~ g d-. g4->
   
   \break
   fis4-> cis8-. fis-> ~ fis cis-. fis4->
   g4-> d8-. g-> ~ g d-. g4->
   fis2.-> fis4
   \normalsize
   
   \repeat percent 7 {
     d'8 \bendBefore \mf ~ <d g> ~ <d g>2. 
     cis8 \bendBefore ~ <cis fis> ~ <cis fis>2.
   }
   
   d8 \bendBefore \< ~ <d g> ~ <d g>2.
   fis1 \bendBefore \f
   
   \break
   R1*16
   
   \teeny
   <fis,, ais cis>8. ^\markup \tiny "Piano" <fis ais cis>16 ~ <fis ais cis>8 <fis ais cis>8 
   <fis ais cis>8. <fis ais cis>16 ~ <fis ais cis>8 <fis ais cis>8
   <fis ais cis>8 r8 r8 fis''8 fis, r8 r4
   
   \break
   \bar "||"
   \mark \markup \boxed "estrofa"
   \normalsize
   fis1 \p
   \appoggiatura fis8 g8. fis16 ~ fis8 e8 d8. e16 ~ e8 fis
   cis1
   \appoggiatura fis8 g8. fis16 ~ fis8 e8 d8. e16 ~ e8 fis
   
   \break
   cis1
   \appoggiatura fis8 g8. fis16 ~ fis8 e8 d8. e16 ~ e8 fis
   cis1
   g'4 \f d b g
   
   fis1
   r8. g'16 \mf ~ g8 g8 g8. a16 ~ a8 g
   fis1
   r8. g16 ~ g8 g8 g8. a16 ~ a8 g
   
   fis1 
   r8. g16 \> ~ g8 g8 g8. a16 ~ a8 g
   fis1 \p
   <cis fis>8. \mf ^"Marcato" \< <cis fis>16 ~ <cis fis>8 <cis fis>8 <cis fis>8. <cis fis>16 ~ <cis fis>8 <cis fis>
   
   <cis fis>8-^ \f r8 r4 r2
   
   \bar "|."
   
}



\header {
  instrument = "Guitarra Eléctrica"
}

\paper {  	
  % distancia entre sistemas
  system-system-spacing #'basic-distance = #18
  
  % distancia entre el título y el primer sistema
  markup-system-spacing #'basic-distance = #20
  
  page-count = 3
}

<<
  \new Staff << \relative c'' \eguitar >>
>>