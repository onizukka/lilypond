\version "2.16.1"
% \include "imports\main.ly"

slides =
#(make-music 'CrescendoEvent
             'span-direction START
             'span-type 'text
             'span-text "slides")

% Instrument Music
% -------------------------------------------------
aguitarmidi = {

  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0	
  
  \key b \minor
  
  \partial 4
  r4
  
  r2 r8 e,4. \p ~ 
  e1 ~ 
  e8 \< g8  b' \f \> g f b g \mf fis, \p \< ~ 
  fis cis' b' ais8 \mf ~ ais2  
  
  r8 g, \p f'' \f \> e b g e'4 \mf 
  fis,,8 \p \< cis' fis b \mf ~ b4 ais4\p 
  r8 e' d b g e b fis8 ~
  fis cis' fis ais ~ ais4 fis
  
  g,4 d'8 b' ~ b2
  fis,4 cis'8 ais' ~ ais2
  g,4 d'8 b' ~ b2
  fis,4 cis'8 ais' ~ ais2
  
  b,8. \f d16 ~ d8 fis b8. fis16 ~ fis8 d8
  a8. cis16 ~ cis8 e a2
  b,8. b16 ~ b8 b b8. cis16 ~ cis8 b8
  ais16 b cis8 ~ cis2.
  
  b8. d16 ~ d8 fis b8. fis16 ~ fis8 d8
  a8. cis16 ~ cis8 e a2
  b,8. b16 ~ b8 b b8. cis16 ~ cis8 b8
  ais1
  
  R1*3
  fis4 \p cis'8 ais' ~ ais2
  
  \chordmode {
    fis,8. \< fis,16 ~ fis,8 fis, fis,8. fis,16 ~ fis,8 fis,8
    fis,8 \f r8 r4 r2
  }
  
  fis,4 \p cis'8 ais' ~ ais2
  g,4 d'8 b' ~ b2
  fis,4 cis'8 ais' ~ ais2
  g,4 d'8 b' ~ b2
  
  fis,4 cis'8 ais' ~ ais2
  g,4 d'8 b' ~ b2
  fis,4 cis'8 ais' ~ ais2
  g,4 d'8 b' ~ b2
  
  fis,4 cis'8 ais' ~ ais2
  g,4 d'8 b' ~ b2
  fis,4 cis'8 ais' ~ ais2
  R1
  
  
  
  fis,4 \p cis'8 ais' ~ ais2
  g,4 d'8 b' ~ b2
  fis,4 cis'8 ais' ~ ais2
  g,4 d'8 b' ~ b2
  
  fis,4 cis'8 ais' ~ ais2
  g,4 d'8 b' ~ b2
  fis,4 cis'8 ais' ~ ais2
  g,4 d'8 b' ~ b2
  
  fis,4 cis'8 ais' ~ ais2
  g,4 d'8 b' ~ b2
  fis,4 cis'8 ais' ~ ais2
  R1
  
  \chordmode {
    b,4:m r8 b,8:m ~ b,2:m
    b,4:m r8 b,8:m ~ b,2:m
    a,4 r8 a,8 ~ a,2
    a,4 r8 a,8 ~ a,2
    
    g,4:7 r8 g,8:7 ~ g,2:7
    g,4:7 r8 g,8:7 ~ g,2:7
    fis,4 r8 fis,8 ~ fis,2
    g,4 r8 g,8 ~ g,2
    
    fis,4 r8 fis,8 ~ fis,2
    g,4 r8 g,8 ~ g,2
  }
  fis,4 cis'8 ais' ~ ais2
  fis,8-^ r4 r8 r2
  
  fis4 cis'8 ais' ~ ais2
  g,4 d'8 b' ~ b2
  fis,4 cis'8 ais' ~ ais2
  g,4 d'8 b' ~ b2
  
  fis,4 cis'8 ais' ~ ais2
  g,4 d'8 b' ~ b2
  fis,4 cis'8 ais' ~ ais2
  g,4 d'8 b' ~ b2
  
  fis,4 cis'8 ais' ~ ais2
  g,4 d'8 b' ~ b2
  fis,4 cis'8 ais' ~ ais2
  R1
  
  \chordmode {
    b,4:m r8 b,8:m ~ b,2:m
    b,4:m r8 b,8:m ~ b,2:m
    a,4 r8 a,8 ~ a,2
    a,4 r8 a,8 ~ a,2
    
    g,4:7 r8 g,8:7 ~ g,2:7
    g,4:7 r8 g,8:7 ~ g,2:7
    fis,4 r8 fis,8 ~ fis,2
    g,4 r8 g,8 ~ g,2
    
    fis,4 r8 fis,8 ~ fis,2
    g,4 r8 g,8 ~ g,2
    fis,4 r8 fis,8 ~ fis,2
  }
  
  <b, d>2 ~ <b d>4. d8
  <a cis>2 ~ <a cis>4. cis8
  <g b>1
  
  fis4 cis'8 ais' ~ ais2
  g,4 d'8 b' ~ b2
  fis,4 cis'8 ais' ~ ais2
  g,4 d'8 b' ~ b2
  
  
  fis,4 cis'8 ais' ~ ais2
  g,4 d'8 b' ~ b2
  fis,4 cis'8 ais' ~ ais2
  R1
  
  \chordmode {
    b,4:m r8 b,8:m ~ b,2:m
    b,4:m r8 b,8:m ~ b,2:m
    a,4 r8 a,8 ~ a,2
    a,4 r8 a,8 ~ a,2
    
    g,4:7 r8 g,8:7 ~ g,2:7
    g,4:7 r8 g,8:7 ~ g,2:7
    fis,4 r8 fis,8 ~ fis,2
    g,4 r8 g,8 ~ g,2
    
    fis,4 r8 fis,8 ~ fis,2
    g,4 r8 g,8 ~ g,2
    
    \unfoldRepeats \repeat volta 8 {
      fis,4 r8 fis,8 ~ fis,2
      g,4 r8 g,8 ~ g,2
    }
    
    fis,4 \f fis, fis, fis,
    g,8. g,16 ~ g,8 g, g,8. a,16 ~ a,8 g,8
    fis,4 fis, fis, fis,
    g,8. g,16 ~ g,8 g, g,8. a,16 ~ a,8 g,8
    
    fis,1 \mf 
    R1*3
    
    R1*4
    
    R1*4
    
    fis,8. \p \< fis,16 ~ fis,8 fis, fis,8. fis,16 ~ fis,8 fis,8
    fis,8 \f r8 r4 r2
  }
  
  fis,4 \p cis'8 ais' ~ ais2
  g,4 d'8 b' ~ b2
  fis,4 cis'8 ais' ~ ais2
  g,4 d'8 b' ~ b2
  
  fis,4 cis'8 ais' ~ ais2
  g,4 d'8 b' ~ b2
  fis,4 cis'8 ais' ~ ais2
  g,4 d'8 b' ~ b2
  
  fis,4 cis'8 ais' ~ ais2
  g,4 d'8 b' ~ b2
  fis,4 cis'8 ais' ~ ais2
  g,4 d'8 b' ~ b2
  
  fis,4 cis'8 ais' ~ ais2
  g,4 d'8 b' ~ b2
  fis,4 cis'8 ais' ~ ais2
  \chordmode {
    fis,8. \< fis,16 ~ fis,8 fis, fis,8. fis,16 ~ fis,8 fis,8
    fis,8 \f r8 r4 r2
  }
  
  \bar "|."
}