\version "2.16.1"
\include "imports\main.ly"

slides =
#(make-music 'CrescendoEvent
             'span-direction START
             'span-type 'text
             'span-text "slides")

% Instrument Music
% -------------------------------------------------
vientos = {

  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0	
  
  \key b \minor
  
  R1*20
  R1*17
  R1*12
  R1*12
  
  \break
  \mark \markup \boxed "to Alto Sax"
  \transpose ees c \relative c' {
    \key b \minor
    r8 fis \p \< g a b cis d e
    fis2 \mf r2
    r8. \appoggiatura g8 fis16 \< ~ fis8 g a8. b16 ~ b8 cis
    d8. \f \> cis16 ~ cis8 b a2 \mf
    
    \acciaccatura {b8 a} g4. fis8 e8. d16 ~ d8 cis
    d8. e16 ~ e8 d b8. a16 ~ a8 g
    fis8. fis16 ~ fis8 fis \> g4 a \p
    r2 r4 r8 d
    
    cis \< d e fis g a b cis
    d2 \f \> \acciaccatura {a8 g} fis8. a16 ~ a8 g
    fis1 \mf 
    R1
    
    \break
    \mark \markup \boxed "Estrofa"
    R1*12
    
    \break
    \mark \markup \boxed "Estribillo"
    R1
    r2 r4 r8 fis, \p \<
    g a b cis d8. e16 ~ e8 fis
    g4. fis8 ~ fis2
    
    r8. d'16 \f ~ d8 cis d8. d16 ~ d8 cis
    b2 \acciaccatura {b8 a} g \> fis e d
    cis1 \p
    \acciaccatura {b8 a} g \< a b cis d e fis g
    
    ais b cis d cis2 \f
    r8. cis16 ~ cis8 d8 \appoggiatura d8 cis4. b8
    b4 \> ais8 ais ~ ais4 \acciaccatura {g8 a} g fis
    
    g4 \mf fis2.
    R1*2
  }
  
  R1*8
  
  \break
  \mark \markup \boxed "to Trumpet"
  \transpose bes c \relative c'' {
      \key b \minor
      r8. b'16 ~ b8 cis d8. e16 ~ e8 fis
      e8. d16 ~ d8 cis e8. a,16 ~ a8 a
      a8. b16 ~ b8 a e'8. a,16 ~ a8. a16
      a2 ~ a8. fis16 ~ fis8 a
      
      b cis d cis b a g4 ~ 
      g8. e16 ~ e8 cis cis2 ~ 
      
      cis2 r2
      b'2 ~ b8. a16 ~ a8 g
      b8. fis16 ~ fis4 ~ fis8. fis16 ~ fis8 g
      a g fis e ~ e8. fis16 ~ fis8 g
      
      fis cis ~ cis2 r4
  
      \break
      \mark \markup \boxed "Slides"
      R1*15
      
      \break
      \mark \markup \boxed "Tutti Perdiéndose"
      R1*16
      
      \break
      \mark \markup \boxed "Estrofa"
      R1*7  
      r2 r4 r8 a
      
      b cis d e fis g a b
      \appoggiatura {a16 b a} g2 r
      r4 r8 d' cis8. d16 ~ d8 e 
      cis2. \appoggiatura {a16} g8 fis
        
      g fis e2 r4
      r8 b d g d' e4 d8
      cis a g a ~ a4 \appoggiatura {b16 a} g8 e
      fis8. fis16 ~ fis8 fis fis8. fis16 ~ fis8 fis
      
      fis8 r r4 r2
  }
   \bar "|."
   
}



\header {
  instrument = "Vientos"
 }

\paper {  	
  % distancia entre sistemas
  system-system-spacing #'basic-distance = #18
  
  % distancia entre el título y el primer sistema
  markup-system-spacing #'basic-distance = #20
}

<<
  \new Staff << \relative c'' \vientos >>
>>