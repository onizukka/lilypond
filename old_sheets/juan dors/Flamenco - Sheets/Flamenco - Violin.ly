\version "2.16.1"
\include "imports\main.ly"

slides =
#(make-music 'CrescendoEvent
             'span-direction START
             'span-type 'text
             'span-text "slides")

% Instrument Music
% -------------------------------------------------
violin = {

  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0	
  
  \key b \minor
  
  R1*12
  
  \teeny
  b'4 ^\markup \tiny "Guitarra Eléctrica" a g fis
  a g fis e
  g fis e d 
  fis cis2.
  % R1*4
  
  \break
  b''4 a g fis
  a g fis e
  g fis e d
  cis1
  
  r8. b16 ~ b8 b8 b8. cis16 ~ cis8 d
  cis1
  r8. b16 ~ b8 b8 b8. cis16 ~ cis8 d
  cis1
  
  cis8. cis16 ~ cis8 cis8 cis8. cis16 ~ cis8 cis
  cis8 r8 r4 r2
  
  %{ R1*3
  R1*4
  R1*3 %}
  
  \break
  \bar "||"
  \mark \markup \boxed "Estrofa"
  
  R1*11
  r4 r8 ^\markup \tiny "Piano" fis'-. fis,-. r r4
  
  \break
  \bar "||"
  \mark \markup \boxed "Estrofa"
  
  \normalsize
  r4. \mf cis8( fis8. eis16 ~ eis8 fis
  g2 d8. e16 ~ e8 fis16 g
  fis8 \mordent cis4) cis8( fis8. eis16 ~ eis8 fis
  g2 d8. e16 ~ e8 fis16 g
  
  fis2.) \mordent e4(
  d \> cis b cis
  fis,2.) \! fis'4( \mf
  eis \> d cis b
  
  cis1 \!)
  R1*3
  %{
  \repeat volta 2 {
    R1*4
  }
  \alternative {
    {R1*8}
    {R1*8}
  }%}
  
  \break
  \bar "||"
  \mark \markup \boxed "Estribillo"
  
  \teeny
  R1*11
  r4 r8 ^\markup \tiny "Piano" fis'-. fis,-. r r4
  
  \break 
  \bar "||"
  \mark \markup \boxed "Estrofa"
  
  \normalsize
  r4 cis,( \p fis ais 
  b ais g d
  fis2.) ais4(
  b ais g d
  
  \break
  fis2.) g4(
  ais b cis b
  fis1)
  g4( b g fis
  
  cis1)
  d4( fis g b
  fis1)
  R1
  
  \break 
  \bar "||"
  \mark \markup \boxed "Estribillo"
  
  R1*11
  
  \bar "||"
  \mark \markup \boxed "Puente"
  \teeny
  <b d>2. ^\markup \tiny "Guitarra Eléctrica" ~ <b d>8 d
  <a cis>2. ~ <a cis>8 cis
  <g b>1
  
  <fis ais>
  
  \normalsize
  r2 r8. fis16 ~ fis8 g
  \break
  ais1
  g2 ~ g8. ais16 ~ ais8 g
  
  fis1
  g2 ~ g8. ais16 ~ ais8 g
  fis1
  R1
  
  \break
  \bar "||"
  \mark \markup \boxed "Estribillo"
  R1*6
  
  fis'4-> ^"Enérgico" cis8-. fis-> ~ fis cis-. fis4->
  g4-> d8-. g-> ~ g d-. g4->
  fis4-> cis8-. fis-> ~ fis cis-. fis4->
  g4-> d8-. g-> ~ g d-. g4->
  
  % R1*4
  %{fis1
  b8. ais16 ~ ais8 g d4 e
  fis1%
  r1%}
  
  \break
  fis2.-> fis4( ^"Cantabile" ^\markup \tiny "(entran slides de guitarra)"
  g8. fis16 ~ fis8 e d8. cis16 ~ cis8 b
  ais8. fis16 ~ fis2) fis'4
  g8. fis16 ~ fis8 e d8. e16 ~ e8 fis
  
  \break
  cis2. ~ cis8 cis(
  fis4. eis8 g4. cis,8
  fis8 cis8 ~ cis2 ~ cis8) cis8(
  g'4. fis8 eis8. cis16 ~ cis8 g'
  
  fis2. ~ fis8) fis(
  g4. fis,8 a'4. g8
  fis4. e8 cis4.) e'8(
  d,4. cis8 b4. ais8
  
  cis2. ~ cis8) fis,(
  \appoggiatura {d'16} g,8. a16 ~ a8 \appoggiatura {d} b ~ b4. ais8
  cis8 fis,8 ~ fis2 ~ fis8) \appoggiatura {cis'16} fis,8(
  g8. a16 ~ a8 b ~ b4. ais8
  % R1*16
  %{r2 r4 fis
  g8. fis16 ~ fis8 e d8. cis16 ~ cis8 b
  ais2 ~ ais4 fis'
  g8. fis16 ~ fis8 e d8. cis16 ~ cis8 b
  
  ais2 ~ ais4 fis'
  g2 fis8. b16 ~ b8 ais
  fis2. fis4
  g2 fis8. b16 ~ b8 ais
  
  fis1
  g2 b
  fis1 ~  
  fis8. g16 ~ g8 fis e8. d16 ~ d8 b
  
  cis1 ~ 
  cis8. g'16 ~ g8 fis e8. d16 ~ d8 b
  cis1 ~ 
  cis1%}
  
  fis2.) fis'4 \f ^"Marcato"
  eis fis g fis
  eis d eis d
  cis d cis b
  
  cis2. fis4( \mf ^"Cantabile"
  g fis eis d
  cis d eis d
  cis \> b ais g
  
  fis1) \p
  R1*3
  
  R1*4
  
  R1*1
  \teeny
  r4 r8 ^\markup \tiny "Piano" fis''-. fis,-. r r4
  
  
  \break
  \bar "||"
  \mark \markup \boxed "Estrofa"
  \normalsize
  r2 r4 fis( \p
  g2. fis4
  fis2.) fis4(
  g2. b4
  
  fis2 ~ fis4.) fis8(
  g4 ais b cis
  fis1)
  R1
  
  R1*6
  
  R1
  fis,8.^"Marcato" \mf \< fis16 ~ fis8 fis fis8. fis16 ~ fis8 fis
  fis8-> \sf r r4 r2
   
  \bar "|."
   
}

% Instrument Sheet
% -------------------------------------------------

\header {
  instrument = "Violin"
}

\paper {  	
  % distancia entre sistemas
  system-system-spacing #'basic-distance = #18
  
  % distancia entre el título y el primer sistema
  markup-system-spacing #'basic-distance = #20
  
  systems-per-page = 8
}

<<
  \new Staff << \relative c' \violin >>
>>