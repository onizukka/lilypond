\version "2.16.1"
% \include "imports\main.ly"

slides =
#(make-music 'CrescendoEvent
             'span-direction START
             'span-type 'text
             'span-text "slides")

% Instrument Music
% -------------------------------------------------
palmasmidi = {

  \unfoldRepeats \drummode {
    \jazzOn
    \compressFullBarRests
    
    % Arregla el problema de tener el scoreMarkup añadido
    \revert MultiMeasureRest #'staff-position
    \override Voice.Rest #'staff-position = #0	
    
    \partial 4
    hc8 hc
    
    \repeat volta 12 {
      hc4 \p hc \f r4 hc8 hc \mf 
    }
    
    \repeat volta 7 {
      hc4 \p hc \f r4 hc8 hc \mf 
    }
    
    \repeat volta 6 {
      hc4 \p hc \f r4 hc8 hc \mf 
    }
    
    hc8 r r4 r2
    
    \bar "|."
  }
}