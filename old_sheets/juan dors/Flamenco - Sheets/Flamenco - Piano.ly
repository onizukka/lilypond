\version "2.16.1"
% \include "imports\main.ly"

slides =
#(make-music 'CrescendoEvent
             'span-direction START
             'span-type 'text
             'span-text "slides")

% Instrument Music
% -------------------------------------------------
pianomidi = {

  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0	
  
  \key b \minor
  
  \partial 4
  r4
  
  R1*12
  
  R1*4
  
  R1*3
  
  <fis fis'>16 \p r8. <ais ais'>16 r8. <cis cis'>16 r8. <fis fis'>16 r8.
  <g g'>16 r8. <d d'>16 r8. <b b'>16 r8. <g g'>16 r8.
  <fis fis'>16 r8. <ais ais'>16 r8. <cis cis'>16 r8. <fis fis'>16 r8.
  <g g'>16 r8. <d d'>16 r8. <b b'>16 r8. <g g'>16 r8.
  
  r4 <fis ais cis>16 r8. r8 <fis ais cis> r4
  <fis ais cis>8. \< <fis ais cis>16 ~ <fis ais cis>8 <fis ais cis>8 
  <fis ais cis>8. <fis ais cis>16 ~ <fis ais cis>8 <fis ais cis>8
  <fis ais cis>8 \f r8 r8 fis''16 r fis, r r8 r4
  
  
  
  r4 <fis, ais cis>16 \p r8. r8 <fis ais cis> r4
  r4 <g b d>16 r8. r8 <g b d> r4
  r4 <fis ais cis>16 r8. r8 <fis ais cis> r4
  r4 <g b d>16 r8. r8 <g b d> r4
  
  r4 <fis ais cis>16 r8. r8 <fis ais cis> r4
  r4 <g b d>16 r8. r8 <g b d> r4
  r4 <fis ais cis>16 r8. r8 <fis ais cis> r4
  r4 <g b d>16 r8. r8 <g b d> r4
  
  r4 <fis ais cis>16 r8. r8 <fis ais cis> r4
  r4 <g b d>16 r8. r8 <g b d> r4
  r4 <fis ais cis>16 r8. r8 <fis ais cis> r4
  <fis ais cis>8 \f r8 r8 fis''16 r fis, r r8 r4
  
  
  
  r4 <fis, ais cis>16 \p r8. r8 <fis ais cis> r4
  r4 <g b d>16 r8. r8 <g b d> r4
  r4 <fis ais cis>16 r8. r8 <fis ais cis> r4
  r4 <g b d>16 r8. r8 <g b d> r4
  
  r4 <fis ais cis>16 r8. r8 <fis ais cis> r4
  r4 <g b d>16 r8. r8 <g b d> r4
  <fis fis'>4 \mf <ais ais'>4 <cis cis'>4 <fis fis'>4
  <g g'>4 <d d'>4 <b b'>4 <g g'>4
  
  <fis fis'>4 <fis ais cis>16 \p r8. r8 <fis ais cis> r4
  r4 <g b d>16 r8. r8 <g b d> r4
  r4 <fis ais cis>16 r8. r8 <fis ais cis> r4
  <fis ais cis>8 r8 r4 r2
  
  
  r4 <b d fis>16 r8. r8 <b d fis> r4
  r4 <b d fis>16 r8. r8 <b d fis> r4
  r4 <a cis e>16 r8. r8 <a cis e> r4
  r4 <a cis e>16 r8. r8 <a cis e> r4
  
  r4 <g b d>16 r8. r8 <g b d> r4
  r4 <g b d>16 r8. r8 <g b d> r4
  <fis fis'>4 \mf <ais ais'>4 <cis cis'>4 <fis fis'>4
  <g g'>4 <d d'>4 <b b'>4 <g g'>4
  
  <fis fis'>4 <fis ais cis>16 \p r8. r8 <fis ais cis> r4
  r4 <g b d>16 r8. r8 <g b d> r4
  r4 <fis ais cis>16 r8. r8 <fis ais cis> r4
  <fis ais cis>8 \f r8 r8 fis''16 r fis, r r8 r4
  
  r4 <fis, ais cis>16 \p r8. r8 <fis ais cis> r4
  r4 <g b d>16 r8. r8 <g b d> r4
  r4 <fis ais cis>16 r8. r8 <fis ais cis> r4
  r4 <g b d>16 r8. r8 <g b d> r4
  
  r4 <fis ais cis>16 r8. r8 <fis ais cis> r4
  r4 <g b d>16 r8. r8 <g b d> r4
  <fis fis'>4 \mf <ais ais'>4 <cis cis'>4 <fis fis'>4
  <g g'>4 <d d'>4 <b b'>4 <g g'>4
  
  <fis fis'>4 <fis ais cis>16 \p r8. r8 <fis ais cis> r4
  r4 <g b d>16 r8. r8 <g b d> r4
  r4 <fis ais cis>16 r8. r8 <fis ais cis> r4
  <fis ais cis>8 r8 r4 r2
  
  
  r4 <b d fis>16 r8. r8 <b d fis> r4
  r4 <b d fis>16 r8. r8 <b d fis> r4
  r4 <a cis e>16 r8. r8 <a cis e> r4
  r4 <a cis e>16 r8. r8 <a cis e> r4
  
  r4 <g b d>16 r8. r8 <g b d> r4
  r4 <g b d>16 r8. r8 <g b d> r4
  r4 <fis ais cis>16 r8. r8 <fis ais cis> r4
  r4 <g b d>16 r8. r8 <g b d> r4
  
  r4 <fis ais cis>16 r8. r8 <fis ais cis> r4
  r4 <g b d>16 r8. r8 <g b d> r4
  r4 <fis ais cis>16 r8. r8 <fis ais cis> r4
  
  <b' d>2 ~ <b d>4. d8
  <a cis>2 ~ <a cis>4. cis8
  <g b>1
    
  r4 <fis, ais cis>16 r8. r8 <fis ais cis> r4
  r4 <g b d>16 r8. r8 <g b d> r4
  r4 <fis ais cis>16 r8. r8 <fis ais cis> r4
  r4 <g b d>16 r8. r8 <g b d> r4
  
  r4 <fis ais cis>16 r8. r8 <fis ais cis> r4
  r4 <g b d>16 r8. r8 <g b d> r4
  r4 <fis ais cis>16 r8. r8 <fis ais cis> r4
  <fis ais cis>8 r8 r4 r2
  
  r4 <b d fis>16 r8. r8 <b d fis> r4
  r4 <b d fis>16 r8. r8 <b d fis> r4
  r4 <a cis e>16 r8. r8 <a cis e> r4
  r4 <a cis e>16 r8. r8 <a cis e> r4
  
  r4 <g b d>16 r8. r8 <g b d> r4
  r4 <g b d>16 r8. r8 <g b d> r4
  r4 <fis ais cis>16 r8. r8 <fis ais cis> r4
  r4 <g b d>16 r8. r8 <g b d> r4
  
  r4 <fis ais cis>16 r8. r8 <fis ais cis> r4
  r4 <g b d>16 r8. r8 <g b d> r4
  
  \unfoldRepeats \repeat volta 8 {  
    r4 <fis ais cis>16 r8. r8 <fis ais cis> r4
    r4 <g b d>16 r8. r8 <g b d> r4
  }
  
  <fis ais cis>4 \f <fis ais cis>4 <fis ais cis>4 <fis ais cis>4
  <g b d>8. <g b d>16 ~ <g b d>8 <g b d>8 
  <g b d>8. <a cis e>16 ~ <a cis e>8 <g b d>8
  <fis ais cis>4 <fis ais cis>4 <fis ais cis>4 <fis ais cis>4
  <g b d>8. <g b d>16 ~ <g b d>8 <g b d>8 
  <g b d>8. <a cis e>16 ~ <a cis e>8 <g b d>8
  
  <fis ais cis>2 \mf <fis ais cis>2
  <g b d>2 <g b d>8. <a cis e>16 ~ <a cis e>8 <g b d>8
  <fis ais cis>2 <fis ais cis>2
  <g b d>2 <g b d>8. <a cis e>16 ~ <a cis e>8 <g b d>8
  
  <fis ais cis>1 \p
  <g b d>
  <fis ais cis>1
  <g b>
  
  <fis ais>1
  R1*3
  
  <fis ais cis>8. \< <fis ais cis>16 ~ <fis ais cis>8 <fis ais cis>8 
  <fis ais cis>8. <fis ais cis>16 ~ <fis ais cis>8 <fis ais cis>8
  <fis ais cis>8 \f r8 r8 fis''16 r fis, r r8 r4
  
  r4 <fis, ais cis>16 \p r8. r8 <fis ais cis> r4
  r4 <g b d>16 r8. r8 <g b d> r4
  r4 <fis ais cis>16 r8. r8 <fis ais cis> r4
  r4 <g b d>16 r8. r8 <g b d> r4
  
  r4 <fis ais cis>16 r8. r8 <fis ais cis> r4
  r4 <g b d>16 r8. r8 <g b d> r4
  <fis fis'>4 \mf <ais ais'>4 <cis cis'>4 <fis fis'>4
  <g g'>4 <d d'>4 <b b'>4 <g g'>4
  
  r4 <fis ais cis>16 r8. r8 <fis ais cis> r4
  r4 <g b d>16 r8. r8 <g b d> r4
  r4 <fis ais cis>16 \p r8. r8 <fis ais cis> r4
  r4 <g b d>16 r8. r8 <g b d> r4
  
  r4 <fis ais cis>16 r8. r8 <fis ais cis> r4
  r4 <g b d>16 r8. r8 <g b d> r4
  r4 <fis ais cis>16 r8. r8 <fis ais cis> r4
  <fis ais cis>8. \< <fis ais cis>16 ~ <fis ais cis>8 <fis ais cis>8 
  <fis ais cis>8. <fis ais cis>16 ~ <fis ais cis>8 <fis ais cis>8
  
  <fis ais cis>8 \f r8 r4 r2
  
  
  \bar "|."
}