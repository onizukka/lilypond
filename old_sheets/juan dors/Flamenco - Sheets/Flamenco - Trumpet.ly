\version "2.16.1"
% \include "imports\main.ly"

slides =
#(make-music 'CrescendoEvent
             'span-direction START
             'span-type 'text
             'span-text "slides")

% Instrument Music
% -------------------------------------------------
trumpetmidi = {

  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0	
  
  \key b \minor
  
  \partial 4
  r4
  
  R1*12
  
  R1*4
  
  R1*3
  
  R1*4
  R1*3
  
  R1*12
  R1*12
  
  R1*12
  
  R1*12
  
  R1*11
  
  R1*3
  
  R1*8
  
  r8. b'16 ~ b8 cis d8. e16 ~ e8 fis
  e8. d16 ~ d8 cis e8. a,16 ~ a8 a
  a8. b16 ~ b8 a e'8. a,16 ~ a8. a16
  a2 ~ a8. fis16 ~ fis8 a
  
  b cis d cis b a g4 ~ 
  g8. e16 ~ e8 cis cis2 ~ 
  cis2 r2
  b''2 ~ b8. a16 ~ a8 g
  b8. fis16 ~ fis4 ~ fis8. fis16 ~ fis8 g
  
  a g fis e ~ e8. fis16 ~ fis8 g
  fis cis ~ cis2 r4
  
  %{
  R1*8
  R1*2 %}
  
  R1
  \unfoldRepeats \repeat volta 7 {
    R1*2
  }
  
  R1*12
  
  R1*4
  
  R1*2
  
  R1*7
  r2 r4 r8 a,
  
  b cis d e fis g a b
  \appoggiatura {a16 b a} g2 r
  r4 r8 d' cis8. d16 ~ d8 e 
  cis2. \appoggiatura {a16} g8 fis
    
  g fis e2 r4
  r8 b d g d' e4 d8
  cis a g a ~ a4 \appoggiatura {b16 a} g8 e
  
  fis8. fis16 ~ fis8 fis fis8. fis16 ~ fis8 fis
  fis8 r r4 r2
   
   \bar "|."
   
}