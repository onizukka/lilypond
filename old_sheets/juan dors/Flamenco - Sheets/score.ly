\version "2.16.1"

\include "imports/main.ly"

\include "Flamenco - Voz.ly"
\include "Flamenco - Alto Sax.ly"
\include "Flamenco - Violin.ly"
\include "Flamenco - Trumpet.ly"
\include "Flamenco - E-Guitar.ly"
\include "Flamenco - A-Guitar.ly"
\include "Flamenco - Piano.ly"
\include "Flamenco - Bajo.ly"
\include "Flamenco - Palmas.ly"

\score {
  <<
    \new StaffGroup <<
      \scoreMarkup
      \new Staff { \relative c'' \vozmidi }
      \new Staff { \relative c' \altosaxmidi }
      \new Staff { \relative c' \violinmidi }
      \new Staff { \relative c' \trumpetmidi }
      \new Staff { \relative c'' \eguitarmidi }
      \new Staff { \relative c' \aguitarmidi }
      \new Staff { \relative c' \pianomidi }
      \new Staff { \relative c \bassmidi }
      \new DrumStaff { \palmasmidi }
    >>
  >>
  
  \layout {}
}

\score {
  <<
    \new Staff { 
      \set Staff.midiInstrument = #"lead 6 (voice)"
      \relative c' \unfoldRepeats \vozmidi
    }
    \new Staff { 
      \set Staff.midiInstrument = #"alto sax"
      \relative c \unfoldRepeats \altosaxmidi
    }
    \new Staff { 
      \set Staff.midiInstrument = #"violin"
      \relative c' \unfoldRepeats \violinmidi
    }
    \new Staff { 
      \set Staff.midiInstrument = #"trumpet"
      \relative c' \unfoldRepeats \trumpetmidi
    }
    \new Staff { 
      \set Staff.midiInstrument = #"distorted guitar"
      \relative c' \unfoldRepeats \eguitarmidi
    }
    \new Staff { 
      \set Staff.midiInstrument = #"acoustic guitar (nylon)"
      \relative c \unfoldRepeats \aguitarmidi
    }
    \new Staff { 
      \set Staff.midiInstrument = #"acoustic grand"
      \relative c' \unfoldRepeats \pianomidi
    }
    \new Staff { 
      \set Staff.midiInstrument = #"electric bass (finger)"
      \relative c \unfoldRepeats \bassmidi
    }
    \new DrumStaff { 
      \palmasmidi
    }
  >>
  
  \midi {
    \context {
      \Score
      tempoWholesPerMinute = #(ly:make-moment 144 4)
    }
  }
}


% Instrument Sheet
% -------------------------------------------------

%{
\header {
  instrument = "E-Guitar"
}

\paper {  	
  % distancia entre sistemas
  system-system-spacing #'basic-distance = #18
  
  % distancia entre el título y el primer sistema
  markup-system-spacing #'basic-distance = #20
}

<<
  \scoreMarkup
  \new Staff << \relative c'' \eguitar >>
>>
%}