aGuitarChords = \chords {
  s1*8
  
  g fis g fis
  
  s1*8
  
  fis1 g fis g
  fis fis s
  
  fis g fis g
  fis g fis g
  fis g fis s
  
  fis g fis g
  fis g fis g
  fis g fis fis
  
  b b a a 
  g:7 g:7 fis g
  fis g fis s
  
  fis g fis g
  fis g fis g
  fis g fis fis
  
  b b a a 
  g:7 g:7 fis g
  fis g fis
  
  b a g
  
  fis g fis g
  fis g fis fis
  
  b b a a 
  g:7 g:7 fis g
  fis g fis g
  
  fis g fis g
  fis g fis g
  fis g fis g
  
  fis g fis g
  fis g fis g
  fis g fis g
  fis g fis g
  
  fis s
  
  fis g fis g
  fis g fis g
  fis g fis g
  fis g fis fis
  s
}