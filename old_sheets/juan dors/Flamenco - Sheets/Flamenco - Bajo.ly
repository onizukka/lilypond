\version "2.16.1"
% \include "imports\main.ly"

slides =
#(make-music 'CrescendoEvent
             'span-direction START
             'span-type 'text
             'span-text "slides")

% Instrument Music
% -------------------------------------------------
bassmidi = {

  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0	
  
  \key b \minor
  \clef bass
  
  \partial 4
  r4
  
  R1*12
  
  b1 \f a g fis
  b a g 
  
  fis g fis g
  
  fis
  fis8.  fis16 ~ fis8 fis fis8. fis16 ~ fis8 fis
  fis8-^ r8 r4 r2
  
  
  fis1  g fis g
  fis g fis g 
  fis g fis fis8 r r4 r2
  
  
  fis1  g fis g
  fis1 g fis g
  fis g fis fis
  
  b b a a
  g g fis g
  fis g fis fis8 r r4 r2
  
  fis1 g fis g
  fis1 g fis g
  fis1 g fis fis
  
  b b a a
  g g fis g
  fis g fis
  b a g
  
  fis g fis g
  fis g fis fis
  
  b b a a
  g g fis g
  fis g
  
  \unfoldRepeats \repeat volta 8 {
    fis g 
  }
  
  fis4 \f fis fis fis
  g8. g16 ~ g8 g g8. a16 ~ a8 g
  fis4 fis fis fis
  g8. g16 ~ g8 g g8. a16 ~ a8 g
  
  fis2 \mf fis2
  g2 g8. a16 ~ a8 g
  fis2 fis2
  g2 g8. a16 ~ a8 g
  
  fis1 \p g fis g
  
  fis1 g fis g
  fis8.  fis16 ~ fis8 fis fis8. fis16 ~ fis8 fis
  fis8-^ r8 r4 r2
  
  fis1 \f g fis g
  fis g fis g 
  fis g fis g 
  fis g fis fis8.  fis16 ~ fis8 fis fis8. fis16 ~ fis8 fis
  
  fis8-^ r8 r4 r2
  
  \bar "|."
}