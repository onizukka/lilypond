﻿smaller = {
   
}

intro = \relative c''
{  
   r1
   
   \set fontSize = #-3
   b4 a g fis 
   a g fis e 
   g fis e d
   \set fontSize = #0  
   
   fis4 ais cis fis
   g d b g
   fis ais cis fis
   g d b g
   
   r1 r1 r2 r8 fis''8 fis,4
   
}

verseSA = \relative c'''
{
   r1 r1 r1 r1
   r1 r1 r1 r1
   r1 r1 r1 r4 r8 fis8 fis,4 r4
}

verseSB = \relative c
{
   r1 r1 r1 r1
   r1 r1 r1 r1
   r1 r1 r1 r1
}

verseSC = \relative c
{
   r1 r1 r1 r1
   r1 r1 r1 r1
   r1 r1 r1 
   r1 r1 r1 r1
   r1 r1 r1 r1
   r1 r1 r1
}

verseSD = \relative c'
{
   r1 r1 r1 r1
   r1 r1 r1 r1
   r1 r1 r1 r1
   r1 r1 r1 r1
   r1 r1 r1 r1
   r1 r1 r1 r1
   r1 r1 r1 r1
   \times 2/3 {fis4 fis fis} \times 2/3 {fis fis fis}
   fis r4 r2
}

verseSE = \relative c'
{
   r1 r1 r1 r1
   r1 r1 r1 r1
   r1 r1 r1 r1
   r1 r1 r1
   
   \times 2/3 {fis4 fis fis} \times 2/3 {fis fis fis}
   fis r4 r2
}

introH = \chordmode
{
   r1 
   r1 r1 r1
   r1 r1 r1 r1
   fis1 fis1 r1
}

verseAAH = \chordmode
{
   fis1 g1 fis1 g1
   fis1 g1 fis1 g1
   fis1 g1 fis1 fis1
}

verseABH = \chordmode
{
   fis1 g1 fis1 g1
   fis1 g1 fis1 g1
   fis1 g1 fis1 g1
   fis1 g1 fis1 fis1 fis1
}

verseBAH = \chordmode
{
   b1:m b1:m a1 a1 
   g1 g1 fis1 g1 
   fis g1 fis1 fis1
}

verseBBH = \chordmode
{
   b1:m b1:m a1 a1 
   g1 g1 fis1 g1 
   fis g1 fis1
   b1:m a1 g1 fis
   g fis g fis 
   g fis fis
}

verseBCH = \chordmode
{
   b1:m b1:m a1 a1 
   g1 g1 fis1 g1 
   fis1 g1 fis1 g1
   fis1 g1 fis1 g1
   fis1 g1 fis1 g1
   fis1 g1 fis1 g1
   fis1 g1 fis1 fis1 
   fis1 fis1
}

piano = \new StaffGroup
<<
	\new ChordNames 
   {
      \introH
      
      \verseAAH
      \verseAAH
      \verseBAH
      \verseAAH
      \verseBBH
      \verseBCH
      \verseABH
	}
	\new Staff
	{
		\key b \minor
      
      \intro
      
      \mark \markup {A}
      \verseSA
      \mark \markup {B}
      \verseSB
      \mark \markup {C}
      \verseSA
      \mark \markup {D}
      \verseSB
      \mark \markup {E}
      \verseSC
      \mark \markup {F}
      \verseSD
      \mark \markup {G}
      \verseSE
	}
>> 