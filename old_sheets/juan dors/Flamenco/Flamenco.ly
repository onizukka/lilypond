\version "2.12.3"

\include "Flamenco - Piano.ly"

\header {
	title = "Flamenco"
	subtitle = "Tiempos"
	composer = "Juan d'Ors"
	instrument = "Piano"
}

\score
{
	\piano
}