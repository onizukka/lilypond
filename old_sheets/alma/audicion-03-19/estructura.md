| Canción              | Instrumento   | Tonalidad | Estilo       | Tempo | Repeticiones | Problenmas                | Grabado |
|----------------------|---------------|-----------|--------------|-------|--------------|---------------------------|---------|
| El barquito          | Piano         | F         | Free         | 120   | Ax2, Bx2     |                           | xx      |
| Rigaudon             | Piano         | F         | Popular      | 120   | no           | Rit. del 9 a las redondas | xx      |
| Bransle de la torche | Piano         | D-/D      | Free         | 110   | no           | bar 19 tal y como es      | xx      |
| Allegretto de Mozart | Piano         | Bb        | Free         | 60    | no           |                           | xx      |
| À la clarie fontaine | Piano         | F         | Soft         | 120   | no           | bar 5                     | xx      |
| Berceuse             | Piano         | D-        | Soft         | 75    | no           |                           | xx      |
| Coucou dans les bois | Piano         | C         | Soft         | 80    | no           |                           | xx      |
| Cuckoo               | Coro          | Ab        | 6/8          | 60    | written      | Last staff                | xx      |
| El patio de mi casa  | Coro          | C         | Rap          | 120   |              |                           | xx      |
| Blues on the attic   | Piano         | D-        | Funk         | 110   | no           |                           | xx      |
| Swing                | Piano         | A-        | Swing        | 100   | no           |                           | xx      |
| Frozen Day           | Guitarra/Saxo | D/A?      | Balada Rock  | 75-80 | written      |                           | xx      |
| Michelle             | Guitarra      | A-        | Shuffle      | 120   | written      |                           | xx      |
| Game                 | Guitarra      | C         | Rock/Shuffle | 90    | written      |                           | xx      |
| Hold Back            | Guitarra      | D-        | Rock         | 130   | written      |                           | xx      |
| Tears                | Guitarra      | C         | Balada Rock  | 80    | written      |                           | xx      |
| Memory               | Vocal         | ?         |              |       |              |                           | xx      |
