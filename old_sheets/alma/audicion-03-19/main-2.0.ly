\version "2.18.2"
\language "english"

% Se debe incluir antes que lilyjazz. Si no, lilyjazz no funciona.
#(set-global-staff-size 19)

\include "main.ily"

\header {
  title = "Audición Alumnos Alma"
  instrument = ""
  composer = ""
  arranger = "gbspsm@gmail.com"
  poet = ""
  tagline = ""
}

\paper {
  indent = 0
}

global = {
  \compressFullBarRests

  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0

  \set Timing.beamExceptions = #'()
  \set Timing.baseMoment = #(ly:make-moment 1/4)
  \set Timing.beatStructure = #'(1 1 1 1)

  \override ParenthesesItem.font-size = #0
}

scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
  % Oculta el primer pentagrama del markup
  \override Staff.VerticalAxisGroup #'remove-first = ##t

  \mkup "El barquito"
  \once \override Score.RehearsalMark.extra-offset = #'(0 . 3)
  \mark \jazzTempoMarkup "Merengue" c4 "120"
  s1*2
  s1*5
  s1*10

  \mkup "Rigaudon"
  \mark \jazzTempoMarkup "Country" c4 "120"
  s1*2
  s1*14
  s1*8

  \mkup "Bransle de la torche"
  \mark \jazzTempoMarkup "Bolero" c4 "110"
  s1*2
  s1*19

  \mkup "Allegretto"
  \mark \jazzTempoMarkup "Merengue" c4 "120"
  s1*2
  s1*30

  \mkup "À la claire fontaine"
  \mark \jazzTempoMarkup "Slow swing" c4 "120"
  s1*2
  s1*12

  \mkup "Berceuse"
  \mark \jazzTempoMarkup "Free, Slow swing" c4 "75"
  s1*2
  s1*24

  \mkup "Coucou dans les bois"
  \mark \jazzTempoMarkup "Slow swing" c4 "80"
  s1*2
  s1*20

  \mkup "Cuckoo"
  \mark \jazzTempoMarkup "Slow swing" c4. "60"
  s1*5*6/8
  s1*23*6/8

  \mkup "El patio de mi casa"
  \mark \jazzTempoMarkup "Funk-Rap" c4 "120"
  s1*39

  \mkup "Blues on the attic"
  \mark \jazzTempoMarkup "Funk" c4 "110"
  s1*4
  s1*28

  \mkup "Swing"
  \mark \jazzTempoMarkup "Swing" c4 "100"
  s1*53

  \mkup "Frozen day"
  \mark \jazzTempoMarkup "Rock Balad" c4 "75"
  s1*4
  s1*35

  \mkup "Michelle"
  \mark \jazzTempoMarkup "Shuffle" c4 "105"
  s1*2
  s1*65

  \mkup "Game"
  \mark \jazzTempoMarkup "Shuffle" c4 "90"
  s1*2
  s1*30

  \mkup "Hold back"
  \mark \jazzTempoMarkup "Rock" c4 "130"
  s1*2
  s1*34

  \mkup "Tears"
  \mark \jazzTempoMarkup "Rock Balad" c4 "80"
  s1*6
  s1*22

  \mkup "Memory"
  \mark \jazzTempoMarkup "Freely" c4. "50"
}

scoreChords = \chords {

  % El barquito chiquitito
  f1
  s1

  f1/a
  d1:m
  c2:7sus2 c:7
  f1
  f1

  bf1
  s1
  f1/a
  s1

  g1:m
  f2/c c
  f1
  s1
  f1
  s1

  % Rigaudon
  f1
  s1

  f1
  s1
  f1/c
  f1

  f1
  s1
  g:7
  c

  s1
  s1
  s1

  c1:9
  c1:9-
  c1:7

  f1
  s1
  f1/c
  f1

  d2:m7 df:5-7
  f1/c
  f2/c c:7
  f1

  % Bransle de la torche
  d1:m
  s1

  d1:m
  c1
  d1:m
  a

  d1:m
  c1
  d2:m a
  d1

  f1
  c1/e
  d1:m
  a

  f1
  c1/e
  d2:m a
  d1
  d2:m a
  d1
  s1

  % Allegretto
  d2 ef:6
  f1:7

  bf2 f:7/c
  bf2/d g:7
  ef2 f:7
  g2:m bf/d
  c2:m f:7
  bf1

  f2/a c:7/g
  f2 f/a
  g2:m/bf c:7
  d2:m f/a
  g2:m/bf c:7
  f1

  d1:m7
  g1:7
  c2:m g:7/b
  c1:m
  fs:7-5
  f2:7 f:7/ef
  bf2/d f/a
  bf2 f:7

  bf2 f:7/c
  bf2/d g:7
  ef2 f:7
  g2:m bf/d
  c2:m f:7
  bf2 bf/d
  c2:m/ef f:7
  g2:m bf/d
  c2:m/ef f:7
  bf1

  % À la claire fontaine
  bf1:7
  c:7

  f1
  bf2:maj7 c:7
  f1/a
  bf2:maj7 c:7

  a1:7/cs
  d2:m a:m
  g1:m7/c
  f2/c c:7

  f1/a
  g2:m7 f2
  g2:9/b f/c
  bf2:maj7/c f

  % Berceuse
  c1:7
  a:7/cs

  d1:m
  s1*3

  d1:m
  s1*3

  d1:m
  d2:m c
  d1:m
  g1
  d1:m
  g2:m a:m7

  d1:m
  s1*3

  d1:m
  s1
  d1:m
  s1*2
  d1:m

  % Coucou dans les bois
  c1
  c1
  r1*4

  c2 g
  c2/g g
  c1/g
  g2 c

  c2 g:7
  c2/g g:7
  c1/g
  g2 c

  r1*4

  r1*2
  c1
  c1

  % Cuckoo
  af2./c
  ef2./af
  af2.
  s1*6/8
  af4. ef:7

  af2.:6
  s1*4*6/8
  af2.:6
  s1*6/8
  af2.:6
  s1*3*6/8
  af2.:6
  s1*2*6/8
  af2.:6
  s1*3*6/8
  af4. ef:7

  af2.:6
  s1*2*6/8
  af2.:6

  % El patio de mi casa
  c1:5.9
  s1
  c1:5.9
  s1*3
  d1:5.9
  e1:5.9
  s1*3
  fs1:5.9
  gs1:5.9
  s1*4
  a1:5.9
  b1:5.9

  c1:5.9
  s1
  c1:5.9
  s1*3
  d1:5.9
  e1:5.9
  s1*3
  fs1:5.9
  gs1:5.9
  s1*5
  b1:5.9

  c1:5.9

  % Blues in the attic
  d1:m
  s1*2
  r1
  d4.:m d8:7.9+ s2
  bf4.:9.13 a8:7 s2
  d2:m d2:7.9+
  g4.:7.13 a8:m7.11 s2

  d1:m
  bf4.:9.13 a8:m7.11 s2
  af2:7 a:7.5+
  d2:m d:7+9

  g2:m9 c:9
  f2:maj7 bf:maj7
  e:dim a:7
  d1:7.9+

  g2:m9 c:9
  f2:maj7 bf:maj7
  a2:sus4 s4. a8
  r1

  d4.:m d8:7.9+ s2
  bf4.:9.13 a8:7 s2
  d2:m d2:7.9+
  g4.:7.13 a8:m7.11 s2

  d1:m
  bf4.:9.13 a8:m7.11 s2
  af2:7 a4.:7.5+ bf8:9
  s4. a8:m7.11 s2

  bf2:9 s4. d8:m7
  s4. g8:sus4 s2
  af2:7 a:7.5+.9+
  r4. d8 s2

  % Swing
  d1:m
  e1:5+

  a2:m9 s4. a8:m7maj
  s1
  a2:m7 s4. a8:m6+
  s4. d8:9 s2

  s2 f4.:7 e8:7
  r1
  f4. g8:5.9 s4. a8:m
  s4. d8:m s2

  a2:m9 s4. a8:m7maj
  s1
  a2:m7 s4. a8:m6+
  s4. d8:9 s2

  s2 f4.:7 e8:7.9+
  r1
  f4. g8:5.9 s4. a8:m
  s1

  s2 d4.:m9 g8:9
  r1
  c4.:7maj c8:6 s4. c8:7maj
  s4. a8:7/cs s2

  s8 d4.:m7 d4.:7.9+ g8:7.13
  r1
  a4.:m/c c8:5.9 s2
  c4.:7maj a8:m/c s2

  s2 df4.:7maj c8:9
  r1
  s2 af4.:7.5+ f8:m/af
  s2 s4. af8:7.13
  s4. f8:7sus4 s4. a8:m5.11
  s4. af8:sus4 s2
  s8 d4.:7.9+ e2:7.9-.9+/d
  a4.:7sus4 e8:7.9+ s2

  a2:m9 s4. a8:m7maj
  s1
  a2:m7 s4. a8:m6+
  s4. d8:9 s2

  s2 f4.:7 e8:7
  r1
  f4. g8:5.9 s4. a8:m
  s4. d8:m s2

  a2:m9 s4. a8:m7maj
  s1
  a2:m7 s4. a8:m6+
  s4. d8:9 s2

  s2 f4.:7 e8:7.9+
  r1
  d4.:7.9+ f8:7.5- s2

  a2:7sus4 c:m6.9
  d2:m7 g4.:m11 fs8:m11
  s1*2

  % Frozen day
  d1 a e e:m
  d1 a e e:m
  g1 a e s

  d1 a e e2:m a
  d1 a e e2:m a

  d1 a e e:m
  g1 a e s
  c1 a

  d1 a e e2:m a
  d1 a e e2:m a
  d1

  % Michelle
  d1
  e1:7

  r1*4
  a1 d:m g:6 fs:dim e2 f:7 e1
  a1 d:m g:6 fs:dim e2 f:7 e1
  a1:m s c:7 f:7 e a:m

  r1*4
  a1 d:m g:6 fs:dim e2 f:7 e1
  a1:m s c:7 f:7 e a:m

  r1*4
  a1 d:m g:6 fs:dim e2 f:7 e1
  a1:m s f:7 e

  a1 d:m g:6 fs:dim e2 f:7 e1
  a1 d:m g:6 fs:dim e2 f:7 e1
  a1

  % Game
  a1:m7 
  g1:7

  c1 s s s
  d1 s s s

  b1:m7 d a g e:m
  d1 s s s

  b1:m7 d a g e:m
  g1 g s

  c1 s s s s

  % Hold back
  bf1/c c:7

  d2:m s4. f8 
  s2 s4. bf8 
  s2 s4. f8
  s2 s4. bf8

  s2 s4. f8
  s1 
  c1
  s1

  d2:m s4. f8 
  s2 s4. bf8 
  s2 s4. f8
  s2 s4. bf8

  s2 s4. f8
  s1 
  c1
  s2 s4. bf8

  s1*3
  s2 s4. f8
  s1*3
  c1

  d2:m s4. f8 
  s2 s4. bf8 
  s2 s4. f8
  s2 s4. bf8

  s2 s4. f8
  s1 
  c1
  s1*3

  % Tears
  d1:m7 g:7

  c2 g/b 
  a2:m a:m/g
  f2 g
  c1

  c2 g/b 
  a2:m a:m/g
  f2 c
  g1

  c2 g/b 
  a2:m a:m/g
  f2 c
  g1

  a1:m
  e1
  e1:dim
  a1:7
  d1:m
  g1

  c2 g/b 
  a2:m a:m/g
  f2 g
  c1

  c2 g/b 
  a2:m a:m/g
  f2 g4:sus4 g:7
  c1

  % Memory
  bf1.
  s1.

  bf1.
  g1.:m
  ef1.
  d1.:m

  c1.:m
  g1.:m7
  f2. s4. ef4./f
  bf2.

  d2.:m d4.:m/ef c4.:m/ef
  d2.:m d4.:m/ef c4.:m/ef
  d2.:m bf4. c4.
  f2. s4. f4.:7maj/e

  d2.:m g2.:m7
  c2.:7 f2.:7maj
  d2. g2.:7
  c1.

  bf1.
  g1.:m
  ef1.
  d1.:m

  c1.:m
  g1.:m7
  f2. s4. ef4./f
  bf2.

  gf1.
  ef1.:m
  cf1.
  bf1.:m

  af1.:m
  ef1.:m
  df2. s4. cf4./df
  gf2.

  bf2.:m bf4.:m/cf af4.:m/cf
  bf2.:m bf4.:m/cf af4.:m/cf
  bf2.:m gf4. af4.:7
  df1.

  bf2.:m7 ef2.:m7
  af2.:7 df2.:7maj
  bf2.:m7 ef2.:7
  af2. af2.:7

  df1.
  bf1.:m
  gf1.
  f1.:m

  ef2.:m4sus ef2.:m
  bf1.:m
  af2. s4. gf4./af
  df1. s1. s1. s1.
} 

instrument = \relative c' {

  \global
  \key f \major

  % El barquito chiquitito
  % ===============================================
  s1
  r2 r4 c(

  \bar "[|:"
  \repeat volta 2 {
    f f g g
    a2) a4( a
    g bf a g
  }
  \alternative {{
    a2 f4) c
    \bar ":|]"
  }{
    a'2 f4 f(
  }}

  \bar "[|:"
  \repeat volta 2 {
    d'2 d
    d4 d2) f,4(
    c'2 c
    c4 c2) f,4(

    bf2 bf
    a4 a g a
  }

  \alternative {{
    f1)
    r2 r4 f
    \bar ":|]"
  }{
    f1~
    f1 \fermata
  }}

  % Rigaudon
  % ===============================================
  \break
  \bar "||"
  \key f \major
  s1
  r2 r4 \f f8( g

  a4 g8 f g4 f8 e)
  f4( c) c a'8( bf
  c4 bf8 a bf4 a8 g
  a2.) f8( g

  a4 g8 f g4 f8 e)
  f4( c) c f8( e
  d4) d8( e f4) g
  c,2 r4 ^"Tacet" \p c8 d

  e4 ^"Rit." \teeny c8 d e4 \normalsize e8 f
  g4 \teeny e8 f g4 \< \normalsize g8 a
  bf4 \teeny g8 a bf4 \normalsize bf8 c
  d1( \! ^"Play"
  df1
  c2) ^"A tempo" r4 f,8( g

  a4 g8 f g4 f8 e)
  f4( c) c a'8( bf
  c4 bf8 a bf4 a8 g
  a2.) f8( g

  a4 g8 f g4 f8 e)
  f4( c) c a8( bf
  c4) g8( \rit a bf4 a8 g
  f1) \! \fermata

  % Bransle de la torche
  % ===============================================
  \pageBreak
  \bar "||"
  s1*2

  \bar "[|:"
  \key d \minor
  \repeat volta 2 {
    d''4.( _\markup { \musicglyph #"p" "-" \musicglyph #"f"} e8 f4)-. f-.
    e2 e4-. e4-.
    d4.( e8 d4)-. d-.
    cs2 a

    d4.( e8 f4)-. f-.
    e4.( f8 g4)-. a-.
    f4( e8 d) cs8( d e4)-.
    d2-- d4-. r
  }

  \bar ":|][|:"
  \repeat volta 2 {
    a'4.( _\markup { \musicglyph #"p" "-" \musicglyph #"f"} g8 f g a f)
    g4-. e8( f g e a g)
    f4-. f8( e d e f g)
    a2 a

    a4.( g8 f g a f)
    g4.( f8 e f g e)
  }
  \alternative {{
    f4-. g8( f e d) e4-.
    d2 d4 r
    \bar ":|]"
  }{
    f4-. g8( f e d) cs4-.
    d2-- d2--
    d1 \fermata
  }}

  % Allegretto
  % ===============================================
  \break
  \bar "||"
  \key bf \major
  s1
  r2 r4 f \mf

  \repeat volta 2 {
    d4( bf) a( c)
    bf4( f) r f-.
    g4-. \< g-. a-. a-.
    bf2 \! r4 f-.
    g4-. \> g-. a-. a-.
    bf2 \! r4 d-. \mf

    c4( f,) e( bf')
    a4( f) r f-.
    g-. \< fs-. g-. a-.
    f2 \! r4 f-.
    g8( \> fs g fs g4)-. a-.
    f2 \! r4 f'-. \mf
  }

  f4( d) d( b)
  b2 r4 g-.
  c4( g) d'( g,)
  ef'2 r4 ef-.

  ef( c) c( a)
  a2 r4 f-.
  bf( f) c'( f,)
  d'2 r4 f-.

  d4( bf) a( c)
  bf4( f) r f-.
  g4-. \< g-. a-. a-.
  bf2 \! r4 f-.
  g4-. \> g-. a-. a-.
  bf2 \! r4 bf-.

  c4-. \< bf-. c-. d-.
  bf2 \! r4 bf-.
  c8( \> ^"rit" b c b c4)-. d-.

  bf1 \! \fermata

  % À la claire fontaine
  % ===============================================
  \pageBreak
  \bar "||"
  \key f \major
  s1*2

  \bar "||"
  f2 f4 a
  a4 g a g
  f2 f4 a
  a4 g a2

  a2 a4 g
  f4 a c a
  c2 c4 a
  f4 a g2 \fermata

  f2 f4 a
  a4 g8 f a4 f
  a2 a4 g8 f
  a4 g f2 \fermata

  % Berceuse
  % ===============================================
  \break
  \bar "||"
  \key d \minor
  s1*2

  \bar "||"
  d'2(-- \p a)
  d2(-- a)
  d4( e d c
  a2 a)

  d2(-- a)
  d2(-- a)
  d4( e d c
  a2 a)

  d4( \mp e d c
  a2 \< g4 a) \!

  f4( f \< g a \!
  b2 \> a) \!
  f4( f \< g a
  bf?2 \! a4 \> c)

  d2(-- \p a)
  d2(-- a)
  d4( e d c
  a2 \> a)
  d4( \pp e d c
  a2 a4 _"rit. e dim." c

  d1 a \> c d) \ppp \fermata

  % Coucou dans les bois
  % ===============================================
  \break
  \bar "||"
  \key c \major
  s1*2

  \bar "||"
  r2 e4 \f c
  r2 e4 c
  r2 e4 c
  r2 e4 c \fermata

  e,4 \p c d2
  e4 c d2
  e4 g e c
  e4 d c2

  e4 c d f
  e4 c d8 e f4
  e4 g e c
  e4 d c2

  r2 e'4 \f \> c
  r2 e4 c
  r2 e4 c
  r2 e4 c \p \fermata

  R1
  e,2-- \pp c2-- ~
  c1 ~
  c1 \fermata

  % Cuckoo
  % ===============================================
  \pageBreak
  \bar "||"
  \key af \major
  \time 6/8
  s1*2*6/8

  \bar "||"
  r4 c'8-. \p af4-- r8
  r4 c8-. af4-- r8
  r4 r8 af \mf c df

  \bar "[|:"
  \repeat volta 2 {
    ef2.-- ~
    ef2. ~
    ef8 r4 r4 af,8
    af4. af4 bf8

    c8 f, f f r af
    af4.-- ~ af4 bf8
    c8 f, f f r af
    af4.-- ~ _"poco piu forte" af4 bf8

    c4 \< ef8 ef4 \! r8
    r8 af,-- af-- af4-- bf8
    c4 ef8 ef r af,
    af4.-- af4 bf8

    c4.( f ~
    f8 ef c ef df bf
    c2.)
    f,2.--

    f2.-- ~
    f8 r4 r4.
  }
  \alternative {{
    r4 r8 af \mf c df
    \bar ":|]"
  }{
    r4 c8-. \pp af4-- r8
    r4 c8-. af4-- r8
  }}

  r4 r8 c8-. r r
  af2. \fermata

  % El patio de mi casa
  % ===============================================
  \break
  \bar "||"
  \key c \major
  \time 4/4
  \override NoteHead.style = #'cross
  c,4 ^"repeat" c8 c-> ~ c4 r4
  {
    \override NoteHead.style = #'cross
    r4 r2 r8 f
    f8 f f f f4 f
    f8 f f f f4 f8 f
    f4 f8 f f4 f
    f8 f f f f4 f8 f
    f8 f f4 f8 f f f
    f4 r r \revert NoteHead.style gs

    b cs b gs
    b8 b cs cs b4 r8 gs
    b8 b b b cs4 cs8 cs
    cs4 cs8 cs ds2

    \override NoteHead.style = #'cross
    f,8 f f f r2
    f8 f f f r4 f8 f
    f8 f f f r4 f8 f
    f8 f f f r4 f8 f
    f8 f f f r4 f8 f
    f8 f f f f f f f
    f8 f f f f f f f
    f4 r4 r2
    r2 r4 f8 f

    f8 f f f f f r4
    f8 f f f f f f f
    f4 f8 f f4 f4
    f8 f f f f4 f8 f
    f8 f f4 f8 f f f
    f4 r r \revert NoteHead.style gs

    b cs b gs
    b8 b cs cs b4 r8 gs
    b8 b b b cs4 cs8 cs
    cs4 cs8 cs ds2

    \override NoteHead.style = #'cross
    f,8 f f f r2
    f8 f f f r4 f8 f
    f8 f f f r4 f8 f
    f8 f f f r4 f8 f
    f8 f f f r4 f8 f
    f8 f f f f f f f
    f8 f f f f f f f
    \stemDown c4 \stemUp f8 f f4 \stemDown c4
  }
  \addlyrics {
    El pa -- tio de mi ca -- sa
    es par -- ti -- cu -- lar
    cuan -- do llue -- ve se mo -- ja
    co -- mo los de -- más
    co -- mo los de -- más
    co -- mo los de -- más

    A -- gá -- cha -- te
    y vuel -- ve te_a_a -- ga -- char
    que los a -- ga -- cha -- di -- tos
    no sa -- ben bai -- lar

    cho -- co -- la -- te
    cho -- co -- la -- te
    cho -- co -- la -- te la -- te
    mo -- li -- ni -- llo la -- te
    co -- rre co -- rre la -- te
    que  te pi -- llo la -- te
    que  te pi -- llo pi -- llo
    que  te pi -- llo pi -- llo
    ¡ya!

    No me di -- gas que tu pa -- tio
    es par -- ti -- cu -- lar,
    ya que se mo -- ja se mo -- ja
    co -- mo los de -- más
    co -- mo los de -- más
    co -- mo los de -- más

    A -- gá -- cha -- te
    y vuel -- ve te_a_a -- ga -- char
    que los a -- ga -- cha -- di -- tos
    no sa -- ben bai -- lar

    cho -- co -- la -- te
    cho -- co -- la -- te
    cho -- co -- la -- te la -- te
    mo -- li -- ni -- llo la -- te
    co -- rre co -- rre la -- te
    que  te pi -- llo la -- te
    que  te pi -- llo pi -- llo
    que  te pi -- llo pi -- llo
    "" ¡te pi -- llé!
  }

  \revert NoteHead.style

  % Blues on the attic
  % ===============================================
  \pageBreak
  \bar "||"
  \key d \minor
  s1*3
  r8 cs( \mf d4)-. f8( d d'4)-.

  \bar "||"
  a4. f8 ~ f4 d4-.
  g8( f g a) r a( f c
  e4)-. d2-- f4-.
  e4-. r8 d8 ~ d2

  r4 a'16( \< c d8)-. \! g,8( f) g( f)
  g8-. g4-- a8-^ r d,8( f g)
  af4-- af-- g8( \> f) \! r8 d-> ~
  d4 \teeny d8-. d( f)-. (f fs)-. fs(

  g4) \p \normalsize d8( a') r g4.->
  r4 a,8( e') r d4.->
  r8 d( \< bf' g a4 \mf g)
  f2. \> r4 \!

  r4 \mp d8( a') r g4.->
  r8 a,8( e' c d4 \< f)
  r8 \f a-. a4-- a8-- a-. r a-^
  r8 cs,( \mf d4)-. f8( d d'4)-.

  a4. f8 ~ f4 d4-.
  g8( f g a) r a( f c
  e4)-. d2-- f4-.
  e4-. r8 d8 ~ d2

  r4 d'16( c a8)-. g8( f) g( f)
  g8-. g4-- a8-^ r d,8( f g)
  af4-- af-- g8( \> f) \! r8 d-> ~
  d2 r8 d8( f g)

  af4-- af-- \< g8( f) r8 \! d-> ~ \mf
  d2 \< r8 d( \f f g)
  af4-- af-- r8 a( c4)-.
  d8 gs, a d, ~ d2 \fermata

  % Swing
  % ===============================================
  \pageBreak
  \bar "||"
  \key a \minor
  s1
  r2 r4 a'-.
  << {
    e'1 \f
    r8 d4(-. e8-> f g e a,
    c1-> ~
    c2) r4 a-.

    c1 ->
    r8 b4(-. c8-> d e c e,
    a4-. \> c8 a-> \! ~ a4) r8 c,(
    d e f \< g a b c d

    e1)-> \f
    r8 d4(-. e8-> f g e a,
    c1-> ~
    c2) r4 a-.

    c1 ->
    r8 b4(-. c8-> d e b e,
    c'8-> \> a g a-> ~ a2 ~
    a2) r4 \< a4-.

    a'1( \f
    r8 g4-. a8-> g4 d
    e4-> g8 e-> ~ e2
    e2) r4 a,-.

    a'1(
    r8 b4-. c8-> b4 g
    e4-> g8 e-> ~ e2 \>
    e2.) \! c4-. \<

    c'1( \f
    r8 bf4-. c8-> bf8 g f ef ~
    ef1 ~
    ef2) r8 f,( g af)->

    r8 af( bf c)-> r8 af( bf c)->
    r8 c( df ef)-> r8 ef( f g-> ~
    g1 ~
    g2) r4 a,4-.

    e'1 \f
    r8 d4(-. e8-> f g e a,
    c1-> ~
    c2) r4 a-.

    c1 ->
    r8 b4(-. c8-> d e c e,
    a4-. \> c8 a-> \! ~ a4) r8 c,(
    d e f \< g a b c d

    e1)-> \f
    r8 d4(-. e8-> f g e a,
    c1-> ~
    c2) r4 a-.

    c1 ->
    r8 b4(-. c8-> d e c e,
    a4-. c8 a)-> r4 r8 a'(-.
    g8-> e d c a g a c

    a4)-> r4 a4.-> gs8-> ~
    gs1 ~ \fermata
    gs1

  } \\ \relative c' {
    \override NoteHead.style = #'cross
    r8 d4.-> d4. d8-> ~
    d4 s4 s2
    r8 d4.-> d4. d8-> ~
    d8 d d d-> ~ d4 s4

    r2 d4.-> d8->
    s1
    d4.-> d8-> ~ d4 s4
    r4. d8-> ~ d4 s4

    r8 d4.-> d4. d8-> ~
    d4 s4 s2
    r8 d4.-> d4. d8-> ~
    d8 d d d-> ~ d4 s4

    r2 d4.-> d8->
    s1
    d4.-> d8-> ~ d4. d8 -> ~
    d4 s2.

    r2 d4.-> d8->
    s1
    d4 d8 d ~ d4 d8 d ~
    d4 d8 d-> r2

    r8 d4.-> d4.-> d8-> ~
    d4 s2.
    d4.-> d8-> ~ d4 r4
    d4-> d8 d-> ~ d4 r4

    r2 d4.-> d8->
    s1
    r2 d4.-> d8-> ~
    d4 r4 r4 r8 d->

    r4 r8 d-> r4 r8 d->
    r4 r8 d-> r4 r8 d-> ~
    d8 d4.-> d4-- r4
    d4.-- d8-> r2

    r8 d4.-> d4. d8-> ~
    d4 s4 s2
    r8 d4.-> d4. d8-> ~
    d8 d d d-> ~ d4 s4

    r2 d4.-> d8->
    s1
    d4.-> d8-> ~ d4 s4
    r4. d8-> ~ d4 s4

    r8 d4.-> d4. d8-> ~
    d4 s4 s2
    r8 d4.-> d4. d8-> ~
    d8 d d d-> ~ d4 s4

    r2 d4.-> d8->
    s1
    d4.-> d8-> r2
    s1

    s1*3
  } >>

  % Frozen day
  % ===============================================
  \pageBreak
  \bar "||"
  \key d \major
  s1*3
  r2 r8 cs d e

  fs1
  e8 cs a4 r8 a b cs
  e1
  d8 b g4 r8 d e fs

  g2. d'4
  cs2 r8 a b cs
  e1 ~
  e4 cs d e

  a4. fs8 e4 d
  d2 cs
  gs'4. fs8 e2
  e4. cs8 b4 a

  a'4. fs8 e4 d
  d2 cs
  gs'4. fs8 e2
  e4. cs8 b4 a

  fs'1
  e8 cs a4 r8 a b cs
  e1
  d8 b g4 r8 d e fs

  g2. d'4
  cs2 r8 a b cs
  e1 ~
  e4 cs d e

  e4. c8 g c e c
  e4. cs8 a cs e cs

  a'4. fs8 e4 d
  d2 cs
  gs'4. fs8 e2
  e4. cs8 b4 a

  a'4. fs8 e4 d
  d2 cs
  gs'4. fs8 e2
  e4. cs8 b4 cs

  d1 \fermata

  % Michelle
  % ===============================================
  \break
  \bar "||"
  \key a \minor

  s1*2
  \bar "[|:"
  \repeat volta 2 {
    a4 e' gs, e'
    g,? e' fs, e'
    << { r4 e d c } \\ { f,1} >>
    b1

    e2 e
    r4 f c2
    b4 e b b
    a c ef c
    b2 a4 c8 b ~
    b1

    e2 e
    r4 f c2
    b4 e b b
    a c ef c
    b2 a4 c8 b ~
    b2. r8 e

    \times 2/3 { a4 g e } \times 2/3 { a4 g e }
    b'2 a
    \times 2/3 { r4 e e } \times 2/3 { e f c }
    c1
    r8 e e4 a e8 d ~
    d8 c ~ c4 r8 c d4
  }
  \bar ":|]"

  a4 e' gs, e'
  g,? e' fs, e'
  << { r4 e d c } \\ { f,1} >>
  b1

  a,4 \times 2/3 { r8 a b } cs4 \times 2/3 { r8 d e }
  f2 c
  b4 \times 2/3 { r8 b c } d4 g,
  a4 \times 2/3 { r8 a b } c4 fs,
  gs2 \times 2/3 { fs4 c' a }
  gs2 \times 2/3 { r8 e fs } \times 2/3 { gs a b }

  \times 2/3 { a''4 g e } \times 2/3 { a4 g e }
  b'2 a
  \times 2/3 { r4 e e } \times 2/3 { e f c }
  c1
  r8 e e4 a e8 d ~
  d8 c ~ c4 r8 c d4

  a4 e' gs, e'
  g,? e' fs, e'
  << { r4 e d c } \\ { f,1} >>
  b1

  e2 e
  r4 f c2
  b4 e b b
  a c ef c
  b2 a4 c8 b ~
  b2. a8 b

  c4 a d b8 c ~
  c4 a d4. b8
  c2 b4 a
  gs2 a4 b

  a4 \times 2/3 { r8 a, b } cs4 \times 2/3 { r8 d e }
  f2 c
  b4 \times 2/3 { r8 b c } d4 g,
  a4 \times 2/3 { r8 a b } c4 fs,
  gs2 \times 2/3 { fs4 c' a }
  gs2 \times 2/3 { r8 e fs } \times 2/3 { gs a b }

  a4 \times 2/3 { r8 a b } cs4 \times 2/3 { r8 d e }
  f2 c
  b4 \times 2/3 { r8 b c } d4 g,
  a4 \times 2/3 { r8 a b } c4 fs,
  gs2 \times 2/3 { fs4 c' a }
  gs2 \times 2/3 { r8 e fs } \times 2/3 { gs a b }

  a1 \fermata

  % Game
  % ===============================================
  \break
  \bar "||"
  \key c \major
  s1*2

  \bar "[|:"
  \repeat volta 2 {
    \teeny
    g'8 b a g f4. d8
    f8 e e f g2
    g8 b a g f4. d8
    f8 e e f g2

    \normalsize
    \repeat percent 2 {
      d4 \times 2/3 { d8 d d } d2
      d4 \times 2/3 { d8 d d } d2
    }

    r2 b'8. c16 cs8. d16 ~
    d2 d8. e16 d8. cs16 ~
    cs2 cs8. d16 cs8. b16 ~
    b2 g8. fs16 f8. e16 ~
    e1

    \repeat percent 2 {
      d4 \times 2/3 { d8 d d } d2
      d4 \times 2/3 { d8 d d } d2
    }

    r2 b'8. c16 cs8. d16 ~
    d2 d8. e16 d8. cs16 ~
    cs2 cs8. d16 cs8. b16 ~
    b2 g8. fs16 f8. e16 ~
    e2 e8. f16 fs8. g16 ~
  }
  \alternative {{
    g1
    \bar ":|]"
  }{
    g1 ~
    g1
  }}

  \repeat volta 4 {
    \teeny
    g8 b a g f4. d8
    f8 e e f g2
    g8 b a g f4. d8
  }
  \alternative {{
    f8 e e f g2
  }{
    f8 e e f g2 \fermata
  }}

  % Hold back
  % ===============================================
  \break
  \bar "||"
  \key f \major
  \normalsize
  s1*2

  \bar "||"
  f2 r8 g r a ~
  a2 r8 c r d ~
  d2 r8 c r a ~
  a2 r8 c r d ~

  d2 r8 c r a ~
  a2 a4 a
  g4 g r8 f e e ~
  e8 c' ~ c2.

  \bar "[|:"
  \repeat volta 2 {
    f,2 r8 g r a ~
    a2 r8 c r d ~
    d2 r8 c r a ~
    a2 r8 c r d ~

    d2 r8 c r a ~
    a1
    a8 g ~ g2.
  }
  \alternative {{
    r1
    \bar ":|]"
  }{
    r2 r4 r8 f
  }}

  r8 f r f f4 a8 a
  g8 g a4 f4. f8
  r8 f r f f g a4
  g2 f4 r8 f

  r8 f r f f4 a8 a
  g8 g a4 f4. f8
  r8 f r f f g a4
  g1

  \bar "[|:"
  \repeat volta 2 {
    f2 r8 g r a ~
    a2 r8 c r d ~
    d2 r8 c r a ~
    a2 r8 c r d ~

    d2 r8 c r a ~
    a2 a4 a
    g4 g r8 f e e ~
  }
  \alternative {{
    e8 c' ~ c2.
  }{
    e,8 c' ~ c2. \fermata
  }}

  % Tears
  % ===============================================
  \break
  \bar "||"
  \key f \major
  s1*2

  \bar "||"
  R1*4

  \bar "[|:"
  \repeat volta 2 {
    r4 e8 c g'4 g8 e ~
    e1
    r4 f8 f e d c e
    d1

    r4 e8 c g'4 g8 e ~
    e1
    r4 f8 f e d c e
    d1

    r4 r8 c c4 d8 b ~
    b1
    r4 r8 bf bf4 c8 a ~
    a2 d4 e8 f ~

    f2 d4 c
    b?8 a g4 r8 e' d c16 d
    c1
    R1*3
  }

  r4 e8 c g'4 g8 e ~
  e1
  r4 ^"molto rit.." f e d
  c1 \fermata

  % Memory
  % ===============================================
  \pageBreak
  \bar "||"
  \key bf \major
  \time 12/8
  s1*2*12/8

  \bar "[|:"
  \repeat volta 2 {
    bf4. bf ~ bf8 a bf c bf g
    bf4. bf ~ bf8 a bf c bf f
    g4. g ~ g8 ef f g f ef
    d2. ~ d4. d4 f8

    f4. c4 d8 \times 3/2 { ef8 f } \times 3/2 { g a }
    bf8 a g f4. ~ f4. d4 bf8
    f'4. f2. g,4 bf8
    \time 6/8
    bf2.
  }
  \bar ":|]"
  \time 12/8
  a4. d a g
  a4. d a ~ a8 r g
  a4. d d ~ d8 c4
  c4. c2. r4.

  f4. f f4 a8 ~ a g f
  e4. e e4 g8 ~ g4 f8
  a4. a a b
  c4. ^"rit..." g2. r4.

  bf4. ^"a tempo" bf ~ bf8 a bf c bf g
  bf4. bf ~ bf8 a bf c bf f
  g4. g ~ g8 ef f g f ef
  d2. ~ d4. d4 f8

  f4. c4 d8 \times 3/2 { ef8 f } \times 3/2 { g a }
  bf8 a g f4. ~ f4. d4 bf8
  f'4. f2. g,4 bf8
  \time 6/8
  << { \teeny r8 bf' c d ef f } \\ { bf,,2. } >>

  \bar "||"
  \teeny
  \time 12/8
  \key gf \major
  gf''4. gf ~ gf8 f gf af gf ef
  gf4. gf ~ gf8 f gf af gf df
  ef4. ef ~ ef8 cf df ef df cf
  bf2. r4. bf4 df8

  df4. af4 bf8 \times 3/2 { cf8 df } \times 3/2 { ef f }
  gf8 f ef df4. ~ df4. bf4 gf8
  df'4. df2. ef,4 gf8
  \time 6/8
  gf2.

  \time 12/8
  \normalsize
  f4. bf f ef
  f4. bf f ~ f8 r ef
  f4. bf bf ~ bf8 af4
  af4. af ~ af r4 df,8

  df4. df df4 f8 ~ f ef df
  c4. c c4 ef8 ~ ef df4
  f4. f f g
  af4. ^"rit..." ef ~ ef r4.

  \key df \major
  df'4. ^"a tempo" df ~ df8 c df ef df bf
  df4. df ~ df8 c df ef df af
  bf4. bf ~ bf8 gf af bf af gf
  f2. ~ f4. f4 af8

  af4. ef4 f8 \times 3/2 { gf8 af } \times 3/2 { bf c }
  df8 c bf af4. ~ af4. f4 df8
  af'4. af2. bf,4 df8
  << { \teeny df''4 af8 b4 bf8 af4 f8 gf4 ef8 } \\ { \normalsize df,2. r2. } >>
  \teeny
  f'4 df8 ef4 c8 df4 af8 af4.
  df'4 af8 b4 bf8 af4 f8 gf4 ef8
  f4 df8 ef4 c8 df4 df,8 df4. \fermata

  \bar "|."
}

% (Sólo se imprime el primer \book que encuentra Lilypond)
%{
  \book {
    \header { instrument = "" }
    \bookOutputName ""

    \score {
      <<
        \scoreMarkup
        \new StaffGroup <<
          \transpose c c \scoreChords
          \new Staff \with { instrumentName = "" midiInstrument = #"" } {
            \transpose c c \instrument
          }
        >>
      >>
    }
  }
  %}

  \book {
    \bookOutputName "Score"

    \paper {
      % Descomentar para score
      % #(set-paper-size "a4" 'landscape)
      % indent = 20
      % systems-per-page = 2
    }

    \score {
      % Descomentar para midi
      % \midi { \tempo 4 = 110 }

      % NO USAR layout-set-staff-size O SE ROMPE lilyjazz!!!
      \layout { }

      <<
        \scoreMarkup
        \new StaffGroup <<
          \transpose c c \scoreChords
          \new Staff \with { instrumentName = "" midiInstrument = #"" } {
            \transpose c c \instrument
          }
        >>
      >>
    }
  }
