\version "2.16.1"
\include "imports/main.ly"


% Instrument Music
% -------------------------------------------------
violin = {

  % Para pintar todos los números de compás debajo del pentagrama
  % \override Score.BarNumber #'break-visibility = #end-of-line-invisible
  % \override Score.BarNumber #'direction = #DOWN
  % \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0
	
  \key g \major
  r8 g g g r8 g g g 
  r8 g g g r8 g g g 
  
  \repeat volta 2 {
    d2. d4 
    a'2 a
    b1
    g2 fis
    
    \break
    g2. a4
    g2 e
    e4 g8 g ~ g2
  }
  
  a2 fis
  fis4 g g g
  a2 a
  g4 d f d8 c
  
  \break
  a'2 fis
  fis4 g g g
  e1
  g4 d c b
  
  d2. d4 
  a'2 a
  b1
  g2 fis
  
  \break
  g2. a4
  g2 e
  e4 g8 g ~ g2
  
  a2 fis
  fis4 g g g
  a2 a
  g4 d f d8 c
  
  \break
  a'2 fis
  fis4 g g g
  e1
  g4 d c b
  
  d2. d4 
  a'2 a
  b1
  g2 fis
  
  \break
  g2. a4
  g2 e
  e4 g8 g ~ g2
  
  d2^\markup \italic "rit." g4 e
  e g8 g ~ g2
  \bar "|."
}


% Instrument Sheet
% -------------------------------------------------

\header {
  instrument = "Violín"
}

\paper {  	
  % distancia entre sistemas
  system-system-spacing #'basic-distance = #18
  
  % distancia entre el título y el primer sistema
  markup-system-spacing #'basic-distance = #20
}

<<
  \scoreMarkup
  \new Staff << \transpose c bes \relative c' \violin >>
>>