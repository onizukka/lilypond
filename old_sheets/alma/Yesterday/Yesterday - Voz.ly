\version "2.16.1"
\include "imports/main.ly"


% Instrument Music
% -------------------------------------------------
voice = \new Voice = "main" {

  % Para pintar todos los números de compás debajo del pentagrama
  % \override Score.BarNumber #'break-visibility = #end-of-line-invisible
  % \override Score.BarNumber #'direction = #DOWN
  % \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0
	
  \key g \major
  r1
  r1
  
  \repeat volta 2 {
    a8 g g2.
    r4 b8 cis dis e fis g 
    fis8. e16 e2.
    r4 e8 e d c b a
    
    \break
    c4 b8 b ~ b4 a 
    g4 b8 a ~ a4 e
    g4 b8 b ~ b2
  }
  
  b2 b
  e4 fis g fis8 e
  fis4. e8 d4 e
  b1
  
  \break
  b2 b
  e4 fis g fis8 e
  fis4. e8 d4 fis
  g1
  
  a,8 g g2.
  r4 b8 cis dis e fis g 
  fis8. e16 e2.
  r4 e8 e d c b a
  
  \break
  c4 b8 b ~ b4 a 
  g4 b8 a ~ a4 e
  g4 b8 b ~ b2
  
  b2 b
  e4 fis g fis8 e
  fis4. e8 d4 e
  b1
  
  \break
  b2 b
  e4 fis g fis8 e
  fis4. e8 d4 fis
  g4 d c b
  
  a8 g g2.
  r4 b8 cis dis e fis g 
  fis8. e16 e2.
  r4 e8 e d c b a
  
  \break
  c4 b8 b ~ b4 a 
  g4 b8 a ~ a4 e
  g4 b8 b ~ b2
  
  g4^\markup \italic "rit." b4 a e
  g b8 b \fermata ~ b2
  \bar "|."
}

text = \new Lyrics \lyricsto "main" {
  <<
    { Yes -- ter -- day
      All my trou -- bles seemmed so
      far a -- way
      Now it looks as though they're here to stay
      Oh, I be -- lieve in yes -- ter -- day } //
    
    \new Lyrics
    { \set associatedVoice = "main"
      Su -- dden -- ly
      I'm not half the man I used to be
      There's a sha -- dow hang -- ing o -- ver me
      Oh, yes -- ter -- day came su -- dden -- ly }
  >>
  
  Why she 
  had to go? I don't 
  know, she would -- n't say
  I said
  some -- thing wrong Now I
  long for yes -- ter -- day
  
  Yes -- ter -- day
  love was such an ea -- sy 
  game to play
  Now I need a place to hide a -- way
  Oh, I be -- lieve in yes -- ter -- day.
  
  Why she 
  had to go? I don't 
  know, she would -- n't say
  I said
  some -- thing wrong Now I
  long for yes -- ter -- 
  da -- a -- a -- ay.
  
  Yes -- ter -- day
  love was such an ea -- sy 
  game to play
  Now I need a place to hide a -- way
  Oh, I be -- lieve in yes -- ter -- day
  I be -- lieve in yes -- ter -- day
}


% Instrument Sheet
% -------------------------------------------------

\header {
  instrument = "Voice"
}

\paper {  	
  % distancia entre sistemas
  system-system-spacing #'basic-distance = #18
  
  % distancia entre el título y el primer sistema
  markup-system-spacing #'basic-distance = #20
}

<<
  \scoreMarkup
  \new Staff << \transpose c' bes \relative c'' \voice \text >>
>>