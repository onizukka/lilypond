\version "2.16.1"
\include "imports/main.ly"


% Instrument Music
% -------------------------------------------------
fhorn = {

  % Para pintar todos los números de compás debajo del pentagrama
  % \override Score.BarNumber #'break-visibility = #end-of-line-invisible
  % \override Score.BarNumber #'direction = #DOWN
  % \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0
	
  \key c \major
  r8 g g g r8 g g g 
  r8 g g g r8 g g g 
  
  \repeat volta 2 {
    e2. e4 
    a2 b
    c1
    a2 g
    
    \break
    g2. g4
    g2 fis
    f?4 g8 g ~ g2
  }
  
  a4 fis gis2
  a4 g f e
  f2 b
  g1
  
  \break
  a4 fis gis2
  a4 g f e
  f1
  g1
  
  e2. e4 
  a2 b
  c1
  a2 g
  
  \break
  g2. g4
  g2 fis
  f?4 g8 g ~ g2
  
  a4 fis gis2
  a4 g f e
  f2 b
  g1
  
  \break
  a4 fis gis2
  a4 g f e
  f1
  g1
  
  e2. e4 
  a2 b
  c1
  a2 g
  
  \break
  g2. g4
  g2 fis
  f?4 g8 g ~ g2
  
  e2^\markup \italic "rit." fis2
  f? g8 g ~ g2
  \bar "|."
}


% Instrument Sheet
% -------------------------------------------------

\header {
  instrument = "Trompa en Fa"
}

\paper {  	
  % distancia entre sistemas
  system-system-spacing #'basic-distance = #18
  
  % distancia entre el título y el primer sistema
  markup-system-spacing #'basic-distance = #20
}

<<
  \scoreMarkup
  \new Staff << \relative c'' \fhorn >>
>>