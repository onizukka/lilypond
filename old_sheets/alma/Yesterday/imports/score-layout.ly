\header {
  title = "Yesterday"
  instrument = ""
  composer = "Paul McCartney & Jhon Lennon"
  arranger = ""
  poet = ""
}



\paper {
  % El definir otro tipo de fuente no permite cambiar el tamaño global del staff. Es un bug conocigo
  #(define fonts (make-pango-font-tree "LilyJAZZText" "Nimbus Sans" "Luxi Mono" (/ 20 20)))
  
  % distancia entre sistemas
  system-system-spacing #'basic-distance = #50
  
  % distancia entre el título y el primer sistema
  markup-system-spacing #'basic-distance = #35
  
  top-markup-spacing #'basic-distance = #5
  
  top-system-spacing #'basic-distance = #15
  
  bottom-margin = 20
  
  systems-per-page = 6
  
  ragged-last-bottom = ##f
  
  ragged-bottom = ##f
  
  oddHeaderMarkup = \markup {
    \on-the-fly #not-first-page \fill-line {
      \fromproperty #'page:page-number-string
      \fromproperty #'header:title
      \fromproperty #'header:instrument
    }
  }
  
  evenHeaderMarkup = \markup {
    \on-the-fly #not-first-page \fill-line {
      \fromproperty #'header:instrument
      \fromproperty #'header:title
      \fromproperty #'page:page-number-string
    }
  }
}