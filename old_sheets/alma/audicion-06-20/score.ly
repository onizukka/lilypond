\version "2.18.2"
\language "english"

% Se debe incluir antes que lilyjazz. Si no, lilyjazz no funciona.
#(set-global-staff-size 19)

\include "main.ily"
\include "music-sheet-paper.ily"

\header {
  title = "Audición ALMA 20-06-20"
  instrument = ""
  composer = ""
  arranger = "gbpsms@gmail.com"
  poet = ""
  tagline = ""
}

\paper { indent = 0 }

global = {
  \compressFullBarRests

  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0

  \set Timing.beamExceptions = #'()
  \set Timing.baseMoment = #(ly:make-moment 1/4)
  \set Timing.beatStructure = #'(1 1 1 1)

  \override ParenthesesItem.font-size = #0
}

% Ecossaise
% ========================================================================
ecossaise_ch = \chordmode {
  s4

  g2:maj7 g:6 d:9 a:m7
  d2:7 d:5+9- d:7/g g

  e1:m7 d2:9 a:m7
  d2:7 af:9 g1:maj7

  d1:7 a2:m7 d:7
  d1:7 f2:5.9/a d:9

  d2.:7 g4:7 c2.:maj7 g4:5.9/b
  d2:7 d:5-7 g1

}

ecossaise_mu = \relative c {
  \global
  \key g \major
  \clef bass

  \once \override Score.RehearsalMark.extra-offset = #'(0 . 3)
  \mark \jazzTempoMarkup "Bachata" c4 "120"

  \partial 4
  r4

  \bar "[|:"
  g4. \mf d'8 d4 g,
  d'4. a'8 a4 a,
  d4. bf'8 bf4 d,
  g,4. g8 g4 fs \p

  e4. b'8 b4 e
  d4. a'8 a4 a,
  d4. a8 af4 af
  << { g4 r g r } \\ \redcross { b' r4 s4 r4} >>
  \eolMark \markup \underline "(Fine)"
  \bar ":|][|:"

  d,4. \f a'8 a4 d,
  a4. a8 d4 a \p
  d4. a'8 a4 d,
  a4. a8 d4 a \f

  << { d4 d2 g,4 } \\ \redcross { b' s2. } >>
  << { c,4 c2 b4 } \\ \redcross { b' s2. } >>
  d,4. d8 d'4 d,
  << { g,4 r g r } \\ \redcross { b' s2. } >>
  \bar ":|][|:"
  \eolMark \markup \underline "(D.C. al Fine)"
  \pageBreak
}

\score {
  \header {
    piece = \markup \huge "Écossaise"
    opus = \markup \huge "L.W.Beethoven"
  }
  <<
    \new ChordNames { \ecossaise_ch }
    \new Staff { \ecossaise_mu }
  >>
}

% Orff
% ========================================================================
orff_ch = \chordmode {
  g1 c/g d/g g
  \break
  g1 c/g d/g g2 g/fs
  \break
  g1/f a:m/e ef:dim g:6/d
  \break
  c1:7maj9 a:m7 d:7 g

  \break
  d1:m/g s e:m/g s
  \break
  f1/g s g:6/g s
  \break
  d1:m/g s e:m/g s
  \break
  f1:m/g f/g g:6 f:m/g

  \break
  g1 c/g d/g g
  \break
  g1 c/g d/g g
  \break
  g1/f a:m/e e:dim g:6/d
  \break
  c1:7maj9 a:m7 d:7 g
}

orff_mu = \relative c {
  \global
  \key g \major
  \clef bass

  \once \override Score.RehearsalMark.extra-offset = #'(0 . 3)
  \mark \jazzTempoMarkup "Reggae" c4 "160"

  r4 \p g8 g d'4 g,
  r4 g8 g e' e c4
  r8 g16 g g8 g a4 d
  r4 d8 d d8 d b g

  r4 \p g8 g d'4 g,
  r4 g8 g e' e c4
  r8 g16 g g8 g a4 d
  g2 fs2

  f4 \p f8 f f f g4
  r8 e16 e e8 e a a e4
  ef4. ef8 ef4. ef8
  d8 e16 e e8 e \times 2/3 { g4 a b }

  c4 \p c,8 c c4 c
  a4 a8 a e'4 c8 a
  d4. d8 d8 d c d
  g,8 d'16 d d8 d8 d4 d

  \bar "||"
  g,4. \f g8 g2 ~
  g2 ~ g8 d f d
  g4. g8 g2 ~
  g1

  g4. \f g8 g2 ~
  g2 ~ g8 f a f
  g4. g8 g2 ~
  g1

  \pageBreak
  g4. \f g8 g2 ~
  g2 ~ g8 d f d
  g4. g8 g2 ~
  g1

  g4. \f g8 g2 ~
  g2 ~ g8 f a f
  g4. g8 g2 ~
  g1

  \bar "||"
  r4 \p g8 g d'4 g,
  r4 g8 g e' e c4
  r8 g16 g g8 g a4 d
  r4 d8 d d8 d b g

  r4 \p g8 g d'4 g,
  r4 g8 g e' e c4
  r8 g16 g g8 g a4 d
  g2 fs2

  f4 \p f8 f f f g4
  r8 e16 e e8 e a a e4
  ef4. ef8 ef4. ef8
  d8 e16 e e8 e \times 2/3 { g4 a b }

  c4 \p c,8 c c4 c
  a4 a8 a e'4 c8 a
  d4. d8 d4 d
  g,1 \fermata
  \bar "||"
  \pageBreak
}

\score {
  \header {
    piece = \markup \huge "Tanz"
    opus = \markup \huge "C.Orff"
  }
  <<
    \new ChordNames { \orff_ch }
    \new Staff { \orff_mu }
  >>
}

% Musette
% ========================================================================
musette_ch = \chordmode {
  d2 g/d
  d4:7 g/d a:m7 d:7
  g2/d c/d
  b4:m7 e:m a:13 d

  a2 g/a
  a2:m7 g4/a e:9
  e2:7 a:6/e
  e2:m7 a:6/e
  e4 a/e e:7 s
  fs:m7 s e:6 a:13

  d2 g/d
  d4:7 g/d a:m7 d:7
  g2/d c/d
  b4:m7 e:m a:13 d
}

musette_mu = \relative c {
  \global
  \key d \minor
  \clef bass

  \once \override Score.RehearsalMark.extra-offset = #'(0 . 3)
  \mark \jazzTempoMarkup "Cumbia" c4 "64"

  d4 \f a'8 d d,4 g8 d'
  d,4 g8 b a4 a8 d
  \break
  d,4 g8 d' d,4 g8 d'
  b4 e,8 b' a8 a, d4

  \break
  a4 \p e'8 a a,4 d8 a'
  a,4 e'8 a a,4 e'8 b
  \break
  e,4 \mf b'8 e e,4 a8 e'
  e,4 \p b'8 e e,4 a8 e'
  \break
  e,4 \mf a8 e' e,4 r
  fs4 r4 e4 a4

  \break
  d4 \f a'8 d d,4 g8 d'
  d,4 g8 b a4 a8 d
  \break
  d,4 g8 d' d,4 g8 d'
  b4 \rit e,8 b' a8 a, d4 \fermata \!
  \bar "||"
  \pageBreak
}

\score {
  \header {
    piece = \markup \huge "Musette"
    opus = \markup \huge "Anonymous"
  }
  <<
    \new ChordNames { \musette_ch }
    \new Staff { \musette_mu }
  >>
}

% Oso
% ========================================================================
oso_mk = {
  \mkup "Oso"
  s1*10

  s1*8
}

oso_ch = \chordmode {
  a2:m a:m6
  e2:9 e:9-
  a2:m a:m6
  e2:9 e:7

  d2:m g:9
  c2:6 f:maj7
  e:7 a:m
  f:9 e:9
  f2.:9 e4:7
  a1:m

  c2:6 g:5.9/b
  f2:5.9/a c:6/g
  f2:5.9 c:6/e
  g:7/d c:maj7

  c2:6 g:5.9/b
  f2:5.9/a c:6/g
  f:6 d:7
  e:4sus7 e:9
}

oso_mu = \relative c {
  \global
  \key a \minor
  \clef bass

  \once \override Score.RehearsalMark.extra-offset = #'(0 . 3)
  \mark \jazzTempoMarkup "Tango" c4 "100"

  \repeat volta 2 {
    R1*4 \mf
    R1*2 \f \>

  } \alternative {{
    R1*2 \p
    \bar ":|]"
  }{
    R1 \p
    \redcross b'4 r4 b r4 \revertRedcross
    \eolMark \markup \underline "(Fine)"
  }}

  \bar "||"
  \break
  R1*4 \f
  R1*4 \mf
  \bar "||"
  \eolMark \markup \underline "(D.C. al Fine wihout repetitions)"
  \pageBreak
}

\score {
  \header {
    piece = \markup \huge "Lazy Bear"
    opus = \markup \huge "V. Neugasimov"
  }
  <<
    \new ChordNames { \oso_ch }
    \new Staff { \oso_mu }
  >>
}

% Dusty Blues
% ========================================================================
dusty_blue_ch = \chordmode {
  s2
  g1:6 | s | s | s
  c:6 | s | g:6 | g
  d:6.9+ | c:6 | g:6 | g4 af:7 g2:6
}

dusty_blue_mu = \relative c' {
  \global
  \key g \major
  \time 4/4

  \mark \jazzTempoMarkup "Bluesy" c4 "76"

  \partial 2 r2

  \bar "||"
  R1*4
  \break
  R1*4
  \break
  R1*2
  r2 r4 \rit r4
  r4 \! r4 r2 \fermata

  \bar "||"
  \pageBreak
}

\score {
  \header {
    piece = \markup \huge "Dusty Blue"
    opus = \markup \huge "J. Armstrong"
  }
  <<
    \new ChordNames { \dusty_blue_ch }
    \new Staff { \dusty_blue_mu }
  >>
}

% Sonatina
% ========================================================================
sonatina_ch = \chordmode {
  a2:m a:m6
  e:7 e:7.9-
  a4:m a:m6/e b:7/fs e:7
  f2:6/g c4:5+ e:7

  a2:m d4:7 f/g
  c2:5+ a:m/c
  f4/a bf:6/d d:dim a:m/c
  a:m/e e:5.9 a:m7+9 a:m

  s4 c2/d c4:maj7/e
  f2.:7maj9 c4/e
  d2:7 af:5-.7
  g2 g/b

  c2 c
  g2 g:7
  c4 c:6/g d:7/a g:7
  af:5+.7 g:7 d:m/c c

  a2 f4:5+.7maj/a a:7.9-
  d4:m s4 s4 d:7
  g2 ef4:5+.7maj/g g:7.9-
  c2:6 c:7maj9
  f4 f:5+ d:m/f f:7
  e4:7 g:7/d g:dim/c a:m/c
  f4:7maj/g g:7.9- g/a b:7/a
  e2 s

  a2:m a:m6
  e:7 e:7.9-
  a4:m a:m6/e b:7/fs e:7
  f2:6/g c4:5+ e:7

  a2:m d4:7 f/g
  c2:5+ a:m/c
  f4/a bf:6/d d:dim a:m/c
  a:m/e e:5.9 a:m7+9 a:m
}

sonatina_mu = \relative c {
  \global
  \key a \minor
  \clef bass

  \once \override Score.RehearsalMark.extra-offset = #'(0 . 3)
  \mark \jazzTempoMarkup "Hip-hop" c4 "70"

  r4 \f a8 a c8. a16 r4
  r4 e8. e16 r4 b'8 e,
  \break
  a8 r8 \p e'4 fs e
  g,16 a16 g8 c8 d c4 e8 e,

  \break
  r4 \f a8 a d4 g,8 c ~
  c2 c8. c16 r4
  \break
  a'8 r8 \p d2 c4
  e4 \f e, a2

  \break
  r4 \p d8. d16 r4 e4
  f2 f8 g16 e ~ e4
  \break
  d4 d,8 a af4 r
  g4 r b4 r

  \break
  r4 \f c8 c e8. c16 r4
  r4 g8. g16 r4 g8 a
  \break
  c8 r8 \p g'4 a g8 g,
  af4 \f g c8 c c4

  \break
  a'2 \p a8. a16 r4
  d,4 r r d16 d r8
  \break
  g2 g8. g16 r4
  c,4 ~ c16 g' a8 c2

  \break
  r4 f,8 f f8. f16 r4
  e8 \sf r d \sf r c8. \f c16 r4
  \break
  g8. g16 r4 a8. a16 r4
  e2 \p r

  \break
  r4 \f a8 a c8. a16 r4
  r4 e8. e16 r4 b'8 e,
  \break
  a8 r8 \f e'4 fs e
  g,16 a16 g8 c8 d c4 e8 e,

  \break
  r4 \f a8 a d4 g,8 c ~
  c2 c8. c16 r4
  \break
  a'8 r8 \p d2 c4
  << { e4 \f \rit e,, a2 \fermata \! } \\ { \redcross s4 s4 s4 b' ^\fermata } >>
  \bar "||"
  \pageBreak
}

\score {
  \header {
    piece = \markup \huge "Sonatina"
    opus = \markup \huge "G. Benda"
  }
  <<
    \new ChordNames { \sonatina_ch }
    \new Staff { \sonatina_mu }
  >>
}

% Holiday
% ========================================================================
holiday_ch = \chordmode {
  f2. f:maj7 f:6 a:m6
  d:m a:5+ d:m7 g:7
  g:m7 c:7 a:m7 d:m7
  g:m7 c:7 f:maj7 d:7

  g:m7 c:7 a:m7 d:m7
  d:m6.9 g:9 c c:7

  f2. f:maj7 f:6 a:m6
  d:m a:5+ d:m7 g:7
  g:m7 c:7 a:m7 d:m7
  g:m7 c:7 f:6 d:9

  g:m7 c:7 d:m7 g:7
  f/c s s s
  df:5+13+ s s s
  f f:5+ d:m/f f:5+
  f s f s
}

holiday_mu = \relative c {
  \global
  \time 3/4
  \key a \minor
  \clef bass

  \once \override Score.RehearsalMark.extra-offset = #'(0 . 3)
  \mark \jazzTempoMarkup "Waltz" c4 "150"

  f2. \p c f, a
  d a' d g,
  \break
  g, c f, d' \mf
  g, c a d
  \break
  g, c a d
  d g, _"Rit." << { c \fermata \! c' _"Rit." } \\ { s4 \teeny g8 a bf c } >>

  \break
  f,2. ^"a tempo" c f, a
  d a' d g,
  \break
  g, c a d
  g, c f, d'

  \break
  g, c d g, \rit
  c \! ^"a tempo" ~ c ~ c ~ c \rit
  \break
  df \! \accel ~ df ~ df ~ df
  f \! f, f' f,
  \break
  f' ~ f f,4 r2 r2. \fermata
  \bar "||"
  \pageBreak
}

\score {
  \header {
    piece = \markup \huge "Holiday in Paris"
    opus = \markup \huge "W. Gillock"
  }
  <<
    \new ChordNames { \holiday_ch }
    \new Staff { \holiday_mu }
  >>
}

% Jasmine Flower
% ========================================================================
jasmine_ch = \chordmode {
}

jasmine_mu = \relative c {
  \global
  \key g \major
  \clef bass

  \once \override Score.RehearsalMark.extra-offset = #'(0 . 3)
  \mark \jazzTempoMarkup "Gently" c4 "80"

  g1 ~ \f g
  g1 ~ \p g
  \break
  g'1 ~ \mf g
  d1 ~ \mf d2. e,4 ~
  \break
  e2. \< d'4 \! ~ d1
  g,1 d' \p
  g1 ^"Poco rit." d2 \fermata g,2\fermata
  \bar "||"
}

\score {
  \header {
    piece = \markup \huge "Jasmine Flower"
    opus = \markup \huge "Trad. Chinese"
  }
  <<
    \new ChordNames { \jasmine_ch }
    \new Staff { \jasmine_mu }
  >>
}

% Hot Toddy
% ========================================================================
toddy_ch = \chordmode {
  a1:7 s d:6 s
  a1:7 s d:6 s
  d:7 s g:7 s
  e:7 s a:7 s
  d:7 s g:7 s
}

toddy_mu = \relative c {
  \global
  \key g \major
  \clef bass

  \once \override Score.RehearsalMark.extra-offset = #'(0 . 3)
  \mark \jazzTempoMarkup "Medium Swing" c4 "100"

  \repeat volta 2 {
    a4. a8 e'4. e8
    a,4. a8 b4 cs
    d4. d8 a4. a8
    d4 cs b bf

    \break
    a4. a8 e'4. e8
    a,4. a8 b4 cs
    d4. d8 a4. a8
    d4 cs b a

  } \alternative {{
    \break
    d4. d8 a4. a8
    d4. d8 a4 d
    g,4. g8 b4. b8
    d4. d8 g,4 fs

    \break
    e4. e8 b'4. b8
    e,4 f fs gs
    a4. a8 e4. e8
    a8 a' g4 fs e

  }{
    \break
    d4. d8 a4. a8
    d4. d8 a4 d
    g,4. g8 d'4. d8
    g,4 a bf c

    \break
    cs4. cs8 e4. e8
    a,4. a8 b4 cs
    d4 r4 r2
    r2 d
    \eolMark \markup "(D.C.)"
    \bar "||"
    \pageBreak
  }}
}

\score {
  \header {
    piece = \markup \huge "Hot Toddy"
    opus = \markup \huge ""
  }
  <<
    \new ChordNames { \toddy_ch }
    \new Staff { \toddy_mu }
  >>
}

% Frère Jacques
% ========================================================================
jacques_ch = \chordmode {
  s1*5
  s1*4
  s1*4
  s1*6
  s1*5

  f1:7 s f:7 s
  f1:7 s f:7 s
  bf1:7 s f:7 s
  bf1 c:7
  c1:7 c1:7 c1:7 c1:7 c1:7
  g1:7/b c:7
  g1:7/b c:7
  g2:m9/bf c:7/bf f/a f:7/a
  g2:m9/bf c:7 f:6 f:6
}

jacques_mu = \relative c {
  \global
  \time 4/4
  \key f \major
  \clef bass

  \once \override Score.RehearsalMark.extra-offset = #'(0 . 3)
  \mark \jazzTempoMarkup "Rock" c4 "100"

  \mkup "(Only drums and piano)"
  \override NoteHead.style = #'cross
  c4. c8 ~ c4 c4
  c4. c8 ~ c4 c4
  c4. c8 ~ c4 c4
  c4. c8 ~ c4 c4
  c4 c4 c4 c4
  \revert NoteHead.style
  \eolMark \markup "5"

  \break
  R1*4
  R1*4
  R1*6

  \break
  c2. r4
  c2. r4
  c2. r4
  c2. r4
  \mkup "(Drum break/solo until band is prepared)"
  c1 \fermata
  \eolMark \markup "5"
  \bar "||"

  \break
  \mkup "(Full band on cue)"
  \mark \jazzTempoMarkup "" c2 "72"
  \repeat volta 2 {
    f,4 a c d
    ef d c a
    f a c d
    ef d c a

    \break
    f4 a c d
    ef d c a
    f a c d
    ef d c a

    \break
    bf d f g
    af g f d
    f, a c d
    ef d c a
  } \alternative {{
    bf f' bf b
    c bf a g

    \break
    c,4. c8 ~ c c c4
    c4. c8 ~ c c c4
    c4. c8 ~ c c c4
    c4. c8 ~ c c c4
    c4 c( d e)
  }{
    \break
    b d f g
    bf? g e c
  }}
  b d f g
  bf? g e c

  \break
  bf2 b
  a2 a
  bf2 c
  f4 -> r4
  f, -> r4
  \bar "||"
  \pageBreak
}

\score {
  \header {
    piece = \markup \huge "Frère Jacques"
    opus = \markup \huge ""
  }
  <<
    \new ChordNames { \jacques_ch }
    \new Staff { \jacques_mu }
  >>
}

% Haendel
% ========================================================================
haendel_mu = \relative c, {
  \time 4/4
  \key e \major
  \clef bass

  \override BreathingSign.text = \markup { \huge \musicglyph #"scripts.caesura.curved" }
  \once \override Score.RehearsalMark.extra-offset = #'(0 . 3)
  \mark \jazzTempoMarkup "March" c4 "88"

  e2 \p e' ds cs  gs fs \eolMark "3" e b' as gs ds' cs b a? \eolMark "4"
  \break
  gs fs e ds' cs b a? gs fs a \eolMark "5"
  \break
  b e b ds fs b, fs ds'4 e fs ^"Poco rit." b, fs'2 \eolMark "5"
  \break
  b, \breathe b' ^"A tempo" as gs ds cs \eolMark "3" b e ds cs gs fs e ds' \eolMark "4"
  \break
  cs b cs b a2. gs4 fs2 a \eolMark "4"
  \break
  b4 b b b b b gs a b2 gs4 a b ^"Poco rit." a b b e,2 \breathe e' ^"A tempo" \eolMark "5"
  \break
  a, fs' ds e cs ds e e, \eolMark "4"
  \break
  b' cs b a b e4 a, b2 e4 a, b2 gs4 a b4 e b b e,2 b' \eolMark "7"
  \break
  e1 \f cs a b e,2 bs' \p \eolMark "5"
  \break
  cs gs a es' fs cs d b cs es \eolMark "5"
  \break
  fs cs d as b fs' g e fs as, \eolMark "5"
  \break
  b fs gs ds'? e cs ds fss \eolMark "4" gs ds e b cs1 ~ \eolMark "3"
  \break
  cs1 ~ cs1 ~ cs ds2 e4 cs ds1 ^"Poco rit." \eolMark "5"
  \break
  gs,2 \breathe e' ^"A tempo" ds cs gs' fs e ds e b \eolMark "5"
  \break
  cs gs a a'4 gs fs2. e4 ds2. cs4 \eolMark "4"
  \break
  b2 a gs2. fs4 e2 cs' fs as \eolMark "4" b fs gs ds e b \eolMark "3"
  \break
  cs gs as es fs cs' ds as \eolMark "4" b fs gs ds' e b \eolMark "3"
  \break
  cs gs a e' fs e b' b, \eolMark "4"
  \break
  e1 ~ e1 a, ~ \eolMark "3" a ~ a ~ a b2. a4 \eolMark "4"
  \break
  gs2 e' ds2 cs gs fs e ds' \eolMark "4" e b cs gs a e' \eolMark "3"
  \break
  fs a, b fs gs fs e2. ds'4 \eolMark "4"
  \break
  cs2 gs e' gs fs1 ~ fs ~ \eolMark "4" fs \rit fs, ~ fs b \fermata \!
  \bar "||"
  \pageBreak
}

\score {
  \header {
    piece = \markup \huge "Che vai pensando"
    opus = \markup \huge "Haendel"
  }
  <<
    % \new ChordNames { \haendel_ch }
    \new Staff { \haendel_mu }
  >>
}

% My fair lady
% ========================================================================
lady_ch = \chordmode {
  bf1/f bf:dim/f bf1/f ef2/f f:7 ef2/f f:7 

  bf1:maj7 ef/f bf:maj7 ef/f
  bf1:maj7 d2:m cs:dim c1:m7 f:7
  c:m9 ef:m6 g:m/f c:7
  c:m7 f:5+9 bf1:maj7 ef/f

  bf1:maj7 g:5.9/b c:m9 ef/f
  bf2:maj7 g:5.9/b c:m7 cs:dim d:m7 df:11+ c:m7 g:7
  c1:m9 d:m7 ef:m7 e:m5-7 
  c1:5-9 f:9 bf:maj a:m7

  d1:7 c2/e fs:dim ef1:6/g ef:6
  ef1:m6 c:5-7/e bf:6/f c:11+
  ef1/a d/a e:m7/a fs:m/a
  d2:11 c:11 b:11 bf:11 b:m7 c:m7 f1:7

  bf1:maj7 g:5.9/b c:m9 ef/f
  bf2:maj7 g:5.9/b c:m7 cs:dim d:m7 df:11+ c:m7 g:7
  c1:m9 af:11+ g:m7 c:7
  c1:m7 f:9-11+ b:maj7 s
} 

lady_mu = \relative c {
  \time 4/4
  \key bf \major
  \clef bass

  \override BreathingSign.text = \markup { \huge \musicglyph #"scripts.caesura.curved" }
  \once \override Score.RehearsalMark.extra-offset = #'(0 . 3)
  \mark \jazzTempoMarkup "Swing" c4 "160"

  f1 f, f' f, f1 \fermata 

  \break
  bf2 bf 
  f' f,
  bf bf 
  f' f,

  \break
  bf bf4 c
  d2 cs
  c d4 ef
  f2 f,

  \break
  c' c4 d
  ef2 ef' 
  f f,4 g
  c,2 c'

  \break
  c, c'4 c,
  f2 f,
  bf d4 ef
  f2 f,

  \break
  bf bf
  b b
  c d4 ef
  f2 f,

  \break
  bf b
  c cs
  d df
  c g

  \break
  c c
  d c4 d
  ef2 ef
  e f4 g

  \break
  c,2 c'4 c,
  f2 f,
  bf bf'
  a a,

  \break
  d d'
  e fs
  g ef4 bf
  ef,2 ef'

  \break
  ef, ef
  e g
  f d4 bf
  c2 c4 bf

  \break
  a2 a'
  a, a'
  a, a'
  a, a'

  \break
  d, c
  b bf
  b c
  f1

  \break
  bf,2 bf
  b b
  c d4 ef
  f2 f,

  \break
  bf b
  c cs
  d df
  c g

  \break
  c bf
  af1 \fermata
  g2 g'4 d
  c2 c'

  \break
  c, g'
  f f' \fermata
  bf, f
  bf,1 \fermata
  \bar "||"
  \pageBreak
}

\score {
  \header {
    piece = \markup \huge "On the stree where you live"
    opus = \markup \huge "F. Loewe"
  }
  <<
    \new ChordNames { \lady_ch }
    \new Staff { \lady_mu }
  >>
}


% Amor
% ========================================================================
amor_ch = \transpose c d \chordmode {
  c4. cs8:dim s4 d4:m7 s4. g8:7 s4 e:m7
  s4. a8:5+ s4 d:m7 s4. g8:7 s2
  c4. cs8:dim s4 d4:m7 s4. g8:7 s4 e:m7
  s4. a8:5+ s4 d:m7 s8 af:9 s g:13 s2

  c2. c4:maj7 s2. c4:6
  s2. c4 s2. c4
  s2. c4:6 s2. d4:m7
  s2 g4. f8:5.9/a s8 fs:13 s g:13 s2

  d2.:m d4:m7+ s2. d4:m7
  s2. g4 s2. f4/g
  s2. g4 s1 c2. s8 fs:m11 s4. b8:5+7 s2

  e2.:m b4:9 s4. b8:7 s4 b4:9
  s4. b8:7 s4 e4:m s2 a4:m7 d:7
  g4. e8:7/gs s4 a:m s4. d8:7 s4 a4:m7
  s4. d8:7 s2 g2 f4/g g:6

  c2. c4:maj7 s2. c4:6
  s2. c4 s2. c4
  s2. c4:6 s2. d4:m7
  s2 g4. f8:5.9/a s8 fs:13 s g:13 s2


  d2.:m d4:m7+ s2. d4:m7
  s2. g4 s2. f4/g
  s2. g4 s1

  c2. af4:7 s2. c4 s2. af4:7 s2. c4
  s4. b8:13 s4. af8:5+7/bf s4. g8:sus9 s2
  c1:5.9
  % s1*19
}

amor_mu = \transpose c d \relative c {
  \key a \minor
  \clef bass

  \once \override Score.RehearsalMark.extra-offset = #'(0 . 3)
  \mark \jazzTempoMarkup "Salsa" c4 "142"

  c4. cs8 ~ cs4 d ~
  d4. g,8 ~ g4 e' ~
  e4. a,8 ~ a4 d ~
  d4. g,8 ~ g4 g
  \break
  c4. cs8 ~ cs4 d ~
  d4. g,8 ~ g4 e' ~
  e4. a,8 ~ a4 d ~
  d8[ af] ~ af[ g] ~ g2

  \break
  \bar "[|:"
  \repeat volta 2 {
    c4. g'8 ~ g4 g, ~
    g4. g'8 ~ g4 c, ~
    c4. g'8 ~ g4 g, ~
    g4. g'8 ~ g4 c, ~
    \break
    c4. g'8 ~ g4 g, ~
    g4. g'8 ~ g4 d ~
    d2 g,4. a8
    r8 fs r8 g8 ~ g4 a

    \break
    d4. a'8 ~ a4 a, ~
    a4. a'8 ~ a4 d, ~
    d4. a'8 ~ a4 g, ~
    g4. g'8 ~ g4 g, ~
    \break
    g4. g'8 ~ g4 g, ~
    g4. g'8 ~ g4 g,
    c2 c4. fs,8
    r4. b8 ~ b2

    \break
    e4. b'8 ~ b4 b, ~
    b4. b'8 ~ b4 b, ~
    b4. b'8 ~ b4 e, ~
    e2 a4 d,
    \break
    g4. gs8 ~ gs4 a ~
    a4. d,8 ~ d4 a' ~
    a4. d,8 ~ d4 d
    g2 g4 g
  }
  \bar ":|]"

  \pageBreak
  c,4. g'8 ~ g4 g, ~
  g4. g'8 ~ g4 c, ~
  c4. g'8 ~ g4 g, ~
  g4. g'8 ~ g4 c, ~
  \break
  c4. g'8 ~ g4 g, ~
  g4. g'8 ~ g4 d ~
  d2 g,4. a8
  r8 fs r8 g8 ~ g4 a

  \break
  d4. a'8 ~ a4 a, ~
  a4. a'8 ~ a4 d, ~
  d4. a'8 ~ a4 g, ~
  \break
  g4. g'8 ~ g4 g, ~
  g4. g'8 ~ g4 g, ~
  g4. g'8 ~ g4 g,

  \break
  c4. g'8 ~ g4 af, ~
  af4. gf'8 ~ gf4 c, ~
  c4. g'8 ~ g4 af, ~
  af4. gf'8 ~ gf4 c, ~
  \break
  c4. b8 ~ b4. bf8 ~
  bf4. g8 ~ g2
  c1 ~ c1 \fermata
  \bar "||"
}

\score {
  \header {
    piece = \markup \huge "Amor, amor, amor"
    opus = \markup \huge "G. Ruiz"
  }
  <<
    \new ChordNames { \amor_ch }
    \new Staff { \amor_mu }
  >>
}
