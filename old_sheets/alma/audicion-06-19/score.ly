\version "2.18.2"
\language "english"

% Se debe incluir antes que lilyjazz. Si no, lilyjazz no funciona.
#(set-global-staff-size 19)

\include "main.ily"

\header {
	title = "Audición ALMA 08-06-19"
	instrument = ""
	composer = ""
	arranger = "gbpsms@gmail.com"
	poet = ""
	tagline = ""
}

\paper {
}

global = {
	\compressFullBarRests

	% Arregla el problema de tener el scoreMarkup añadido
	\revert MultiMeasureRest #'staff-position
	\override Voice.Rest #'staff-position = #0

	\set Timing.beamExceptions = #'()
	\set Timing.baseMoment = #(ly:make-moment 1/4)
	\set Timing.beatStructure = #'(1 1 1 1)

	\override ParenthesesItem.font-size = #0
}

% Mi barba 
% ========================================================================
mi_barba_mk = {
	\once \override Score.RehearsalMark.extra-offset = #'(0 . 3)
  \mkup "Mi barba"
	\mark \jazzTempoMarkup "Afro-cuban" c4 "120"
  s2.
  s2.*16
}

mi_barba_ch = \chordmode {
  s2.
  c2. | a:7 | d:m | d:m7
  g | g:7 | c | c:6/g
  c2. | c:7/bf | d:m/a | af:m7.5-
  e:m/g | g:7 | c | c:6
}

mi_barba_mu = \relative c'' {
  \time 3/4
	\key c \major

  r4 r4 g(

  \bar "||"
  c2 g4 | g f e | f d2~ | d) g4(
  b2 g4 | f e d | e c2~ | c) g'4(
  c2 g4 | g f e | f d2~ | d) g4_(
  b2 g4 | f e d | e c2~ | c2.) \fermata

  \bar "||"
  \break
}

% Calypso lento (repetir segunda parte)
% ========================================================================
calypso_ch = \chordmode {
  f1 | g:m | c:7 | f2 c:5+
  f1 | g:m | c:7 | f1

  bf1 | f/a | g2:m c:7 | f1
  bf1 | f/a | c:7 
  f
  f
}

calypso_mu = \relative c' {
  \time 4/4
  \key f \major

  f4 f8 f ~ f a f4-. | g8( bf4)-. d,8 ~ d2
  e4 e8 e ~ e g e4-. | f8( a4)-. c8 ~ c2
  f,4 f8 f ~ f a f4-. | g8( bf4)-. d,8 ~ d4. f8
  e4 bf'8 bf ~ bf a g4-. | f2. r4

  \bar "[|:"
  \repeat volta 2 {
    d'4 d8 d ~ d e d4-. | c8( f4)-. a,8 ~ a2
    bf8( d4)-. g,8 ~ g2 | a8( c4)-. f,8 ~ f2
    d'4 d8 d ~ d e d4-. | c8( f4)-. a,8 ~ a2
    c4 c8 bf ~ bf a g4-. 
  }
  \alternative {{
    f2. r4 
    \bar ":|]" 
  }{
    f1 \fermata
  }}
  \bar "||"
  \break
}

calypso_lento_mk = {
  \mkup "Calypso Joe (lento)"
	\mark \jazzTempoMarkup "Calypso" c4 "70"
  s1*8
  s1*9
}

calypso_lento_mu = \transpose f d \calypso_mu
calypso_lento_ch = \transpose f d \calypso_ch

calypso_rapido_mk = {
  \mkup "Calypso Joe (rapido)"
	\mark \jazzTempoMarkup "Calypso" c4 "120"
  s1*8
  s1*9
}

calypso_rapido_mu = \calypso_mu
calypso_rapido_ch = \calypso_ch

% Harry Potter
% ========================================================================
harry_potter_mk = {
  \mkup "Harry Potter"
	\mark \jazzTempoMarkup "3/4 Funk" c4 "120"

  s2.

  s2.*16
  s2.*16
}

harry_potter_mu = \relative c' {
  \time 3/4
  \key e \minor

  r2 b4

  \bar "||"
  e4. g8 fs4 | e2 b'4 | a2. | fs2.
  e4. g8 fs4 | ds2 f4 | b,2.~ | b2 b4
  e4. g8 fs4 | e2 b'4 | d2 df4 | c2 af4
  c4. b8 as4 | as,2 g'4| e2.~ | e2 g4

  b2 g4 | b2 g4 | c2 b4 | as2 fs4 |
  g4. b8 as4 | as,2 b4 | b'2.~ | b2 g4 |
  b2 g4 | b2 g4 | d'2 df4 | c2 af4
  c4. b8 as4 | as,2 g'4| e2.~ | e2. \fermata
}

harry_potter_ch = \chordmode {
  s2.
  e:m | e:m | f/e | b:7/e
  e:m | b:5-.7maj | e:m | e:m/b
  e:m | e:m | g:m | f:m 
  a:m/e | fs:9-/b | e:m | e:m

  e:m | e:m/b | c/e | fs/c
  e:m | fs:7 | e:m/g | b/fs
  e:m | e:m/b | g:m/bf | f:m/af
  c:7/g | fs:9- | e:m | s2.
  \bar "||"
  \break
}
 
% Kwela
% ========================================================================
kwela_mk = {
  \mark "Bass score"
  \mkup "Kwela for Caitlin"
	\mark \jazzTempoMarkup "Kwela Swing (Shuffle)" c4 "90"

  s1*8
  s1*8
  s1*8
  s1*10
  s1*7
}

kwela_mu = \relative c {
  \time 4/4
  \key d \major
  \clef bass

  d4 \f fs a fs | g b d b | a cs e cs | d r4 r2
  d4 \p c b a | g fs e d | a' g fs e | d r4 r2

  \improOn
  \repeat percent 4 { r4 \f r4 r4 r4  }
  \repeat percent 2 { r4 \p r4 r4 r4  }
  r4 \f r4 r4 r4
  \improOff
  b8 c cs d-> r2

  b4 \p d fs d | e g b g | fs as cs as | b r4 r2
  b4 a? gs e | a g fs d | g g fs as, | b r4 r2

  g4. \f d'8 r2 | fs,4. b8 r2 | g4. fs8 r2 | e4. a8 r2
  g4 b d e | gs as8 b r2 | d,4. d8 r2 | d4. d8 r2 
  g,4. \p d'8 r2 | a'8 c cs d r2 

  d,4 \f fs a fs | g b d b | a \mf a a a | a' \p a, a' a,
  a' r4 r2 | r4 c,,2. \ff | r4 d2.-> \fermata
  \bar "||"
  \break
}

kwela_ch = \chordmode {
  d1 | g | a | d1
  d1:7 | g | a:7 | d1
  g2 d | a d | g d/fs | e:7 a:7
  g d/fs | fs:7 b:m | e:m:7 d/fs | a4.:9 d8 s2 

  b1:m | e:m | fs:7 | b:m
  b2:m7 e:7 | a:7 d:7 | g fs:7 | b1:m
  g4. d8 s2 | fs4.:7 b8:m s2 | g4. d8/fs s2 | e4.:7 a8:7 s2 
  g2 d | fs4.:9 b8:m s2 
  g4./d d8 s2 | g4./d d8 s2 | g4. d8 s2 | a4.:7 d8 s2

  d1 | g | a | a | a 
  s4 c2.:5.9 | s4 d2.:5.9
}
 
% Dusty blue
% ========================================================================
dusty_blue_mk = {
  \mkup "Dusty Blue"
	\mark \jazzTempoMarkup "Bluesy" c4 "76"

  s1
  s1*12
}

dusty_blue_mu = \relative c' {
  \clef treble
  \key g \major
  \time 4/4

  r2 \times 2/3 { r8 d( d } \times 2/3 { e d e }

  \bar "||"
  g4) r4 \times 2/3 { r8 d( d } \times 2/3 { e d e }
  g4) r4 r8 d( e[ d]
  g4 a b d)
  r8 f(--~ f[ e] d4) \times 2/3 { d8( e d }

  g4) r4 r8 f( d[ c]
  bf-- g) r4 r4 r8 g
  b(-- d) r4 r4 e8( d
  f2-- d4) r

  r8 a'(~ a[ g] f d) r4
  r8 g(~ g[ f] d4 c
  b8 g) r4 \times 2/3 { f'8(^"molto rit." g d } \times 2/3 { df c bf }

  g1) \fermata
  \bar "||"
  \break
}

dusty_blue_ch = \chordmode {
  s1
  g:6 | s | s | s 
  c:6 | s | g:6 | g
  d:6.9+ | c:6 | g:6 | g4 af:7 g2:6
}
 
% Toccatina
% ========================================================================
toccatina_mk = {
  \mark "Focus on rhythm"
  \mkup "Toccatina"
	\mark \jazzTempoMarkup "Old School Funk" c4 "110"

  s1
  s1*6
  s1*6
  s1*5
  s1*8
}

toccatina_mu = \relative c' {
  \time 4/4
  \key a \minor

  r2 r4 r8 e( \p
  \bar "||"
  a4. c8 b4 e,) | a4.( c8 b4 e,) | a( \< b c d)
  \bar "||"
  e4.( \mf f8) e4.( f8) | d4.( e8) d4.( e8) | c4 \> d b c
  \bar "||"
  \break

  a4.( \p c8 b4 e,) | a4.( c8 b4 e,) | a( \< b c d)
  \bar "||"
  ef4.( \f f8) ef4.( f8) | gf4.( f8) gf4.( f8) | ef4 f df ef
  \bar "||"
  \break

  c4.( df8) c4.( df8) | b?4.( c8) b4.( c8) 
  bf4.( a8) bf4.( a8) | bf4 \> e, bf' e, | bf' e, bf' e,( 
  \bar "||"
  \break

  a4. \p c8 b4 e,) | a4.( c8 b4 e,) | a g f e | d e c' b
  a4.( \> e8) a4.( e8) | a4 \pp e a e | a r e r | a,-. \pp r4 r2
  \bar "||"
  \break
}

toccatina_ch = \chordmode {
  s1
  a2:m e:m7 | a:m e:m7 | a1
  e:m | d:m | c4 d:m e2:m

  a2:m e:m7 | a:m e:m7 | a1
  ef:m | gf:m | ef4:m f:m df ef:m

  c1 | b | bf
  bf4 e:7 bf e:7 | bf e:7 bf e:7

  a2:m e:m7 | a:m e:m7 | a4:m g f e:m | d:m e:m c e:m
  a1:m | a4:m e:m a:m e:m | a2:m e:m | a1:m
} 
 
% Entertainer (hasta primer pentagrama tercera página)
% ========================================================================
entertainer_mk = {
  \mkup "The Entertainer"
	\mark \jazzTempoMarkup "Afro-cuban" c4 "120"

  s1*4
  s1*16
  s1*16
  s1*11
  s1*4
}

entertainer_mu = \relative c''{

  \override NoteHead.stencil = #jazz-slashednotehead
  g1 \f | g1 | g1 | g4-. r g-> r
  \break
  \bar "||"

  \mark \segno
  r8 \p g-. r4 g4-. r8 g-> ~ | g2 r2 \< | g4. \f g8~ g4 g4-. | g2. \> r4
  r8 \p g-. r4 g4-. r8 g-> ~ | g2 r2 \< | g4. \mf g8~ g2 | g2. \> r4
  r8 \p g-. r4 g4-. r8 g-> ~ | g2 r2 \< | g4. \f g8~ g4 g4-. | g2. \> r4
  g4. \p g8~ g2 | g4. \< g8~ g2 | g4. \f g8~ g4 g4-. | g2 \> r2 \p \eolMark \toCoda

  \break
  \bar "[|:"
  \repeat volta 2 {
    g4. g8~g2 | g4. g8~g2 | s1 \mf | s1 \>
    g4. \p g8~g2 | g4. g8~g2 | g8 \f g4-. g8-> ~ g4 g | s1 \>
    g4. \p g8~g2 | g4. g8~g2 | s1 \mf | s1 \>
    g4 \p g g g | g g g g | g-. g-. g8 g4-. g8-> ~ | g2 r2 \eolMark "after 2nd time" \eolMark \dalSegnoAlCoda
  }

  \break
  \mark \coda
  \bar ":|][|:"
  \repeat volta 2 {
    g4 \f g2 g4-. | s1 | g4 g2 g4-. | s1
    g4. \mf g8 ~ g4 g-. | 
  }
  \alternative {{
    g2 g2 | g4. \p g8 ~ g2 | g2 \< g2 
    \bar ":|]"
  }{
    g2 \! g2 | g4. g8 ~ g2 | g4-. r g4-> r
  }}

  g4 \p g g g | g \< g g g | g-. \f g-. g8 g4-. g8-> ~ | g2 g4-> r
  \revert NoteHead.stencil
  \bar "||"
  \break

  % d8( \g e c a~ a b g4-.) | d'8( e c a~ a b g4-.) | d'8( e c a~ a b a ag | g4-.) r g'-> d,8( ds

  % e8 \p c'4) e,8( c'4) e,8( c'~ | c2) r8 c(\f d[ ds] | e c d e~ e b d4-.) | c2. d,8(\p ds
  % e8 c'4) e,8( c'4) e,8( c'~ | c2) r4 a8(\mf g | fs a c e~ e d c a | d2.) d,8(\p ds
  % e8 c'4) e,8( c'4) e,8( c'~ | c2) r8 c(\f d[ ds] | e c d e~ e b d4-.) | c2. c8(\p d
  % e8 c d e~ e) c(\< d c | e c d e~ e) c( d c | e\f c d e~ e b d4-.) \eolMark \toCoda | c2 r8 e,8(\p f[ fs]
  %
  % \bar "[|:"
  % \repeat volta 2 {
  %   g4 a8 g~ g) e( f fs | g4 a8 g~ g) e'(\mf c g | a b c d e d c d | g,) e'( f g a g) e,(\p f
  %   g4 a8 g~ g) e( f fs | g4 a8 g~ g) g(\< a as | b8\f b4-. b8~ b a fs d | g2) r8 e(\p f[ fs]
  %   g4 a8 g~ g) e( f fs | g4 a8 g~ g) e'(\mf c g | a b c d e d c d | c2) r8 g(\mf fs[ g]
  %   c4-.) a8( c~ c) a( c a | g c e g~ g) e( c g | a4-.) c-. e8( d4-.) c8~ | 
  % }
  % \alternative {{
  %   c2 r8 e,8\p f[ fs]
  % \bar ":|]"
  % }{
  %   c'2. d,8 ds
  %   \eolMark \dalSegnoAlCoda
  % }}
  % \bar "||"
  % \break
  %
  % \mark \coda
  % c'2. r4
  %
  % \repeat volta 2 {
  %   a8( gs a2 c4-. | d1) | f,8( e f2 a4-. | bf2.) r8 g(
  %   d4 g8 d~ d g d4-.) | c2( f) | e8( gs b e~ e d b c | a2 bf)
  %   a8( gs a2 c4-. | d1) | f,8( e f2 a4-. | bf2.) r8 g(
  %   d4 g8 d~ d g d4-.) | c2( f4.) f8( | a c4-. g8~ g c, d e
  % }
  % \alternative {{
  %   f4-.) b,8( c d e f g)
  % }{
  %   f4-. r f4-. r
  % }}
  % \break
  %
  % c'4( a8 c~ c) a( c a | g c e g~ g) e( c g | a4-.) c-. e8( d4-.) c8~ | c4 r c4-. r
} 

entertainer_ch = \chordmode {
  g1 | s1 | s1 | g:7
  c2 c:7/bf | f/a c/g | c/g g:7 | c g:7
  c2 c:7/bf | f/a c/g | d/fs d | g1:7
  c2 c:7/bf | f/a c/g | c/g g:7 | c1
  c2 c:7/bf | f/a f:m/af | c/g g:7 | c1

  c1 | s1 | f2 f:m | c/e g:7 
  c1 | s1 | g2/d d:7 | g1:7
  c1 | s1 | f2 f:m | c/e c:7
  f2 fs:5- | c1/g | d2:7 g:7 | c1 

  f1 | bf | d:m | g:m
  g2:m/bf g:m | 
  f2/a d:m | e1:7 | a2:m c:7/g | 
  f2 df:m7 | f2 c:7 | f1

  f2 fs:5- | c1/g | d2:7 g:7 | c1
}
 
% Alfabeto (suelen retrasar)
% ========================================================================
alfabeto_mk = {
  \mkup "Alfabeto"
	\mark \jazzTempoMarkup "Afro-cuban" c4 "135"
  s2.*16
  s2.*16
}

alfabeto_mu = \relative c' {
  \time 3/4
  \key d \major

  d2. \mf | e | fs \< | g 
  a4 \f d b | a8 a d4 b | a g fs | e2.
  a8 a a4 fs | g e fs | a8 \p a a4 fs | g e fs
  fs \< g a | b cs d \f | cs8 \pp \> b a g fs e | d2. \!

  \break
  \bar "||"
  d2. \mf | e | fs \< | g 
  a4 \f d b | a8 a d4 b | a g fs | e2.
  a8 a a4 fs | g e fs | a8 \p a a4 fs | g e fs
  fs \< g a | b ^"molto rit." cs d \f \fermata | cs8 \pp \> b a g fs e | d2. \!
  \bar "||"
  \break
}

alfabeto_ch = \chordmode {
  d2. | a/cs | d | a:7
  d2 g4 | d2/fs e4:m7 | d2 b4:m | a2.
  d2. | a2:7 d4 | d2/fs d4 | a2:7 d4
  d2.:7 | g | a:7 | d

  d2. | c | b:m7 | a:7
  d | e:m11 | d2/fs g4:7 | a2.
  d2. | a2:7 d4 | bf2.:7.5+ | a2:7 d4
  d2.:7 | g4 fs:m e:m7.5- | a2.:7 | d
} 
 
% Gershwin
% ========================================================================
gershwin_mk = {
  \mkup "Nice work if you can get it"
	\mark \jazzTempoMarkup "Swing" c4 "120"

  s1*4
  s1*20
  s1*16
  s1*8
  s1*8
  s1*2 s1*2
}

gershwin_mu = \relative c'{
  \time 4/4
  \key g \major

  R1*2

  R1
  R1

  r8 \p e g d e b d e | g4 g g8 b4. | r8 g4 d8 e b d e | g a \times 2/3 { b a b } g a4.
  b8 b4 b8 g4 g | d'4 d b2 | r8 a4 fs8 a a a a | a b4 g8 fs4 a | e1 | r4 d' as fs
  r8 e g d e b d e | g4 g g8 b4. | r8 g4 d8 e b d g | b4 \< b b8 cs4.
  r8 \! d4 d8 b4. b8 | d4 d b8 cs4. | b8 b b4 b b | b4 b a b | g8 g4. ~ g2 ~ | g4 r4 r2

  \break
  \bar "[|:"
  \repeat volta 2 {
    b4 \mf c c b | a b2. | g4 a a g | fs1
    d4. e8 ~ e d e g | a b4. r8 d, e g | a b4. g4 g8 g ~ | g2. r4
    b4 c c b | a b2. | g4 a \times 2/3 { a gs g } | fs1
    d4. e8 ~ e d e g | a b4. r8 d, e g | a b4. g4 g8 g ~ | g2. r4

    r8 \mp b4 g8 b4 g | as4. g8 ~ g2 | b8 b b, b e g4. | fs1
    r8 a4 f8 a4 f | a4. a8 ~ a4 a8 b | d4 d d8 e b4 | as1

    b4 \p c c b | a b2. | g4 a \times 2/3 { a gs g } | fs1
    d4. e8 ~ e d e g | a b4. r8 d, e g | a \< b4. ~ b2 | d4 \f b g e
  }
  \alternative {{
    g2. r4 | s1
    \bar ":|]"
  }{
    g1 ~ | g2 ~ g4 r
  }}

  \break
  \bar "||"
}

gershwin_ch = \chordmode {
  s1*2

  g1:6 | s1

  g1 | e4:m7 a:7 a:m7 d:13 | g1 | e2:m7 a4:m7 d:7
  b2:5+ e:m | a4:m7 d:7 g:7maj g:6 | d2 bf:7 | e:m7 a4:13 a:7 | d1:9 | d2:9+ d:7.5+
  g1 | e4:m7 a:7 a:m7 d:13 | g1 | a2:9 cs4:m7 fs:7
  b2:m b:m7 | b:dim e:m6 | b:m e:7.9- | a:m9 c4:m6/d d:13 | g1:6 | s2 e4:m7 fs:m7

  b2:7.5+ e:9 | a:7.5+ d:9 | g:13 c:9 | a1:9 
  g2 g:6 | a:m7 g/b | c4:6 g e:dim d:7sus4 | g1
  b2:7.5+ e:9 | a:7.5+ d:9 | g:13 c:9 | a1:9 
  g2 g:6 | a:m7 g/b | c4:6 g e:dim d:7sus4 | g1

  e1:m | d2:5+/c c:9 | e:m e:m7 | a1:13
  d1:m | e2 a:7.5+ | d1 | fs:7

  b2:7.5+ e:9 | a:7.5+ d:9 | g:13 c:9 | a1:9 
  g2 g:6 | a:m7 g/b | b:7.5- e:7 | a:m9 d4:11 d:9+
  g2 e:m7 | a:m e:7.5+ | g4 a:7+ ef:7+ d:7+ | g1:6.9
}
 
% Haendel (ceder y esperar en los 3 puntos pactados + compas 5 pagina 2)
% ========================================================================
haendel_mk = {
  \mkup "Aria de Haendel"
	\mark \jazzTempoMarkup "Fast Swing" c4 "200"
  s1*24
  s1*34
  s1*34
  s1*32
}

haendel_mu = \relative c, {
  \time 4/4
  \key e \major
  \clef bass

  << \relative c, {
    \oneVoice
    e2 \p e' ds cs | gs fs e b' | as gs ds' cs
    b a? gs fs | e ds' cs b | a? gs fs a
    b e b ds | fs b, fs ds'4 e | fs b, fs'2 b, b'
    as gs ds cs | b e ds cs | gs fs e ds'

    cs b cs b | a2. gs4 fs2 a | b4 b b b b b gs a
    b2 gs4 a b a b b | e,2 e' a, fs' | ds e cs ds 
    e e, b' cs | b a b e4 a, | b2 e4 a, b2 gs4 a
    b4 e b b e,2 b' | e1 \f cs | a b | e,2 bs' \p cs gs
    a es' fs cs | d b cs es | fs cs d as | b fs' g e

    fs as, b fs | gs ds'? e cs | ds fss gs ds | e b cs1 ~
    cs1 ~ cs1 ~ | cs ds2 e4 cs | ds1 gs,2 e' | ds cs gs' fs
    e ds e b | cs gs a a'4 gs | fs2. e4 ds2. cs4
    b2 a gs2. fs4 | e2 cs' fs as | b fs gs ds
    e b cs gs | as es fs cs' | ds as b fs

    gs ds' e b | cs gs a e' | fs e b' b,
    e1 ~ e1 | a, ~ a ~ | a ~ a | b2. a4 gs2 e' 
    ds2 cs gs fs | e ds' e b | cs gs a e'
    fs a, b fs | gs fs e2. ds'4 | cs2 gs e' gs
    fs1 ~ fs ~ | fs fs, ~ | fs b \fermata

  } \\ \relative c, {
    \stemDown
    \override BreathingSign.text = \markup { \huge \musicglyph #"scripts.caesura.curved" }
    \override NoteHead.stencil = #jazz-slashednotehead
    s1 | s1 | s1 \bar "||" | s1 | s1 | s1 
    s1 \break \eolMark "4" \bar "||" | s1 | s1 | s1 | s2 s8 ^"straight" s s s | s s s s s s s s \break \eolMark "5" \bar "||" 
    s1 ^"swing" | s1 | s1 | s1 | s4 ^"poco rit." s8 s s4 s8 s \break \eolMark "5" \bar "||" | s4 \breathe s ^"a tempo" s2 
    s1 | s1 \bar "||" | s1 | s1 | s1 | s1 \break \eolMark "4" \bar "||" 

    s1 | s1 | s2. s8 ^"straight" s | s s s s s s s s \break \eolMark "4" \bar "||" | s1 ^"swing" | s1 
    s1 | s4 ^"poco rit." s8 s s4 s8 s | s4 \breathe s4 ^"a tempo" s2 \break \eolMark "5" \bar "||" | s1 | s1 | s1
    s8 ^"straight" s s s s s s s \break \eolMark "4" \bar "||" | s1 ^"swing" | s1 | s1 | s1 | s1 
    s1 | s1 \break \eolMark "7" \bar "||" | s1 | s1 | s1 | s1 | s1 \break \eolMark "5" \bar "||" | s1  
    s1 | s1 | s1 | s1 \break \eolMark "5" \bar "||" | s1 | s1 | s1 | s1  

    s1 \break \eolMark "5" \bar "||" | s1 | s1 | s1 | s1 \bar "||" | s1 | s1 | s1 \break \eolMark "3" \bar "||" 
    s1 | s1 | s1 | s1 |  s2 ^"poco rit." s4. s8 \break \eolMark "5" \bar "||" | s4 \breathe s4 ^"a tempo" s2 | s1 | s1  
    s1 | s1 \break \eolMark "5" \bar "||" s1 | s1 | s1 | s1 \break \eolMark "4" \bar "||" | s1 | s1  
    s1 | s1 \bar "||" | s1 | s1 | s1 \break \eolMark "3" \bar "||" | s1

    s1 | s1 | s1 \bar "||" | s1 | s1 | s1 \break \eolMark "3" \bar "||" 
    s1 | s1 | s1 | s1 \break \eolMark "4" \bar "||" | | s1 | s1 | s1 \bar "||" | s1
    s1 | s1 | s1 ^"straight" \break \eolMark "4" \bar "||" | s1 ^"swing" | s1 | s1 | s1 \bar "||" 
    s1 | s1 | s1 \break \eolMark "3" \bar "||" | s1 | s1 | s1 
    s1 | \break \eolMark "4" \bar "||" | s1 | s1 | s1 | s1 \bar "||" | s1 ^"ralentando" s1 | s1 | s1
  } >>

  \bar "||"
  \break
}

haendel_ch = \chordmode {

  e1 b2 cs:m | e b:7 e b | fs gs:m b fs:7
  b b:7 e b:7 | e gs:m cs:m e | a e fs1:m
  b2:7 e b1 | fs2 b fs b | fs1 b
  fs2 gs:m b fs:9 | b e b cs:m | e b e b

  cs:m e cs:m gs:m | a1 fs:m | b2 e/b b e/b 
  b2 e b1:9 | e a2:7maj a:6 | gs:m7 e fs:m7 b
  e1:7maj b2:6 a:6 | b:6 a:6 b:6 a:7 | b:6 a:7 b:6 fs:m9
  b4 e b2:7 e1 | e cs:m | a b:7 | e2 gs:7 cs:m gs:7 
  a cs:7.9- fs:m cs:7 | d b:m6 cs1 | fs2:m cs d fs:7 | b:m fs:7 g e:m7

  fs1:7 b2 fs:7 | gs:m b:7maj e cs:m | ds1 gs2:m ds:7 | e b:7maj cs1:m
  cs1:m cs:m6 | cs:dim ds2:7 cs:m7 | gs:m ds:m7 gs:m e | b cs:m e b:9
  e b e b | cs:m e:7 a1 | fs b2 fs:7 | b b:7 e b | e cs:m fs1 | b2 fs:7 gs:m b:7
  e2 gs:9/b cs2:m e:7maj | fs cs:9 fs1 | ds2:m fs b fs:7

  gs:m b:7 e b | cs e:7 a e | fs:m e b1
  e1. e2:7maj | a1 s | a:6 fs:m | b1 e
  b2 cs:m e b | e b e b | cs:m e a e:6
  fs2:m a:7maj b2 fs:7 | gs:m b e2. b4 | cs2:m e cs1:m
  fs1:7 s | b1 s | b2 fs:7 b1
}
 
% Rodrigo
% ========================================================================
rodrigo_mk = {
  \mark "Bass & Lead score mixed"
  \mkup "De donde venís, amore"
	\mark \jazzTempoMarkup "" c4 "90"

  s1*2
  s1 s4 s1*2

  s1*2
  s1 s4 s1*4

  s1*2
  s1*5

  s1 s4 
  s1 s4
  s1*3
}

rodrigo_mu = \relative c' {
  \key f \major
  \time 4/4

  \clef bass
  c8-. \p r c8-. r c8-. c8-. c4-. | f,8-. \mf c-. d-. a-. \> bf-. c-. f,4-. \!

  \break \time 5/4 \clef treble \bar "||" \teeny
  f'''4 ( \p c8 a bf g c4 a8) r
  \time 4/4
  f'8-. c-. d-. a( bf c f,8) r | \clef bass \bar "||" \normalsize r4 r8 a,,-. \> bf-. c-. f,4-. \!

  \break 
  c''8-. \p r c8-. r c8-. c8-. c4-. | f,8-. \mf c-. d-. a-. \> bf-. c-. f,4-. \!

  \break \time 5/4 \clef treble \bar "||" \teeny
  d'''4 ( \p d8 cs d a c4 f,8) r
  \time 4/4
  f'8-. r g-. r g8-. c-. a-. r | f-. a-. f-. a-. g-. c-. a-. r

  \break
  f8-. c-. d-. a( bf c f,8) r | \clef bass \bar "||" \normalsize r4 r8 a,,-. \> bf-. c-. f,4-. \!
  \break
  c''8-. \p r c8-. r c8-. c8-. c4-. | f,8-. \mf c-. d-. a-. \> bf-. c-. f,4-. \!

  \break
  \clef treble \bar "||" \teeny
  f'''4 ( \mf c a8 a16 bf c8 f, ) | f'8-. c-. d-. a( bf4 a)
  a8( \p d a f g4 a) | \break f'8-. \f c-. d-. a( bf c f,) r
  \clef bass \bar "||" \normalsize
  f,8-. \mf c-. d-. a-. \> bf-. c-. f,4-. \!

  \break \time 5/4 \clef treble \bar "||" \teeny
  f'''16-. \p a-. f-. r f-. a-. f-. a-. 
  \clef bass \bar "||"  \normalsize
  r8 \f a,,,-. \> bf c-. f,-. r 

  \break \bar "||" \clef treble \teeny
  f'''16-. \p a-. f-. r f-. a-. f-. a-. 
  \clef bass \bar "||" \normalsize
  r8 \f a,,,8-. \> bf c-. f,-. r 

  \break
  \time 4/4
  f'8-. \f c-. d-. a8-. \> bf-. c-. f,-. r 
  f'8-. \f c-. d-. a8-. \> bf-. c-. f,-. r 
  f'8-. \f c-. d-. a8-. \> bf-. c-. f,-. r \!
  
  \bar "||"
  \break
}

rodrigo_ch = \chordmode {
  s1*2
  f2:5.9 c4:7/f f2:5.9 | f1 | s1

  s1*2
  d2:m c4:7 f2:5.9 | f4 c2:7 f4 | f2 c4:7 f4 | f1 | s1

  s1*2
  f2:m7.11 d4:7.11 f:m7.11 | f2:7maj d2:m
  a2:7 d:m | c4:7 f c:7 f | s1

  f2:maj7.9+ s2 s4 | f2:maj7.9+ s2 s4 
} 
 
% Another one bites the dust
% ========================================================================
dust_mk = {
  \mark "Bass score"
  \mkup "Another one bites the dust"
	\mark \jazzTempoMarkup "Rock" c4 "110"
  s1
  s1*4
  s1*6

  s1*4
  s1*4
  s1*6
  s1*6

  s1*4
}

dust_mu = \relative c {
  \clef bass
  \key e \minor
  \time 4/4
  
  r2. r8 a16 g
  e4-. e-. e-. r8. e16 | e8 e g e16 a r4 r8 a16 g
  e4-. e-. e-. r8. e16 | e8 e g e16 a r2
  \break
  \bar "||"

  \repeat percent 3 {
    e4-. e-. e-. r8. e16 | e8 e g e16 a r2
  }

  \break
  \mark "Lead"
  \bar "[|:"
  \repeat volta 2 {
    \repeat percent 2 {
      e4-. e-. e-. r8. e16 | e8 e g e16 a r2
    }

    \break
    c8 c r16 c r cs d g r8 g,4-.
    c8 c r16 c r cs d4-. g,4-.
    \break
    c8 c r16 c r cs d g r8 g,4-.
    a8 a r16 a r as b4-. r16 g8.->
    
    \break
    \repeat percent 3 {
      e4-. e-. e-. r8. e16 | e8 e g e16 a r2
    }

    fs4-. r8 b16 as a8 a a4-. | fs4-. r r g--
    \repeat percent 2 {
      e4-. e-. e-. r8. e16 | e8 e g e16 a r2
    }
  }
  \bar ":|]"

  \break
  e4-. e-. e-. r8. e16 | e8 e g e16 a r2
  e4-. e-. e-. r8. e16 | e8 e g e16 a r2

  \bar "||"
  \break
}

dust_ch = \chordmode {
  s1
  s1*4
  s1*6

  s1*4
  s1*4
  s1*6
  s1*6

  s1*4
  s1*3
}
 
% Smoke on the water
% ========================================================================
smoke_mk = {
  \mkup "Smoke on the water"
	\mark \jazzTempoMarkup "Rock" c4 "110"
  s1*4
  s1*8
  
  s1*16
  s1*6

  s1*8
  s1*8
}

smoke_mu = \relative c''' {
  \clef treble
  \key g \minor
  \time 4/4

  \repeat volta 2 {
    s1 | s1 | s1 | s1
  }

  \break
  \bar ":|][|:"
  \repeat volta 2 {
    s1 | s1 | s1 | s1
  }

  \bar ":|][|:"
  \repeat volta 2 {
    s1 | s1 | s1 | s1
  }

  \break
  \bar ":|][|:"
  \repeat volta 2 {
    g4 g f d8 f~ | f d4. r4 c8 bf | c4 bf8 c ~ c4 d8 g, ~ | g2 r2
    \break
    r4 f' f8 d f d | g4 bf, r2 | r4 d c bf | d8 d4 g,8 ~ g4 r 
    \break
    r4 g' g8 f g f | g4 g r2 | c,8 bf df4 c bf8 d ~ | d4 r r2
    \break
    r4 f f8 d f d | f4 g r2 | f4 d c8 f4 g,8 ~ | g1

    \break
    e'2. d8 c | ef2 c | r4 bf8 g bf g f g ~ | g1 | e'2. d8 c | ef2 c 
    \break
    s1 | s1 | s1 | s1
    s1 | s1 | s1 | s1
  }
  \bar ":|]"

  \break
  s1 | s1 | s1 | s1
  \bar "||"
  s1 | s1 | s1 | s1

  \break
  \bar "||"
}

smoke_ch = \chordmode {
  g2:m c/g | g2:m c/g | g2:m c2 | bf4 g2.:m
  s1*8
  g1:m | s1 | g2:m f | g1:m
  g1:m | s1 | g2:m f | g1:m
  g1:m | s1 | g2:m f | g1:m
  g1:m | s1 | g2:m f | g1:m
  c | af | g:m | s1
  c | af 
  g2:m c/g | g2:m c/g | g2:m c2 | bf4 g2.:m
  s1*4
  s1*8
}
 
% Flor de olmo
% ========================================================================
olmo_mk = {
	\once \override Score.RehearsalMark.extra-offset = #'(0 . 3)
  \mkup "Flor de olmo"
	\mark \jazzTempoMarkup "Slow Salsa" c4 "105"
}

olmo_mu = \relative c' {
  \key f \major
  \clef treble
  \time 4/4

  r2 r4 c

  \bar "[|:"
  \mkup "Repeat 3x times, Guitar solo on 2nd x, Sax answers on 3rd x"
  \repeat volta 3 {
    a'1 | a4 bf8 a ~ a g ~ g4 | fs1 | r2 r4 d
    \break
    bf'1 | bf4 c8 bf ~ bf a ~ a4 | g1 | r2 r4 r8 a 
    \break
    f8 a d d, ~ d ef ~ ef f | a d f d, ~ d ef ~ ef f | d ef g bf cs a gs fs | f?2. r4
    \break
    f2. r4 | f1 | r2 r4 r8 g | d8 ef g bf d4 d | bf2 r4 c, |

    \break
    a'1 | a4 bf8 a ~ a g ~ g4 | f1 | r2 r4 d
    \break
    bf'1 | bf4 c8 bf ~ bf af ~ af4 | g1 | r8 c g d' ~ d g ~ g4
    \break
    e2 f4 f, | a2 a 
  } 
  \alternative {{
    f1 | R1
    \bar ":|]"
  }{
    f1 ~ | f1 \fermata
  }}

  \bar "||"
  \break
}

olmo_ch = \chordmode {
  s1
  f:7maj | ef:7.9+ | d:7 | d:7/a
  g:7 | g:7/f | e:7.5- | a:7
  d2:m c:m/d | d2:m c:m/d | c:m7 f:5+.9-.9+
  bf1:7maj | a:7.13- | a:6 | g:7
  c2:m7 f:7 | bf:7maj bf/c

  f1:7maj | ef:9 | d:m7 | d:7.9+
  g:9 | g:7/f | ef:7maj | e:7.9+
  a2:7 d:m7 | gf:7 c:7 
  
  f1:7maj | g2:m7 c:7
  f1:7maj | s1
}
 
% Hamburguesa Vegetal
% ========================================================================
hamburguesa_mk = {
	\once \override Score.RehearsalMark.extra-offset = #'(0 . 3)
  \mkup "Hamburguesa vegetal"
	\mark \jazzTempoMarkup "Estilo???" c4 "120"
}

hamburguesa_mu = {
}

hamburguesa_ch = {
}

%
%
%
%
%
%
% Score
% ========================================================================
% ========================================================================
% ========================================================================
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
	% Oculta el primer pentagrama del markup
	\override Staff.VerticalAxisGroup #'remove-first = ##t
  % \alfabeto_mk
  % \mi_barba_mk
  % \pageBreak
  % \calypso_lento_mk
  % \calypso_rapido_mk
  % \pageBreak
  % \harry_potter_mk
  % \toccatina_mk
  % \pageBreak
  % \entertainer_mk
  % \pageBreak
  % \kwela_mk
  % \dusty_blue_mk
  % \pageBreak
  % \gershwin_mk
  \haendel_mk
  % \pageBreak
  % \rodrigo_mk
  % \pageBreak
  % \dust_mk
  % \pageBreak
  % \smoke_mk
  % \pageBreak
  % \olmo_mk
  % \hamburguesa_mk
}

scoreChords = {
  % \alfabeto_ch
  % \mi_barba_ch
  % \calypso_lento_ch
  % \calypso_rapido_ch
  % \harry_potter_ch
  % \toccatina_ch
  % \entertainer_ch
  % \kwela_ch
  % \dusty_blue_ch
  % \gershwin_ch
  \haendel_ch
  % \rodrigo_ch
  % \dust_ch
  % \smoke_ch
  % \olmo_ch
  % \hamburguesa_ch
}

melody = {
  % \alfabeto_mu
  % \mi_barba_mu
  % \calypso_lento_mu
  % \calypso_rapido_mu
  % \harry_potter_mu
  % \toccatina_mu
  % \entertainer_mu
  % \kwela_mu
  % \dusty_blue_mu
  % \gershwin_mu
  \haendel_mu
  % \rodrigo_mu
  % \dust_mu
  % \smoke_mu
  % \olmo_mu
  % \hamburguesa_mu
} 

% (Sólo se imprime el primer \book que encuentra Lilypond)
%{
\book {
	\header { instrument = "" }
  \bookOutputName ""

	\score {
		<<
			\scoreMarkup
			\new StaffGroup <<
        \transpose c c \scoreChords
				\new Staff \with { instrumentName = "" midiInstrument = #"" } {
          \transpose c c \instrument
        }
			>>
		>>
	}
}
%}

\book {
  \bookOutputName "Score"

	\paper {
		% Descomentar para score
		% #(set-paper-size "a4" 'landscape)
		% indent = 20
		% systems-per-page = 2
	}

	\score {
		% Descomentar para midi
		% \midi { \tempo 4 = 110 }

		% NO USAR layout-set-staff-size O SE ROMPE lilyjazz!!!
		\layout { }

		<<
			\scoreMarkup
      \new ChordNames { \scoreChords }
      \new Staff \with { instrumentName = "" midiInstrument = #"" } {
        \transpose c c \melody
      }
		>>
	}
}
