\version "2.18.2"


% Descripción de funcionalidades
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% PLUGINS
% - \eolMark:					mark al final de línea
% - \bendBefore: 				solución al bendBefore (i.e. dis8 \bendBefore)
% - \rit: 						crea una porción de ritardando que debe terminar como las dinámicas, con un \!
% - \improOn, \improOff:	negra de improvisación
% - \emptyStaff:				Imprime un pentagrama vacío
% - \emptyBar:					Imprime un compás invisible (útil para indentados)



% LAYOUT
% - El aspecto de los silencios multicompás es como los de las partituras de jazz
% - Margen extra de las articulaciones para evitar la colisión con las ligaduras
% - Glissandos bonitos



% PAPER
% - Se imprime el título, instrumento y número de página en todas las páginas
% - Solo se debe tocar el page-count (si eso). Sale bonito tal cual.

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Descripción de funcionalidades
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% 03-09-14: Layout
% Ahora las articulaciones van por dentro de las ligaduras. Parece que es la mejor solución



% 04-09-14: Layout
% Los creciendos y decreciendos tienen la línea un poco más gruesa
%
% Markup
% Añadidas guías para correcta posición y markup de la indicación de tempo
%
% Plugins
% Añadida función \emptyStave para imprimir un pentagrama vacío.
% En algunas partituras hay un pentagrama vacío antes del coda (lo cual mola bastante, por cierto)



% 05-09-14: Plugins
% Añadido a la función \emptyStave el esconder la armadura de la clave, que no estaba.



% 08-09-14: Layout
% Ahora no se muestra la cancelación de accidentales al haber un cambio de armadura



% 09-09-14: Chords
% Añadida sentencia para que los acordes menores salgan con un guión en vez de con una M minúscula



% 10-09-14: Plugins
% Mejorada la función \bendAfter.
% Ahora es genérica, y se puede definir cualquier texto en un lateral que haga las funciones de ornamento



% 15-09-14: Layout
% Añadido un poco más de margen a los reguladores



% 16-09-14: Plugins
% Mejorada la función para imprimir barras de improvisación.
% Antes se llamaba al comando \rs para pintar una barra que ocupaba una negra
% Se reemplaza \rs por \restImproOn y \restImproOff, siendo esta opción mucho más flexible.
% Cuando se activa \restImproOn, todo silencio se convierte en barra de improvisación, respetando su duración.



% 20-09-14: Plugins
% Mejorada la función para imprimir barras de improvisación.
% Se reemplaza \restImproOn y \restImproOff por \improOn y \improOff
% Ahora, también las notas del pentagrama salen como barra de improvisación.
% (los comandos \improvisationOn e \improvisationOff de lilypond no se llevan bien con LilyJAZZ)
% Se ha añadido un grob correspondiente a los slash de la fuente LilyJAZZ en el archivo lily-jazz.ly



% 20-09-14: Layout
% Comentada la línea que oculta la cancelación de accidentales al haber cambio de armadura.
% Realmente, la cancelación es muy bienvenida.



% 01-10-14: Plugins
% Renombrada la función \emptyStave a \emptyStaff (stave es el plural de staff) y 
% corregida (ya no muestra el número de compás).
% Recordar que después de usar \emptyStaff se necesita redefinir el 
% número de compás con \set Score.currentBarNumber = #xx
%
% Añadida la función \emptyBar que oculta un compás entero. Se puede usar para crear un 
% efecto de indentación en un pentagrama dado.
% Recordar que después de usar \emptyBar se necesita:
% - redefinir el número de compás con \set Score.currentBarNumber = #xx
% - volver a pintar la clave con \clef
% - volver a pintar la armadura con \key



% 15-10-14: Language
% Cambiado el lenguaje de entrada de notas del finlandes al inglés
% La diferencia es que ahora los sostenidos se escriben "-s" en vez de "-is"
% y los bemoles "-f" en vez de "-es".
%
% Estructura
% Para poder autoincluir los archivos de instrumento en otros 
% (para montar un midi, o un score), se han movido los plugins a un archivo separado
% y ahora se utiliza la función \includeOnce, evitando así el problema de las
% dependencias circulares.
%
% Global
% Todas las instrucciones que se colocaban justo antes de empezar la música están ahora
% recogidas en una función \global.
% A \global se le ha añadido instrucciones para que agrupe las corcheas de dos en dos
% en vez de cuatro en cuatro (el comportamiento por defecto).
%
% Plugins
% Se le ha aumentado el grosor de los bordes de la función \boxed



% 26-10-14: Chords
% Añadido el include "chord-exception.ily" que recoge una mejor notación de los acordes.
% Actualmente, recoge los acordes maj7, 7(b5), 7(#5), 7(b9), 7(#9), 7(add13), maj9(add13), 9(#11)
% Se irá añadiendo acordes a menudo.
 

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Dependencias
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% \includeOnce
% Incluye una dependencia solo una vez. Así se pueden incluir los archivos en otros 
% (para montar un score por ejemplo) sin liarla parda con dependencias cíclicas.

includeOnce = #(
	define-void-function (parser location filename) (string?) (
		if (not (defined? (string->symbol filename))) (
			begin (
				ly:parser-include-string parser (
					string-concatenate (list "\\include \"" filename "\"")
				)
			)
			(primitive-eval (list 'define (string->symbol filename) #t))
		)
	)
)



% LilyJAZZ
\includeOnce "includes/ignatzek-jazz-chords.ily"
\includeOnce "includes/lily-jazz.ily"



% Language
\language "english"



% Plugins
\includeOnce "includes/plugins-1.2.ily"



% Chord Exceptions
\includeOnce "includes/chord-exceptions.ily"

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~











% Disposición
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Cabecera
\header {
	title = ""
	instrument = ""
	composer = ""
	arranger = ""
	poet = ""
	tagline = ""
}



% Márgenes, fuentes y distancias del papel
#(set-global-staff-size 14)
\paper {
	% El definir otro tipo de fuente no permite cambiar el tamaño global del staff. Es un bug conocigo
	#(define fonts (make-pango-font-tree "LilyJAZZText" "Nimbus Sans" "Luxi Mono" (/ 20 20)))

	% distancia entre la parte superior de la página y el título
	top-markup-spacing #'basic-distance = 5

	% distancia entre el título y el primer sistema
	markup-system-spacing #'basic-distance = 25

	% distancia entre el texto y el primer sistema 
	top-system-spacing #'basic-distance = 15

	% márgen inferior
	bottom-margin = 20
	
	% intentado del primer sistema
	indent = 0
	 
	% número de páginas
	% page-count = 1

	ragged-last-bottom = ##f 

	oddHeaderMarkup = \markup {
	 \on-the-fly #not-first-page \fill-line {
		\fromproperty #'page:page-number-string
		\fromproperty #'header:title
		\fromproperty #'header:instrument
	 }
	}

	evenHeaderMarkup = \markup {
	 \on-the-fly #not-first-page \fill-line {
		\fromproperty #'header:instrument
		\fromproperty #'header:title
		\fromproperty #'page:page-number-string
	 }
	}
}

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~







% Elementos comunes de la partitura
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
	% Oculta el primer pentagrama del markup
	\override Staff.VerticalAxisGroup #'remove-first = ##t

	% Indicación de tiempo
	% \once \override Score.RehearsalMark.X-offset = #5
	% \mark \jazzTempoMarkup "Easy Swing" c4 "112"

	\once \override Score.RehearsalMark.X-offset = #5
	\mark \markup {
		\column { \boxed "Satanic Swing" }
		\column { "Swing: A-B-A(IMP)-B(Rs->G)-C-A-B-IMP-A-B->Dx2-IMP-Dx2-FIN" }
	}
	\partial 4
	s4
	s1*6
	
	\once \override Score.RehearsalMark.X-offset = #5
	\mark \markup \boxed "B"
	s1*8
	
	\once \override Score.RehearsalMark.X-offset = #5
	\mark \markup \boxed "C"
	s1*8
	
	\once \override Score.RehearsalMark.X-offset = #5
	\mark \markup \boxed "D"
	s1*12
	
	\pageBreak
	
	\once \override Score.RehearsalMark.X-offset = #5
	\mark \markup \boxed "Kasapo"
	s1*30

	\pageBreak

	\once \override Score.RehearsalMark.X-offset = #5
	\mark \markup {
		\column { \boxed "Esperanza" }
		\column { "Straight: Inx3-Ax2-Bx2-Ax2-Bx2-IMP-Ax2(perc)-Bx2" }
	}
	s1*4

	\once \override Score.RehearsalMark.X-offset = #5
	\mark \markup \boxed "A"
	s1*8
	
	\once \override Score.RehearsalMark.X-offset = #5
	\mark \markup \boxed "B"
	s1*10
	
	\once \override Score.RehearsalMark.X-offset = #5
	\mark \markup {
		\column { \boxed "Sirtaquizás" }
		\column { "Swing: Ax2-Bx2-Ax2-Bx2-Q" }
	}
	s1*5

	\once \override Score.RehearsalMark.X-offset = #5
	\mark \markup \boxed "A"
	s1*10
	
	\once \override Score.RehearsalMark.X-offset = #5
	\mark \markup \boxed "B"
	s1*10
	
	\once \override Score.RehearsalMark.X-offset = #5
	\mark \markup "Quizás: (Bix2-Esx2-Bix2-Es-Fin)"
	s1*4
	
	\once \override Score.RehearsalMark.X-offset = #5
	\mark \markup {
		\column { \boxed "St James Morrison" }
		\column { "Swing: Ax2-Bx2-C-Bx2-C-Ax2-Fin" }
	}
	s1*8
	
	\mark \markup \boxed "B"
	s1*5
	
	\mark \markup \boxed "C"
	s1*10
	
	\once \override Score.RehearsalMark.X-offset = #5
	\mark \markup {
		\column { \boxed "Down by the jungle" }
		\column { "Swing: Drx3-A-Drx2~(4=1)~QsBix2-Esx2-Bix2-Esx2(perc)-Drx2-(c.bf.a - The Cure)" }
	}
	s1*4
	
	\mark \markup \boxed "A"
	s1*10
}



% Función musical global
global = {
	% Para pintar todos los números de compás debajo del pentagrama
	% \override Score.BarNumber #'break-visibility = #end-of-line-invisible
	% \override Score.BarNumber #'direction = #DOWN
	% \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

	\jazzOn
	\compressFullBarRests

	% Arregla el problema de tener el scoreMarkup añadido
	\revert MultiMeasureRest #'staff-position
	\override Voice.Rest #'staff-position = #0	

	\set Timing.beamExceptions = #'()
	\set Timing.baseMoment = #(ly:make-moment 1/4)
	\set Timing.beatStructure = #'(1 1 1 1) 
}

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Elementos específicos del instrumento
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Cifrado
scoreChords = \chords {    
	\set minorChordModifier = \markup { "-" }
	\set chordNameExceptions = #chordNameExceptions
	\override ChordName.font-size = #2.5

	s1*82
	bf1 f1 c:7 f2 f:7
	bf1 d1 g:7 c:7
}

% Música
music = \relative c'' {
  
	\global
	\key g \minor

	\partial 4
	\times 2/3 {b8 c cs}
	
	\repeat volta 2 {
		d4 r8 d ~ d4 r 
		\times 2/3 {d8 ef d} cs d ef d bf g
	}
	\alternative {
		{ c4 r8 c ~ c4 r
		  r2 r4 \times 2/3 {b8 c cs} }
		{ e,8 e r e ~ e4 r
		  r2 r4 \times 2/3 {b'8 c cs} }
	}
	
	\break
	d4 r8 d ~ d4 r
	\times 2/3 {d8 ef d} cs d ef d bf g
	c fs,4 c'8 ~ c4 r
	\times 2/3 {c8 d c} b c d c b c
	
	\break
	bf bf4 bf8 ~ bf4 r
	\times 2/3 {bf8 c bf} a bf c bf4 a8 ~
	a1
	r2 r8 cs, d4
	
	\break
	a'8 a4 a8 ~ a2
	r2 r8 cs, d4
	bf'8 bf4 bf8 ~ bf2
	r2 r4 bf \turn
	
	\break
	a8 a4 a8 ~ a2
	r2 r8 cs, d4
	bf'8 d,4 a'8 r cs, d4
	bf'8 bf4 a8 r2
	
	\break
	\key g \major
	\repeat volta 2 {
		\improOn bf4. bf8 \improOff r2
		\improOn bf4. bf8 \improOff r2
		\improOn r4 r4 r4 r4
		r4 r4 r4 r4 \improOff
		
		\break
		r8 bf c bf df c4.
		r8 bf c bf df c4.
		r8 f4. e4 ef
		d4 cs8 d ~ d4 r
		
		\break
		ef8 ef d c ~ c4 r
		r8 af' g f cs d4 bf8 ~
		bf2 r2
		R1
	}
	
	\break
	\key a \minor
	\repeat volta 2 {
		e,4 a c e
		ds8 e f e c b a4
		e4 a c e
		ds8 e f e ds2
		
		\break
		e,4 a c e
		ds8 e f e c b a4
		a8 b c ds e ds c b 
		a4 gs4 a r4
	}
	
	\break
	a'4 g fs e
	ds c b a
	a' g fs e
	a,8 b c ds e2
	
	\break
	a4 g fs e
	ds c b a
	a8 b c ds e ds c b 
	a4 gs a r
	\bar "||"
	
	\break
	d4 c b a 
	d4 d d2
	e8 d c4 c8 b a4
	e'4 e e2
	
	\break
	d4 c b a
	f'1
	e8 ds c ds e ds c b 
	a4 gs a4 r
	\bar "||"
	
	\break
	\repeat volta 2 {
		r4 gs' gs r
		r4 a a r
	}
	\alternative {
		{ a,8 b c ds e ds c b
		  a b c ds e2 }
		{ a,8 b c ds e ds c b
		  a4 gs a r4 }
	}
	
	\break
	\key a \major
	f'1
	e
	d ~ 
	d2 ^"(Tacet 2nd x)" c4 cs8 \glissando e
	
	\break
	\repeat volta 2 {
		a,8 e a c e4 e8 c
		a8 e a c e4 r
		a,8 e a c e f4 e8
		d c b a gs4 r
		
		\break
		r8 b4 c8 d4 d8 c 
		b b4 c8 d4 r
		r8 b4 c8 d4 e8 f
		e d c d e4 r
	}
	
	\break
	\repeat volta 2 {
		f8 f4 f8 e4 d8 f ~
		f2 r2
		e8 e4 e8 d4 c8 e ~
		e2 r2
		
		d8 d4 d8 c4 b8 d ~
		\break
		d2 r2
	}
	
	\alternative {
		{ e8 f e d c4 d8 e ~
		  e8 d4 cs8 d4 e4 }
		{ e8 f e d c d c b
		  a8 e4 e8 a4 r }
	}
	
	\break
	\key g \minor
	\repeat volta 2 {
		r8 g \times 2/3 {bf c df} c bf ~ \times 2/3 {bf g f}
		g2 r2
	}
	\alternative {
		{ r8 g \times 2/3 {bf c df} c bf \times 2/3 {g f g}
		  \times 2/3 {bf g g} g2 r4 }
		{ r8 g \times 2/3 {bf c df} c bf ~ bf cs }
	}
	
	\break
	\key g \major
	\repeat volta 2 {
		d2 r4 r8 cs
		d2 r4 r8 cs
		d2 r8 e ~ \times 2/3 {e d cs} 
		d2 r4 r8 b
		
		\break
		c2 r8 d ~ \times 2/3 {d c b} 
		c2 r4 r8 as
		b2 r4 r8 g
		a2 r8 b \times 2/3 {a g fs}
		
		g2 r2
		r2 r4 r8 cs ^ \markup \small "(1st x only)"
	}
	
	\break
	\repeat volta 2 {
		cs8 d d d d d d d
		cs8 d d d d d d16 e d cs
		cs8 d d d d d d d
		cs8 d d d d d d d
		
		\break
		b8 c c c c c c16 d c b
		b8 c c c c c c c
		as8 b b b b b b16 a g b 
		gs8 a a a a a a16 g fs a
		
		g2 r2
		R1
	}
	
	\break
	R1*3
	r2 r8 d f g
	
	\pageBreak
	\key d \minor
	\repeat volta 2 {
		a4 a8 a g4 a8 f
		r d r4 r8 d f g
		a4 a8 a bf a4 g8 ~
		g2 r8 d f a ~
		
		\break
		a4 a8 a g4 a8 f
		r d r4 r8 d \times 2/3 {e d f ~ }
		f \fermata af g f g \fermata f e f
		d2 r2
	}
	
	\break
	\repeat volta 2 {
		d8 e d f d4 r
		g8 a g f d4 r
		g8 a g f d4 r8 d
	}
	\alternative {
		{ e f e d ~ d a' \times 2/3 {af g f} }
		{ e f e d ~ d a' a4 }
	}
	
	\break
	a2 r2
	f8 f f f ~ f g f4
	a2 r8 a c4
	a2 r2
	f8 f f f ~ f g f a ~ 
	a2 r8 a c a ~ 
	
	\break
	a2 r8 a c a ~ 
	a2 r8 a c a ~
	a2 r2
	R1
	
	\bar "||"
	
	\break
	\key f \major
	R1*3
	r2 r8 f16 f ~ f e d8
	
	\break
	c8. g'16 ~ g4 r8 bf16 bf ~ bf a g8
	f2 r8 f16 f ~ f e d8
	c8. g'16 ~ g4 r8 bf16 bf ~ bf a g8
	f2 r8 f16 f g g a8 
	
	\break
	R1*8
}

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

\book {
	\score {
		<<
			\scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\new Staff { \music }
			>>
		>>
	}
}


