﻿\version "2.16.1"

secondTenor = {

	\compressFullBarRests

	\times 4/4
	\key c \minor
	
    R1*4
    
    c8 c-\bendAfter #-2 r4 r8 aes c8[ c]
    g8 c r8 c ~ c2
    r8 c c4 c8 b r8 d ~
    d4 ~ d4-\bendAfter #-6 r2
    
    \bar "|."
}


%{
\include "gab-commons-1.0.ly"
\include "layout.ly"
\include "markup.ly"

\header {
    instrument = "2nd Tenor Sax (Eb)"
}

{ << \relative c'' \secondTenor \\ \scoreMarkup >> }
%}