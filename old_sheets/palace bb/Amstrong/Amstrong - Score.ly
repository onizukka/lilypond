﻿\version "2.16.1"

% necesaria la librería gab-commons-1.0.ly
% utiliza rest-merge
\include "gab-commons-1.0.ly"

\include "layout.ly"
\include "markup.ly"
\include "Amstrong - 1st Alto.ly"
\include "Amstrong - 2nd Alto.ly"
\include "Amstrong - 1st Tenor.ly"
\include "Amstrong - 2nd Tenor.ly"
\include "Amstrong - Baritone.ly"

\include "Amstrong - 1st Trumpet.ly"
\include "Amstrong - 2nd Trumpet.ly"
\include "Amstrong - 3rd Trumpet.ly"
\include "Amstrong - 4th Trumpet.ly"
\include "Amstrong - 5th Trumpet.ly"

\include "Amstrong - 1st Trombone.ly"
\include "Amstrong - 2nd Trombone.ly"
\include "Amstrong - 3rd Trombone.ly"
\include "Amstrong - 4th Trombone.ly"

% Para pintar solamente alguna de las partes, 
% hay que comentar el \score con "%{" al principio y "%}" al final.
% Es probable que haya que ajusta el espacio entre sistemas (ver más abajo)
\score {
    <<
        \new StaffGroup <<
            \new Staff << 
                \set Staff.instrumentName = #"ASx 1"
                \set Staff.midiInstrument = #"alto sax"
                \relative c'' \firstAlto \\ \scoreMarkup 
            >>
            \new Staff { 
                \set Staff.instrumentName = #"ASx 2"
                \set Staff.midiInstrument = #"alto sax"
                \relative c'' \secondAlto 
            }
            \new Staff {
                \set Staff.instrumentName = #"TSx 1"
                \set Staff.midiInstrument = #"tenor sax"
                \relative c'' \firstTenor 
            }
            \new Staff {
                \set Staff.instrumentName = #"TSx 2"
                \set Staff.midiInstrument = #"tenor sax"
                \relative c' \secondTenor 
            }
            \new Staff {
                \set Staff.instrumentName = #"BSx 1"
                \set Staff.midiInstrument = #"baritone sax"
                \relative c' \baritone
            }
        >>
        %{
        \new StaffGroup <<
            \new Staff {
                \set Staff.instrumentName = #"Tp 1"
                \transpose ees bes, \relative c'' \firstTrumpet
            }
            \new Staff { 
                \set Staff.instrumentName = #"Tp 2"
                \transpose ees bes, \relative c'' \secondTrumpet
            }
            \new Staff {
                \set Staff.instrumentName = #"Tp 3"
                \transpose ees bes, \relative c'' \thirdTrumpet
            }
            \new Staff {
                \set Staff.instrumentName = #"Tp 4"
                \transpose ees bes, \relative c'' \fourthTrumpet
            }
            \new Staff {
                \set Staff.instrumentName = #"Tp 5"
                \transpose ees bes, \relative c'' \fifthTrumpet
            }
        >>
        
        \new StaffGroup <<
            \new Staff {
                \set Staff.instrumentName = #"Tb 1"
                \transpose bes c \relative c'' \firstTrombone
            }
            \new Staff {
                \set Staff.instrumentName = #"Tb 2"
                \transpose bes c \relative c'' \secondTrombone
            }
            \new Staff {
                \set Staff.instrumentName = #"Tb 3"
                \transpose bes c \relative c'' \thirdTrombone
            }
            \new Staff {
                \set Staff.instrumentName = #"Tb 4"
                \transpose bes c \relative c'' \fourthTrombone
            }
        >>%}
    >>
    \layout {}
    \midi {
        \context {
            \Score tempoWholesPerMinute = #(ly:make-moment 90 4)
        }
    }
}

\header {
    instrument = "Score"
}