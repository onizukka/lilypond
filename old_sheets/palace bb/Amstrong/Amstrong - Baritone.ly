﻿\version "2.16.1"

baritone = {

	\compressFullBarRests

	\times 4/4
	\key c \minor
	
    R1*4
    
    a8 g-\bendAfter #-2 r4 r8 ees f8[ ees]
    g8 g r8 g ~ g2
    r8 g g4 g8 g r8 c ~
    c4 ~ c4-\bendAfter #-6 r2
    
    \bar "|."
}


%{
\include "gab-commons-1.0.ly"
\include "layout.ly"
\include "markup.ly"

\header {
    instrument = "Baritone Sax"
}

{ << \relative c' \baritone \\ \scoreMarkup >> }
%}