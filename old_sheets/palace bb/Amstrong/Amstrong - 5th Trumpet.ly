﻿\version "2.16.1"

fifthTrumpet = {

	\compressFullBarRests

	\times 4/4
	\key fis \minor
	
    \partial 4
    cis8 f
    
    \bar ".|"
    
    a2 fis?4 fis8 fis
    r8 a ~ a2 cis,8 fis
    gis8 gis ~ gis4 gis4 f8 cis ~
    cis2 r4 cis8 f
    
    a2 fis?4 fis8 fis
    r8 a ~ a2 r4
    R1*2
    
    \break
    \bar ".|"
    \repeat percent 2 { R1*2 }
    
    \bar "|:"
	\repeat volta 2 { R1*15 }
	\alternative {
		{ R1 }
		{ r2 r4 cis,8 f  }
	}
    
    \break
    
    a2 fis?4 fis8 fis
    r8 a ~ a2 cis,8 fis
    gis8 gis ~ gis4 gis4 f8 cis ~
    cis2 r4 cis8 f
    
    a2 fis?4 fis8 fis
    r8 a ~ a4 f2
    cis2 b'
    gis2 gis
    
    cis1^\fermata
    
    \bar "|."
}


%{
\include "gab-commons-1.0.ly"
\include "layout.ly"
\include "markup.ly"

\header {
    instrument = "5th Trumpet (Eb)"
}

{ << \transpose ees bes, \relative c'' \fifthTrumpet \\ \scoreMarkup >> }

\paper {  
	% Definición de fuente. No sé para qué sirve "myStaffSize".
	myStaffSize = #20 #(define fonts (make-pango-font-tree "Throw My Hands Up in the Air" "Nimbus Sans" "Luxi Mono" (/ myStaffSize 20)))
	
	% distancia entre sistemas
	system-system-spacing #'basic-distance = #15
	
	% distancia entre el título y el primer sistema
	markup-system-spacing #'basic-distance = #20
}
%}