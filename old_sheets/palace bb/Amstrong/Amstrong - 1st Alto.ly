﻿\version "2.16.1"

firstAlto = {

	\compressFullBarRests

	\times 4/4
	\key c \minor
	
    R1*4
    
    g8 g-\bendAfter #-2 r4 r8 ees f8[ ees]
    g8 g r8 f ~ f2
    r8 g g4 a8 a r8 b ~
    b4 ~ b4-\bendAfter #-6 r2
    
    \bar "|."
}


%{
\include "gab-commons-1.0.ly"
\include "layout.ly"
\include "markup.ly"

\header {
    instrument = "1st Alto Sax"
}

{ << \relative c'' \firstAlto \\ \scoreMarkup >> }
%}