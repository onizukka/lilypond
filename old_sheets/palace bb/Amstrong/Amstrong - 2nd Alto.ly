﻿\version "2.16.1"

secondAlto = {

    \compressFullBarRests

	\times 4/4
	\key c \minor
	
    R1*4
    
    d,8 ees-\bendAfter #-2 r4 r8 d d8[ c]
    ees8 ees r8 f ~ f2
    r8 ees ees4 ees8 des r8 d ~
    d4 ~ d4-\bendAfter #-6 r2
    
    \bar "|."
}


%{
\include "gab-commons-1.0.ly"
\include "layout.ly"
\include "markup.ly"

\header {
    instrument = "2nd Alto Sax"
}

{ << \relative c'' \secondAlto \\ \scoreMarkup >> }
%}