﻿\version "2.16.1"

firstTenor = {

	\compressFullBarRests

	\times 4/4
	\key c \minor
	
    R1*4
    
    g8 g-\bendAfter #-2 r4 r8 ees f8[ ees]
    g8 g r8 f ~ f2
    r8 g g4 g8 g r8 a ~
    a4 ~ a4-\bendAfter #-6 r2
    
    \bar "|."
}


%{
\include "gab-commons-1.0.ly"
\include "layout.ly"
\include "markup.ly"

\header {
    instrument = "1st Tenor Sax"
}

{ << \relative c'' \firstTenor \\ \scoreMarkup >> }
%}