\version "2.16.1"
\include "imports/main.ly"


% Instrument Music
% -------------------------------------------------
trumpetTwo = {

  % Para pintar todos los números de compás debajo del pentagrama
  \override Score.BarNumber #'break-visibility = #end-of-line-invisible
  \override Score.BarNumber #'direction = #DOWN
  \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0
  
  \key c \major
  
  R1
  r2 r4 \f d8-- d-> ~
  d4 r4 r4 c8-- c-> ~
  \break
  c4 r4 ees8( f) ees-- d-^
  r8 c,-^ r e?-^ r g-^ r bes-^
  r8 d-^ r4 r2
  
  \break
  \repeat volta 2 {
    R1*7
    \break
    r2 r4 \f d8-- d-> ~
    d4 r4 r4 c8-- c-> ~
    c4 r4 ees8( f) ees-- d-^
  }	
  \alternative {
     { \break
       r8 c,-^ r e?-^ r g-^ r bes-^
       r8 d-^ r4 r2}
     { r8 c,-^ r e?-^ r g-^ r bes-^
       r8 d-^ r4 ees2-> \bendAfter -4}
  }
  
  \break
  R1*2
  c,4-> r8 c'-^ r bes-^ r g-- 
  bes-- g-. r ees-> ~ ees2
  
  \break
  R1
  a4.-- a8-. r2
  R1
  r4 f8( ees f ees c4-.)
  
  \break
  R1
  a'4.-- a8-. r2
  d4-^ r4 r2
  R1
  
  \break
  r4 g,8( fis g fis g fis
  g4-.) r4 r2
  r4 g8( fis g fis g fis
  g4-.) r4 r2
  
  \break
  r4 c8( b c b c b
  c4-.) r4 r2
  r4 g8( fis g fis g fis
  g4-.) r4 r4 d'8-- d-> ~
  
  \break
  d4 r4 r4 c8-- c-> ~
  c4 r4 ees8( f) ees-- d-^
  r8 c,-^ r e?-^ r g-^ r bes-^
  r8 d-^ r4 ees2-> \bendAfter -4
  
  \break
  \repeat volta 2 {
    R1
    r4 r8 ees8->( ~ ees d ees4-^)
    R1
    r4 r8 ees8->( ~ ees d ees4-^)   
    
    \break
    R1
    r4 r8 c8->( ~ c b c4-^)
    R1
    r4 r8 ees8->( ~ ees d ees4-^)
    
    \break
    d4-^ r4 r4 c8-- c-> ~
    c4\bendAfter -4 r4 ees8( f) ees-- bes-^
  }	
  \alternative {
     { r8 c,-^ r e?-^ r g-^ r bes-^
       r8 d-^ r4 ees2-> \bendAfter -4}
     { \break
       r8 c,-^ r e?-^ r g-^ r bes-^
       r8 d-^ r4 r4 d8-- d-> ~ }
  }
  d4\bendAfter -4 r4 r4 c8-- c-> ~
  c4\bendAfter -4 r4 ees8( f) ees-- bes-^
  \break
  r8 c,-^ r e?-^ r g-^ r bes-^
  r8 \rit d-^ r c-^ r ees-^ r ees-> ~
  
  ees1 \fermata \!
}


% Instrument Sheet
% -------------------------------------------------

\header {
  instrument = "Trumpet 2 (Eb)"
}

\paper {  	
  % distancia entre sistemas
  system-system-spacing #'basic-distance = #18
  
  % distancia entre el título y el primer sistema
  markup-system-spacing #'basic-distance = #20
}

<<
  \scoreMarkup
  \new Staff << \transpose ees bes \relative c'' \trumpetTwo >>
>>
