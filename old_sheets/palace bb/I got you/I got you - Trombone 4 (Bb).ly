\version "2.16.1"
\include "imports/main.ly"


% Instrument Music
% -------------------------------------------------
tromboneFour = {

  % Para pintar todos los números de compás debajo del pentagrama
  \override Score.BarNumber #'break-visibility = #end-of-line-invisible
  \override Score.BarNumber #'direction = #DOWN
  \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0
  
  \key bes \major
  
  R1
  r2 r4 \f f8-- f-> ~
  f4 f4-^ f4-^ ees8-- ees-> ~
  ees4 ees4-^ ees-^ des8-- bes-^
  \break
  r8 bes-^ r d?-^ r f-^ r aes-^
  r8 c-^ r4^\markup "soli" des8-- ees4.->
  
  \repeat volta 2 {
    bes2->\bendAfter -4 r2
    r4 r8 aes-- bes( aes) bes-- des ~
    \break
    des8 bes4. r2
    r2 des8( ees) des-- ees-> ~
    
    ees4 \bendAfter -4 r4 r2
    r4 r8 aes,-- bes( aes) bes-- des ~
    \break
    des8 bes4. r2
    r2 r4 \f f8-- f-> ~
    
    f4 f4-^ f4-^ ees8-- ees-> ~
    ees4 ees4-^ ees-^ des8-- bes-^
  }	
  \alternative {
     { \break
       r8 bes-^ r d?-^ r f-^ r aes-^
       r8 c-^ r4 des8-- ees4.->}
     { r8 bes,-^ r d?-^ r f-^ r aes-^
       r8 c-^ r4 f,2-> \bendAfter -4}
  }
  
  \break
  bes,4-> r8 bes'-^ r aes-^ r f--
  aes-- f-. r des-> ~ des2
  bes4-> r8 bes'-^ r aes-^ r f--
  aes-- f-. r des-> ~ des2
  
  \break
  f4.-- f8-. r2
  f4.-- f8-. r2
  R1*2
  
  \break
  f4.-- f8-. r2
  f4.-- f8-. r2
  f4-^ r4 r2
  r4 f-^ r2
  
  \break
  bes,4-^ r4 r2
  bes4-. r8 bes'-. r aes-. r f--
  bes,4-^ r4 r2
  bes4-. r8 bes'-. r aes-. r f--
  
  \break
  ees4-. r4 r2
  ees4-. r8 ees-. r des-. r aes--
  bes4-. r4 r2
  bes4-. r8 bes'-. r aes f8--[ f->] ~
  
  \break
  f4 f4-^ f4-^ ees8-- ees-> ~
  ees4 ees4-^ ees-^ des8-- bes-^
  r8 bes-^ r d?-^ r f-^ r aes-^
  r8 c-^ r4 f,2-> \bendAfter -4
  
  \break
  \repeat volta 2 {
    bes,4.-> bes'8-. r aes-. r f--
    aes8-- bes-. r bes,->( ~ bes a? bes4-^)
    bes4.-> bes'8-. r aes-. r f--
    aes8-- bes-. r bes,->( ~ bes a bes4-^)
    
    \break
    ees4.-> ees8-. r des-. r bes--
    des8-- ees-. r ees->( ~ ees d ees4-^)
    bes4.-> bes'8-. r aes-. r f--
    aes8-- bes-. r bes,->( ~ bes a bes4-^)
    
    \break
    f'4-^ f4-^ f4-^ ees8-- ees-> ~
    ees4\bendAfter -4 ees4-^ ees-^ b8-- bes-^
  }	
  \alternative {
     { r8 bes-^ r d?-^ r f-^ r aes-^
       r8 c-^ r4 r2}
     { \break
       r8 bes,-^ r d?-^ r f-^ r aes-^
       r8 c-^ r4 r4 f,8-- f-> ~ }
  }
  f4\bendAfter -4 f4-^ f4-^ ees8-- ees-> ~
  ees4\bendAfter -4 ees4-^ ees-^ b8-- bes-^
  \break
  r8 bes-^ r d?-^ r f-^ r aes-^
  r8 \rit c-^ r bes-^ r b, r bes-> ~
  
  bes1 \fermata \!
}


% Instrument Sheet
% -------------------------------------------------

\header {
  instrument = "Trombone 4 (Bb)"
}

\paper {  	
  % distancia entre sistemas
  system-system-spacing #'basic-distance = #18
  
  % distancia entre el título y el primer sistema
  markup-system-spacing #'basic-distance = #20
}

<<
  \scoreMarkup
  \new Staff << \transpose bes c \relative c'' \tromboneFour >>
>>
