\version "2.16.1"
\include "imports/main.ly"


% Instrument Music
% -------------------------------------------------
trumpetOne = {

  % Para pintar todos los números de compás debajo del pentagrama
  \override Score.BarNumber #'break-visibility = #end-of-line-invisible
  \override Score.BarNumber #'direction = #DOWN
  \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0
  
  \key c \major
  
  R1
  r2 r4 \f g'8-- g-> ~
  g4 r4 r4 f8-- f-> ~
  \break
  f4 r4 ees8( f) ees-- g-^
  r8 c,,-^ r e?-^ r g-^ r bes-^
  r8 d-^ r4 r2
  
  \break
  \repeat volta 2 {
    R1*7
    \break
    r2 r4 \f g8-- g-> ~
    g4 r4 r4 f8-- f-> ~
    f4 r4 ees8( f) ees-- g-^
  }	
  \alternative {
     { \break
       r8 c,,-^ r e?-^ r g-^ r bes-^
       r8 d-^ r4 r2}
     { r8 c,-^ r e?-^ r g-^ r bes-^
       r8 d-^ r4 g2-> \bendAfter -4}
  }
  
  \break
  R1*2
  c,,4-> r8 c'-^ r bes-^ r g-- 
  bes-- g-. r ees-> ~ ees2
  
  \break
  R1
  c'4.-- c8-. r2
  R1
  r4 f,8( ees f ees c4-.)
  
  \break
  R1
  c'4.-- c8-. r2
  g'4-^ r4 r2
  R1
  
  \break
  r4 g,8( fis g fis g fis
  g4-.) r4 r2
  r4 g8( fis g fis g fis
  g4-.) r4 r2
  
  \break
  r4 c8( b c b c b
  c4-.) r4 r2
  r4 g8( fis g fis g fis
  g4-.) r4 r4 g'8-- g-> ~
  
  \break
  g4 r4 r4 f8-- f-> ~
  f4 r4 ees8( f) ees-- g-^
  r8 c,,-^ r e?-^ r g-^ r bes-^
  r8 d-^ r4 g2-> \bendAfter -4
  
  \break
  \repeat volta 2 {
    R1
    r4 r8 g8->( ~ g fis g4-^)
    R1
    r4 r8 g8->( ~ g fis g4-^)   
    
    \break
    R1
    r4 r8 f?8->( ~ f e f4-^)
    R1
    r4 r8 g8->( ~ g fis g4-^)
    
    \break
    g4-^ r4 r4 f8-- f-> ~
    f4\bendAfter -4 r4 ees8( f) ees-- g-^
  }	
  \alternative {
     { r8 c,,-^ r e?-^ r g-^ r bes-^
       r8 d-^ r4 g2-> \bendAfter -4}
     { \break
       r8 c,,-^ r e?-^ r g-^ r bes-^
       r8 d-^ r4 r4 g8-- g-> ~ }
  }
  g4\bendAfter -4 r4 r4 f8-- f-> ~
  f4\bendAfter -4 r4 ees8( f) ees-- g-^
  \break
  r8 c,,-^ r e?-^ r g-^ r bes-^
  r8 \rit d-^ r c-^ r ees-^ r g-> ~
  
  g1 \fermata \!
}


% Instrument Sheet
% -------------------------------------------------

\header {
  instrument = "Trumpet 1 (Eb)"
}

\paper {  	
  % distancia entre sistemas
  system-system-spacing #'basic-distance = #18
  
  % distancia entre el título y el primer sistema
  markup-system-spacing #'basic-distance = #20
}

<<
  \scoreMarkup
  \new Staff << \transpose ees bes \relative c'' \trumpetOne >>
>>
