\version "2.14.2.1"

\header {
	title = "Peter Gunn Theme"
	subtitle = "Theme song from TV Series"
	composer = "Music by Henry Mancini"
	arranger = "Arranged by Philippe Marillia"
	instrument = "Tenor 2"
}
	
voice = \relative c'
{
	\compressFullBarRests

	\tempo "Rock" 4 = 126
	\time 4/4
	\key g \major
   
	R1*4
   
	\repeat volta 2 {
	   f2.\sfz\< ~ f8 d8^^\!
	   
	   R1
	   
	   f1\sfz\< (
	   d'8\! gis,->) r4 r2
	   
	   r8 d (f [g] \times 2/3 { aes4 aes aes)}
	   \times 2/3 {aes4 g f}
	   \times 2/3 {d c d}
	}
	\alternative {
		{
			ais'8 b-\bendAfter #-3 r4 r2 r2 
			\times 2/3 {r4 f f}}
		{
			ais8 b-\bendAfter #-3 r4 r2}
	}
	
	R1
	
	\break
	
	r2 r4 g^^\f
	\autoBeamOff
	r2 r8 d' d-\bendAfter #-3 r
	\autoBeamOn
	R1
	g,8--\< g-- g-- g-- g-- g-- g--\! g-- 
	
	\break
	
	g4\f\bendAfter #-3 r4 r2
	\repeat unfold 6 {
		r8 g f [d] f g4-\bendAfter #-3 r8
		R1
	}
	r8 g f [d] f g4-\bendAfter #-3 r8
	R1*2
		
	\break
	
	\repeat volta 2 {
		R1*8
	}
	
	\repeat volta 2 {
		\repeat percent 2 {
			g,8(-- g-- a-- g-- ais16 b g8 c-- b)--
		}
		\repeat percent 2 {
			g8(-- g-- a-- g-- ais16 b g8 c-- b)--
		}
		\repeat percent 2 {
			g8(-- g-- a-- g-- ais16 b g8 c-- b)--
		}
	}
	\alternative {
		{
			\repeat percent 2 {
				g8(-- g-- a-- g-- ais16 b g8 c-- b)--
			}}
		{
			\repeat percent 2 {
				g8(-- g-- a-- g-- ais16 b g8 c-- b)--
			}}
	}
	
	g8(-- g-- a-- g-- ais16 b g8 c-- b)--
	g8-- g-- a-- g-- r8 fis16 g ~ g b d8-.
	aes4^^-\bendAfter #-3 aes^^-\bendAfter #-3 
	aes^^-\bendAfter #-3 aes^^-\bendAfter #-3 
	aes^^-\bendAfter #-3 aes^^-\bendAfter #-3 
	aes^^-\bendAfter #-3 \breathe
	
	aes8-> g ~
	g1\fermata\< ~
	g8\! r8 r4 r2
}

\paper {  
	myStaffSize = #20
	#(define fonts
		(make-pango-font-tree
			"Throw My Hands Up in the Air"
			"Nimbus Sans"
            "Luxi Mono"
            (/ myStaffSize 20)))
	system-count = #11
	markup-system-spacing #'basic-distance = #20
}

\score {
   \new Staff { \transpose ees bes \voice }
}