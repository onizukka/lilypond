\version "2.14.2.1"

\header {
	title = "Peter Gunn Theme"
	subtitle = "Theme song from TV Series"
	composer = "Music by Henry Mancini"
	arranger = "Arranged by Philippe Marillia"
	instrument = "Baritone 2 (Tb.4 Subst)"
}
	
voice = \relative c''
{
	\compressFullBarRests

	\tempo "Rock" 4 = 126
	\time 4/4
	\key d \major
   
	R1*4
   
	\repeat volta 2 {
		\repeat unfold 6 {
			d2->-. r2
		}
	}
	\alternative {
		{
			d2->-. r2
			d2->-. r2}
		{
			d2->-. r2
			d2->-. r2}
	}
	
	\break
	
	d8^^ r8 r4 r4 d^^\f
	\autoBeamOff
	r2 r8 a' a-\bendAfter #-3 r
	\autoBeamOn
	R1
	d,1\<
	
	\break
	
	fis8\!\f\< fis\! r fis\< fis\! r r4
	\repeat unfold 7 {
		R1
		fis8\< fis\! r fis\< fis\! r r4
	}
	r2 \times 2/3 {r4\ff e-- e--} \break
	
	\repeat volta 2 {
		\repeat percent 3 {
			r2^\markup {\small \bold \smallCaps {2nd time only}} a,4-.-^ r4
			a4.-> a8-> r2
		}
		r2 a4.-> a8->
		R1
	} \break
	
	\repeat volta 2 {
	   e'2.\sfz\< ~ e8\! c8^^
	   
	   R1
	   
	   e1\sfz\< (
	   a8\! dis,->) r4 r2
	   
	   R1*2
	}
	\alternative {
		{
			dis8\< e\! r4 r2 r2 
			\times 2/3 {r4 e-- e--}}
		{
			dis8\< e\! r4 r4 r8 d}
	}
	
	dis\< e\! r4 r4 r8 d-. 
	dis (e) r4 r4 r8 d-.
	dis (e) r4 r2
	ees4^^-\bendAfter #-3 ees4^^-\bendAfter #-3 
	ees4^^-\bendAfter #-3 ees4^^-\bendAfter #-3 
	ees4^^-\bendAfter #-3 ees4^^-\bendAfter #-3 
	ees4^^-\bendAfter #-3 \breathe
	
	ees8-> d ~
	d1\fermata\< ~
	d8\! r8 r4 r2
}

\paper {  
	myStaffSize = #20
	#(define fonts
		(make-pango-font-tree
			"Throw My Hands Up in the Air"
			"Nimbus Sans"
            "Luxi Mono"
            (/ myStaffSize 20)))
	system-count = #11
	markup-system-spacing #'basic-distance = #20
}

\score {
   \new Staff { \transpose bes ees \voice }
}