\version "2.14.2.1"

\header {
	title = "Peter Gunn Theme"
	subtitle = "Theme song from TV Series"
	composer = "Music by Henry Mancini"
	arranger = "Arranged by Philippe Marillia"
	instrument = "Soprano 2 (Tp.2 Subst)"
}
	
voice = \relative c'
{
	\compressFullBarRests

	\tempo "Rock" 4 = 126
	\time 4/4
	\key g \major
   
	R1*4
   
	\repeat volta 2 {
	   f2.\sfz\< ~ f8 d8^^\!
	   
	   R1
	   
	   f1\sfz\< (
	   d'8\! gis,->) r4 r2
	   
	   r8 d' (f [g] \times 2/3 { aes4 aes aes)}
	   \times 2/3 {aes4 g f}
	   \times 2/3 {d c d}
	}
	\alternative {
		{
			ais8 b-\bendAfter #-3 r4 r2 r2 
			\times 2/3 {r4 f' f}}
		{
			ais,8 b-\bendAfter #-3 r4 r2}
	}
	
	R1
	
	\break
	
	R1*4
	
	f8\f\< f\! r f\< f\! r r4
	\repeat unfold 7 {
		R1
		f8\< f\! r f\< f\! r r4
	}
	r2 \times 2/3 {r4\ff d'-- d--} \break
	
	\repeat volta 2 {
		\repeat percent 3 {
			r2^\markup {\small \bold \smallCaps {2nd time only}} b4-.-^ r4
			b4.-> b8-> r2
		}
		r2 b4.-> b8->
		R1
	} \break
	
	\repeat volta 2 {
	   d2.\sfz\< ~ d8\! b8^^
	   
	   R1
	   
	   d1\sfz\< (
	   d8\! gis,->) r4 r2
	   
	   r8 b (d [e]) \times 2/3 {f4-- f-- f--}
	   \times 2/3 {f-- e-- d--}
	   \times 2/3 {b-- a-- b--}
	}
	\alternative {
		{
			e,8\< f\! r4 r2 r2 
			\times 2/3 {r4 d'-- d--}}
		{
			e,8\< f\! r4 r4 r8 g}
	}
	
	e\< f\! r4 r4 r8 g'-. 
	e (f) r4 r4 r8 g-.
	e (f) r4 r8 fis,?16 g ~ g b d8-.
	d4-^-\bendAfter #-3 d4-^-\bendAfter #-3 
	d4-^-\bendAfter #-3 d4-^-\bendAfter #-3 
	d4-^-\bendAfter #-3 d4-^-\bendAfter #-3 
	d4-^-\bendAfter #-3 \breathe
	
	d8-> e ~
	e1\fermata\< ~
	e8\! r8 r4 r2
}

\paper {  
	myStaffSize = #20
	#(define fonts
		(make-pango-font-tree
			"Throw My Hands Up in the Air"
			"Nimbus Sans"
            "Luxi Mono"
            (/ myStaffSize 20)))
	system-count = #11
	markup-system-spacing #'basic-distance = #20
}

\score {
   \new Staff { \voice }
}