\version "2.14.2.1"

\header {
	title = "Peter Gunn Theme"
	subtitle = "Theme song from TV Series"
	composer = "Music by Henry Mancini"
	arranger = "Arranged by Philippe Marillia"
	instrument = "Tenor 5 (Tb.3 Subst)"
}
	
voice = \relative c'
{
	\compressFullBarRests

	\tempo "Rock" 4 = 126
	\time 4/4
	\key g \major
   
	R1*4
   
	\repeat volta 2 {
	   f2.\sfz\< ~ f8 d8^^\!
	   
	   R1
	   
	   f1\sfz\< (
	   d'8\! gis,->) r4 r2
	   
	   r8 d (f [g] \times 2/3 { aes4 aes aes)}
	   \times 2/3 {aes4 g f}
	   \times 2/3 {d c d}
	}
	\alternative {
		{
			ais8 b-\bendAfter #-3 r4 r2 r2 
			\times 2/3 {r4 f' f}}
		{
			ais,8 b-\bendAfter #-3 r4 r2}
	}
	
	R1
	
	\break
	
	r2 r4 g'^^\f
	\autoBeamOff
	r2 r8 d d-\bendAfter #-3 r
	\autoBeamOn
	R1
	g8--\< g-- g-- g-- g-- g-- g--\! g-- 
	
	\break
	
	d\f\< d\! r d\< d\! r r4
	\repeat unfold 7 {
		r8 g f [d] f g4-\bendAfter #-3 r8
		d\< d\! r d\< d\! r r4
	}
	r2 \times 2/3 {r4\ff d-- d--} \break
	
	\repeat volta 2 {
		\repeat percent 3 {
			r2^\markup {\small \bold \smallCaps {2nd time only}} d4-.-^ r4
			d4.-> d8-> r2
		}
		r2 d4.-> d8->
		R1
	} \break
	
	\repeat volta 2 {
	   d2.\sfz\< ~ d8\! d8^^
	   
	   R1
	   
	   d1\sfz\< (
	   b8\! gis->) r4 r2
	   
	   r8 d' (fis [g]) \times 2/3 {a4-- a,-- a--}
	   \times 2/3 {b-- bes-- a--}
	   \times 2/3 {d-- c-- d--}
	}
	\alternative {
		{
			cis8\< d\! r4 r2 r2 
			\times 2/3 {r4 d-- d--}}
		{
			cis8\< d\! r4 r4 r8 g,}
	}
	
	cis\< d\! r4 r4 r8 g,-. 
	b (c) r4 r4 r8 g-.
	b (c) r4 r8 fis,16 g ~ g b d8-.
	g,4^^-\bendAfter #-3 g4^^-\bendAfter #-3 
	g4^^-\bendAfter #-3 g4^^-\bendAfter #-3 
	g4^^-\bendAfter #-3 g4^^-\bendAfter #-3 
	g4^^-\bendAfter #-3 \breathe
	
	g8-> g ~
	g1\fermata\< ~
	g8\! r8 r4 r2
}

\paper {  
	myStaffSize = #20
	#(define fonts
		(make-pango-font-tree
			"Throw My Hands Up in the Air"
			"Nimbus Sans"
            "Luxi Mono"
            (/ myStaffSize 20)))
	system-count = #11
	markup-system-spacing #'basic-distance = #20
}

\score {
   \new Staff { \transpose ees bes \voice }
}