\version "2.14.2.1"

\header {
	title = "Peter Gunn Theme"
	subtitle = "Theme song from TV Series"
	composer = "Music by Henry Mancini"
	arranger = "Arranged by Philippe Marillia"
	instrument = "Tenor 3 (Tb.1 Subst)"
}
	
voice = \relative c'
{
	\compressFullBarRests

	\tempo "Rock" 4 = 126
	\time 4/4
	\key g \major
   
	R1*4
   
	\repeat volta 2 {
	   f2.\sfz\< ~ f8 d8^^\!
	   
	   R1
	   
	   f1\sfz\< (
	   d'8\! gis,->) r4 r2
	   
	   r8 d (f [g] \times 2/3 { aes4 aes aes)}
	   \times 2/3 {aes4 g f}
	   \times 2/3 {d c d}
	}
	\alternative {
		{
			ais8 b-\bendAfter #-3 r4 r2 r2 
			\times 2/3 {r4 f' f}}
		{
			ais,8 b-\bendAfter #-3 r4 r2}
	}
	
	R1
	
	\break
	
	r2 r4 g'^^\f
	\autoBeamOff
	r2 r8 d d-\bendAfter #-3 r
	\autoBeamOn
	R1
	g8--\< g-- g-- g-- g-- g-- g--\! g-- 
	
	\break
	
	b\f\< b\! r b\< b\! r r4
	\repeat unfold 7 {
		r8 g f [d] f g4-\bendAfter #-3 r8
		b\< b\! r b\< b\! r r4
	}
	r2 \times 2/3 {r4\ff a-- a--} \break
	
	\repeat volta 2 {
		\repeat percent 3 {
			r2^\markup {\small \bold \smallCaps {2nd time only}} f4-.-^ r4
			f4.-> f8-> r2
		}
		r2 f4.-> f8->
		R1
	} \break
	
	\repeat volta 2 {
	   a2.\sfz\< ~ a8\! f8^^
	   
	   R1
	   
	   a1\sfz\< (
	   d,8\! gis,->) r4 r2
	   
	   r8 f' (a [bes]) \times 2/3 {b4-- b-- b--}
	   \times 2/3 {b-- bes-- a--}
	   \times 2/3 {f-- ees-- f--}
	}
	\alternative {
		{
			cis8\< d\! r4 r2 r2 
			\times 2/3 {r4 a'-- a--}}
		{
			cis,8\< d\! r4 r4 r8 g,}
	}
	
	cis\< d\! r4 r4 r8 g-. 
	gis (a) r4 r4 r8 g-.
	gis (a) r4 r8 fis,16 g ~ g b d8-.
	c4-^-\bendAfter #-3 c4-^-\bendAfter #-3 
	c4-^-\bendAfter #-3 c4-^-\bendAfter #-3 
	c4-^-\bendAfter #-3 c4-^-\bendAfter #-3 
	c4-^-\bendAfter #-3 \breathe
	
	c8-> b ~
	b1\fermata\< ~
	b8\! r8 r4 r2
}

\paper {  
	myStaffSize = #20
	#(define fonts
		(make-pango-font-tree
			"Throw My Hands Up in the Air"
			"Nimbus Sans"
            "Luxi Mono"
            (/ myStaffSize 20)))
	system-count = #11
	markup-system-spacing #'basic-distance = #20
}

\score {
   \new Staff { \transpose ees bes \voice }
}