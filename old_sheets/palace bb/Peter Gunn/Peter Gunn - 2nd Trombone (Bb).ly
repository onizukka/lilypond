\version "2.14.2.1"

\header {
	title = "Peter Gunn Theme"
	subtitle = "Theme song from TV Series"
	composer = "Music by Henry Mancini"
	arranger = "Arranged by Philippe Marillia"
	instrument = "Tenor 4 (Tb.2 Subst)"
}
	
voice = \relative c'
{
	\compressFullBarRests

	\tempo "Rock" 4 = 126
	\time 4/4
	\key g \major
   
	R1*4
   
	\repeat volta 2 {
	   f2.\sfz\< ~ f8 d8^^\!
	   
	   R1
	   
	   f1\sfz\< (
	   d'8\! gis,->) r4 r2
	   
	   r8 d' (f [g] \times 2/3 { aes4 aes aes)}
	   \times 2/3 {aes4 g f}
	   \times 2/3 {d c d}
	}
	\alternative {
		{
			ais8 b-\bendAfter #-3 r4 r2 r2 
			\times 2/3 {r4 f' f}}
		{
			ais,8 b-\bendAfter #-3 r4 r2}
	}
	
	R1
	
	\break
	
	r2 r4 g^^\f
	\autoBeamOff
	r2 r8 d d-\bendAfter #-3 r
	\autoBeamOn
	R1
	g8--\< g-- g-- g-- g-- g-- g--\! g-- 
	
	\break
	
	g\f\< g\! r g\< g\! r r4
	\repeat unfold 7 {
		r8 g f [d] f g4-\bendAfter #-3 r8
		g\< g\! r g\< g\! r r4
	}
	r2 \times 2/3 {r4\ff f'-- f--} \break
	
	\repeat volta 2 {
		\repeat percent 3 {
			r2^\markup {\small \bold \smallCaps {2nd time only}} a4-.-^ r4
			a4.-> a8-> r2
		}
		r2 a4.-> a8->
		R1
	} \break
	
	\repeat volta 2 {
	   f2.\sfz\< ~ f8\! b,8^^
	   
	   R1
	   
	   f'1\sfz\< (
	   f8\! gis,->) r4 r2
	   
	   r8 b (d [e]) \times 2/3 {f4-- f-- f--}
	   \times 2/3 {f-- e-- d--}
	   \times 2/3 {b-- a-- b--}
	}
	\alternative {
		{
			gis8\< a\! r4 r2 r2 
			\times 2/3 {r4 f'-- f--}}
		{
			gis,8\< a\! r4 r4 r8 g}
	}
	
	gis\< a\! r4 r4 r8 g'-. 
	e (f) r4 r4 r8 g-.
	e (f) r4 r8 fis,?16 g ~ g b d8-.
	ees,4^^-\bendAfter #-3 ees4^^-\bendAfter #-3 
	ees4^^-\bendAfter #-3 ees4^^-\bendAfter #-3 
	ees4^^-\bendAfter #-3 ees4^^-\bendAfter #-3 
	ees4^^-\bendAfter #-3 \breathe
	
	ees8-> d ~
	d1\fermata\< ~
	d8\! r8 r4 r2
}

\paper {  
	myStaffSize = #20
	#(define fonts
		(make-pango-font-tree
			"Throw My Hands Up in the Air"
			"Nimbus Sans"
            "Luxi Mono"
            (/ myStaffSize 20)))
	system-count = #11
	markup-system-spacing #'basic-distance = #20
}

\score {
   \new Staff { \voice }
}