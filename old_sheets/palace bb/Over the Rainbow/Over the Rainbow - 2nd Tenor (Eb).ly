\version "2.14.2.1"

\header {
	title = "Over the Rainbow"
	composer = "Music by Harold Arlen"
   arranger = "Arranged by Dave Wolpe"
   poet = "Lyrics by E.Y.Harburg"
   instrument = "1st Tenor"
}

#(define-markup-command (underlinedTx layout props text) (markup?)
   (interpret-markup layout props
      (markup #:normalsize #:italic #:override '(offset . 5) #:underline #:smallCaps text)
   )
)

#(define-markup-command (defaultTx layout props text) (markup?)
  (interpret-markup layout props
      (markup #:normalsize #:italic #:smallCaps text)
   )
)

#(define-markup-command (index layout props text) (markup?)
  (interpret-markup layout props
      (markup #:huge #:override '(circle-padding . 0.5) #:circle text)
   )
)
   
tenorTwo = \relative c''
{
   \compressFullBarRests
   
   \time 4/4
   \key c \major
   
   
   \override Score.RehearsalMark #'self-alignment-X = #LEFT     
   \override TextSpanner #'(bound-details left text) = "rit. "
   \override TextSpanner #'(bound-details right padding) = #4
   \textSpannerDown
   
   \mark \markup {\underlinedTx "Rubato"}
   
   R1*3
   
   a2(\p\< b)\!\>
   
   \mark \markup{\underlinedTx "Tempo" \defaultTx "(not too fast)"}
   r8\! g( a4) r8 a( bes4)
   a2( f 
   g) r2
   a2(\mf\startTextSpan  f)\>
   
   \mark \markup \underlinedTx "A tempo"
   r2\!\stopTextSpan b\p(
   a f
   g1)
   a2(\< gis
   g1)\!\mf ~ g2 fis
   r4\startTextSpan c'\glissando a2\>
   
   \mark \markup {\underlinedTx "Rubato"}
   R1*2\!\stopTextSpan
   
   e1(^^\p\< 
   \mark \markup {\underlinedTx "Molto rit."}
   f1)\!\mf\>
   
   \bar "||" 
   \once \override Score.RehearsalMark #'self-alignment-X = #CENTER
   \mark \markup {\index A}
   
   R1\!^\markup {\underlinedTx "A tempo"}
   b2.(\p\< bes4 
   a1)\!\mp
   
   R1*4
   
   r4 g(\< c b
   \bar "||" 
   \once \override Score.RehearsalMark #'self-alignment-X = #CENTER
   \mark \markup {\index B}
   c2\!\mf a
   b) fis4\glissando( cis
   c2\> b'\! 
   b) r2
   
   R1*4
   
   \bar "||" 
   \once \override Score.RehearsalMark #'self-alignment-X = #CENTER
   \mark \markup {\index C}
   g1(  a)
   
   r4 g8(\< a bes2
   a2)\! r2
   
   r2 g(\p 
   a) a4(\< a
   b2\!\mf a) 
	r4 a2(\< d,4    
	
   \bar "||" 
   \once \override Score.RehearsalMark #'self-alignment-X = #CENTER
   \mark \markup {\index D}
	g2\f\! c2
	c2) r2
	R1*2
	
	r4\p\< a4( c2
	g2\! g
	a2. b4
	a2)
	
   r4 r8^\markup {\defaultTx "Unis."} g8(
   \bar "||" 
   \once \override Score.RehearsalMark #'self-alignment-X = #CENTER
   \mark \markup {\index E}
   e g e g e g e g
   f\< g f g f g f g
   a1\!\f ~ a2.)\> r4\! 
   
   r2 g\p( 
   a) a4(\< a
   b2\!\mf a)
   r4 a2(\< b4 
   \bar "||"
   \once \override Score.RehearsalMark #'self-alignment-X = #CENTER
   \mark \markup {\index F}
   c2\!\f a
   b) fis4(\glissando cis
   c2\> b'
   b)\! r
   
   R1*4
   
	\break
   \bar "||"
   \once \override Score.RehearsalMark #'self-alignment-X = #CENTER
   \mark \markup {\index G}
	R1
	
	\time 2/4	
	R2
	
	\time 4/4
   
   a2\p\<\startTextSpan f2
   g2\!\f\stopTextSpan^\markup {\underlinedTx "A tempo"} r2
   
   << d1\> {s2 s2\startTextSpan} >>
   g1\p\fermata\stopTextSpan
   
   \bar "|."
}

\paper { 
	myStaffSize = #20
	#(define fonts
		(make-pango-font-tree
			"Throw My Hands Up in the Air"
			"Nimbus Sans"
            "Luxi Mono"
            (/ myStaffSize 20)))
   system-count = #10
   markup-system-spacing #'basic-distance = #20
}

\score {
   \new Staff { \transpose ees bes \tenorTwo }
}