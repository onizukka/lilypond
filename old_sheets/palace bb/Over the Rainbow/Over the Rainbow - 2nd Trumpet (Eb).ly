\version "2.14.2.1"

\header {
	title = "Over the Rainbow"
	composer = "Music by Harold Arlen"
   arranger = "Arranged by Dave Wolpe"
   poet = "Lyrics by E.Y.Harburg"
   instrument = "2nd Trumpet"
}

#(define-markup-command (underlinedTx layout props text) (markup?)
   (interpret-markup layout props
      (markup #:normalsize #:italic #:override '(offset . 5) #:underline #:smallCaps text)
   )
)

#(define-markup-command (defaultTx layout props text) (markup?)
  (interpret-markup layout props
      (markup #:normalsize #:italic #:smallCaps text)
   )
)

#(define-markup-command (index layout props text) (markup?)
  (interpret-markup layout props
      (markup #:huge #:override '(circle-padding . 0.5) #:circle text)
   )
)
   
voice = \relative c''
{
   \compressFullBarRests
   
   \time 4/4
   \key c \major
   
   
   \override Score.RehearsalMark #'self-alignment-X = #LEFT     
   \override TextSpanner #'(bound-details left text) = "rit. "
   \override TextSpanner #'(bound-details right padding) = #4
   \textSpannerDown
   
   \mark \markup {\underlinedTx "Rubato" \defaultTx "cup mute"}
   
   d1_\p^\marcato d^\marcato d^\marcato R1*1
   
   \mark \markup{\underlinedTx "Tempo" \defaultTx "(not too fast)"}
   R1*2
   
   r8\mf d (e [c] d2 ~) d2\startTextSpan r2
   
   \mark \markup {\underlinedTx "A tempo" \defaultTx "open"}
   R1*3\stopTextSpan
   
   \mark \markup {\underlinedTx "In hat" \defaultTx "(or stand)"} 
   r8\p fis,\< (gis [a] ais4 b\! ~ b2\>) r2\! 
   
   \mark \markup {\underlinedTx "To cup"}
   R1*2\startTextSpan
   
   \mark \markup {\underlinedTx "Rubato"}
   d1_\fp^\accent\stopTextSpan ~ d
   
   \mark \markup {\underlinedTx "Open"}
   \once \override TextSpanner #'(bound-details left text) = "molto rit. "
   R1*2\startTextSpan
   
   \bar "||" 
   \once \override Score.RehearsalMark #'self-alignment-X = #CENTER
   \mark \markup {\index A}
   
   R1*3\stopTextSpan^\markup {\underlinedTx "A tempo"}
   
   \mark \markup {\underlinedTx "Hat"} 
   r4\mf g, (b bes a2\> aes g\!) r2
   R1*2
   
   \bar "||"
   \once \override Score.RehearsalMark #'self-alignment-X = #CENTER
   \mark \markup {\index B}
   
   R1*3
   \mark \markup {\underlinedTx "Open"}
   r4\p g4 (bes bes 
   a2) r2^\markup {\underlinedTx "To cup"}
   R1*2
   r4\mf d2.^\accent\>
   
   \bar "||" 
   \once \override Score.RehearsalMark #'self-alignment-X = #CENTER
   \mark \markup {\index C}
   
   R1*2\!
   c2\p\< (cis d2\! b4\> b
   b2\p\!) r2^\markup {\underlinedTx "Open"}
   R1*2
   r4\mf c2\< (b4 
   
   \bar "||" 
   \once \override Score.RehearsalMark #'self-alignment-X = #CENTER
   \mark \markup {\index D}
   
   g1\f\! ~) g2 r2
   R1*5
   r4 d'2.^\accent\mf\> (
   
   \bar "||" 
   \once \override Score.RehearsalMark #'self-alignment-X = #CENTER
   \mark \markup {\index E}
   
   c1\p\! d\<)
   r4\f\! c4 (dis2 d4 cis\> c b b2\p\!) r2
   R1*2
   r4\mf d8\< (e f4 g
   
   \bar "||"
   \once \override Score.RehearsalMark #'self-alignment-X = #CENTER
   \mark \markup {\index F}
   
   a4\f gis g fis g2) r2 R1
   r4\mf g,4 (bes4 bes a2 aes\> g\p\!) r2 
   R1*2\startTextSpan
   
   \bar "||"
   \once \override Score.RehearsalMark #'self-alignment-X = #CENTER
   \mark \markup {\index G}
   
   
   R1\stopTextSpan^\markup {\underlinedTx "Rubato"}
   \time 2/4
   R2
   \time 4/4
   R1\startTextSpan
   
   \mark \markup {\underlinedTx "A tempo"}
   r8\f\stopTextSpan d' (e c d2) 
   
   \mark \markup {\underlinedTx "To harmon mute"}
   <<R1 {s2 s2\startTextSpan} >>
   e1\p\fermata\stopTextSpan
   
   \bar "||"
}

\paper { 
	myStaffSize = #20
	#(define fonts
		(make-pango-font-tree
			"Throw My Hands Up in the Air"
			"Nimbus Sans"
            "Luxi Mono"
            (/ myStaffSize 20)))
   system-count = #11
   markup-system-spacing #'basic-distance = #20
}

\score {
   \new Staff { \transpose ees bes \voice }
}