\version "2.14.2.1"

\header {
	title = "Over the Rainbow"
	composer = "Music by Harold Arlen"
   arranger = "Arranged by Dave Wolpe"
   poet = "Lyrics by E.Y.Harburg"
   instrument = "1st Tenor"
}

#(define-markup-command (underlinedTx layout props text) (markup?)
   (interpret-markup layout props
      (markup #:normalsize #:italic #:override '(offset . 5) #:underline #:smallCaps text)
   )
)

#(define-markup-command (defaultTx layout props text) (markup?)
  (interpret-markup layout props
      (markup #:normalsize #:italic #:smallCaps text)
   )
)

#(define-markup-command (index layout props text) (markup?)
  (interpret-markup layout props
      (markup #:huge #:override '(circle-padding . 0.5) #:circle text)
   )
)
   
tenorOne = \relative c'
{
   \compressFullBarRests
   
   \time 4/4
   \key c \major
   
   
   \override Score.RehearsalMark #'self-alignment-X = #LEFT     
   \override TextSpanner #'(bound-details left text) = "rit. "
   \override TextSpanner #'(bound-details right padding) = #4
   \textSpannerDown
   
   \mark \markup {\underlinedTx "Rubato"}
   
   R1*3
   
   c2(\p\< f)\!\>
   
   \mark \markup{\underlinedTx "Tempo" \defaultTx "(not too fast)"}
   r8\! g,( a4) r8 a( bes4)
   c2( b? 
   a) r8\mf g'( a[ e]
   f\startTextSpan g f g b,2)\>
   
   \mark \markup \underlinedTx "A tempo"
   r2\!\stopTextSpan d\p(
   c a
   b c)
   d1(\<
   c1)\!\mf ~ c
   r4\startTextSpan f\glissando c2\>
   
   \mark \markup {\underlinedTx "Rubato"}
   R1*2\!\stopTextSpan
   
   a(^^\p\< 
   \mark \markup {\underlinedTx "Molto rit."}
   c2\!\mf b)\>
   
   \bar "||" 
   \once \override Score.RehearsalMark #'self-alignment-X = #CENTER
   \mark \markup {\index A}
   
   R1\!^\markup {\underlinedTx "A tempo"}
   b2.(\p\< bes4 
   a1)\!\mp
   
   R1*4
   
   r4 g'(\< f2
   \bar "||" 
   \once \override Score.RehearsalMark #'self-alignment-X = #CENTER
   \mark \markup {\index B}
   e\!\mf dis 
   d) b4( ais
   a2\> f\! 
   e) r2
   
   R1*3
   
   r2 r4 g'(\p
   \bar "||" 
   \once \override Score.RehearsalMark #'self-alignment-X = #CENTER
   \mark \markup {\index C}
   d1) ~ d
   
   r4 g,8(\< a bes e a g 
   f\! g f g a4.)\> r8\!
   
   r2 d,(\p 
   c) c4(\<\glissando dis
   d2\!\mf c) 
   << 
      {\tiny 
		s1 s1
		s2 fis8(--^\markup {\defaultTx "3rd Trb.Cue"} eis) f(-- e)
		e2 c b\> bes
		s1\! s1 s1 s2} \\
      {
	   r4 c2(\< b4
	   \bar "||" 
	   \once \override Score.RehearsalMark #'self-alignment-X = #CENTER
	   \mark \markup {\index D}
	   d2\!\f aes 
	   g2) r2 
	   \once \override MultiMeasureRest #'staff-position = #-8 R1 
	   \once \override MultiMeasureRest #'staff-position = #-8 R1
	   r4\p c(\< aes2    
	   e'?\! d4 cis 
	   c2. f,4 
	   e2)}
   >>
   r4 r8^\markup {\defaultTx "Unis."} g'8(
   \bar "||" 
   \once \override Score.RehearsalMark #'self-alignment-X = #CENTER
   \mark \markup {\index E}
   e g e g e g e g
   f\< g f g f g f g
   a1\!\f ~ a2.)\> r4\! 
   
   r2 d,\p( 
   c) c4(\<\glissando dis
   d2\!\mf c)
   r4 c2(\< d4 
   \bar "||"
   \once \override Score.RehearsalMark #'self-alignment-X = #CENTER
   \mark \markup {\index F}
   e2\!\f dis 
   d) b4( ais
   a2\> f
   e)\! r
   
   R1*3
   
   << 
      {\tiny r2\startTextSpan 
		a(\mf\>^\markup {\defaultTx "3rd Trb.Cue"}\stopTextSpan
		g1\p^\markup {\underlinedTx "Rubato"}
		\time 2/4
		a2)} \\
      { \once \override MultiMeasureRest #'staff-position = #-8 R1
		\bar "||"
		\once \override Score.RehearsalMark #'self-alignment-X = #CENTER
		\mark \markup {\index G}
		\once \override MultiMeasureRest #'staff-position = #-8 R1
		\time 2/4	
		\once \override MultiMeasureRest #'staff-position = #-8 R2
	  }	
   >>
   
   \time 4/4
   
   c2\p\<\startTextSpan c4( b)
   d2\!\f\stopTextSpan^\markup {\underlinedTx "A tempo"} r8 g( a[ e]
   
   << a,1)\> {s2 s2\startTextSpan} >>
   d1\p\fermata\stopTextSpan
   
   \bar "|."
}

\paper { 
	myStaffSize = #20
	#(define fonts
		(make-pango-font-tree
			"Throw My Hands Up in the Air"
			"Nimbus Sans"
            "Luxi Mono"
            (/ myStaffSize 20)))
   system-count = #10
   markup-system-spacing #'basic-distance = #20
}

\score {
   \new Staff { \transpose ees bes \tenorOne }
}