\version "2.14.2.1"

\header {
	title = "Over the Rainbow"
	composer = "Music by Harold Arlen"
   arranger = "Arranged by Dave Wolpe"
   poet = "Lyrics by E.Y.Harburg"
   instrument = "Lead"
}

#(define-markup-command (underlinedTx layout props text) (markup?)
   (interpret-markup layout props
      (markup #:normalsize #:italic #:override '(offset . 5) #:underline #:smallCaps text)
   )
)

#(define-markup-command (defaultTx layout props text) (markup?)
  (interpret-markup layout props
      (markup #:normalsize #:italic #:smallCaps text)
   )
)

#(define-markup-command (index layout props text) (markup?)
  (interpret-markup layout props
      (markup #:huge #:override '(circle-padding . 0.5) #:circle text)
   )
)
   
lead = \relative c'
{
   \compressFullBarRests
   
   \time 4/4
   \key c \major
   
   
   \override Score.RehearsalMark #'self-alignment-X = #LEFT     
   \override TextSpanner #'(bound-details left text) = "rit. "
   \override TextSpanner #'(bound-details right padding) = #4
   \textSpannerDown
   
   \mark \markup {\underlinedTx "Rubato"}
   r8 g' a[ e] g4 d8 e
   f4 f f8 g c, d 
   e4 e e8 g b, c
   d1
   
   \mark \markup{\underlinedTx "Tempo" \defaultTx "(not too fast)"}
   e2 e
   \times 2/3 {e4 g f} e d
   c2. r4\startTextSpan
   R1
   
   \mark \markup \underlinedTx "A tempo"
   r8\stopTextSpan g' a[ e] g4 d8 e
   f4 f f8 g c, d 
   e4 e e8 g b, c
   d1
   
   r2 b4 c
   c' b a b
   g1\startTextSpan
   
   \mark \markup {\underlinedTx "Rubato"}
   r4\stopTextSpan a,8 c e g b c
   g1
   r4 a,8\< c e g b c
   
   \mark \markup {\underlinedTx "Molto rit."}
   d2\! g,2\>
   
   \bar "||" 
   \once \override Score.RehearsalMark #'self-alignment-X = #CENTER
   \mark \markup {\index A}
   c,2\! c'
   b4 g8 a b4 c
   c,2 a'
   g1
   
   r4 a,4 f'2
   e4 c8 d e4 f
   d4 b8 c d4 e
   c2 r
   
   \bar "||" 
   \once \override Score.RehearsalMark #'self-alignment-X = #CENTER
   \mark \markup {\index B}
   c c'
   b4 g8 a b4 c
   r c, a'2
   g2. r4
   
   a,2 f'2
   e4 c8 d e4 f
   d4 b8 c d4 e
   c2. r8 g'
   
   \bar "||" 
   \once \override Score.RehearsalMark #'self-alignment-X = #CENTER
   \mark \markup {\index C}
   e g e g e g e g 
   f g f g f g f g
   a2 a2( 
   a2.) r8 g
   
   e g e g e g e g
   fis a fis a fis\< a fis a 
   b2\! b
   d a
   
   \bar "||" 
   \once \override Score.RehearsalMark #'self-alignment-X = #CENTER
   \mark \markup {\index D}
   r4 c, c'2
   b4 g8 a b4 c
   r4 c, a'2
   g1\>
   a,2\! f'
   e4 c8 d e4 f
   d b8 c d4 e 
   c2. r4
   
   \bar "||" 
   \once \override Score.RehearsalMark #'self-alignment-X = #CENTER
   \mark \markup {\index E}
   R1*3
   r2 r4 r8 g'
   
   e g e g e g e g
   fis a fis a fis\< a fis a 
   b2\! b
   d a
   
   \bar "||"
   \once \override Score.RehearsalMark #'self-alignment-X = #CENTER
   \mark \markup {\index F}
   c c
   b4 g8 a b4 c
   r4 a a2\>
   g1\!
   
   r4 a, f'2
   e4 c8 d e4 f
   d b8 c d4 e
   c2\startTextSpan r4 r8 g'
   
	
	\bar "||"
	\once \override Score.RehearsalMark #'self-alignment-X = #CENTER
	\mark \markup {\index G}
	e\stopTextSpan^\markup {\underlinedTx "Rubato"} g e g e g e8. g16
	
	\time 2/4
	f8 g f g
	
	\time 4/4
   f4\<\startTextSpan g a b 
   
   c1\!\stopTextSpan^\markup {\underlinedTx "A tempo"}( c2\>) r2\!\startTextSpan
   
   R1\fermata\stopTextSpan
   
   \bar "|."
}

\paper { 
	myStaffSize = #20
	#(define fonts
		(make-pango-font-tree
			"Throw My Hands Up in the Air"
			"Nimbus Sans"
            "Luxi Mono"
            (/ myStaffSize 20)))
   system-count = #10
   markup-system-spacing #'basic-distance = #20
}

\score {
   \new Staff { \transpose ees bes \lead }
}