\version "2.14.2.1"

\header {
	title = "Over the Rainbow"
	composer = "Music by Harold Arlen"
   arranger = "Arranged by Dave Wolpe"
   poet = "Lyrics by E.Y.Harburg"
   instrument = "1st Trombone"
}

#(define-markup-command (underlinedTx layout props text) (markup?)
   (interpret-markup layout props
      (markup #:normalsize #:italic #:override '(offset . 5) #:underline #:smallCaps text)
   )
)

#(define-markup-command (defaultTx layout props text) (markup?)
  (interpret-markup layout props
      (markup #:normalsize #:italic #:smallCaps text)
   )
)

#(define-markup-command (index layout props text) (markup?)
  (interpret-markup layout props
      (markup #:huge #:override '(circle-padding . 0.5) #:circle text)
   )
)
   
voice = \relative c'
{
   \compressFullBarRests
   
   \time 4/4
   \key bes \major
   \clef bass
   
   
   \override Score.RehearsalMark #'self-alignment-X = #LEFT     
   \override TextSpanner #'(bound-details left text) = "rit. "
   \override TextSpanner #'(bound-details right padding) = #4
   \textSpannerDown
   
   \mark \markup {\underlinedTx "Rubato" \defaultTx "cup mute"}
   
   d1_\p^\marcato ees^\marcato d^\marcato R1*1
   
   \mark \markup{\underlinedTx "Tempo" \defaultTx "(not too fast)"}
   R1*2
   
   r8\mf d (f [c] d2 ees)\startTextSpan r2^\markup \defaultTx "open"
   
   \mark \markup \underlinedTx "A tempo"
   R1*2\stopTextSpan
   
   d1\p (e2\< ees d1\mf\!)
   
   \mark \markup {\underlinedTx "To cup"}
   R1*2\startTextSpan
   
   \mark \markup {\underlinedTx "Rubato"}
   d1_\fp^\accent\stopTextSpan (ees)
   
   \mark \markup {\underlinedTx "Open"}
   \once \override TextSpanner #'(bound-details left text) = "molto rit. "
   R1*2\startTextSpan
   
   \bar "||" 
   \once \override Score.RehearsalMark #'self-alignment-X = #CENTER
   \mark \markup {\index A}
   
   R1*2\stopTextSpan^\markup {\underlinedTx "A tempo"}
   
   r4\mp bes (d\< des c2\!\mf b)
   R1*3
   
   \mark \markup {\defaultTx "2nd Ten.Cue"}
   << 
      {\tiny r4 f\<( bes a bes2\! g a) r} \\
      {  R1 
         \once \override Score.RehearsalMark #'self-alignment-X = #CENTER
         \mark \markup {\index B}
         \bar "||" 
         R1*2 }
   >>
   R1
   
   r4\p d4 (ees d 
   d2) r2^\markup {\underlinedTx "To cup"}
   R1*2
   r4\mf d4^\accent\> (ees2)
   
   \bar "||" 
   \once \override Score.RehearsalMark #'self-alignment-X = #CENTER
   \mark \markup {\index C}
   
   R1*2\!
   d1\p\< (ees2\! d4\> des
   d?2\p\!) r2^\markup {\underlinedTx "Open"}
   R1*2
   r4\mf ees2\< (ees4 
   
   \bar "||" 
   \once \override Score.RehearsalMark #'self-alignment-X = #CENTER
   \mark \markup {\index D}
   
   d1\f\! ~) d2 e8--^\markup{\underlinedTx "Soli"} ([dis]) ees-- ([d])
   d2 (des c b\>)
   R1*3\!
   
   r4\mf d4^\accent\> (ees2
   
   \bar "||" 
   \once \override Score.RehearsalMark #'self-alignment-X = #CENTER
   \mark \markup {\index E}
   
   d1\p\! ees\<)
   r4\f\! d4 (cis2 f4 ees?\> ees des d?2\p\!) r2
   R1*2
   r4\mf ees2\< (ees4 
   
   \bar "||"
   \once \override Score.RehearsalMark #'self-alignment-X = #CENTER
   \mark \markup {\index F}
   
   d2\f cis c?) r2 R1
   r4\mf d4 (ees d d2 c\> bes\p\!) r2 
   R1
   
   r2\startTextSpan ees\mf\> (
   
   \bar "||"
   \once \override Score.RehearsalMark #'self-alignment-X = #CENTER
   \mark \markup {\index G}
   
   d1\p\!\stopTextSpan^\markup {\underlinedTx "Rubato"}
   \time 2/4
   ees2)
   \time 4/4
   \mark \markup{\defaultTx "2nd Ten.Cue"}
   << 
      {  \tiny g,2 (ees) s1} \\ 
      {   
         \override TextSpanner #'(bound-details left text) = "rit. "
         \textSpannerDown
         R1\<\startTextSpan \mark \markup {\underlinedTx "A tempo"} 
         r8\f\stopTextSpan\! d' (f c d2) } 
   >>
   
   << R1 {s2 s2\startTextSpan} >>
   d1\p\fermata\stopTextSpan
   
   \bar "||"
}

\paper { 
   system-count = #10
   markup-system-spacing #'basic-distance = #20
}

\score {
   \new Staff { \voice }
}