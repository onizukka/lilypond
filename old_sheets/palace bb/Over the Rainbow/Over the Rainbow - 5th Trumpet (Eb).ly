\version "2.14.2.1"

\header {
	title = "Over the Rainbow"
	composer = "Music by Harold Arlen"
   arranger = "Arranged by Dave Wolpe"
   poet = "Lyrics by E.Y.Harburg"
   instrument = "5th Trumpet"
}

#(define-markup-command (underlinedTx layout props text) (markup?)
   (interpret-markup layout props
      (markup #:normalsize #:italic #:override '(offset . 5) #:underline #:smallCaps text)
   )
)

#(define-markup-command (defaultTx layout props text) (markup?)
  (interpret-markup layout props
      (markup #:normalsize #:italic #:smallCaps text)
   )
)

#(define-markup-command (index layout props text) (markup?)
  (interpret-markup layout props
      (markup #:huge #:override '(circle-padding . 0.5) #:circle text)
   )
)
   
voice = \relative c''
{
   \compressFullBarRests
   
   \time 4/4
   \key c \major
   
   
   \override Score.RehearsalMark #'self-alignment-X = #LEFT     
   \override TextSpanner #'(bound-details left text) = "rit. "
   \override TextSpanner #'(bound-details right padding) = #4
   \textSpannerDown
   
   \mark \markup {\underlinedTx "Rubato" \defaultTx "cup mute"}
   
   c1_\p^\marcato c^\marcato c^\marcato R1*1
   
   \mark \markup{\underlinedTx "Tempo" \defaultTx "(not too fast)"}
   R1*2
   
   r8\mf c (d [a] c2 ~) c2\startTextSpan r2
   
   \mark \markup {\underlinedTx "A tempo" \defaultTx "open"}
   R1*3\stopTextSpan
   
   \mark \markup {\underlinedTx "In hat" \defaultTx "(or stand)"} 
   r8\p fis,\< (gis [a] ais4 b\! ~ b2\>) r2\! 
   
   \mark \markup {\underlinedTx "To cup"}
   R1*2\startTextSpan
   
   \mark \markup {\underlinedTx "Rubato"}
   c1_\fp^\accent\stopTextSpan ~ c
   
   \mark \markup {\underlinedTx "Open"}
   \once \override TextSpanner #'(bound-details left text) = "molto rit. "
   R1*2\startTextSpan
   
   \bar "||" 
   \once \override Score.RehearsalMark #'self-alignment-X = #CENTER
   \mark \markup {\index A}
   
   R1*3\stopTextSpan^\markup {\underlinedTx "A tempo"}
   
   \mark \markup {\underlinedTx "Hat"} 
   r4\mf g (b bes a2\> aes g\!) r2
   R1*2
   
   \bar "||"
   \once \override Score.RehearsalMark #'self-alignment-X = #CENTER
   \mark \markup {\index B}
   
   R1*3
   \mark \markup {\underlinedTx "Open"}
   r4\p g4 (f e 
   e2) r2^\markup {\underlinedTx "To cup"}
   R1*2
   r4\mf c'2.^\accent\>
   
   \bar "||" 
   \once \override Score.RehearsalMark #'self-alignment-X = #CENTER
   \mark \markup {\index C}
   
   R1*2\!
   g1\p\< (a2\! f4\> f
   e2\p\!) r2^\markup {\underlinedTx "Open"}
   R1*2
   r4\mf f2\< (f4 
   
   \bar "||" 
   \once \override Score.RehearsalMark #'self-alignment-X = #CENTER
   \mark \markup {\index D}
   
   d1\f\! c2) r2
   R1*5
   r4 c'2.^\accent\mf\> (
   
   \bar "||" 
   \once \override Score.RehearsalMark #'self-alignment-X = #CENTER
   \mark \markup {\index E}
   
   g1\p\! a\<)
   r4\f\! fis4 (a2 g4 g\> f? f e2\p\!) r2
   R1*2
   r4\mf a2\< (b4 
   
   \bar "||"
   \once \override Score.RehearsalMark #'self-alignment-X = #CENTER
   \mark \markup {\index F}
   
   c1\f b2) r2 R1
   r4\mf g4 (f e 
   e2 d\> e\p\!) r2 
   R1*2\startTextSpan
   
   \bar "||"
   \once \override Score.RehearsalMark #'self-alignment-X = #CENTER
   \mark \markup {\index G}
   
   
   R1\stopTextSpan^\markup {\underlinedTx "Rubato"}
   \time 2/4
   R2
   \time 4/4
   R1\startTextSpan
   
   \mark \markup {\underlinedTx "A tempo"}
   r8\f\stopTextSpan c' (d a c2) 
   
   \mark \markup {\underlinedTx "To harmon mute"}
   <<R1 {s2 s2\startTextSpan} >>
   d1\p\fermata\stopTextSpan
   
   \bar "||"
}

\paper { 
   system-count = #11
   markup-system-spacing #'basic-distance = #20
}

\score {
   \new Staff { \transpose ees bes \voice }
}