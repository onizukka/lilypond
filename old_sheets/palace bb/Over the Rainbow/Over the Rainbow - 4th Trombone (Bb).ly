\version "2.14.2.1"

\header {
	title = "Over the Rainbow"
	composer = "Music by Harold Arlen"
   arranger = "Arranged by Dave Wolpe"
   poet = "Lyrics by E.Y.Harburg"
   instrument = "4th Trombone"
}

#(define-markup-command (underlinedTx layout props text) (markup?)
   (interpret-markup layout props
      (markup #:normalsize #:italic #:override '(offset . 5) #:underline #:smallCaps text)
   )
)

#(define-markup-command (defaultTx layout props text) (markup?)
  (interpret-markup layout props
      (markup #:normalsize #:italic #:smallCaps text)
   )
)

#(define-markup-command (index layout props text) (markup?)
  (interpret-markup layout props
      (markup #:huge #:override '(circle-padding . 0.5) #:circle text)
   )
)
   
voice = \relative c'''
{
   \compressFullBarRests
   
   \time 4/4
   \key bes \major
   
   
   \override Score.RehearsalMark #'self-alignment-X = #LEFT     
   \override TextSpanner #'(bound-details left text) = "rit. "
   \override TextSpanner #'(bound-details right padding) = #4
   \textSpannerDown
   
   \mark \markup {\underlinedTx "Rubato" \defaultTx "cup mute"}
   
   f1_\p^\marcato f,^\marcato f'^\marcato R1*1
   
   \mark \markup{\underlinedTx "Tempo" \defaultTx "(not too fast)"}
   R1*2
   
   r8\mf bes,4-- bes8-- bes2 (f\startTextSpan) r2^\markup \defaultTx "open"
   
   \mark \markup \underlinedTx "A tempo"
   R1*2\stopTextSpan
   
   bes2\p( g a2\< d, g1\mf\!)
   
   \mark \markup \underlinedTx "To cup"
   R1*2\startTextSpan
   
   \mark \markup \underlinedTx "Rubato"
   r4\mf\stopTextSpan f'2-- f,4-- ~ f f'2-- f,4--
   
   \mark \markup \underlinedTx "Open"
   \once \override TextSpanner #'(bound-details left text) = "molto rit. "
   R1*2\startTextSpan
   
   \bar "||" 
   \once \override Score.RehearsalMark #'self-alignment-X = #CENTER
   \mark \markup \index A
   
   R1\stopTextSpan^\markup \underlinedTx "A tempo"
   
   << { a'2.\p( aes4 g1\mp)} \\ {s2 s2\< s1\!}>>
   R1*5
   
   \bar "||" 
   \once \override Score.RehearsalMark #'self-alignment-X = #CENTER
   \mark \markup \index B
   
   R1*3
   
   r4\p bes,2 (bes4
   ees,2) r2^\markup \underlinedTx "To cup"
   R1*2
   r4\mf f'4^\accent\> (f,2)
   
   \bar "||" 
   \once \override Score.RehearsalMark #'self-alignment-X = #CENTER
   \mark \markup \index C
   
   R1*2\!
   f'1\p\< (c2\! f,\> 
   bes2\p\!) r2^\markup \underlinedTx "Open"
   R1*2
   r4\mf c\< (f,2
   
   \bar "||" 
   \once \override Score.RehearsalMark #'self-alignment-X = #CENTER
   \mark \markup {\index D}
   
   bes2\f\! aes g) fis4--^\markup{\underlinedTx "Soli"} (f--)
   e2 (ees d des\>)
   R1*3\!
   
   r4\mf f'4^\accent\> (f,2)
   
   \bar "||" 
   \once \override Score.RehearsalMark #'self-alignment-X = #CENTER
   \mark \markup {\index E}
   
   r4\p f2-- f4-- ~ f\< f2-- f4\glissando(e2 a) 
   d4( g,\> c f, bes2\!) r2
   R1*2
   r4\mf c4\<( f,2
   
   \bar "||"
   \once \override Score.RehearsalMark #'self-alignment-X = #CENTER
   \mark \markup {\index F}
   
   e2\f a d,) r2 R1
   r4\mf bes'2 (bes4 ees,2 aes4\> ges f2\p\!) r2 
   R1
   
   r2\startTextSpan f'4\mf\> (f,)
   
   \bar "||"
   \once \override Score.RehearsalMark #'self-alignment-X = #CENTER
   \mark \markup {\index G}
   
   bes2\p\!\stopTextSpan^\markup \underlinedTx "Rubato"( b
   \time 2/4
   c2)
   \time 4/4
   R1\startTextSpan 
   \mark \markup {\underlinedTx "A tempo"} 
   r8\f\stopTextSpan\! bes4-- bes8-- bes2 
   
   << R1 {s2 s2\startTextSpan} >>
   bes1\p\fermata\stopTextSpan
   
   \bar "||"
}

\paper { 
   system-count = #10
   markup-system-spacing #'basic-distance = #20
}

\score {
   \new Staff { \transpose bes c \voice }
}