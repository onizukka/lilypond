\version "2.14.2.1"

\header {
	title = "Over the Rainbow"
	composer = "Music by Harold Arlen"
   arranger = "Arranged by Dave Wolpe"
   poet = "Lyrics by E.Y.Harburg"
   instrument = "2nd Trombone"
}

#(define-markup-command (underlinedTx layout props text) (markup?)
   (interpret-markup layout props
      (markup #:normalsize #:italic #:override '(offset . 5) #:underline #:smallCaps text)
   )
)

#(define-markup-command (defaultTx layout props text) (markup?)
  (interpret-markup layout props
      (markup #:normalsize #:italic #:smallCaps text)
   )
)

#(define-markup-command (index layout props text) (markup?)
  (interpret-markup layout props
      (markup #:huge #:override '(circle-padding . 0.5) #:circle text)
   )
)
   
voice = \relative c'
{
   \compressFullBarRests
   
   \time 4/4
   \key bes \major
   \clef bass
   
   
   \override Score.RehearsalMark #'self-alignment-X = #LEFT     
   \override TextSpanner #'(bound-details left text) = "rit. "
   \override TextSpanner #'(bound-details right padding) = #4
   \textSpannerDown
   
   \mark \markup {\underlinedTx "Rubato" \defaultTx "cup mute"}
   
   bes1_\p^\marcato bes^\marcato bes^\marcato R1*1
   
   \mark \markup{\underlinedTx "Tempo" \defaultTx "(not too fast)"}
   R1*2
   
   r8\mf bes (c [g] bes2)~ bes\startTextSpan r2^\markup \defaultTx "open"
   
   \mark \markup \underlinedTx "A tempo"
   R1*2\stopTextSpan
   
   a2\p( bes c1\< bes1\mf\!)
   
   \mark \markup \underlinedTx "To cup"
   R1*2\startTextSpan
   
   \mark \markup \underlinedTx "Rubato"
   bes1_\fp^\accent\stopTextSpan ~ bes
   
   \mark \markup \underlinedTx "Open"
   \once \override TextSpanner #'(bound-details left text) = "molto rit. "
   R1*2\startTextSpan
   
   \bar "||" 
   \once \override Score.RehearsalMark #'self-alignment-X = #CENTER
   \mark \markup \index A
   
   R1*2\stopTextSpan^\markup \underlinedTx "A tempo"
   
   r4\mp bes (d\< des c2\!\mf b)
   R1*4
   
   \bar "||" 
   \once \override Score.RehearsalMark #'self-alignment-X = #CENTER
   \mark \markup \index B
   
   R1*3
   
   r4\p a4 (aes aes 
   g2) r2^\markup \underlinedTx "To cup"
   R1*2
   r4\mf bes2.^\accent\>
   
   \bar "||" 
   \once \override Score.RehearsalMark #'self-alignment-X = #CENTER
   \mark \markup \index C
   
   R1*2\!
   c2\p\< (b bes?\! a4\> a 
   a2\p\!) r2^\markup \underlinedTx "Open"
   R1*2
   r4\mf bes2\< (a4 
   
   \bar "||" 
   \once \override Score.RehearsalMark #'self-alignment-X = #CENTER
   \mark \markup {\index D}
   
   c1\f\! bes2) a4--^\markup{\underlinedTx "Soli"} (aes--)
   << { g1 (f) } {s1 s2 s2\>} >>
   R1*3\!
   
   r4\mf bes2.^\accent\> (
   
   \bar "||" 
   \once \override Score.RehearsalMark #'self-alignment-X = #CENTER
   \mark \markup {\index E}
   
   bes1\p\! bes\<)
   r4\f\! bes4 (bes2 c4 b\> bes a a2\p\!) r2
   R1*2
   r4\mf bes2\< (a4 
   
   \bar "||"
   \once \override Score.RehearsalMark #'self-alignment-X = #CENTER
   \mark \markup {\index F}
   
   bes2\f g f) r2 R1
   r4\mf a (aes aes g2 ges\> d\p\!) r2 
   R1
   
   r2\startTextSpan bes'\mf\> (
   
   \bar "||"
   \once \override Score.RehearsalMark #'self-alignment-X = #CENTER
   \mark \markup {\index G}
   
   a2\p\!\stopTextSpan^\markup {\underlinedTx "Rubato"} aes
   \time 2/4
   bes2)
   \time 4/4
   R1\startTextSpan 
   \mark \markup {\underlinedTx "A tempo"} 
   r8\f\stopTextSpan\! bes (c g bes2) 
   
   << R1 {s2 s2\startTextSpan} >>
   c1\p\fermata\stopTextSpan
   
   \bar "||"
}

\paper { 
   system-count = #10
   markup-system-spacing #'basic-distance = #20
}

\score {
   \new Staff { \voice }
}