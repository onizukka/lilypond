﻿\version "2.16.1"

secondTenor = {

	\compressFullBarRests

	\times 4/4
	\key fis \minor
	
    \partial 4
    gis8 cis
    
    \bar ".|"
    fis2 cis4 cis8 d
    r8 fis ~ fis2 a,8 cis
    f8 f ~ f4 cis4 cis8 a ~
    a2 r4 gis8 cis
    
    fis2 cis4 cis8 d
    r8 fis ~ fis2 f8 gis
    a8[ a] gis[ gis] gis gis f4
    a,8 b4 fis8 ~ fis2
    
    \break
    \bar ".|"
    \repeat percent 2 {
        r8 fis a[ b] c[ b] a[ fis]
        a4 fis8 gis ~ gis2
    }
    
    \bar "|:"
	\repeat volta 2 { R1*15 }
	\alternative {
		{ R1 }
		{ r2 r4 gis8 cis  }
	}
    
    \break
    fis2 cis4 cis8 d
    r8 fis ~ fis2 a,8 cis
    f8 f ~ f4 cis4 cis8 a ~
    a2 r4 gis8 cis
    
    fis2 cis4 cis8 d
    r8 fis ~ fis4 b,2
    a2 d
    f2 f
    
    fis?1^\fermata
    
    \bar "|."
}


%{
\include "gab-commons-1.0.ly"
\include "layout.ly"
\include "markup.ly"

\header {
    instrument = "2nd Tenor Sax (Eb)"
}

{ << \transpose ees bes, \relative c'' \secondTenor \\ \scoreMarkup >> }

\paper {  
	% Definición de fuente. No sé para qué sirve "myStaffSize".
	myStaffSize = #20 #(define fonts (make-pango-font-tree "Throw My Hands Up in the Air" "Nimbus Sans" "Luxi Mono" (/ myStaffSize 20)))
	
	% distancia entre sistemas
	system-system-spacing #'basic-distance = #15
	
	% distancia entre el título y el primer sistema
	markup-system-spacing #'basic-distance = #20
}
%}