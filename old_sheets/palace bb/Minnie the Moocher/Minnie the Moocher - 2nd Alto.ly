﻿\version "2.16.1"

secondAlto = {

	\compressFullBarRests

	\times 4/4
	\key cis \minor
	
    \partial 4
    gis8 c
    
    \bar ".|"
    e2 cis?4 cis8 cis
    r8 e ~ e2 gis,8 cis
    fis8 fis ~ fis4 dis4 c8 gis ~
    gis2 r4 gis8 c
    
    e2 cis?4 cis8 cis
    r8 e ~ e2 dis8 fis
    gis8[ gis] fis[ fis] fis fis dis4
    gis,8 gis4 e8 ~ e2
    
    \break
    \bar ".|"
    \repeat percent 2 {
        r8 cis e[ fis] g[ fis] e[ cis]
        e4 cis8 dis ~ dis2
    }
    
    \bar "|:"
	\repeat volta 2 { R1*15 }
	\alternative {
		{ R1 }
		{ r2 r4 gis8 c  }
	}
    
    \break
    e2 cis?4 cis8 cis
    r8 e ~ e2 gis,8 cis
    fis8 fis ~ fis4 dis4 c8 gis ~
    gis2 r4 gis8 c
    
    e2 cis?4 cis8 cis
    r8 e ~ e4 gis,2
    gis2 cis
    dis2 dis
    
    gis,1^\fermata
    
    \bar "|."
}


%{
\include "gab-commons-1.0.ly"
\include "layout.ly"
\include "markup.ly"

\header {
    instrument = "2nd Alto Sax"
}

{ << \relative c'' \secondAlto \\ \scoreMarkup >> }

\paper {  
	% Definición de fuente. No sé para qué sirve "myStaffSize".
	myStaffSize = #20 #(define fonts (make-pango-font-tree "Throw My Hands Up in the Air" "Nimbus Sans" "Luxi Mono" (/ myStaffSize 20)))
	
	% distancia entre sistemas
	system-system-spacing #'basic-distance = #15
	
	% distancia entre el título y el primer sistema
	markup-system-spacing #'basic-distance = #20
}
%}