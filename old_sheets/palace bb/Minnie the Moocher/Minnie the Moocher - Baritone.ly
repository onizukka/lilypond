﻿\version "2.16.1"

baritone = {

	\compressFullBarRests

	\times 4/4
	\key cis \minor
	
    \partial 4
    cis8 e
    
    \bar ".|"
    gis2 fis4 e8 fis
    r8 gis ~ gis2 cis,8 e
    gis8 gis ~ gis4 fis4 e8 cis ~
    cis2 r4 cis8 e
    
    gis2 fis4 e8 fis
    r8 gis ~ gis2 fis8 gis
    cis8[ cis] a[ a] gis gis fis4
    cis8 bis4 gis8 ~ gis2
    
    \break
    \bar ".|"
    \repeat percent 2 {
        r8 cis, e[ fis] g[ fis] e[ cis]
        e4 cis8 dis ~ dis2
    }
    
    \bar "|:"
	\repeat volta 2 { R1*15 }
	\alternative {
		{ R1 }
		{ r2 r4 cis'8 e8  }
	}
    
    \break
    gis2 fis4 e8 fis
    r8 gis ~ gis2 cis,8 e
    gis8 gis ~ gis4 fis4 e8 cis ~
    cis2 r4 cis8 e
    
    gis2 fis4 e8 fis
    r8 gis ~ gis4 e2
    cis2 fis
    gis2 gis
    
    e1^\fermata
    
    \bar "|."
}


%{
\include "gab-commons-1.0.ly"
\include "layout.ly"
\include "markup.ly"

\header {
    instrument = "Baritone Sax"
}

{ << \relative c'' \baritone \\ \scoreMarkup >> }

\paper {  
	% Definición de fuente. No sé para qué sirve "myStaffSize".
	myStaffSize = #20 #(define fonts (make-pango-font-tree "Throw My Hands Up in the Air" "Nimbus Sans" "Luxi Mono" (/ myStaffSize 20)))
	
	% distancia entre sistemas
	system-system-spacing #'basic-distance = #15
	
	% distancia entre el título y el primer sistema
	markup-system-spacing #'basic-distance = #20
}
%}