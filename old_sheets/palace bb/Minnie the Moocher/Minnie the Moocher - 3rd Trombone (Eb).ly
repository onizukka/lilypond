﻿\version "2.16.1"

thirdTrombone = {

	\compressFullBarRests

	\times 4/4
	\key e \minor
	
    \partial 4
    fis8 b
    
    \bar ".|"
    e2 b4 b8 c
    r8 e ~ e2 g,8 b
    dis8 dis ~ dis4 dis4 b8 g ~
    g2 r4 fis8 b
    
    e2 b4 b8 c
    r8 e ~ e2 b8 e
    g8[ g] e[ e] dis dis b4
    g8 fis4 e8 ~ e2
    
    \break
    \bar ".|"
    \repeat percent 2 { R1*2 }
    
    \bar "|:"
	\repeat volta 2 { R1*15 }
	\alternative {
		{ R1 }
		{ r2 r4 fis8 b }
	}
    
    \break
    e2 b4 b8 c
    r8 e ~ e2 g,8 b
    dis8 dis ~ dis4 dis4 b8 g ~
    g2 r4 fis8 b
    
    e2 b4 b8 c
    r8 e ~ e4 b2
    g2 fis
    a2 a
    
    g1^\fermata
    
    \bar "|."
}



\include "gab-commons-1.0.ly"
\include "layout.ly"
\include "markup.ly"

\header {
    instrument = "3rd Trombone (Eb)"
}

{ << \transpose ees c \relative c'' \thirdTrombone \\ \scoreMarkup >> }

\paper {  
	% Definición de fuente. No sé para qué sirve "myStaffSize".
	myStaffSize = #20 #(define fonts (make-pango-font-tree "Throw My Hands Up in the Air" "Nimbus Sans" "Luxi Mono" (/ myStaffSize 20)))
	
	% distancia entre sistemas
	system-system-spacing #'basic-distance = #15
	
	% distancia entre el título y el primer sistema
	markup-system-spacing #'basic-distance = #20
}