﻿\version "2.16.1"

thirdTrumpet = {

	\compressFullBarRests

	\times 4/4
	\key fis \minor
	
    \partial 4
    fis8 a
    
    \bar ".|"
    cis2 b4 a8 b
    r8 cis ~ cis2 fis,8 a
    cis8 cis ~ cis4 b4 a8 fis ~
    fis2 r4 fis8 a
    
    cis2 b4 a8 b
    r8 cis ~ cis2 cis8 fis
    a8[ a] fis[ fis] f[ f] d4
    a8 gis4 fis8 ~ fis2
    
    \break
    \bar ".|"
    \repeat percent 2 { R1*2 }
    
    \bar "|:"
	\repeat volta 2 { R1*15 }
	\alternative {
		{ R1 }
		{ r2 r4 fis8 a  }
	}
    
    \break
    
    cis2 b4 a8 b
    r8 cis ~ cis2 fis,8 a
    cis8 cis ~ cis4 b4 a8 fis ~
    fis2 r4 fis8 a
    
    cis2 b4 a8 b
    r8 cis ~ cis4 a2
    fis2 d'
    cis2 cis
    
    fis1^\fermata
    
    \bar "|."
}


%{
\include "gab-commons-1.0.ly"
\include "layout.ly"
\include "markup.ly"

\header {
    instrument = "3rd Trumpet (Eb)"
}

{ << \transpose ees bes, \relative c'' \thirdTrumpet \\ \scoreMarkup >> }

\paper {  
	% Definición de fuente. No sé para qué sirve "myStaffSize".
	myStaffSize = #20 #(define fonts (make-pango-font-tree "Throw My Hands Up in the Air" "Nimbus Sans" "Luxi Mono" (/ myStaffSize 20)))
	
	% distancia entre sistemas
	system-system-spacing #'basic-distance = #15
	
	% distancia entre el título y el primer sistema
	markup-system-spacing #'basic-distance = #20
}
%}