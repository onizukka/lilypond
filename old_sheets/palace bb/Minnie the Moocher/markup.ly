scoreMarkup = {
		
    \partial 4
    s4
        
    s1*8
    
	\mark \markup \index A
    \repeat unfold 2 { s1*2 }
    
	\mark \markup \index B
	\repeat volta 2 { s1*15^\markup "(Vocal)" }
	\alternative {
		{ s1^\markup "ad libitum" }
		{ s1^\markup "last time" }
	}
    
	\mark \markup \index C
    s1*8
    
    s1
}