\header {
	title = "Minnie the Moocher"
    composer = "Music by Cab Calloway"
    arranger = "Arranged by Mike Henebry"
}

\paper {  
	% Definición de fuente. No sé para qué sirve "myStaffSize".
	myStaffSize = #20 #(define fonts (make-pango-font-tree "Throw My Hands Up in the Air" "Nimbus Sans" "Luxi Mono" (/ myStaffSize 20)))
	
	% distancia entre sistemas
	system-system-spacing #'basic-distance = #20
	
	% distancia entre el título y el primer sistema
	markup-system-spacing #'basic-distance = #20
}