﻿\version "2.16.1"

% Instrument Music
% -------------------------------------------------
secondTenor = {

	\compressFullBarRests
    
	\time 5/4
	\key fis \minor
    
    \small
    
    \override Glissando #'style = #'zigzag
    
    \set beatExceptions = #'()
    \set beatStructure = #'(3 2)
	
    \repeat percent 3 {
        fis2.\mp cis2
        fis2. cis2
    }
    fis2. cis2
    cis4.->\mf cis8-> ~ cis4 cis8(^\markup "unis." fis) a( b)
    
    \break
    \bar "|:"
    c8( cis) c8( b a4-.) cis,--\glissando e-.
    fis2. \times 2/3 {gis16( a gis} fis8 e4)--
    fis2. \times 2/3 {e16( fis e} cis8 b4)--
    cis2. cis8(^\markup "tacet 2nd time" fis) a( b)
    \bar ":|"
    
    \break
    \repeat percent 3 {
        cis,4.->\f cis8-> ~ cis4 cis2->
        b4.->^\markup "simile" b8-> ~ b4 b2->    
    }
    cis4. cis8 ~ cis4 cis2
    gis4. gis8 ~ gis4 cis8( fis) a( b)
    
    \break
    \bar "|:"
    c8( cis) c8( b a4-.) cis,--\glissando e-.
    fis2. \times 2/3 {gis16( a gis} fis8 e4)--
    fis2. \times 2/3 {e16( fis e} cis8 b4)--
    cis2. cis8(^\markup "tacet 2nd time" fis) a( b)
    \bar ":|"
    
    \break
    R1*8*5/4
    
    d,4-.\f^\markup "soli." cis8( a) d8( ees d4) cis8( d)
    a8( cis a8 gis) r8 e'( gis2)
    r8 d8( fis[ fis a, cis] d fis fis d
    cis e cis b) r8 gis( a2)
    
    r8 d( fis[ a]) r8 cis,( d d4.--)
    r8 e( gis[ b]) r8 dis,( e e4.--)
    r8 e( g[ b]) r8 c,( e e4.--)
    b8( c b gis) r8 fis gis2\glissando
    
    \bar "|:"
    cis'8\ff^\markup "unis." e ~ e2 ~ e8 dis4 e16 dis 
    cis2. ~ cis8 cis4 d16 cis
    b2. ~ b8 g4 e8
    fis2. r2 
    \bar ":|"
    
    \break
    a,4.\f a8 r4 bes2
    c4. c8 r4 fis,?2
    ais4. ais8 r4 gis?2
    fis2. gis4( g-.)
    
    a4._\markup "cresc." a8 r4 bes2
    c4. c8 r4 fis,?2
    ais4. ais8 r4 gis?2
    b4.->\ff b8-> ~ b4 cis8( fis) a( b)
    
    \break
    \bar "|:"
    c8( cis) c8( b a4-.) cis,--\glissando e-.
    fis2. \times 2/3 {gis16( a gis} fis8 e4)--
    fis2. \times 2/3 {e16( fis e} cis8 b4)--
    cis2. cis8(^\markup "tacet 2nd time" fis) a( b)
    \bar ":|"
    
    \break
    fis2.\p_\markup "subito" cis2
    fis2. cis2
    fis2. cis2
    cis?2.\sfz\< cis2\ff\!
    
    c'8( cis) c( b) a([ fis]) cis4\glissando e-.
    cis4-^ r4 r4 r2
    
    \bar "|."
}

% Instrument Sheet
% -------------------------------------------------
%{
\include "imports/main.ly"

\header {
    instrument = "2nd Tenor Sax (Eb)"
}

% { << \transpose ees bes, \relative c'' \secondTenor \\ \scoreMarkup >> }
%}