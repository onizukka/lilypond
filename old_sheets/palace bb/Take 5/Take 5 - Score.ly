﻿\version "2.16.1"

\include "imports/main.ly"

\include "Take 5 - 2nd Tenor.ly"
\include "Take 5 - 1st Trumpet.ly"
\include "Take 5 - 1st Trombone.ly"
\include "Take 5 - 4th Trombone.ly"

% Para pintar solamente alguna de las partes, 
% hay que comentar el \score con "%{" al principio y "%}" al final.
% Es probable que haya que ajusta el espacio entre sistemas (ver más abajo)
\score {
    <<
        \new StaffGroup <<
            \new Staff << 
                \set Staff.instrumentName = #"Sxs"
                \transpose ees bes, \relative c'' \secondTenor \\ \scoreMarkup 
            >>
            \new Staff { 
                \set Staff.instrumentName = #"Tps"
                \transpose ees bes \relative c'' \firstTrumpet 
            }
        >>
        
        \new StaffGroup <<
            \new Staff {
                \set Staff.instrumentName = #"Tb 1"
                \transpose bes c \relative c''' \firstTrombone
            }
            \new Staff {
                \set Staff.instrumentName = #"Tb 4"
                \transpose bes c \relative c''' \fourthTrombone
            }
        >>
    >>
}

\header {
    instrument = "Score"
}

\paper {
	% distancia entre sistemas
    system-system-spacing #'basic-distance = #10
	
	% distancia entre el título y el primer sistema
	markup-system-spacing #'basic-distance = #10
}

#(set-default-paper-size "a4" 'landscape)