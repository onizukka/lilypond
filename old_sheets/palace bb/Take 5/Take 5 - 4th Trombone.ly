﻿\version "2.16.1"

% Instrument Music
% -------------------------------------------------
fourthTrombone = {

	\compressFullBarRests
    
	\time 5/4
	\key e \minor
    
    \small
    
    \override Glissando #'style = #'zigzag
    
    \set beatExceptions = #'()
    \set beatStructure = #'(3 2)
    \set beamHalfMeasure = ##f
	
    R1*4*5/4
    
    b8\mf^\markup "unis."( d ~ d2 ~ d8) cis4->( d16 cis 
    b2. ~ b8) b4-> c?16 b 
    a2. ~ a8 fis'4->( d8)
    e4.\f-> e8->( e4) e-^ r4
    
    \bar "|:"
    \repeat unfold 4 {
        e,2. b'2
    }
    \bar ":|"
    
    \repeat percent 3 {
        g4.\f-> g8-> ~ g4 g2->
        fis4.^\markup "simile" fis8 ~ fis4 fis2
    }
    g4. g8 ~ g4 g2
    a4. a8 ~ a4 a2
    
    
    \break
    \bar "|:"
    \repeat unfold 4 {
        e2. b'2
    }
    \bar ":|"
    
    R1*4*5/4
    
    b8^\markup "unis. (w/tpts.)"( d ~ d2 ~ d8) cis4->( d16 cis
    b2.) r4 b8 d 
    e8 e ~ e2 ~ e8 cis-> ~ cis8[ a]
    b2. r2
    
    \break
    R1*8*5/4
    
    \bar "|:"
    \repeat percent 4 {
        e,4.\f b'8 r8 e, b'2
    }
    \bar ":|"
    
    \break
    e,8\f^\markup "unis." g r e( c4-.) f8( g aes bes
    g bes) r g( ees4-.) e8( fis gis cis
    ais cis) r ais( fis?4-.) a8( b c e
    d4 \times 2/3 {b8 a g} b4-.) r2
    
    b8( c b bes a4-.) c4--\glissando f-.
    d8( ees d des c4-.) cis4--\glissando fis-.
    dis8( e dis d cis4-.) d4--\glissando g-.
    fis,4.->\ff fis8-> ~ fis4 fis-^ r4
    
    \break
    \bar "|:"
    \repeat unfold 4 {
        e2. b'2
    }
    \bar ":|"
    
    \break
    R1*3*5/4
    
    r4 r4 r4 bes,8( ees) g8( a)
    bes8( b) bes8( a) g([ e]) b4\glissando d-.
    fis4-^ r4 r4 r2
    
    \bar "|."
}


% Instrument Sheet
% -------------------------------------------------
%{
\include "imports/main.ly"

\header {
    instrument = "4th Trombone (Bb)"
}

% { << \relative c' \fourthTrombone \\ \scoreMarkup >> }
% { << \transpose bes c \relative c''' \fourthTrombone \\ \scoreMarkup >> }
%}