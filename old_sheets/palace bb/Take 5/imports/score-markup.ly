scoreMarkup = {
		
    \time 5/4
    \tempo "Moderato"
    
    s1*8*5/4
    
    \mark \markup \index A
    s1*4*5/4
    
    s1*8*5/4
    
    
    \mark \markup \index B
    s1*4*5/4
    
    s1*8*5/4
    
    \mark \markup \index C
    s1*8*5/4
    
    s1*4*5/4
    
    \mark \markup \index D
    s1*8*5/4
    
    s1*4*5/4
    
    s1*6*5/4
}