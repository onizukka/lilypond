#(set-global-staff-size 16)

\header {
    title = "Take 5"
    composer = "Music by Paul Desmond"
    arranger = "Arranged by Jerry Coker"
}

\paper {  
    % Definición de fuente. 
    % No sé para qué sirve "myStaffSize".
	myStaffSize = #20 #(
        define fonts 
        (make-pango-font-tree 
            "Throw My Hands Up in the Air" 
            "Nimbus Sans"
            "Luxi Mono" 
            (/ myStaffSize 20)
        )
    )
	
	% distancia entre sistemas
    system-system-spacing #'basic-distance = #18
	
	% distancia entre el título y el primer sistema
	markup-system-spacing #'basic-distance = #25
}