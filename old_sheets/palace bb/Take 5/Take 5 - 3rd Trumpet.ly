﻿\version "2.16.1"

% Instrument Music
% -------------------------------------------------
thirdTrumpet = {

	\compressFullBarRests
    
	\time 5/4
	\key fis \minor
    
    \small
    
    \override Glissando #'style = #'zigzag
    
    \set beatExceptions = #'()
    \set beatStructure = #'(3 2)
    \set beamHalfMeasure = ##f
	
    R1*4*5/4
    
    cis8\mf^\markup "unis."( e ~ e2 ~ e8) dis4->( e16 dis 
    cis2. ~ cis8) cis4-> d?16 cis 
    b2. ~ b8 gis4->( e8)
    fis4.\f-> fis8->( fis4) fis-^ r4
    
    \bar "|:"
    R1*3*5/4
    r2 r4 fis'8\f^\markup "unis."^\markup "tacet 1st time"( eis fis eis)
    \bar ":|"
    
    fis8( a4) fis8( d4) b8( cis d dis
    e8 gis4) e8( cis4) a8( b bis cis
    d8 fis4) d8( b4) gis8( a b bis
    cis bis cis d e4) e8( dis e eis
    
    fis8 a4) fis8( d4) b8( cis d dis
    e8 gis4) e8( cis4) a8( b bis cis
    d8 fis4) d8( b4) gis8([ b]) \times 2/3 {e16( f e} d8
    cis2. ~ cis8) r8 r4
    
    
    \break
    \bar "|:"
    e2.^\markup "2nd time only" ~ e8 dis4->( e16 dis 
    cis2. ~ cis8) cis4-> d?16 cis 
    b2. ~ b8 gis4->( e8)
    fis2. r2
    \bar ":|"
    
    
    fis8--\mf^\markup "unis. (a la Miles Davis)" fis-. r4 r4 r8 a( gis8[ e])
    fis8-- fis-. r4 r4 r2
    r4 \times 2/3 {fis8( a cis} b4 ~ b) gis8( e)
    fis8-- fis-. r4 r4 r2
    
    cis'8( e ~ e2 ~ e8) dis4->( e16 dis
    cis2.) r4 cis8 e 
    fis8 fis ~ fis2 ~ fis8 dis-> ~ dis8[ b]
    cis2. r2
    
    \break
    R1*8*5/4
    
    \bar "|:"
    r8 gis\ff r4 gis-. r8 ais4.->
    r8 a?^\markup "simile" r4 a r8 b4.
    r8 a r4 a r8 ais4. 
    gis2. r2
    \bar ":|"
    
    \break
    fis8\f^\markup "unis." a r fis( d4-.) g8( a bes c
    a c) r a( f4-.) fis8( gis? ais dis
    bis dis) r bis( gis?4-.) b8( cis d fis
    e4 \times 2/3 {cis8 b a} cis4-.) r2
    
    cis8( d cis c b4-.) d4--\glissando g-.
    e8( f e ees d4-.) dis4--\glissando gis?-.
    eis8( fis eis e dis4-.) e4--\glissando a-.
    dis,4.->\ff dis8-> ~ dis4 dis-^ r4
    
    \break
    \bar "|:"
    e2.^\markup "2nd time only" ~ e8 dis4->( e16 dis 
    cis2. ~ cis8) cis4-> d?16 cis 
    b2. ~ b8 gis4->( e8)
    fis2. r2
    \bar ":|"
    
    \break
    R1*3*5/4
    
    r4 r4 r4 c8( f) a8( b)
    c8( cis) c8( b) a([ fis]) cis4\glissando e-.
    a4-^ r4 r4 r2
    
    \bar "|."
}


% Instrument Sheet
% -------------------------------------------------
\include "imports/main.ly"

\header {
    instrument = "3rd Trumpet (Eb)"
}

% { << \relative c'' \thirdTrumpet \\ \scoreMarkup >> }
{ << \transpose ees bes \relative c'' \thirdTrumpet \\ \scoreMarkup >> }