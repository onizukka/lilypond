\version "2.16.1"
\include "imports/main.ly"


% Instrument Music
% -------------------------------------------------
trumpetTwo = {

  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0
  
  \partial 2 r4 r
  
  R1*4
  
  \repeat volta 2 {
     \repeat percent 3 {
       bes'4^.\f r4 r2
       bes,4^.\mp r4 r2
     }
     bes'4^. r4 r2
     r1
     
     \break
     \repeat percent 2 {
       f,2 ~ \times 2/3 {f8 a a} \times 2/3 {c a a}
     }
     e2 ~ \times 2/3 {e8 a a} \times 2/3 {c a a}
  }
  \alternative {
    { e2 r }
    { e2 r }
  }
  
  \break
  ees'2. ~ ees8^. f
  ees4^. r4 r2
  f2. ~ f8^. g
  f4^. r4 r2
  a2. ~ a8^. bes
  
  \break
  a4^. r4 r2
  \times 2/3 {c,8 c^. c^.} \times 2/3 {c c^. c^.} \times 2/3 {c c^. c^.} \times 2/3 {c c^. c^.}
  \times 2/3 {d d^. d^.} \times 2/3 {d d^. d^.} \times 2/3 {d d^. d^.} \times 2/3 {d d^. d^.}
  
  \break
  \repeat percent 3 {
    bes'4^.\f r4 r2
    bes,4^.\mp r4 r2
  }
  bes'4^. r4 r2
  r1
  
  \break
  \repeat percent 2 {
    f,2 ~ \times 2/3 {f8 a a} \times 2/3 {c a a}
  }
  e2 ~ \times 2/3 {e8 a a} \times 2/3 {c a a}
  e2 r2
  
  R1*8
  
  \break
  ees'2. ~ ees8^. f
  ees4^. r4 r2
  f2. ~ f8^. g
  f4^. r4 r2
  a2. ~ a8^. bes
  
  \break
  a4^. r4 r2
  \times 2/3 {c,8 c^. c^.} \times 2/3 {c c^. c^.} \times 2/3 {c c^. c^.} \times 2/3 {c c^. c^.}
  \times 2/3 {d d^. d^.} \times 2/3 {d d^. d^.} \times 2/3 {d d^. d^.} \times 2/3 {d d^. d^.}
  
  \break
  \repeat percent 3 {
    bes'4^.\f r4 r2
    bes,4^.\mp r4 r2
  }
  bes'4^. r4 r2
  r1
  
  \break
  \repeat percent 2 {
    f,2 ~ \times 2/3 {f8 a a} \times 2/3 {c a a}
  }
  e2 ~ \times 2/3 {e8 a a} \times 2/3 {c a a}
  e2 r2
  
  \break
  \repeat percent 2 {
    f2 ~ \times 2/3 {f8 a a} \times 2/3 {c a a}
  }
  e2 ~ \times 2/3 {e8 g g} \times 2/3 {bes g bes}
  e,4^. gis8 g ~ g2\fermata
}


% Instrument Sheet
% -------------------------------------------------

\header {
  instrument = "2nd Trumpet (Eb)"
}

\paper {  	
  % distancia entre sistemas
  system-system-spacing #'basic-distance = #13
  
  % distancia entre el título y el primer sistema
  markup-system-spacing #'basic-distance = #15
}

<<
  \scoreMarkup
  \new Staff << \transpose ees bes \relative c'' \trumpetTwo >>
>>
