\version "2.16.1"
\include "imports/main.ly"


% Instrument Music
% -------------------------------------------------
tromboneFour = {

  \jazzOn
  \key g \major
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0
  
  \partial 2 r4 r
  
  R1*4
  
  \repeat volta 2 {
     \repeat percent 3 {
       g4^.\f r4 r2
       g4^.\mp r4 r2
     }
     g4^. r4 r2
     r1
     
     \break
     \repeat percent 2 {
       c2 r
     }
     g2 r
  }
  \alternative {
    { g2 r }
    { r1 }
  }
  
  \break
  c2. ~ c8^. d
  c4^. r4 r2
  c2. ~ c8^. d
  c4^. r4 r2
  c2. ~ c8^. d
  
  \break
  c4^. r4 r2
  \times 2/3 {d8 d^. d^.} \times 2/3 {d d^. d^.} \times 2/3 {d d^. d^.} \times 2/3 {d d^. d^.}
  \times 2/3 {d8 d^. d^.} \times 2/3 {d d^. d^.} \times 2/3 {d d^. d^.} \times 2/3 {d d^. d^.}
  
  \break
  \repeat percent 3 {
    g,4^.\f r4 r2
    g4^.\mp r4 r2
  }
  g4^. r4 r2
  r1
     
  \break
  \repeat percent 2 {
     c2 r
  }
  \repeat percent 2 {
     g2 r
  }
  
  R1*8
  
   \break
  c2. ~ c8^. d
  c4^. r4 r2
  c2. ~ c8^. d
  c4^. r4 r2
  c2. ~ c8^. d
  
  \break
  c4^. r4 r2
  \times 2/3 {d8 d^. d^.} \times 2/3 {d d^. d^.} \times 2/3 {d d^. d^.} \times 2/3 {d d^. d^.}
  \times 2/3 {d8 d^. d^.} \times 2/3 {d d^. d^.} \times 2/3 {d d^. d^.} \times 2/3 {d d^. d^.}
  
  \break
  \repeat percent 3 {
    g,4^.\f r4 r2
    g4^.\mp r4 r2
  }
  g4^. r4 r2
  r1
     
  \break
  \repeat percent 2 {
     c2 r
  }
  \repeat percent 2 {
     g2 r
  }
  
  \break
  \repeat percent 2 {
     c2 r
  }
  g2 ~ \times 2/3 {g8^. d' d} \times 2/3 {f d f}
  g,4^. gis8 g ~ g2\fermata
}


% Instrument Sheet
% -------------------------------------------------

\header {
  instrument = "4th Trombone (Bb)"
}

\paper {  	
  % distancia entre sistemas
  system-system-spacing #'basic-distance = #13
  
  % distancia entre el título y el primer sistema
  markup-system-spacing #'basic-distance = #15
}

<<
  \scoreMarkup
  \new Staff << \transpose bes ees \relative c'' \tromboneFour >>
>>
