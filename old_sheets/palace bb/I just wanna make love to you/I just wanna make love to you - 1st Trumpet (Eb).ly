\version "2.16.1"
\include "imports/main.ly"


% Instrument Music
% -------------------------------------------------
trumpetOne = {

  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0
  
  \partial 2 r4 r
  
  R1*4
  
  \repeat volta 2 {
     \repeat percent 3 {
       c'4^.\f r4 r2
       c,4^.\mp r4 r2
     }
     c'4^. r4 r2
     r1
     
     \break
     \repeat percent 2 {
       a,2 ~ \times 2/3 {a8 c d} \times 2/3 {ees d c}
     }
     g2 ~ \times 2/3 {g8 c d} \times 2/3 {ees d c}
  }
  \alternative {
    { g2 r }
    { g2 r }
  }
  
  \break
  f'2. ~ f8^. g
  f4^. r4 r2
  a2. ~ a8^. bes
  a4^. r4 r2
  c2. ~ c8^. d
  
  \break
  c4^. r4 r2
  \times 2/3 {d,8 d^. d^.} \times 2/3 {d d^. d^.} \times 2/3 {d d^. d^.} \times 2/3 {d d^. d^.}
  \times 2/3 {g g^. g^.} \times 2/3 {g g^. g^.} \times 2/3 {g g^. g^.} \times 2/3 {g g^. g^.}
  
  \break
  \repeat percent 3 {
    c4^.\f r4 r2
    c,4^.\mp r4 r2
  }
  c'4^. r4 r2
  r1
     
  \break
  \repeat percent 2 {
    a,2 ~ \times 2/3 {a8 c d} \times 2/3 {ees d c}
  }
  g2 ~ \times 2/3 {g8 c d} \times 2/3 {ees d c}
  g2 r2
  
  R1*8
  
   \break
  f'2. ~ f8^. g
  f4^. r4 r2
  a2. ~ a8^. bes
  a4^. r4 r2
  c2. ~ c8^. d
  
  \break
  c4^. r4 r2
  \times 2/3 {d,8 d^. d^.} \times 2/3 {d d^. d^.} \times 2/3 {d d^. d^.} \times 2/3 {d d^. d^.}
  \times 2/3 {g g^. g^.} \times 2/3 {g g^. g^.} \times 2/3 {g g^. g^.} \times 2/3 {g g^. g^.}
  
  \break
  \repeat percent 3 {
    c4^.\f r4 r2
    c,4^.\mp r4 r2
  }
  c'4^. r4 r2
  r1
  
  \break
  \repeat percent 2 {
    a,2 ~ \times 2/3 {a8 c d} \times 2/3 {ees d c}
  }
  g2 ~ \times 2/3 {g8 c d} \times 2/3 {ees d c}
  g2 r2
  
  \break
  \repeat percent 2 {
    a2 ~ \times 2/3 {a8 c d} \times 2/3 {ees d c}
  }
  g2 ~ \times 2/3 {g8^. g g} \times 2/3 {bes g bes}
  c4^. cis8 c ~ c2\fermata
}


% Instrument Sheet
% -------------------------------------------------

\header {
  instrument = "1st Trumpet (Eb)"
}

\paper {  	
  % distancia entre sistemas
  system-system-spacing #'basic-distance = #13
  
  % distancia entre el título y el primer sistema
  markup-system-spacing #'basic-distance = #15
}

<<
  \scoreMarkup
  \new Staff << \transpose ees bes \relative c'' \trumpetOne >>
>>
