\header {
  title = "I just wanna make love to you"
  subtitle = "From Ettar James in \"At last\" (1961)"
  instrument = ""
  composer = "Willie Dixon"
  arranger = "Arr. By Philippe Marillia"
  poet = "Muddy Waters"
}

\paper {  
  % Definición de fuente. No sé para qué sirve "myStaffSize".
  myStaffSize = #20 #(define fonts (make-pango-font-tree "LilyJAZZText" "Nimbus Sans" "Luxi Mono" (/ myStaffSize 20)))
  
  % distancia entre sistemas
  system-system-spacing #'basic-distance = #10
  
  % distancia entre el título y el primer sistema
  markup-system-spacing #'basic-distance = #10
}