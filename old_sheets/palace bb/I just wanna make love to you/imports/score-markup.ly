scoreMarkup = \new Staff \with {
   \RemoveEmptyStaves
}
{
  % Oculta el primer pentagrama del markup
  \override Staff.VerticalAxisGroup #'remove-first = ##t
  \partial 2 s2
  
  s1*4
  
  s1*8
  
  \bar "||"
  s1*5
  
  \bar "||"
  s1*20
  
  \bar "||"
  \mark \markup \boxed "sax solo"
  s1*8
  
  \bar "||"
  \mark \markup \boxed "vocals"
  s1*24
  
  \bar "|."
}