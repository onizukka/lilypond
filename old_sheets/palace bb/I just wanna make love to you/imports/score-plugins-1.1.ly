% Funciones globales de utilidad
% ------------------------------



% multi-mark-engraver
% Aporta la posibilidad de pintar un rehearsal mark al final de una línea y al comienzo de la siguiente
% Obtenido desde http://lists.gnu.org/archive/html/lilypond-user/2011-08/msg00157.html
% -----------------------------------------------------------------------------------------------------
#(define (multi-mark-engraver ctx)
   (let ((texts '())
         (final-texts '())
         (events '()))

     `((start-translation-timestep
        . ,(lambda (trans)
             (set! final-texts '())))

       (listeners
        (mark-event
         . ,(lambda (trans ev)
              (set! events (cons ev events)))))

       (acknowledgers
        (break-alignment-interface
         . ,(lambda (trans grob source)
              (for-each (lambda (mark)
                          (set! (ly:grob-parent mark X) grob))
                        texts))))

       (process-music
        . ,(lambda (trans)
             (for-each
              (lambda (ev)
                (let* ((mark-grob
                        (ly:engraver-make-grob trans 'RehearsalMark ev))
                       (label (ly:event-property ev 'label))
                       (formatter (ly:context-property ctx 'markFormatter)))

                  (if (and (procedure? formatter)
                           (not (markup? label)))
                      (begin
                       (if (not (number? label))
                           (set! label
                                 (ly:context-property ctx 'rehearsalMark)))

                       (if (and (integer? label)
                                (exact? label))
                           (set! (ly:context-property ctx 'rehearsalMark)
                                 (1+ label)))

                       (if (number? label)
                           (set! label (apply formatter (list label ctx)))
                           (ly:warning "rehearsalMark must have
integer value"))))

                  (if (markup? label)
                      (begin
                       (set! (ly:grob-property mark-grob 'text) label)
                       (let ((dir (ly:event-property ev 'direction)))
                         (and (ly:dir? dir)
                              (set! (ly:grob-property mark-grob 'direction)
                                    dir))))
                      (ly:warning "mark label must be a markup object"))

                  (set! texts (cons mark-grob texts))))
              (reverse events))))

       (stop-translation-timestep
        . ,(lambda (trans)
             (if (pair? texts)
                 (let ((staves (ly:context-property ctx 'stavesFound))
                       (priority-index 0))
                   (for-each (lambda (grob)
                               (let ((my-priority (ly:grob-property
grob 'outside-staff-priority 1500)))
                                 (for-each (lambda (stave)

(ly:pointer-group-interface::add-grob grob 'side-support-elements
                                               stave))
                                           staves)
                                 (set! (ly:grob-property grob
'outside-staff-priority) (+ my-priority priority-index))
                                 (set! priority-index (1+ priority-index))
                                 (set! final-texts (cons grob final-texts))))
                             (reverse texts))
                     (set! texts '())
                     (set! events '())))))

        (finalize
         . ,(lambda (trans)
              (and (pair? final-texts)
                   (for-each (lambda (grob)
                               (set! (ly:grob-property grob 'break-visibility)
                                     end-of-line-visible))
                             final-texts)))))))


% Funciones originales
%{
myMark =
#(define-music-function (parser location text) (markup?)
  (make-music 'MarkEvent
              'label text))
			  
toCoda = {
	\tweak #'self-alignment-X #RIGHT
	\tweak #'break-visibility #begin-of-line-invisible
	\myMark \markup { \hspace #1.25 \raise #1.25 \musicglyph #"scripts.coda" }
}
%}

% Función maqueada.
% Se apoya en el código obtenido por consola al ejecutar { \displayMusic { \toCoda }}
eolMarkup =
#(define-music-function (parser location text) (markup?)
	(make-music 
		'MarkEvent 
		'tweaks (list 
			(cons (quote self-alignment-X) 1)
			(cons (quote break-visibility) #(#t #t #f))
		) 
		'label (markup
			#:line (
				#:hspace 1.25
				#:raise 1.25
				text
			)
		)
	)
)




% rest-score y merge-rests-on-positioning
% Permite realizar la fusión de los silencios entre dos voces en un mismo pentagrama.
% Obtenido desde https://code.google.com/p/lilypond/issues/detail?id=1228 (fichero merge-rests.ily)
% -------------------------------------------------------------------------------------------------

#(define (rest-score r)
  (let ((score 0)
	(yoff (ly:grob-property-data r 'Y-offset))
	(sp (ly:grob-property-data r 'staff-position)))
    (if (number? yoff)
	(set! score (+ score 2))
	(if (eq? yoff 'calculation-in-progress)
	    (set! score (- score 3))))
    (and (number? sp)
	 (<= 0 2 sp)
	 (set! score (+ score 2))
	 (set! score (- score (abs (- 1 sp)))))
    score))

#(define (merge-rests-on-positioning grob)
  (let* ((can-merge #f)
	 (elts (ly:grob-object grob 'elements))
	 (num-elts (and (ly:grob-array? elts)
			(ly:grob-array-length elts)))
	 (two-voice? (= num-elts 2)))
    (if two-voice?
	(let* ((v1-grob (ly:grob-array-ref elts 0))
	       (v2-grob (ly:grob-array-ref elts 1))
	       (v1-rest (ly:grob-object v1-grob 'rest))
	       (v2-rest (ly:grob-object v2-grob 'rest)))
	  (and
	   (ly:grob? v1-rest)
	   (ly:grob? v2-rest)
	   (let* ((v1-duration-log (ly:grob-property v1-rest 'duration-log))
		  (v2-duration-log (ly:grob-property v2-rest 'duration-log))
		  (v1-dot (ly:grob-object v1-rest 'dot))
		  (v2-dot (ly:grob-object v2-rest 'dot))
		  (v1-dot-count (and (ly:grob? v1-dot)
				     (ly:grob-property v1-dot 'dot-count -1)))
		  (v2-dot-count (and (ly:grob? v2-dot)
				     (ly:grob-property v2-dot 'dot-count -1))))
	     (set! can-merge
		   (and
		    (number? v1-duration-log)
		    (number? v2-duration-log)
		    (= v1-duration-log v2-duration-log)
		    (eq? v1-dot-count v2-dot-count)))
	     (if can-merge
		 ;; keep the rest that looks best:
		 (let* ((keep-v1? (>= (rest-score v1-rest)
				      (rest-score v2-rest)))
			(rest-to-keep (if keep-v1? v1-rest v2-rest))
			(dot-to-kill (if keep-v1? v2-dot v1-dot)))
		   ;; uncomment if you're curious of which rest was chosen:
		   ;;(ly:grob-set-property! v1-rest 'color green)
		   ;;(ly:grob-set-property! v2-rest 'color blue)
		   (ly:grob-suicide! (if keep-v1? v2-rest v1-rest))
		   (if (ly:grob? dot-to-kill)
		       (ly:grob-suicide! dot-to-kill))
		   (ly:grob-set-property! rest-to-keep 'direction 0)
		   (ly:rest::y-offset-callback rest-to-keep)))))))
    (if can-merge
	#t
	(ly:rest-collision::calc-positioning-done grob))))

	
% La solución ofrecida para fusionar los silencios de multi-compás no termina de funcionar.
% Se adopta la solución de añadir "\revert MultiMeasureRest #'staff-position" antes de cada silencio de multi-compás
%{
#(define merge-multi-measure-rest-on-Y-offset
  ;; Call this to get the 'Y-offset of a MultiMeasureRest.
  ;; It keeps track of other MultiMeasureRests in the same NonMusicalPaperColumn
  ;; and StaffSymbol. If two are found, delete one and return 0 for Y-offset of
  ;; the other one.
  (let ((table (make-weak-key-hash-table)))
    (lambda (grob)
      (let* ((ssymb (ly:grob-object grob 'staff-symbol))
             (nmcol (ly:grob-parent grob X))
             (ssymb-hash (begin
               (if (not (hash-ref table ssymb))
                   (hash-set! table ssymb (make-hash-table 1)))
               (hash-ref table ssymb)))
             (othergrob (hash-ref ssymb-hash nmcol)))
            (if (ly:grob? othergrob)
              (begin
                ;; Found the other grob in this staff/column, delete it and move ours
                (ly:grob-suicide! othergrob)
                (hash-remove! ssymb-hash nmcol)
                0)
              (begin
                ;; Just save this grob and return the default value
                (hash-set! ssymb-hash nmcol grob)
                (ly:staff-symbol-referencer::callback grob)))))))
%}

%{
mergeRestsOn = {
	\override Staff.RestCollision #'positioning-done = #merge-rests-on-positioning
	\override Staff.MultiMeasureRest #'Y-offset = #merge-multi-measure-rest-on-Y-offset
}

mergeRestsOff = {
	\revert Staff.RestCollision #'positioning-done
	\revert Staff.MultiMeasureRest #'Y-offset
}

mergeRests = \layout {
	\context {
		\Staff
		\override RestCollision #'positioning-done = #merge-rests-on-positioning
		\override MultiMeasureRest #'Y-offset = #merge-multi-measure-rest-on-Y-offset
	}
}
%}




% startParenthesis y endParenthesis
% http://comments.gmane.org/gmane.comp.gnu.lilypond.general/64087
% -------------------------------------------------------------------------------------------------

#(define ((my-stencils start) grob)
(let* ((par-list (parentheses-item::calc-parenthesis-stencils grob))
        (null-par (grob-interpret-markup grob (markup #:null))))
  (if start
     (list (car par-list) null-par)
     (list null-par (cadr par-list)))))

startParenthesis = #(define-music-function (parser location note) 
(ly:music?)
"Add an opened parenthesis to the left of `note"
#{
  \once \override ParenthesesItem #'stencils = #(my-stencils #t)
  \parenthesize $note
#})

endParenthesis = #(define-music-function (parser location note) (ly:music?)
"Add a closed parenthesis to the right of `note"
#{
  \once \override ParenthesesItem #'stencils = #(my-stencils #f)
  \parenthesize $note
#})





% Funciones propias
% -------------------------------------------------------------------------------------------------
						
% Índice con borde circular y un cómodo espacio interior (y no el que viene por defecto)
#(define-markup-command (boxindex layout props text) (markup?)
	(interpret-markup layout props
		(markup #:huge #:override '(box-padding . 0.5) #:box text)
	)
)

#(define-markup-command (boxed layout props text) (markup?)
	(interpret-markup layout props
		(markup #:huge #:override '(box-padding . 0.9) #:box text)
	)
)

rit =
#(make-music 'CrescendoEvent
             'span-direction START
             'span-type 'text
             'span-text "rit.")

rs = {
  \once \override Rest #'stencil = #ly:percent-repeat-item-interface::beat-slash
  \once \override Rest #'thickness = #0.48
  \once \override Rest #'slope = #1.7
  r4
}





% Añadido de funciones al contexto
% -------------------------------------------------------------------------------------------------
\layout {
	\context {
		\Score		
		% multi-mark-engraver
		\remove "Mark_engraver"
		\consists #multi-mark-engraver
		\consists "Tweak_engraver"
		% merge-rests-on-positioning
		\override RestCollision #'positioning-done = #merge-rests-on-positioning
		% La solución ofrecida para fusionar los silencios de multi-compás no termina de funcionar.
		% Se adopta la solución de añadir "\revert MultiMeasureRest #'staff-position" antes de cada silencio de multi-compás
		% \override MultiMeasureRest #'Y-offset = #merge-multi-measure-rest-on-Y-offset
	}
}