\version "2.18.2"


% Descripción de funcionalidades
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% PLUGINS
% - \eolMark:					mark al final de línea
% - \bendBefore: 				solución al bendBefore (i.e. dis8 \bendBefore)
% - \rit: 						crea una porción de ritardando que debe terminar como las dinámicas, con un \!
% - \improOn, \improOff:	negra de improvisación
% - \emptyStaff:				Imprime un pentagrama vacío
% - \emptyBar:					Imprime un compás invisible (útil para indentados)



% LAYOUT
% - El aspecto de los silencios multicompás es como los de las partituras de jazz
% - Margen extra de las articulaciones para evitar la colisión con las ligaduras
% - Glissandos bonitos



% PAPER
% - Se imprime el título, instrumento y número de página en todas las páginas
% - Solo se debe tocar el page-count (si eso). Sale bonito tal cual.

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Descripción de funcionalidades
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% 03-09-14: Layout
% Ahora las articulaciones van por dentro de las ligaduras. Parece que es la mejor solución



% 04-09-14: Layout
% Los creciendos y decreciendos tienen la línea un poco más gruesa
%
% Markup
% Añadidas guías para correcta posición y markup de la indicación de tempo
%
% Plugins
% Añadida función \emptyStave para imprimir un pentagrama vacío.
% En algunas partituras hay un pentagrama vacío antes del coda (lo cual mola bastante, por cierto)



% 05-09-14: Plugins
% Añadido a la función \emptyStave el esconder la armadura de la clave, que no estaba.



% 08-09-14: Layout
% Ahora no se muestra la cancelación de accidentales al haber un cambio de armadura



% 09-09-14: Chords
% Añadida sentencia para que los acordes menores salgan con un guión en vez de con una M minúscula



% 10-09-14: Plugins
% Mejorada la función \bendAfter.
% Ahora es genérica, y se puede definir cualquier texto en un lateral que haga las funciones de ornamento



% 15-09-14: Layout
% Añadido un poco más de margen a los reguladores



% 16-09-14: Plugins
% Mejorada la función para imprimir barras de improvisación.
% Antes se llamaba al comando \rs para pintar una barra que ocupaba una negra
% Se reemplaza \rs por \restImproOn y \restImproOff, siendo esta opción mucho más flexible.
% Cuando se activa \restImproOn, todo silencio se convierte en barra de improvisación, respetando su duración.



% 20-09-14: Plugins
% Mejorada la función para imprimir barras de improvisación.
% Se reemplaza \restImproOn y \restImproOff por \improOn y \improOff
% Ahora, también las notas del pentagrama salen como barra de improvisación.
% (los comandos \improvisationOn e \improvisationOff de lilypond no se llevan bien con LilyJAZZ)
% Se ha añadido un grob correspondiente a los slash de la fuente LilyJAZZ en el archivo lily-jazz.ly



% 20-09-14: Layout
% Comentada la línea que oculta la cancelación de accidentales al haber cambio de armadura.
% Realmente, la cancelación es muy bienvenida.



% 01-10-14: Plugins
% Renombrada la función \emptyStave a \emptyStaff (stave es el plural de staff) y 
% corregida (ya no muestra el número de compás).
% Recordar que después de usar \emptyStaff se necesita redefinir el 
% número de compás con \set Score.currentBarNumber = #xx
%
% Añadida la función \emptyBar que oculta un compás entero. Se puede usar para crear un 
% efecto de indentación en un pentagrama dado.
% Recordar que después de usar \emptyBar se necesita:
% - redefinir el número de compás con \set Score.currentBarNumber = #xx
% - volver a pintar la clave con \clef
% - volver a pintar la armadura con \key



% 15-10-14: Language
% Cambiado el lenguaje de entrada de notas del finlandes al inglés
% La diferencia es que ahora los sostenidos se escriben "-s" en vez de "-is"
% y los bemoles "-f" en vez de "-es".
%
% Estructura
% Para poder autoincluir los archivos de instrumento en otros 
% (para montar un midi, o un score), se han movido los plugins a un archivo separado
% y ahora se utiliza la función \includeOnce, evitando así el problema de las
% dependencias circulares.
%
% Global
% Todas las instrucciones que se colocaban justo antes de empezar la música están ahora
% recogidas en una función \global.
% A \global se le ha añadido instrucciones para que agrupe las corcheas de dos en dos
% en vez de cuatro en cuatro (el comportamiento por defecto).
%
% Plugins
% Se le ha aumentado el grosor de los bordes de la función \boxed



% 26-10-14: Chords
% Añadido el include "chord-exception.ily" que recoge una mejor notación de los acordes.
% Actualmente, recoge los acordes maj7, 7(b5), 7(#5), 7(b9), 7(#9), 7(add13), maj9(add13), 9(#11)
% Se irá añadiendo acordes a menudo.
 

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Dependencias
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% \includeOnce
% Incluye una dependencia solo una vez. Así se pueden incluir los archivos en otros 
% (para montar un score por ejemplo) sin liarla parda con dependencias cíclicas.

includeOnce = #(
	define-void-function (parser location filename) (string?) (
		if (not (defined? (string->symbol filename))) (
			begin (
				ly:parser-include-string parser (
					string-concatenate (list "\\include \"" filename "\"")
				)
			)
			(primitive-eval (list 'define (string->symbol filename) #t))
		)
	)
)



% LilyJAZZ
\includeOnce "includes/ignatzek-jazz-chords.ily"
\includeOnce "includes/lily-jazz.ily"



% Language
\language "english"



% Plugins
\includeOnce "includes/plugins-1.2.ily"



% Chord Exceptions
\includeOnce "includes/chord-exceptions.ily"

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~











% Disposición
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Cabecera
\header {
	title = "My Way"
	instrument = "Trombone IV"
	composer = ""
	arranger = ""
	poet = ""
	tagline = ""
}



% Márgenes, fuentes y distancias del papel
\paper {
	% El definir otro tipo de fuente no permite cambiar el tamaño global del staff. Es un bug conocigo
	#(define fonts (make-pango-font-tree "LilyJAZZText" "Nimbus Sans" "Luxi Mono" (/ 20 20)))

	% distancia entre la parte superior de la página y el título
	top-markup-spacing #'basic-distance = 5

	% distancia entre el título y el primer sistema
	markup-system-spacing #'basic-distance = 25

	% distancia entre el texto y el primer sistema 
	top-system-spacing #'basic-distance = 15

	% márgen inferior
	bottom-margin = 20
	
	% intentado del primer sistema
	indent = 0
	 
	% número de páginas
	% page-count = 1

	ragged-last-bottom = ##f 

	oddHeaderMarkup = \markup {
	 \on-the-fly #not-first-page \fill-line {
		\fromproperty #'page:page-number-string
		\fromproperty #'header:title
		\fromproperty #'header:instrument
	 }
	}

	evenHeaderMarkup = \markup {
	 \on-the-fly #not-first-page \fill-line {
		\fromproperty #'header:instrument
		\fromproperty #'header:title
		\fromproperty #'page:page-number-string
	 }
	}
}

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~







% Elementos comunes de la partitura
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
	% Oculta el primer pentagrama del markup
	\override Staff.VerticalAxisGroup #'remove-first = ##t

	% Indicación de tiempo
	% \once \override Score.RehearsalMark.X-offset = #5
	% \mark \jazzTempoMarkup "Easy Swing" c4 "112"

	s1*2
	\mark \markup \boxed "A"
	
	\bar "||"
	s1*16
	
	\mark \markup \boxed "B"
	\bar "||"
	s1*8
  	\mark \markup "(Quasi Horn)"
  	s1*8
	
	\mark \markup \boxed "C"
	\bar "||"
	s1*12
	
	\mark \markup \boxed "D"
	\bar "||"
	s1*8
  	\mark \markup "(Q.H.)"
  	s1*8
	
	\mark \markup {
		\column { \boxed "E" }
		\hspace #2
		\column { "(Q.H.)" }
	}
	\bar "||"
	s1*10
	s1*2
	
	\once \override Score.RehearsalMark.X-offset = #5
	\mark \markup {
		\column { \boxed "F" }
		\column { "(Brighter Tempo)" }
	}
	\bar "||"
	s1*5
	
	\bar "|."
}



% Función musical global
global = {
	% Para pintar todos los números de compás debajo del pentagrama
	% \override Score.BarNumber #'break-visibility = #end-of-line-invisible
	% \override Score.BarNumber #'direction = #DOWN
	% \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

	\jazzOn
	\compressFullBarRests
	\language "nederlands"

	% Arregla el problema de tener el scoreMarkup añadido
	\revert MultiMeasureRest #'staff-position
	\override Voice.Rest #'staff-position = #0	

	\set Timing.beamExceptions = #'()
	\set Timing.baseMoment = #(ly:make-moment 1/4)
	\set Timing.beatStructure = #'(1 1 1 1) 
}

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Elementos específicos del instrumento
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Cifrado
scoreChords = \chords {    
	\set minorChordModifier = \markup { "-" }
	\set chordNameExceptions = #chordNameExceptions
	\override ChordName.font-size = #2.5

	% c1:m f2:m g2:m7
}

% Música
tromboneIV = \relative c {
	
	\global
  
	\key c \major
	\clef bass
  	R1*2
  
  	R1*16
  	
  	\break
  	R1*8
  	
  	\break
  	c1(\mp\< g'\! a c\>)
  	
  	\break
  	g,1\! ~ 
  	g
  	r
  	r4 r8 c^^ g2^>\<(
  	
  	\break
  	c1\mf ~ c\> f,\! ~ f)
  	
  	\break
  	r2 r4 f(
  	g2 f\<
  	e1\!
  	a)
  	
  	\break
  	d,( g c,\> ~ c\!)
  	
  	\break
  	R1*8
  	
  	\break
  	c'1(\mp
  	g'
  	a
  	c2.) aes,4(
  	
  	\break
  	g1 ~
  	g
  	c ~
  	c4.) r8 g4^-^>\< g^-^>
  	
  	\break
  	\repeat percent 2 {
    	c4.^>\mf c8^^ c2^>
  	}
  	f4.^> f8^^ f4^-^> f^-^>
  	f^-^> r8 \times 2/3 {f16 f f} f4^-^> e8^^ e^^
  	
  	\break
  	a4.^> a16 a a4^-^> a^-^>
  	g^-^> r8 \times 2/3 {g16 g g} g4^-^> f^-^>
  	b4.^> b16 b b4^-^> b^-^>
  	c4.^> b8^^ a2^>
  	
  	\break
  	a4.^>\rit a8^^ a4^-^> d,^-^>
  	g2\!^> g,2\fermata
  	c1( ^\markup "(Directed slowly)" ~
  	c4) g2.^>\<
    	
  	\break
  	c1\f^> ~ 
  	c
  	r4 d,2.^>\<
  	r4\ff ees^-^>( d^-^> des^-^>
  	c1)\fermata
}

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

\book {
	\score {
		<<
			\scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\new Staff { \tromboneIV }
			>>
		>>
	}
}


