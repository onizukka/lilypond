\version "2.16.1"
\include "imports/main.ly"

% Instrument Music
% -------------------------------------------------
baritone = {
  \jazzOn
  \compressFullBarRests
  
  \key a \major
  
  R1*2
  
  R1*16
  
  \break
  a1(\mp gis g fis)
  \break
  b1( a gis a)
  \break
  a1(\< ~ a\! d,\> b\!)
  \break
  e1( ~ e a ~ a2) e(\<
  \break
  a1\mf ~ a d, ~ d2) d4( cis
  \break
  b1 e2 d cis1 fis)
  \break
  b,( e a, ~ a)\>
  \break
  cis'(\mp\! ~ cis ~ cis b4 ais2.)
  \break
  d1( ~ d ~ d d4 cis2.)
  \break
  a1( ~ a d, b2.) f'4(
  \break
  e1 ~ e a ~ a4.) r8 e4(\<^- e^-
  
  \repeat percent 2 {
     a4.)^>\mf a8^. a2^>
  }
  d,4.^> d8^. d2^>
  d4.^> d8^. d4^- cis8^^ cis^^
  
  \break
  b4.( b8 b2
  e4. e8 d2
  cis4. cis8 cis2
  fis4. fis8 fis2)
  
  \break
  cis4.(\rit cis8 cis2
  
  fis2\! ~ fis)\fermata
  a,1\f^> ~ a4 e'2.^>
  
  \break
  a,1^>\f ~ 
  a
  r4 b2.^>\<
  r4\ff c^-( b bes
  
  a1)\fermata
}


% Instrument Sheet
% -------------------------------------------------
%{
\header {
  instrument = "Baritone Sax"
}

\paper {  	
  % distancia entre sistemas
  system-system-spacing #'basic-distance = #21
  
  % distancia entre el título y el primer sistema
  markup-system-spacing #'basic-distance = #25
}

<<
  \scoreMarkup
  \new Staff << \transpose c f \relative c'' \baritone >>
>>
%}