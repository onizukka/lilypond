\version "2.16.1"
\include "imports/main.ly"

% Instrument Music
% -------------------------------------------------
clarinetThree = {
  
  \key d \major
  
  \mark \markup \boxed "as clarinet"
  R1*2
  
  R1*16
  
  fis1(\mp ~ fis ~ fis e4 dis2.)
  
  \break
  g1( ~ g g g4 fis2.)
  
  \break
  d1( c b d)
  
  \break
  a'1( ~ a g fis2 g)
  
  \break
  a4.(\mf d,8 d4 a'
  a4. c,8 c4 a'8 b
  a4. g8 g4 fis
  fis4. e8 e4) fis16( g a b
  
  \break
  d4. g,8 g4 d'4
  cis4 ~ cis16 b cis d e4 a, 
  e' a, a' e)
  r8 a,16( b cis d e fis b4 a
  
  \break
  b4. e,8 e4 fis8 g
  g4 a,) r4 g'(
  g4. b,8 b4 fis'16 g b, cis
  d1)\>
  
  \break
  d,1(\!\mp cis c b2 a)
  
  \break
  e'1( d cis d)
  
  \break
  d1( c b d)
  
  \break
  a'1( ~ 
  a
  g)
  r2 r4 \times 4/7 {a,16\<( b cis d e fis g}
  
  \break
  a2\mf ~ a8 d, a' a
  a2) r8 c,( a'[ d]
  d2 g,4 d'
  d2) r8 e( fis[ g]
  
  \break
  g4. fis8 e4. d8
  cis2) r4 a'(
  a4. g8 fis4. e8 
  d2) r4 b(
  
  \break
  b4.\rit cis8 d4 b
  cis2)\! ~ cis2\fermata
  b1(
  a4.) r8 r2
}

tenorOne = {
  \jazzOn
  \compressFullBarRests
  
  %\transpose ees bes \relative c' \clarinetThree
  \relative c' \clarinetThree
  
  \key a \major
  R1*3
  \mark \markup \boxed "as tenor"
  r4\ff c^-( b^- bes^-
  a1)\fermata 
}


% Instrument Sheet
% -------------------------------------------------

%{
\header {
  instrument = "1st Tenor"
}

\paper {  	
  % distancia entre sistemas
  system-system-spacing #'basic-distance = #21
  
  % distancia entre el título y el primer sistema
  markup-system-spacing #'basic-distance = #25
}

<<
  \scoreMarkup 
  \new Staff << \transpose c f \relative c'' \tenorOne >> 
>>
%}