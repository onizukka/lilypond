\version "2.16.1"

\include "imports/main.ly"

\include "My Way - 1st Alto.ly"
\include "My Way - 2nd Alto.ly"

\paper {
  #(set-paper-size "a4" 'landscape)
}

\score {
  <<
    \new StaffGroup <<
      \new Staff << \transpose c f \relative c'' \altoOne >>
      \new Staff << \transpose c f \relative c'' \altoTwo >>
    >>
  >>
}