scoreChords = \chords {
  s1*2
  s1*16
  
  c1 
  e:m 
  g:m6 
  a4:7sus4 a2.:7
  
  d1:m 
  d:m7 
  g:7 
  c4:sus4 c2.
  
  c1 
  g2:m7 c:7.9-
  f4 f:5+ f:6 f:maj7
  d1:dim5.7
  
  c1
  g2:7sus4 g:7
  f1
  c2 g:7
  
  c2 c:maj7
  c1:7
  f
  f
  
  d:m7
  g:7
  e:m7
  a:m7
  
  d:m7
  g:7
  f
  c
  
  c1
  e:m 
  g:m6 
  a4:7sus4 a2.:7
  
  d1:m 
  d:m7 
  g:7 
  c4:sus4 c2.
  
  c1 
  g2:m7 c:7.9-
  f4 f:5+ f:6 f:maj7
  d1:m7.5-
  
  c1
  g2:7sus4 g:7
  f1
  c2 g:7
  
  c2 c:maj7
  g:m7 c:7.9-
  f:maj7 f:6
  f:maj7 f:6
  
  d1:m7
  g:7
  e:m7
  a:m7
  
  d:m7
  g:7
  s1
  s1
  
  s1
  s1
  s1
  s4 ees d:9 des:maj7
  c1
}