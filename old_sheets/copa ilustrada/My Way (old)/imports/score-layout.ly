\header {
  title = "My Way"
  instrument = ""
  composer = ""
  arranger = ""
  lyrics = ""
}

\paper {    
  % Definición de fuente. No sé para qué sirve "myStaffSize".
  myStaffSize = #20 #(define fonts (make-pango-font-tree "LilyJAZZText" "Nimbus Sans" "Luxi Mono" (/ myStaffSize 20)))
  
  % distancia entre sistemas
  system-system-spacing #'basic-distance = #10
  
  % distancia entre el título y el primer sistema
  markup-system-spacing #'basic-distance = #10
}