\version "2.16.1"
\include "imports/main.ly"

% Instrument Music
% -------------------------------------------------

tromboneThree = {
  \jazzOn
  \compressFullBarRests
    
  \key c \major
  R1*2
  
  R1*16
  
  \break
  R1*8
  
  \break
  \mark \markup \boxed "quasi horn"
  g1(\mp 
  f2 e\<
  c4\! cis d e
  aes1\>)
  
  \break
  e1\!( 
  f)
  r
  r4 r8 g^^ g2^>\<(
  
  \break
  e1\mf ~ e\> c'\! e,2 f)
  
  \break
  d1( g\< e\! e)
  
  \break
  d( f c\> c\!)
  
  \break
  R1*8
  
  \break
  \mark \markup \boxed "q. h."
  g'1(\mp
  f2 e
  c4 cis d e
  aes2.) aes4(
  
  \break
  e1 
  f
  f
  e4.) r8 g4^-^>\< g^-^>
  
  \break
  g4.^>\mf g8^^ g2^>
  f4.^> f8^^ e2^>
  a4.^> a8^^ a4^-^> a^-^>
  a^-^> r8 \times 2/3 {a16 a a} a4^-^^ g8^^ a^^
  
  \break
  d,4.^> d16 d d4^-^> d^-^>
  d^-^> r8 \times 2/3 {d16 d d} d4^-^> b^-^>
  e4.^> e16 e e4^-^> e^-^>
  g4.^> e8^^ e2^>
  
  \break
  d4.^>\rit d8^^ d4^-^> c^-^>
  g'2\!^> g,2\fermata
  a'1(
  g4) g,2.^>\<
    
  \break
  g'2\f^> ~ g8( a g fis
  f?2 e)
  r4 d2.^>\<
  r4\ff bes^-^>( a^-^> aes^-^>
  g1)\fermata
}


% Instrument Sheet
% -------------------------------------------------
\header {
  instrument = "3rd Trombone"
}

\paper {  	
  % distancia entre sistemas
  system-system-spacing #'basic-distance = #23
  
  % distancia entre el título y el primer sistema
  markup-system-spacing #'basic-distance = #25
}

<<
  \scoreMarkup
  \new Staff << \transpose c f \transpose ees c' \relative c'' \tromboneThree >>
>>