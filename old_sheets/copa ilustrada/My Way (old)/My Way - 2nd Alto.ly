\version "2.16.1"
\include "imports/main.ly"

% Instrument Music
% -------------------------------------------------
clarinetTwo = {
  
  \key d \major
  
  \mark \markup \boxed "as clarinet"
  R1*2
  
  R1*16
  
  \repeat percent 2 { r4\mp fis8( a, a'2) }
  r8 e( fis[ a,] a'4\< fis
  b4.\! a16 gis a4\> b,)\!
  
  \break
  \repeat percent 2 { r4 g'8( b, b'2) }
  r8 fis( g[ a,] a'4\< e'~
  e\! d16\> cis d a fis2)\!
  
  \break
  r4 d,16( e fis8 a4\< fis'
  e a4\! ~ a4. c,8
  b4 g'2 fis4 
  fis \times 2/3 {e8 bes g} d'4\> e,\!
  
  \break
  fis2) r2
  R1*2
}

fluteTwo = {
  \key c \major
  \mark \markup \boxed "as flute"
  r2 r4 \times 4/7 {g'16(\< a b c d e f\! }
  
  g4.\mf c,8 c4 g'
  g4. bes,8 bes4 g'8 a
  g4. f8 f4 e
  e4. d8 d4) e16(\< f g a\!
  
  \break
  c4. f,8 f4 c'
  b ~ b16 a b c d4 g,
  d' g, g d)
  r8 g,16( a b c d e a4 g
  
  a4. d,8 d4 e8 f
  f4 g,) r f'(
  f4. a,8 a4 e'16 f c d
  e1)\>
  
  \repeat percent 2 { r4\mp g,8( e' e4 g,) }
  r4 e'8( d g2 ~
  g4 \times 2/3 {e8 f g} a,2)
  
  \break
  r4 a8( f' f4 a,)
  r4 a8( f'16 e f4 a,)
  g4.( f'8 f2 ~
  f4 \times 2/3 {e8 f g} c,2)
  
  \break
  R1*7
  r2 r4 \times 4/7 {g16(\< a b c d e f\! }
  
  \break
  g2\mf ~ g8 c, g' g
  g2) r8 bes,( g'[ c]
  c2 f,4 c'
  c2) r8 d,8( e[ f]
  
  \break
  f4. e8 d4. c8
  b2) r4 g'(
  g4. f8 e4. d8 
  c2) r4 a'(
  
  \break
  a4.\rit b8 c4 a 
  b2)\! ~ b\fermata
  a1(
  g4.) r8 r2
}

altoTwo = {
  
  \jazzOn
  \compressFullBarRests
  
  \transpose ees bes \relative c' \clarinetTwo
  \transpose ees c \relative c' \fluteTwo
  
  \key a \major
  R1*3
  \mark \markup "as alto"
  r4\ff bes^-( a^- a^-
  a1)\fermata 
}


% Instrument Sheet
% -------------------------------------------------
%{
\header {
  instrument = "2nd Alto"
}

\paper {  	
  % distancia entre sistemas
  system-system-spacing #'basic-distance = #22
  
  % distancia entre el título y el primer sistema
  markup-system-spacing #'basic-distance = #25
}

<<
  \scoreMarkup
  \new Staff << \transpose c f \relative c'' \altoTwo >> 
>>
%}