\version "2.18.2"
\language "english"

% Se debe incluir antes que lilyjazz. Si no, lilyjazz no funciona.
#(set-global-staff-size 19)

% includes
\include "main.ily"

% Cabecera
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\header {
	title = "Chuso"
	instrument = "Alto Sax & Tenor Sax"
	composer = "Sonífera Isla"
	arranger = ""
	poet = ""
	tagline = "Edition: gbpsms@gmail.com"
}

% Márgenes, fuentes y distancias del papel
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\paper {
}

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
	% Oculta el primer pentagrama del markup
	\override Staff.VerticalAxisGroup #'remove-first = ##t

	% Indicación de tiempo
	% \once \override Score.RehearsalMark.self-alignment-X = #LEFT
	% \once \override Score.RehearsalMark.extra-offset = #'(0 . 3)

	% s1*2
	% \once \override Score.RehearsalMark.X-offset = #5
	% \mark \markup \boxed "A"

  \mkup "(Intro ad.lib.)"
	\mark \jazzTempoMarkup "Medium Swing" c4 "155"
  s1

  \bar "||"
  \mkup "(Keyboard break)"
  s1*2

  \break
  \bar "[|:"
  s1*4
  \break
  s1*4
  \break
  s1*4
  \break
  \bar ":|]"
  s1*4
  s1*4

  \break
  \bar "[|:"
  \mkup "(Repeat 4 xs)"
  s1*2
  \bar ":|]"
  s1*2

  \mkup "(1st & 2nd: Keyboard solo, 3rd: Bass solo, 4th: 4's Sax & Drums)"
  \mkup "(Repeat 4 xs)"
  \bar "[|:"
  s1*4
  \break
  \mkup "(Drums)"
  s1*4
  \break
  \mkup "(Sax)"
  s1*4
  \break
  \mkup "(Drums)"
  s1*4
  \break
  \mkup "(Sax)"
  s1*4
  \break
  \mkup "(Drums)"
  s1*3
  \break
  s1*3
  \bar ":|]"

  \break
  s1*4
  \break
  s1*4
  \break
  s1*4

  \break
  s1*4
  \break
  s1*4
  \break
  s1*4
  s1*2
  s1*8

  \break
  \bar "[|:"
  \mkup "(Repeat until keyboard cue)"
  s1*2
  \bar ":|]"
  s1*3
  s1
  \bar "|."
}



% Función musical global
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
global = {
	% Para pintar todos los números de compás debajo del pentagrama
	% \override Score.BarNumber #'break-visibility = #end-of-line-invisible
	% \override Score.BarNumber #'direction = #DOWN
	% \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

	\compressFullBarRests

	% Arregla el problema de tener el scoreMarkup añadido
	\revert MultiMeasureRest #'staff-position
	\override Voice.Rest #'staff-position = #0

	\set Timing.beamExceptions = #'()
	\set Timing.baseMoment = #(ly:make-moment 1/4)
	\set Timing.beatStructure = #'(1 1 1 1)

	\override ParenthesesItem.font-size = #0
}

% Cifrado
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
scoreChords = \chords {
	% c1:m f2:m g2:m7

  s1
  s1*2

  s1*16

  s1*4
  s1*2
  s1*2

  e1:m9
  s1
  e1:7.5-
  s1

  d:maj7
  s1
  a:dim
  g:maj7

  fs:7
  f:maj7
  c:maj7
  b:7.9+

  e1:m9
  s1
  e1:7-5
  s1

  d:maj7
  s1
  a:dim
  g:maj7

  fs:7.9+
  f:maj7
  e:m7
  a:7
  d
  b:7

}

% Música
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
altoSax = \relative c' {

	\global
	\key e \minor

  R1

  R1*2

  \repeat volta 2 {
    fs4 \repeatTie \mf -> fs -> fs8 e fs4 -> ~
    fs4. e8 fs a fs e
    fs4 -> fs -> fs8 e fs4 -> ~
    fs4. e8 fs a fs e

    b'4 -> b -> b8 a b4 -> ~
    b4. a8 b d b a
    g4 -> g -> g8 fs d4 -> ~
    d4. b8 d e d b

    a'2. ~ a8 g
    a2. ~ a8 g
  }
  \alternative {
    {
      b2. ~ b8 a
      b2 ~ b4 g8 fs -> \laissezVibrer
    }
    {
      fs2. ~ fs8 e
      fs2 ~ fs4 e8 bf
    }
  }

  R1*2

  R1*4

  \repeat volta 4 {
    e4 -> r8 fs -> r2
    g4 -> r8 fs -> r2
  }

  R1*2

  \repeat volta 2 {
    \impro 16
    \impro 16
    \impro 16
    \impro 16
    \impro 16
    \impro 12
    \impro 12
  }

  fs4 -> fs -> fs8 e fs4 -> ~
  fs4. e8 fs a fs e
  fs4 -> fs -> fs8 e fs4 -> ~
  fs4. e8 fs a fs e

  b'4 -> b -> b8 a b4 -> ~
  b4. a8 b d b a
  g4 -> g -> g8 fs d4 -> ~
  d4. b8 d e d b

  a'2. ~ a8 g
  a2. ~ a8 g
  b2. ~ b8 a
  b2 ~ b4 g8 fs -> ~

  fs4 -> fs -> fs8 e fs4 -> ~
  fs4. e8 fs a fs e
  fs4 -> fs -> fs8 e fs4 -> ~
  fs4. e8 fs a fs e

  b'4 -> b -> b8 a b4 -> ~
  b4. a8 b d b a
  g4 -> g -> g8 fs d4 -> ~
  d4. b8 d e d b

  a'2. ~ a8 g
  a2. ~ a8 g
  fs2. ~ fs8 e
  fs2 ~ fs4 e8 b

  R1*2

  R1*8

  \repeat volta 4 {
    e4 -> r8 fs -> r2
    g4 -> r8 fs -> r2
  }

  R1*3
  fs1 \fermata
}

tenorSax = \relative c' {

	\global
	\key e \minor

  R1
  R1*2

  \repeat volta 2 {
    fs4 -> \mf \repeatTie fs -> fs8 e fs4 -> ~
    fs4. e8 fs a fs e
    fs4 -> fs -> fs8 e fs4 -> ~
    fs4. e8 fs a fs e

    b'4 -> b -> b8 a b4 -> ~
    b4. a8 b d b a
    g4 -> g -> g8 fs d4 -> ~
    d4. b8 d e d b

    a'2. ~ a8 g
    a2. ~ a8 g
  }
  \alternative {
    {
      b2. ~ b8 a
      b2 ~ b4 g8 fs -> \laissezVibrer
      \bar ":|]"
    }
    { \break
      fs2. ~ fs8 e
      fs2 ~ fs4 e8 bf
    }
  }

  R1*2

  R1*4

  \repeat volta 4 {
    b'4 -> r8 cs -> r2
    d4 -> r8 cs -> r2
  }

  R1*2

  \repeat volta 2 {
    \impro 16
    \impro 16
    \impro 16

    \impro 16
    \impro 16
    \impro 12
    \impro 12
  }

  fs,4 -> fs -> fs8 e fs4 -> ~
  fs4. e8 fs a fs e
  fs4 -> fs -> fs8 e fs4 -> ~
  fs4. e8 fs a b cs

  fs4 -> fs -> fs8 e fs4 -> ~
  fs4. e8 fs a fs d
  b4 -> cs -> d8 cs b4 -> ~
  b4. g8 a cs b a

  fs'2. ~ fs8 e
  f2. ~ f8 d
  e2. ~ e8 c
  ds2 ~ ds4 r

  r2 e,
  fs g
  a bf
  c \breathe d

  e fs
  e d
  c d
  g,4. g8 a cs b a

  fs'2. ~ fs8 e
  f2. ~ f8 d
  cs2. ~ cs8 b
  a2 ~ a4 a8 d,

  R1*2

  R1*8

  \repeat volta 4 {
    b'4 -> r8 cs -> r2
    d4 -> r8 cs -> r2
  }

  R1*3
  cs1 \fermata
}

% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\book {
	\header { instrument = "Tenor Sax" }
  \bookOutputName "Chuso - Tenor"
	\score {
		<<
			\scoreMarkup
			\new StaffGroup <<
        \scoreChords
        \transpose bf c \scoreChords
				\new Staff { \transpose bf c' \tenorSax }
			>>
		>>
	}
}

\book {
  \bookOutputName "Chuso"

	\paper {
		% Descomentar para score
		% #(set-paper-size "a4" 'landscape)
		% indent = 20
		% systems-per-page = 2
	}

	\score {
		% Descomentar para midi
		% \midi { \tempo 4 = 110 }

		% NO USAR layout-set-staff-size O SE ROMPE lilyjazz!!!
		\layout { }

		<<
			\scoreMarkup
			\new StaffGroup <<
        \transpose ef c \scoreChords
				\new Staff \with { instrumentName = "Alto Sax" midiInstrument = #"" } {
          \transpose ef c' \altoSax
        }

        \transpose bf c \scoreChords
				\new Staff \with { instrumentName = "Tenor Sax" midiInstrument = #"" } {
          \transpose bf c' \tenorSax
        }
			>>
		>>
	}
}



