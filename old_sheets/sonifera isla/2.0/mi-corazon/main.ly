\version "2.18.2"
\language "english"

% Se debe incluir antes que lilyjazz. Si no, lilyjazz no funciona.
#(set-global-staff-size 19)

% includes
\include "main.ily"

% Cabecera
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\header {
	title = "Mi corazón"
	instrument = "Alto Sax & Tenor Sax"
	composer = "Sonífera Isla"
	arranger = ""
	poet = ""
	tagline = "Edition: gbpsms@gmail.com"
}

% Márgenes, fuentes y distancias del papel
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\paper {
}

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
	% Oculta el primer pentagrama del markup
	\override Staff.VerticalAxisGroup #'remove-first = ##t

	% Indicación de tiempo
	% \once \override Score.RehearsalMark.self-alignment-X = #LEFT
	% \once \override Score.RehearsalMark.extra-offset = #'(0 . 3)
	% \mark \jazzTempoMarkup "Easy Swing" c4 "112"

	% s1*2
	% \once \override Score.RehearsalMark.X-offset = #5
	% \mark \markup \boxed "A"

  s1*7
  s1*8

  \bar "||"
  s1*8
  s1*2

  \break
  s1*7
  s1*8

  \bar "||"
  s1*8
  s1*8
  s1*2
	
  \break 
	\bar "||"
	s1*2
	
  \bar "[|:"
  s1*4
  \break
  s1*4
  \bar ":|]"
  s1

  \break
  \bar "||"
  \mark \jazzTempoChange c2 c4
  s1
  s1*6/4
  s1
  s1*6/4
  s1
  s1*2
  s1*6/4

  \break
  s1*4
  \break
  s1*4
  \break
  s1*4
  s1*4
		
  \break
  s1*4
  \bar "||"
  \mkup "(Violin solo)"
  s1*8
	
  \break
  \bar "[|:"
  s1*4
  \bar ":|]"
  s1

  \break
  \bar "[|:"
  \mkup "(Repeat ad.lib. with audience. Last x on singer's cue)"
  s1*8
	
  \break
  \bar ":|][|:"
  \mark \jazzTempoChange c4 c2
  s1*4

  \break
  \bar ":|][|:"
  s1*4
  \bar ":|]"
  s1
  \bar "|."
}



% Función musical global
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
global = {
	% Para pintar todos los números de compás debajo del pentagrama
	% \override Score.BarNumber #'break-visibility = #end-of-line-invisible
	% \override Score.BarNumber #'direction = #DOWN
	% \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

	\compressFullBarRests

	% Arregla el problema de tener el scoreMarkup añadido
	\revert MultiMeasureRest #'staff-position
	\override Voice.Rest #'staff-position = #0

	\set Timing.beamExceptions = #'()
	\set Timing.baseMoment = #(ly:make-moment 1/4)
	\set Timing.beatStructure = #'(1 1 1 1)

	\override ParenthesesItem.font-size = #0
}

% Cifrado
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
scoreChords = \chords {
	% c1:m f2:m g2:m7
}

% Música
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
altoSax = \relative c'' {
  
	\global
	\key gf \major

  R1*7
  R1*8

  \bar "||"
  R1*8
  R1*2

  \break
  R1*7
  R1*8

  \bar "||"
  R1*8
  R1*8
  R1*2
	
	\bar "||"
	\key af \minor
	R1*2
	
  \bar "[|:"
	\repeat volta 2 {
		af4. \f bf8 cf4. bf8
		af4. gf8 ef4. gf8
		f8 df bf4 -. r2
		R1
		
		af'4. bf8 cf4. bf8
		af4. gf8 ef4. gf8
		bf8 f df4 -. r2
	}
	\alternative {
		{ R1 }
		{ bf'8 -. bf -. bf4 -. r2 }
	}
	
	\key af \major
  R1
  \time 6/4
  R1*6/4

  \time 4/4
  R1
  \time 6/4
  R1*6/4

  \time 4/4
  R1

  R1*2
  \time 6/4
  R1*6/4

  \time 4/4
	\key gf \major
  r4 bf \f af16 gf ef8 -. gf8 df -. 
  af'2 f \>
  r4 \! bf \f af16 gf ef8 -. gf8 df -. 
  af'2 \breathe bf4 df 

  ef4 ~ ef8. df16 ef df bf8 gf ef
  af1 \<

  r4 \! bf -. \mf r bf -.
  R1
  r4 bf -. r bf -.
  R1
  r4 bf -. r bf -.
  R1

  R1*4
		
	\key af \minor
  R1*4
  R1*7
	r4 bf8 \mf df ~ df4 bf8 gf ~
	
	\repeat volta 2 {
		gf4 ef bf' gf 
		f gf8 f ~ f ef d ef
		gf4 ef bf' gf 
	}
	\alternative {
	  { af bf8 df ~ df4 bf8 gf \laissezVibrer }
	  { af4 bf8 df r4 ef -> \f \bendAfter #-5 }
	}

  \repeat volta 2 {
    R1*8
  }
	
	\repeat volta 2 {
		r4 \f af,,8 bf cf2
		r4 cf8 df ef2
		r8 bf df d ef gf af ef
		R1
  }

	\repeat volta 2 {
		r4 af8 bf cf2
		r4 cf8 df ef2
		r8 bf df d ef gf af ef 
  }
	\alternative {
		{ R1 }
		{ r2 r4 ef4 -^ }
	}
}

tenorSax = \relative c'' {
  
	\global
	\key gf \major

  R1*7
  R1*8

  R1*8
  R1*2

  R1*7
  R1*8

  R1*8
  R1*8
  R1*2
	
	\key af \minor
	R1*2
	
	\repeat volta 2 {
		cf4. \f ef8 af4. gf8
		ef4. df8 cf4. ef8
		df8 bf f4 -. r2
		R1
		
		cf'4. ef8 af4. gf8
		ef4. df8 cf4. ef8
		f8 df bf4 -. r2
	}
	
	\alternative {
		{ R1 }
		{ ef8 -. ef -. ef4 -. r2 }
	}
	
	\key af \major
  R1
  \time 6/4
  R1*6/4

  \time 4/4
  R1
  \time 6/4
  R1*6/4

  \time 4/4
  R1

  R1*2
  \time 6/4
  R1*6/4
	
  \time 4/4
	\key gf \major
  r4 bf' \mf af16 gf ef8 -. gf df
  c2 b \>
  r4 \! bf' \mf af16 gf ef8 -. gf df
  c2 \breathe ef4 f
  gf4 ~ gf8. df16 ef df bf8 gf bf
  df1 \<

  r4 \! bf -. r bf -.
  R1
  r4 bf -. r bf -.
  R1
  r4 bf -. r bf -.
  R1

  R1*4
		
	\key af \minor
  R1*4
  R1*7
	r4 bf8 \mf df ~ df4 bf8 ef ~
	
	\repeat volta 2 {
		ef4 cf gf' ef 
		df ef8 df ~ df4 bf
		ef4 cf gf' ef 
	}
	\alternative {
	  { f fs8 g ~ g4 f8 ef \laissezVibrer }
	  { f4 fs8 g r4 ef -> \f \bendAfter #-5 }
	}

  \repeat volta 2 {
    R1*8
  }
	
	\repeat volta 2 {
		r4 \f af,8 bf cf2
		r4 af8 bf cf2
		r8 bf df d ef gf af ef
		R1
  }

	\repeat volta 2 {
		r4 cf8 ef af2
		r4 af8 bf cf2
    r8 bf, df d ef gf af ef 
  }
	\alternative {
		{ R1 }
    { r2 r4 ef4 -^ }
	}
	
	\bar "|."
}

% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\book {
	\header { instrument = "Tenor Sax" }
  \bookOutputName "Mi corazón - Tenor"
	\score {
		<<
			\scoreMarkup
			\new StaffGroup <<
        \scoreChords
				\new Staff { \transpose bf c' \tenorSax }
			>>
		>>
	}
}

\book {
  \bookOutputName "Mi corazón"

	\paper {
		% Descomentar para score
		% #(set-paper-size "a4" 'landscape)
		% indent = 20
		% systems-per-page = 2
	}

	\score {
		% Descomentar para midi
		% \midi { \tempo 4 = 110 }

		% NO USAR layout-set-staff-size O SE ROMPE lilyjazz!!!
		\layout { }

		<<
			\scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\new Staff \with { instrumentName = "Alto Sax" midiInstrument = #"" } {
          \transpose ef c' \altoSax
        }
				\new Staff \with { instrumentName = "Tenor Sax" midiInstrument = #"" } {
          \transpose bf c' \tenorSax
        }
			>>
		>>
	}
}



