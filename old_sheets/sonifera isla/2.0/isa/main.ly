\version "2.18.2"
\language "english"

% Se debe incluir antes que lilyjazz. Si no, lilyjazz no funciona.
#(set-global-staff-size 19)

% includes
\include "main.ily"

% Cabecera
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\header {
	title = "Isa"
	instrument = "Alto Sax & Tenor Sax"
	composer = "Sonífera Isla"
	arranger = ""
	poet = ""
	tagline = "Edition: gbpsms@gmail.com"
}

% Márgenes, fuentes y distancias del papel
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\paper {
}

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
	% Oculta el primer pentagrama del markup
	\override Staff.VerticalAxisGroup #'remove-first = ##t

	% Indicación de tiempo
	% \once \override Score.RehearsalMark.self-alignment-X = #LEFT
	% \once \override Score.RehearsalMark.extra-offset = #'(0 . 3)
	% \mark \jazzTempoMarkup "Easy Swing" c4 "112"

	% s1*2
	% \once \override Score.RehearsalMark.X-offset = #5
	% \mark \markup \boxed "A"

  s1*2
  \bar "||"
  s1*2
	
  \break
  \mark \segno
  \bar "[|:"
  s1*4
  \bar ":|]"
  s1

  \break
  s1*8
  s1*2

  \break
  s1*8
  s1*2
  s1*6
  s1*8

  \break
  s1*3
  \break
  s1*3
  \break
  s1*3
  \break
  s1
  \bar "||"
  \eolMark \toCoda
  s1*2
  \bar "||"
  \eolMark \toSegno

  \break
  \mark \coda
  s1*2
  \mark \jazzTempoChange c4 c4.
  s1*8*12/8
  s1*6*12/8
  \mark \jazzTempoChange c4. c4
  s1*2

  \break
  s1*3
  \break
  s1*3
  \break
  s1*3
  \break
  s1*3
	
  \break
  \bar "[|:"
  s1*3
  \break
  s1
  \bar ":|]"
  s1*3
  \bar "|."
}



% Función musical global
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
global = {
	% Para pintar todos los números de compás debajo del pentagrama
	% \override Score.BarNumber #'break-visibility = #end-of-line-invisible
	% \override Score.BarNumber #'direction = #DOWN
	% \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

	\compressFullBarRests

	% Arregla el problema de tener el scoreMarkup añadido
	\revert MultiMeasureRest #'staff-position
	\override Voice.Rest #'staff-position = #0

	\set Timing.beamExceptions = #'()
	\set Timing.baseMoment = #(ly:make-moment 1/4)
	\set Timing.beatStructure = #'(1 1 1 1)

	\override ParenthesesItem.font-size = #0
}

% Cifrado
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
scoreChords = \chords {
	% c1:m f2:m g2:m7
}

% Música
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
altoSax = \relative c'' {
  
	\global
	\key df \major

  R1*2
  R1
	r2 r8 \mf af8 bf [ c ] 
	
  \repeat volta 2 {
    df8 -- c -- r af --  r df -- r cf \bendBefore --
    r8 af -- r df \bendBefore -- r cf af4 ~
    af1 \> 
  }
  \alternative {
    { r2 \! r8 af bf [ c ] }
    { R1 }
  }
	
	R1*7
	r2 r8 \mp af8 bf [ c ] 
	
	df8 c r af r df r cf
	r8 af r df r cf af4 ~

	af1 \> 
  R1*7 \!
  R1*2

  R1*6
  R1*8

  r4 df8 \mp -- c -. r af -. r af -> ~
  af2 r
  r4 df8 -- c -. r af -. r af -> ~
  af2 r
  r8 \mf af f16 ef df8 b d g ef
  gf? f c e -> \sfp ~ e2 \<

  r4 \! df'8 -- \mp c -. r af -. r af -> ~
  af2 r
  r4 df8 -- c -. r af -. r af -> ~
  af2 r
  R1
	r2 r8 \mf af8 bf [ c ] 
	
  r8 \mp df,4 -> \< ef8 -> ~ ef f4 -> gf8 -> ~
  gf af4 -> bf8 -> ~ \times 2/3 { bf4 c af }
	
  \time 12/8
  df2. \mf \> r2. \!
  R1*7*12/8
  R1*6*12/8

  \time 4/4
  R1*2

  r4 df8 \mp -- c -. r af -. r af -> ~
  af2 r
  r4 df8 -- c -. r af -. r af -> ~
  af2 r
  r8 \mf af f16 ef df8 b d g ef
  gf? f c e -> \sfp ~ e2 \<

  r4 \! df'8 -- \mp c -. r af -. r af -> ~
  af2 r
  r4 df8 -- c -. r af -. r af -> ~
  af2 r
  R1
	r2 r8 \mf af8 bf [ c ] 
	
  \repeat volta 2 {
    df8 -- c -- r af --  r df -- r cf \bendBefore --
    r8 af -- r df \bendBefore -- r cf af4 ~
    af1 \>
  }
  \alternative {
    { r2 \! r8 af bf [ c ] }
    { r2 r8 \f af bf [ c ] }
  }

  df8 c r af r df r cf
  r8 af r cf r c df -- [ df -. ]
}

tenorSax = \relative c'' {
  
	\global
	\key df \major

  R1*2
  R1
	r2 r8 \mf af8 bf [ c ] 
	
  \repeat volta 2 {
    df8 -- c -- r af --  r df -- r cf \bendBefore --
    r8 af -- r df \bendBefore -- r cf ef4 ~
    ef1 \> 
  }
  \alternative {
    { r2 \! r8 af, bf c }
    { R1 }
  }
	
	R1*7
	r2 r8 \mp af8 bf [ c ] 
	
	df8 c r af r df r cf
	r8 af r df r cf ef4 ~

	ef1 \> 
  R1*7 \!
  R1*2

  R1*6
  R1*8

  r4 df8 \mp -- c -. r af -. r af -> ~
  af2 r
  r4 df8 -- c -. r af -. r af -> ~
  af2 r
  r8 \mf af' f16 ef df8 b d g ef
  gf? f af, c -> \sfp ~ c2 \<

  r4 \! df8 -- \mp c -. r af -. r af -> ~
  af2 r
  r4 df8 -- c -. r af -. r af -> ~
  af2 r
  R1
	r2 r8 \mf af8 bf [ c ] 
	
  r8 \mp bf4 -> \< c8 -> ~ c df4 -> ef8 -> ~
  ef f4 -> gf8 -> ~ \times 2/3 { gf4 af f }
	
  \time 12/8
  a2. \mf \> r2. \!
  R1*7*12/8
  R1*6*12/8

  \time 4/4
  R1*2

  r4 df,8 \mp -- c -. r af -. r af -> ~
  af2 r
  r4 df8 -- c -. r af -. r af -> ~
  af2 r
  r8 \mf af' f16 ef df8 b d g ef
  gf? f af, c -> \sfp ~ c2 \<

  r4 \! df8 -- \mp c -. r af -. r af -> ~
  af2 r
  r4 df8 -- c -. r af -. r af -> ~
  af2 r
  R1
	r2 r8 \mf af8 bf [ c ] 
	
  \repeat volta 2 {
    df8 -- c -- r af --  r df -- r cf \bendBefore --
    r8 af -- r df \bendBefore -- r cf ef4 ~
    ef1 \>
  }
  \alternative {
    { r2 \! r8 af, bf [ c ] }
    { r2 r8 \f af bf [ c ] }
  }

  df8 c r af r df r cf
  r8 af r cf r c df -- [ df -. ]
}

% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\book {
	\header { instrument = "Tenor Sax" }
  \bookOutputName "Isa - Tenor"

	\score {
		<<
			\scoreMarkup
			\new StaffGroup <<
        \scoreChords
				\new Staff { \transpose bf c' \tenorSax }
			>>
		>>
	}
}

\book {
  \bookOutputName "Isa"

	\paper {
		% Descomentar para score
		% #(set-paper-size "a4" 'landscape)
		% indent = 20
		% systems-per-page = 2
	}

	\score {
		% Descomentar para midi
		% \midi { \tempo 4 = 110 }

		% NO USAR layout-set-staff-size O SE ROMPE lilyjazz!!!
		\layout { }

		<<
			\scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\new Staff \with { instrumentName = "Alto Sax" midiInstrument = #"" } {
          \transpose ef c' \altoSax
        }
				\new Staff \with { instrumentName = "Tenor Sax" midiInstrument = #"" } {
          \transpose bf c' \tenorSax
        }
			>>
		>>
	}
}



