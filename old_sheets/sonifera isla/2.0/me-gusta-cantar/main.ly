\version "2.18.2"
\language "english"

% Se debe incluir antes que lilyjazz. Si no, lilyjazz no funciona.
#(set-global-staff-size 19)

% includes
\include "main.ily"

% Cabecera
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\header {
	title = "Me gusta cantar"
	instrument = "Alto Sax & Tenor Sax"
	composer = "Sonífera Isla"
	arranger = ""
	poet = ""
	tagline = "Edition: gbpsms@gmail.com"
}

% Márgenes, fuentes y distancias del papel
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\paper {
}

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
	% Oculta el primer pentagrama del markup
	\override Staff.VerticalAxisGroup #'remove-first = ##t

	% Indicación de tiempo
	% \once \override Score.RehearsalMark.self-alignment-X = #LEFT
	% \once \override Score.RehearsalMark.extra-offset = #'(0 . 3)
	% \mark \jazzTempoMarkup "Easy Swing" c4 "112"

	% s1*2
	% \once \override Score.RehearsalMark.X-offset = #5
	% \mark \markup \boxed "A"

  \bar "[|:"
  \mkup "(Repeat ad.lib. Last x on Keyboard cue)"
  s1*4
  \bar ":|]"
  s1
  
  \break
  \bar "[|:"
  \mark \segno
  s1*2
  \break
  s1*2
  \break
  s1*2
  \break
  s1*2
  s1*2/4

  s1*8
  s1*2
  \bar "||"
  \eolMark \toCoda

  \break
  s1*4
  s1*2

  s1*2/4
  \bar ":|]"
  s1*2/4

  \break
  \mkup "(1st to 4th x: Tacet. 5th to 11th x: Play. 12nd to 15th: Sax solo)"
  \mkup "(Repeat 16 xs)"
  \bar "[|:"
  s1*2
  \break
  s1*2
  \bar ":|]"

  \break
  s1*8
  \bar "||"
  \eolMark \toSegno

  \break
  \mark \coda
  s1*8

  \bar "[|:"
  \mkup "(Repeat ad. lib. Tacet first 4 xs. Last x on Drums cue)"
  s1*2
  \break
  \bar ":|]"
  s1*3
  \bar "|."
}



% Función musical global
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
global = {
	% Para pintar todos los números de compás debajo del pentagrama
	% \override Score.BarNumber #'break-visibility = #end-of-line-invisible
	% \override Score.BarNumber #'direction = #DOWN
	% \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

	\compressFullBarRests

	% Arregla el problema de tener el scoreMarkup añadido
	\revert MultiMeasureRest #'staff-position
	\override Voice.Rest #'staff-position = #0

	\set Timing.beamExceptions = #'()
	\set Timing.baseMoment = #(ly:make-moment 1/4)
	\set Timing.beatStructure = #'(1 1 1 1)

	\override ParenthesesItem.font-size = #0
}

% Cifrado
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
scoreChords = \chords {
	% c1:m f2:m g2:m7
  s1*5
  s1*8
  s1*2/4

  s1*10
  s1*6
  s1*2/4
  s1*2/4

  f1:m
  c1:m
  g1
  c1:m
}

% Música
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
altoSax = \relative c'' {

  \global
  \key c \minor
  
  \repeat volta 2 {
    R1*4
  }
  r2 r8. c16 bf8 -- g16 f -.
  
  \repeat volta 2 {
    r f8 -- f16 af8 -- af16 c -. r c8 -- c16 d8 -- d16 ef -.
    r ef8 -- ef16 c8 -- c16 ef -. r ef8 -- ef16 c8 -- c16 f -.
    r d8 -- b16 d8 -- g,16 b -. r g8 -- af16 g8 -- f16 ef -.
    r ef8 -- d16 f8 -. d8 -. ef8 -. r16 c' bf8 -- g16 f -.

    r f8 -- f16 af8 -- af16 c -. r c8 -- c16 d16 c d ef -.
    r ef8 -- ef16 c8 -- c16 ef -. r ef8 -- ef16 c8 -- c16 f -.
    r d8 -- b16 d8 -- g,16 b -. r g8 -- af16 g8 -- f16 g' -.
    r ef8 -- c16 ef8 -. b8 -. c8 -. r8 r4

    \time 2/4
    R1*2/4

    \time 4/4
    R1*8
    R1*2

    R1*4
    ef8. -> c16 -. r c d8 -> ~ d8. a16 -. r a b8 -> ~
    b8. g'16 -. r g c,8 -> ~ c2 \<
  }
  \alternative {
    {
      \time 2/4
      r8. \! c16 bf8 -- g16 f -.
    }
    {
      \time 2/4
      R1*2/4
    }
  }

  \time 4/4
  \repeat volta 8 {
    r16 c af8 -. c -- af16 c -. r16 c af8 -. c -- d16 c -. 
    r16 c g8 -. c -- g16 c -. r16 c g8 -. c -- d16 b -. 
    r16 b g8 -. b -- g16 b -. r16 b g8 -. b -- d16 c -. 
    r16 c g8 -. c16 c d c -. r16 c g8 -. ef'16 c d c -. 
  }

  R1*7
  r2 r8. c'16 bf8 -- g16 f -.

  R1*8

  \repeat volta 2 {
    R1
    r4 r8. c'16 -. r16 df8 -. c16 -. r16 bf gf8 -.
  }

  R1*2
  ef'4 -. ef8. bf16 df8 ef -. r4
}

tenorSax = \relative c'' {

  \global
  \key c \minor
  
  \repeat volta 2 {
    R1*4
  }
  r2 r8. c'16 bf8 -- g16 c, -.
  
  \repeat volta 2 {
    r c8 -- c16 f8 -- f16 af -. r af8 -- f16 af8 -- af16 c -.
    r c8 -- c16 g8 -- g16 c -. r c8 -- c16 g8 -- g16 d' -.
    r b8 -- g16 b8 -- d,16 f -. r d8 -- f16 ef8 -- d16 c -.
    r c8 -- b16 d8 -. g,8 -. c8 -. r16 c' bf8 -- g16 c, -.

    r c8 -- c16 f8 -- f16 af -. r af8 -- af16 f g af c -.
    r c8 -- c16 g8 -- g16 c -. r c8 -- c16 g8 -- g16 d' -.
    r b8 -- g16 b8 -- d,16 f -. r d8 -- af'16 g8 -- f16 ef' -.
    r c8 -- g16 c8 -. f,8 -. ef8 -. r8 r4

    \time 2/4
    R1*2/4

    \time 4/4
    R1*8
    R1*2

    R1*4
    ef8. -> c16 -. r c d8 -> ~ d8. a16 -. r a b8 -> ~
    b8. b16 -. r b c8 -> ~ c2 \<
  }
  \alternative {
    {
      \time 2/4
      r8. \! c'16 bf8 -- g16 c, -.
    }
    {
      \time 2/4
      R1*2/4
    }
  }

  \time 4/4
  \repeat volta 8 {
    r16 af f8 -. af -- f16 af -. r16 af f8 -. af -- af16 g -. 
    r16 g ef8 -. g -- ef16 g -. r16 g ef8 -. g -- af16 g -. 
    r16 g d8 -. g -- f16 g -. r16 g d8 -. g -- f16 g -. 
    r16 g ef8 -. g16 af fs g -. r16 g ef8 -. f16 fs g af -. 
  }

  R1*7
  r2 r8. c'16 bf8 -- g16 c, -.

  R1*8

  \repeat volta 2 {
    R1
    r4 r8. af'16 -. r16 bff8 -. af16 -. r16 gf ef8 -.
  }

  R1*2
  ef4 -. ef8. bf16 df8 ef -. r4
}


% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\book {
	\header { instrument = "Tenor Sax" }
  \bookOutputName "Me gusta cantar - Tenor"

	\score {
		<<
			\scoreMarkup
			\new StaffGroup <<
        \scoreChords
        \transpose bf c \scoreChords
				\new Staff { \transpose bf c' \tenorSax }
			>>
		>>
	}
}

\book {
  \bookOutputName "Me gusta cantar"

	\paper {
		% Descomentar para score
		% #(set-paper-size "a4" 'landscape)
		% indent = 20
		% systems-per-page = 2
	}

	\score {
		% Descomentar para midi
		% \midi { \tempo 4 = 110 }

		% NO USAR layout-set-staff-size O SE ROMPE lilyjazz!!!
		\layout { }

		<<
			\scoreMarkup
			\new StaffGroup <<
        \transpose ef c \scoreChords
				\new Staff \with { instrumentName = "Alto Sax" midiInstrument = #"" } {
          \transpose ef c' \altoSax
        }

        \transpose ef bf \scoreChords
				\new Staff \with { instrumentName = "Tenor Sax" midiInstrument = #"" } {
          \transpose bf c' \tenorSax
        }
			>>
		>>
	}
}



