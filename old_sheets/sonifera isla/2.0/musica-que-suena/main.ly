\version "2.18.2"
\language "english"

% Se debe incluir antes que lilyjazz. Si no, lilyjazz no funciona.
#(set-global-staff-size 19)

% includes
\include "main.ily"

% Cabecera
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\header {
	title = "Música que suena"
	instrument = "Alto Sax & Tenor Sax"
	composer = "Sonífera Isla"
	arranger = ""
	poet = ""
	tagline = "Edition: gbpsms@gmail.com"
}

% Márgenes, fuentes y distancias del papel
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\paper {
}

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
	% Oculta el primer pentagrama del markup
	\override Staff.VerticalAxisGroup #'remove-first = ##t

	% Indicación de tiempo
	% \once \override Score.RehearsalMark.self-alignment-X = #LEFT

	% s1*2
	% \once \override Score.RehearsalMark.X-offset = #5
	% \mark \markup \boxed "A"
  
	\once \override Score.RehearsalMark.extra-offset = #'(0 . 3)
	\mark \jazzTempoMarkup "Medium Swing" c4 "170"

  s1*8
  s1*2

  \bar "[|:"
  s1*2
  \break
  s1*2
  \bar ":|]"
  s1
	
	\break
	s1*7
	s1*4
  s1*4
	
	\break
	s1*7
	
	\break
	s1*4
  s1*4
	
	\break
  s1*4
	
  \bar "[|:"
  \break
  s1*4
		
  \break
  s1*4
  \bar ":|]"
  s1

  \break
  s1*3

  \break
  s1*8
	
	\break
  s1*4
	
	\break
  s1*3
	
	\break
  s1*4
	
	\break
  s1*4
	
	\break
	s1*3
  s1*2
  s1*2
	
	\break
	s1*8
	
	\break
  s1*4
	
  \break
  \bar "[|:"
  s1*4
		
  \break
  s1*4
  \bar ":|]"
  s1
  \break
  s1*4
  \break
  s1*2
  s1*6/4
  s1*2

	\break
  s1*4
	
	\break
  s1*4
	
	\break
  s1*4
  s1*8
  s1
	\bar "|."
}



% Función musical global
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
global = {
	% Para pintar todos los números de compás debajo del pentagrama
	% \override Score.BarNumber #'break-visibility = #end-of-line-invisible
	% \override Score.BarNumber #'direction = #DOWN
	% \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

	\compressFullBarRests

	% Arregla el problema de tener el scoreMarkup añadido
	\revert MultiMeasureRest #'staff-position
	\override Voice.Rest #'staff-position = #0

	\set Timing.beamExceptions = #'()
	\set Timing.baseMoment = #(ly:make-moment 1/4)
	\set Timing.beatStructure = #'(1 1 1 1)

	\override ParenthesesItem.font-size = #0
}

% Cifrado
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
scoreChords = \chords {
	% c1:m f2:m g2:m7
}

% Música
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
altoSax = \relative c'' {
  
	\global
	\key g \major

  R1*8
  R1
	r2 r8 \mf d,8 e f
	
	\repeat volta 2 {
	  fs? d e b d e fs a 
	  g e fs g b fs ef d
	  fs d e? b d e r4
	}
	\alternative {
    { r2 r8 d8 e f }
    { b8 -. r r b -> ~ b2 }
	}
	
	R1*7
	R1*4
	r8 \mf a, c e b' g e c
	b' a ~ a2.
	r8 fs g4 -. g8 gs r gs
	a4 r r2
	
	R1
  r4 r8 \f b8 -> ~ b4 r
  R1
	e,4 \mp \< fs g a
	b1 \mf
	R1*2
	
	R1*4
	r8 \mf a, c e b' g e c
	b' a ~ a2.
	r8 fs g4 -. g8 gs r gs
	a4 r r2
	
	r8 a, c e b' g e c
	b a ~ a2.
	r4 a'8 \f g a b d,4 --
  e4 r r2
	
	\repeat volta 2 {
		R1*2
		\small e4 ^"(Con cazú)" \bendBefore b e4. \bendBefore e8
		gs \bendBefore gs4 -. \bendBefore e8 ~ e4 r
		R1*2
	}
	
	\alternative { 
		{ \normalsize g8 \mf fs g b e, e4 -. b'8
		  a gs a c d, d r4 }

		{ g8 fs g b e, e4 -. b'8
		  a gs a c d, d r g -> ~}
	}
  
  g8 fs g b e, e4 -. b'8
  a gs a c d,4 r
	
	R1*8
	
	R1
	b'8 \mf a r4 r2
	R1
	r8 c,8 d4 e fs
	
	b1
	r2 cs4. d8 ~
	d4 r4 r2
	
	R1
	b8 b4 -. b8 r2
	R1
	b8 b4 -. b8 r2
	
	r8 a, c e b' g e c
	b' a ~ a2.
	r8 fs g4 -. g8 gs r gs
	a4 r r2
	
	R1*3
	e4 \mp \< fs g a
	b1 \mf
	R1*2
	
	R1*4
	r8 \mf a, c e b' g e c
	b' a ~ a2.
	r8 fs g4 -. g8 gs r gs
	a4 r r2
	
	r8 a, c e b' g e c
	b a ~ a2.
	r4 a'8 \f g a b d,4 -- 
  e4 r r2
	
	\repeat volta 2 {
		R1*2
		\small e4 ^"(Con cazú)" \bendBefore b e4. \bendBefore e8
		gs \bendBefore gs4 -. \bendBefore e8 ~ e4 r
		R1*2
	}
	
	\alternative { 
		{ \normalsize g8 \mf fs g b e, e4 -. b'8
		  a gs a c d, d r4 }
		
		{ g8 fs g b e, e4 -. b'8
		  a gs a c d, d r g -> ~ }
	}
  g8 fs g b e, e4 -. b'8
  a gs a c d, d r b' -> ~

  b8 as b d gs, gs4 -. d'8
  c b c e fs, fs r b -> ~
  b8 as b d gs, gs4 -. d'8

  \time 6/4
  c d c b c4 \< d e fs8 g \f

  \time 4/4
  R1*2
	
	b,1 \p ~
	b1 \<
	b8 \mp d r4 r4 r8 b
	c8 d r4 r2
	
	r2 b2 \p ~
	b1 \<
	b8 \mp d r4 r4 r8 b
	c8 d r4 r2
	
	r8 \f g,8 fs g b e4 -. d16 df
	c8 b \times 2/3 { c d e } fs4 ds8 e ~
	e1 ~
  e1

  R1*8
  R1 \fermataMarkup
}

tenorSax = \relative c'' {
  
	\global
	\key g \major

  R1*8
  R1
	r2 r8 \mf d8 e f
	
	\repeat volta 2 {
	  fs? d e b d e fs a 
	  g e fs g b fs ef d
	  fs d e? b d e r4
	}
	\alternative {
    { r2 r8 d8 e f }
    { d8 -. r r ds -> ~ ds2 }
	}
	
	R1*7
	R1*4
	r8 \mf a c e b' g e c
	g' fs ~ fs2.
	r8 e e4 -. e8 e r f
	fs?4 r r2
	
	R1
  r4 r8 \f ds8 -> ~ ds4 r
  R1
	c4 \mp \< d e fs
	g1 \mf
	R1*2
	
	R1*4
	r8 \mf a, c e b' g e c
	g' fs ~ fs2.
	r8 e e4 -. e8 e r f
	fs?4 r r2
	
	r8 a, c e b' g e c
	b a ~ a2.
	r4 a'8 \f g a b b,4 --
  c4 r r2
	
	\repeat volta 2 {
		R1*4
		R1*2
	}
	
	\alternative { 
		{ g8 \mf fs g b e, e4 -. b'8
		  a gs a c d, d r4 }
		{ g8 fs g b e, e4 -. b'8
		  a gs a c d, d r g -> ~}
	}
  
  g8 fs g b e, e4 -. b'8
  a gs a c d,4 r
	
	R1*7
	r4 b'4 \f -- c -- cs --
	
	d8 cs d e d b a g
	d' d r4 r8 b c cs
	d8 cs d e d b a g
	r8 e'8 fs4 -- g -- a --
	
	g8 e fs g ~ g2
	r8 fs fs g e d e fs ~
	fs4 r d4 -- ds --
	
	e8 ds e fs g fs e fs
	d8 d4 -. d8 r8 b d ds
	e8 ds e fs g fs e fs
	d8 d4 -. d8 r b d b ~
	
  b a ~ a2.
  r4 r8 a a e'4 -- d8 ~
	d1
	d4 r r2
	
	R1*3
	c4 \mp \< d e fs
	g1 \mf
	R1*2
	
	R1*4
	r8 \mf a, c e b' g e c
	g' fs ~ fs2.
	r8 e e4 -. e8 e r f
	fs?4 r r2
	
	r8 a, c e b' g e c
	b a ~ a2.
	r4 a'8 \f g a b b,4 --
  c4 r r2
	
	\repeat volta 2 {
		R1*4
		R1*2
	}
	
	\alternative { 
		{ g8 \mf fs g b e, e4 -. b'8
		  a gs a c d, d r4 }
		{ g8 fs g b e, e4 -. b'8
		  a gs a c d, d r g -> ~ }
	}
  g8 fs g b e, e4 -. b'8
  a gs a c d, d r g' -> ~

  g8 fs g b e, e4 -. b'8
  a gs a c d, d r g -> ~
  g8 fs g b e, e4 -. b'8

  \time 6/4
  a b a gs a4 \< b c a8 g \f

  \time 4/4
  R1*2
	
  d2 \p e2 ~
  e2 \< fs
	d8 \mp fs r4 r4 r8 d
	g8 fs r4 r2
	
	r2 e2 \p ~
  e2 \< fs
	d8 \mp fs r4 r4 r8 d
	g8 fs r4 r2
	
	r8 \f g,8 fs [ g ] b e4 -. d16 df
	c8 b \times 2/3 { c d e } fs4 g8 b ~
	b1 ~
  b1

  R1*8
  R1 \fermataMarkup
}

% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~


\book {
	\header { instrument = "Tenor Sax" }
  \bookOutputName "Música que suena - Tenor"
	\score {
		<<
			\scoreMarkup
			\new StaffGroup <<
        \scoreChords
				\new Staff { \transpose bf c' \tenorSax }
			>>
		>>
	}
}

\book {
  \bookOutputName "Música que suena"

	\paper {
		% Descomentar para score
		% #(set-paper-size "a4" 'landscape)
		% indent = 20
		% systems-per-page = 2
	}

	\score {
		% Descomentar para midi
		% \midi { \tempo 4 = 110 }

		% NO USAR layout-set-staff-size O SE ROMPE lilyjazz!!!
		\layout { }

		<<
			\scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\new Staff \with { instrumentName = "Alto Sax" midiInstrument = #"" } {
          \transpose ef c' \altoSax
        }
				\new Staff \with { instrumentName = "Tenor Sax" midiInstrument = #"" } {
          \transpose bf c' \tenorSax
        }
			>>
		>>
	}
}



