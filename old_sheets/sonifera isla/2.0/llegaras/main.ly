\version "2.18.2"
\language "english"

% Se debe incluir antes que lilyjazz. Si no, lilyjazz no funciona.
#(set-global-staff-size 19)

% includes
\include "main.ily"

% Cabecera
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\header {
	title = "Llegarás"
	instrument = "Alto Sax & Tenor Sax"
	composer = "Sonífera Isla"
	arranger = ""
	poet = ""
	tagline = "Edition: gbpsms@gmail.com"
}

% Márgenes, fuentes y distancias del papel
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\paper {
}

% Función musical global
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
global = {
	% Para pintar todos los números de compás debajo del pentagrama
	% \override Score.BarNumber #'break-visibility = #end-of-line-invisible
	% \override Score.BarNumber #'direction = #DOWN
	% \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

	\compressFullBarRests

	% Arregla el problema de tener el scoreMarkup añadido
	\revert MultiMeasureRest #'staff-position
	\override Voice.Rest #'staff-position = #0

	\set Timing.beamExceptions = #'()
	\set Timing.baseMoment = #(ly:make-moment 1/4)
	\set Timing.beatStructure = #'(1 1 1 1)

	\override ParenthesesItem.font-size = #0
}

% Cifrado
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
scoreChords = \chords {
	% c1:m f2:m g2:m7
}

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {

	% Oculta el primer pentagrama del markup
	\override Staff.VerticalAxisGroup #'remove-first = ##t

	\once \override Score.RehearsalMark.extra-offset = #'(0 . 3)
	\mark \jazzTempoMarkup "" c4 "75"

  s1*4

  \bar "[|:"
  s1*4
  \bar ":|]"
  s1

  \break
  s1*8
  s1*7
  s1*2

  \break
  \bar "[|:"
  s1*3
  \break
  s1*2
  \bar ":|]"
  s1*2/4
  s1

  \break
  \bar "[|:"
  s1*4
  \bar ":|]"
  s1


  \break
  s1*7
  s1*2

  \break
  \bar "[|:"
  s1*4
  s1
  s1*2/4

  \break
  s1*4

  \break
  s1*5/4
  \bar "||"
  s1

  \break
  s1*3
  \break
  s1*3
  \break
  s1*3

  \break
  s1*3
  s1*2

  \break
  s1*3
  s1
  s1*2/4
  s1*2
  \bar "|."
}




% Música
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
altoSax = \relative c' {

	\global
	\key c \major

  R1
  f16 \mf f8 g16 e4 d16 d8 e16 c4
  R1
  a'16 \< a8 c16 bf4 b16 b8 d16 c4 -. \f

  \repeat volta 2 {
    e,2 \mp ~ e8. e16 r f r g
    d2. \< c4 \glissando
    f2. \! \times 2/3 { f8 g a }
  }
  \alternative {
    { af2 \glissando e }
    { c'4 bf f \> g }
  }

  R1*8 \!
  R1*7
  r8. \f f16 bf8. f16 af4 -. r
  r8. d,16 g8. d16 e4 -. r

  \repeat volta 2 {
    r4 d'8. \f c16 d c a8 -. r4
    r4 c8. a16 c d c8 -. r4
    r4 d8. \turn c16 d f d8 -. r4
    R1
  }
  \alternative {
    { R1 }
    { \time 2/4
      R1*2/4 }
  }

  \time 4/4
  R1

  \repeat volta 2 {
    e,2 \mp ~ e8. e16 r f r g
    d2. \< c4 \glissando
    f2. \! \times 2/3 { f8 g a }
  }
  \alternative {
    { af2 \glissando e }
    { c'4 bf f \> g }
  }

  R1*7 \!
  r8. \f f16 bf8. f16 af4 -. r
  r8. d,16 g8. d16 e4 -. r

  \repeat volta 2 {
    r4 d'8. \f c16 d c a8 -. r4
    r4 c8. a16 c d c8 -. r4
    r4 d8. \turn c16 d f d8 -. r4
    R1
  }
  \alternative {
    { R1 }
    { \time 2/4
      R1*2/4 }
  }

  \time 4/4
  R1
  f,16 \mf f8 g16 e4 d16 d8 e16 c4
  R1
  a'16 a8 c16 bf4 b16 b8 d16 c4 -.
  \time 5/4
  cs16 \< cs8 e16 d4 -. ds16 ds8 fs16 e16 -. \f b, [ e fs ] g fs e d

  \time 4/4
  \key e \minor
  b16 e8 -. e16 ~ e4 r16 b e fs g e g fs ~
  fs16 d8 -. d16 ~ d4 r8 fs16 g a fs a g ~
  g16 e8 -. e16 ~ e4 r16 e g a b a g e ~
  e16 d8 -. g16 ~ g4 r16 d g a b a g e ~
  e16 d8. -. fs4 \prall r16 b, ( e fs g fs e d 

  b16 e8 -. e16 ~ e4 ) r16 b ( e fs g e g fs ~
  fs16 d8 -. d16 ~ d4 ) \times 2/3 { r8 fs ( g a fs a }
  g16 e8 -. e16 ~ e4 ) r16 e ( g a b a g e ~
  e16 d8 -. g16 ~ g4 ) r16 d ( g a b a g e ~
  e16 d8. -. fs2. ) \prall

  r4 e'8. \f d16 e d b8 -. r4
  r4 d8. b16 d e d8 -. r4
  r4 e8. \turn d16 e g e8 -. r4
  R1*2

  r4 e8. d16 e d b8 -. r4
  r4 d8. b16 d e d8 -. r4
  r4 e8. \turn d16 e g e8 -. r4
  R1
  \time 2/4
  R1*2/4

  \time 4/4
  R1 \rit
  R1 \! \fermataMarkup
}

tenorSax = \relative c'' {

	\global
	\key c \major

  R1
  a16 \mf a8. g4 af16 af8. g4
  R1
  f'16 \< f8. g4 af16 af8. g4 -. \f 

  \repeat volta 2 {
    e,2 \mp ~ e8. e16 r f r g
    d2. \< c4 \glissando 
    f2. \! \times 2/3 { f8 g a }
  }
  \alternative {
    { af2 \glissando e }
    { c'4 bf f \> g }
  }

  R1*8 \!
  R1*7
  r8. \f c16 d8. d16 c4 -. r
  r8. b16 d8. g,16 c4 -. r

  \repeat volta 2 {
    r4 d8. \f c16 d c a8 -. r4
    r4 c8. a16 c d c8 -. r4
    r4 d8. \turn c16 d f d8 -. r4
    R1
  }
  \alternative {
    { R1 }
    { \time 2/4
      R1*2/4 }
  }

  \time 4/4
  R1

  \repeat volta 2 {
    e,2 \mp ~ e8. e16 r f r g
    d2. \< c4 \glissando
    f2. \! \times 2/3 { f8 g a }
  }
  \alternative {
    { af2 \glissando e }
    { c'4 bf f \> g }
  }

  R1*7 \!
  r8. \f c16 d8. d16 c4 -. r
  r8. b16 d8. g,16 c4 -. r

  \repeat volta 2 {
    r4 d8. \f c16 d c a8 -. r4
    r4 c8. a16 c d c8 -. r4
    r4 d8. \turn c16 d f d8 -. r4
    R1
  }
  \alternative {
    { R1 }
    { \time 2/4
      R1*2/4 }
  }

  \time 4/4
  R1
  a16 \mf a8. g4 af16 af8. g4
  R1
  f'16 f8. g4 af16 af8. g4 -.
  \time 5/4
  bf16 \< a8 g16 fs4 -. c'16 b8 a16 g16 -. \f b,, [ e fs ] g fs e d

  \time 4/4
  \key e \minor
  b16 e8 -. e16 ~ e4 r16 b e fs g e g fs ~
  fs16 d8 -. d16 ~ d4 r8 fs16 g a fs a g ~
  g16 e8 -. e16 ~ e4 r16 e g a b a g e ~
  e16 d8 -. g16 ~ g4 r16 d g a b a g e ~
  e16 d8. -. fs4 \prall r16 g ( b d e d b fs

  g16 b8 -. b16 ~ b4 ) r16 g ( b d e b e d ~
  d16 a8 -. b16 ~ b4 ) \times 2/3 { r8 d ( e fs d fs }
  e16 b8 -. c16 ~ c4 ) r16 c ( e fs g e c b ~
  b16 b8 -. d16 ~ d4 ) r16 c ( e fs g fs e b ~
  b16 b8. -. d4 \prall a'2 \prall )

  r4 e8. \f d16 e d b8 -. r4
  r4 d8. b16 d e d8 -. r4
  r4 e8. \turn d16 e g e8 -. r4
  R1*2

  r4 g8. fs16 g fs e8 -. r4
  r4 fs8. e16 fs g fs8 -. r4
  r4 g8. \turn fs16 g c g8 -. r4
  R1
  \time 2/4
  R1*2/4

  \time 4/4
  R1 \rit
  R1 \! \fermataMarkup
}

% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

\book {
	\header { instrument = "Tenor" }
  \bookOutputName "Llegarás - Tenor"

	\score {
		<<
			\scoreMarkup
			\new StaffGroup <<
        \scoreChords
				\new Staff { \transpose bf c' \tenorSax }
			>>
		>>
	}
}

\book {
  \bookOutputName "Llegarás"

	\paper {
		% Descomentar para score
		% #(set-paper-size "a4" 'landscape)
		% indent = 20
		% systems-per-page = 2
	}

	\score {
		% Descomentar para midi
		% \midi { \tempo 4 = 110 }

		% NO USAR layout-set-staff-size O SE ROMPE lilyjazz!!!
		\layout { }

		<<
			\scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\new Staff \with { instrumentName = "Alto Sax" midiInstrument = #"" } {
          \transpose ef c' \altoSax
        }
				\new Staff \with { instrumentName = "Tenor Sax" midiInstrument = #"" } {
          \transpose bf c' \tenorSax
        }
			>>
		>>
	}
}



