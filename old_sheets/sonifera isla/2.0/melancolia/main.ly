\version "2.18.2"
\language "english"

% Se debe incluir antes que lilyjazz. Si no, lilyjazz no funciona.
#(set-global-staff-size 19)

% includes
\include "main.ily"

% Cabecera
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\header {
	title = "Melancolía"
	instrument = "Alto Sax & Tenor Sax"
	composer = "Sonífera Isla"
	arranger = ""
	poet = ""
	tagline = "Edition: gbpsms@gmail.com"
}

% Márgenes, fuentes y distancias del papel
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\paper {
}

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
	% Oculta el primer pentagrama del markup
	\override Staff.VerticalAxisGroup #'remove-first = ##t

	% Indicación de tiempo
	% \once \override Score.RehearsalMark.self-alignment-X = #LEFT
	% \once \override Score.RehearsalMark.extra-offset = #'(0 . 3)
	% \mark \jazzTempoMarkup "Easy Swing" c4 "112"

	% s1*2
	% \once \override Score.RehearsalMark.X-offset = #5
	% \mark \markup \boxed "A"

  s1*8
  s1*8

  \break
  \bar "[|:"
  \mkup "(On segno don't repeat)"
  \mark \segno
  s1*2
  \break
  s1*2
  \bar ":|]"
  s1*4

  \break
  s1*2
  \break
  s1*2

  \break
  s1*4
  \break
  s1*4

  \break
  s1*4

  \break
  s1
  s1*4
  \eolMark \toSegno

  \break
  \bar "||"
  s1*8

  \bar "||"
  \mkup "(Drum solo ad.lib.)"
  s1

  \break
  \bar "||"
  \mkup "(Guitar solo, Q&A singer/audience, Q&A guitar/saxes.)"
  s1

  \break
  \bar "||"
  \mkup "(On Alto Sax cue)"
  s1*2
  \break
  s1*2

  \break
  s1*4
  \break
  s1*2
  \break
  s1*2

  \break
  s1*4
  \break
  s1*4

  \break
  s1*4
  \break
  s1*2
  s1*2
  \break
  s1*2
  s1*3
  s1*2

  \break
  s1*3
  \break
  s1
  s1*2
	\bar "|."
}



% Función musical global
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
global = {
	% Para pintar todos los números de compás debajo del pentagrama
	% \override Score.BarNumber #'break-visibility = #end-of-line-invisible
	% \override Score.BarNumber #'direction = #DOWN
	% \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

	\compressFullBarRests

	% Arregla el problema de tener el scoreMarkup añadido
	\revert MultiMeasureRest #'staff-position
	\override Voice.Rest #'staff-position = #0

	\set Timing.beamExceptions = #'()
	\set Timing.baseMoment = #(ly:make-moment 1/4)
	\set Timing.beatStructure = #'(1 1 1 1)

	\override ParenthesesItem.font-size = #0
}

% Cifrado
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
scoreChords = \chords {
	% c1:m f2:m g2:m7
}

% Música
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
altoSax = \relative c'' {
  
	\global
	\key ef \major

  R1*8
  R1*8

  \repeat volta 2 {
    r8 \f ef -^ r16 c8 -^ bf16 df8. -> df16 ( c bf a bf )
    af2 ~ af8. af16 ( gf ef df ef )
    r8 g' -^ r16 ef8 -^ df16 ef2
    r8 c -^ r16 bf8 -^ gf16 af4. df,16 ef -.
  }
  R1*4

  r8 \mf ef -. r16 ef -. r8 df16 ef -. r8 ef8 -- df16 ef -.
  r8 ef -. r16 ef16 c8 -. gf'16 gf f8 -. ef8 -- f16 ef -.
  r8 ef -. r16 ef -. r8 df16 ef -. r8 ef8 -- df16 ef -.
  r8 ef -. r16 ef16 c8 -. gf'16 gf f8 -. ef8 -. r 

  g1 ( 
  ef1 )
  ef2 \< f
  r8 \f \times 2/3 { f16 ( fs g } bf af cf ef ) df8. -> d16 -> ~ d8 ef8 -^

  r4 \mf g,2. ->
  a1
  bf2 af
  r8 bf -^ \< r c -^ r d -^ ef16 e8 -^ f16 -^ \f

  R1
  r2 r8 bf,16 bf g bf g f -> ~
  f2 g
  r2 r8 ef'16 ef d ef d c -> ~

  c2 bf
  R1*4

  R1*8

  R1

  R1

  r8 \f ef -^ r16 c8 -^ bf16 df8. -> df16 ( c bf a bf )
  af2 ~ af8. af16 ( gf ef df ef )
  r8 g' -^ r16 ef8 -^ df16 ef2
  r8 c -^ r16 bf8 -^ gf16 af4. df,16 ef -.

  R1
  r2 ef'16 \ff df bf a af gf8. ->
  ef4 -> r r2
  r2 ef'16 df bf a af bf8. ->

  r8 \mf ef, -. r16 ef -. r8 df16 ef -. r8 ef8 -- df16 ef -.
  r8 ef -. r16 ef16 c8 -. gf'16 gf f8 -. ef8 -- f16 ef -.
  r8 ef -. r16 ef -. r8 df16 ef -. r8 ef8 -- df16 ef -.
  r8 ef -. r16 ef16 c8 -. gf'16 gf f8 -. ef8 -. r 

  g1 (
  ef1 )
  ef2 \< f
  r8 \f \times 2/3 { f16 ( fs g } bf af cf ef ) df8. -> d16 -> ~ d8 ef8 -^

  r4 \mf g,2. ->
  a1
  bf2 af
  r8 bf -^ \< r c -^ r d -^ ef16 e8 -^ f16 -^ \f

  R1
  r2 r8 bf,16 bf g bf g f -> ~
  f2 g
  r2 r8 ef'16 ef d ef d c -> ~

  c2 bf
  R1

  R1
  r4 r8 \mf g,16 bf c c bf8 -. ef d16 c -.
  R1
  r4 r8 f,16 g bf g bf c ef8 d16 c -.

  R1*3
  R1*2

  r2 r8 \f df -> ~ df16 d8. ->
  ef4 -^ r r8 af -> ~ af16 a8. ->
  bf4 -^ r df8 -. \ff df -. df16 d8. -> 
  ef4 -^ r gf8 -. gf -. gf16 ef -. r8

  R1
  ef1 \fermata
}

tenorSax = \relative c'' {
  
	\global
	\key ef \major

  R1*8
  R1*8

  \repeat volta 2 {
    r8 \f ef -^ r16 c8 -^ bf16 df8. -> df16 ( c bf a bf )
    af8 af -^ r16 gf8 -^ ef16 gf8. -> af16 ( gf ef df ef )
    r8 ef' -^ r16 c8 -^ bf16 df8. -> df16 ( c bf a bf )
    af8 af -^ r16 gf8 -^ ef16 gf8. -> af16 ( gf ef df ef )
  }
  R1*4

  r8 \mf bf' -. r16 bf -. r8 bf16 bf -. r8 bf8 -- bf16 bf -.
  r8 bf -. r16 bf16 af8 -. ef'16 ef df8 -. c8 -- c16 bf -.
  r8 bf -. r16 bf -. r8 bf16 bf -. r8 bf8 -- bf16 bf -.
  r8 bf -. r16 bf16 af8 -. ef'16 ef df8 -. c8 -. r

  ef1 ( 
  c1 )
  cf2 \< df
  r8 \f \times 2/3 { f,16 ( fs g } bf af cf ef ) df8. -> d16 -> ~ d8 ef8 -^

  r4 \mf ef2. ->
  f1
  ef1 
  r8 d -^ \< r ef -^ r f -^ g16 gs8 -^ a16 -^ \f

  R1
  r8 c16 c bf c bf g -> ~ g4 g16 bf g f -> ~
  f2 d
  r8 c'16 c bf c bf a -> ~ a4 ~ a8. af16 -> ~

  af2 g
  R1*4

  R1*8

  R1

  R1

  r8 \f ef -^ r16 c8 -^ bf16 df8. -> df16 ( c bf a bf )
  af8 af -^ r16 gf8 -^ ef16 gf8. -> af16 ( gf ef df ef )
  r8 ef' -^ r16 c8 -^ bf16 df8. -> df16 ( c bf a bf )
  af8 af -^ r16 gf8 -^ ef16 gf8. -> af16 ( gf ef df ef )

  R1
  r2 ef'16 \ff df bf a af gf8. ->
  ef4 -> r r2
  r2 ef'16 df bf a af df8. ->

  r8 \mf bf -. r16 bf -. r8 bf16 bf -. r8 bf8 -- bf16 bf -.
  r8 bf -. r16 bf16 af8 -. ef'16 ef df8 -. c8 -- c16 bf -.
  r8 bf -. r16 bf -. r8 bf16 bf -. r8 bf8 -- bf16 bf -.
  r8 bf -. r16 bf16 af8 -. ef'16 ef df8 -. c8 -. r

  ef1 ( 
  c1 )
  cf2 \< df
  r8 \f \times 2/3 { f,16 ( fs g } bf af cf ef ) df8. -> d16 -> ~ d8 ef8 -^

  r4 \mf ef2. ->
  f1
  ef1 
  r8 d -^ \< r ef -^ r f -^ g16 gs8 -^ a16 -^ \f

  R1
  r8 c16 c bf c bf g -> ~ g4 g16 bf g f -> ~
  f2 d
  r8 c'16 c bf c bf a -> ~ a4 ~ a8. af16 -> ~

  af2 g
  R1

  R1
  r4 r8 \mf g,16 bf c c bf8 -. c bf16 af -.
  R1
  r4 r8 f16 g bf g bf g c8 bf16 af -.

  R1*3
  R1*2

  r2 r8 \f df, -> ~ df16 d8. ->
  ef4 -^ r r8 df -> ~ df16 d8. ->
  ef4 -^ r df'8 -. \ff df -. df16 d8. -> 
  ef4 -^ r gf8 -. gf -. gf16 ef -. r8

  R1
  ef1 \fermata
}

% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\book {
	\header { instrument = "Tenor Sax" }
  \bookOutputName "Melancolía - Tenor"

	\score {
		<<
			\scoreMarkup
			\new StaffGroup <<
        \scoreChords
				\new Staff { \transpose bf c' \tenorSax }
			>>
		>>
	}
}
%}

\book {
  \bookOutputName "Melancolía"

	\paper {
		% Descomentar para score
		% #(set-paper-size "a4" 'landscape)
		% indent = 20
		% systems-per-page = 2
	}

	\score {
		% Descomentar para midi
		% \midi { \tempo 4 = 110 }

		% NO USAR layout-set-staff-size O SE ROMPE lilyjazz!!!
		\layout { }

		<<
			\scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\new Staff \with { instrumentName = "Alto Sax" midiInstrument = #"" } {
          \transpose ef c' \altoSax
        }
				\new Staff \with { instrumentName = "Tenor Sax" midiInstrument = #"" } {
          \transpose bf c' \tenorSax
        }
			>>
		>>
	}
}



