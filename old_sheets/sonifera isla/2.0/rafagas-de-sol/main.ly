\version "2.18.2"
\language "english"

% Se debe incluir antes que lilyjazz. Si no, lilyjazz no funciona.
#(set-global-staff-size 19)

% includes
\include "main.ily"

% Cabecera
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\header {
	title = "Ráfagas de sol"
	instrument = "Alto Sax & Tenor Sax"
	composer = "Sonífera Isla"
	arranger = ""
	poet = ""
	tagline = "Edition: gbpsms@gmail.com"
}

% Márgenes, fuentes y distancias del papel
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\paper {
}

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
	% Oculta el primer pentagrama del markup
	\override Staff.VerticalAxisGroup #'remove-first = ##t

	% Indicación de tiempo
	% \once \override Score.RehearsalMark.self-alignment-X = #LEFT
	% \once \override Score.RehearsalMark.extra-offset = #'(0 . 3)
	% \mark \jazzTempoMarkup "Easy Swing" c4 "112"

	% s1*2
	% \once \override Score.RehearsalMark.X-offset = #5
	% \mark \markup \boxed "A"

  \mkup "(Keyboard intro ad.lib.)"
	\mark \jazzTempoMarkup "Easy Swing" c4 "135"
  s1

  \bar "||"
  \mkup "(Slow)"
  s1*16

  \break
  s1*15

  \mkup "(A tempo on cue)"
  s1*3

  \break
  \mark \segno
  \bar "[|:"
  s1*8
  \bar ":|]"

  \break
  s1*8
  s1*5

  \break
  s1*4
  \eolMark \toCoda

  \break
  \bar "||"
  s1*6
  \eolMark \dalSegnoAlCoda

  \break
  \bar "||"
  \mark \coda
  s1*7
  \mkup "(Slow)"
  s1*2
  s1
  \bar "|."
}



% Función musical global
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
global = {
	% Para pintar todos los números de compás debajo del pentagrama
	% \override Score.BarNumber #'break-visibility = #end-of-line-invisible
	% \override Score.BarNumber #'direction = #DOWN
	% \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

	\compressFullBarRests

	% Arregla el problema de tener el scoreMarkup añadido
	\revert MultiMeasureRest #'staff-position
	\override Voice.Rest #'staff-position = #0

	\set Timing.beamExceptions = #'()
	\set Timing.baseMoment = #(ly:make-moment 1/4)
	\set Timing.beatStructure = #'(1 1 1 1)

	\override ParenthesesItem.font-size = #0

}

% Cifrado
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
scoreChords = \chords {
	% c1:m f2:m g2:m7
}

% Música
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
altoSax = \relative c'' {

	\global
	\key d \major

  R1

  R1*8
  R1*8

  R1*8
  R1*7
  r2 r8 \mf cs d ds
  e cs a af g e'4. ->
  d4 r r2

  \repeat volta 2 {
    r4 a,8 \mp b ~ b a ~ a4
    b1 \>
    R1*2 \!

    r4 a8 b ~ b d ~ d4
    b1 \>
    R1*2 \!
  }

  R1*7
  r2 r4 r8 \mp c' ~ ->

  c1 ~
  c2 a
  b1 ~
  b2 c
  cs?2. \> r4 \!

  d2 \f b2
  e2. r4
  d2 b2
  e2 r
  R1

  r2 r8 \mf cs d ds
  e cs a af g e'4. ->
  d4 r r8 cs d ds
  e cs a af g e'4. ->
  d4 r r2

  R1*7
  R1*2
  R1 \fermataMarkup
}

tenorSax = \relative c'' {

	\global
	\key d \major

  R1

  R1*8
  R1*8

  R1*8
  R1*7
  r2 r8 \mf cs d ds
  e cs a af g e'4. ->
  d4 r r2

  \repeat volta 2 {
    r4 fs,8 \mp g ~ g fs ~ fs4
    g1 \>
    R1*2 \!

    r4 fs8 g ~ g a ~ a4
    g1 \>
    R1*2 \!
  }

  R1*7
  r2 r4 r8 \mp g' ~ ->

  g1 
  fs1 ~
  fs1
  e1 ~
  e2 fs4 \> r4 \!

  fs2 \f d2
  b'2. r4
  fs2 d2
  b'2 r
  R1

  r2 r8 \mf cs, d ds
  e cs a af g e'4. ->
  d4 r r8 cs d ds
  e cs a af g g'4. ->
  fs4 r r2

  R1*7
  R1*2
  R1 \fermataMarkup
}

% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\book {
	\header { instrument = "Tenor Sax" }
  \bookOutputName "Ráfagas de sol - Tenor"

	\score {
		<<
			\scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\new Staff { \transpose bf c' \tenorSax }
			>>
		>>
	}
}

\book {
  \bookOutputName "Ráfagas de sol"

	\paper {
		% Descomentar para score
		% #(set-paper-size "a4" 'landscape)
		% indent = 20
		% systems-per-page = 2
	}

	\score {
		% Descomentar para midi
		% \midi { \tempo 4 = 110 }

		% NO USAR layout-set-staff-size O SE ROMPE lilyjazz!!!
		\layout { }

		<<
			\scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\new Staff \with { instrumentName = "Alto Sax" midiInstrument = #"" } {
          \transpose ef c' \altoSax
        }
				\new Staff \with { instrumentName = "Tenor Sax" midiInstrument = #"" } {
          \transpose bf c' \tenorSax
        }
			>>
		>>
	}
}


