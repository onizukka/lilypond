\version "2.18.2"
\language "english"

% Se debe incluir antes que lilyjazz. Si no, lilyjazz no funciona.
#(set-global-staff-size 19)

% includes
\include "main.ily"

% Cabecera
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\header {
	title = "Buster Keaton"
	instrument = "Alto Sax & Tenor Sax"
	composer = "Sonifera Isla"
	arranger = ""
	poet = ""
	tagline = "Edition: gbpsms@gmail.com"
}

% Márgenes, fuentes y distancias del papel
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\paper {
  page-count = 3
}

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
	% Oculta el primer pentagrama del markup
	\override Staff.VerticalAxisGroup #'remove-first = ##t

	% Indicación de tiempo
	% \once \override Score.RehearsalMark.self-alignment-X = #LEFT
	% \once \override Score.RehearsalMark.extra-offset = #'(0 . 3)

	% s1*2
	% \once \override Score.RehearsalMark.X-offset = #5
	
	\mark \jazzTempoMarkup "" c4 "115"
  s1*8
  s1*8

  \break
  s1*8
  s1*6
  s1*4

  \break
  s1*4
  s1*6
  s1*8

  \break
  R1*4

  \mkup "(Keyboard solo ad.lib.)"
  \bar "||"
  s1

  \mkup "(Drums cue)"
  \bar "||"
  s1

  \break
	\bar "[|:"
  s1*4
	\bar ":|]"
	
	\break
  s1*4
	
	\break
	\bar "[|:"
  s1*2
  s1*2
  \bar ":|]"
	\break
  s1*3
  s1*8
	
	\break
  s1*4
	
	\break
  s1*4
	
	\break
	\bar "[|:"
  s1*2
  \break
  s1*2
  \bar ":|]"
  \break
  s1*3

	\break
  s1*4
	
	\break
  s1*4
	
	\break
  s1*4
	\bar "|."
}



% Función musical global
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
global = {
	% Para pintar todos los números de compás debajo del pentagrama
	% \override Score.BarNumber #'break-visibility = #end-of-line-invisible
	% \override Score.BarNumber #'direction = #DOWN
	% \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

	\compressFullBarRests

	% Arregla el problema de tener el scoreMarkup añadido
	\revert MultiMeasureRest #'staff-position
	\override Voice.Rest #'staff-position = #0

	\set Timing.beamExceptions = #'()
	\set Timing.baseMoment = #(ly:make-moment 1/4)
	\set Timing.beatStructure = #'(1 1 1 1)

	\override ParenthesesItem.font-size = #0
}

% Cifrado
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
scoreChords = \chords {
	% c1:m f2:m g2:m7
}

% Música
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
altoSax = \relative c'' {

	\global
	\key a \minor

  R1*8
  R1*8

  R1*8
  R1*6
  R1*4

  R1*4
  R1*6
  R1*8

  R1*4

  R1


  \override NoteHead.style = #'cross
  \times 2/3 { b8 b b b8 b b } b8 r r4
  \revert NoteHead.style
	
	\repeat volta 2 {
	  a8. \mf g16 ~ -> -- g2 f8 d
	  f8. d16 ~ -> -- d2 c8 d
	  a'8. g16 ~ -> -- g2 f8 d
	  f2. r4
	}
	
	a8. g16 ~ -> -- g2 fs8 d
	f?8. d16 ~ -> -- d2 c8 d
	a'8. g16 ~ -> -- g2 fs8 d
	f?2. r4
	
	\repeat volta 2 {
	  g2 \< c
	  a16 \mf ( a8 -. ) a16 -. r4 a16 ( a8 -. ) a16 -. r4
	}
	\alternative {
	  { g2 \< bf
	    c16 \mf ( c8 -. ) c16 -. r4 c16 ( c8 -. ) c16 -. r4 }
	  { g2 \< cs
      e1 \f ~ \>
      e1 }
	}
	
  R1*7 \!
	r2 r4 \mf e,8 g
	
	e8. c16 ~ c8 e ~ e2 ~
	e2 r4 e8 g
	e8. c16 ~ c8 d ~ d2 ~
	d2 r4 e8 g
	
	e8. c16 ~ c8 a e'8. c16 ~ c8 e -> ~ 
	e2 r4 e'8 g
	e8. c16 ~ c8 a e'8. c16 ~ c8 c16 d 
	e ( d c af c af c d e \< g e g e g ) e \f ( g
	
	\repeat volta 2 {
	  e c a d b c b a -> ) ~ a8. fs16 -. r a ( e' g
	  e c a d b c a d -> ~ d8. ) e16 ( f e d f 
	  e c a d b c b a -> ) ~ a8. fs16 -. r a ( e' g
	}
	\alternative {
	  { e c a d b c a d -. ) ~ d e8 -. f16 -. ~ f e ( d f ) }
	  { e ( c a d b c a d -> ~ d4 ) \appoggiatura { e16 f } e d e f }
	}
	
	\key ef \minor
	gf?4 \f ef8 df f, ( af c bf -> ) ~
	bf2. r4
	gf'4 ef8 df f, ( af c ef -> \fp ) ~
	ef2 \< ~ ef4. d8 -^ \f
	r8 gf ef df f, ( af c bf -> ) ~
	bf2 gf2
	
	r8 \mf df' -. bf -. ef16 ( af, -. ) r df8 -. bf16 -. r4
	R1
	r8 df -. bf -. ef16 ( af, -. ) r df8 -. bf16 -. r4
	R1
	
	r8 df -. bf -. ef16 ( af, -. ) r df8 -. af16 -. r df ( bf8 -. )
	R1
	r8 df -. bf -. ef16 ( af, -. ) r df8 -. bf16 -. r4
	b?4 \f -^ --  b -^ -- e,8. -> -- e16 -. r8 e' -> \bendAfter #-5
}

tenorSax = \relative c'' {

	\global
	\key a \minor

  R1*8
  R1*8

  R1*8
  R1*6
  R1*4

  R1*4
  R1*6
  R1*8

  R1*4

  R1

  \override NoteHead.style = #'cross
  \times 2/3 { b8 b b b8 b b } b8 r r4
  \revert NoteHead.style
	
	\repeat volta 2 {
	  a8. \mf g16 ~ -> -- g2 f8 d
	  f8. d16 ~ -> -- d2 c8 d
	  a'8. g16 ~ -> -- g2 f8 d
	  f2. r4
	}
	
	fs'8. e16 ~ -> -- e2 d8 b
	c8. bf16 ~ -> -- bf2 g8 a
	fs'8. e16 ~ -> -- e2 d8 b?
	d2. r4
	
	\repeat volta 2 {
	  df2 \< g
	  e16 \mf ( e8 -. ) e16 -. r4 e16 ( e8 -. ) e16 -. r4
	}
	\alternative {
	  { ef2 \< g
	    a16 \mf ( a8 -. ) a16 -. r4 a16 ( a8 -. ) a16 -. r4 }
	  { ef2 \< g
      c1 \f ~ \>
      c1 }
	}
	
  R1*7 \!
	r2 r4 \mf e,,8 g
	
	e8. c16 ~ c8 e ~ e2 ~
	e2 r4 e8 g
	e8. c16 ~ c8 d ~ d2 ~
	d2 r4 e8 g
	
	e8. c16 ~ c8 a e'8. c16 ~ c8 e -> ~ 
	e2 r4 e'8 g
	e8. c16 ~ c8 a e'8. c16 ~ c8 c16 d 
	e ( d c af c af c d e \< g e g e g ) e \f ( g
	
	\repeat volta 2 {
	  e c a d b c b a -> ) ~ a8. fs16 -. r a ( e' g
	  e c a d b c a d -> ~ d8. ) e16 ( f e d f 
	  e c a d b c b a -> ) ~ a8. fs16 -. r a ( e' g
	}
	\alternative {
	  { e c a d b c a d -. ) ~ d e8 -. f16 -. ~ f e ( d f ) }
	  { e ( c a d b c a d -> ~ d4 ) \appoggiatura { e16 f } e d e f }
	}
	
	\key ef \minor
	bf?4 \f af8 bf df, ( f af gf -> ) ~
	gf2. r4
	bf4 gf8 f df ( ef f ef -> \sfp ) ~
	ef2 \< ~ ef4. ef8 \f -^
	r8 bf' af bf df, ( f af ef -> ) ~
	ef2 d2
	
	r8 \mf df -. bf -. ef16 ( af, -. ) r df8 -. bf16 -. r4
	R1
	r8 df -. bf -. ef16 ( af, -. ) r df8 -. bf16 -. r4
	R1
	
	r8 df -. bf -. ef16 ( af, -. ) r df8 -. af16 -. r df ( bf8 -. )
	R1
	r8 df -. bf -. ef16 ( af, -. ) r df8 -. bf16 -. r4
	b?4 -^ --  b -^ -- e,8. -> -- e16 -. r8 e' -> \bendAfter #-5
}

% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\book {
  \paper { page-count = 2 }
	\header { instrument = "Tenor Sax" }
  \bookOutputName "Buster Keaton - Tenor"

	\score {
		<<
			\scoreMarkup
			\new StaffGroup <<
        \transpose bf c \scoreChords
				\new Staff { \transpose bf c' \tenorSax }
			>>
		>>
	}
}

%{
\book {
  \bookOutputName "Buster Keaton"

	\paper {
		% Descomentar para score
		% #(set-paper-size "a4" 'landscape)
		% indent = 20
		% systems-per-page = 2
	}

	\score {
		% Descomentar para midi
		% \midi { \tempo 4 = 110 }

		% NO USAR layout-set-staff-size O SE ROMPE lilyjazz!!!
		\layout { }

		<<
			\scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\new Staff \with { instrumentName = "Alto Sax" midiInstrument = #"" } {
          \transpose ef c' \altoSax
        }
				\new Staff \with { instrumentName = "Tenor Sax" midiInstrument = #"" } {
          \transpose bf c' \tenorSax
        }
			>>
		>>
	}
}
%}




