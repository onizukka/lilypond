\version "2.18.2"
\language "english"

% Se debe incluir antes que lilyjazz. Si no, lilyjazz no funciona.
#(set-global-staff-size 19)

% includes
\include "main.ily"

% Cabecera
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\header {
	title = "A veces"
	instrument = "Alto Sax & Tenor Sax"
	composer = "Sonífera Isla"
	arranger = ""
	poet = ""
	tagline = "Edition: gbpsms@gmail.com"
}

% Márgenes, fuentes y distancias del papel
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\paper {
}

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
	% Oculta el primer pentagrama del markup
	\override Staff.VerticalAxisGroup #'remove-first = ##t

	% Indicación de tiempo
	% \once \override Score.RehearsalMark.self-alignment-X = #LEFT

	% s1*2
	% \once \override Score.RehearsalMark.X-offset = #5
	% \mark \markup \boxed "A"

	\once \override Score.RehearsalMark.extra-offset = #'(0 . 3)
	\mark \jazzTempoMarkup "" c4 "85"

  s1*8
  s1*8

  \bar "[|:"
  \mkup "(Repeat ad.lib.)"
  s1*2

  \break
  \bar ":|][|:"
  \mkup "(On lead cue)"
  s1*4

  \break
  s1*4
		
  \break
  s1*4
  s1*3
  \break
  s1*1
  s1*4
  \bar ":|]"
  s1*3
	\break
  s1*3
	
	\break
  \mkup "(Background)"
  s1*4
  \break
  s1*4
	
  \break
  s1*4
  \break
  s1*4
	
	\break
  s1*4
	\break
  s1*4

  \break
  \bar "[|:"
  \mkup "(Rap cue)"
  s1*8
  s1*4
  s1*4
  \break
  s1*4
  \break
  s1*4
  \bar ":|]"
  s1

  \break
  s1*4
  \break
  s1*4

  \break
  s1*4
  \break
  s1*3
  \break
  \bar "[|:"
  \mkup "(Repeat 3 xs)"
  s1*2

  \break
  \bar ":|][|:"
  \mkup "(Repeat 3 xs)"
  s1*2
  \bar ":|]"
  s1
  \bar "|."
}



% Función musical global
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
global = {
	% Para pintar todos los números de compás debajo del pentagrama
	% \override Score.BarNumber #'break-visibility = #end-of-line-invisible
	% \override Score.BarNumber #'direction = #DOWN
	% \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

	\compressFullBarRests

	% Arregla el problema de tener el scoreMarkup añadido
	\revert MultiMeasureRest #'staff-position
	\override Voice.Rest #'staff-position = #0

	\set Timing.beamExceptions = #'()
	\set Timing.baseMoment = #(ly:make-moment 1/4)
	\set Timing.beatStructure = #'(1 1 1 1)

	\override ParenthesesItem.font-size = #0
}

% Cifrado
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
scoreChords = \chords {
	% c1:m f2:m g2:m7
}

% Música
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
altoSax = \relative c'' {

	\global
	\key g \minor

  R1*8
  R1*8

  \repeat volta 2 {
    r4 \mf a16 ( g d8 -. e4 -- f8. -- d16 -. )
    R1
  }

	\repeat volta 2 {
    R1*2
    r4 a'16 ( g d8 -. e4 -- f8. -- d16 -. )
    R1

    R1*2
    r4 a'16 ( g d8 -. e4 -- f8. -- d16 -. )
    R1
		
		d1
		e2 \< cs
		d1 \f \>
		R1 \!
		
		ef1 \sfp \<
	}
	\alternative {
		{ d4 -^ \f r4 r2
      R1
		  r4 a'16 ( g d8 -. e4 -- f8. -- d16 -. )
      R1*4 }
		{ d2 \f df
		  c2 d2
		  d1 }
	}
	
	c2 d2
	d1 
  r4 a'16 ( g d8 -. e4 -- f8. -- d16 -. )
	
	r4 \p g2.
	r4 g2.
	r4 g2.
	r4 g8. ( f16 ~ f8 c8 ~ c8 c16 bf )
	
	r4 g'2.
	r4 g2.
	r4 g4 ~ g8 f'16 f r4
	d8. c16 ~ c8. bf16 ~ bf8 g -. f8 c
	
	d1
	e2 \< cs
	d1 \f \>
	R1 \!
	
	ef1 \sfp \< 
	d4 -^ \f r4 r2
	R1
  r4 a'16 ( g d8 -. e4 -- f8 -- g16 a -.)
	
  r16 g f g f8 -> e16 f -. r e d e d8 -> c16 d -. 
  r16 c bf c bf2 r4
  r4 g'' -> \bendAfter #-5 r2
  r4 g -> \bendAfter #-5 r r8 c,16 d -.

  r16 c bf c bf8 -> a16 bf -. r a g a g8 -> f16 g -. 
  r16 f e f d2 r4
  r4 g' -> \bendAfter #-5 r2
  r4 g -> \bendAfter #-5 r2

  \repeat volta 2 {
    R1*8

    r8 c,, -. c16 ( bf -. ) r8 c8 -. c16 ( bf -. ) r4
    R1*3
    r8 c -. c16 ( bf -. ) r8 c8 -. c16 ( bf -. ) r4
    R1*3

    d1
    e2 \< cs
    d1 \f \>
    R1 \!

    ef1 \sfp \< 
    d4 -^ \f r4 r2
    R1
  }
	\alternative {
    { r4 a'16 ( g d8 -. e4 -- f8. -- d16 -. ) }
    { r4 a'16 ( g d8 -. e4 -- f8 -- g16 a -.) }
	}

  r16 g f g f8 -> e16 f -. r e d e d8 -> c16 d -. 
  r16 c bf c bf2 r4
  r4 g'' -> \bendAfter #-5 r2
  r4 g -> \bendAfter #-5 r r8 c,16 d -.

  r16 c bf c bf8 -> a16 bf -. r a g a g8 -> f16 g -. 
  r16 f e f d2 r4
  r4 g' -> \bendAfter #-5 r2
  r4 g -> \bendAfter #-5 r2
	
	d,1
	e2 \< cs
	d1 \f \>
	R1 \!
	
	ef1 \sfp \< 
	d2 \f df
  c2 \< d2

	\repeat volta 3 {
    r8 \! c -. c16 ( bf -. ) r8 c8 -. c16 ( bf -. ) r4
    c2 \sfp \< d2
	}

	\repeat volta 3 {
    r4 \! a'16 ( g d8 -. e4 -- f8. -- d16 -. )
    R1
  }

  r4 a'16 ( g d8 -. e4 -- f8. -- g16 -. )
}

tenorSax = \relative c'' {

	\global
	\key g \minor

  R1*8
  R1*8

  \repeat volta 2 {
    r4 \mf a'16 ( g d8 -. c4 -- d8. -- bf16 -. )
    R1
  }

	\repeat volta 2 {
    R1*2
    r4 a'16 ( g d8 -. c4 -- d8. -- bf16 -. )
    R1

    R1*2
    r4 a'16 ( g d8 -. c4 -- d8. -- bf16 -. )
    R1
		
		bf1
		c2 \< a
		g1 \f \>
		R1 \!
		
		bf1 \sfp \<
	}
	
	\alternative {
		{ a4 -^ \f r4 r2
      R1
		  r4 a'16 ( g d8 -. c4 -- d8. -- bf16 -. )
      R1*4 }
    
		{ a2 \f af
		  g2 a2
		  g1 }
	}
	
	g2 a2
	g1 
  r4 a'16 ( g d8 -. c4 -- d8. -- bf16 -. )
	
	r4 \p g2.
	r4 g2.
	r4 g2.
	r4 g8. ( f16 ~ f8 c8 ~ c8 c16 bf )
	
	r4 g'2.
	r4 g2.
	r4 g4 ~ g8 c16 c r4
	d8. c16 ~ c8. bf16 ~ bf8 g -. a8 f
	
	bf1
	c2 \< a
	g1 \f \>
	R1 \!
	
	bf1 \sfp \< 
	a4 -^ \f r4 r2
	R1
  r4 a'16 ( g d8 -. c4 -- d8 -- e16 f -.)
	
  r16 e d e d8 -> c16 d -. r c bf c bf8 -> a16 bf -. 
  r16 a g a g2 r4
  r4 d'' -> \bendAfter #-5 r2
  r4 d -> \bendAfter #-5 r r8 a16 bf -.

  r16 a g a g8 -> f16 g -. r f e f e8 -> d16 e -. 
  r16 d c d bf2 r4
  r4 d' -> \bendAfter #-5 r2
  r4 d -> \bendAfter #-5 r2

  \repeat volta 2 {
    R1*8

    r8 a, -. a16 ( g -. ) r8 f8 -. f16 ( g -. ) r4
    R1*3
    r8 a -. a16 ( g -. ) r8 f8 -. f16 ( g -. ) r4
    R1*3

    bf1
    c2 \< a
    g1 \f \>
    R1 \!

    bf1 \sfp \< 
    a4 -^ \f r4 r2
    R1
  }
	
	\alternative {
    { r4 a'16 ( g d8 -. c4 -- d8. -- bf16 -. ) }
    { r4 a'16 ( g d8 -. c4 -- d8 -- e16 f -.) }
	}

  r16 e d e d8 -> c16 d -. r c bf c bf8 -> a16 bf -. 
  r16 a g a g2 r4
  r4 d'' -> \bendAfter #-5 r2
  r4 d -> \bendAfter #-5 r r8 a16 bf -.

  r16 a g a g8 -> f16 g -. r f e f e8 -> d16 e -. 
  r16 d c d bf2 r4
  r4 d' -> \bendAfter #-5 r2
  r4 d -> \bendAfter #-5 r2

	bf,1
	c2 \< a
	g1 \f \>
	R1 \!
	
	bf1 \sfp \< 
	a2 \f af
  g2 \< a2

	\repeat volta 3 {
    r8 \! a -. a16 ( g -. ) r8 f8 -. f16 ( g -. ) r4
    g2 \sfp \< a2
	}

	\repeat volta 3 {
    r4 \! a'16 ( g d8 -. c4 -- d8. -- bf16 -. )
    R1
  }

  r4 a'16 ( g d8 -. c4 -- bf8. -- g16 -. )
}

% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\book {
	\header { instrument = "Tenor Sax" }
  \bookOutputName "A veces - Tenor"

	\score {
		<<
			\scoreMarkup
			\new StaffGroup <<
        \scoreChords
				\new Staff { \transpose bf c' \tenorSax }
			>>
		>>
	}
}

\book {
  \bookOutputName "A veces"
	\paper {
		% Descomentar para score
		% #(set-paper-size "a4" 'landscape)
		% indent = 20
		% systems-per-page = 2
	}

	\score {
		% Descomentar para midi
		% \midi { \tempo 4 = 110 }

		% NO USAR layout-set-staff-size O SE ROMPE lilyjazz!!!
		\layout { }

		<<
			\scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\new Staff \with { instrumentName = "Alto Sax" midiInstrument = #"" } {
          \transpose ef c' \altoSax
        }
				\new Staff \with { instrumentName = "Tenor Sax" midiInstrument = #"" } {
          \transpose bf c' \tenorSax
        }
			>>
		>>
	}
}



