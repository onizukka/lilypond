\version "2.18.2"
\language "english"

% Se debe incluir antes que lilyjazz. Si no, lilyjazz no funciona.
#(set-global-staff-size 19)

% includes
\include "main.ily"

% Cabecera
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\header {
	title = "Una palabra tuya"
	instrument = "Alto Sax & Tenor Sax"
	composer = "Sonífera Isla"
	arranger = ""
	poet = ""
	tagline = "Edition: gbpsms@gmail.com"
}

% Márgenes, fuentes y distancias del papel
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\paper {
}

% Función musical global
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
global = {
	% Para pintar todos los números de compás debajo del pentagrama
	% \override Score.BarNumber #'break-visibility = #end-of-line-invisible
	% \override Score.BarNumber #'direction = #DOWN
	% \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

	\compressFullBarRests

	% Arregla el problema de tener el scoreMarkup añadido
	\revert MultiMeasureRest #'staff-position
	\override Voice.Rest #'staff-position = #0

	\set Timing.beamExceptions = #'()
	\set Timing.baseMoment = #(ly:make-moment 1/4)
	\set Timing.beatStructure = #'(1 1 1 1)

	\override ParenthesesItem.font-size = #0
}

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
	% Oculta el primer pentagrama del markup
	\override Staff.VerticalAxisGroup #'remove-first = ##t

	% Indicación de tiempo
	% \once \override Score.RehearsalMark.self-alignment-X = #LEFT
	% \once \override Score.RehearsalMark.extra-offset = #'(0 . 3)
	% \mark \jazzTempoMarkup "Easy Swing" c4 "112"

	% s1*2
	% \once \override Score.RehearsalMark.X-offset = #5
	% \mark \markup \boxed "A"

  \mkup "(Perc. intro ad.lib.)"
  s1*1

  \bar "||"
  s1*16

  \break
  s1*10

  \break
  s1*4

  \break
  \bar "[|:"
  s1*16
  \break
  s1*2
  \break
  s1*2
  \break
  s1*8

  \break
  s1*3
  \break
  s1*3
  \break
  s1*2
  \bar ":|]"


  \mkup "(Guitar solo)"
  \break
  s1*8
  s1*8
  \bar "||"
  \break
  s1*4
  s1*2
  \break
  s1*2
  \break
  s1*8

  \break
  s1*3
  \break
  s1*3
  \break
  s1*2

  \mkup "(1st & 2nd: Tenor solo. 3rd & 4th: Alto solo. 5th & 6th: Sax battle)"
  \mkup "(Repeat 6 xs)"
  \bar "[|:"
  \break
  s1*2
  \break
  s1*2
  \bar ":|]"

  \break
  s1*3
  \break
  s1*3
  \break
  s1*2

  \break
  s1*4
  \break
  s1*4

  \break
  s1*3/4
  s1
  \bar "|."
}

% Cifrado
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
scoreChords = \chords {
	% c1:m f2:m g2:m7

  s1*1
  s1*16
  s1*10
  s1*4


  s1*16
  s1*2
  s1*2
  s1*8

  s1*3
  s1*3
  s1*2

  s1*8
  s1*8
  s1*4

  s1*2
  s1*2
  s1*8

  s1*3
  s1*3
  s1*2

  d2:m g2
  d2:m g2
  d2:m g2
  f2 a2
}

% Música
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
altoSax = \relative c'' {

	\global
	\key d \minor

  R1

  R1*8
  R1*8

  R1*7
  r2 cs8. -- -^ \f cs16 -- -^ ~ cs8 d -^
  R1
  r2 cs8. -- -^ cs16 -- -^ ~ cs8 d16 d -^

  R1
  r2 cs8. -- -^ \f cs16 -- -^ ~ cs8 d -^
  R1
  r2 cs8. -- -^ cs16 -- -^ ~ cs8 d16 d -^

  \repeat volta 2 {
    R1*8
    R1*8

    r16 \mf bf, ( b d f af c b ) -> ~ b4 r
    r16 c, ( cs e g bf d cs ) -> ~ cs8. cs16 -. r16 c8. ->
    r16 bf, ( b d f af c b ) -> ~ b4 r
    r16 c, ( cs e g bf d cs ) -> ~ cs a ( b c cs a f' e

    d1 ) \sfp \<
    d4 -> \f \bendAfter #-5 r r2
    R1*2
    R1*4

    a8 -> \f a -> a4 -- -> r8. f16 ( g a g f 
    a8 ) -> a -> a4 -- -> r4 r16 f ( g gs
    a8 ) -> f -> d4 -- -> r8. f16 ( g a g f )
    f16 ( d f g gs a c bf a bf cs f e d cs e )

    a,8 -> a -> a4 -- -> r8. f16 ( g a g f
    a8 ) -> a -> a4 -- -> r8. c16 ( b c b c
    a8 ) -> f -> d4 -- -> r8. f16 ( g a g f )
    f16 ( d f g gs a c bf a bf cs f a, e' d8 ) ->
  }

  R1*8
  R1*8

  R1*4

  r16 \mf bf, ( b d f af c b ) -> ~ b4 r
  r16 c, ( cs e g bf d cs ) -> ~ cs8. cs16 -. r16 c8. ->
  r16 bf, ( b d f af c b ) -> ~ b4 r
  r16 c, ( cs e g bf d cs ) -> ~ cs a ( b c cs a f' e

  d1 ) \sfp \<
  d4 -> \f \bendAfter #-5 r r2
  R1*2
  R1*4

  r8 \f a -^ r4 r16 g8 -^ g16 -^ r4
  r8 a -^ r4 r16 g8 -^ g16 -^ r4
  r8 a -^ r4 r16 g8 -^ g16 -^ r4
  r8 a -^ a4 -^ b8. -^ cs16 -^ r8 d8 -^

  r8 a -^ r4 r16 g8 -^ g16 -^ r4
  r8 a -^ r4 r16 g8 -^ g16 -^ r8 c -^
  r8 a -^ r4 r16 g8 -^ g16 -^ r4
  r8 a -^ a4 -^ b8. -^ cs16 -^ r8 d8 -^

  \repeat volta 2 {
    \impro 16
  }

  a8 \f -> a -> a4 -- -> r8. f16 ( g a g f )
  a8 -> a -> a4 -- -> r4 r16 f ( g gs )
  a8 -> f -> d4 -- -> r8. f16 ( g a g f )
  f16 ( d f g gs a c bf a bf cs f e d cs e )

  a,8 -> a -> a4 -- -> r8. f16 ( g a g f )
  a8 -> a -> a4 -- -> r8. c16 ( b c b c )
  a8 -> f -> d4 -- -> r8. f16 ( g a g f )
  f16 ( d f g gs a c bf a bf cs f a, e' d8 ) ->

  R1
  r2 cs8. -> \f cs16 -> ~ cs8 d -^
  R1
  r2 cs8. -> cs16 -> ~ cs8 d16 d -^

  R1
  r2 cs8. -> \f cs16 -> ~ cs8 d -^
  R1
  r2 cs8. -> cs16 -> ~ cs8 d16 d -^

  \time 3/4
  r16 a, ( gs a c' bf f ef ) -. r8 e -> ~

  \time 4/4
  e1 \fermata
}

tenorSax = \relative c'' {

	\global
	\key d \minor

  R1

  R1*8
  R1*8

  R1*7
  r2 a'8. -- -^ \f a16 -- -^ ~ a8 a -^
  R1
  r2 a8. -- -^ a16 -- -^ ~ a8 a16 a -^

  R1
  r2 a8. -- -^ \f a16 -- -^ ~ a8 a -^
  R1
  r2 a8. -- -^ a16 -- -^ ~ a8 b16 b -^

  \repeat volta 2 {
    R1*8
    R1*8

    r16 \mf fs, ( g b d f af g ) -> ~ g4 r
    r16 gs, ( a cs e g bf a ) -> ~ a8. a16 -. r16 af8. ->
    r16 fs, ( g b d f af g ) -> ~ g4 r
    r16 gs, ( a cs e g bf a ) -> ~ a a, ( b c cs a f' e

    d1 ) \sfp \<
    d4 -> \f \bendAfter #-5 r r2
    R1*2
    R1*4

    a8 -> \f a -> a4 -- -> r8. f16 ( g a g f 
    a8 ) -> a -> a4 -- -> r4 r16 f ( g gs
    a8 ) -> f -> d4 -- -> r8. f16 ( g a g f )
    f16 ( d f g gs a c bf a bf cs f e d cs e )

    a,8 -> a -> a4 -- -> r8. f16 ( g a g f
    a8 ) -> a -> a4 -- -> r8. c16 ( b c b c
    a8 ) -> f -> d4 -- -> r8. f16 ( g a g f )
    f16 ( d f g gs a c bf a bf cs f a, e' d8 ) ->
  }

  R1*8
  R1*8

  R1*4

  r16 \mf fs, ( g b d f af g ) -> ~ g4 r
  r16 gs, ( a cs e g bf a ) -> ~ a8. a16 -. r16 af8. ->
  r16 fs, ( g b d f af g ) -> ~ g4 r
  r16 gs, ( a cs e g bf a ) -> ~ a a, ( b c cs a f' e

  d1 ) \sfp \<
  d4 -> \f \bendAfter #-5 r r2
  R1*2
  R1*4

  r8 \f a -^ r4 r16 g8 -^ g16 -^ r4
  r8 a -^ r4 r16 g8 -^ g16 -^ r4
  r8 a -^ r4 r16 g8 -^ g16 -^ r4
  r8 a -^ a4 -^ b8. -^ cs16 -^ r8 d8 -^

  r8 d -^ r4 r16 d8 -^ d16 -^ r4
  r8 d -^ r4 r16 d8 -^ d16 -^ r8 f -^
  r8 d -^ r4 r16 d8 -^ d16 -^ r4
  r8 c -^ c4 -^ b8. -^ cs16 -^ r8 d8 -^

  \repeat volta 2 {
    \impro 16
  }

  a8 \f -> a -> a4 -- -> r8. f16 ( g a g f )
  a8 -> a -> a4 -- -> r4 r16 f ( g gs )
  a8 -> f -> d4 -- -> r8. f16 ( g a g f )
  f16 ( d f g gs a c bf a bf cs f e d cs e )

  a,8 -> a -> a4 -- -> r8. f16 ( g a g f )
  a8 -> a -> a4 -- -> r8. c16 ( b c b c )
  a8 -> f -> d4 -- -> r8. f16 ( g a g f )
  f16 ( d f g gs a c bf a bf cs f a, e' d8 ) ->

  R1
  r2 a'8. -> \f a16 -> ~ a8 a -^
  R1
  r2 a8. -> a16 -> ~ a8 a16 a -^

  R1
  r2 a8. -> \f a16 -> ~ a8 a -^
  R1
  r2 a8. -> a16 -> ~ a8 b16 b -^

  \time 3/4
  r16 a, ( gs a c bf f ef ) -. r8 c' -> ~

  \time 4/4
  c1 \fermata
}

% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\book {
	\header { instrument = "Tenor Sax" }
  \bookOutputName "Una palabra tuya - Tenor"

	\score {
		<<
			\scoreMarkup
			\new StaffGroup <<
        \scoreChords
        \transpose bf c \scoreChords
				\new Staff { \transpose bf c' \tenorSax }
			>>
		>>
	}
}

\book {
  \bookOutputName "Una palabra tuya"

	\paper {
		% Descomentar para score
		% #(set-paper-size "a4" 'landscape)
		% indent = 20
		% systems-per-page = 2
	}

	\score {
		% Descomentar para midi
		% \midi { \tempo 4 = 110 }

		% NO USAR layout-set-staff-size O SE ROMPE lilyjazz!!!
		\layout { }

		<<
			\scoreMarkup
			\new StaffGroup <<
        \transpose ef c \scoreChords
				\new Staff \with { instrumentName = "Alto Sax" midiInstrument = #"" } {
          \transpose ef c' \altoSax
        }
        \transpose bf c \scoreChords
				\new Staff \with { instrumentName = "Tenor Sax" midiInstrument = #"" } {
          \transpose bf c' \tenorSax
        }
			>>
		>>
	}
}



