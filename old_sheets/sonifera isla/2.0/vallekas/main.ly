\version "2.18.2"
\language "english"

% Se debe incluir antes que lilyjazz. Si no, lilyjazz no funciona.
#(set-global-staff-size 19)

% includes
\include "main.ily"

% Cabecera
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\header {
	title = "Vallekas"
	instrument = "Alto Sax & Tenor Sax"
	composer = "Sonífera Isla"
	arranger = ""
	poet = ""
	tagline = "Edition: gbpsms@gmail.com"
}

% Márgenes, fuentes y distancias del papel
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\paper {
}

% Función musical global
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
global = {
	% Para pintar todos los números de compás debajo del pentagrama
	% \override Score.BarNumber #'break-visibility = #end-of-line-invisible
	% \override Score.BarNumber #'direction = #DOWN
	% \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

	\compressFullBarRests

	% Arregla el problema de tener el scoreMarkup añadido
	\revert MultiMeasureRest #'staff-position
	\override Voice.Rest #'staff-position = #0

	\set Timing.beamExceptions = #'()
	\set Timing.baseMoment = #(ly:make-moment 1/4)
	\set Timing.beatStructure = #'(1 1 1 1)

	\override ParenthesesItem.font-size = #0
}

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
	% Oculta el primer pentagrama del markup
	\override Staff.VerticalAxisGroup #'remove-first = ##t

	% Indicación de tiempo
	% \once \override Score.RehearsalMark.self-alignment-X = #LEFT
	% \once \override Score.RehearsalMark.extra-offset = #'(0 . 3)
	% \mark \jazzTempoMarkup "Easy Swing" c4 "112"

	% s1*2
	% \once \override Score.RehearsalMark.X-offset = #5
	% \mark \markup \boxed "A"

	\mark \jazzTempoMarkup "Easy Swing" c4 "130"
  s1*4*6/4
  s1*2*6/4

  \break
  s1*2*6/4
  s1*4*6/4

  \break
  s1*2*6/4

  \break
  s1*2*6/4
  \time 4/4
  s1*4

  \break
  s1*4
  \eolMark \daCapo

  \break
  \bar "||"
  s1*3*6/4
  \time 4/4
  s1*2

  \break
  \mkup "(Keyboard & guitar solo)"
  s1*8
  s1*8
  s1*8
  s1*8
  s1*8
  s1*8

  \break
  \bar "[|:"
  s1*4

  \break
  \bar ":|][|:"
  s1*4

  \break
  \bar ":|]"
  s1*4*6/4
  s1*2*6/4

  \break
  s1*2*6/4
  \time 4/4
  s1
  \bar "|."
}

% Cifrado
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
scoreChords = \chords {
	% c1:m f2:m g2:m7
}

% Música
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
altoSax = \relative c'' {

	\global
	\key d \minor
  \time 6/4

  R1*4*6/4

  r2. a4 \mf \< -- -> c -- -> d -- ->
  r2. \! a4 \mf \< -- -> c -- -> f -- ->
  d4 -^ \f r r a4 \mf \< -- -> c -- -> d -- ->
  c2 -> \sfp \< r4 \f c a8 g f4 -- ->

  R1*4*6/4

  a8 \mf d c a g2 ~ g8 d f g
  a8 d c a g4 ~ g8 f g f d4 -.
  a'8 d c a g2 ~ g8 d f g
  a8 d c a g4 c a8 g f4 -- ->

  \time 4/4
  R1*4

  r4 d' -- -^ \f f -> ~ f8 f
  e8 c a g a4 c8 g ~
  g4 g -- -^ bf -> ~ bf8 bf
  a8 f d c d f -> ~ f4

  \time 6/4
  f8 \f f -. r4 r2 r2
  g8 g -. r4 r2 r2
  f8 f -. r4 r2 r2

  \time 4/4
  g1 -> \> ~
  g

  R1*8 \!
  R1*8
  R1*8
  R1*8
  R1*8
  R1*8

  \repeat volta 2 {
    r4 d' -- -^ \f f -> ~ f8 f
    e8 c a g a4 c8 g ~
    g4 g -- -^ bf -> ~ bf8 bf
    a8 f d c d f -> ~ f4
  }

  \repeat volta 2 {
    r4 d' -- -^ \f f -> ~ f8 f
    e8 c a g a4 c8 g ~
    g4 g -- -^ bf -> ~ bf8 bf
    a8 f d c d f -> ~ f4
  }

  \time 6/4
  R1*4*6/4

  d8 \f d -. r f g [ g -. ] r4 r2
  d8 d -. r f g [ g -. ] r4 r2
  d8 d -. r f g [ g -. ] r4 r2
  f8 f -. r d c4 c' -- -> a8 g f4 -.

  \time 4/4
  \times 2/3 { d'4 -- -> \ff d -- -> f -- -> }
  \times 2/3 { g -- -> g -^ r }
}

tenorSax = \relative c'' {

	\global
	\key d \minor
  \time 6/4

  R1*4*6/4

  r2. a4 \mf \< -- -> c -- -> d -- ->
  r2. \! a4 \mf \< -- -> c -- -> f -- ->
  d4 -^ \f r r a4 \mf \< -- -> c -- -> d -- ->
  c2 -> \sfp \< r4 \f c' a8 g f4 -- ->

  R1*4*6/4

  a,8 \mf d c a g2 ~ g8 d f g
  a8 d c a g4 ~ g8 f g f d4 -.
  a'8 d c a g2 ~ g8 d f g
  a8 d c a g4 c a8 g f4 -- ->

  \time 4/4
  R1*4

  r4 d' -- -^ \f f -> ~ f8 f
  e8 c a g a4 c8 g ~
  g4 g -- -^ bf -> ~ bf8 bf
  a8 f d c d f -> ~ f4

  \time 6/4
  d'8 \f d -. r4 r2 r2
  d8 d -. r4 r2 r2
  c8 c -. r4 r2 r2

  \time 4/4
  c1 -> \> ~
  c

  R1*8 \!
  R1*8
  R1*8
  R1*8
  R1*8
  R1*8

  \repeat volta 2 {
    r4 d -- -^ \f f -> ~ f8 f
    e8 c a g a4 c8 g ~
    g4 g -- -^ bf -> ~ bf8 bf
    a8 f d c d f -> ~ f4
  }

  \repeat volta 2 {
    r4 a' -- -^ \f d -> ~ d8 d
    c8 a e c e4 a8 d, ~
    d4 d -- -^ g -> ~ g8 g
    f8 d a f a c -> ~ c4
  }

  \time 6/4
  R1*4*6/4

  d,8 \f d -. r f g [ g -. ] r4 r2
  d8 d -. r f g [ g -. ] r4 r2
  d8 d -. r f g [ g -. ] r4 r2
  f8 f -. r d c4 e' -- -> c8 bf a4 -.

  \time 4/4
  \times 2/3 { d4 -- -> \ff d -- -> f -- -> }
  \times 2/3 { g -- -> g -^ r }
}

% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\book {
	\header { instrument = "Tenor Sax" }
  \bookOutputName "Vallekas - Tenor"

	\score {
		<<
			\scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\new Staff { \transpose bf c' \tenorSax }
			>>
		>>
	}
}

\book {
  \bookOutputName "Vallekas"

	\paper {
		% Descomentar para score
		% #(set-paper-size "a4" 'landscape)
		% indent = 20
		% systems-per-page = 2
	}

	\score {
		% Descomentar para midi
		% \midi { \tempo 4 = 110 }

		% NO USAR layout-set-staff-size O SE ROMPE lilyjazz!!!
		\layout { }

		<<
			\scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\new Staff \with { instrumentName = "Alto Sax" midiInstrument = #"" } {
          \transpose ef c' \altoSax
        }
				\new Staff \with { instrumentName = "Tenor Sax" midiInstrument = #"" } {
          \transpose bf c' \tenorSax
        }
			>>
		>>
	}
}



