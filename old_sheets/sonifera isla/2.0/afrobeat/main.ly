\version "2.18.2"
\language "english"

% Se debe incluir antes que lilyjazz. Si no, lilyjazz no funciona.
#(set-global-staff-size 19)

% includes
\include "main.ily"

% Cabecera
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\header {
	title = "Afrobeat"
	instrument = "Alto Sax & Tenor Sax"
	composer = "Sonifera Isla"
	arranger = ""
	poet = ""
	tagline = "Edition: gbpsms@gmail.com"
}

% Márgenes, fuentes y distancias del papel
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\paper {
}

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
	% Oculta el primer pentagrama del markup
	\override Staff.VerticalAxisGroup #'remove-first = ##t

	% Indicación de tiempo
	% \once \override Score.RehearsalMark.self-alignment-X = #LEFT
	% \once \override Score.RehearsalMark.extra-offset = #'(0 . 3)
	% \mark \jazzTempoMarkup "Easy Swing" c4 "112"

	% s1*2
	% \once \override Score.RehearsalMark.X-offset = #5
	% \mark \markup \boxed "A"

  s1*2
  \break
  s1*2

  \break
  s1*4
  s1*8

  \break
  \bar "||"
  \mark \segno
  s1*4
  \break
  s1*4
  s1*2
  \break
  s1*2
  s1*4
  s1*8

  \break
  \bar "[|:"
  s1*4
  \bar ":|]"

  \break
  s1*2
  \break
  s1*3
  \break
  s1*3
  
  \break
  s1*3
  \break
  s1*3
  \eolMark \toSegno

  \break
  \bar "||"
  \mkup "(Keyboard solo ad.lib.)"
  s1

  \break
  \bar "||"
  s1*8
  s1*8
  s1*8
  s1*8

  \break
  \bar "[|:"
  s1*4
  \bar ":|]"

  \break
  s1*2
  \break
  s1*3
  \break
  s1*3

  \break
  s1*2
  \break
  s1*3
  \break
  s1*3

  \break
  s1*3
  \break
  s1*3

  \break
  \bar "||"
  \mkup "(Audience sings ad.lib.)"
  s1

  \break
  \bar "[|:"
  \mkup "(On singer's cue)"
  s1*2
  s1*3/4
  \break
  \bar ":|][|:"
  s1*2
  s1*3/4
  \break
  \bar ":|][|:"
  \mkup "(Repeat 1st x ad.lib. 2nd x on singer's cue)"
  s1*2
  s1*3/4
  \break
  s1*2
  \break
  s1*3/4
  \mkup "(Slow)"
  s1*2*3/4
  \bar "|."
}




% Función musical global
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
global = {
	% Para pintar todos los números de compás debajo del pentagrama
	% \override Score.BarNumber #'break-visibility = #end-of-line-invisible
	% \override Score.BarNumber #'direction = #DOWN
	% \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

	\compressFullBarRests

	% Arregla el problema de tener el scoreMarkup añadido
	\revert MultiMeasureRest #'staff-position
	\override Voice.Rest #'staff-position = #0

	\set Timing.beamExceptions = #'()
	\set Timing.baseMoment = #(ly:make-moment 1/4)
	\set Timing.beatStructure = #'(1 1 1 1)

	\override ParenthesesItem.font-size = #0
}

% Cifrado
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
scoreChords = \chords {
	% c1:m f2:m g2:m7
}

% Música
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
altoSax = \relative c'' {

	\global
	\key c \minor

	r8 \f c16 g bf ( a ) f fs g8 -> d16 ef f8 -. b, ~ -> \fp
  b1 \< 
  r8 \! c'16 -> \f g bf16 ( a ) f fs g8 -> d16 ef f8 -. g ~ -> \fp 
  g1 \<

  c4 -> \! \bendAfter #-3 r4 r2
  R1*3
  R1*8

  R1*2
  r2 r4 r8 \mf d, (
  ef ) d ( c ) d ( ef4 ) c8 b ~

  b1 \>
  R1 \!
  r2 r4 r8 \mf d (
  ef ) d ( c ) d ( ef4 ) c8 b ~

  b1 \>
  R1 \!
  r2 r4 r8 \mf d (
  ef ) d ( c ) d ( ef4 ) c8 b ~

  b1
  bf
  a 
  g \>

  R1*8 \!

  \repeat volta 2 {
    c16 -- \mf c -. r8 r8 c16 -- c -. r2
    c16 -- c -. r8 r8 c16 -- c -. r2
    c16 -- c -. r8 r8 c16 -- c -. r2
    d16 -- d -. r8 r8 d16 -- d -. r4 d16 -- d -. r8
  }

  r8 \f c'16 ( g -. ) r16 bf -. r a ( f8 g ef c -. )
  r8 c'16 ( g -. ) r16 bf -. r a ( f8. g16 ~ g8 ef -. )
  r8 c'16 ( g -. ) r16 bf -. r a ( f8 g ef c -. )
  r8 d -^ r8 ef -^ r8 f -> fs -> g -^

  r8 c16 ( g -. ) r16 bf -. r a ( f8 g ef c -. )
  r8 c'16 ( g -. ) r16 bf -. r a ( f8. g16 ~ g8 ef -. )
  r8 c'16 ( g -. ) r16 bf -. r a ( f8 g ef c -. )
  r8 d -^ r8 ef -^ r8 f -> fs -> g -^

  r2 r4 r8 \mp bf,16 d
  ef ef ef ef -. r4 r4 r8 bf16 d
  ef ef ef ef -. r4 r4 r8 bf16 d

  ef ef ef ef -. r4 r4 r8 bf16 d
  ef ef ef ef -. r4 r4 r8 bf16 d
  ef ef ef ef -. r4 r2

  R1

  R1*8
  R1*8
  R1*8
  R1*8

  \repeat volta 2 {
    c16 -- \mf c -. r8 r8 c16 -- c -. r2
    c16 -- c -. r8 r8 c16 -- c -. r2
    c16 -- c -. r8 r8 c16 -- c -. r2
    d16 -- d -. r8 r8 d16 -- d -. r4 d16 -- d -. r8
  }

  r8 \f c'16 ( g -. ) r16 bf -. r a ( f8 g ef c -. )
  r8 c'16 ( g -. ) r16 bf -. r a ( f8. g16 ~ g8 ef -. )
  r8 c'16 ( g -. ) r16 bf -. r a ( f8 g ef c -. )
  r8 d -^ r8 ef -^ r8 f -> fs -> g -^

  r8 c16 ( g -. ) r16 bf -. r a ( f8 g ef c -. )
  r8 c'16 ( g -. ) r16 bf -. r a ( f8. g16 ~ g8 ef -. )
  r8 c'16 ( g -. ) r16 bf -. r a ( f8 g ef c -. )
  r8 d -^ r8 ef -^ r8 f -> fs -> g -^

  r8 ef'16 ( c -. ) r16 d -. r c ( a8 bf g ef -. )
  r8 ef'16 ( c -. ) r16 d -. r c ( a8. bf16 ~ bf8 g -. )
  r8 ef'16 ( c -. ) r16 d -. r c ( a8 bf g ef -. )
  a8. -> bf16 -> ~ bf8 c -> ~ c16 cs8. -> d4 -^

  r8 ef16 ( c -. ) r16 d -. r c ( a8 bf g ef -. )
  r8 ef'16 ( c -. ) r16 d -. r c ( a8. bf16 ~ bf8 g -. )
  r8 ef'16 ( c -. ) r16 d -. r c ( a8 bf g ef -. )
  a8. -> bf16 -> ~ bf8 c -> ~ c16 cs8. -> d4 -^

  r2 r4 r8 \mf bf,16 d
  ef ef ef ef -. r4 r4 r8 bf16 d
  ef ef ef ef -. r4 r4 r8 bf16 d

  ef ef ef ef -. r4 r4 r8 bf16 d
  ef ef ef ef -. r4 r4 r8 bf16 d
  ef ef ef ef -. r4 r2

  R1

  R1*2
  \time 3/4
  R1*3/4
  
  \time 4/4
  c8. \mf ef16 -. r8 g -> ~ g4 g16 af f ef -> ~
  ef8. c16 -. r8 d -> ~ d4 af'16 bf g f -> ~
  \time 3/4
  f8 ef -. r16 bf' g f -> ~ f8 ef -.

  \time 4/4
  \repeat volta 2 {
    ef8. g16 -. r8 bf -> ~ b4 bf16 c bf g -> ~
    g8. bf16 -. r8 f -> ~ f4 af16 bf g f -> ~
  }
  \alternative {
    { \time 3/4
      f8 ef -. r16 bf' g f -> ~ f8 ef -. }
    { \time 4/4
      f8 ef -. r16 bf' g f -> ~ f8 ef -. af16 bf g f -> ~ }
  }

  f8 ef -. r16 bf' g f -> ~ f8 ef -. af16 bf g f -> ~
  \time 3/4
  f8 ef -. r16 bf' g f -> ~ f8 ef -. 

  ef4 \rit f g
  c2. \! \fermata
}

tenorSax = \relative c'' {

	\global
	\key c \minor

	r8 \f c16 g bf ( a ) f fs g8 -> d16 ef f8 -. b, ~ -> \fp
  b1 \< 
  r8 \! c'16 -> \f g bf16 ( a ) f fs g8 -> d16 ef f8 -. b ~ -> \fp
  b1 \<

  c4 -> \! \bendAfter #-3 r4 r2
  R1*3

  R1*8

  R1*2
  r2 r4 r8 \mf d, (
  ef ) d ( c ) d ( ef4 ) c8 g' ~

  g1 \>
  R1 \!
  r2 r4 r8 \mf d (
  ef ) d ( c ) d ( ef4 ) c8 g' ~

  g1 \>
  R1 \!
  r2 r4 r8 \mf d (
  ef ) d ( c ) d ( ef4 ) c8 g' ~

  g1 ~
  g2. ef4
  f1
  ef \>

  R1*8 \!

  \repeat volta 2 {
    g16 -- \mf g -. r8 r8 g16 -- g -. r2
    af16 -- af -. r8 r8 af16 -- af -. r2
    a16 -- a -. r8 r8 a16 -- a -. r2
    bf16 -- bf -. r8 r8 bf16 -- bf -. r4 b16 -- b -. r8
  }

  r8 \f c16 ( g -. ) r16 bf -. r a ( f8 g ef c -. )
  r8 c'16 ( g -. ) r16 bf -. r a ( f8. g16 ~ g8 ef -. )
  r8 c'16 ( g -. ) r16 bf -. r a ( f8 g ef c -. )
  r8 f -^ r8 g -^ r8 a -> as -> b -^

  r8 c16 ( g -. ) r16 bf -. r a ( f8 g ef c -. )
  r8 c'16 ( g -. ) r16 bf -. r a ( f8. g16 ~ g8 ef -. )
  r8 c'16 ( g -. ) r16 bf -. r a ( f8 g ef c -. )
  r8 f -^ r8 g -^ r8 a -> as -> b -^

  r2 r4 r8 \mp ef,16 f
  g g g g -. r4 r4 r8 ef16 f
  g g g g -. r4 r4 r8 ef16 f

  g g g g -. r4 r4 r8 ef16 f
  g g g g -. r4 r4 r8 ef16 f
  g g g g -. r4 r2

  R1

  R1*8
  R1*8
  R1*8
  R1*8

  \repeat volta 2 {
    g16 -- \mf g -. r8 r8 g16 -- g -. r2
    af16 -- af -. r8 r8 af16 -- af -. r2
    a16 -- a -. r8 r8 a16 -- a -. r2
    bf16 -- bf -. r8 r8 bf16 -- bf -. r4 b16 -- b -. r8
  }

  r8 \f c16 ( g -. ) r16 bf -. r a ( f8 g ef c -. )
  r8 c'16 ( g -. ) r16 bf -. r a ( f8. g16 ~ g8 ef -. )
  r8 c'16 ( g -. ) r16 bf -. r a ( f8 g ef c -. )
  r8 f -^ r8 g -^ r8 a -> as -> b -^

  r8 c16 ( g -. ) r16 bf -. r a ( f8 g ef c -. )
  r8 c'16 ( g -. ) r16 bf -. r a ( f8. g16 ~ g8 ef -. )
  r8 c'16 ( g -. ) r16 bf -. r a ( f8 g ef c -. )
  r8 f -^ r8 g -^ r8 a -> as -> b -^

  r8 c16 ( g -. ) r16 bf -. r a ( f8 g ef c -. )
  r8 c'16 ( g -. ) r16 bf -. r a ( f8. g16 ~ g8 ef -. )
  r8 c'16 ( g -. ) r16 bf -. r a ( f8 g ef c -. )
  f8. -> g16 -> ~ g8 a -> ~ a16 as8. -> b4 -^

  r8 c16 ( g -. ) r16 bf -. r a ( f8 g ef c -. )
  r8 c'16 ( g -. ) r16 bf -. r a ( f8. g16 ~ g8 ef -. )
  r8 c'16 ( g -. ) r16 bf -. r a ( f8 g ef c -. )
  f8. -> g16 -> ~ g8 a -> ~ a16 as8. -> b4 -^

  r2 r4 r8 \mf bf,16 d
  ef ef ef ef -. r4 r4 r8 bf16 d
  ef ef ef ef -. r4 r4 r8 bf16 d

  ef ef ef ef -. r4 r4 r8 bf16 d
  ef ef ef ef -. r4 r4 r8 bf16 d
  ef ef ef ef -. r4 r2

  R1

  R1*2
  \time 3/4
  R1*3/4
  
  \time 4/4
  c8. \mf ef16 -. r8 g -> ~ g4 g16 af f ef -> ~
  ef8. c16 -. r8 d -> ~ d4 af'16 bf g f -> ~
  \time 3/4
  f8 ef -. r16 bf' g f -> ~ f8 ef -.

  \time 4/4
  \repeat volta 2 {
    g8. c16 -. r8 ef -> ~ ef4 ef16 f d c -> ~
    c8. af16 -. r8 bf -> ~ bf4 f'16 g ef d -> ~
  }
  \alternative {
    { \time 3/4
      d8 c -. r16 g' ef d -> ~ d8 c -. }
    { \time 4/4
      d8 c -. r16 g' ef d -> ~ d8 c -. f16 g ef d -> ~ }
  }

  d8 c -. r16 g' ef d -> ~ d8 c -. f16 g ef d -> ~
  \time 3/4
  d8 c -. r16 g' ef d -> ~ d8 c -. 

  c4 \rit d bf
  c2. \! \fermata
}

% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\book {
	\header { instrument = "Tenor Sax" }
  \bookOutputName "Afrobeat - Tenor"
	\score {
		<<
			\scoreMarkup
			\new StaffGroup <<
        \scoreChords
				\new Staff { \transpose bf c' \tenorSax }
			>>
		>>
	}
}

\book {
  \bookOutputName "Afrobeat"

	\paper {
		% Descomentar para score
		% #(set-paper-size "a4" 'landscape)
		% indent = 20
		% systems-per-page = 2
	}

	\score {
		% Descomentar para midi
		% \midi { \tempo 4 = 110 }

		% NO USAR layout-set-staff-size O SE ROMPE lilyjazz!!!
		\layout { }

		<<
			\scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\new Staff \with { instrumentName = "Alto Sax" midiInstrument = #"" } {
          \transpose ef c' \altoSax
        }
				\new Staff \with { instrumentName = "Tenor Sax" midiInstrument = #"" } {
          \transpose bf c' \tenorSax
        }
			>>
		>>
	}
}



