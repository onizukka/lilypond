\version "2.18.2"
\language "english"

% Se debe incluir antes que lilyjazz. Si no, lilyjazz no funciona.
#(set-global-staff-size 19)

% includes
\include "main.ily"

% Cabecera
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\header {
	title = "Sueños"
	instrument = "Alto Sax & Tenor Sax"
	composer = "Sonífera Isla"
	arranger = ""
	poet = ""
	tagline = "Edition: gbpsms@gmail.com"
}

% Márgenes, fuentes y distancias del papel
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\paper {
}

% Función musical global
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
global = {
	% Para pintar todos los números de compás debajo del pentagrama
	% \override Score.BarNumber #'break-visibility = #end-of-line-invisible
	% \override Score.BarNumber #'direction = #DOWN
	% \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

	\compressFullBarRests

	% Arregla el problema de tener el scoreMarkup añadido
	\revert MultiMeasureRest #'staff-position
	\override Voice.Rest #'staff-position = #0

	\set Timing.beamExceptions = #'()
	\set Timing.baseMoment = #(ly:make-moment 1/4)
	\set Timing.beatStructure = #'(1 1 1 1)

	\override ParenthesesItem.font-size = #0
}

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
	% Oculta el primer pentagrama del markup
	\override Staff.VerticalAxisGroup #'remove-first = ##t

	% Indicación de tiempo
	% \once \override Score.RehearsalMark.self-alignment-X = #LEFT
	% \once \override Score.RehearsalMark.extra-offset = #'(0 . 3)
	% \mark \jazzTempoMarkup "Easy Swing" c4 "112"

	% s1*2
	% \once \override Score.RehearsalMark.X-offset = #5
	% \mark \markup \boxed "A"

  \mkup "(Keyboard ad.lib.)"
  s1

  \bar "||"
  \mkup "(On Alto sax cue)"
  s1

  \break
  \bar "[|:"
  s1*3
  \break
  s1*2
  s1*8

  \break
  s1
  s1*6/4
  s1
  s1*6/4
  s1

  \break
  s1*3
  \break
  s1*2
  s1*6
  \break
  s1
  \bar ":|]"
  s1

  s1
  \break
  s1*3
  \break
  s1*3
  \break
  s1
  s1*7
  s1*6/4

  \mkup "(Slow)"
  s1*2

  \break
  \bar "||"
  s1
  s1*6/4
  s1*2
  s1*8

  \bar "||"
  \mkup "(Keyboard ad.lib.)"
  s1
  \break
  \bar "||"
  \mkup "(Saxes Q&A ad.lib.)"
  s1
  \bar "||"
  \mkup "(On Alto Sax cue)"
  s1

  \break
  \bar "[|:"
  \mkup "(Accel. poco a poco)"
  s1*2
  \break
  s1*2
  \bar ":|]"
  s1

  \break
  \bar "[|:"
  \mkup "(A tempo)"
  s1*2
  \break
  s1*2
  \bar ":|]"
  s1

  \break
  s1*3
  \break
  s1*3
  \break
  s1*2
  \bar "|."
}

% Cifrado
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
scoreChords = \chords {

  s1

  s1

  s1*3
  s1*2
  s1*8

  s1
  s1*6/4
  s1
  s1*6/4
  s1

  s1*3
  s1*2
  s1*6
  s1
  s1

  s1
  s1*3
  s1*3
  s1
  s1*7
  s1*6/4

  s1*2

  s1
  s1*6/4
  s1*2
  s1*8

  s1
  g1:m
}

% Música
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
altoSax = \relative c'' {

	\global
	\key c \minor

  R1
	r2 af16 \f g fs g g'8 f16 ef -.

  \repeat volta 2 {
    r4 ef -^ ef8. -^ ef16 -^ r ef -> ~ ef8 
    ef4 -^ ef -^ af,16 g fs g g'8 -> f16 ef -.
    r4 ef -^ ef8. -^ ef16 -^ r ef -> ~ ef8 
    ef4 -^ ef -^ af,16 g fs g g'8 -> f16 ef -.
    r4 ef -^ ef8. -^ ef16 -^ r ef -> ~ ef8  

    R1*8

    R1
    \time 6/4
    R1*6/4
    \time 4/4
    R1
    \time 6/4
    R1*6/4
    \time 4/4
    R1

    r16 \mf ef, c8 -. ef -> c16 ef -. r16 ef c8 -. ef8. -> d16 -. 
    r16 d b8 -. d -> b16 d -. r16 d b8 -. d4 ->
    c8 -. a -. c -. a -. d -. b -. d8. -> c16 -. 
    r16 ef c8 -. ef -> c16 ef -. r16 ef c8 -. ef8 ( bf )
    c4 ( \< d ef f

    g2 ) \f \> r2 \!
    R1*5
  }
  \alternative {
    { r2 af16 \f g fs g g'8 f16 ef -. }
    { r2 af,16 \f g fs g g'8 -. f16 ef -> ~ }
  }

  ef4 c -. ef8 -. c16 ef -. r16 g f8 -> ~
  f8 d -. b4 -. b8 -. d16 f -. r16 af g8 -> ~
  g8 ef -. c4 -. ef8 -. c16 ef -. r16 g f8 -> ~
  f8 d -. b4 -. b8 -. d16 f -. r16 af g8 -> ~

  g8 ef -. c4 -. ef8 -. c16 ef -. r16 g f8 -> ~
  f8 d -. b4 -. b8 -. d16 f -. r16 af g8 -> ~
  g8 ef -. c4 -. ef8 -. c16 ef -. r16 g f8 -> ~
  f8 [ d -. ] b8 -. r16 g af g fs g g'8 -> b,16 c -.

  R1*7
  \time 6/4
  R1*6/4
  \time 4/4
  R1*2

  R1
  \time 6/4
  R1*6/4
  \time 4/4
  R1*2

  R1*7
  R1 \fermataMarkup
  R1
  R1
	r2 af16 \f g fs g g'8 -- -> f8 -- ->

  \repeat volta 2 {
    ef4 -- -^ ef -- -^ ef8. -- -^ ef16 -^ r ef -> ~ ef8 
    f4 -- -^ f -- -^ af,16 g fs g g'8 -- -> f8 -- ->
    ef4 -- -^ ef -- -^ ef8. -- -^ ef16 -^ r ef -> ~ ef8 
  }
  \alternative {
    { f4 -^ f -^ af,16 g fs g g'8 -- -> f8 -- -> }
    { f4 -^ f -^ af,16 g fs g g'8 f16 ef -. }
  }

  \repeat volta 2 {
    r4 ef -^ \f ef8. -^ ef16 -^ r ef -> ~ ef8 
    ef4 -^ ef -^ af,16 g fs g g'8 f16 ef -.
    r4 ef -^ ef8. -^ ef16 -^ r ef -> ~ ef8 
  }
  \alternative {
    { ef4 -^ ef -^ af,16 g fs g g'8 f16 ef -. }
    { ef4 -^ ef -^ af,16 g fs g g'8 -. f16 ef -> ~ }
  }

  ef4 c -. ef8 -. c16 ef -. r16 g f8 -> ~
  f8 d -. b4 -. b8 -. d16 f -. r16 af g8 -> ~
  g8 ef -. c4 -. ef8 -. c16 ef -. r16 g f8 -> ~
  f8 d -. b4 -. b8 -. d16 f -. r16 af g8 -> ~

  g8 ef -. c4 -. ef8 -. c16 ef -. r16 g f8 -> ~
  f8 d -. b4 -. b8 -. d16 f -. r16 af g8 -> ~
  g8 ef -. c4 -. ef8 -. c16 ef -. r16 g f8 -> ~
  f8 [ d -. ] b8 -. r16 g af g fs g g'8 -> b,16 c -.
}

tenorSax = \relative c'' {

	\global
	\key c \minor

  R1
	r2 af16 \f g fs g g'8 f16 ef -.

  \repeat volta 2 {
    r4 ef -^ ef8. -^ ef16 -^ r ef -> ~ ef8 
    ef4 -^ ef -^ af,16 g fs g g'8 -> f16 ef -.
    r4 ef -^ ef8. -^ ef16 -^ r ef -> ~ ef8 
    ef4 -^ ef -^ af,16 g fs g g'8 -> f16 ef -.
    r4 ef -^ ef8. -^ ef16 -^ r ef -> ~ ef8  

    R1*8

    R1
    \time 6/4
    R1*6/4
    \time 4/4
    R1
    \time 6/4
    R1*6/4
    \time 4/4
    R1

    r16 \mf c af8 -. c -> af16 c -. r16 c af8 -. c8. -> b16 -. 
    r16 b g8 -. b -> g16 b -. r16 b g8 -. b4 ->
    a8 -. f -. a -. f -. b -. g -. b8. -> g16 -. 
    r16 c g8 -. c -> g16 c -. r16 c g8 -. c8 ( g )
    af4 ( \< bf c bf

    ef2 ) \f \> r2 \!
    R1*5
  }
  \alternative {
    { r2 af,16 \f g fs g g'8 f16 ef -. }
    { r2 af,16 \f g fs g g'8 -. f16 ef -> ~ }
  }

  ef4 c -. ef8 -. c16 ef -. r16 g f8 -> ~
  f8 d -. b4 -. b8 -. d16 f -. r16 af g8 -> ~
  g8 ef -. c4 -. ef8 -. c16 ef -. r16 g f8 -> ~
  f8 d -. b4 -. b8 -. d16 f -. r16 af ef'8 -> ~

  ef8 c -. g4 -. c8 -. g16 c -. r16 ef d8 -> ~
  d8 b -. g4 -. g8 -. b16 d -. r16 f ef8 -> ~
  ef8 c -. g4 -. c8 -. g16 c -. r16 ef d8 -> ~
  d8 [ b -. ] g8 -. r16 d f ef d ef f8 -> g16 c, -.

  R1*7
  \time 6/4
  R1*6/4
  \time 4/4
  R1*2

  R1
  \time 6/4
  R1*6/4
  \time 4/4
  R1*2

  R1*7
  R1 \fermataMarkup
  R1
  R1
	r2 af16 \f g fs g g'8 -- -> f8 -- ->

  \repeat volta 2 {
    ef4 -- -^ ef -- -^ ef8. -- -^ ef16 -^ r ef -> ~ ef8 
    f4 -- -^ f -- -^ af,16 g fs g g'8 -- -> f8 -- ->
    ef4 -- -^ ef -- -^ ef8. -- -^ ef16 -^ r ef -> ~ ef8 
  }
  \alternative {
    { f4 -^ f -^ af,16 g fs g g'8 -- -> f8 -- -> }
    { f4 -^ f -^ af,16 g fs g g'8 f16 ef -. }
  }

  \repeat volta 2 {
    r4 ef -^ \f ef8. -^ ef16 -^ r ef -> ~ ef8 
    ef4 -^ ef -^ af,16 g fs g g'8 f16 ef -.
    r4 ef -^ ef8. -^ ef16 -^ r ef -> ~ ef8 
  }
  \alternative {
    { ef4 -^ ef -^ af,16 g fs g g'8 f16 ef -. }
    { ef4 -^ ef -^ af,16 g fs g g'8 -. f16 ef -> ~ }
  }

  ef4 c -. ef8 -. c16 ef -. r16 g f8 -> ~
  f8 d -. b4 -. b8 -. d16 f -. r16 af g8 -> ~
  g8 ef -. c4 -. ef8 -. c16 ef -. r16 g f8 -> ~
  f8 d -. b4 -. b8 -. d16 f -. r16 af ef'8 -> ~

  ef8 c -. g4 -. c8 -. g16 c -. r16 ef d8 -> ~
  d8 b -. g4 -. g8 -. b16 d -. r16 f ef8 -> ~
  ef8 c -. g4 -. c8 -. g16 c -. r16 ef d8 -> ~
  d8 [ b -. ] g8 -. r16 d f ef d ef f8 -> g16 c, -.
}

% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\book {
	\header { instrument = "Tenor Sax" }
  \bookOutputName "Sueños - Tenor"

	\score {
		<<
			\scoreMarkup
			\new StaffGroup <<
        \scoreChords
        \transpose bf c \scoreChords
				\new Staff { \transpose bf c' \tenorSax }
			>>
		>>
	}
}

\book {
  \bookOutputName "Sueños"

	\paper {
		% Descomentar para score
		% #(set-paper-size "a4" 'landscape)
		% indent = 20
		% systems-per-page = 2
	}

	\score {
		% Descomentar para midi
		% \midi { \tempo 4 = 110 }

		% NO USAR layout-set-staff-size O SE ROMPE lilyjazz!!!
		\layout { }

		<<
			\scoreMarkup
			\new StaffGroup <<
        \transpose ef c \scoreChords
				\new Staff \with { instrumentName = "Alto Sax" midiInstrument = #"" } {
          \transpose ef c' \altoSax
        }
        \transpose bf c \scoreChords
				\new Staff \with { instrumentName = "Tenor Sax" midiInstrument = #"" } {
          \transpose bf c' \tenorSax
        }
			>>
		>>
	}
}



