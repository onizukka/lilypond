\version "2.18.2"
\language "english"

% Se debe incluir antes que lilyjazz. Si no, lilyjazz no funciona.
#(set-global-staff-size 19)

% includes
\include "main.ily"

% Cabecera
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\header {
	title = "Ya verás"
	instrument = "Alto Sax & Tenor Sax"
	composer = "Sonífera Isla"
	arranger = ""
	poet = ""
	tagline = "Edition: gbpsms@gmail.com"
}

% Márgenes, fuentes y distancias del papel
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\paper {
}



% Función musical global
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
global = {
	% Para pintar todos los números de compás debajo del pentagrama
	% \override Score.BarNumber #'break-visibility = #end-of-line-invisible
	% \override Score.BarNumber #'direction = #DOWN
	% \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

	\compressFullBarRests

	% Arregla el problema de tener el scoreMarkup añadido
	\revert MultiMeasureRest #'staff-position
	\override Voice.Rest #'staff-position = #0

	\set Timing.beamExceptions = #'()
	\set Timing.baseMoment = #(ly:make-moment 1/4)
	\set Timing.beatStructure = #'(1 1 1 1)

	\override ParenthesesItem.font-size = #0
}

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
	% Oculta el primer pentagrama del markup
	\override Staff.VerticalAxisGroup #'remove-first = ##t

	% Indicación de tiempo
	% \once \override Score.RehearsalMark.self-alignment-X = #LEFT
	% \once \override Score.RehearsalMark.extra-offset = #'(0 . 3)
	% \mark \jazzTempoMarkup "Easy Swing" c4 "112"

	% s1*2
	% \once \override Score.RehearsalMark.X-offset = #5
	% \mark \markup \boxed "A"

  s1*2

  \break
  s1*2
  s1*4

  \break
  \bar "[|:"
  s1*4
  \break
  s1*5
  s1*2
  \break
  s1*4
  s1*3
  s1*6/4
  \break
  s1*2
  \bar ":|]"
  s1*2
  s1*6/4
  s1

  \break
  s1*2
  \break
  s1*2
  s1*8

  \break
  \mkup "(Keyboard/Guitar solo 1st x, Sax/Guitar solo 2nd x)"
  \bar "[|:"
  s1*4
  \break
  s1*4
  \break
  s1*8
  \bar ":|]"

  \break
  s1*3
  \break
  s1*1
  s1*4

  \break
  s1*5
  s1*2

  \break
  s1*2
  s1*2
  s1*3
  s1*6/4
  s1*2

  \break
  s1*4
  s1*14/16

  \break
  s1*2*7/8
  s1*2
  \bar "|."
}

% Cifrado
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
scoreChords = \chords {
	% c1:m f2:m g2:m7
  s1*2

  s1*2
  s1*4

  s1*4
  s1*5
  s1*2
  s1*4
  s1*3
  s1*6/4
  s1*2
  s1*2
  s1*6/4
  s1

  s1*2
  s1*2
  s1*8

  d1:m
  g2:m a2:m
  d1:m
  g2:m a2:m
  d1:m
  g2:m a2:m
  d1:m
  g2:m a2:m
}

% Música
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
altoSax = \relative c'' {

	\global
	\key c \minor

  c16 \f c c c -. r8 c16 c c c -. r8 r4
  c16 c c c -. r8 c16 c c c -. r8 r4
  c16 c c c -. r8 c16 c c c -. r8 r4
  d16 d d d -. r8 d16 d d d -. r8 r4

  R1*4

  \repeat volta 2 {
    r2 gf,8. -> \f f16 -> ~ f8 gf -^
    r2 gf8. -> f16 -> ~ f8 ef -^
    r2 bf'8. -> a16 -> ~ a8 bf -^
    r2 bf8. -> a16 -> ~ a8 g16 g -^

    R1*5

    r4 bf,16 ( \mf c ) -. r8 r2
    r4 bf16 ( c ) -. bf ( c ) -. r2
    c2 -> bf ->
    c8. -> \< cs16 -> ~ cs8 d8 -> \f \bendAfter #-4 r2
    R1*2

    R1*3
    \time 6/4
    R1*6/4
    
  }
  \alternative {
    { \time 4/4
      R1*2 }
    { R1*2
      \time 6/4
      R1*6/4
      \time 4/4
      R1 }
  }

  c'16 \f c c c -. r8 c16 c c c -. r8 r4
  c16 c c c -. r8 c16 c c c -. r8 r4
  c16 c c c -. r8 c16 c c c -. r8 r4
  d16 d d d -. r8 d16 d d d -. r8 r4

  R1*8

  \repeat volta 2 {
    \impro 32
    R1*8
  }

  c16 \f c c c -. r8 c16 c c c -. r8 r4
  c16 c c c -. r8 c16 c c c -. r8 r4
  c16 c c c -. r8 c16 c c c -. r8 r4
  d16 d d d -. r8 d16 d d d -. r8 r4

  ef1 -> \mf \< ~
  ef
  f
  g -> \sfp \<

  r4 \! g -> \bendAfter #-5 r2
  R1*4

  r4 bf,,16 ( \mf c ) -. r8 r2
  r4 bf16 ( c ) -. bf ( c ) -. r2
  c2 -> bf ->
  c8. -> \< cs16 -> ~ cs8 d8 -> \f \bendAfter #-4 r2
  R1*2

  R1*3
  \time 6/4
  R1*6/4
  \time 4/4
  R1*2

  R1*4

  \time 7/8
  c8 -. [ c -. ] bf'16 [ -> c gf8 -. ] f8. [ -> e16 -> ] ~ e8 
  ef8 -. [ ef -. ] df'16 [ -> ef bf8 -. ] a8. [ -> af16 -> ] ~ af8 
  g8 -. [ g -. ] f'16 [ -> g ef8 -. ] d8. [ -> df16 -> ] ~ df8 

  \time 4/4
  c4 -. r4 r2 \fermata
  ef1 \fermata
}

tenorSax = \relative c'' {

	\global
	\key c \minor

  c16 \f c c c -. r8 c16 c c c -. r8 r4
  c16 c c c -. r8 c16 c c c -. r8 r4
  c16 c c c -. r8 c16 c c c -. r8 r4
  g'16 g g g -. r8 g16 g g g -. r8 r4

  R1*4

  \repeat volta 2 {
    r2 ef8. -> \f d16 -> ~ d8 ef -^
    r2 ef8. -> d16 -> ~ d8 c -^
    r2 gf'8. -> f16 -> ~ f8 gf -^
    r2 gf8. -> f16 -> ~ f8 d16 ef -^

    R1*5

    r4 d,16 ( \mf e ) -. r8 r2
    r4 d16 ( e ) -. d ( e ) -. r2
    a2 -> g ->
    a8. -> \< as16 -> ~ as8 b8 -> \f \bendAfter #-4 r2
    R1*2

    R1*3
    \time 6/4
    R1*6/4
    
  }
  \alternative {
    { \time 4/4
      R1*2 }
    { R1*2
      \time 6/4
      R1*6/4
      \time 4/4
      R1 }
  }

  c16 \f c c c -. r8 c16 c c c -. r8 r4
  c16 c c c -. r8 c16 c c c -. r8 r4
  c16 c c c -. r8 c16 c c c -. r8 r4
  g'16 g g g -. r8 g16 g g g -. r8 r4

  R1*8

  \repeat volta 2 {
    \impro 32
    R1*8
  }

  c,16 \f c c c -. r8 c16 c c c -. r8 r4
  c16 c c c -. r8 c16 c c c -. r8 r4
  c16 c c c -. r8 c16 c c c -. r8 r4
  g'16 g g g -. r8 g16 g g g -. r8 r4

  c1 -> \mf \<
  bf
  a
  g -> \sfp \<

  r4 \! c -> \bendAfter #-5 r2
  R1*4

  r4 d,,16 ( \mf e ) -. r8 r2
  r4 d16 ( e ) -. d ( e ) -. r2
  a2 -> g ->
  a8. -> \< as16 -> ~ as8 b8 -> \f \bendAfter #-4 r2
  R1*2

  R1*3
  \time 6/4
  R1*6/4
  \time 4/4
  R1*2

  R1*4

  \time 7/8
  c,8 -. [ c -. ] bf'16 [ -> c gf8 -. ] f8. [ -> e16 -> ] ~ e8 
  ef8 -. [ ef -. ] df'16 [ -> ef bf8 -. ] a8. [ -> af16 -> ] ~ af8 
  g8 -. [ g -. ] f'16 [ -> g ef8 -. ] d8. [ -> df16 -> ] ~ df8 

  \time 4/4
  c4 -. r4 r2 \fermata
  bf'1 \fermata
}

% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\book {
	\header { instrument = "Tenor Sax" }
  \bookOutputName "Ya verás - Tenor"
	\score {
		<<
			\scoreMarkup
			\new StaffGroup <<
        \scoreChords
        \transpose bf c \scoreChords
				\new Staff { \transpose bf c' \tenorSax }
			>>
		>>
	}
}

\book {
  \bookOutputName "Ya verás"

	\paper {
		% Descomentar para score
		% #(set-paper-size "a4" 'landscape)
		% indent = 20
		% systems-per-page = 2
	}

	\score {
		% Descomentar para midi
		% \midi { \tempo 4 = 110 }

		% NO USAR layout-set-staff-size O SE ROMPE lilyjazz!!!
		\layout { }

		<<
			\scoreMarkup
			\new StaffGroup <<
        \transpose ef c\scoreChords
				\new Staff \with { instrumentName = "Alto Sax" midiInstrument = #"" } {
          \transpose ef c' \altoSax
        }
        \transpose bf c\scoreChords
				\new Staff \with { instrumentName = "Tenor Sax" midiInstrument = #"" } {
          \transpose bf c' \tenorSax
        }
			>>
		>>
	}
}



