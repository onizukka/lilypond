﻿\version "2.16.1"


% Instrument Music
% -------------------------------------------------
tenorOne = \relative c'' {

	\compressFullBarRests
	\override Voice.Rest #'staff-position = #0
	
	\key a \minor
	
	\time 4/4
	\transpose bes ees' \relative c'' {
		e1 ~ e2. ees4
		e?1 ~ e2. ees4
		g,1 ~ g1
		
		r2 r4 e'4
		r2 r4 g
		r2 r4 b ~
		b2 a8 g fis4 ~ 
		fis r4 r2
	}
	
	\time 2/4
	r2
	
	\time 12/8
	\transpose bes ees' \relative c'' {
		r4\mf g8-- g-- r r r4 g8-- g-- r r
		r4 g8-- g-- r r} r r e, g e g
	a-. r a ~ a c d ees d ees d ees d
	ees c ees \times 2/3 {d16[\< ees e] fis[ gis bes] b[ cis d]} e8\! r r r4.
	\bar "||"
	\break
	
	R1*12/8*6
	r2. a,2.\p
	g2. a2. ~
	a2. gis8-> r r r4.
	\bar "||"
	\break
	
	\transpose bes ees' \relative c'' {
		r4.\mf r8 g a bes a bes a bes a
		g e d e4 e8 ~ e4. r4.
	}
	\bar "||"
	\break
	
	\transpose bes ees' \relative c'' {
		b1.\p 
		d2. ~ d4. c4.
		fis4. e2. ~ e4.
		d2. ~ d4. c 
	}
	e2. d4. c4.
	e2. fis2.
	g2.\< a2. 
	c2.\1 g8\<( g g g-.) g-. g-.
	
	\transpose bes ees' \relative c'' {
		r4\mf g8-- g-- r r r4 g8-- g-- r r
		r4 g8-- g-- r r} r r e, g e g
	a-. r a ~ a c d ees d ees d ees d
	ees c ees \times 2/3 {d16[\< ees e] fis[ gis bes] b[ cis d]} e8\! r r r4.
	\bar "||"
	\break
	
	\key bes \minor
	R1*12/8*8
	\bar "||"
	
	\key b \minor
	R1*12/8
	r2. r8 r8 a,8-> ~ a4.
	fis2. r2. 
	r2. r8 r8 fis d e fis 
	
	b4. fis r fis
	fis1.
	
	r8\mf r d\< d d\!\f r\mf r r d\< d d\!\f r
	r8\mf r d\< d d\!\f r\mf r b b\< b b\!\f r
	
	r8 b b d d r r b b d d r
	r8 cis cis e e r r g g g g g
	
	r4. b8 b r r4. b8 b r
	r4. b8 b r r4. e,8\< d e
	e\!\f( e e e-.) e-. e-. fis-> r r r4.
	
	R1*12/8*5
	\transpose bes ees' \relative c'' {
		gis1. a1. 
		r4. e4. ~ e2. ~ 
		e1. ~ e8 r cis ~ cis4. ~ cis2.
	}
	\bar "||."	
}


% Instrument Sheet
% -------------------------------------------------
\include "imports/main.ly"

\header {
	 instrument = "Tenor 1"
}

{ << \tenorOne \\ \scoreMarkup >> }
