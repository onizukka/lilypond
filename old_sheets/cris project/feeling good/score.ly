\version "2.16.1"

\include "imports/main.ly"

% \include "trumpet.ly"
% \include "tenor.ly"
% \include "trombone.ly"

%{
\score {
	\new StaffGroup <<
		\new Staff <<
			\scoreMarkup \\
			\transpose bes c \relative c' \trumpet
		>>
		\new Staff { 
			\transpose bes c \relative c' \tenor 
		}
		\new Staff { 
			\relative c' \trombone 
		}
	>>
}
%}