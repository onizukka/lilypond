\version "2.12.3"

\header {
	title = "O Joyfull Children"
	composer = "Composer: Lee Holdrige"
   arranger = "Arranger: Gabriel Palacios"
}

voice = \relative c''
{
   \time 3/4
   
   \partial 4*1 r4
   R2.*8 r4 r g |
   e'2 d4 | f e d | c b a | g c r8 e, | f4 d a' | g4 e4. b'8 | a4 b c | d2. ~ | d2 g,4 
   e'2 d4 | f e d | c b a | g c r8 e, | f4 d4. a'8 | g4 e4. b'8 | a4 d b | c2. ~ | c2 r4 
   
   a b c | b g r8 g | fis4 g a | b2 g4 | e2. | fis4 g4. a8 | b2. | a4 b cis | b2 gis4 | e'2. ~ | e2.
   a,4 b c | b g r8 g | fis4 g a | b2 g4 | e2. | fis4 g4. a8 | b2. | a4 b cis | d cis a | b gis e' | cis2. ~ | cis2.
   d4 d e | cis2. | d | e ~ | e2 a,4 
   
   fis'2 e4 | g fis e | d cis b | a d4. fis,8 | g4 e b' | a fis4. cis'8 | b4 cis d | e2. ~ | e2 a, 
   fis'2 e4 | g fis e | d cis b | a d4. fis,8 | g4 e4. b'8 | a fis4. cis'8 | b8 a g4 b8 d | cis8 b a4. e'8 
   d4 b e | cis ais4. ais8 | d2 b4 | e2. ~ | e | e4 d cis | d2. ~ | d2. ~ | d2. ~ | d2.\fermata 
   
   \bar "||"
}

rightHand = \relative c''
{
   \time 3/4
   
   \partial 4*1 g4 |
   e'4. f16 e d c d e | f4 e d | c b8 c a4 | g c f16 e d e | 
   f4 d4. a'8  | g8 f e d c b | a4 b c  d2. | g,2\fermata r4
   
   <<      
      \partcombine
      {  r8 g c g' c4         | r8 g, d' f g4      | r4 a,8 c f c'         | r4 g,8 c e c' 
         r4 d,,8 a' d f       | r8 e, b' e g b     | a4 <g b> <a c>        | d8 e f e d c          | b c d4 r4 
         r8 g,, c g' c4       | r8 g, d' f g4      | r4 a,8 c f c'         | r4 g,8 c e c' 
         r4 d,,8 a' d f       | r8 e, b' e g b     | a4 d b                | c2. ~ | c2. }
      \\
      {  r8 e,, g e' g4       | r8 b,, g' d' b4    | r4 c,8 f a f'         | r4 c,8 e g e'
         r4 f,,8 d' f a       | r8 g, e' g b e     | <a, c f>2.\arpeggio   | <f' g b>2.\arpeggio   | <d f g>\arpeggio
         r8 e, g e' g4        | r8 b,, g' d' b4    | r4 c,8 f a f'         | r4 c,8 e g e'
         r4 f,,8 d' f a       | r8 g, e' g b e     | <a, d f>2 <d f g>4    | r8 e, g e g e         | c e g e g e }
      \\
      {  <e g e'>2.\arpeggio  | <d g d'>\arpeggio  | <c a' c>\arpeggio     | <g c g'>\arpeggio  
         R2.                  | R                  | R                     | R                     | R   
         <e' g e'>2.\arpeggio | <d g d'>\arpeggio  | <c a' c>\arpeggio     | <g c g'>\arpeggio  
         R2.                  | R                  | R                     | R                     | R  }
   >>
   
   R2.*9
   R2.*9
   
   R2.*11
   R2.*12
   R2.*5
   
   R2.*9
   R2.*8
   R2.*10
   
   \bar "||"
}

leftHand = \relative c
{
   \time 3/4
   \clef bass
   
   \partial 4*1 r4
   <<
      {  <c g' e'>2.    | <b g' d'>       | <a f' c'>       | <g c e>         |
         <d a' f'>      | <e b' g'>       | <f c' a'>       | <g d' b'> }
      \\
      {  r4 e''8 c g c, | r4 d'8 g, d b   | r4 c'8 f, c a   | r4 e'8 c g c,   |
         r8 f a d f4    | r8 g, b e g4    | r8 a, c f a4    | r4 g8 b d f}
   >>
   <g,, d' g b d f>2\arpeggio\fermata r4 
   
   R2.*9
   R2.*9
   
   R2.*11
   R2.*12
   R2.*5
   
   R2.*9
   R2.*8
   R2.*10
   
   \bar "||"
}

\score {
   <<
      \new Staff { \voice }
      \new PianoStaff
      <<
         \override PianoStaff.NoteCollision #'merge-differently-headed = ##t
         \set PianoStaff.connectArpeggios = ##t
         
         \new Staff { \rightHand }
         \new Staff { \leftHand }
      >>
   >>
}