﻿\version "2.16.1"

showClefKey = {
  \once \override Staff.KeySignature #'break-visibility = #all-visible
  \once \override Staff.Clef #'break-visibility = #all-visible
}

\paper { 
	indent = 0\cm
	top-markup-spacing #'minimum-distance = #10
	markup-system-spacing #'minimum-distance = #5
}

\markup {
	\column {
		\fill-line {
			\line {
				\fontsize #2 {
					Escala de referencia para los solos de Peter Gunn
				}
			}
		}
	}
}
\score {
	\new Staff \with {
		\remove "Time_signature_engraver"
	}
	{	
		\transpose ees c \relative c' {
			\override Stem #'transparent = ##t
			
			\cadenzaOn
			s2 f1 ges aes a b des ees f
		}
	}
	\layout {
		#(layout-set-staff-size 20)
		\context {
			\Score
			\override SpacingSpanner
			#'base-shortest-duration = #(ly:make-moment 1 516)
		}
	}
}

\markuplist {
	\vspace #1
	\wordwrap-lines \fontsize #2 {
		Aunque no soy muy bueno improvisando, me permito el lujo de detallar ciertos conceptos que ayudan a entender las bases de una buena improvisación.
	}
	\vspace #2
	\wordwrap-lines \fontsize #2 {
		La mejor forma de entender la música es comparándola con el lenguaje. En nuestro caso, una improvisación es el equivalente a un monólogo. Para que un monólogo resulte interesante, hay que 
		\italic {aportar una idea y desarrollarla.}
	}
	\wordwrap-lines \fontsize #2 {
		Por ejemplo:
	}
}
\score {
	\new Staff {
		\transpose ees c \relative c'' {
			f4 c8 ees r4 r8 f ~
			f4 c8 ees r8 f r8 g ~
			g d f ees b d r8 c
			\times 2/3 {bes4 f aes ~}
			aes4. bes
		}
	}
	\layout {
		#(layout-set-staff-size 20)
		\context {
			\Score
			\override SpacingSpanner
			#'base-shortest-duration = #(ly:make-moment 1 24)
		}
	}
}