\version "2.14.2.1"

\header {
	title = "Blues Brothers MegaMix Vol. 2"
   arranger = "Arranged by Dave Tanner"
   instrument = "3rd Trumpet (Eb)"
}

#(define-markup-command (underlinedTx layout props text) (markup?)
   (interpret-markup layout props
      (markup #:normalsize #:italic #:override '(offset . 5) #:underline #:smallCaps text)
   )
)

#(define-markup-command (defaultTx layout props text) (markup?)
  (interpret-markup layout props
      (markup #:normalsize #:italic #:smallCaps text)
   )
)

#(define-markup-command (index layout props text) (markup?)
  (interpret-markup layout props
      (markup #:huge #:override '(circle-padding . 0.5) #:circle text)
   )
)

hide = #(define-music-function (parser location note) (ly:music?)
#{
   \hideNotes $note \unHideNotes
#})
   
#(define (make-bend x) (make-music 'BendAfterEvent 'delta-step x))

bend = #(
   define-music-function 
   (parser location delta) 
   (integer?)
   (make-bend (* -1 delta))
)

gl = #(
   define-music-function 
   (parser location rotation offset length) 
   (list? pair? number?)
   #{
      \once \override BendAfter #'rotation = #$rotation
      \once \override BendAfter #'extra-offset = #$offset
      \once \override BendAfter #'minimum-length = #$length
      \once \override BendAfter #'thickness = #2.5
      \bend #0
   #} 
)

voice = \relative c'
{
   \compressFullBarRests
   
   \time 4/4
   \key g \major

   \once \override DynamicText #'extra-offset = #'(6 . 0)
   R1*6\mf
   \override TupletBracket #'extra-offset = #'(0 . 1.2)
   \override TupletNumber #'extra-offset = #'(0 . 1.2)
   \bar "||"
   << { f2. r8 d^> ~ }
      { s4\> s2\!\< s8\! s8 } >> |
   \gl #'(-30 -1 0) #'(0 . 0) #2.5 d4 r4 r2 |
   << { f1 }
      { s4\> s4 s4\!\< s4 } >> |
   \gl #'(35 1 0) #'(-6.3 . -0.4) #4 d'8\!->( 
   \gl #'(-30 -1 0) #'(0 . 0) #2.5 aes4.) r2 |
   \break
   
   
   r8 d, f g \times 2/3 {
      \gl #'(15 1 0) #'(-4.2 . -0.8) #0 aes4^> 
      \gl #'(15 1 0) #'(-4.2 . -0.8) #0 aes^> 
      \gl #'(15 1 0) #'(-4.2 . -0.8) #0 aes^>} |
   \times 2/3 {
      \gl #'(15 1 0) #'(-4.2 . -0.8) #0 aes4^> 
      \gl #'(15 1 0) #'(-4.2 . -0.8) #0 g^>
      \gl #'(15 1 0) #'(-4.7 . -1.3) #0 f^>} 
   \times 2/3 {
      \gl #'(15 1 0) #'(-4.2 . -0.8) #0 d^> 
      \gl #'(15 1 0) #'(-4.2 . -0.8) #0 c^> 
      \gl #'(15 1 0) #'(-4.2 . -0.8) #0 d^>} |
   ais8( b4.^>) r2 |
   R1 |
   \break
   
   
   << { f'2. r8 d^> ~ }
      { s4\> s2\!\< s8\! s8 } >> |
   \gl #'(-30 -1 0) #'(0 . 0) #2.5 d4 r4 r2 |
   << { f1 }
      { s4\> s4 s4\!\< s4 } >>
   \gl #'(35 1 0) #'(-6.3 . -0.4) #4 d'8\!->( 
   \gl #'(-30 -1 0) #'(0 . 0) #2.5 aes4.) r2 |
   \break
   
   
   r8 d, f g \times 2/3 {
      \gl #'(15 1 0) #'(-4.2 . -0.8) #0 aes4^> 
      \gl #'(15 1 0) #'(-4.2 . -0.8) #0 aes^> 
      \gl #'(15 1 0) #'(-4.2 . -0.8) #0 aes^>} |
   \times 2/3 {
      \gl #'(15 1 0) #'(-4.2 . -0.8) #0 aes4^> 
      \gl #'(15 1 0) #'(-4.2 . -0.8) #0 g^>
      \gl #'(15 1 0) #'(-4.7 . -1.3) #0 f^>} 
   \times 2/3 {
      \gl #'(15 1 0) #'(-4.2 . -0.8) #0 d^> 
      \gl #'(15 1 0) #'(-4.2 . -0.8) #0 c^> 
      \gl #'(15 1 0) #'(-4.2 . -0.8) #0 d^>} |
   ais8( b4.^>) r2 |
   << { g'1^> }
      { s4\> s2.\!\< }
   >>      
   \revert TupletBracket #'extra-offset
   \revert TupletNumber #'extra-offset
   \break
   
   
   \bar "|:"
   a2.\!\mf\< r8\! a-> ~ |
   \gl #'(-35 -1 0) #'(0 . 0) #0 a4 r4 r2 |
   gis8-> a-. r gis-> a4-. a4-. |
   R1 |
   \break
   
   
   a2.\!\mf\< r8\! a-> ~ |
   \gl #'(-35 -1 0) #'(0 . 0) #0 a4 r4 r2 |
   ais8-> b-. r ais-> b4-. b4-. |
   R1 |
   \break
   
   
   \bar ":|:"
   R1*8
   \bar ":|:"
   R1*8
   \bar ":|"
   \break
   
   
   \repeat percent 6 
   {
      gis8--\mf a-^ r gis-- a4-^ r8 gis'->\< ~ |
      gis4 ~ gis8\! g'8->\f ~ g4 r4 |
   }
   gis,,8--\mf a-^ r gis-- a4-^ r8 g->\< ~ |
   g4 ~ \stemDown g8\! g''8->\f \stemNeutral \times 2/3 {r4 b,,->\mf b->} 
   \break
   
   
   \repeat volta 2
   {
      << { b2. r8 a-> ~ }
         { s4\> s2\!\< s8\! s8 } >> |
      \gl #'(-30 -1 0) #'(0 . 0) #2.5 a4 r r2 |
      << { b1 }
         { s4\> s4 s4\!\< s4 } >>
      \gl #'(35 1 0) #'(-6.3 . -0.4) #4 d'8->\! 
      \gl #'(-30 -1 0) #'(0 . 0) #2.5 aes4.-> r2 |
      \break
      
      
      r8 a, b cis 
      \times 2/3 {
         \gl #'(15 1 0) #'(-4.2 . -0.8) #0 d4-> 
         \gl #'(15 1 0) #'(-4.2 . -0.8) #0 d-> 
         \gl #'(15 1 0) #'(-4.2 . -0.8) #0 d-> } |
      \times 2/3 {
         \gl #'(15 1 0) #'(-4.2 . -0.8) #0 d4-> 
         \gl #'(15 1 0) #'(-4.2 . -0.8) #0 cis-> 
         \gl #'(15 1 0) #'(-4.2 . -0.8) #0 b-> }  
         
      \override TupletBracket #'extra-offset = #'(0 . 1.2)
      \override TupletNumber #'extra-offset = #'(0 . 1.2)
      \times 2/3 {
         \gl #'(15 1 0) #'(-4.2 . -0.8) #0 a^> 
         \gl #'(15 1 0) #'(-4.2 . -0.8) #0 g^> 
         \gl #'(15 1 0) #'(-4.2 . -0.8) #0 a^>} |
      \revert TupletBracket #'extra-offset
      \revert TupletNumber #'extra-offset
   }
   \alternative 
   {
      {
         e8( f4.^>) r2 | r2 \times 2/3 {
            r4 
            \gl #'(15 1 0) #'(-4.2 . -0.8) #0 b-> 
            \gl #'(15 1 0) #'(-4.2 . -0.8) #0 b->}
         \break
      }
      {e,8( f4.^>) r4 r8 g-. }
   }
   
   e8( f4.^>) r4 r8 g'-. |
   cis,( d4.->) r4 r8 g |
   cis,( d4.->) r8 fis,16-> g-^ r b-> d-^ r |
   
   bes4->\f bes-> bes-> bes-> |
   bes-> bes-> bes-> bes8->( cis->) ~ |
   cis1 ~ |
   cis
}

\paper {
   markup-system-spacing #'basic-distance = #20
}

\score {
   \new Staff { \voice }
}