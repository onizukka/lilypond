\version "2.18.0"
\include "imports/main.ly"

% Instrument Music
% -------------------------------------------------
tromboneFour = {

  \override Score.BarNumber #'break-visibility = #end-of-line-invisible
  \override Score.BarNumber #'direction = #DOWN
  \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0
  
  % \clef bass
  \key bes \major
  \tempo "Bright Swing" 4 = 176
  
  f1 ~
  f2 r8 f8 f[ f] ~ 
  f1 ~
  f4. f8-. r2
  
  \break
  r8 \f f'4.-> des4-^ r8 ges8-> ~
  ges4 ges-^ r8 f4.->
  bes,4-^ r4 r2
  r1
  
  \break
  \repeat volta 2 {
    R1*7
    r2 r4^\markup "Soli" \mf \scoop #'(-0.2 . 0.7) ees32 a8 aes ~
    
    \break
    aes1
    f4. f8 ~ f4 f-. 
    g2 \bendAfter #-4 r2
    R1
    
    R1*2
  }
  
  \alternative {
    { \break
      r8 \f bes,4.-> des4.-> c8-> ~ 
      c4 ges8-- f-> r2 }
    { r4 \f bes-^ aes8---> aes4-. bes8->
      r1 }
  }
  
  \break
  r2 c2( \p
  f,1
  d)
  r8 g4. \< g4-. \! r8 c-> \mf
  
  R1*2
  \break
  d4-^ \f r r8 g,4.->
  c4-^ r r8 ces4-^ bes8-> ~
  
  bes2 \bendAfter #-3 r
  R1*2
  \break
  r4 r8 d-> r4 r8 g,->
  
  R1*2
  r4 r8  g-> ~ g4. c8->
  r4 f,2->( ces'4-^)
  
  \break
  \repeat volta 2 {
    bes2-> ~ bes8 f' g des ~ 
    des4. ges,8 ~ ges2
    r4 c-^ c8-- c4-. ees8-> ~ 
    ees4 \bendAfter #-5 r r2
    
    \break
    r8 bes'4.-> a4-. c8-- ces->
    r4 r8 bes-> r4 r8 bes->
    r bes-> r4 r2
    r8 ees,4.-> ees4-^ r8 aes-> ~
    
    \break
    aes2 \bendAfter #-5 r
    R1*3
    R1*2
  }
  \alternative {
    { r2 r8 \f b4.->
      bes4-^ r8 aes->( ~ aes4 aes4-^) }
    { \break
      r4 \f bes,-^ aes8---> a4-. bes8->
      r1 }
  }
  
  r2 c2( \p
  f,1
  d)
  \break
  r8 g4. \< g4-. \! r8 c-> \mf
  
  R1*2
  d4-^\f r r8 g,4.->
  c4-^ r4 r8 ces4-^ bes8-> ~
  
  \break
  bes2 \bendAfter #-5 r
  R1*2
  r4 r8 d-> r4 r8 g,->
  
  r1
  \break
  r2 r8 ges'4.-> 
  f4-^ r r2
  r2 r8 f, f[ f] ~
  f2 f
  f f4-. r4
  
  \break
  r8 f'4.-> des4-^ r8 ges-> ~
  ges4 ges-^ r8 f4.-> 
  bes,4-^ r r g'4-> ~
  g1 \fermata
}


% Instrument Sheet
% -------------------------------------------------

\header {
  instrument = "Bass Trombone (4th) (Eb)"
}

\paper {  	
  % distancia entre sistemas
  system-system-spacing #'basic-distance = #18
  
  % distancia entre el título y el primer sistema
  markup-system-spacing #'basic-distance = #20
}

<<
  \scoreMarkup
  \new Staff << \transpose ees c'' \relative c, \tromboneFour >>
>>