\version "2.18.0"
\include "imports/main.ly"

% Instrument Music
% -------------------------------------------------
tromboneThree = {

  \override Score.BarNumber #'break-visibility = #end-of-line-invisible
  \override Score.BarNumber #'direction = #DOWN
  \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0
  
  % \clef bass
  \key bes \major
  \tempo "Bright Swing" 4 = 176
  
  r8 bes4. b4-. b-. 
  bes?4-- bes8-- a-. r2
  r8 a( bes4-.) bes-. bes-.
  \once \override Script.extra-offset = #'(2.5 . 0.7)
  bes \turn gis8-- a-. r2
  
  \break
  r8 \f c4.-> ces4-^ r8 bes8-> ~
  bes4 bes-^ r8 a4.->
  f4-^ r4 r2
  r1
  
  \break
  \repeat volta 2 {
    R1*7
    r2 r4^\markup "Soli" \mf \scoop #'(-0.2 . -0.2) bes32 des8 c ~
    
    \break
    c1
    aes4. aes8 ~aes4 aes-. 
    bes2 \bendAfter #-4 r2
    R1
    
    R1*2
  }
  
  \alternative {
    { \break
      r8 \f f4.-> aes4.-> g8-> ~ 
      g4 e8-- ees-> r2 }
    { r4 \f g-^ ges8---> ges4-. g8->
      r1 }
  }
  
  \break
  r2 g2( \p
  ees1
  f)
  r8 f4. \< f4-. r8 \mf g->
  
  R1*2
  \break
  a4-^ \f r r8 f4.->
  g4-^ r r8 a4-^ g8-> ~
  
  g2 \bendAfter #-3 r
  R1*2
  \break
  r4 r8 c-> r4 r8 bes->
  
  R1*2
  r4 r8  f-> ~ f4. g8->
  r4 ees2->( a4-^)
  
  \break
  \repeat volta 2 {
    g2-> ~ g8 a g ces ~ 
    ces4. bes8 ~ bes2
    r4 bes-^bes8-- bes4-. a8-> ~ 
    a4 \bendAfter #-5 r r2
    
    \break
    r8 d4.-> c4-. d8-- des->
    r4 r8 des-> r4 r8 d->
    r d-> r4 r2
    r8 d4.-> des4-^r8 d-> ~
    
    \break
    d2 \bendAfter #-5 r
    R1*3
    R1*2
  }
  \alternative {
    { r2 r8 \f ees4.->
      ees4-^ r8 ees->( ~ ees4 des4-^) }
    { \break
      r4 \f g,-^ ges8---> ges4-. g8->
      r1 }
  }
  
  r2 g2( \p
  ees1
  f)
  \break
  r8 f4. \< f4-. \! r8 g-> \mf
  
  R1*2
  a4-^\f r r8 f4.->
  g4-^ r4 r8 a4-^ g8-> ~
  
  \break
  g2 \bendAfter #-5 r
  R1*2
  r4 r8 c-> r4 r8 bes->
  
  r1
  \break
  r2 r8 a4.-> 
  a4-^ r r2
  r2 r8 f, f[ f] ~
  f2 f
  f ees'4-. r4
  
  \break
  r8 c'4.-> ces4-^ r8 bes-> ~
  bes4 bes-^r8 a4.-> 
  f4-^ r r des'4-> ~
  des1 \fermata
}


% Instrument Sheet
% -------------------------------------------------

\header {
  instrument = "3rd Trombone (Eb)"
}

\paper {  	
  % distancia entre sistemas
  system-system-spacing #'basic-distance = #18
  
  % distancia entre el título y el primer sistema
  markup-system-spacing #'basic-distance = #20
}

<<
  \scoreMarkup
  \new Staff << \transpose ees c'' \relative c' \tromboneThree >>
>>