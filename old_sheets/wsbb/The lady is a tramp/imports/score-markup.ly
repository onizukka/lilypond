scoreMarkup = \new Staff \with {
   \RemoveEmptyStaves
}
{
  % Oculta el primer pentagrama del markup
  \override Staff.VerticalAxisGroup #'remove-first = ##t
  
  s1*8
  \mark \markup \boxed "9"
  \bar "||"
  
  s1*18 
  \mark \markup \boxed "27"
  \bar "||"
  
  s1*8
  \mark \markup \boxed "35"
  \bar "||"
  
  s1*8
  \mark \markup \boxed "43"
  \bar "||"
  
  s1*18
  \mark \markup \boxed "61"
  \bar "||"
  
  s1*8
  \mark \markup \boxed "69"
  \bar "||"
  
  s1*14
  \bar "|."
}