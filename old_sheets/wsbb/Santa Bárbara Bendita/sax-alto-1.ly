firstAlto = \new Staff
{
	\set Staff.midiInstrument = #"alto sax"
	\set Staff.instrumentName = "1st Sax. Alto"
	\set Score.skipBars = ##t
	
	\transpose ees c {
	\relative c''
	{
	
		\key bes \minor
		
		\time 1/4
		r4

		\bar "|:"
		\time 3/4
		des2. ~
		\time 4/4
		des2. ~ des8 r8
		\time 3/4
		des2. ~
		\time 4/4
		des2. r4
		\time 2/4
		des2 
		\time 3/4
		c2 ~ c8 r8
		bes2 ~ bes8 r8
		\time 4/4
		a2. ~ a8 r8
		\bar ":|"
	}
	}
}

