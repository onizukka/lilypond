secondTrumpet = \new Staff
{
	\set Staff.midiInstrument = #"trumpet"
	\set Staff.instrumentName = "2nd Trumpet"
	\set Score.skipBars = ##t
	
	\transpose bes c {
	\relative c'''
	{
	
		\key bes \minor
	
		\time 1/4
		r4
		
		\bar "|:"
		\time 3/4
		r2.
		\time 4/4
		r1
		\time 3/4
		r2.
		\time 4/4
		r2. r8 des16 f
		\time 2/4
		bes8. bes16 bes8. bes16
		\time 3/4
		aes2. ges2. 
		\time 4/4
		f2. r4
		\bar ":|"
	}
	}
}

