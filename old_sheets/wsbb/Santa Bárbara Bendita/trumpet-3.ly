thirdTrumpet = \new Staff
{
	\set Staff.midiInstrument = #"trumpet"
	\set Staff.instrumentName = "3rd Trumpet"
	\set Score.skipBars = ##t
	
	\transpose ees c {
	\relative c''
	{
	
		\key bes \minor
	
		\time 1/4
		r8 f

		\bar "|:"
		\time 3/4
		bes8. a16 bes8 c bes f ~
		\time 4/4
		f2. r8 f
		\time 3/4
		bes8. a16 bes8 c bes f ~
		\time 4/4
		f2. r8 bes16 c
		\time 2/4
		des8. des16 des8. des16
		\time 3/4
		c2. bes2. 
		\time 4/4
		a2. r8 f
		\bar ":|"
	}
	}
}

