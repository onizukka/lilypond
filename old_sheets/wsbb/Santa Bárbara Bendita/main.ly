\version "2.12.3"

\include "score.ly"

\header
{
	title = "En el pozo Maria Luisa"
}

\paper {
	#(set-default-paper-size "a4" 'landscape)
}

\score
{
	\layoutScore
	
	\layout {}
	\midi {
	  \context {
		 \Score
		 tempoWholesPerMinute = #(ly:make-moment 60 4)
	  }
	}
}