firstTenor = \new Staff
{
	\set Staff.midiInstrument = #"tenor sax"
	\set Staff.instrumentName = "1st Sax. Tenor"
	\set Score.skipBars = ##t
	
	\transpose bes c {
	\relative c''
	{
	
		\key bes \minor
		
		\time 1/4
		r4

		\bar "|:"
		\time 3/4
		f2. ~
		\time 4/4
		f2. ~ f8 r8
		\time 3/4
		f2. ~
		\time 4/4
		f2. r4
		\time 2/4
		f2 
		\time 3/4
		ees2 ~ ees8 r8
		des2 ~ des8 r8
		\time 4/4
		c2. ~ c8 r8
		\bar ":|"
	}
	}
}

