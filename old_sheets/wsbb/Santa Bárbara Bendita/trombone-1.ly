intro = \relative c'
{
	fis8 a b e, ~ e2 |
	f8 a b d, ~ d2 |
	fis8 a b e, ~ e2 |
	f8 a b d, ~ d2 |
	e1
}

verseAA = \relative c''
{
	gis4. fis8^^ r gis^^ r fis|
	a4. d,8 ~ d2 |
	cis4. b8^^ r cis^^ r b|
	e4 b2 a4 |
}


firstTrombone= \new Staff
{
	\set Staff.midiInstrument = #"trombone"
	\set Staff.instrumentName = "1st Trombone"
	\set Score.skipBars = ##t
	
	\transpose bes c {
	\relative c'''
	{
	
		\key bes \minor
		
		\time 1/4
		r4

		\bar "|:"
		\time 3/4
		r2.
		\time 4/4
		f16. ges32 f16 ees f8 des16 ees f8 r8 r8 f16 f
		\time 3/4
		bes8. a16 bes8 c bes f
		\time 4/4
		f16. ges32 f16 ees f8 des16 ees f8 r8 r8 bes16 c
		\time 2/4
		des8. des16 des8. des16
		\time 3/4
		c8 c16 bes aes8 r16 bes c8 c16 c
		bes8 bes16 aes ges8 r16 aes bes8 bes16 bes
		\time 4/4
		a8[ ges] f r8 r2
		\bar ":|"
	}
	}
}

