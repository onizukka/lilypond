baritone = \new Staff
{
	\set Staff.midiInstrument = #"baritone sax"
	\set Staff.instrumentName = "Sax. Baritone"
	\set Score.skipBars = ##t
	
	\transpose ees c {
	\relative c''
	{
	
		\key bes \minor
	
		\time 1/4
		r4

		\bar "|:"
		\time 3/4
		bes2. ~
		\time 4/4
		bes2. ~ bes8 r8
		\time 3/4
		bes2. ~
		\time 4/4
		bes2. r4
		\time 2/4
		bes2 ~
		\time 3/4
		bes2 ~ bes8 r8
		bes2 ~ bes8 r8
		\time 4/4
		bes2. ~ bes8 r8
		\bar ":|"
	}
	}
}

