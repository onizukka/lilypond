\version "2.12.3"

\include "trumpet-1.ly"
\include "trumpet-2.ly"
\include "trumpet-3.ly"
\include "trumpet-4.ly"
\include "trombone-1.ly"
\include "trombone-2.ly"
\include "trombone-3.ly"
\include "trombone-4.ly"
\include "sax-alto-1.ly"
\include "sax-alto-2.ly"
\include "sax-tenor-1.ly"
\include "sax-tenor-2.ly"
\include "sax-baritone.ly"

layoutScore =
{
	<<
		
		% \singer
		% \mainLyrics
		
		\new StaffGroup 
		<<
			\new StaffGroup
			<<
				\firstTrumpet
				\secondTrumpet
			>>
			
			\new StaffGroup
			<<
				\thirdTrumpet
				\fourthTrumpet
			>>
		>>
		
		\new StaffGroup 
		<<
			\new StaffGroup 
			<<
				\firstTrombone
				\secondTrombone
			>>
			
			\new StaffGroup 
			<<
				\thirdTrombone
				\fourthTrombone
			>>
		>>
		
		\new StaffGroup 
		<<	
			\new StaffGroup 
			<<
				\firstAlto
				\secondAlto
			>>
			
			\new StaffGroup 
			<<
				\firstTenor
				\secondTenor
			>>
			
			\baritone
		>>
	>>
}