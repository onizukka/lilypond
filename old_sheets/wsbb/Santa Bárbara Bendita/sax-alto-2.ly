secondAlto = \new Staff
{
	\set Staff.midiInstrument = #"alto sax"
	\set Staff.instrumentName = "2nd Sax. Alto"
	\set Score.skipBars = ##t
	
	\transpose ees c {
	\relative c''
	{
	
		\key bes \minor
		
		\time 1/4
		r4

		\bar "|:"
		\time 3/4
		bes2. ~
		\time 4/4
		bes2. ~ bes8 r8
		\time 3/4
		bes2. ~
		\time 4/4
		bes2. r4
		\time 2/4
		bes2 
		\time 3/4
		aes2 ~ aes8 r8
		ges2 ~ ges8 r8
		\time 4/4
		f2. ~ f8 r8
		\bar ":|"
	}
	}
}

