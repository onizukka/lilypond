secondTrombone = \new Staff
{
	\set Staff.midiInstrument = #"trombone"
	\set Staff.instrumentName = "2nd Trombone"
	\set Score.skipBars = ##t
	
	
	\transpose bes c {
	\relative c'''
	{
		\key bes \minor
		
		\time 1/4
		r4

		\bar "|:"
		\time 3/4
		r2.
		\time 4/4
		r2. r8 f16 f
		\time 3/4
		des8. c16 des8 ees des bes ~
		\time 4/4
		bes2. r8 des16 f
		\time 2/4
		bes8. bes16 bes8. bes16
		\time 3/4
		aes8 aes16 aes aes8 r16 aes aes8 aes16 aes
		ges8 ges16 ges ges8 r16 ges ges8 ges16 ges
		\time 4/4
		f8[ ees] c r8 r2
		\bar ":|"
	}
	}
}

