fourthTrombone = \new Staff
{
	\set Staff.midiInstrument = #"trombone"
	\set Staff.instrumentName = "4th Trombone"
	\set Score.skipBars = ##t
	
	\transpose ees c {
	\relative c''
	{
	
		\key bes \minor
	
		\time 1/4
		r4

		\bar "|:"
		\time 3/4
		r2.
		\time 4/4
		r2. r8 f16 f
		\time 3/4
		f8. f16 f8 f f des ~
		\time 4/4
		des2. r8 des16 ees
		\time 2/4
		f8. f16 f8. f16
		\time 3/4
		ees8 ees16 ees ees8 r16 ees ees8 ees16 ees
		des8 des16 des des8 r16 des des8 des16 des
		\time 4/4
		c8[ c] c r8 r2
		\bar ":|"
	}
	}
}

