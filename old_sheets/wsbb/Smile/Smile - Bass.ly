\version "2.18.2"


% Descripción de funcionalidadf
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% PLUGINS
% - \eolMark:					mark al final de línea
% - \bendBefore: 				solución al bendBefore (i.e. dis8 \bendBefore)
% - \rit: 						crea una porción de ritardando que debe terminar como las dinámicas, con un \!
% - \improOn, \improOff:	negra de improvisación
% - \emptyStaff:				Imprime un pentagrama vacío
% - \emptyBar:					Imprime un compás invisible (útil para indentados)



% LAYOUT
% - El aspecto de los silencios multicompás es como los de las partituras de jazz
% - Margen extra de las articulaciones para evitar la colisión con las ligaduras
% - Glissandos bonitos



% PAPER
% - Se imprime el título, instrumento y número de página en todas las páginas
% - Solo se debe tocar el page-count (si eso). Sale bonito tal cual.

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Descripción de funcionalidadf
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% 03-09-14: Layout
% Ahora las articulaciones van por dentro de las ligaduras. Parece que es la mejor solución



% 04-09-14: Layout
% Los creciendos y decreciendos tienen la línea un poco más gruesa
%
% Markup
% Añadidas guías para correcta posición y markup de la indicación de tempo
%
% Plugins
% Añadida función \emptyStave para imprimir un pentagrama vacío.
% En algunas partituras hay un pentagrama vacío antes del coda (lo cual mola bastante, por cierto)



% 05-09-14: Plugins
% Añadido a la función \emptyStave el esconder la armadura de la clave, que no estaba.



% 08-09-14: Layout
% Ahora no se muestra la cancelación de accidentales al haber un cambio de armadura



% 09-09-14: Chords
% Añadida sentencia para que los acordf menores salgan con un guión en vez de con una M minúscula



% 10-09-14: Plugins
% Mejorada la función \bendAfter.
% Ahora es genérica, y se puede definir cualquier texto en un lateral que haga las funciones de ornamento



% 15-09-14: Layout
% Añadido un poco más de margen a los reguladores



% 16-09-14: Plugins
% Mejorada la función para imprimir barras de improvisación.
% Antes se llamaba al comando \rs para pintar una barra que ocupaba una negra
% Se reemplaza \rs por \restImproOn y \restImproOff, siendo esta opción mucho más flexible.
% Cuando se activa \restImproOn, todo silencio se convierte en barra de improvisación, respetando su duración.



% 20-09-14: Plugins
% Mejorada la función para imprimir barras de improvisación.
% Se reemplaza \restImproOn y \restImproOff por \improOn y \improOff
% Ahora, también las notas del pentagrama salen como barra de improvisación.
% (los comandos \improvisationOn e \improvisationOff de lilypond no se llevan bien con LilyJAZZ)
% Se ha añadido un grob correspondiente a los slash de la fuente LilyJAZZ en el archivo lily-jazz.ly



% 20-09-14: Layout
% Comentada la línea que oculta la cancelación de accidentales al haber cambio de armadura.
% Realmente, la cancelación es muy bienvenida.



% 01-10-14: Plugins
% Renombrada la función \emptyStave a \emptyStaff (stave es el plural de staff) y 
% corregida (ya no muestra el número de compás).
% Recordar que dfpués de usar \emptyStaff se necfita redefinir el 
% número de compás con \set Score.currentBarNumber = #xx
%
% Añadida la función \emptyBar que oculta un compás entero. Se puede usar para crear un 
% efecto de indentación en un pentagrama dado.
% Recordar que dfpués de usar \emptyBar se necfita:
% - redefinir el número de compás con \set Score.currentBarNumber = #xx
% - volver a pintar la clave con \clef
% - volver a pintar la armadura con \key



% 15-10-14: Language
% Cambiado el lenguaje de entrada de notas del finlandf al inglés
% La diferencia es que ahora los sostenidos se escriben "-s" en vez de "-is"
% y los bemoles "-f" en vez de "-es".
%
% Estructura
% Para poder autoincluir los archivos de instrumento en otros 
% (para montar un midi, o un score), se han movido los plugins a un archivo separado
% y ahora se utiliza la función \includeOnce, evitando así el problema de las
% dependencias circulares.
%
% Global
% Todas las instrucciones que se colocaban justo antes de empezar la música están ahora
% recogidas en una función \global.
% A \global se le ha añadido instrucciones para que agrupe las corcheas de dos en dos
% en vez de cuatro en cuatro (el comportamiento por defecto).
%
% Plugins
% Se le ha aumentado el grosor de los bordf de la función \boxed



% 26-10-14: Chords
% Añadido el include "chord-exception.ily" que recoge una mejor notación de los acordf.
% Actualmente, recoge los acordf maj7, 7(b5), 7(#5), 7(b9), 7(#9), 7(add13), maj9(add13), 9(#11)
% Se irá añadiendo acordf a menudo.
 

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Dependencias
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% \includeOnce
% Incluye una dependencia solo una vez. Así se pueden incluir los archivos en otros 
% (para montar un score por ejemplo) sin liarla parda con dependencias cíclicas.

includeOnce = #(
	define-void-function (parser location filename) (string?) (
		if (not (defined? (string->symbol filename))) (
			begin (
				ly:parser-include-string parser (
					string-concatenate (list "\\include \"" filename "\"")
				)
			)
			(primitive-eval (list 'define (string->symbol filename) #t))
		)
	)
)



% LilyJAZZ
\includeOnce "includes/ignatzek-jazz-chords.ily"
\includeOnce "includes/lily-jazz.ily"



% Language
\language "english"



% Plugins
\includeOnce "includes/plugins-1.2.ily"



% Chord Exceptions
\includeOnce "includes/chord-exceptions.ily"

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~











% Disposición
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Cabecera
\header {
  title = "Smile"
  subtitle = "(from Tony BENNET Duets Album with Barbra STREISAND)"
  instrument = "Bass"
  composer = "Charlie CHAPLIN, John TURNER & Geoffrey PARSONS"
  arranger = ""
  poet = \markup \tiny "Transcripción: Gabriel PALACIOS"
  meter = \markup \tiny "gabriel.ps.ms@gmail.com"
  tagline = \markup {
    \center-column {
      \line { \tiny "Transcripción: Gabriel PALACIOS" }
      \line { \tiny "gabriel.ps.ms@gmail.com" }
    }
  }
}



% Márgenes, fuentes y distancias del papel
\paper {
	% El definir otro tipo de fuente no permite cambiar el tamaño global del staff. Es un bug conocigo
	#(define fonts (make-pango-font-tree "LilyJAZZText" "Nimbus Sans" "Luxi Mono" (/ 20 20)))

	% distancia entre la parte superior de la página y el título
	top-markup-spacing #'basic-distance = 5

	% distancia entre el título y el primer sistema
	markup-system-spacing #'basic-distance = 25

	% distancia entre el texto y el primer sistema 
	top-system-spacing #'basic-distance = 15

	% márgen inferior
	bottom-margin = 20
	
	% intentado del primer sistema
	indent = 0
	 
	% número de páginas
	% page-count = 2

	ragged-last-bottom = ##f 

	oddHeaderMarkup = \markup {
	 \on-the-fly #not-first-page \fill-line {
		\fromproperty #'page:page-number-string
		\fromproperty #'header:title
		\fromproperty #'header:instrument
	 }
	}

	evenHeaderMarkup = \markup {
	 \on-the-fly #not-first-page \fill-line {
		\fromproperty #'header:instrument
		\fromproperty #'header:title
		\fromproperty #'page:page-number-string
	 }
	}
}

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~







% Elementos comunes de la partitura
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
	% Oculta el primer pentagrama del markup
	\override Staff.VerticalAxisGroup #'remove-first = ##t
	
	% Indicación de tiempo
	\once \override Score.RehearsalMark.X-offset = #5
	\mark \jazzTempoMarkup "Ballad" c4 "60"

	s1*9
	
	\mark \markup \boxed "A"
	s1*20
	
	\mark \markup \boxed "B"
	s1*19
	
	\bar "|."
}



% Función musical global
global = {
	% Para pintar todos los números de compás debajo del pentagrama
	% \override Score.BarNumber #'break-visibility = #end-of-line-invisible
	% \override Score.BarNumber #'direction = #DOWN
	% \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

	\jazzOn
	\compressFullBarRests

	% Arregla el problema de tener el scoreMarkup añadido
	\revert MultiMeasureRest #'staff-position
	\override Voice.Rest #'staff-position = #0	

	\set Timing.beamExceptions = #'()
	\set Timing.baseMoment = #(ly:make-moment 1/4)
	\set Timing.beatStructure = #'(1 1 1 1) 
}

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Elementos específicos del instrumento
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Cifrado
scoreChords = \chords {
    
  \set minorChordModifier = \markup { "-" }
  
  s1*8
  s2 bf:9-.13
  
  ef1:9
  f:m7/ef
  f:dim/ef
  af2/bf s4 af:7
  
  g1:m7
   gf:dim
  f1:m9
  bf2:9-.11+ bf4:m9 bf:dim
  
  f1:m9
  f1:m9
  af:m9
  df2:9 df4:9 df4:9/cf
  
  bf2:6 g:5+
  c2:m9 f:7
  f1:m7/bf
  ef2:7.5-/bf bf:9-
  
  a1:m7.5-
  af2:m7 df4:7 cf:6
  bf2:6 af:maj/bf
  c:maj/d c:dim/d
  
  g1:6.9
  c2:6/g c:maj/g
  g1:dim
  a2:m7 d4:9 c:7.5-
  
  b1:m11
  bf:dim
  a:m7
  e2:6.9 e4:sus4.7.9+ e:9-
  
  a1:m9
  a2:m7 b:m7
  c1:m7
  f1:9.11+
  
  d2:6 s2
  s1*5
  
  g1:9
}

% Música
bass = \relative c' {
  
  \global
  
  \clef bass
  \key gf \major
  
  R1*8
  r2 bf4 \p \< bf,
  
  \key ef \major
  
  \tag #'part \break
  ef8 \mf bf' ef4 r4. bf8
  ef,2. ef4
  ef2. r8 ef
  bf'2 bf4 af
  
  \tag #'part \break
  g2. r8 g
  gf2. r8 gf
  f2 f4 ~ \times 2/3 { f4 f8 } 
  bf,8 bf4 bf8 bf4 g'8 c,
  
  \tag #'part \break
  f2 ~ f8 c f c
  f2 f4 af
  af2. \times 2/3 { r8 af, af' } 
  df,2 df4 cf
  
  \tag #'part \break
  bf4 ~ \times 2/3 { bf8 bf' f } g4 g8 g,
  c4 ~ \times 2/3 { c4 ef8 } f4 ~ \times 2/3 { f8 f c }
  bf4. bf'8 bf4 bf8 bf,
  bf2 bf4 bf8 g 
  
  \tag #'part \break
  a2. \ppsubito \< a4
  af?4 \mf ~ \times 2/3 { af8 af' ef } df4 cf
  bf4. bf'8 bf4 bf8 bf,
  d4 \< d'2 d,4
  
  \key g \major
  
  \tag #'part \break
  g4 \f \> g'2 d8 \mf d,
  g4 g'2 g8 d,
  g2 g'
  a,2 d,4 c
  
  \tag #'part \break
  b ~ \times 2/3 { b4 b8 } b4 b 
  bf? ~ bf8 bf'16 g bf,4. bf8
  a2. a4
  e'2 e4. e8
  
  \tag #'part \break
  a,2 ~ a8 a' e e
  a,2 b4. b8
  c2 g'4 c, 
  f2 f4 ef
  
  \tag #'part \break
  d2 \> r2 \!
  R1*2
  R1 \fermataMarkup
  
  \tag #'part \break
  R1*2
  g,1 \fermata
}

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

%{
\book {
	\score {
		\tag #'part <<
			\scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\new Staff { \bass }
			>>
		>>
	}
}
%}