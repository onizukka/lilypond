\version "2.18.2"


% Descripción de funcionalidadf
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% PLUGINS
% - \eolMark:					mark al final de línea
% - \bendBefore: 				solución al bendBefore (i.e. ds8 \bendBefore)
% - \rit: 						crea una porción de ritardando que debe terminar como las dinámicas, con un \!
% - \improOn, \improOff:	negra de improvisación
% - \emptyStaff:				Imprime un pentagrama vacío
% - \emptyBar:					Imprime un compás invisible (útil para indentados)



% LAYOUT
% - El aspecto de los silencios multicompás es como los de las partituras de jazz
% - Margen extra de las articulaciones para evitar la colisión con las ligaduras
% - Glissandos bonitos



% PAPER
% - Se imprime el título, instrumento y número de página en todas las páginas
% - Solo se debe tocar el page-count (si eso). Sale bonito tal cual.

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Descripción de funcionalidadf
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% 03-09-14: Layout
% Ahora las articulaciones van por dentro de las ligaduras. Parece que es la mejor solución



% 04-09-14: Layout
% Los creciendos y decreciendos tienen la línea un poco más gruesa
%
% Markup
% Añadidas guías para correcta posición y markup de la indicación de tempo
%
% Plugins
% Añadida función \emptyStave para imprimir un pentagrama vacío.
% En algunas partituras hay un pentagrama vacío antes del coda (lo cual mola bastante, por cierto)



% 05-09-14: Plugins
% Añadido a la función \emptyStave el esconder la armadura de la clave, que no estaba.



% 08-09-14: Layout
% Ahora no se muestra la cancelación de accidentales al haber un cambio de armadura



% 09-09-14: Chords
% Añadida sentencia para que los acordf menores salgan con un guión en vez de con una M minúscula



% 10-09-14: Plugins
% Mejorada la función \bendAfter.
% Ahora es genérica, y se puede definir cualquier texto en un lateral que haga las funciones de ornamento



% 15-09-14: Layout
% Añadido un poco más de margen a los reguladores



% 16-09-14: Plugins
% Mejorada la función para imprimir barras de improvisación.
% Antes se llamaba al comando \rs para pintar una barra que ocupaba una negra
% Se reemplaza \rs por \restImproOn y \restImproOff, siendo esta opción mucho más flexible.
% Cuando se activa \restImproOn, todo silencio se convierte en barra de improvisación, respetando su duración.



% 20-09-14: Plugins
% Mejorada la función para imprimir barras de improvisación.
% Se reemplaza \restImproOn y \restImproOff por \improOn y \improOff
% Ahora, también las notas del pentagrama salen como barra de improvisación.
% (los comandos \improvisationOn e \improvisationOff de lilypond no se llevan bien con LilyJAZZ)
% Se ha añadido un grob correspondiente a los slash de la fuente LilyJAZZ en el archivo lily-jazz.ly



% 20-09-14: Layout
% Comentada la línea que oculta la cancelación de accidentales al haber cambio de armadura.
% Realmente, la cancelación es muy bienvenida.



% 01-10-14: Plugins
% Renombrada la función \emptyStave a \emptyStaff (stave es el plural de staff) y 
% corregida (ya no muestra el número de compás).
% Recordar que dfpués de usar \emptyStaff se necfita redefinir el 
% número de compás con \set Score.currentBarNumber = #xx
%
% Añadida la función \emptyBar que oculta un compás entero. Se puede usar para crear un 
% efecto de indentación en un pentagrama dado.
% Recordar que dfpués de usar \emptyBar se necfita:
% - redefinir el número de compás con \set Score.currentBarNumber = #xx
% - volver a pintar la clave con \clef
% - volver a pintar la armadura con \key



% 15-10-14: Language
% Cambiado el lenguaje de entrada de notas del finlandf al inglés
% La diferencia es que ahora los sostenidos se escriben "-s" en vez de "-is"
% y los bemoles "-f" en vez de "-es".
%
% Estructura
% Para poder autoincluir los archivos de instrumento en otros 
% (para montar un midi, o un score), se han movido los plugins a un archivo separado
% y ahora se utiliza la función \includeOnce, evitando así el problema de las
% dependencias circulares.
%
% Global
% Todas las instrucciones que se colocaban justo antes de empezar la música están ahora
% recogidas en una función \global.
% A \global se le ha añadido instrucciones para que agrupe las corcheas de dos en dos
% en vez de cuatro en cuatro (el comportamiento por defecto).
%
% Plugins
% Se le ha aumentado el grosor de los bordf de la función \boxed



% 26-10-14: Chords
% Añadido el include "chord-exception.ily" que recoge una mejor notación de los acordf.
% Actualmente, recoge los acordf maj7, 7(b5), 7(#5), 7(b9), 7(#9), 7(add13), maj9(add13), 9(#11)
% Se irá añadiendo acordf a menudo.
 

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Dependencias
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% \includeOnce
% Incluye una dependencia solo una vez. Así se pueden incluir los archivos en otros 
% (para montar un score por ejemplo) sin liarla parda con dependencias cíclicas.

includeOnce = #(
	define-void-function (parser location filename) (string?) (
		if (not (defined? (string->symbol filename))) (
			begin (
				ly:parser-include-string parser (
					string-concatenate (list "\\include \"" filename "\"")
				)
			)
			(primitive-eval (list 'define (string->symbol filename) #t))
		)
	)
)



% LilyJAZZ
\includeOnce "includes/ignatzek-jazz-chords.ily"
\includeOnce "includes/lily-jazz.ily"



% Language
\language "english"



% Plugins
\includeOnce "includes/plugins-1.2.ily"



% Chord Exceptions
\includeOnce "includes/chord-exceptions.ily"

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~











% Disposición
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Cabecera
\header {
  title = "Smile"
  subtitle = "(from Tony BENNET Duets Album with Barbra STREISAND)"
  instrument = "Piano"
  composer = "Charlie CHAPLIN, John TURNER & Geoffrey PARSONS"
  arranger = ""
  poet = \markup \tiny "Transcripción: Gabriel PALACIOS"
  meter = \markup \tiny "gabriel.ps.ms@gmail.com"
  tagline = \markup {
    \center-column {
      \line { \tiny "Transcripción: Gabriel PALACIOS" }
      \line { \tiny "gabriel.ps.ms@gmail.com" }
    }
  }
}



% Márgenes, fuentes y dstancias del papel
\paper {
	% El definir otro tipo de fuente no permite cambiar el tamaño global del staff. Es un bug conocigo
	#(define fonts (make-pango-font-tree "LilyJAZZText" "Nimbus Sans" "Luxi Mono" (/ 20 20)))

	% dstancia entre la parte superior de la página y el título
	top-markup-spacing #'basic-dstance = 5

	% dstancia entre el título y el primer sistema
	markup-system-spacing #'basic-dstance = 25

	% dstancia entre el texto y el primer sistema 
	top-system-spacing #'basic-dstance = 15

	% márgen inferior
	bottom-margin = 20
	
	% intentado del primer sistema
	indent = 0
	 
	% número de páginas
	% page-count = 1

	ragged-last-bottom = ##f 

	oddHeaderMarkup = \markup {
	 \on-the-fly #not-first-page \fill-line {
		\fromproperty #'page:page-number-string
		\fromproperty #'header:title
		\fromproperty #'header:instrument
	 }
	}

	evenHeaderMarkup = \markup {
	 \on-the-fly #not-first-page \fill-line {
		\fromproperty #'header:instrument
		\fromproperty #'header:title
		\fromproperty #'page:page-number-string
	 }
	}
}

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~







% Elementos comunes de la partitura
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
	% Oculta el primer pentagrama del markup
	\override Staff.VerticalAxisGroup #'remove-first = ##t
	
	% Indicación de tiempo
	\once \override Score.RehearsalMark.X-offset = #5
	\mark \jazzTempoMarkup "Ballad" c4 "60"

	s1*9
	
	\mark \markup \boxed "A"
	s1*20
	
	\mark \markup \boxed "B"
	s1*19
	
	\bar "|."
}



% Función musical global
global = {
	% Para pintar todos los números de compás debajo del pentagrama
	% \override Score.BarNumber #'break-visibility = #end-of-line-invisible
	% \override Score.BarNumber #'direction = #DOWN
	% \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

	\jazzOn
	\compressFullBarRests

	% Arregla el problema de tener el scoreMarkup añadido
	\revert MultiMeasureRest #'staff-position
	\override Voice.Rest #'staff-position = #0	

	\set Timing.beamExceptions = #'()
	\set Timing.baseMoment = #(ly:make-moment 1/4)
	\set Timing.beatStructure = #'(1 1 1 1) 
}

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Elementos específicos del instrumento
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Cifrado
scoreChords = \chords {
    
  \set minorChordModifier = \markup { "-" }
  
  s1*8
  s2 bf:9-.13
  
  ef1:9
  f:m7/ef
  f:dim/ef
  af2/bf s4 af:7
  
  g1:m7
   gf:dim
  f1:m9
  bf2:9-.11+ bf4:m9 bf:dim
  
  f1:m9
  f1:m9
  af:m9
  df2:9 df4:9 df4:9/cf
  
  bf2:6 g:5+
  c2:m9 f:7
  f1:m7/bf
  ef2:7.5-/bf bf:9-
  
  a1:m7.5-
  af2:m7 df4:7 cf:6
  bf2:6 af:maj/bf
  c:maj/d c:dim/d
  
  g1:6.9
  c2:6/g c:maj/g
  g1:dim
  a2:m7 d4:9 c:7.5-
  
  b1:m11
  bf:dim
  a:m7
  e2:6.9 e4:sus4.7.9+ e:9-
  
  a1:m9
  a2:m7 b:m7
  c1:m7
  f1:9.11+
  
  d2:6 s2
  s1*5
  
  g1:9
}

% Música
rightHandA = \relative c'' {

  \global
  \voiceOne
  
  \key fs \major
  
  \override Voice.Rest.staff-position = #8
  
  <as cs>2 \arpeggio r16 as8. ~ as4
  f2 r4 <ds as' ds>4
  \tag #'part \break
  r4 <es' gs> s2
  gs,8 ( fs <b ds>4 ) fss4. <ds fss ass ds>8 \arpeggio
  
  \tag #'part \break
  <ds gs b>4.  <as fs' cs'>8 <ds as' fs'>4 <fs gs as cs>8 <ds fs gs as>
  \set tieWaitForNote = ##t
  r4 \appoggiatura { a16 ~ b ~ cs ~ e ~ fs ~ a ~ cs ~ } <a, b cs e fs a cs e>4 as'4 ( cs )
  
  \tag #'part \break
  cs4 b es, as  
  \change Staff = "leftHand" r8 ds,, as' [ fs ] ds' b 
  \change Staff = "rightHand" << { fs'8 ds} \\ { gs,4 } >>
  
  \tag #'part \break
  bf'4 g' \afterGrace d2 { bf,16 ( cf df d e f g af bf cf df d e f g af bf cf df d e f g af}
    
  \key ef \major
  
  \override Voice.Rest.staff-position = #0
  \override Voice.MultiMeasureRest.staff-position = #2
  
  \tag #'part \break
  bf1 )
  r8 g, f'2.
  <af,, d f>1
  r8 af bf4 r2
  
  \tag #'part \break
  r2 f''8 ef r4
  r2 <b d f>4 r4
  R1*2
  
  \tag #'part \break
  <af c ef>4 r2.
  <af c>2 ~ <af c>8 af c g'
  ef1
  R1
  
  \tag #'part \break
  r2. <g, bf>8 <f af>
  <d f>2 r
  R1
  r2. <b d>4
  
  \tag #'part \break
  r8 g c g' c4 <d ef g>
  R1*2
  r2. \times 4/7 { fs,,16 ( g a bf ef g a }
  
  \key g \major
   
  \tag #'part \break
  <d, g b d>1 ) \arpeggio
  R1*3
  
  \tag #'part \break
  d'1
  R1*2
  r2. r8 d, ~
  
  \tag #'part \break
  d4 b' e,2
  r8 e d' [ c ] a fs e' d
  R1*2
  
  \tag #'part \break
  <d,, a' d>2 \arpeggio r2
  \once \override Voice.MultiMeasureRest.staff-position = #0
  R1*2
  
  <af' b f'>2 <a c ef fs> \fermata
  
  \tag #'part \break
  <c a'>4 <b g'>2 <b g'>4
  <c a'>8 <d b'>8 <b g'>2.
  <d fs a d>1 \arpeggio \fermata
}

rightHandB = \relative c' {

  \global
  \voiceTwo
  
  \key fs \major
  
  \override Voice.Rest.staff-position = #-9
  
  <ds>2 \arpeggio <b ds>16  ~ <b ds>8. ~ <b ds>4
  <a c>2 \arpeggio <fs b ds>2
  <ds' as' cs ds>2 r8 <gss, ds'>4. ~
  <cs ds>4 ~ <b ds> r8 <bs cs es>8 <fss ass dss>4
  
  \slurDown s2 r8 gs ( as16 fs gs fs ) \slurNeutral
  <fs a cs e>2 <cs' fs>
  
  r8 gs' fss [ fs ] <gs, cs>4 <css es>
  \once \override NoteColumn #'ignore-collision = ##t
  <gs b ds gs>1 \arpeggio 
  <c ef g>4 <af' c ef> <d, g b>2
  
  s1*36
}

leftHandA = \relative c, {
  
  \global
  \voiceThree
  
  \clef bass
  \key fs \major
  
  \override Voice.Rest.staff-position = #5
  
  <cs' as'>2. r4 
  <d fs>2 \arpeggio r4 gs
  r8 fs ~ <fs ds'>4 r8 es4. (
  <gs, fs'>2 ) r8 cs4.
  
  r16 ds16 ~ <ds b'>8 ~ <ds b'>4 ~ <ds b'>2
  cs2 d
  
  << { r8 ds' d4 } \\ { r8 as4. } >> <as, fs'>4 fs'
  b,1 \arpeggio
  ef8 af c4 af2
  
  s1*36
}

leftHandB = \relative c, {
  
  \global
  \voiceFour
  
  \clef bass
  \key fs \major
  
  fs2. \arpeggio cs'4 ~
  cs cs <gs, cs>2
  as'2
  \appoggiatura { cs,8 } ds'2 -> ~ ds2 ds,2
  
  gs1
  <b, fs'>2 e
  
  fs4 as ds, gs
  cs,1 \arpeggio
  df2 bf'
    
  \key ef \major
  R1*20
  
  \key g \major
  
  R1*12
  
  \once \override Voice.MultiMeasureRest.staff-position = #2
  R1
  R1*2
  <d, d'>2 <d d'>
  
  c8 g' e' a d e a b
  c,,,8 ef' a d ef f g a
  
  <g,, d' b'>1 \arpeggio
}

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

%{
\book {
	\score {
		<<
			\scoreMarkup
			\scoreChords
			\new PianoStaff <<
      		\set PianoStaff.connectArpeggios = ##t
      		\new Staff = "rightHand" <<
        			\new Voice \rightHandA
        			\new Voice \rightHandB
      		>>
      		\new Staff = "leftHand" \with { \RemoveEmptyStaves } <<
    				\override Staff.VerticalAxisGroup.remove-first = ##t
        			\new Voice \leftHandA
        			\new Voice \leftHandB
      		>>
    		>>
		>>
	}
}
%}