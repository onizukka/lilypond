\version "2.12.3"

\header {
	title = "New York, New York"
	composer = "John JKander, Fred Ebb"
}

solista = \new Voice = "solista"
{
   \set Score.skipBars = ##t
   \relative c''
   {
      % ------------------------------------------------- %
      \time 4/4
      \key c \major
      R1*7
      % ------------------------------------------------- %
      
      r4 c, b8 c4 a8 
      g1
      r4 c4 d8 c4 a8 
      g1
      
      r8 b4. c4 d 
      e ees8 e ~ e ees e4
      r4 g e ~ e4 e8 d ~ 
      e1
      
      r4 c, b8 c4 a8 
      g1
      r4 c4 d8 c4 a8 
      g1
      
      r8 b4 c d 
      e ees8 e ~ e ees e4
      r4 g e ~ e4 g8 e ~ 
      e1
      
      r8 
   }
}

coro = \new Staff
{
   \set Score.skipBars = ##t
   \relative c''
   {
      % ------------------------------------------------- %
      \time 4/4
      \key g \minor
      % ------------------------------------------------- %
   }
}

lSolista = \new Lyrics \lyricsto "solista" 
{
   \lyricmode
   {
      
   }
}

\new ChoirStaff <<
   \solista
   \lSolista
   \coro
>>