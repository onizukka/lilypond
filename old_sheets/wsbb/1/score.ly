\version "2.16.1"

\include "imports/main.ly"

\version "2.18.0"

\header {
  % Eliminar el pie de página predeterminado de LilyPond
  tagline = ##f
}

\paper {
  #(set-paper-size "a3" 'landscape)
}

global = {
  \key f \major
  \time 4/4
  
   % Para pintar todos los números de compás debajo del pentagrama
  \override Score.BarNumber #'break-visibility = #end-of-line-invisible
  %\override Score.BarNumber #'direction = #DOWN
  %\override Score.BarNumber #'extra-offset = #'(1.5 . 0)

  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0
}

trumpetI = \relative c''' {
  \global
  \transposition bes
  
  r2 r4 r8 gis' ~ 
  gis4. e8 ~ e4. e8 ~
  e4. d8 ~ d4. c8 ~ 
  c2.. a8 ~ 
  
  a4. b8 ~ b4. g8 ~
  g4. a8 ~ a4. f8
  
  
  
}

trumpetII = \relative c''' {
  \global
  \transposition bes
  
  r2 r4 r8 e ~ 
  e4. cis8 ~ cis4. d8 ~
  d4. bes8 ~ bes4. a8 ~ 
  a2.. fis8 ~ 
  
  fis4. gis8 ~ gis4. e8 ~ 
  e4. fis8 ~ fis4. d8
}

trumpetIII = \relative c'' {
  \global
  \transposition ees
  
  r2 r4 r8 cis' ~ 
  cis4. a8 ~ a4. bes8 ~ 
  bes4. a8 ~ a4. g8 ~ 
  g4. f8 ~ f4. e8 ~ 
  
  e4. dis8 ~ dis4. d8 ~ 
  d4. cis8 ~ cis4. c8
}

trumpetIV = \relative c'' {
  \global
  \transposition ees
  
  r2 r4 r8 b' ~ 
  b4. a8 ~ a4. bes8 ~
  bes4. a8 ~ a4. g8 ~ 
  g4. f8 ~ f4. e8 ~ 
  
  e4. dis8 ~ dis4. d8 ~ 
  d4. cis8 ~ cis4. c8
}

altoSaxI = \relative c'' {
  \global
  \transposition ees
  
  r8 c d e f a c cis ~
  cis2 r8 d, e f ~
  f2 
}

altoSaxII = \relative c'' {
  \global
  \transposition ees
  
  r8 c d e f a c a ~
  a2 r8 b, cis d ~
  d2 
}

tenorSaxI = \relative c'' {
  \global
  \transposition bes,
  
  r8 c d e f a c fis ~
  fis2 r8 a, b bes ~
  bes2 
}

tenorSaxII = \relative c'' {
  \global
  \transposition bes,
  
  r8 c d e f a c e ~
  e2 r8 fis, gis a ~
  a2
}

baritoneSax = \relative c'' {
  \global
  \transposition ees,
  
  r8 c d e f a c b ~
  b2 ~ b8 b,4. ~ 
  b2 ~ b8 b4. ~
  b2 ~ b4. b8
}

tromboneI = \relative c''' {
  \global
  \transposition bes
  
  r2 r4 r8 e ~ 
  e4. fis8 ~ fis4. e8 ~
  e4. d8 ~ d4. e8 ~ 
  e4. d8 ~ d4.  cis8 ~ 
  
  cis4. dis8 ~ dis4. b8 ~
  b4. cis8 ~ cis4. a8
}

tromboneII = \relative c''' {
  \global
  \transposition bes
  
  r2 r4 r8 dis ~ 
  dis4. cis8 ~ cis4. bes8 ~
  bes4. bes8 ~ bes4. c8 ~ 
  c4. c8 ~ c4. a8 ~ 
  
  a4. b8 ~ b4. g8 ~
  g4. a8 ~ a4. f8
  
}

tromboneIII = \relative c''' {
  \global
  \transposition ees
  
  r2 r4 r8 b ~ 
  b4. b8 ~ b4. f8 ~
  f4. f8 ~ f4. a8 ~ 
  a4. a8 ~ a4. fis8 ~ 
  
  fis4. gis8 ~ gis4. e8 ~
  e4. fis8 ~ fis4. d8
  
}

tromboneIV = \relative c''' {
  \global
  \transposition ees
  
  r2 r4 r8 gis ~ 
  gis4. a8 ~ a4. e8 ~
  e4. d8 ~ d4. g8 ~ 
  g4. f8 ~ f4. e8 ~ 
  
  e4. fis8 ~ fis4. d8 ~
  d4. e8 ~ e4. c8
  
}

bass = \relative c {
  \global
  \clef bass
  
  r2 r4 r8 b ~ 
  b2 ~ b8 b4. ~ 
  b2 ~ b8 b4. ~
  b2 ~ b4. b8 ~
  
  b4. e8 ~ e4. a,8 ~
  a4. d8 ~ d4. g,8 ~
  g1
}

guitar = \relative c'' {
  \global
  
}

right = \relative c'' {
  \global
  
}

left = \relative c {
  \global
  \clef bass 
  
}

drum = \drummode {
  \global
  % La percusión continúa aquí.
  
}

chordNames = \chordmode {
  \global
  % Los acordes continúan aquí.
  
}

trumpetIPart = \new Staff \with {
  instrumentName = "Tp.I"
  midiInstrument = "soprano sax"
} \trumpetI

trumpetIIPart = \new Staff \with {
  instrumentName = "Tp.II"
  midiInstrument = "soprano sax"
} \trumpetII

trumpetIIIPart = \new Staff \with {
  instrumentName = "Tp.III"
  midiInstrument = "alto sax"
} \trumpetIII

trumpetIVPart = \new Staff \with {
  instrumentName = "Tp.IV"
  midiInstrument = "alto sax"
} \trumpetIV

altoSaxIPart = \new Staff \with {
  instrumentName = "AS.I"
  midiInstrument = "alto sax"
} \altoSaxI

altoSaxIIPart = \new Staff \with {
  instrumentName = "AS.II"
  midiInstrument = "alto sax"
} \altoSaxII

tenorSaxIPart = \new Staff \with {
  instrumentName = "TS.I"
  midiInstrument = "tenor sax"
} \tenorSaxI

tenorSaxIIPart = \new Staff \with {
  instrumentName = "TS.II"
  midiInstrument = "tenor sax"
} \tenorSaxII

baritoneSaxPart = \new Staff \with {
  instrumentName = "Bt"
  midiInstrument = "baritone sax"
} \baritoneSax

tromboneIPart = \new Staff \with {
  instrumentName = "Tb.I"
  midiInstrument = "tenor sax"
} \tromboneI

tromboneIIPart = \new Staff \with {
  instrumentName = "Tb.II"
  midiInstrument = "tenor sax"
} \tromboneII

tromboneIIIPart = \new Staff \with {
  instrumentName = "Tb.III"
  midiInstrument = "baritone sax"
} \tromboneIII

tromboneIVPart = \new Staff \with {
  instrumentName = "Tb.IV"
  midiInstrument = "baritone sax"
} \tromboneIV

bassPart = \new Staff \with {
  instrumentName = "Bass"
  midiInstrument = "acoustic bass"
} \bass

guitarPart = \new Staff \with {
  instrumentName = "Guit."
  midiInstrument = "acoustic guitar (steel)"
} \guitar

pianoPart = \new PianoStaff \with {
  instrumentName = "Piano"
  midiInstrument = "acoustic grand"
} <<
  \new Staff = "right" \right
  \new Staff = "left" \left
>>

drumsPart = \new DrumStaff \with {
  \consists "Instrument_name_engraver"
  instrumentName = "Drums"
  midiInstrument = "drums"
} \drum

chordsPart = \new ChordNames \chordNames

\score {
  <<
    
    \new StaffGroup <<
      \transpose bes c \trumpetIPart
      \transpose bes c \trumpetIIPart
      \transpose ees c \trumpetIIIPart
      \transpose ees c \trumpetIVPart
    >>
    
    \new StaffGroup <<
      \transpose bes c \tromboneIPart
      \transpose bes c \tromboneIIPart
      \transpose bes c \tromboneIIIPart
      \transpose bes c \tromboneIVPart
    >>
    
    \new StaffGroup <<
      \transpose ees c \altoSaxIPart
      \transpose ees c \altoSaxIIPart
      \transpose bes c \tenorSaxIPart
      \transpose bes c \tenorSaxIIPart
      \transpose ees c \baritoneSaxPart
    >>
    
    \new StaffGroup <<
      \pianoPart
      \chordsPart
      \guitarPart
      \bassPart
      \drumsPart
    >>
  >>
  \layout { }
  \midi {
    \tempo 2 = 90
  }
  
}
