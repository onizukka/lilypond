\version "2.16.1"
\include "imports/main.ly"


% Instrument Music
% -------------------------------------------------
boneThree = {

  % Para pintar todos los números de compás debajo del pentagrama
  \override Score.BarNumber #'break-visibility = #end-of-line-invisible
  \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0
  
  \key d \major
  
  \partial 1
  r1
  
  \bar "||"
  R1*2
  
  ais1 \f ~
  ais ~
  ais ~
  ais2 r4 r8 cis,
  
  \break
  d r r4 r2
  R1*3
  
  \bar "||"
  R1*8
  
  \break
  r4 \f fis8 fis r b r d
  R1
  r4 cis8 cis r cis r cis
  r8 cis4. c8 cis r4
  
  \break
  r4 b8 b g g r4
  R1
  r4 fis8 fis fis fis fis fis
  d d r4 r2
  
  \break
  R1*2
  r2 g'8 g r g
  r e r d r b4.
  
  r4 r8 g gis gis r4
  R1
  \break
  r4 b8 ais b c b b ~ 
  b4 c8 c b ais4 a8 ~
  
  a4. \> a8 a a r d, \!
  r d4. r2
  \break
  cis'8 \ff cis cis cis cis b r cis
  r b4. cis8 b r4 
  
  R1*2
  d8 \f d r d r d4. ~
  d1
  
  \break
  a8 a r a r a4. ~ 
  a1
  g8 g r g r g4. ~
  g1
  
  r8 cis r cis r4 r8 cis
  r cis r cis ~ cis2
  
  \break
  \bar "||"
  d8 d r d r d r d ~ 
  d1
  cis8 cis r cis ~ cis4 fis,
  cis' c8 b r2
  
  R1*12
  
  \break
  \bar "||"
  R1*16
  
  \bar "||"
  a2. \ff a4 ~
  a2 a2 ~
  a4 ais2.
  b4-. cis2.
  
  \break
  \bar "||"
  \key f \major
  f,4. e8 d cis r c
  r bes' r g r fis4.
  f1 ~ 
  f4 f8 f ~ f4 f8 f
  
  R1*4
  
  \break
  f4. e8 d cis r c
  r bes' r g r fis4.
  f1 ~ 
  f4 f8 f ~ f4 f8 f
  
  R1*4
  
  \break
  \bar "||"
  R1*8
  
  \bar "||"
  e'4-.\f r8 e r4 e4-. 
  r8 e r4 e-. r8 dis
  e4-. r8 e r4 e ~
  \break
  e dis8 e f e r4
  
  bes-. r8 bes r4 bes ~
  bes1
  r2 fis 
  f? e
  
  \break
  c4-.\mp r8 c r4 c4-. 
  r8 c r4 c-. r8 dis
  e4-. r8 e r4 e ~
  e dis8 e f e r4
  
  g4-. a-. bes8 b r c ~
  \break
  c1
  r8 a4. bes4 \< b8 c
  R1
  
  r2 \times 2/3 {fis,8 g gis} \times 2/3 {a bes b \!}
  c1 ~
  c4 d,8 f r2
  
  \bar "|."
}


% Instrument Sheet
% -------------------------------------------------

\header {
  instrument = "Trombone 3 (Eb)"
  title = "Take the A Train"
  composer = "BILLY STRAYHORN - 1941"
  arranger = "Arr. PHILIPPE MARILLIA"
}

\paper {  	
  % distancia entre sistemas
  system-system-spacing #'basic-distance = #18
  
  % distancia entre el título y el primer sistema
  markup-system-spacing #'basic-distance = #20
  
  systems-per-page = 9
}

<<
  \scoreMarkup
  \new Staff << \transpose ees bes \relative c'' \boneThree >>
>>
