\version "2.16.1"
\include "imports/main.ly"


% Instrument Music
% -------------------------------------------------
instrument = {

  % Para pintar todos los números de compás debajo del pentagrama
  \override Score.BarNumber #'break-visibility = #end-of-line-invisible
  \override Score.BarNumber #'direction = #DOWN
  \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0
	
  \key a \minor
        
  R1*4
  \repeat volta 2 {
    R1*6
  }
  \alternative {
    { \break
      R1*2 }
    { R1*2 }
  }
  
  R1*4
  R1*4
  
  \break
  R1*4
  R1*4
  
  r1
  r2 r8 b2.-> \glissando
        
  \key e \minor
  g'2 fis8-- e4-^ g8-> ~
  g2 r8 b,2.-> \glissando
  g'2 fis8 \mordent e4-^ g8-> ~
  g2 r8 g2.->
  
  fis2 dis8-- b4-^ fis'8 \portato ~
  fis8 fis4 \portato g8 \portato ~ g8 fis4 \portato e8 \portato ~
  e1
  r2 r8 b4.-> \glissando
  
  b'2 a8-- g4-^ b-> ~
  b2 r8 b,2.-> \glissando
  b'2 a8 \mordent g4-- b8-> ~
  b2 r8 b2.->
  
  a2 fis8-- dis4-^ a'8 \portato ~
  a8 a4 \portato bes8 \portato ~ bes8 a4 \portato g8 \portato ~
  g1
  r4 r8 g'8 e4 b4
  
  e
  
  
}


% Instrument Sheet
% -------------------------------------------------

\header {
  instrument = "Violin"
}

\paper {  	
  % distancia entre sistemas
  system-system-spacing #'basic-distance = #18
  
  % distancia entre el título y el primer sistema
  markup-system-spacing #'basic-distance = #20
}

<<
  \scoreMarkup
  \new Staff << \transpose ees bes \relative c'' \instrument >>
>>
