\version "2.18.2"
\language "english"

% for chords with up to three alterations, stacked on top of each other
#(define-markup-command (stacked layout props strA strB strC) (string? string? string?)
  (interpret-markup layout props
    (markup 
      #:super strA
      #:fontsize 1.5 "["
      #:fontsize -4 
      #:raise 1.8
      #:column (strB strC) 
      #:fontsize 1.5 "]"
    )
  )
)

chordNameExceptionsMusic = {
	
	%% Acordes mayores
	% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
	
	
	
	
	
	%% Mayores: Acordes de séptima mayor
	% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
	% maj7
	<c e g b>1 ^\markup \super { 
		\hspace #.4 
		\column { \override #'(baseline-skip . 2.8) \override #'(thickness . .2) \triangle ##f }
	}
	% maj7 9(add13)
	<c e g b d' a'>1 ^\markup \super { 
		\hspace #.4 
		\column { \override #'(baseline-skip . 2.8) \override #'(thickness . .2) \triangle ##f }
		\hspace #.3
		\column { "9(add13)"}
	}
	
	
	%% Mayores: Acordes de séptima
	% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
	% 7(b5)
	<c e gf bf>1 ^\markup \super "7(>5)"
	% 7(#5)
	<c e gs bf> ^\markup \super "7(<5)"
	
	%% Mayores 7a: Acordes de novena
	% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
	% 9
	<c e gs bf d' f'> ^\markup \super "9"
	% 7(b9)
	<c e g bf df'>1 ^\markup \super "7(>9)"
	% 7(#9)
	<c e g bf ds'>1 ^\markup \super "7(<9)"
	% (add9)
	<c e g d'> ^\markup \super "(add 9)"
	
	
	%% Mayores 7a: Acordes de oncena
	% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
	% 9(#11)
	<c e g bf d' fs'> ^\markup \super "9(<11)"
	
	
	%% Acordes de trecena
	% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
	% 13
	<c e g bf d' f' a'>1 ^\markup \super "13"
	% 7(add13)
	<c e g bf a'>1 ^\markup \super "7(add 13)"
	% 13(#11)
	<c e g bf d' fs' a'>1 ^\markup \super "13(<11)"
	% 7(b13b9)
	<c e g bf df' f' af'>1 ^\markup \stacked #"7" #">13" #">9"
	% 7(b13b9)
	<c e g bf ds' fs' a'>1 ^\markup \stacked #"13" #"<11" #"<9"
	
	
	
	
	
	
	
	%% Acordes menores
	% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
	
	%% Menores: Disminuidos
	% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
	% Semidisminuido
	<c ef gf bf>1 ^\markup {
		\hspace #.4
		\column { \super \fontsize #1.5 "o" } 
		\hspace #-1.4
		\column { \vspace #.15 \super \fontsize #1.5 "/" }
	}
	
	%% Menores: Acordes con oncena
	% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
	% - 11
	<c ef g bf d' f'>1 ^\markup { "-" \super "11" }
}


% Convert music to list and prepend to existing exceptions.
chordNameExceptions = #( append (sequential-music-to-chord-exceptions chordNameExceptionsMusic #t) ignatzekExceptions)

#(define (chordRootNamer pitch majmin)	;majmin is a required argument for "chordNamer", but not used here
 	(let* ((alt (ly:pitch-alteration pitch)))
 		(make-line-markup
 		 (list
 		  (make-simple-markup (vector-ref #("C" "D" "E" "F" "G" "A" "B") (ly:pitch-notename pitch)))
 		  (if (= alt 0) ;alteration ?
 		      (markup "")		; do nothing
 		      (if (= alt FLAT)	; flat or sharp
 		          (markup ">")
 		          (markup "<")
 		          ))))))

% modify the default ChordNames context
\layout {
	\context { 
		\ChordNames
		chordRootNamer = #chordRootNamer	% update the chord names
		chordNameExceptions = #chordNameExceptions	% update the chord exceptions
		minorChordModifier = "-"
		
		\override ChordName.font-size = #2.5
		\override ChordName.font-name = #"lilyjazz-chord"  % use the custom font for displaying the chords
	}
}