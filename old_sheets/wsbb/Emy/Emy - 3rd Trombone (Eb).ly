\version "2.16.1"
% \include "imports/main.ly"

% Instrument Music
% -------------------------------------------------

tromboneThree = {
  \jazzOn
  \compressFullBarRests
  
  \clef bass
  
  g,1 ~\pp \<
  g \f \>
  r4\!
}


% Instrument Sheet
% -------------------------------------------------
%{
\header {
  instrument = "3rd Trombone"
}

\paper {  	
  % distancia entre sistemas
  system-system-spacing #'basic-distance = #23
  
  % distancia entre el título y el primer sistema
  markup-system-spacing #'basic-distance = #25
}

<<
  \scoreMarkup
  \new Staff << \transpose c f \transpose ees c' \relative c'' \tromboneThree >>
>>
%}