\version "2.16.1"
% \include "imports/main.ly"

% Instrument Music
% -------------------------------------------------

tromboneFour = {
  \jazzOn
  \compressFullBarRests
  
  \clef bass
  
  c,,,1 ~ \pp \<
  c \f \>
  r4\!
}


% Instrument Sheet
% -------------------------------------------------
%{
\header {
  instrument = "4th Trombone"
}

\paper {  	
  % distancia entre sistemas
  system-system-spacing #'basic-distance = #23
  
  % distancia entre el título y el primer sistema
  markup-system-spacing #'basic-distance = #25
}

<<
  \scoreMarkup
  % \new Staff << \relative c' \tromboneThree >>
  \new Staff << \transpose c f \transpose ees c' \relative c' \tromboneFour >>
>>
%}