\version "2.16.1"
% \include "imports/main.ly"

% Instrument Music
% -------------------------------------------------
altoTwo = {
  \jazzOn
  \compressFullBarRests
}


% Instrument Sheet
% -------------------------------------------------
%{
\header {
  instrument = "2nd Alto"
}

\paper {  	
  % distancia entre sistemas
  system-system-spacing #'basic-distance = #22
  
  % distancia entre el título y el primer sistema
  markup-system-spacing #'basic-distance = #25
}

<<
  \scoreMarkup
  \new Staff << \transpose c f \relative c'' \altoTwo >> 
>>
%}