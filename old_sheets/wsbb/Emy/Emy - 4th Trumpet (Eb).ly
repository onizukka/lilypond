\version "2.16.1"
% \include "imports/main.ly"

% Instrument Music
% -------------------------------------------------

trumpetFour = {
  \jazzOn
  \compressFullBarRests
  
  c1 ~ \pp \<
  c \f \>
  r4\!
}


% Instrument Sheet
% -------------------------------------------------
%{
\header {
  instrument = "4th Trumpet"
}

\paper {  	
  % distancia entre sistemas
  system-system-spacing #'basic-distance = #17
  
  % distancia entre el título y el primer sistema
  markup-system-spacing #'basic-distance = #25
}

<<
  \scoreMarkup
  \new Staff << \transpose c f \transpose ees bes \relative c'' \trumpetFour >>
>>
%}