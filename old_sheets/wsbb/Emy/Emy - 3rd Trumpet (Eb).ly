\version "2.16.1"
% \include "imports/main.ly"

% Instrument Music
% -------------------------------------------------

trumpetThree = {
  \jazzOn
  \compressFullBarRests
  
  e1 ~ \pp \<
  e \f \>
  r4\!
}


% Instrument Sheet
% -------------------------------------------------
%{
\header {
  instrument = "3rd Trumpet"
}

\paper {  	
  % distancia entre sistemas
  system-system-spacing #'basic-distance = #17
  
  % distancia entre el título y el primer sistema
  markup-system-spacing #'basic-distance = #25
}

<<
  \scoreMarkup
  \new Staff << \transpose c f \transpose ees bes \relative c'' \trumpetThree >>
>>
%}