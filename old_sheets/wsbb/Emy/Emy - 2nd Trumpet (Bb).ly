\version "2.16.1"
% \include "imports/main.ly"

% Instrument Music
% -------------------------------------------------

trumpetTwo = {
  \jazzOn
  \compressFullBarRests
  
  f1 ~ \pp \<
  f \f \>
  r4\!
}


% Instrument Sheet
% -------------------------------------------------
%{
\header {
  instrument = "2nd Trumpet"
}

\paper {  	
  % distancia entre sistemas
  system-system-spacing #'basic-distance = #17
  
  % distancia entre el título y el primer sistema
  markup-system-spacing #'basic-distance = #25
}

<<
  \scoreMarkup
  \new Staff << \transpose c f \relative c'' \trumpetTwo >>
>>
%}