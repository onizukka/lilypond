\version "2.16.1"
% \include "imports/main.ly"

% Instrument Music
% -------------------------------------------------
tenorOne = {
  \jazzOn
  \compressFullBarRests
  
}


% Instrument Sheet
% -------------------------------------------------

%{
\header {
  instrument = "1st Tenor"
}

\paper {  	
  % distancia entre sistemas
  system-system-spacing #'basic-distance = #21
  
  % distancia entre el título y el primer sistema
  markup-system-spacing #'basic-distance = #25
}

<<
  \scoreMarkup 
  \new Staff << \transpose c f \relative c'' \tenorOne >> 
>>
%}