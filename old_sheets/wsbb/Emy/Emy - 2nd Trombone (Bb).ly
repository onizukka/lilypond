\version "2.16.1"
% \include "imports/main.ly"

% Instrument Music
% -------------------------------------------------

tromboneTwo = {
  \jazzOn
  \compressFullBarRests
  
  \clef bass
  
  a,1 ~ \pp \< 
  a \f \>
  r4\!
}


% Instrument Sheet
% -------------------------------------------------
%{
\header {
  instrument = "2nd Trombone"
}

\paper {  	
  % distancia entre sistemas
  system-system-spacing #'basic-distance = #23
  
  % distancia entre el título y el primer sistema
  markup-system-spacing #'basic-distance = #25
}

<<
  \scoreMarkup
  \new Staff << \transpose c f \transpose bes c' \relative c'' \tromboneTwo >>
>>
%}