\version "2.16.1"
% \include "imports/main.ly"

% Instrument Music
% -------------------------------------------------

trumpetOne = {
  \jazzOn
  \compressFullBarRests
  
  a'1 ~ \pp \<
  a \f \>
  r4\!
}


% Instrument Sheet
% -------------------------------------------------
%{
\header {
  instrument = "1st Trumpet"
}

\paper {  	
  % distancia entre sistemas
  system-system-spacing #'basic-distance = #17
  
  % distancia entre el título y el primer sistema
  markup-system-spacing #'basic-distance = #25
}

<<
  \scoreMarkup
  \new Staff << \transpose c f \relative c'' \trumpetOne >>
>>
%}