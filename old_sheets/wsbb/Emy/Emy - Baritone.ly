\version "2.16.1"
% \include "imports/main.ly"

% Instrument Music
% -------------------------------------------------
baritone = {
  \jazzOn
  \compressFullBarRests
  
  \clef bass
  
  c,,,1 ~ \pp \<
  c \f \>
  r4\!
}


% Instrument Sheet
% -------------------------------------------------
%{
\header {
  instrument = "Baritone Sax"
}

\paper {  	
  % distancia entre sistemas
  system-system-spacing #'basic-distance = #21
  
  % distancia entre el título y el primer sistema
  markup-system-spacing #'basic-distance = #25
}

<<
  \scoreMarkup
  \new Staff << \transpose c f \relative c'' \baritone >>
>>
%}