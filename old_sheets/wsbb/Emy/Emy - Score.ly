\version "2.16.1"

\include "imports/main.ly"

\include "Emy - 1st Alto.ly"
\include "Emy - 2nd Alto.ly"
\include "Emy - 1st Tenor.ly"
\include "Emy - 2nd Tenor.ly"
\include "Emy - Baritone.ly"

\include "Emy - 1st Trumpet (Bb).ly"
\include "Emy - 2nd Trumpet (Bb).ly"
\include "Emy - 3rd Trumpet (Eb).ly"
\include "Emy - 4th Trumpet (Eb).ly"

\include "Emy - 1st Trombone (Bb).ly"
\include "Emy - 2nd Trombone (Bb).ly"
\include "Emy - 3rd Trombone (Eb).ly"
\include "Emy - 4th Trombone (Eb).ly"

\include "Emy - Piano - Guitar.ly"
\include "Emy - Bass.ly"

\paper {
}

\score {
  <<
    \new StaffGroup <<
      \new Staff \with {midiInstrument = #"alto sax"
                        midiMinimumVolume = 0.3 
                        midiMaximumVolume = 1} << \relative c'' \altoOne >>
      \new Staff \with {midiInstrument = #"alto sax"
                        midiMinimumVolume = 0.1 
                        midiMaximumVolume = 0.8} << \relative c'' \altoTwo >>
      \new Staff \with {midiInstrument = #"tenor sax"
                        midiMinimumVolume = 0.3 
                        midiMaximumVolume = 1} << \relative c'' \tenorOne >>
      \new Staff \with {midiInstrument = #"tenor sax"
                        midiMinimumVolume = 0.1 
                        midiMaximumVolume = 0.8} << \relative c'' \tenorTwo >>
      \new Staff \with {midiInstrument = #"baritone sax"
                        midiMinimumVolume = 0.2 
                        midiMaximumVolume = 0.9} << \relative c'' \baritone >>
    >>
    \new StaffGroup <<
      \new Staff \with {midiInstrument = #"soprano sax"
                        midiMinimumVolume = 0.3 
                        midiMaximumVolume = 1} << \relative c'' \trumpetOne >>
      \new Staff \with {midiInstrument = #"soprano sax"
                        midiMinimumVolume = 0.1 
                        midiMaximumVolume = 0.8} << \relative c'' \trumpetTwo >>
      \new Staff \with {midiInstrument = #"alto sax"
                        midiMinimumVolume = 0.1 
                        midiMaximumVolume = 0.8} << \relative c'' \trumpetThree >>
      \new Staff \with {midiInstrument = #"alto sax"
                        midiMinimumVolume = 0.1 
                        midiMaximumVolume = 0.8} << \relative c'' \trumpetFour >>
    >>
    \new StaffGroup <<
      \new Staff \with {midiInstrument = #"tenor sax"
                        midiMinimumVolume = 0.3 
                        midiMaximumVolume = 1} << \relative c'' \tromboneOne >>
      \new Staff \with {midiInstrument = #"tenor sax"
                        midiMinimumVolume = 0.1 
                        midiMaximumVolume = 0.8} << \relative c'' \tromboneTwo >>
      \new Staff \with {midiInstrument = #"baritone sax"
                        midiMinimumVolume = 0.1 
                        midiMaximumVolume = 0.8} << \relative c'' \tromboneThree >>
      \new Staff \with {midiInstrument = #"baritone sax"
                        midiMinimumVolume = 0.1 
                        midiMaximumVolume = 0.8} << \relative c'' \tromboneFour >>
    >>
    \new StaffGroup <<
      \new PianoStaff << 
        \new Staff \relative c'' \rightHand 
        \scoreChords
        \new Staff \relative c' \leftHand 
      >>
      \new Staff \with {midiInstrument = #"electric bass (finger)"} << \relative c'' \bass >>
    >>
  >>
  
  \midi {}
  \layout {}
}