\version "2.16.1"
% \include "imports/main.ly"

% Instrument Music
% -------------------------------------------------
rightHand = {
  
}

leftHand = {
  
}


% Instrument Sheet
% -------------------------------------------------
%{
\header {
  instrument = "Piano - Guitar"
}

\paper {  	
  % distancia entre sistemas
  system-system-spacing #'basic-distance = #18
  
  % distancia entre el título y el primer sistema
  markup-system-spacing #'basic-distance = #25
  
  bottom-margin = 30
  max-systems-per-page = 5
}

<< 
  \scoreMarkup
  \new PianoStaff << 
     \new Staff \transpose c f \relative c'' \rightHand 
     \transpose c f \scoreChords
     \new Staff \transpose c f \relative c' \leftHand 
  >>
>> 
%}