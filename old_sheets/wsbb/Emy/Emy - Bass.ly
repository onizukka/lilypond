\version "2.16.1"
% \include "imports/main.ly"

% Instrument Music
% -------------------------------------------------
bass = {
  \jazzOn
  \clef bass
}


% Instrument Sheet
% -------------------------------------------------
%{
\header {
  instrument = "Bass"
}

\paper {  	
  % distancia entre sistemas
  system-system-spacing #'basic-distance = #17
  
  % distancia entre el título y el primer sistema
  markup-system-spacing #'basic-distance = #25
  
  page-count = 2
}

<< 
  \scoreMarkup
  \new Staff \transpose c f \relative c \bass
>> 
%}