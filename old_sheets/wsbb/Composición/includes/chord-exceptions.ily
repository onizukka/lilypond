chordNameExceptionsMusic = {
	
	% maj7
	<c e g b>1 ^\markup \super { 
		\hspace #.4 
		\column { \override #'(baseline-skip . 2.8) \override #'(thickness . .2) \triangle ##f }
	}
	
	% 7(b5)
	<c e gf bf>1 ^\markup \super { 
		\hspace #.4
		\column { "7" }
		\hspace #.3
		\column { "(" }
		\hspace #.1
		\column { \vspace #-.2 \tiny \jazzglyph #"accidentals.flatjazz" }
		\column { "5)" }
	}
	
	% 7(#5)
	<c e gs bf> ^\markup \super { 
		\hspace #.4
		\column { "7" }
		\hspace #.3
		\column { "(" }
		\hspace #.1
		\column { \vspace #-.2 \tiny \jazzglyph #"accidentals.sharpjazz" }
		\column { "5)" }
	}
	
	% 7(b9)
	<c e g bf df'>1 ^\markup \super { 
		\hspace #.4
		\column { "7" }
		\hspace #.3
		\column { "(" }
		\hspace #.1
		\column { \vspace #-.2 \tiny \jazzglyph #"accidentals.flatjazz" }
		\column { "9)" }
	}
	
	% 7(#9)
	<c e g bf ds'>1 ^\markup \super { 
		\hspace #.4
		\column { "7" }
		\hspace #.3
		\column { "(" }
		\hspace #.1
		\column { \vspace #-.2 \tiny \jazzglyph #"accidentals.sharpjazz" }
		\column { "9)" }
	}
	
	% 7(add13)
	<c e g bf a'>1 ^\markup \super { 
		\hspace #.4
		\column { "7" }
		\hspace #.3
		\column { "(add13)" }
	}
	
	% maj9(add13)
	<c e g b d' a'>1 ^\markup \super { 
		\hspace #.4 
		\column { \override #'(baseline-skip . 2.8) \override #'(thickness . .2) \triangle ##f }
		\hspace #.3
		\column { "9(add13)"}
	}
	
	% 9(#11)
	<c e g bf d' fs'> ^\markup \super { 
		\hspace #.4
		\column { "9" }
		\hspace #.3
		\column { "(" }
		\hspace #.1
		\column { \vspace #-.2 \tiny \jazzglyph #"accidentals.sharpjazz" }
		\column { "11)" }
	}
}


% Convert music to list and prepend to existing exceptions.
chordNameExceptions = #( append
  ( sequential-music-to-chord-exceptions chordNameExceptionsMusic #t)
  ignatzekExceptions)