\version "2.16.1"
\include "imports/main.ly"

% Instrument Music
% -------------------------------------------------
bass = {
  
  \jazzOn
  \key c \major
  \clef bass
  
  \repeat percent 2 { c4. c8 c2 }
  
  c4. c8 c2 
  b4. b8 b2 
  bes4. bes8 bes2 
  
  \break
  a4. a8 a a b cis
  d4. d8 d2 
  c4. c8 c2 
  b4. b8 b4 g
  c4. c8 c g a b
  
  \break
  c4. c8 c2
  c4. c8 c4. c8
  f4. c8 c2 
  d4. d8 d d e f
  
  \break
  g4. g,8 g4. g'8
  f4. g,8 g4 g
  c4. c8 c2
  c4. g'8 g g g g
  
  \break
  c,4. c8 c2
  b4. b8 b2 
  bes4. bes8 bes2
  a4. a8 a a b cis
  
  \break
  d4. d8 d2 
  c4. c8 c2 
  b4. b8 b4 g
  c4. c8 c4 g8 g
  
  \break
  c4. c8 c2
  c4. c8 c4. c8
  f4. f8 f2 
  d4. d8 d4 aes
  
  \break
  g4. g8 g4. g'8
  g,4. g8 g4 g
  c4. c8 c2
  c4. g'8 g g g g
  
  \break
  c,4. g'8 g c, ~ c c
  c4. g'8 g c, c c
  f4. c8 c c ~ c f
  f4. f8 f f e e
  
  \break
  d4. d8 d d e f
  g4. g8 f f f f
  e4. e8 e e ~ e e
  a4. a8 a a a a
  
  \break
  d,4. d8 d d e f
  g g ~ g g g g g g
  c,4. c8 c c ~ c c
  c4. g'8 g g g g
  
  \break
  c,4. c8 c2 
  b4. b8 b2 
  bes4. bes8 bes2 
  a4. a8 a a b cis
  
  \break
  d4. d8 d2 
  c4. c8 c2 
  b4. b8 b4 g
  c4. c8 c4. g8
  
  \break
  c4. c8 c2
  c4. c8 c4. c8
  f4. f8 f2 
  d4. d8 d4 aes
  
  \break
  g4. g8 g4. g'8
  g,4. g8 g4 g
  c4. c8 c2
  c4. g8 g g g g
  
  \break
  c4. c8 c2
  c4. c8 c4. c8
  f4. f8 f4 c8 c
  f4. f8 f f e e
  
  \break
  d4. d8 d e f d
  g4. g8 f f f4
  e4. e8 e f g e
  a4. a8 a a a a
  
  \break
  d,4.\rit d8 d d e f
  g2\! g,\fermata
  \mark \markup \boxed "arco"
  c1 ~ 
  c4 g2.^>\<
  
  \break
  c1\! ~ 
  c 
  r4 d2.^>\<
  r4\ff ees^- d^- des^- 
  
  c1\fermata
}


% Instrument Sheet
% -------------------------------------------------
\header {
  instrument = "Bass"
}

\paper {  	
  % distancia entre sistemas
  system-system-spacing #'basic-distance = #17
  
  % distancia entre el título y el primer sistema
  markup-system-spacing #'basic-distance = #25
  
  page-count = 2
}

<< 
  \scoreMarkup
  \new Staff \transpose c f \relative c \bass
>> 