scoreMarkup = \new Staff \with {
   \RemoveEmptyStaves
}
{  
  \override Staff.VerticalAxisGroup #'remove-first = ##t
  
  s1*2
  \mark \markup \boxindex A
  
  \bar "||"
  s1*16
  
  \mark \markup \boxindex B
  \bar "||"
  s1*16
  
  \mark \markup \boxindex C
  \bar "||"
  s1*12
  
  \mark \markup \boxindex D
  \bar "||"
  s1*16
  
  \mark \markup \boxindex E
  \bar "||"
  s1*10
  
  \once \override Score.RehearsalMark #'X-offset = #1.5
  \mark \markup \boxed "directed slowly"
  s1*2
  
  \once \override Score.RehearsalMark #'X-offset = #1.5
  \mark \markup \boxed "brighter tempo"
  \mark \markup \boxindex F
  \bar "||"
  s1*5
  
  \bar "|."
}