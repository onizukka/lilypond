\version "2.16.1"
\include "imports/main.ly"

% Instrument Music
% -------------------------------------------------
rightHand = {
  
  \key c \major
  \repeat percent 2 { r8 g( c g e' g, c g) }
  
  r8 g( c g e' g, c g)
  r8 g( c g e' g, d' g,)
  \break
  r8 g( d' g, e' g, d' g,)
  r8 g( cis g e' g, cis g)
  
  r8 a( d a f' a, d a)
  r8 a( d a f' a, d a)
  \break
  r8 g( d' g, f' g, d' g,)
  r8 g( c g e' g, c g)
  
  r8 c( g' c, g' b, g' g,)
  r8 bes( g' bes, e g, bes e,)
  \break
  r8 c'( f a, f' a, e' g,)
  r8 aes( f' aes, e' aes, d f,)
  
  r8 g( d' g, c g g' g,)
  r8 g( d' g, b f' b, g)
  \break
  r8 a( d a f a f d')
  \override Glissando #'style = #'zigzag
  r8 g,( e' c g'4 g,,)\glissando
  
  e''^^ \rs \rs \rs
  \rs \rs \rs \rs
  
  \break
  \repeat unfold 16 { \rs }
  
  \break
  \repeat unfold 16 { \rs }
  
  \break
  \repeat unfold 16 { \rs }
  
  \break
  \repeat unfold 16 { \rs }
  
  \break
  \repeat unfold 16 { \rs }
  
  
  
  \break
  \repeat unfold 16 { \rs }
  
  \break
  \repeat unfold 8 { \rs }
  \ottava #2
  r4 g'8^\markup \boxed "like bells" e' e4 g,
  r4 g8 e' e4 g,
  
  \break
  r4 e'8 d g2 ~
  g4 \times 2/3 {e8 f g} a,2
  r4 a8 f' f4 a,
  r4 a8 f'16 e f4 a,
  
  \break
  g4. f'8 f2 ~
  f4 \times 2/3 {e8 f g} c,2
  \ottava 0
  \repeat unfold 8 { \rs }
  
  \break
  \repeat unfold 16 { \rs }
  
  
  \break
  \repeat unfold 16 { \rs }
  
  \break
  \repeat unfold 16 { \rs }
  
  \break
  \repeat unfold 12 { \rs }
  g,,2 r2\fermata
  
  \break
  r4 f( a, d
  <<
    { c4.) r8 r2 }
    \new CueVoice {
      { \stemUp s2 \times 2/3 {e'4^\markup \normalsize "(brass)" f g} }
    }
  >>
  <<
    { <c, e>2 <b ees> } \\
    { g2 ~ g8 a g fis }
  >>
  <f bes d>2 <e bes' des>2 
  
  \break
  R1
  \override NoteHead #'style = #'slash
  r4 f^- f^- f^-
  f1\fermata
  
  
}

leftHand = {
  \key c \major
  \repeat percent 2 { c4 e e e }
  
  c4 e e e 
  b e e e
  
  \break
  bes e e e
  a, e' e e
  d f f f
  c f f f
  
  \break
  b, f' f f
  c e e e
  c2 b
  bes bes
  
  \break
  a4 f' f f
  aes, f' f f
  g, e' e e
  f, d' d d
  
  \break
  c f c c
  c e c r
  \clef bass
  c,,4. c8 c2
  b4. b8 b2
  
  \break
  bes4. bes8 bes2
  a4. a8 a a b cis
  d4. d8 d2
  c4. c8 c2
  
  \break
  b4. b8 b4 g
  c4. c8 c4 g8 g
  c4. c8 c2
  c4. c8 c4. c8
  
  \break
  f4. f8 f2
  d4. d8 d4 aes
  g4. g8 g4. g'8
  g,4. g8 g4. g8
  
  \break
  c4. c8 c2
  c4. g'8 g8 g g g
  c,4. g'8 g8 c, ~ c c
  c4. g'8 g8 c, c c
  
  \break
  f4. c8 c8 c ~ c f
  f4. f8 f8 f e e
  d4. d8 d8 d e f
  g4. g8 f8 f f f
  
  \break
  e4. e8 e8 e ~ e e
  a4. a8 a8 a a a
  d,4. d8 d8 d e f
  g8 g ~ g g g g g g
  
  \break
  c,4. c8 c c ~ c c
  c4. g'8 g g g g
  c,4. c8 c2
  b4. b8 b2
  
  \break
  bes4. bes8 bes2
  a4. a8 a a b cis
  d4. d8 d2
  c4. c8 c2
  
  \break
  b4. b8 b4 g
  c4. c8 c4 g
  c4. c8 c2
  c4. c8 c4. c8
  
  f4. f8 f2
  d4. d8 d4 aes
  g4. g8 g4. g'8
  g,4. g8 g4. g8
  
  \break
  c4. c8 c2
  c4. g'8 g8 g g g
  c,4. c8 c2
  c4. c8 c4. c8
  
  \break
  f4. f8 f4 c8 c
  f4. f8 f f e e
  d4. d8 d e f d
  g4. g8 f8 f f4
  
  \break
  e4. e8 e f g e
  a4. a8 a a a a
  d,4.\rit d8 d d e f
  g2\! <g, g'>\fermata
  
  <c c'>1 ~ 
  <c c'>4 <g g'>2.^>
  <c c'>1\f
  <c c'>1 
  
  \break
  r4\f <d d'>2.
  r4 ees d des
  c1\fermata
}


% Instrument Sheet
% -------------------------------------------------
%{
\header {
  instrument = "Piano - Guitar"
}

\paper {  	
  % distancia entre sistemas
  system-system-spacing #'basic-distance = #18
  
  % distancia entre el título y el primer sistema
  markup-system-spacing #'basic-distance = #25
  
  bottom-margin = 30
  max-systems-per-page = 5
}

<< 
  \scoreMarkup
  \new PianoStaff << 
     \new Staff \transpose c f \relative c'' \rightHand 
     \transpose c f \scoreChords
     \new Staff \transpose c f \relative c' \leftHand 
  >>
>> 
%}