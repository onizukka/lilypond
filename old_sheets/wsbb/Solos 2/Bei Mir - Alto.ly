\version "2.16.1"
\include "imports/main.ly"


% Instrument Music
% -------------------------------------------------
instrument = {

  % Para pintar todos los números de compás debajo del pentagrama
  \override Score.BarNumber #'break-visibility = #end-of-line-invisible
  \override Score.BarNumber #'direction = #DOWN
  \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0

  \key a \minor
  
  c1
  a4 fis8 e a c r4
  e,8 e r e r f4 e8
  dis e f8 e r2
  
  \break
  r4 e8 e f e r4
  dis8 e f e r a r a ~
  a2 f
  a4 c8 e r2
  
  \break
  \bar "||"
  \appoggiatura {dis16} e4 c8 a r4 c8 d
  e4 c8 a r4 c8 d
  ees r c r a r fis r
  e dis e a ~ a4. c8
  
  \break
  b4 ais8 b r4 r8 e
  b4 ais8 b r c b a ~ 
  a2 a2
  c4 d8 e r2
  
  \bar "|."
}


% Instrument Sheet
% -------------------------------------------------

\header {
  instrument = "Alto Sax"
  title = "Bei mir bist du schön"
  subtitle = "(Solo)"
}

\paper {  	
  % distancia entre sistemas
  system-system-spacing #'basic-distance = #18
  
  % distancia entre el título y el primer sistema
  markup-system-spacing #'basic-distance = #20
}

<<
  \scoreMarkup
  \scoreChords
  \new Staff << \transpose ees c' \relative c'' \instrument >>
>>
