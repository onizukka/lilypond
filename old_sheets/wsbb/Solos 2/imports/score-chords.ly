scoreChords = \chords {
  fis1:m
  fis1:m
  fis1:m
  fis1:m
  
  cis1:7
  cis1:7
  fis2:m b2:m
  fis2:m cis2:7
  
  fis1:m
  fis1:m
  fis1:m
  fis1:m
  
  cis1:7
  cis1:7
  fis2:m b2:m
  fis2:m cis2:7
}