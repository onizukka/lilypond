\version "2.18.2"
\language "english"


% Descripción de funcionalidades
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% PLUGINS
% - \eolMark:							mark al final de línea
% - \bendBefore: 					solución al bendBefore (i.e. dis8 \bendBefore)
% - \rit: 								crea una porción de ritardando que debe terminar como las dinámicas, con un \!
% - \improOn, \improOff:	negra de improvisación
% - \emptyStaff:					Imprime un pentagrama vacío
% - \emptyBar:						Imprime un compás invisible (útil para indentados)
% - \glissBefore:					muestra un glissando hacia la nota
% - \shortGlissBefore:		muestra un glissando corto hacia la nota
% - \jazzTurn:						muestra el simbolo equivalente a un turn en jazz
% - \subp:								muestra "sp", subito piano
% - \ap:									(accidental padding), separa un poco una alteración de la siguiente nota
% 												(útil al usar parenthesize)
% - \optOttava:						octavaje opcional

% LAYOUT
% - El aspecto de los silencios multicompás es como los de las partituras de jazz
% - Margen extra de las articulaciones para evitar la colisión con las ligaduras
% - Glissandos bonitos

% PAPER
% - Se imprime el título, instrumento y número de página en todas las páginas
% - Solo se debe tocar el page-count (si eso). Sale bonito tal cual.

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Changelog de funcionalidades
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Plugins
% 23-09-85: Añadida función \emptyStave para imprimir un pentagrama vacío.
% 					En algunas partituras hay un pentagrama vacío antes del coda (lo cual mola bastante, por cierto)
% 05-09-14: Añadido a la función \emptyStave el esconder la armadura de la clave, que no estaba.
% 10-09-14: Mejorada la función \bendBefore
% 					Ahora es genérica, y se puede definir cualquier texto en un lateral que haga las funciones de ornamento
% 16-09-14: Mejorada la función para imprimir barras de improvisación.
% 					Antes se llamaba al comando \rs para pintar una barra que ocupaba una negra
% 					Se reemplaza \rs por \restImproOn y \restImproOff, siendo esta opción mucho más flexible.
% 					Cuando se activa \restImproOn, todo silencio se convierte en barra de improvisación, respetando su duración.
% 20-09-14: Mejorada la función para imprimir barras de improvisación.
% 					Se reemplaza \restImproOn y \restImproOff por \improOn y \improOff
% 					Ahora, también las notas del pentagrama salen como barra de improvisación.
% 					(los comandos \improvisationOn e \improvisationOff de lilypond no se llevan bien con LilyJAZZ)
% 					Se ha añadido un grob correspondiente a los slash de la fuente LilyJAZZ en el archivo lily-jazz.ly
% 01-10-14: Plugins
% 					Renombrada la función \emptyStave a \emptyStaff (stave es el plural de staff) y 
% 					corregida (ya no muestra el número de compás).
% 					Recordar que después de usar \emptyStaff se necesita redefinir el 
% 					número de compás con \set Score.currentBarNumber = #xx
% 01-10-14: Añadida la función \emptyBar que oculta un compás entero. Se puede usar para crear un 
% 					efecto de indentación en un pentagrama dado.
% 					Recordar que después de usar \emptyBar se necesita:
% 					- redefinir el número de compás con \set Score.currentBarNumber = #xx
% 					- volver a pintar la clave con \clef
% 					- volver a pintar la armadura con \key
% 15-10-14: Se le ha aumentado el grosor de los bordes de la función \boxed
% 18-10-15: Añadidas funciones de ornamento para antes de la nota:
% 					- \glissBefore: para mostrar un glissando antes de una nota
% 					- \shortGlissBefore: para mostrar un glissando corto antes de una nota
% 					- \jazzTurn: para mostrar el turn típico trompetero de jazz
% 24-10-15: Añadida funcion \subp para mostrar subito piano "sp"
% 27-10-15: Añadida funcion \ap (de accidental padding) para separar un poco el accidental de la siguiente nota
% 					al usar un paréntesis con \parenthesize, ya que se solapan el uno al otro
% 27-10-15: Añadida funcion \optOttava para imprimir octavaje opcional
% 					También he aprovechado para cambiar la fuente del texto "8va" que utilizaba Feta por defecto
% 28-10-15: Mejorada la función glissBefore y shortGlissBefore.
% 					Ahora se le pasa una lista #'(r x y) en la que "r" es la rotacion, "x" la posicion horizontal 
%						e "y" la positión vertical.

% Markup
% 23-09-85: Añadidas guías para correcta posición y markup de la indicación de tempo
% 27-10-15: Guía para posición de la indicación de tempo mejorada
% 30-10-15: El texto que se coloque sobre silencios de multicompás POR FIN coge la fuente LilyJAZZ
% 31-10-15: El texto de letras (lyricmode) ya coge la fuente LilyJAZZ

% Layout
% 23-09-85: Ahora las articulaciones van por dentro de las ligaduras. Parece que es la mejor solución
% 04-09-14: Los creciendos y decreciendos tienen la línea un poco más gruesa
% 08-09-14: Ahora no se muestra la cancelación de accidentales al haber un cambio de armadura
% 15-09-14: Añadido un poco más de margen a los reguladores
% 20-09-14: Comentada la línea que oculta la cancelación de accidentales al haber cambio de armadura.
% 					Realmente, la cancelación es muy bienvenida.

% Chords
% 09-09-14: Añadida sentencia para que los acordes menores salgan con un guión en vez de con una M minúscula
% 26-10-14: Añadido el include "chord-exception.ily" que recoge una mejor notación de los acordes.
% 					Actualmente, recoge los acordes maj7, 7(b5), 7(#5), 7(b9), 7(#9), 7(add13), maj9(add13), 9(#11)
% 					Se irá añadiendo acordes a menudo.

% Global
% 15-10-14: Todas las instrucciones que se colocaban justo antes de empezar la música están ahora
% 					recogidas en una función \global.
% 					A \global se le ha añadido instrucciones para que agrupe las corcheas de dos en dos
% 					en vez de cuatro en cuatro (el comportamiento por defecto).

% Language
% 15-10-14: Cambiado el lenguaje de entrada de notas del finlandes al inglés
% 					La diferencia es que ahora los sostenidos se escriben "-s" en vez de "-is"
% 					y los bemoles "-f" en vez de "-es".

% Estructura
% 15-10-14: Para poder autoincluir los archivos de instrumento en otros 
% 					(para montar un midi, o un score), se han movido los plugins a un archivo separado
% 					y ahora se utiliza la función \includeOnce, evitando así el problema de las
% 					dependencias circulares. 
% 18-10-15: Mejorada la manera de trabajar con papeles separados y score al mismo tiempo
% 02-11-15: Actualizado a lilyjazz v2
%						Nuevos archivos de fuente para musica y acordes
%						Mucha limpieza en lilyjazz.ily, chords.ily y main.ly

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Dependencias
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% \includeOnce
% Incluye una dependencia solo una vez. Así se pueden incluir los archivos en otros 
% (para montar un score por ejemplo) sin liarla parda con dependencias cíclicas.

%{
includeOnce = #(
	define-void-function (parser location filename) (string?) (
		if (not (defined? (string->symbol filename))) (
			begin (ly:parser-include-string parser (string-concatenate (list "\\include \"" filename "\"")))
			(primitive-eval (list 'define (string->symbol filename) #t))
		)
	)
)
%}

% Tamaño global 
% Se debe incluir antes que lilyjazz. Si no, lilyjazz no funciona.
#(set-global-staff-size 20)

% includes
\include "includes/lilyjazz.ily"
\include "includes/chords.ily"
\include "includes/plugins-1.2.ily"

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~











% Disposición
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Cabecera
\header {
	title = "Star Wars Medley"
	instrument = "Trombone I (Bb)"
	composer = "John Williams"
	arranger = "Arr: Dave Wolpe"
	poet = ""
	tagline = ""
}

% Márgenes, fuentes y distancias del papel
\paper {
	
	% distancia entre la parte superior de la página y el título
	top-markup-spacing #'basic-distance = 5

	% distancia entre el título y el primer sistema
	markup-system-spacing #'basic-distance = 25

	% distancia entre el texto y el primer sistema 
	top-system-spacing #'basic-distance = 15

	% márgen inferior
	bottom-margin = 20
	
	% intentado del primer sistema
	indent = 0
	 
	% número de páginas
	% page-count = 1

	ragged-last-bottom = ##f 

	oddHeaderMarkup = \markup {
	 \on-the-fly #not-first-page \fill-line {
		\fromproperty #'page:page-number-string
		\fromproperty #'header:title
		\fromproperty #'header:instrument
	 }
	}

	evenHeaderMarkup = \markup {
	 \on-the-fly #not-first-page \fill-line {
		\fromproperty #'header:instrument
		\fromproperty #'header:title
		\fromproperty #'page:page-number-string
	 }
	}
}

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~







% Elementos comunes de la partitura
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
	% Oculta el primer pentagrama del markup
	\override Staff.VerticalAxisGroup #'remove-first = ##t

	% Indicación de tiempo
	\once \override Score.RehearsalMark.self-alignment-X = #LEFT
	\once \override Score.RehearsalMark.extra-offset = #'(0 . 3) 
	\mark \tempoMark "Majestic" "4" "=112"
	s1*16
	
	\mark \tempoMark "" "4." "=112"
	\mark \markup \musicglyph #'"scripts.varsegno"
	s1*4*12/8
	
	% \once \override Score.RehearsalMark.X-offset = #5
	\mark \markup \boxed "A"
	s1*8*12/8
	
	\mark \markup \boxed "B"
	s1*6*12/8
	
	\once \override Score.RehearsalMark.self-alignment-X = #RIGHT
	\mark \markup {
		\column { \underline "To Coda" }
		\column { \vspace #-.3 \musicglyph #'"scripts.varcoda" }
	}
	s1*2*12/8
	
	\mark \markup \boxed "C"
	s1*8*12/8
	
	\mark \markup \underline "Slowly"
	s1*3
	s1*5/4
	
	\mark \markup \boxed "D"
	s1*10
	
	\mark \markup \underline "Slower"
	s1*4
	
	\mark \markup \underline "A Tempo"
	s1*8
	s1*2*3/4
	s1
	
	\mark \markup \underline "Slowly"
	s1*2
	
	\mark \markup \underline "Rubato"
	s1*2
	
	\eolMark \markup \underline "(D.S. al CODA)"
	\mark \markup {
		\column { \vspace #-.3 \musicglyph #'"scripts.varcoda" }
		\hspace #1
		\column { \underline "CODA" }
	}
	s1*4*12/8
	
	\mark \markup \boxed "F"
}



% Función musical global
global = {
	% Para pintar todos los números de compás debajo del pentagrama
	% \override Score.BarNumber #'break-visibility = #end-of-line-invisible
	% \override Score.BarNumber #'direction = #DOWN
	% \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

	\stemNeutral
	\compressFullBarRests

	% Arregla el problema de tener el scoreMarkup añadido
	\revert MultiMeasureRest #'staff-position
	\override Voice.Rest #'staff-position = #0	

	\set Timing.beamExceptions = #'()
	\set Timing.baseMoment = #(ly:make-moment 1/4)
	\set Timing.beatStructure = #'(1 1 1 1) 
	
	\override ParenthesesItem.font-size = #0
	\override Glissando.breakable = ##t
  	\override Glissando.after-line-breaking = ##t
}

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Elementos específicos del instrumento
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Cifrado
scoreChords = \chords {
	% c1:m f2:m g2:m7
}

vr = { \once \override Voice.Rest.staff-position = #4 }

% Música
instrumentA = \relative c' {
  
	\global
	\key af \major

	R1
	r2 r4^"Marcato"\ff \times 2/3 {ef8 ef ef}
	af2 g
	f2(-> ef4)-^ r8. ef16
	
	\break
	f2(-> ef4)-^ r8. ef16
	f1(->
	ef1)
	R1
	
	\break
	\repeat percent 2 {r4 f~-> \times 2/3{f8 c c} c4}
	r4 af'?2~-> af8 c,~->
	c4 c \times 2/3 {c8 c c} c4
	
	\break
	df?4 df \times 2/3 {df8 df df} df4
	df4 df \times 2/3 {df8 df df} df4~
	\times 2/3 {df8 df df} df4~ \times 2/3 {df8 df df} \times 2/3 {df df df}
	r2\fermata ef?2\fermata
	
	\break
	\bar "||"
	\key f \major
	\time 12/8
	c1.(\f
	d1.)
	e1.(
	d1.)
	
	\break
	\bar "||"
	c1.(\f
	d2.) r4 c8-. r[ c( c]
	d2.) r4 c8-. r[ c( c]
	d2.) r8[ d16-- d-- d8]-. d4-- d8-.
	
	\break
	c1.(
	d2.) r4 c8-. r[ c( c]
	d2.) r4 c8-. r[ c( c]
	d2.) d2.--
	
	\break
	\bar "||"
	R1*12/8
	r2. g4-- g8-. r4.
	r4. d4-^ r8 r2.
	r2. g4-- g8~-- g g-- g--
	
	\break
	f2.( d)
	r2. d4--\mf\< e8~-- e f( e)-.\ff
	f1.->
	r4. df~-> df8[ df16-- df-- df8]-. ef4.->
	
	\break
	\bar "||"
	f,2.^\markup \underline "(Soli)"-> c'2.->
	bf8 a g f'2.( c4)-^ r8
	d2.(-> c2.)
	r4. f-> \vr r8[ f16-- f-- f8]-. <<{\global f4.->\glissando \hideNotes
	
	\break
	f,4.}\\{\global s4. r4.}>> <<{\global \unHideNotes f'4.~-> f2.~
	f2. r4 f8~-> f4.~
	f2.~ f4. f4-^ r8} \\ {s4. s2. s1. s4.\> s2.\< s4.\!}>>
	gf1.->\fermata 
	
	\break
	\bar "||"
	\time 4/4
	\key c \major
	R1*3
	\time 5/4
	r2. r4 g,(^\markup \underline "(Solo)"\mf 
	
	\break
	\time 4/4
	g8 e' e2~ e8 e
	f8 f16 e d2)~ d8 g,(
	g8 e' e2~ e8 g
	af8 af16 g f2)~ f8 af,(
	
	\break
	af8 af' af2~ af8 af
	b?8 a?16 gs fs2)~ fs8 fs(
	a8 gs16 fs e4~ e8 ds16 e fs?8 e16 fs
	af2 ef4.) ef8(
	
	\break
	af8 g16 f ef4~ ef8 d16 ef f8 ef16 f
	g1)_"Rit."
	\bar "||"
	R1
	r2 r4^\markup \underline "(Lead)"\mf a,
	\break
	\times 2/3 {d4( ef f)} \times 2/3 {d4( ef f)}
	g2 r
	
	\bar "||"
	R1*3
	r2 r4 r8^\markup \underline "(Solo)"\f af,(
	
	\break
	af8 af' af2~ af8 af
	b?8 a?16 gs fs2)~ fs8 fs(
	a8 gs16 fs e4~\> e8 ds16 e fs8 e16 fs
	af2\< ef2)
	
	\break
	\time 3/4
	r4\ff d8( ef16 f g4)
	r8_"Rit." af~-- af[ g]~-- g[ f]--
	\time 4/4
	g2\fermata r2
	
	\bar "||"
	R1
	\break
	r2 r4\fermata g,4^\markup \underline "(Solo at will)"
	\times 2/3 {af8 g f} \times 2/3 {c' bf af} \times 2/3 {ef' d c} \times 2/3 {af' g f}
	c'2~\> c2\fermata\!
	
	\break
	\bar "||"
	\time 12/8
	\key f \major
	f,1.~->\ff
	f2.~f8 r r r4.
	d4-- d8-. r4. d4-- d8-. r4.
	<<{\global ef4-- ef8-. r4. ef2.-> \glissando
	
	\break
	\hideNotes c,4.}\\{\global s2. s2. r4.}>> f'4.~-> f2.~
	f2. r4 f8~-> f4.
	f2. r4 f8~-> f4.
	\break
	R1*12/8
	\set Timing.baseMoment = #(ly:make-moment 1/8)
	\set Timing.beatStructure = #'(3 3 3 3) 
	d4--\mf\< d8~-- d d4-- d4-- d8~-- d d4(--
	f4)-^\ff r8 \times 2/3 {f,8-^ f-^ f-^} f2.\fermata
	
	\bar "|."
}

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

\book {
	\paper { 
		% Descomentar para score
		% #(set-paper-size "a4" 'landscape)
		% indent = 20
		% systems-per-page = 2
	}
	
	\score {
		% Descomentar para midi
		% \midi { \tempo 4 = 110 }
		
		% ATENCIÓN
		% NO USAR layout-set-staff-size O SE ROMPE lilyjazz!!!
		\layout { }
		
		\transpose bf c''
		<<
			\scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\new Staff \with { instrumentName = "" midiInstrument = #"" } { \instrumentA }
			>>
		>>
	}
}



%{
\book {
	\header { instrument = "" }
   \bookOutputName ""
	\score {
		<<
			\scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\new Staff { \transpose bf c' \instrumentA }
			>>
		>>
	}
}

\book {
	\header { instrument = "" }
   \bookOutputName ""
	\score {
		<<
			\scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\new Staff { \transpose ef c' \instrumentB }
			>>
		>>
	}
}
%}

