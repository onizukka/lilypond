\version "2.12.3"

\header {
	title = "Feeling Good"
	composer = "Anthony Newley, Leslie Bricusse"
}

solista = \new Voice = "solista"
{
   \set Score.skipBars = ##t
   \relative c''
   {
      % ------------------------------------------------- %
      \time 4/4
      \key g \minor
      % ------------------------------------------------- %
      
      r4 g ~ \times 2/3 {g8 bes c} d4
      r8 d \times 2/3 {g, bes c} d2
      r4 g, ~ \times 2/3 {g8 bes c} d4
      r8 d \times 2/3 {g, bes c} d2
      
      r8 g bes, f'32 d c bes g8 bes ~ bes c16 d
      r8. g,16 \times 2/3 {g8 g g} bes2
      r4 \times 2/3 {g8 g bes} c2
      r4 \times 2/3 {bes8 bes c} d2
      r4 bes16 c d f g2
      r4 \times 2/3 {d8 bes c ~} c2 ~
      c4 r g8 g \times 2/3 {c bes g ~}
      
      \time 2/4
      g2\fermata
      
      % ------------------------------------------------- %
      \time 12/8
      R1.*4
      \bar "||"
      % ------------------------------------------------- %
      
      r4. g bes8 c d ~ d4.
      r8 r d g, bes c d4. r4.
      r4. g, bes8 c d ~ d4.
      r8 d d f g f d4. r4. 
      
      r8 r g ~ g f r \autoBeamOff d \autoBeamOn \times 2/3 {bes c bes} g bes4
      r8 r g g g g bes4. r8 r g ~
      \times 2/3 {g bes bes} c ~ c4 bes8 c d d ~ d4 c8 ~
      c d f ~ f g r8 r4. d8 c c ~ 
      c4. r4. g8[ g] \times 2/3 {c bes g ~} g4 ~
      \bar "||"
      g2. r2.
      
      % ------------------------------------------------- %
      R1.
      \bar "||"
      % ------------------------------------------------- %
      
      r4. g8 a bes ~ bes c d f f4
      r8 r g g g d ~ d c bes c d4
      r4. g,8 a bes c d f ~ f f4
      r8 f g ~ g g g ~ g d r r4.
      
      r8 g g f4 g8 f4 g8 \autoBeamOff f \autoBeamOn \times 2/3 {c bes c ~}
      c bes bes g g bes c4. r8 g g
      bes4 c8 ~ c r bes c4 d8 d4 bes16 c 
      d8 f4 g2. d16[ c] \times 2/3 {bes8 c bes} 
      bes g4 r4. r4. r4. 
      
      r4. r8 r g' bes2.
      g2. r4. r4. 
      R1.
      
      % ------------------------------------------------- %
      \key gis \minor
      R1.*8
      \key a \minor
      % ------------------------------------------------- %
      
      r4. a g8 e d c r r
      r r e e c d e2.
      r4. a,8 c d e4. r 
      r8 a a ~ a a a e4. ~ e8 e a 
      
      c c4 ~ c8 a g a4 c8 r r a
      c a4 e8 c d c4 a8 ~ a4 a8 
      c8 d4 ~ d8 a a c d d~d c c
      d e4 ~ e4. r4. r8 a, a 
      
      c d4 ~ d8 c c d e4 ~ e8 d d
      e g4 ~ g8 a r r4. r8 a a 
      c c r r a a c c4 ~ c8 a a
      c c4 ~ c8 a a c a c ~ c4 a8
      
      c2. r8 r e, ~ e e4
      c' b8 a e ees d4 c8 ~ c4.
      r8 a4 ~ a2. ~ a8 c4 
      r8 r a b c d c4. a8 a g 
      e a4 ~ a2. r4. 
      
      r4. r8 ees' d d c c a4 c8 ~
      c4 a8 ~ a4. r4. r4. 
      r4. a'4 g8 e d d c c a 
      c a4 ~ a4. r4. r4. 
      
      % ------------------------------------------------- %
      R1.*2
      \bar "||"
      % ------------------------------------------------- %
   }
}

coro = \new Staff
{
   \set Score.skipBars = ##t
   \key g \minor
   \relative c''
   {% ------------------------------------------------- %
      \time 4/4
      \key g \minor
      % ------------------------------------------------- %
      
      R1*11
      
      \time 2/4
      g2\fermata
      
      % ------------------------------------------------- %
      \time 12/8
      R1.*4
      \bar "||"
      % ------------------------------------------------- %
      
      R1.*10
      
      % ------------------------------------------------- %
      R1.
      \bar "||"
      % ------------------------------------------------- %
      
      R1.*12
      
      % ------------------------------------------------- %
      \key gis \minor
      R1.*8
      \key a \minor
      % ------------------------------------------------- %
      
      R1.*21
      
      % ------------------------------------------------- %
      R1.*2
      \bar "||"
      % ------------------------------------------------- %
   
   }
}

lSolista = \new Lyrics \lyricsto "solista" 
{
   \lyricmode
   {
      Birds fly -- ing high __ 
      You know how I feel 
      Sun __ in the sky 
      You know how I feel
      Breeze drif -- tin' _ on _ by __ _ _ _
      You know how I feel 
      It's a new dawn 
      It's a new day
      Its' __ _ a new life 
      for __ _ me __ and I', fee -- ling good
      
      Fish in the sky __
      You know how I feel 
      River run -- ning free
      You know how _ I feel 
      
      Blos -- som on the tree __ _ _ _
      You know how i feel __
      It's a new dawn 
      it's a new day __ 
      it's a new life __
      for _ me __
      and I'm fee -- ling good __
      
      Dra -- gon fly __ out in the sun
      You know what I __ mean don't you know
      But -- ter flies all ha -- vin' fun
      You know what I mean
      sleep in peace when day is __ _ done __ _ _
      That's what _ I _ mean 
      And this old world is a new world 
      and a bold world __ _ for __ _ me __ _ _ _ _
      for __ _ me
      
      Stars when you shi -- ne
      you know how I feel 
      Scene of the pine
      You know how I feel __
      oh free -- dom is mine __ _ _ _
      and I know how I feel __ _ _
      It's a dawn it's a _ new day __
      it's a new life
      
      It's a new dawn 
      it's a new day
      it's a new life __ _
      
      It's a new dawn 
      it's a new day __
      it's a new life __
      it's a new li -- fe __ for me
      
      And I'm fee __ _ ling __ _ _ _ _
      go -- od
      I'm __ _ _ _ fee -- ling __ _ _ go -- od __
      I'm fee -- ling __ _ so go -- od __ _
      
      I'm fee -- ling __ _ _ so good __ _ _ _
   }
}

\new ChoirStaff <<
   \solista
   \lSolista
   \coro
>>