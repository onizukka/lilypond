\version "2.16.1"
\include "imports/main.ly"


% Instrument Music
% -------------------------------------------------
sax = {
  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0

  bes8 bes8-> ~ bes bes bes4.-> bes8
  bes4 bes2-> ~ bes8 cis
  
  d4 r4 r2
  r4 r8 bes cis d ees e 
  f g aes g f ees d c
  b d f g aes fis g a 
  
  bes d, ees e g fis f e 
  ees d cis16 d cis d cis8 g f ees
  d f g a c a b a 
  bes a4 f'8 ~ f4. d8
  
  \grace {c16[ d]} c4. bes8 d f4.
  r2 ees8 f g fis 
  f4 r a8 g f d
  c4 r8 a b d f g
  
  aes fis g a bes fis \times 2/3 {d ees f}
  g c, ~ c d ees f aes ges
  f ees d c bes f bes d 
  g e f ees d c bes a 
  
  aes4 r g'8 e f4
  bes2 ~ bes4. g8
  f ees f g \grace {cis,16} d4 c
  r2 r4 r8 des'8 ~
  
  des4 bes8 a g f d dis
  e des c bes a g fis g
  bes4 r a8 g f e
  ees f g bes \grace {cis16} d2
  
  bes2 ~ bes-> ~ 
  bes-> \grace {e16} f4. ees8
  d bes4. ~ bes4-> r
  r2 g'8 cis, d4
  
  f r4 r2
  cis8 d f cis d fis cis d 
  g4 r4 r2
  r1
  
  \bar "|."
}


% Instrument Sheet
% -------------------------------------------------

\header {
  instrument = "Sax"
}

\paper {  	
  % distancia entre sistemas
  system-system-spacing #'basic-distance = #18
  
  % distancia entre el título y el primer sistema
  markup-system-spacing #'basic-distance = #20
}

<<
  \scoreMarkup
  \new Staff << \transpose c f \transpose ees c \relative c'' \sax >>
>>
%}
