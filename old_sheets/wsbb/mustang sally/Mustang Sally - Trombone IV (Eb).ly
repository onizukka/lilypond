\version "2.18.2"
\language "english"


% Descripción de funcionalidades
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% PLUGINS
% - \eolMark:							mark al final de línea
% - \bendBefore: 					solución al bendBefore (i.e. dis8 \bendBefore)
% - \rit: 								crea una porción de ritardando que debe terminar como las dinámicas, con un \!
% - \improOn, \improOff:	negra de improvisación
% - \emptyStaff:					Imprime un pentagrama vacío
% - \emptyBar:						Imprime un compás invisible (útil para indentados)
% - \glissBefore:					muestra un glissando hacia la nota
% - \shortGlissBefore:		muestra un glissando corto hacia la nota
% - \jazzTurn:						muestra el simbolo equivalente a un turn en jazz
% - \subp:								muestra "sp", subito piano
% - \ap:									(accidental padding), separa un poco una alteración de la siguiente nota
% 												(útil al usar parenthesize)
% - \optOttava:						octavaje opcional

% LAYOUT
% - El aspecto de los silencios multicompás es como los de las partituras de jazz
% - Margen extra de las articulaciones para evitar la colisión con las ligaduras
% - Glissandos bonitos

% PAPER
% - Se imprime el título, instrumento y número de página en todas las páginas
% - Solo se debe tocar el page-count (si eso). Sale bonito tal cual.

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Changelog de funcionalidades
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Plugins
% 23-09-85: Añadida función \emptyStave para imprimir un pentagrama vacío.
% 					En algunas partituras hay un pentagrama vacío antes del coda (lo cual mola bastante, por cierto)
% 05-09-14: Añadido a la función \emptyStave el esconder la armadura de la clave, que no estaba.
% 10-09-14: Mejorada la función \bendBefore
% 					Ahora es genérica, y se puede definir cualquier texto en un lateral que haga las funciones de ornamento
% 16-09-14: Mejorada la función para imprimir barras de improvisación.
% 					Antes se llamaba al comando \rs para pintar una barra que ocupaba una negra
% 					Se reemplaza \rs por \restImproOn y \restImproOff, siendo esta opción mucho más flexible.
% 					Cuando se activa \restImproOn, todo silencio se convierte en barra de improvisación, respetando su duración.
% 20-09-14: Mejorada la función para imprimir barras de improvisación.
% 					Se reemplaza \restImproOn y \restImproOff por \improOn y \improOff
% 					Ahora, también las notas del pentagrama salen como barra de improvisación.
% 					(los comandos \improvisationOn e \improvisationOff de lilypond no se llevan bien con LilyJAZZ)
% 					Se ha añadido un grob correspondiente a los slash de la fuente LilyJAZZ en el archivo lily-jazz.ly
% 01-10-14: Plugins
% 					Renombrada la función \emptyStave a \emptyStaff (stave es el plural de staff) y 
% 					corregida (ya no muestra el número de compás).
% 					Recordar que después de usar \emptyStaff se necesita redefinir el 
% 					número de compás con \set Score.currentBarNumber = #xx
% 01-10-14: Añadida la función \emptyBar que oculta un compás entero. Se puede usar para crear un 
% 					efecto de indentación en un pentagrama dado.
% 					Recordar que después de usar \emptyBar se necesita:
% 					- redefinir el número de compás con \set Score.currentBarNumber = #xx
% 					- volver a pintar la clave con \clef
% 					- volver a pintar la armadura con \key
% 15-10-14: Se le ha aumentado el grosor de los bordes de la función \boxed
% 18-10-15: Añadidas funciones de ornamento para antes de la nota:
% 					- \glissBefore: para mostrar un glissando antes de una nota
% 					- \shortGlissBefore: para mostrar un glissando corto antes de una nota
% 					- \jazzTurn: para mostrar el turn típico trompetero de jazz
% 24-10-15: Añadida funcion \subp para mostrar subito piano "sp"
% 27-10-15: Añadida funcion \ap (de accidental padding) para separar un poco el accidental de la siguiente nota
% 					al usar un paréntesis con \parenthesize, ya que se solapan el uno al otro
% 27-10-15: Añadida funcion \optOttava para imprimir octavaje opcional
% 					También he aprovechado para cambiar la fuente del texto "8va" que utilizaba Feta por defecto
% 28-10-15: Mejorada la función glissBefore y shortGlissBefore.
% 					Ahora se le pasa una lista #'(r x y) en la que "r" es la rotacion, "x" la posicion horizontal 
%						e "y" la positión vertical.

% Markup
% 23-09-85: Añadidas guías para correcta posición y markup de la indicación de tempo
% 27-10-15: Guía para posición de la indicación de tempo mejorada
% 30-10-15: El texto que se coloque sobre silencios de multicompás POR FIN coge la fuente LilyJAZZ
% 31-10-15: El texto de letras (lyricmode) ya coge la fuente LilyJAZZ

% Layout
% 23-09-85: Ahora las articulaciones van por dentro de las ligaduras. Parece que es la mejor solución
% 04-09-14: Los creciendos y decreciendos tienen la línea un poco más gruesa
% 08-09-14: Ahora no se muestra la cancelación de accidentales al haber un cambio de armadura
% 15-09-14: Añadido un poco más de margen a los reguladores
% 20-09-14: Comentada la línea que oculta la cancelación de accidentales al haber cambio de armadura.
% 					Realmente, la cancelación es muy bienvenida.

% Chords
% 09-09-14: Añadida sentencia para que los acordes menores salgan con un guión en vez de con una M minúscula
% 26-10-14: Añadido el include "chord-exception.ily" que recoge una mejor notación de los acordes.
% 					Actualmente, recoge los acordes maj7, 7(b5), 7(#5), 7(b9), 7(#9), 7(add13), maj9(add13), 9(#11)
% 					Se irá añadiendo acordes a menudo.

% Global
% 15-10-14: Todas las instrucciones que se colocaban justo antes de empezar la música están ahora
% 					recogidas en una función \global.
% 					A \global se le ha añadido instrucciones para que agrupe las corcheas de dos en dos
% 					en vez de cuatro en cuatro (el comportamiento por defecto).

% Language
% 15-10-14: Cambiado el lenguaje de entrada de notas del finlandes al inglés
% 					La diferencia es que ahora los sostenidos se escriben "-s" en vez de "-is"
% 					y los bemoles "-f" en vez de "-es".

% Estructura
% 15-10-14: Para poder autoincluir los archivos de instrumento en otros 
% 					(para montar un midi, o un score), se han movido los plugins a un archivo separado
% 					y ahora se utiliza la función \includeOnce, evitando así el problema de las
% 					dependencias circulares. 
% 18-10-15: Mejorada la manera de trabajar con papeles separados y score al mismo tiempo
% 02-11-15: Actualizado a lilyjazz v2
%						Nuevos archivos de fuente para musica y acordes
%						Mucha limpieza en lilyjazz.ily, chords.ily y main.ly

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Dependencias
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% \includeOnce
% Incluye una dependencia solo una vez. Así se pueden incluir los archivos en otros 
% (para montar un score por ejemplo) sin liarla parda con dependencias cíclicas.

%{
includeOnce = #(
	define-void-function (parser location filename) (string?) (
		if (not (defined? (string->symbol filename))) (
			begin (ly:parser-include-string parser (string-concatenate (list "\\include \"" filename "\"")))
			(primitive-eval (list 'define (string->symbol filename) #t))
		)
	)
)
%}

% Tamaño global 
% Se debe incluir antes que lilyjazz. Si no, lilyjazz no funciona.
#(set-global-staff-size 20)

% includes
\include "includes/lilyjazz.ily"
\include "includes/chords.ily"
\include "includes/plugins-1.2.ily"

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~











% Disposición
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Cabecera
\header {
	title = "Mustang Sally"
	subtitle = "(as performed by Buddy Guy)"
	instrument = "Trombone IV (Eb)"
	composer = "Mack Rice"
	arranger = "Arr: Gabriel Palacios"
	poet = ""
	tagline = ""
}

% Márgenes, fuentes y distancias del papel
\paper {
	
	% distancia entre la parte superior de la página y el título
	top-markup-spacing #'basic-distance = 5

	% distancia entre el título y el primer sistema
	markup-system-spacing #'basic-distance = 25

	% distancia entre el texto y el primer sistema 
	top-system-spacing #'basic-distance = 15

	% márgen inferior
	bottom-margin = 20
	
	% intentado del primer sistema
	indent = 0
	 
	% número de páginas
	% page-count = 1

	ragged-last-bottom = ##f 

	oddHeaderMarkup = \markup {
	 \on-the-fly #not-first-page \fill-line {
		\fromproperty #'page:page-number-string
		\fromproperty #'header:title
		\fromproperty #'header:instrument
	 }
	}

	evenHeaderMarkup = \markup {
	 \on-the-fly #not-first-page \fill-line {
		\fromproperty #'header:instrument
		\fromproperty #'header:title
		\fromproperty #'page:page-number-string
	 }
	}
}

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~







% Elementos comunes de la partitura
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
	% Oculta el primer pentagrama del markup
	\override Staff.VerticalAxisGroup #'remove-first = ##t
	
	% Indicación de tiempo
	\once \override Score.RehearsalMark.X-offset = #5
	\once \override Score.RehearsalMark.extra-offset = #'(0 . 3)
	\mark "Medium Rock"

	s8
	s1*4
	
	\mark \markup \boxed "A"
	s1*24
	
	\mark \markup \boxed "B"
	s1*24
	
	\mark \markup \boxed "C"
	s1*24
	
	\mark \markup \boxed "D"
	s1*24
	
	\mark \markup \boxed "E"
	s1*24
	
	\once \override Score.RehearsalMark.X-offset = #5
	\mark \markup {
		\column { \boxed "F" }
		\column { "(Guitar solo)" }
	}
	
	s1*8
	\mark \markup {
		\column { \boxed "G" }
		\column { "(x2)" }
	}
}



% Función musical global
global = {
	% Para pintar todos los números de compás debajo del pentagrama
	% \override Score.BarNumber #'break-visibility = #end-of-line-invisible
	% \override Score.BarNumber #'direction = #DOWN
	% \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

	\compressFullBarRests

	% Arregla el problema de tener el scoreMarkup añadido
	\revert MultiMeasureRest #'staff-position
	\override Voice.Rest #'staff-position = #0	

	\set Timing.beamExceptions = #'()
	\set Timing.baseMoment = #(ly:make-moment 1/4)
	\set Timing.beatStructure = #'(1 1 1 1) 
	
	\override ParenthesesItem.font-size = #0
}

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Elementos específicos del instrumento
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Cifrado
scoreChords = \chords {
	% c1:m f2:m g2:m7
}

% Música
instrumentA = \relative c'' {

	\global
	\key c \major
	
	\partial 8
	r8

	% intro
	r4 \f bf'2 -> ~ bf8 a (
	bf8 -> ) r bf -> r r2
	r4 bf2 -> ~ bf8 a (
	bf8 -> ) r bf -> r r2
	
	% verse I
	\break
	R1*24
	
	% verse II
	\break
	\repeat percent 4 {
		r4 \mf bf2 -> ~ bf8 a (
		bf8 -> ) r bf -> r r2
	}
	
	\break
	\repeat percent 2 {
		r4 g2 -> ~ g8 fs (
		g8 -> ) r g -> r r2
	}
	\repeat percent 2 {
		r4 \mf bf2 -> ~ bf8 a (
		bf8 -> ) r bf -> r r2
	}
	
	\break
	g,1 \fp -> \< ~
	g2. ~ g8 \f gf (
	f4 -^ ) r r2 
	R1
	
	\break
	g'1 \mf 
	f1
	d1 \< 
	R1 \!
	
	% Shout
	\break
	r4 r8 d8 \bendBefore ~ d2
	R1
	r16 g -> \bendAfter -5 r8 r4 r2
	r16 d8. -> \bendBefore d8 -. c-. d4 -> \bendBefore c8. \bendBefore bf16 -^
	
	\break
	r2 r4 r8. c16 -^
	R1
	r4 c8 -> \bendAfter -5 r r2
	r8 c' -. bf -. a -. g8 -. f -. fs16 ( g c, ) f, -> ~
	
	\break
	f4 ^ "(Drums solo background)" ~ f8. f16 -^ r2
	f4 -- f8 -^ r8 r8 ef -> ~ ef16 e8. ->
	f8 -^ r r8 f8 -> \fp \< ~ f2
	r8 \f f -> ~ f16 fs8. -> g2 -> \bendAfter -6
	
	\break
	R1*4
	
	\break
	<< { \override Voice.Rest #'staff-position = #0
	g1 -> ^ "(Trombone solo)" \mf ~
	g2. ~ g8 fs (
	f4 -^ ) r4 r2 } \\ { s1 s2 s4. \< s8 \f s1 } >>
	R1 
	
	\break
	c'1 ~ \mf 
	c1
	c,1 \<
	r2 \! r4 r16 g'8. ->
	
	% verse III
	\break
	c8 -> r r4 r2
	R1*23
	
	% verse IV
	\break
	\repeat percent 3 {
		r4 r8 bf' -> r bf -> r a (
		g4 -. ) r4 r2
	}
	r4 r8 bf -> r bf -> r a (
	bf4 -. ) r4 r2
	
	\break
	r4 r8 c -> r c -> r bf (
	a4 -. ) r4 r2
	r4 r8 c -> r c -> r b (
	c4 -. ) r4 r2
	
	\break
	r4 r8 bf -> r bf -> r a (
	g4 -. ) r4 r2
	r4 r8 bf -> r bf -> r a (
	bf4 -. ) r4 r2
	
	\break
	g,1 \fp -> \< ~
	g2. ~ g8 \f fs (
	f4 -^ ) r r2 
	R1
	
	\break
	g'1 \mf 
	f1
	d1 \< 
	c'8 \f -. c -. bf -. a -. g8 -. f -. f16 ( ef ) e -- c -^
	
	\break
	R1*8
	
	\break
	\repeat volta 2 {
		bf'8 \mf ( -> a ) bf4 -^ r8 bf -> r bf -> ~ (
		bf8 a ) bf4 -^ r2
		bf8 ( -> a ) bf4 -^ r8 bf -> r bf -> \fp \< ~ 
		bf2 ~ bf4 \f r
		
		\break
		bf8 ( -> a ) bf4 -^ r8 bf -> r bf -> ~ (
		bf8 a ) bf4 -^ r2
	}
	
	\alternative {
		{ bf8 ( -> a ) bf4 -^ r8 bf -> r bf -> \fp \< ~ 
		  bf2 ~ bf4 \f r }
		
		{ \break
		  bf8 ( -> a ) bf4 -^ r8 bf -> r bf -> \fp \< ~ 
		  bf2 ~ bf4 \f r }
	}
	
	R1
	
	\bar "|."
}

instrumentB = \relative c'' {
  
	\global
	\key c \major

	c
}

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

\book {
	\paper { 
		% Descomentar para score
		% #(set-paper-size "a4" 'landscape)
		% indent = 20
		% systems-per-page = 2
	}
	
	\score {
		% Descomentar para midi
		% \midi { \tempo 4 = 110 }
		
		% ATENCIÓN
		% NO USAR layout-set-staff-size O SE ROMPE lilyjazz!!!
		\layout { }
		
		<<
			\scoreMarkup
			\scoreChords
			\transpose ef c \new StaffGroup <<
				\new Staff \with { instrumentName = "" midiInstrument = #"" } { \instrumentA }
				% \new Staff \with { instrumentName = "" midiInstrument = #"" } { \instrumentB }
			>>
		>>
	}
}



%{
\book {
	\header { instrument = "" }
   \bookOutputName ""
	\score {
		<<
			\scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\new Staff { \transpose bf c' \instrumentA }
			>>
		>>
	}
}

\book {
	\header { instrument = "" }
   \bookOutputName ""
	\score {
		<<
			\scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\new Staff { \transpose ef c' \instrumentB }
			>>
		>>
	}
}
%}

