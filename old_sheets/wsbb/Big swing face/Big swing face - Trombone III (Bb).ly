\version "2.18.2"
\language "english"

% Se debe incluir antes que lilyjazz. Si no, lilyjazz no funciona.
#(set-global-staff-size 20)

\include "main.ily"
\include "music-sheet-paper.ily"

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~











% Disposición
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Cabecera
\header {
	title = "Big Swing Face"
	instrument = "Trombone III (Eb)"
	composer = "Bill Potts"
	arranger = ""
	poet = ""
	tagline = ""
}


% Elementos comunes de la partitura
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
	% Oculta el primer pentagrama del markup
	\override Staff.VerticalAxisGroup #'remove-first = ##t

	% Indicación de tiempo
	% \once \override Score.RehearsalMark.self-alignment-X = #LEFT
	% \once \override Score.RehearsalMark.extra-offset = #'(0 . 3) 
	% \mark \jazzTempoMarkup "Easy Swing" c4 "112"

  \partial 8
  s8
  
  s1*8
  
  \mark \markup \boxed "A"
  s1*12
  
  \mark \markup \boxed "B"
  s1*12
  
  \mark \markup {
    \column { \boxed "C" }
    \column { "Piano" }
  }
  s1*24
  
	\once \override Score.RehearsalMark.self-alignment-X = #LEFT
  \mark \markup {
    \column { \boxed "D" }
    \column { "Tenor" }
  }
  s1*24
  
	\once \override Score.RehearsalMark.self-alignment-X = #LEFT
  \mark \markup \boxed "E"
  s1*12
  
  \mark \markup \boxed "F"
  s1*12
  
  \mark \markup \boxed "G"
  s1*12
  
  \mark \markup \boxed "H"
  s1*12
  
  \mark \markup \boxed "I"
  s1*12
  
  \mark \markup \boxed "J"
  s1*12
  
  \mark \markup \boxed "K"
}



% Función musical global
global = {
	% Para pintar todos los números de compás debajo del pentagrama
	% \override Score.BarNumber #'break-visibility = #end-of-line-invisible
	% \override Score.BarNumber #'direction = #DOWN
	% \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

	\compressEmptyMeasures

	% Arregla el problema de tener el scoreMarkup añadido
	\revert MultiMeasureRest #'staff-position
	\override Voice.Rest #'staff-position = #0	

	\set Timing.beamExceptions = #'()
	\set Timing.baseMoment = #(ly:make-moment 1/4)
	\set Timing.beatStructure = #'(1 1 1 1) 
	
	\override ParenthesesItem.font-size = #0
}

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Elementos específicos del instrumento
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Cifrado
scoreChords = \chords {
	% c1:m f2:m g2:m7
}

% Música
instrumentA = \relative c' {
  
	\global
	\key f \major

  \partial 8
  
  c8 -^ \ff
  \bar "||"
  
  R1
  d2 -> df4. -> c8 -^
  R1
  d4 -^ d -^ df4. -> c8 -^
  
  \break
  R1
  d4 -^ d -^ df8 df4 -. c8 -^
  R1*2
  
  \bar "[|:"
  \repeat volta 2 {
    \repeat percent 2 { f,8 -- \p f -. r4 r4 g -^ }
    \break
    f8 ( f -> g g -> f f -> f f -> )
    ef8 -- ef -. r4 r8 ef -^ r4
    
    \repeat percent 2 { f8 -- f -. r4 r4 f -^ }
    \break
    f4 -^ f8 ( f -> f4. -> f8 -^ )
    r2 r8 a -^ r4
    
    \repeat percent 2 { f8 -- f -. r4 r4 f4 -^ }
    \break
    f4 -^ d -^ f8 ( ef4 -. ) g8 -^
    r2 r8 g4. ->
  }
  \bar ":|]"
  
  f8 -- f -. r4 r4 f8 ( f ->
  d -- d -. ) r4 \< r4 r8 c' ~ -> \ff
  \break
  c4 d4 -^ d8 ( c4 -. ) c8 -^
  r8 c4. -> c4 -^ b8 ( c -^ ) 
  
  r2 r4 af -^
  bf4 -^ af -^ d -^ af -^
  \break
  bf4 -^ af -^ d8 ( af4 -^ ) c8 -^
  r8 a?4. -> d4 -^ a4 -^
  
  << 
    { \stemNeutral 
      \override Voice.Rest #'staff-position = #0	
      d2.. ( -> \< c8 \glissando -> \! )
      \hideNotes c,2 \unHideNotes r8 d'4. -> } \\
    { \override Voice.Rest #'staff-position = #0	
	    s1
      r2 s2 }
  >>
  \break
  c4 -^ a4 -^ cf -^ af -^
  d4 -^ bf -^ b8 ( af4 -. ) c?8 -^
  
  \bar "||"
  R1*24
  
  \break
  \bar "||"
  R1*24
  
  \bar "||"
  r8 \mf c4. ( -> a4. -> b8 -^ )
  r2 r8 bf?4. -> 
  a4 -^ r r b -^
  \pageBreak
  << 
    { \stemNeutral 
      \override Voice.Rest #'staff-position = #0	
      c4. -> c8 -> \glissando \hideNotes c,2 } \\
    { \override Voice.Rest #'staff-position = #0	
	    s2 r2 }
  >>
  
  r4 f4 -^ g8 ( bf?4 -. ) d8 -^
  R1
  r8 c4. -> ( bf8 \< bf -> bf a -^ \! )
  \break
  R1
  
  f4 -^ r8 f8 -^ r2
  g4. ( -> \< g8 -> ~ g4. a8 -^ \! )
  R1*2
  
  \break
  \bar "||"
  R1*11
  r8 \f f4. \( -> g8 ( bf4 -. ) c8 -^ \)
  
  \bar "||"
  R1*3
  \break
  r2 r8 \f fs,4. -> 
  
  f4 -^ r4 r2
  R1*3
  
  r4 r8 f -^ r2
  R1
  \break
  r8 \f f \< ( g a -^ ) r8 bf4 -^ d8 -^
  r8 c4. -> d4 -^ d -^
  
  \bar "||"
  ef2.. \ff ^\markup { 
    \hspace #2 \musicglyph #'"scripts.prallprall" 
    \hspace #-1 \musicglyph #'"scripts.prallprall" } c8 -^
  R1*3
  
  \break
  r2 r8 \f bf4 -^ r8
  r2 r8 b4 -^ r8
  c4 -^ r4 r2
  R1*4
  r2 r4 ef ~ -> \ff

  \break
  \bar "||"
  ef2 c4. -> c8 -^
  r8 af4. -> b4 -^ b -^
  d4 -^ c -^ af8 ( b4 -. ) a8 -^
  r2 r8 fs4. ->
  
  f?4 -^ r4 r2
  \break
  R1*3
  
  r4 r8 \ff d' -> ~ d4 f -^
  e8 ( d4 -. ) d8 -^ r8 df4. ->
  c4 -^ \> c -^ a -^ af -^
  \break
  g8 ( g4 -. ) g8 -^ r8 g4. -> \p
  
  \bar "||"
  \repeat percent 2 { f8 -- f -. r4 r4 g -^ }
  f8 ( f -> g g -> f f -> f f -> )
  \pageBreak
  f8 -- f -. r4 r8 ef -^ r4
  
  \repeat percent 2 { f8 -- f -. r4 r4 f -^ }
  f4 -^ f8 ( f -> f4. -> f8 -^ )
  \break
  r2 r8 a -^ r4
  
  \repeat percent 2 { f8 -- f -. r4 r4 f -^ }
  f4 -^ d -^ f8 ( ef4 -. ) g8 -^
  \break
  R1
  
  \bar "[|:"
  \repeat volta 2 {
    d'4 -> \ff c -^ d8 ( f4 -. ) d8 -^
    r8 c4. -> d4 -^ f -^
    d4 -^ c -^ d8 ( ef4 -. ) c8 -^
    \break
    r2 r4 c4 -^
    
    << 
      { \stemNeutral
        \override Voice.Rest #'staff-position = #0	
        d2.. -> \< bf8 -> \! \glissando 
        \hideNotes c,2 \unHideNotes r4 c'4 -^
        b2.. -> \< b8 -> \! \glissando 
        \hideNotes c,2 \unHideNotes r8 c'4. ->
      } \\ 
      { \override Voice.Rest #'staff-position = #0	
        s1 
        r2 s2
        s1 
        r2 s2 }
    >>
    
    \break
    bf4 -^ r4 g4 -^ bf -^ 
    bf8 ( d4 -. ) d8 -^ r8 df4. ->
  }
  \alternative {
    { d?4 -^ r4 r2 
      R1 }
    { d?4 -^ r4 r2 
      \break
      d2 -> ( df4. -> c8 -^ ) }
  }
  
  R1
  f4 -^ f -^ df4. -> c8 -^
  R1
  \break
  f4 -^ f -^ df8 df4 -. c8 -^
  
  r4 r8 e -^ r4 r8 g -> ~
  g1 \fermata
  
  \bar "|."
}

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

\book {
	\paper { 
		% Descomentar para score
		% #(set-paper-size "a4" 'landscape)
		% indent = 20
		% systems-per-page = 2
	}
	
	\score {
		% Descomentar para midi
		% \midi { \tempo 4 = 110 }
		
		% ATENCIÓN
		% NO USAR layout-set-staff-size O SE ROMPE lilyjazz!!!
		\layout { }
		
		<<
			\scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\transpose bf c'' \new Staff \with { instrumentName = "" midiInstrument = #"" } { \instrumentA }
			>>
		>>
	}
}



%{
\book {
	\header { instrument = "" }
   \bookOutputName ""
	\score {
		<<
			\scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\new Staff { \transpose bf c' \instrumentA }
			>>
		>>
	}
}

\book {
	\header { instrument = "" }
   \bookOutputName ""
	\score {
		<<
			\scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\new Staff { \transpose ef c' \instrumentB }
			>>
		>>
	}
}
%}

