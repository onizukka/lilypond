\version "2.12.3"

\include "Cantina - Bass.ly"

\header {
	title = "Cantina Band"
	composer = "John Williams"
	arranger = "Gabriel Palacios"
}

\score {
	{
		<<
			\bass
		>>
	}

}