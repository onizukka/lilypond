bass = \new Staff
{
	\set Staff.instrumentName = "Bass"
	\set Score.skipBars = ##t
	
	\key d \minor
	
	\relative c''
	{
		\bar "|:"
		d4-^  a-^ d-^ a-^
		d,^^ f^^ a c
		d gis,8 a d4 gis,8 a
		g4 g c8 d e c
		d4 a d a8 e'
		c4 d f c
		bes b c d
		g c, f8 e d c
		d4 a d a 
		d a d8 a16 a gis8 a
		d4 a d a 
		d a ees'\glissando c8 cis
		d4 a d a
		d a d a
		bes b\glissando c'16 c,~c4 d8
		g bes, c aes' f e d c
		\repeat "percent" 3 { d8 c bes a g f e d }
		d r8 r4 r2
		\repeat "percent" 3 { d'8 c bes a g f e d }
		d'8 c bes a d r8 r4 
		\bar ":|"
		c4 g' c,8 bes16 a~a8 g 
		f4 c' f8~ f16 a~a bes~bes8
		c4 g c8 bes16 a~a8 g 
		f4 f d8~d16 a'~a d~d8
		bes4 bes b b
		c c8 cis d4 d,
		g g, c c
		f des f8. a16 bes8 b
		c bes g e c bes g e
		f a bes b c f a bes
		c b bes g e c bes g 
		f a gis a fis a d, a'
		f aes a bes g a bes b 
	}
}	

