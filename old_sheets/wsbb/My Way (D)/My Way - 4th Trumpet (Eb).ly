\version "2.16.1"
\include "imports/main.ly"

% Instrument Music
% -------------------------------------------------

trumpetFour = {
  \jazzOn
  \compressFullBarRests
    
  \key c \major
  R1*2
  
  R1*16
  
  R1*15
  r4 r8 d,^^ e2^>\<(
  
  \break
  d2\! cis
  c1\>
  b2\! d
  b1)
  
  g'1(\>e\<
  \break
  fis^>\! ~ fis)
  
  e( g b,\> a)\!
  
  \break
  R1*15
  r4 r8 d^^ e4^-^>\< e^-^>
  
  \break
  a,4.^>\mf a8^^ a2^>
  g'4.^> g8^^ fis2^>
  b,4.^> b8^^ e4^-^> e^-^>
  b^-^> r8 \times 2/3 {b16 b b} e4^-^^ b8^^ b^^
  
  \break
  e4.^> e16 e e4^-^> e^-^>
  e^-^> r8 \times 2/3 {e16 e e} e4^-^> cis^-^>
  fis4.^> fis16 fis fis4^-^> fis^-^>
  a4.^> fis8^^ fis2^>
  
  \break
  e4.^>\rit e8^^ e4^-^> e^-^>
  a2\!^> r2\fermata
  r1
  r2\f \times 2/3 {fis4^> g^> a^>}
  
  \break
  a2^> ~ a8( b a gis
  a4.) r8 \times 2/3 {fis4 g? a}
  b4. r8 e,8(\< g b cis
  d1)\ff ~ 
  d\fermata
}


% Instrument Sheet
% -------------------------------------------------
\header {
  instrument = "4th Trumpet"
}

\paper {  	
  % distancia entre sistemas
  system-system-spacing #'basic-distance = #17
  
  % distancia entre el título y el primer sistema
  markup-system-spacing #'basic-distance = #25
}

<<
  \scoreMarkup
  \new Staff << \transpose c d \transpose ees bes \relative c'' \trumpetFour >>
>>