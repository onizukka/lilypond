\version "2.16.1"
\include "imports/main.ly"

% Instrument Music
% -------------------------------------------------

trumpetOne = {
  \jazzOn
  \compressFullBarRests
    
  \key d \major
  R1*2
  
  R1*16
  
  R1*15
  r4 r8 a^^ a2^>\<(
  
  \break
  d2\! cis
  c1\>
  b2\! d
  b) b(\<
  
  d1\> cis\<
  \break
  cis^>\! b)
  
  b( cis b\> a)\!
  
  \break
  R1*15
  r4 r8 a^^ a4^-^>\< a^-^>
  
  \break
  \repeat percent 2 {
     a4.^>\mf a8^^ a2^>
  }
  a4.^> a8^^ b4^-^> b^-^>
  b^-^> r8 \times 2/3 {b16 b b} b4^-^^ a8^^ b^^
  
  \break
  d4.^> d16 d d4^-^> d^-^>
  cis^-^> r8 \times 2/3 {cis16 cis cis} cis4^-^> a^-^>
  e'4.^> e16 e e4^-^> e^-^>
  fis4.^> e8^^ d2^>
  
  \break
  d4.^>\rit d8^^ d4^-^> e^-^>
  a2\!^> r2\fermata
  r1
  r2\f \times 2/3 {fis4^> g^> a^>}
  
  \break
  a2^> ~ a8( b a gis
  a4.) r8 \times 2/3 {fis4 g a}
  b4. r8 e,8(\< g b cis
  d1)\ff ~ 
  d\fermata
}


% Instrument Sheet
% -------------------------------------------------

\header {
  instrument = "1st Trumpet"
}

\paper {  	
  % distancia entre sistemas
  system-system-spacing #'basic-distance = #17
  
  % distancia entre el título y el primer sistema
  markup-system-spacing #'basic-distance = #25
}

<<
  \scoreMarkup
  \new Staff << \transpose c d \relative c'' \trumpetOne >>
>>
