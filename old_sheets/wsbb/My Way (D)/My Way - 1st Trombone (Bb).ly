\version "2.16.1"
\include "imports/main.ly"

% Instrument Music
% -------------------------------------------------

tromboneOne = {
  \jazzOn
  \compressFullBarRests
    
  \key c \major
  R1*2
  
  R1*16
  
  \break
  R1*8
  
  \break
  \mark \markup \boxed "quasi horn"
  e1(\mp 
  d2\< des
  c4\! cis d? e
  g\> f2.\!)
  
  \break
  d2( c
  c b
  a1
  g)
  
  \break
  c2(\mf b 
  bes1\>
  a2\! c
  a f)
  
  \break
  f'1( d\< b\! a)
  
  \break
  a( b a\> g\!)
  
  \break
  R1*8
  
  \break
  \mark \markup \boxed "q. h."
  e'1(\mp 
  d2 des
  c4 cis d? e
  g f e d
  
  \break
  d2 c
  c b)
  r4 r8 a( f'4 a,8 b
  c4.) r8 r2
  
  \break
  \mark \markup \boxed "q. h."
  e1(\mf
  d2 des)
  c( d?
  e d4 e8 f
  
  \break
  f4. e8 d4. c8
  b2) r4 g'(
  g4. f8 e4. d8
  e2) r4 a(
  
  \break
  a4.^>\rit b8 c4) a^> 
  b2^>\! r2\fermata
  r4 f^>( a, d
  c4.) r8 r2
  
  \break
  e2^>\f( ees
  d des)
  r4 d,2.^>\<
  r4\ff des'^-^> c^-^> c^-^>
  c1\fermata
}


% Instrument Sheet
% -------------------------------------------------
\header {
  instrument = "1st Trombone"
}

\paper {  	
  % distancia entre sistemas
  system-system-spacing #'basic-distance = #23
  
  % distancia entre el título y el primer sistema
  markup-system-spacing #'basic-distance = #25
}

<<
  \scoreMarkup
  \new Staff << \transpose c d \transpose bes c' \relative c'' \tromboneOne >>
>>