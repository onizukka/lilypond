\version "2.16.1"
\include "imports/main.ly"

% Instrument Music
% -------------------------------------------------

tromboneFour = {
  \jazzOn
  \compressFullBarRests
    
  \key c \major
  R1*2
  
  R1*16
  
  \break
  R1*8
  
  \break
  \mark \markup \boxed "quasi horn"
  c1(\mp\< g'\! a c\>)
  
  \break
  e,1\! ~ 
  e
  r
  r4 r8 a^^ e2^>\<(
  
  \break
  a1\mf ~ a\> d,\! ~ d)
  
  \break
  r2 r4 d(
  e2 d\<
  c1\!
  f)
  
  \break
  b,( e a,\> ~ a\!)
  
  \break
  R1*8
  
  \break
  \mark \markup \boxed "q. h."
  c1(\mp
  g'
  a
  c2.) aes,4(
  
  \break
  g1 ~
  g
  c ~
  c4.) r8 g4^-^>\< g^-^>
  
  \break
  \repeat percent 2 {
    c4.^>\mf c8^^ c2^>
  }
  f4.^> f8^^ f4^-^> f^-^>
  f^-^> r8 \times 2/3 {f16 f f} f4^-^^ e8^^ e^^
  
  \break
  a4.^> a16 a a4^-^> a^-^>
  g^-^> r8 \times 2/3 {g16 g g} g4^-^> f^-^>
  b4.^> b16 b b4^-^> b^-^>
  c4.^> b8^^ a2^>
  
  \break
  a4.^>\rit a8^^ a4^-^> d,^-^>
  g2\!^> g,2\fermata
  c1( ~
  c4) g2.^>\<
    
  \break
  c1\f^> ~ 
  c
  r4 d,2.^>\<
  r4\ff ees^-^>( d^-^> des^-^>
  c1)\fermata
}


% Instrument Sheet
% -------------------------------------------------
\header {
  instrument = "4th Trombone"
}

\paper {  	
  % distancia entre sistemas
  system-system-spacing #'basic-distance = #23
  
  % distancia entre el título y el primer sistema
  markup-system-spacing #'basic-distance = #25
}

<<
  \scoreMarkup
  % \new Staff << \relative c' \tromboneThree >>
  \new Staff << \transpose c d \transpose ees c' \relative c' \tromboneFour >>
>>