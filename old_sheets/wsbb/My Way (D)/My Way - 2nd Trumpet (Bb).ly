\version "2.16.1"
\include "imports/main.ly"

% Instrument Music
% -------------------------------------------------

trumpetTwo = {
  \jazzOn
  \compressFullBarRests
    
  \key d \major
  R1*2
  
  R1*16
  
  R1*15
  r4 r8 fis,^^ g2^>\<(
  
  \break
  a1\!
  a1\>
  g2\! b
  g) g(\<
  
  b1\> a\<
  \break
  a^>\! ~ a)
  
  g( a g\> fis)\!
  
  \break
  R1*15
  r4 r8 fis^^ g4^-^>\< g^-^>
  
  \break
  fis4.^>\mf fis8^^ fis2^>
  e4.^> e8^^ ees2^>
  fis4.^> fis8^^ g4^-^> g^-^>
  fis^-^> r8 \times 2/3 {fis16 fis fis} g4^-^^ fis8^^ fis^^
  
  \break
  b4.^> b16 b b4^-^> b^-^>
  a^-^> r8 \times 2/3 {a16 a a} a4^-^> g^-^>
  cis4.^> cis16 cis cis4^-^> cis^-^>
  d4.^> cis8^^ b2^>
  
  \break
  b4.^>\rit b8^^ b4^-^> b^-^>
  e2\!^> r2\fermata
  g,1(
  fis4.) r8\f \times 2/3 {fis'4^> g^> a^>}
  
  \break
  fis2^>( f
  e4.) r8 \times 2/3 {ees4 ees ees}
  g4. r8 b,8(\< d g a
  b2\ff bes
  a1)\fermata
}


% Instrument Sheet
% -------------------------------------------------
\header {
  instrument = "2nd Trumpet"
}

\paper {  	
  % distancia entre sistemas
  system-system-spacing #'basic-distance = #17
  
  % distancia entre el título y el primer sistema
  markup-system-spacing #'basic-distance = #25
}

<<
  \scoreMarkup
  \new Staff << \transpose c d \relative c'' \trumpetTwo >>
>>