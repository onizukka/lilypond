\version "2.16.1"

\include "imports/main.ly"

\include "My Way - 1st Alto.ly"

% Score
% -------------------------------------------------
%{
\score {
  <<
    \new StaffGroup <<
      \new Staff << \transpose c f \relative c'' \altoOne >>
      \new Staff << \transpose c f \relative c'' \altoTwo >>
    >>
  >>
}
%}
