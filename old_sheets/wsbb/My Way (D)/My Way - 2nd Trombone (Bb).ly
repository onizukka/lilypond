\version "2.16.1"
\include "imports/main.ly"

% Instrument Music
% -------------------------------------------------

tromboneTwo = {
  \jazzOn
  \compressFullBarRests
    
  \key c \major
  R1*2
  
  R1*16
  
  \break
  R1*8
  
  \break
  \mark \markup \boxed "quasi horn"
  c1(\mp 
  bes\<
  a\!
  e'4\> d2.\!)
  
  \break
  d2( c
  c b
  a1
  g)
  
  \break
  g1(\mf ~ g\> a\! ~ a)
  
  \break
  c1( b\< g\! ~ g)
  
  \break
  f( g f\> e\!)
  
  \break
  R1*8
  
  \break
  \mark \markup \boxed "q. h."
  c'1(\mp
  bes
  a
  c2.) c4(
  
  \break
  g1 ~
  g
  a
  g4.) r8 r2
  
  \break
  \mark \markup \boxed "q. h."
  e'1(\mf
  d2 des)
  c( d?
  e d4 e8 f
  
  \break
  f4. e8 d4. c8
  b2) r4 g'(
  g4. f8 e4. d8
  e2) r4 a,(
  
  \break
  a4.\rit b8 c4) a^> 
  b2^>\! g,2\fermata
  r4 f''^>( a, d
  c4.) r8 r2
  
  \break
  c2^>\f( b
  bes1)
  r4 d,2.^>\<
  r4\ff g^-^> ges^-^> f^-^>
  e1\fermata
}


% Instrument Sheet
% -------------------------------------------------
\header {
  instrument = "2nd Trombone"
}

\paper {  	
  % distancia entre sistemas
  system-system-spacing #'basic-distance = #23
  
  % distancia entre el título y el primer sistema
  markup-system-spacing #'basic-distance = #25
}

<<
  \scoreMarkup
  \new Staff << \transpose c d \transpose bes c' \relative c'' \tromboneTwo >>
>>