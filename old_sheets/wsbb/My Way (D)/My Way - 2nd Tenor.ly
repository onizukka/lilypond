\version "2.16.1"
\include "imports/main.ly"

% Instrument Music
% -------------------------------------------------
clarinetFour = {
  
  \key d \major
  
  \mark \markup \boxed "as clarinet"
  R1*2
  
  R1*16
  
  a'1(\mp ~ a ~ a ~ a)
  
  \break
  b1( ~ b a a)
  
  \break
  a1( g2 fis g1 ~ g)
  
  \break
  fis1( g d' d2 cis)
  
  \break
  a4.(\mf d,8 d4 a'
  a4. c,8 c4 a'8 b
  a4. g8 g4 fis
  fis4. e8 e4) fis16( g a b
  
  \break
  d4. g,8 g4 d'4
  cis4 ~ cis16 b cis d e4 a, 
  e' a, a' e)
  r8 a,16( b cis d e fis b4 a
  
  \break
  b4. e,8 e4 fis8 g
  g4 a,) r4 g'(
  g4. b,8 b4 fis'16 g b, cis
  d1)\>
  
  \break
 \repeat percent 2 { r4\mp a8( fis' fis4 a,) }
  r4 fis'8( e a2 ~
  a4 \times 2/3 {fis8 g a} b,2)
  
  \break
  r4 b8( g' g4 b,)
  r4 b8( g'16 fis g4 b,)
  a4.( g'8 g2 ~
  g4 \times 2/3 {fis8 g a} d,2)
  
  \break
  a1( g2 fis g1 ~ g)
  
  \break
  fis1(
  g
  b)
  r2 r4 \times 4/7 {a,16\<( b cis d e fis g}
  
  \break
  a2\mf ~ a8 d, d a'
  a2) r8 c,( a'[ d]
  d2 g,4 d'
  d2) r8 e( fis[ g]
  
  \break
  g4. fis8 e4. d8
  cis2) r4 a'(
  a4. g8 fis4. e8 
  d2) r4 b(
  
  \break
  b4.\rit cis8 d4 b
  cis2)\! ~ cis2\fermata
  g1(
  fis4.) r8 r2
}

tenorTwo = {  
  \jazzOn
  \compressFullBarRests
  
  %\transpose ees bes \relative c' \clarinetThree
  \relative c' \clarinetFour
  
  \break
  \key a \major
  R1*3
  \mark \markup \boxed "as tenor"
  r4\ff a^-( aes^- g^-
  fis1)\fermata 
}


% Instrument Sheet
% -------------------------------------------------
\header {
  instrument = "2nd Tenor"
}

\paper {  	
  % distancia entre sistemas
  system-system-spacing #'basic-distance = #21
  
  % distancia entre el título y el primer sistema
  markup-system-spacing #'basic-distance = #25
}

<<
  \scoreMarkup
  \new Staff << \transpose c d \relative c'' \tenorTwo >>
>>