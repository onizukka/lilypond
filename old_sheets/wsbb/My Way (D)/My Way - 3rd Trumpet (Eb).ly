\version "2.16.1"
\include "imports/main.ly"

% Instrument Music
% -------------------------------------------------

trumpetThree = {
  \jazzOn
  \compressFullBarRests
    
  \key d \major
  R1*2
  
  R1*16
  
  R1*15
  r4 r8 d,^^ cis2^>\<(
  
  \break
  fis1\!
  fis1\>
  g1\!
  d)
  
  e1(\> g\<
  \break
  e^>\! d)
  
  d( e d\> ~ d)\!
  
  \break
  R1*15
  r4 r8 d^^ cis4^-^>\< cis^-^>
  
  \break
  d4.^>\mf e8^^ cis2^>
  c4.^> c8^^ c2^>
  d4.^> d8^^ d4^-^> d^-^>
  d^-^> r8 \times 2/3 {d16 d d} d4^-^^ d8^^ d^^
  
  \break
  g4.^> g16 g g4^-^> g^-^>
  g^-^> r8 \times 2/3 {g16 g g} g4^-^> e^-^>
  a4.^> a16 a a4^-^> a^-^>
  b4.^> a8^^ a2^>
  
  \break
  g4.^>\rit g8^^ g4^-^> g^-^>
  cis2\!^> r2\fermata
  d,1( ~
  d4.) r8\f \times 2/3 {fis4^> g^> a^>}
  
  \break
  d2^>( cis
  c4.) r8 \times 2/3 {c4 c c}
  d4. r8 g,8(\< b e fis
  g2\ff fis4 f
  fis?1)\fermata
}


% Instrument Sheet
% -------------------------------------------------
\header {
  instrument = "3rd Trumpet"
}

\paper {  	
  % distancia entre sistemas
  system-system-spacing #'basic-distance = #17
  
  % distancia entre el título y el primer sistema
  markup-system-spacing #'basic-distance = #25
}

<<
  \scoreMarkup
  \new Staff << \transpose c d \transpose ees bes \relative c'' \trumpetThree >>
>>