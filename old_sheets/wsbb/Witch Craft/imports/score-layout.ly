\header {
  title = "Witch Craft"
  instrument = ""
  composer = ""
  arranger = ""
  lyrics = ""
}



\paper {
  % El definir otro tipo de fuente no permite cambiar el tamaño global del staff. Es un bug conocigo
  #(define fonts (make-pango-font-tree "LilyJAZZText" "Nimbus Sans" "Luxi Mono" (/ 20 20)))
  
  % distancia entre sistemas
  system-system-spacing #'basic-distance = #15
  
  % distancia entre el título y el primer sistema
  markup-system-spacing #'basic-distance = #20
  
  top-markup-spacing #'basic-distance = #5
  
  top-system-spacing #'basic-distance = #10
  
  bottom-margin = 10
  
  page-count = 1
  
  oddHeaderMarkup = \markup {
    \on-the-fly #not-first-page \fill-line {
      \fromproperty #'page:page-number-string
      \fromproperty #'header:title
      \fromproperty #'header:instrument
    }
  }
  
  evenHeaderMarkup = \markup {
    \on-the-fly #not-first-page \fill-line {
      \fromproperty #'header:instrument
      \fromproperty #'header:title
      \fromproperty #'page:page-number-string
    }
  }
}