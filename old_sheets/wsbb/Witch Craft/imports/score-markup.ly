scoreMarkup = \new Staff \with {
   \RemoveEmptyStaves
}
{
  % Oculta el primer pentagrama del markup
  \override Staff.VerticalAxisGroup #'remove-first = ##t
  
  \bar "||"
  s1*4
  
  \bar "||"
  \mark \markup \boxindex A
  s1*8
  
  \bar "||"
  \mark \markup \boxindex B
  s1*8
  
  \bar "||"
  \mark \markup \boxindex C
  s1*8
  
  \bar "||"
  \mark \markup \boxindex D
  s1*8
  
  \bar "||"
  \mark \markup \boxindex E
  s1*8
  
  \bar "||"
  \mark \markup \boxindex F
  s1*8
  
  \bar "||"
  \mark \markup \boxindex G
  s1*8
  
  \bar "||"
  \mark \markup \boxindex H
  s1*8
  
  \bar "||"
  \mark \markup \boxindex I
  s1*13
  
  \bar "|."
}