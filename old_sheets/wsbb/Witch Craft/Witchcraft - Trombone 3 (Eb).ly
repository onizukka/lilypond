\version "2.16.1"
\include "imports/main.ly"


% Instrument Music
% -------------------------------------------------
tromboneThree = {

  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0
  
  \key f \major
  
  \mark \markup \boxed "Cups"
  \repeat percent 4 {
    r4 r8 ees-- ~ ees ees4.--
  }
  
  R1 
  r2 r8. a16( a8. a16
  aes4-.) r4 r2
  \break
  r2 r8. aes16( aes8. aes16
  
  g4-.) r4 r2
  R1*2
  r2 r4 r8. a16(
  
  bes4-.) r r2
  r2 r4 r8. bes16--
  bes4-. r r2
  r2 r4 r8. bes16(
  
  \break
  aes4-.) r r2
  R1*3
  
  r2 a4-. r
  a-. r8 a-- ~ a4 \glissando c,-.
  r2 bes'4-. r
  bes-. r8 bes-- ~ bes4 \glissando c,-.
  
  r2 a'4-. r
  \break
  a-. r8 a-- ~ a4 \glissando c,-.
  r8 a'-. r a( a8. d,16 f8. gis16-.)
  r8 gis-. r f( gis4--) gis-.
  
  fis-. r r2
  r2 r4 r8. a16--
  a4-. r r2
  \break
  r2 r4 r8. fis16--
  
  f?4-. r r2
  R1
  r8 g-. r g( f4-.) \> d-.
  e-. e8.( \! e16 d8. e16 d4-.)
  
  \mark \markup \boxed "Open"
  R1*7
  \break
  f'4.-- ees8-. r ees4.-- \f
  
  r8 a,4.-- a4-. a-.
  a( c8. c16-.) r2
  r8 aes4.-- aes4-. aes-.
  aes( b8. b16-.) r2
  
  r8 b4.-- b4-. b-.
  \break
  b( ees8. ees16-.) r8. b?16-- b4-.
  bes?4.--  e?8-. r ees-. r d(
  cis4) d-. r2
  
  r2 a4-. r
  a-. r8 a-- ~ a4 \glissando c,-.
  r2 bes'4-. r
  \break
  bes-. r8 bes-- ~ bes4 \glissando c,-.
  
  r2 a'4-. r
  a-. r8 a-- ~ a4 \glissando c,-.
  r8 a'-. r a( a8. d,16 f8. gis16-.)
  r8 gis-. r f( gis4--) gis-.
  
  fis4-. r r2
  \break
  r2 r4 r8. a16--
  a4-. r r2
  r2 r4 r8. fis16--
  
  f?4-. r r2
  R1
  r8 g-. r g( f4-.) d-.
  e-. e8.( e16 d8. e16 d4-.)
  
  \mark \markup \boxed "Cups"
  R1*3
  \break
  r2 r8. bes'16-. bes8.-. bes16(
  
  g2.->) \sfp \< bes?4-. \!
  bes4-- bes8.-. bes16-- ~ bes4 bes-.
  a-.\> r8 ees8-- \p ~ ees4 r
  r4 r8 ees-. r ees4.--
  
  \break
  \repeat percent 2 {
    r4 r8 ees-- ~ ees4 r
    r4 r8 ees-. r ees4.--
  }
  
  a4-. r r2
}


% Instrument Sheet
% -------------------------------------------------

\header {
  instrument = "Trombone 3 (Eb)"
}

\paper {  	
  % distancia entre sistemas
  system-system-spacing #'basic-distance = #18
  
  % distancia entre el título y el primer sistema
  markup-system-spacing #'basic-distance = #20
}

<<
  \scoreMarkup
  \new Staff << \transpose ees c \relative c'' \tromboneThree >>
>>