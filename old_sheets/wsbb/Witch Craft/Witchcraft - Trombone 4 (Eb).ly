\version "2.16.1"
\include "imports/main.ly"


% Instrument Music
% -------------------------------------------------
tromboneFour = {

  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0
  
  % \clef bass
  \key f \major
  
  \mark \markup \boxed "Cups"
  \repeat percent 4 {
    r4 r8 g-- ~ g g4.--
  }
  
  R1
  r2 r8. f16( f8. f16
  f4-.) r4 r2
  \break
  r2 r8. f16( f8. f16
  
  f4-.) r4 r2
  R1*2
  r2 r4 r8. fis16(
  
  g4-.) r r2
  r2 r4 r8. g16--
  g4-. r r2
  r2 r4 r8. g16(
  
  \break
  ees4-.) r r2
  R1*3
  
  r2 c'4-. r
  c-. r8 c-- ~ c4 c-.
  r2 f4-. r
  f-. r8 f-- ~ f4 c-.
  
  r2 c4-. r
  \break
  c-. r8 c-- ~ c4 c-.
  r8 b-. r b( b8. f16 a8. b16-.)
  r8 b-. r gis( b4--) b-.
  
  a-. r r2
  r2 r4 r8. b16--
  c4-. r r2
  \break
  r2 r4 r8. a16--
  
  g4-. r r2
  R1
  r8 b-. r b( g4-.) \> f-.
  g-. g8.( \! g16 e8. g16 e4-.)
  
  \mark \markup \boxed "Open"
  R1*7
  \break
  d'4.-- c8-. r c4.-- \f
  
  r8 f,4.-- f4-. g-.
  g( a8. a16-.) r2
  r8 f4.-- f4-. f-.
  f( aes8. aes16-.) r2
  
  r8 aes4.-- aes4-. aes-.
  \break
  aes( b8. b16-.) r8. aes16-- aes4-.
  g4.--  d'8-. r c-. r bes(
  a4) bes-. r2
  
  r2 c4-. r
  c-. r8 c-- ~ c4 c-.
  r2 f4-. r
  \break
  f-. r8 f-- ~ f4 c-.
  
  r2 c4-. r
  c-. r8 c-- ~ c4 c-.
  r8 b-. r b( b8. f16 a8. b16-.)
  r8 b-. r gis( b4--) b-.
  
  a4-. r r2
  \break
  r2 r4 r8. b16--
  c4-. r r2
  r2 r4 r8. a16--
  
  g4-. r r2
  R1
  r8 bes-. r bes( g4-.) f-.
  g-. g8.( g16 e8. g16 e4-.)
  
  \mark \markup \boxed "Cups"
  R1*3
  \break
  r2 r8. aes16-. aes8.-. aes16(
  
  f2.->) \sfp \< g4-. \!
  g4-- g8.-. g16-- ~ g4 gis-.
  c-.\> r8 g8-- \p ~ g4 r
  r4 r8 g-. r g4.--
  
  \break
  \repeat percent 2 {
    r4 r8 g-- ~ g4 r
    r4 r8 g-. r g4.--
  }
  
  c4-. r r2
}


% Instrument Sheet
% -------------------------------------------------

\header {
  instrument = "Trombone 4 (Eb)"
}

\paper {  	
  % distancia entre sistemas
  system-system-spacing #'basic-distance = #18
  
  % distancia entre el título y el primer sistema
  markup-system-spacing #'basic-distance = #20
}

<<
  \scoreMarkup
  \new Staff << \transpose ees c'' \relative c' \tromboneFour >>
>>