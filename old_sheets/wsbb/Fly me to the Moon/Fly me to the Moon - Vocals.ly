\version "2.12.3"

\header {
	title = "Fly me to the moon"
	composer = "Bart Howard"
}

solista = \new Voice = "solista"
{
   \relative c''
   {
      % ------------------------------------------------- %
      \set Score.skipBars = ##t
      \key aes \major
      \time 4/4
      R1*10
      % ------------------------------------------------- %
      
      aes4 g f ees8 des ~
      des4. ees8 f4 aes
      g4. f8 ees4 des8 c ~
      c1
      
      \times 2/3 {r4 f ees} des8 c r bes ~
      bes4. c8 des4 f
      e4. des8 c4 bes8 aes ~
      aes2. r8 a
      
      \times 2/3 {bes f' f ~} f2.
      r4 r8 aes ~ aes4 g8 ees ~
      ees1 ~
      ees2. r8 g,
      
      \times 2/3 {aes des des ~} des2.
      r4 r8 f ~ f4 ees
      des8 c ~ c2.
      R1
      
      % ------------------------------------------------- %
      
      aes'4 g f ees8 des ~ 
      des2 r8 ees f aes
      g4. f8 ees4 des8 c ~
      c1
      
      f4 ees des c8 bes
      r8 c4. des4 f
      e2 ~ e8 des c bes
      aes2. r8 a
      
      \times 2/3 {bes f' f ~} f2.
      r4 r8 aes ~ aes4 g
      ees1 ~
      ees2. r8 e
      
      \times 2/3 {f aes, aes ~} aes2. ~
      aes2 aes4 aes8 aes ~
      aes1 ~
      aes1
      
      % ------------------------------------------------- %
      R1*7
      % ------------------------------------------------- %
      
      r2 r4 r8 f'
      \times 2/3 {f des f ~} f2.
      r4 r8 aes ~ aes4 g8 ees ~
      ees1
      
      r2 r4 r8 aes,
      aes des des2.
      r4 r8 f ~ f4 ees4
      des4. c8 ~ c2
      
      % ------------------------------------------------- %
      R1*8
      % ------------------------------------------------- %
      
      r2 r4 r8 a8
      bes f' f2.
      r4 r8 aes ~ aes4 g8 c ~
      c1 ~
      
      c2. r8 b
      \times 2/3 {c aes aes ~} aes2. ~
      aes2. r8 ees'
      \times 2/3 {ces aes aes ~} aes2. ~
      aes2. r4
      
      ges1
      g1
      aes1 ~
      aes1
      
      % ------------------------------------------------- %
      R1*2
      % ------------------------------------------------- %
      
      r2 aes8 f aes aes ~
      aes1\fermata
      \bar "||"
   }
}

coro = \new Voice = "coro"
{
   \relative c''
   {
      % ------------------------------------------------- %
      \set Score.skipBars = ##t
      \key aes \major
      \time 4/4
      R1*10
      % ------------------------------------------------- %
      
      R1*16
      
      % ------------------------------------------------- %
      
      c4 bes aes g8 f ~ 
      f2 r4 c'8 c
      bes4. aes8 g4 f8 ees ~
      es1
      
      R1*2
      <e g>1
      <f aes>2. r4
      
      r4. <des f>8 <des f>8 <des f>4 <des f>8 ~
      <des f>4 r4 r2
      
      R1*3
      r2 <des e>4 <des ees>8 <aes c>8 ~
      <aes c>1 ~
      <aes c>1
      
      % ------------------------------------------------- %
      R1*8
      % ------------------------------------------------- %
      
      <des f>2. <des fes>8 <des fes>
      
      R1*5
      
      % ------------------------------------------------- %
      R1*8
      % ------------------------------------------------- %
      
      R1*5
      
      r2 r4 r8 <g b>
      \times 2/3 {<aes c> <f aes> <f aes> ~} <f aes>2. ~
      <f aes>2. r8 <ces' ees>
      \times 2/3 {<aes ces> <fes aes> <fes aes> ~} <fes aes>2. ~
      <fes aes>2. r4
      
      R1*2
      
      % ------------------------------------------------- %
      R1*2
      % ------------------------------------------------- %
      
      R1*4
      
      \bar "||"
   }
}

lSolista = \new Lyrics \lyricsto "solista" 
{
   \lyricmode
   {
      Fly me to the moon, __ and let me play a -- mong the stars. __
      Let me see what spring __ is like on Ju -- pi -- ter and Mars. __
      In o -- ther words, __ hold my hand! __ In o -- ther words, __ dar -- ling kiss me! __
      
      Fill my heart with song, __ and let me sing for -- e -- ver more, __
      You are all I long for, all I wor -- ship and a -- dore.
      In o -- ther words, __ please be true! __ In o -- ther words, __ I love you! __
      
      In o -- ther words, __ hold my hand! __ In o -- ther words, dar -- ling kiss me! __
      
      In o -- ther words, __ please be true! __ In o -- ther words, __ in o -- ther words, __ I __ love __ you! __ I __ _ love you!
   }
}

lCoro = \new Lyrics \lyricsto "coro" 
{
   \lyricmode
   {
      Fill my heart with song, __ let me sing for -- e -- ver more, __
      A __ dore
      In o -- ther __ words __
      I love you
      O -- ther __ words
      In o -- ther words __
      In o -- ther words __
   }
}

\new ChoirStaff <<
   \solista
   \lSolista
   \coro
   \lCoro
>>