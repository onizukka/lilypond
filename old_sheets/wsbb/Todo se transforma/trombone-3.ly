intro = \relative c
{
	fis8 a fis b, ~ b2 |
	f'8 d e f ~ f2 |
	fis8 a fis a, ~ a2 |
	R1
	e1
}

verseAA = \relative c'
{
	gis4. a8^^ r gis^^ r b|
	cis4. a8 ~ a2 |
	e4. fis8^^ r e^^ r e|
	b'4 f2 d4 |
}

thirdTrombone = \new Staff
{
	\set Staff.midiInstrument = #"trombone"
	\set Staff.instrumentName = "3rd Trombone"
	\set Score.skipBars = ##t
	
	\key a \major
	\clef bass
	
	% \intro
	
	\verseAA
}

