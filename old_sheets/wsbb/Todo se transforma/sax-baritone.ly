verseAA = \relative c,
{
	e8 a^^ r fis8 ~ fis4 ~ fis8 a |
	e8 a^^ r g8 ~ g4 ~ g8 a |
	e8 a^^ r fis8 ~ fis4 ~ fis8 a |
	e8 a^^ r d8 ~ d2 |
}

baritone = \new Staff
{
	\set Staff.midiInstrument = #"baritone sax"
	\set Staff.instrumentName = "Sax. Baritone"
	\set Score.skipBars = ##t
	
	\key a \major
	\clef bass
	
	\verseAA
}

