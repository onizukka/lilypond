verseAA = \relative c''
{
	r4 cis2 b8^^ r8 |
	r4 g2 d'8^^ r8 |
	r4 cis,2 b8^^ r8 |
	r4 b4 ~ b2 |
}

fourthTrumpet = \new Staff
{
	\set Staff.midiInstrument = #"trumpet"
	\set Staff.instrumentName = "4th Trumpet"
	\set Score.skipBars = ##t
	
	\key a \major
	
	\verseAA
}

