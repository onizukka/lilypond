

verseAA = \relative c'''
{
	r4 gis2 a8^^ r8 |
	r4 fis2 a8^^ r8 |
	r4 gis,2 a8^^ r8 |
	r4 f4 ~ f2 |
}

secondTrumpet = \new Staff
{
	\set Staff.midiInstrument = #"trumpet"
	\set Staff.instrumentName = "2nd Trumpet"
	\set Score.skipBars = ##t
	
	\key a \major
	
	\verseAA
}

