verseAA = \relative c''
{
	r4 e2 e8^^ r8 |
	r4 d2 e8^^ r8 |
	r4 e,2 e8^^ r8 |
	r4 e4 ~ e2 |
}

thirdTrumpet = \new Staff
{
	\set Staff.midiInstrument = #"trumpet"
	\set Staff.instrumentName = "3rd Trumpet"
	\set Score.skipBars = ##t
	
	\key a \major
	
	\verseAA
}

