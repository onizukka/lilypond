intro = \relative c
{
	cis8 b a gis ~ gis2 |
	c8 b a g ~ g2 |
	cis8 b a gis ~ gis2 |
	R1
}

verseAA = \relative c
{
	e4. b8^^ r cis^^ r e|
	fis4. g8 ~ g2 |
	cis,4. e8^^ r b^^ r cis|
	e4 c2 a4 |
}

fourthTrombone = \new Staff
{
	\set Staff.midiInstrument = #"trombone"
	\set Staff.instrumentName = "4th Trombone"
	\set Score.skipBars = ##t
	
	\key a \major
	\clef bass
	
	% \intro
	
	\verseAA
}

