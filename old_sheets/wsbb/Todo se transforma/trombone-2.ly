intro = \relative c
{
	e8 fis e dis ~ dis2 |
	e8 g f d ~ d2 |
	e8 fis e cis ~ cis2 |
	R1
	b1
}

verseAA = \relative c'
{
	e4. b8^^ r e^^ r cis|
	fis4. b,8 ~ b2 |
	a4. gis8^^ r a^^ r fis|
	cis'4 a2 f4 |
}


secondTrombone = \new Staff
{
	\set Staff.midiInstrument = #"trombone"
	\set Staff.instrumentName = "2nd Trombone"
	\set Score.skipBars = ##t
	
	\key a \major
	\clef bass
	
	% \intro
	
	\verseAA
}

