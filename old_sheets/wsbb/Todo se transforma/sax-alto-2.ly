intro = \relative c'
{
	fis8 a fis b, ~ b2 |
	f'8 d e f ~ f2 |
	fis8 a fis a, ~ a2 |
	R1
	e1
}

verseAA = \relative c'
{
	a8 e'^^ r a,8 ~ a4 ~ a8 b |
	a8 e'^^ r a,8 ~ a4 ~ a8 d |
	a8 e'^^ r a,8 ~ a4 ~ a8 b |
	a8 e'^^ r b8 ~ b2 |
}

secondAlto = \new Staff
{
	\set Staff.midiInstrument = #"alto sax"
	\set Staff.instrumentName = "2nd Sax. Alto"
	\set Score.skipBars = ##t
	
	\key a \major
	
	%\intro
	
	\verseAA
}

