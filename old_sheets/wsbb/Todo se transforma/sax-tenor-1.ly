intro = \relative c'
{
	d8 fis e b ~ b2 |
	f8 a b d, ~ d2 |
	d8 fis e b ~ b2 |
	f8 a b d, ~ d2 |
	e1
}

verseAA = \relative c'
{
	cis8 fis^^ r b,8 ~ b4 ~ b8 e |
	cis8 fis^^ r b,8 ~ b4 ~ b8 e |
	cis8 fis^^ r b,8 ~ b4 ~ b8 e |
	cis8 fis^^ r d8 ~ d2 |
}

firstTenor = \new Staff
{
	\set Staff.midiInstrument = #"tenor sax"
	\set Staff.instrumentName = "1st Sax. Tenor"
	\set Score.skipBars = ##t
	
	\key a \major
	\clef bass
	
	% \intro
	
	\verseAA
}

