intro = \relative c'
{
	fis8 a b e, ~ e2 |
	f8 a b d, ~ d2 |
	fis8 a b e, ~ e2 |
	f8 a b d, ~ d2 |
	e1
}

verseAA = \relative c'
{
	cis8 fis^^ r b,8 ~ b4 ~ b8 e |
	cis8 fis^^ r b,8 ~ b4 ~ b8 e |
	cis8 fis^^ r b,8 ~ b4 ~ b8 e |
	cis8 fis^^ r d8 ~ d2 |
}

firstAlto = \new Staff
{
	\set Staff.midiInstrument = #"alto sax"
	\set Staff.instrumentName = "1st Sax. Alto"
	\set Score.skipBars = ##t
	
	\key a \major
	
	% \intro
	
	\verseAA
}

