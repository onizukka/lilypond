intro = \relative c'
{
	cis8 b a gis ~ gis2 |
	c8 b a g ~ g2 |
	cis8 b a gis ~ gis2 |
	R1
}

verseAA = \relative c'
{
	gis8 cis^^ r e,8 ~ e4 ~ e8 a |
	gis8 cis^^ r e,8 ~ e4 ~ e8 g |
	gis8 cis^^ r e,8 ~ e4 ~ e8 a |
	gis8 cis^^ r a8 ~ a2 |
}


secondTenor = \new Staff
{
	\set Staff.midiInstrument = #"tenor sax"
	\set Staff.instrumentName = "2nd Sax. Tenor"
	\set Score.skipBars = ##t
	
	\key a \major
	\clef bass
	
	% \intro
	
	\verseAA
}

