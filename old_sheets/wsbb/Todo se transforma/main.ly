\version "2.12.3"

\include "score.ly"
\include "swing.ly"

\header
{
	title = "Todo se transforma"
	composer = "Composer: Jorge Drexler"
	% arranger = "Arranger: Gabriel Palacios"
}

\score
{
	\layoutScore
	
	\layout {}
	\midi {
	  \context {
		 \Score
		 tempoWholesPerMinute = #(ly:make-moment 110 4)
	  }
	}
}