%% Taken from http://github.com/chicagogrooves/music_utils/blob/master/lilypond_files/lib/swing.ly
%% Fixed the TODOs and cleaned up the Scheme just a tad.

%% Example \swingIt #'8 { \groove }

swingIt =
#(define-music-function (parser location swingDiv music) (number? ly:music?)
   (_i "Apply a swing feel to music in @var{argMusic} according to @var{swingDiv}. ")
   (doSwingIt parser location swingDiv music))

#(define (doSwingIt parser location swingDiv music)
   (let*
       (;; bookeeping variables - track the current time, etc..
	(real-elapsed (ly:make-moment 0 1))
	(evt-duration (ly:make-moment 0 1))
	(measure-pos  (ly:make-moment 0 1))
	(evt-rest     0)  	
	;; configuration variables
	(swing-amt    (ly:make-moment 1 1))
	(swing-unit   (ly:make-moment 1 (/ swingDiv 2))))    
     ;; music-map yields sequential events from argMusic
     ;; into the anonymous function which is its first argument
     (music-map 
      (lambda (mus)
	;; (ly:warning "(ly:music-property mus 'name): ~a" (ly:music-property mus 'name))
	(if (eq? (ly:music-property mus 'name) 'SimultaneousMusic)
	    (ly:warning "swingIt doesn't know how to do parallel music yet.")
	    ;; Something like the following, on each simultaneous part, should work:
	    ;; (doSwingIt parser location swingDiv something-from-mus)
	    )	    
	(if (and (eq? (ly:music-property mus 'name) 'EventChord)
		 (< 0 (ly:moment-main-denominator (ly:music-length mus))))
	    (begin
	      ;; take measurements
	      (set! evt-duration (ly:music-length mus))
	      (set! measure-pos (ly:moment-mod real-elapsed swing-unit))
	      (set! real-elapsed (ly:moment-add real-elapsed evt-duration))
	      (set! evt-rest (- (ly:moment-main-numerator evt-duration) 1))
	      ;; if a note doesn't start or end on an eigth note we'll never adjust it
	      ;; (...but that's not what the below test checks for?)
	      (if (or (eq? swingDiv (ly:moment-main-denominator measure-pos))
		      (eq? swingDiv (ly:moment-main-denominator evt-duration)))
		  (begin
		    ;; Lengthen eigth notes that start on quarter notes
		    (if (eq? 0 (ly:moment-main-numerator measure-pos))		    			
			;; Lengthen the first portion of a swing unit by a factor of 4/3 (50 becomes 66.7)
			(ly:music-compress mus 
					   (ly:make-moment 
					    (+ 4 (* 3 evt-rest))
					    (+ 3 (* 3 evt-rest)))))
		    ;; like saying 'shorten eigth notes that start on eigth notes' 
		    (if (and (eq? (* 2 (ly:moment-main-denominator swing-unit))
				  (ly:moment-main-denominator evt-duration))
			     (eq? (* 2 (ly:moment-main-denominator swing-unit))
				  (ly:moment-main-denominator measure-pos)))
			;; Shrink the second portion of a swing unit by a factor of 2/3 (50 becomes 33.3)
			(ly:music-compress mus 
					   (ly:make-moment 
					    (+ 2 (* 3 evt-rest)) 
					    (+ 3 (* 3 evt-rest)))))))))
	mus)
      music)))