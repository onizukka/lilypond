intro = \relative c'
{
	fis8 a b e, ~ e2 |
	f8 a b d, ~ d2 |
	fis8 a b e, ~ e2 |
	f8 a b d, ~ d2 |
	e1
}

verseAA = \relative c''
{
	gis4. fis8^^ r gis^^ r fis|
	a4. d,8 ~ d2 |
	cis4. b8^^ r cis^^ r b|
	e4 b2 a4 |
}


firstTrombone= \new Staff
{
	\set Staff.midiInstrument = #"trombone"
	\set Staff.instrumentName = "1st Trombone"
	\set Score.skipBars = ##t
	
	\key a \major
	\clef bass
	
	 % \intro
	 
	 \verseAA
}

