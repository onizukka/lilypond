﻿verseAA = \relative c' 
{
	r16 cis cis d e8 e16 d cis8. cis16 cis8 cis16 d |
	e8 a16 gis fis8. fis16 r4 r16 cis d e ~ |
	e a gis e fis8 r8 r8 r16 cis cis cis8 d16 |
	e8. e16 a gis fis8 ~ fis16 fis r16 e16 fis gis8 a16 ~ |
	
	a8 b16 gis a8. a16 r4 \times 2/3 {r8 fis gis} |
	a8. b16 cis8 a16 gis ~ gis gis r8 \times 2/3 {r8 fis gis} |
	a8. b16 cis8 a16 gis ~ gis gis r8 fis16 gis a8 |
	r8. b16 cis a gis gis ~ gis8 r
}

verseBA = \relative c'
{
	r8 r16 e |
	
	cis' cis8 cis16 b8 a16 a16 ~ a8 r16 e a a8 b16 |
	cis cis d8 cis b16 a r4 a16 a8 b16 |
	cis d8 cis16 b a8. r4 a16 b cis8 |
	r8 d e cis16 b ~ b a b8 ~ b cis |
	
	r4 cis8 d16 cis b8 a r16 a b cis16 ~ |
	cis8 r16 a d cis b b ~ b8 r8 a b |
	cis d16 cis ~ cis b b a ~ a8 r16 gis b a8 gis16 |
	b8 a16 gis fis8. e16 ~ e8 r8
}

chorusAA = \relative c'
{
	b'16 cis b a |
	
	a4 r4 a8 b cis b16 a ~ |
	a4 r4 r2 |
	r8. e16 cis' cis cis cis ~ cis cis8. ~ cis16 b a a ~ |
	a4 r4 r4 cis16 b8 a16 ~ |
	
	a a8 fis16 ~ fis8 r8 r8. cis'16 ~ cis b8 a16 ~ |
	a a8 fis16 ~ fis8 r8 r4 cis'16 b8 a16 |
	a8 fis r4 b16 cis b8 a a |
}

chorusBA = \relative c'
{
	a'8 r8 r4 r2 |
	r2 cis16 cis b8 a a |
	a8 r8 r4 r2 |
	r1 |
}

chorusBB = \relative c'
{
	a'8 r8 r4 r2 |
	r2 cis16 cis b8 a a |
	a8 r8 r4 r2 |
	r2 cis16 cis b8 a a |
}

singer = \new Voice = "main"
{
	\set Staff.midiInstrument = #"flute"
	\set Staff.instrumentName = "Lyrics"
	\set Score.skipBars = ##t
	
	\key a \major
	
	R1*8
	\verseAA
	\verseBA
	\chorusAA
	\chorusBA
	\verseAA
	\verseBA
	\chorusAA
	\chorusBA
}

mainLyrics = \new Lyrics \lyricsto "main"
{
	Tu be -- so se_hi -- zo ca -- lor
	y lue -- go_el ca -- lor mo -- vi -- mien -- to,
	lue -- go go -- ta de su -- dor,
	y lue -- go va -- por, y lue -- go vien -- to
	
	que_en un rin -- cón de la rio -- ja 
	mo -- vió_el as -- pa de_un mo -- li -- no
	mien -- tras se pi -- sa -- ba_el vi -- no
	que be -- bió tu bo -- ca ro -- ja.

	Tu bo -- ca ro -- ja_en la mía,
	la co -- pa que gi -- ra en mi ma -- no
	mien -- tras el vi -- no ca -- í -- a su -- pe que, 
	de_al -- gún le -- ja -- no rin -- cón
	
	de_o -- tra ga -- la -- xia, 
	el a -- mor que me da -- rí -- as 
	trans -- for -- ma -- do vol -- ve -- rí -- a, un dí -- a, 
	a dar -- te las gra -- cias.
	
	Ca -- da u -- no da lo que re -- ci -- be,
	y lue -- go re -- ci -- be lo que da
	Na -- da_es más sim -- ple, no_hay o -- tra for -- ma,
	na -- da se pier -- de, to -- do se trans -- for -- ma.
	
	To -- do se trans -- for -- ma
}