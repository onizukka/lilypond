intro = \relative c
{
	d4 fis a d8 b |
	c4 a f a, |
	d4 fis a d8 a |
	c4 a f b |
	
	a8 a r8 fis8 ~ fis2 |
}

verseAA = \relative c
{
	a8 a^^ r fis8 ~ fis2 |
	a8 a^^ r g8 ~ g2 |
	a8 a^^ r fis8 ~ fis2 |
	a8 a^^ r b8 ~ b2  |
}

bass = \new Staff
{
	\set Staff.midiInstrument = #"contrabass"
	\set Staff.instrumentName = "Bass"
	\set Score.skipBars = ##t
	
	\key a \major
	\clef bass
	
	% \intro
	\verseAA
}