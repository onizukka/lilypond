intro = \relative c'
{
	fis8 a b e, ~ e2 |
	f8 a b d, ~ d2 |
	fis8 a b e, ~ e2 |
	f8 a b d, ~ d2 |
	e1
}

verseAA = \relative c'''
{
	r4 a2 b8^^ r8 |
	r4 a2 b8^^ r8 |
	r4 a,2 b8^^ r8 |
	r4 a4 ~ a2 |
}

firstTrumpet = \new Staff
{
	\set Staff.midiInstrument = #"trumpet"
	\set Staff.instrumentName = "1st Trumpet"
	\set Score.skipBars = ##t
	
	\key a \major
	
	\verseAA
}

