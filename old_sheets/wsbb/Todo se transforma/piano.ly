\include "swing.ly"

piano = \new PianoStaff
{
	\set Staff.midiInstrument = #"acoustic grand"
	\set Staff.instrumentName = "Piano"
	\set Score.skipBars = ##t
	
	\key a \major
	<<
		\context Staff = "rh"
		{
		}
		
		\context Staff = "lh"
		{
			\clef bass
		}
	>>
}

