\version "2.18.2"
\language "english"


% Descripción de funcionalidades
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% PLUGINS
% - \eolMark:					mark al final de línea
% - \bendBefore: 				solución al bendBefore (i.e. dis8 \bendBefore)
% - \rit: 						crea una porción de ritardando que debe terminar como las dinámicas, con un \!
% - \improOn, \improOff:	negra de improvisación
% - \emptyStaff:				Imprime un pentagrama vacío
% - \emptyBar:					Imprime un compás invisible (útil para indentados)
% - \glissBefore:				muestra un glissando hacia la nota
% - \shortGlissBefore:		muestra un glissando corto hacia la nota
% - \jazzTurn:					muestra el simbolo equivalente a un turn en jazz
% - \subp:						muestra "sp", subito piano
% - \ap:							(accidental padding), separa un poco una alteración de la siguiente nota
% 									(útil al usar parenthesize)
% - \optOttava:				octavaje opcional

% LAYOUT
% - El aspecto de los silencios multicompás es como los de las partituras de jazz
% - Margen extra de las articulaciones para evitar la colisión con las ligaduras
% - Glissandos bonitos

% PAPER
% - Se imprime el título, instrumento y número de página en todas las páginas
% - Solo se debe tocar el page-count (si eso). Sale bonito tal cual.

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Changelog de funcionalidades
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Plugins
% 23-09-85: Añadida función \emptyStave para imprimir un pentagrama vacío.
% 				En algunas partituras hay un pentagrama vacío antes del coda (lo cual mola bastante, por cierto)
% 05-09-14: Añadido a la función \emptyStave el esconder la armadura de la clave, que no estaba.
% 10-09-14: Mejorada la función \bendBefore
% 				Ahora es genérica, y se puede definir cualquier texto en un lateral que haga las funciones de ornamento
% 16-09-14: Mejorada la función para imprimir barras de improvisación.
% 				Antes se llamaba al comando \rs para pintar una barra que ocupaba una negra
% 				Se reemplaza \rs por \restImproOn y \restImproOff, siendo esta opción mucho más flexible.
% 				Cuando se activa \restImproOn, todo silencio se convierte en barra de improvisación, respetando su duración.
% 20-09-14: Mejorada la función para imprimir barras de improvisación.
% 				Se reemplaza \restImproOn y \restImproOff por \improOn y \improOff
% 				Ahora, también las notas del pentagrama salen como barra de improvisación.
% 				(los comandos \improvisationOn e \improvisationOff de lilypond no se llevan bien con LilyJAZZ)
% 				Se ha añadido un grob correspondiente a los slash de la fuente LilyJAZZ en el archivo lily-jazz.ly
% 01-10-14: Plugins
% 				Renombrada la función \emptyStave a \emptyStaff (stave es el plural de staff) y 
% 				corregida (ya no muestra el número de compás).
% 				Recordar que después de usar \emptyStaff se necesita redefinir el 
% 				número de compás con \set Score.currentBarNumber = #xx
% 01-10-14: Añadida la función \emptyBar que oculta un compás entero. Se puede usar para crear un 
% 				efecto de indentación en un pentagrama dado.
% 				Recordar que después de usar \emptyBar se necesita:
% 				- redefinir el número de compás con \set Score.currentBarNumber = #xx
% 				- volver a pintar la clave con \clef
% 				- volver a pintar la armadura con \key
% 15-10-14: Se le ha aumentado el grosor de los bordes de la función \boxed
% 18-10-15: Añadidas funciones de ornamento para antes de la nota:
% 				- \glissBefore: para mostrar un glissando antes de una nota
% 				- \shortGlissBefore: para mostrar un glissando corto antes de una nota
% 				- \jazzTurn: para mostrar el turn típico trompetero de jazz
% 24-10-15: Añadida funcion \subp para mostrar subito piano "sp"
% 27-10-15: Añadida funcion \ap (de accidental padding) para separar un poco el accidental de la siguiente nota
% 				al usar un paréntesis con \parenthesize, ya que se solapan el uno al otro
% 27-10-15: Añadida funcion \optOttava para imprimir octavaje opcional
% 				También he aprovechado para cambiar la fuente del texto "8va" que utilizaba Feta por defecto
% 28-10-15: Mejorada la función glissBefore y shortGlissBefore.
% 				Ahora se le pasa una lista #'(r x y) en la que "r" es la rotacion, "x" la posicion horizontal 
%				e "y" la positión vertical.

% Markup
% 23-09-85: Añadidas guías para correcta posición y markup de la indicación de tempo
% 27-10-15: Guía para posición de la indicación de tempo mejorada
% 30-10-15: El texto que se coloque sobre silencios de multicompás POR FIN coge la fuente LilyJAZZ
% 31-10-15: El texto de letras (lyricmode) ya coge la fuente LilyJAZZ

% Layout
% 23-09-85: Ahora las articulaciones van por dentro de las ligaduras. Parece que es la mejor solución
% 04-09-14: Los creciendos y decreciendos tienen la línea un poco más gruesa
% 08-09-14: Ahora no se muestra la cancelación de accidentales al haber un cambio de armadura
% 15-09-14: Añadido un poco más de margen a los reguladores
% 20-09-14: Comentada la línea que oculta la cancelación de accidentales al haber cambio de armadura.
% 				Realmente, la cancelación es muy bienvenida.

% Chords
% 09-09-14: Añadida sentencia para que los acordes menores salgan con un guión en vez de con una M minúscula
% 26-10-14: Añadido el include "chord-exception.ily" que recoge una mejor notación de los acordes.
% 				Actualmente, recoge los acordes maj7, 7(b5), 7(#5), 7(b9), 7(#9), 7(add13), maj9(add13), 9(#11)
% 				Se irá añadiendo acordes a menudo.

% Global
% 15-10-14: Todas las instrucciones que se colocaban justo antes de empezar la música están ahora
% 				recogidas en una función \global.
% 				A \global se le ha añadido instrucciones para que agrupe las corcheas de dos en dos
% 				en vez de cuatro en cuatro (el comportamiento por defecto).

% Language
% 15-10-14: Cambiado el lenguaje de entrada de notas del finlandes al inglés
% 				La diferencia es que ahora los sostenidos se escriben "-s" en vez de "-is"
% 				y los bemoles "-f" en vez de "-es".

% Estructura
% 15-10-14: Para poder autoincluir los archivos de instrumento en otros 
% 				(para montar un midi, o un score), se han movido los plugins a un archivo separado
% 				y ahora se utiliza la función \includeOnce, evitando así el problema de las
% 				dependencias circulares. 
% 18-10-15: Mejorada la manera de trabajar con papeles separados y score al mismo tiempo
% 02-11-15: Actualizado a lilyjazz v2
%				Nuevos archivos de fuente para musica y acordes
%				Mucha limpieza en lilyjazz.ily, chords.ily y main.ly

% Plugins
% 23-01-16: makePercent
%						Helpful for displaying > 2 measure percent repeats with the use of /longa rests

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Dependencias
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% \includeOnce
% Incluye una dependencia solo una vez. Así se pueden incluir los archivos en otros 
% (para montar un score por ejemplo) sin liarla parda con dependencias cíclicas.

%{
includeOnce = #(
	define-void-function (parser location filename) (string?) (
		if (not (defined? (string->symbol filename))) (
			begin (ly:parser-include-string parser (string-concatenate (list "\\include \"" filename "\"")))
			(primitive-eval (list 'define (string->symbol filename) #t))
		)
	)
)
%}

% Tamaño global 
% Se debe incluir antes que lilyjazz. Si no, lilyjazz no funciona.
% #(set-global-staff-size 10)

% includes
\include "includes/lilyjazz.ily"
\include "includes/chords.ily"
\include "includes/plugins-1.2.ily"

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~











% Disposición
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Cabecera
\header {
	title = "Don Fernando"
	instrument = "Score"
	composer = "Julio Méndez"
	arranger = ""
	poet = ""
	tagline = ""
}

% Márgenes, fuentes y distancias del papel
\paper {
	
	% distancia entre la parte superior de la página y el título
	top-markup-spacing #'basic-distance = 5

	% distancia entre el título y el primer sistema
	markup-system-spacing #'basic-distance = 25

	% distancia entre el texto y el primer sistema 
	top-system-spacing #'basic-distance = 15

	% márgen inferior
	bottom-margin = 20
	
	% intentado del primer sistema
	indent = 0
	 
	% número de páginas
	% page-count = 1

	ragged-last-bottom = ##f 

	oddHeaderMarkup = \markup {
	 \on-the-fly #not-first-page \fill-line {
		\fromproperty #'page:page-number-string
		\fromproperty #'header:title
		\fromproperty #'header:instrument
	 }
	}

	evenHeaderMarkup = \markup {
	 \on-the-fly #not-first-page \fill-line {
		\fromproperty #'header:instrument
		\fromproperty #'header:title
		\fromproperty #'page:page-number-string
	 }
	}
}

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~







% Elementos comunes de la partitura
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
	% Oculta el primer pentagrama del markup
	\override Staff.VerticalAxisGroup #'remove-first = ##t

	% Indicación de tiempo
	\once \override Score.RehearsalMark.self-alignment-X = #LEFT
	\once \override Score.RehearsalMark.extra-offset = #'(0 . 3) 
	\mark \markup "Easy Funk"
  s1*15
  
	\once \override Score.RehearsalMark.self-alignment-X = #LEFT
	\once \override Score.RehearsalMark.extra-offset = #'(-6 . 0) 
	\mark \markup {
	  \column { \boxed "A" }
	  \hspace #1
	  \column { \vspace #-.3 \musicglyph #'"scripts.varsegno" }
	  \column { \small "(with repetition)" }
	}
	s1*6
	\eolMark \markup "(To Coda on 2nd x)"
	s1*4
	
	\mark \markup \boxed "B"
	s1*3
	s1*2/4
	s1*7
	s1*5/4
	s1*3
	s1*7/4*2
	s1
	
	\mark \markup \boxed "C"
	s1*5
	
	\once \override Score.RehearsalMark.self-alignment-X = #LEFT
	\mark \markup {
	  \column { \boxed "D" }
	  \hspace #1
	  \column { "(Trombone I solo)" }
	}
	s1*32
	
	\once \override Score.RehearsalMark.self-alignment-X = #LEFT
	\mark \markup {
	  \column { \boxed "E" }
	  \hspace #1
	  \column { "(2nd solo)" }
	}
	s1*32

	\once \override Score.RehearsalMark.self-alignment-X = #LEFT
	\mark \markup {
	  \column { \boxed "F" }
	  \hspace #1
	  \column { "(3rd solo)" }
	}
	s1*24
	
	\eolMark \markup "(D.S. to Coda)"
	% s8
	\mark \markup \musicglyph #'"scripts.coda"
}

scoreBreaks = \new Staff \with { \RemoveEmptyStaves } {
	% Oculta el primer pentagrama del markup
	\override Staff.VerticalAxisGroup #'remove-first = ##t

	s1*8
	
	\tag #'score \break
	s1*7
	
	\tag #'score \break
	s1*6
	
	\tag #'score \break
	s1*4
	
	\tag #'score \break
	s1*3
	s1*2/4
	s1*3
	s1
	
	\tag #'score \break
	s1*3
	s1*5/4
	s1*2
	
	\tag #'score \break
	s1
	s1*7/4*2
	s1
	
	\tag #'score \break
	s1*5
	
	\tag #'score \break
	s1*8
	
	\tag #'score \break
	s1*8
	
	\tag #'score \break
	s1*8
	
	\tag #'score \break
	s1*8
	
	\tag #'score \break
	s1*12
	
	\tag #'score \break
	s1*8
	
	\tag #'score \break
	s1*8
	
	\tag #'score \break
	s1*20
	
	\tag #'score \break
	s1*6
	
	\tag #'score \break
	s1*6
}



% Función musical global
global = {
	% Para pintar todos los números de compás debajo del pentagrama
	% \override Score.BarNumber #'break-visibility = #end-of-line-invisible
	% \override Score.BarNumber #'direction = #DOWN
	% \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

	\compressFullBarRests

	% Arregla el problema de tener el scoreMarkup añadido
	\revert MultiMeasureRest #'staff-position
	\override Voice.Rest #'staff-position = #0	

	\set Timing.beamExceptions = #'()
	\set Timing.baseMoment = #(ly:make-moment 1/4)
	\set Timing.beatStructure = #'(1 1 1 1) 
	
	\override ParenthesesItem.font-size = #0
}

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Elementos específicos del instrumento
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Cifrado
scoreChords = \chords {
	% c1:m f2:m g2:m7
}

tromboneOneChords = \chords {
  s1*25
  
  s1*3
  s1*2/4
  s1*3
  s1
  s1*3
  s1*5/4
  s1*3
  s1*7/4*2
  s1
  
  s1*5
  
  bf1:6
  a:m
  d:m
  
}

% Música
altoSaxOne = \relative c'' {
  
	\global
	\key f \minor

	f2 \mp \( af8 ( bf df df, -> ) ~
	df2 df8 ( ef df f -> ) ~
	f4 \) ef8 \( ( f af c4 -- ) df8 (
	c bf ) df ( c bf ) df ( c4 -- ) \)
	
	\tag #'sheet \break
	R1*4
	ef,4 -- r4 ef8 \( ( f af bf -> ) ~
	bf4 ( ~ bf16 c bf af -> ) ~ af4 ( f )
	af4. ( g16 f ) c4 ( bf )
	af8 -. bf16 c ~ c2. ~
	
	\tag #'sheet \break
	c4 \) ef16 \mf ( f af d -> \< ) ~ d2 ~ 
	d4 \f c,16 ( d g e' -> \< ) ~ e2 ~ 
	e2 \ff r2

  \bar "[|:"
	\key g \minor
	\tag #'sheet \break
	\repeat volta 2 {
	  bf16 ( \f a g8 -. ) a16 ( f d g -. ) ~ g d8. -> d16 ( c d ef -. ) ~
	  ef c a8 -. bf8 ( g16 ) d' -> ~ d4 r
	  \tag #'sheet \break
	  bf'16 ( a g8 -. ) a16 ( c  bf a -. ) ~ a ef8. -> d16 ( c bf8 -. )
    f'16 ( d8. -- ) r8 a'16 f -- -> ~ f4 \< r \!
    
    \tag #'sheet \break
    bf16 ( a g8 -. ) f16 ( d c d -> ~ ) d ( c bf a -> ~ ) a8 ( f )
	  d'16 ( c8. ) c8 ( bf16 c ) d4. -- r8
	}
	\alternative {
	  { \tag #'sheet \break
	  g16 ( f d c -> ) ~ c ( bf bf' a -. ) ~ a f8. -> bf16 ( a g c -. ) ~ 
	  c a8. -> bf16 ( a g d -> ) ~ d4 r 
    \bar ":|]" }
	  { \tag #'sheet \break
	  r16 g ( f d c bf bf' a -. ) ~ a f8. -> bf16 ( a g c -. ) ~ 
	  c a8. -> bf16 ( a g d -> ) ~ d8. d16 ( c d g f ) }
	}
	
	\key a \minor
	\tag #'sheet \pageBreak
	d4 \mf e8 ( g16 d -> ) ~ d ( c a e' -. ) r16 d ( c8 -- )
	a8 ( g16 c -> ) ~ c16 ( a ) e'8 -- -> r4 f16 _( a g c, )
	\tag #'sheet \break
	g'8. -> e16 -> ( ~ e d c8 -> ) ~ c8. g16 ( a8 c16 f )
	\time 2/4
	g8 -- -> r8 r8. d16 -> ( ~
	
	\time 4/4
	\tag #'sheet \break
  d16 c a c d g8 -- e16 -> ) ~ e8 d -. r8 g -> ( ~
  g16 a e d a8 c16 d -- ) r8 e16 ( a b g e d
	\tag #'sheet \break
  c8 -. ) e16 ( a -> ) ~ a e8. -- -> r8. c'16 ( a e c a )
  d8 -- -> r r4 r4 r8 d -> ~ 
  
	\tag #'sheet \break
	d4 e8 ( g16 d -> ) ~ d ( c a e' -. ) r16 d ( c8 )
	a8 ( g16 c -> ) ~ c16 ( a ) e'8 -- -> r4 f16 _( a g c, )
	\tag #'sheet \break
	g'8. -> e16 -> ( ~ e d c8 -> ) ~ c8. g16 ( a8 c16 f )
	\time 5/4
	g8 -- -> r r4 r4 r2
	
	\time 4/4
	\tag #'sheet \break
  g8 ( d16 e ) a ( g8. ~ g16 ) c ( b a g8 -. ) e16 ( d )
  g8 ( d16 e ) a ( g8 d16 -> ) ~ d c8. -> e4 -- ->
	\tag #'sheet \break
  r8 a,16 ( \< d ) g8 ( e16 a ) b ( a8. ) b8 ( g16 c )
  \time 7/4
  c,8. -> -- \f \( ( g16 d' e g ) c, -> -- ( ~ c a c e g d ) e8 -- -> ( g,16 d' g b ) e,8 -- -> ( c16 e g c b8 -. ) \)
	\tag #'sheet \break
  c,8. -> \( ( g16 d' e g ) c, -> ~ c ( a c e g d ) e8 -- -> ( g,16 d' g b ) e,8 -- -> ( c16 e g c b c -> \sfp ~ )
  \time 4/4
  c1 \) \<

  \bar "[|:"
	\tag #'sheet \pageBreak
  \repeat volta 2 {
    a16 \( ( -> \ff a a a -> ) ~ a g8 -- -> e16 -- -> ~ e d8 -- -> c16 -- -> ~ c d g8 -. 
    c8. -- -^ b16 -- -^ ~ b8 g -> ~ g4 \) r
   	a16 \( c8 -^ b16 -^ ~ b g8 -^ a16 -^ ~ a g8 -^ e16 -- -> ~ e d8. -> \)
  }
  \alternative {
    { \tag #'sheet \break
   	  e16 ( -> e e e -> ) ~ e ( d c d ) e ( d ) c8 -> ~ c4 
      \bar ":|]" }
    { e16 ( -> e e e -> ) ~ e ( d c d ) e ( d ) c8 -> ~ c4 }
  }
  \bar "||"
  
	\tag #'sheet \break
	R1*8 
	
	a'4 -- \pp a16 ( a a a -> ) ~ a4 r8 a (
	g4 d8 e,16 e' ^> ) ~ e4 d8 ( b
	c4 ~ c8. ) c16 -> ~ c2
	r2 r4 a8 ( c
	
	\tag #'sheet \break
	d2. ) r4 
	g4 ~ g8. d16 -> ~ d2
	e2 r2
	r4 a,8 ( c16 g' ) e8 ( c16 a' -> ) ~ a8 ( g16 d )
	
	\tag #'sheet \break
	f4. -- r8 r4 f8 ( a
	e2 ) r4 g8 ( f16 e
	c4 ~ c8. e16 -> ) ~ e4 r
	r2 r4 r16 \mp c ( d e
	
	\tag #'sheet \break
	g8 -. ) g16 ( f g g ) r8 d8 -. f16 g r4
	g8 -. f16 ( f g g ) r8 d8 -. e16 g r4
	r4 g8 -. f16 e -> ~ e4 r4
	r2 r4 r16 c ( d e
	
	\tag #'sheet \break
	g8 -. ) g16 ( f g g ) r8 d8 -. f16 g r4
	g8 -. f16 ( f g g ) r8 d8 -. e16 g r4
	r4 a,8 -. \mf g16 c -> ~ c4 r4
	r4 d16 ( c d e -> ) ~ e4 r
	
	\tag #'sheet \break
	c16 ( \f c c c c c d c -> ) ~ c8. b16 -> ( ~ b c ) c8 -> ~
	c4 r4 r2
	c16 ( c c c c c d c -> ) ~ c8. b16 -> ( ~ b c ) c8 -> ~
	c4 r4 r2
	
	\key g \minor
	\tag #'sheet \break
	ef16 ( ef ef ef ef ef g d -> ~ ) d4 d8 ( bf )
	g'4. r8 r2
	ef16 ( ef ef ef ef ef g d -> ~ ) d4 d8 ( g16 c -> ) ~
	c1 \>
	
	\tag #'sheet \pageBreak
	R1*8 \!
	
	g4 -- \pp g16 ( g g g -> ) ~ g4 r8 g (
	f4 c8 d,16 d' ^> ) ~ d4 c8 ( a
	bf4 ~ bf8. ) bf16 -> ~ bf2
	r2 r4 g8 ( bf
	
	\tag #'sheet \break
	c2. ) r4 
	f4 ~ f8. c16 -> ~ c2
	d2 r2
	r4 g,8 ( bf16 f' ) d8 ( bf16 g' -> ) ~ g8 ( f16 c )
	
	\tag #'sheet \break
	ef4. -- r8 r4 ef8 ( g
  d2 ) r4 f8 ( ef16 d
	bf4 ~ bf8. d16 -> ) ~ d4 r
	r2 r4 r16 \mp bf ( c d
	
	\tag #'sheet \break
	f8 -. ) f16 ( ef f f ) r8 c8 -. ef16 f r4
	f8 -. ef16 ( ef f f ) r8 c8 -. d16 f r4
	r4 f8 -. ef16 d -> ~ d4 r4
	r2 r4 r16 bf ( c d
	
	\tag #'sheet \break
	f8 -. ) f16 ( ef f f ) r8 c8 -. ef16 f r4
	f8 -. ef16 ( ef f f ) r8 c8 -. d16 f r4
	r4 f8 -. d16 f -> ~ f4 r4
	r4 d16 ( f g f -> ) ~ f4 ef16 ( d c bf -> ) ~
	
	\tag #'sheet \break
	bf2 \> r2 \!
	R1*15
	
	\tag #'sheet \break
	d8. \p -> d16 -> ~ d8 d8 -- f8. -> f16 -> ~ f8 f8 --
	g8. -> g16 -> ~ g8 bf8 -. d,8 -. c16 bf -> ~ bf4
	g8. g16 -> r4 r4 g8 -. c16 bf -> ~ 
	\tag #'sheet \break
	bf2 \> r2 \!
	
	d8. \mp -> d16 -> ~ d8 d8 -- f8. -> d16 -> ~ d8 c8 --
	bf8. -> bf16 -> ~ bf8 a8 -- bf8. -> c16 -> ~ c4
	\tag #'sheet \break
	d8. \mf -> d16 -> ~ d8 d8 -- f8. -> d16 -^ ~ d8 c16 d -> ~
	d4 a16 \< ( g ) bf ( a g ) ef' ( d c ) bf' ( a g bf ) \!
	
	\bar "||"
  r16 \moltorall g ( f d c bf bf' a -> ) ~ a f8. -> bf16 ( a g c -> ) ~ 
  c a8. -> bf16 ( a g e -> ) ~ e2 \! \fermata
  
  \bar "|."
}


altoSaxTwo = \relative c'' {
  
	\global
	\key f \minor

	c2 \mp \( ef8 ( f af af, -> ) ~
	af4 r af8 ( c bf df -> ) ~
	df4 \) df8 \( ( ef f af4 -- ) af8 (
	g f ) af ( g f ) af ( g4 -- ) \)
	
	\tag #'sheet \break
	R1*4
	bf,4 -- r4 bf8 \( ( df ef f -> ) ~
	f4 ( ~ f16 g f ef -> ) ~ ef4 ( df )
	f4. ( ef16 df ) af4 ( g )
	f8 -. af16 af ~ af2. ~
	
	\tag #'sheet \break
	af4 \) bf16 \mf ( f' c' c -> \< ) ~ c2 ~ 
	c4 \f g16 ( bf d d -> \< ) ~ d2 ~ 
	d2 \ff r2

  \bar "[|:"
	\key g \minor
	\tag #'sheet \break
	\repeat volta 2 {
	  bf16 ( ^"(2nd x only)" a g8 -. ) r4 r2
    r2 r16 d ( g bf c bf g c, )
    \tag #'sheet \break
    f4 -. r4 r2
	  r2 r8 bf16 ( c ) a8 g16 ( f
	  
    \tag #'sheet \break
    d4 ) -- -. r4 r2
    r2 r16 d8 -> f16 -> ~ f ( g ) bf8 -- ->
	}
	\alternative {
	  { \tag #'sheet \break
	    R1
	    r2 r8 bf,16 \mf ( d f g bf c )
      \bar ":|]" }
	  { \tag #'sheet \break
	    R1
	    r2 r8. d,16 ( c d g f ) }
	}
	
	\key a \minor
	\tag #'sheet \pageBreak
	d4 \mf e8 ( g16 d -> ) ~ d ( c a e' -. ) r16 d ( c8 -- )
	a8 ( g16 c -> ) ~ c16 ( a ) e'8 -- -> r4 f16 _( a g c, )
	\tag #'sheet \break
	g'8. -> e16 -> ( ~ e d c8 -> ) ~ c8. g16 ( a8 c16 f )
	\time 2/4
	g8 -- -> r8 r8. d16 -> ( ~
	
	\time 4/4
	\tag #'sheet \break
  d16 c a c d g8 -- e16 -> ) ~ e8 d -. r8 g -> ( ~
  g16 a e d a8 c16 d -- ) r8 e16 ( a b g e d
	\tag #'sheet \break
  c8 -. ) e16 ( a -> ) ~ a e8. -- -> r8. c'16 ( a e c a )
  d8 -- -> r r4 r4 r8 d -> ~ 
  
	\tag #'sheet \break
	d4 e8 ( g16 d -> ) ~ d ( c a e' -. ) r16 d ( c8 )
	a8 ( g16 c -> ) ~ c16 ( a ) e'8 -- -> r4 f16 _( a g c, )
	\tag #'sheet \break
	g'8. -> e16 -> ( ~ e d c8 -> ) ~ c8. g16 ( a8 c16 f )
	\time 5/4
	g8 -- -> r r4 r4 r2
	
	\time 4/4
	\tag #'sheet \break
  g8 ( d16 e ) a ( g8. ~ g16 ) c ( b a g8 -. ) e16 ( d )
  g8 ( d16 e ) a ( g8 d16 -> ) ~ d c8. -> e4 -- ->
	\tag #'sheet \break
  r8 a,16 ( \< d ) g8 ( e16 a ) b ( a8. ) b8 ( g16 c )
  \time 7/4
  c,8 -. \f r r4 r4 r2 r2
	\tag #'sheet \break
  r4 r4 r4 r2 r2
  \time 4/4
  r4 r8 c16 \mp \< ( d f g a c d g, a c \! )

  \bar "[|:"
	\tag #'sheet \pageBreak
  \repeat volta 2 {
    a16 \( ( -> \ff a a a -> ) ~ a g8 -- -> e16 -- -> ~ e d8 -- -> c16 -- -> ~ c d g8 -. 
    c8. -- -^ b16 -- -^ ~ b8 g -> ~ g4 \) r
   	a16 \( c8 -^ b16 -^ ~ b g8 -^ a16 -^ ~ a g8 -^ e16 -- -> ~ e d8. -> \)
  }
  \alternative {
    { \tag #'sheet \break
      e16 ( -> e e e -> ) ~ e ( d c d ) e ( d ) c8 -> ~ c4 
      \bar ":|]" }
    { e16 ( -> e e e -> ) ~ e ( d c d ) e ( d ) c8 -> ~ c4 }
  }
  \bar "||"
  
	\tag #'sheet \break
	R1*8 
	
	e4 -- \pp e16 ( e e e -> ) ~ e4 r8 f (
	d4 b8 d,16 c' ^> ) ~ c4 b8 ( g
	a4 ~ a8. ) a16 -> ~ a2
	r2 r4 f8 ( g
	
	\tag #'sheet \break
	a2. ) r4 
	e'4 ~ e8. d16 -> ~ d2
	c2 r2
	r4 g8 ( a16 e' ) c8 ( a16 g' -> ) ~ g8 ( e16 a, )
	
	\tag #'sheet \break
	d4. -- r8 r4 c8 ( f
	b,2 ) r4 d8 ( c16 b
	a4 ~ a8. c16 -> ) ~ c4 r
	r2 r4 r16 \mp a ( c d
	
	\tag #'sheet \break
	d8 -. ) d16 ( c d d ) r8 c8 -. c16 f r4
	d8 -. c16 ( b d d ) r8 b8 -. b16 f' r4
	r4 d8 -. c16 c -> ~ c4 r4
	r2 r4 r16 a ( c d
	
	\tag #'sheet \break
	d8 -. ) d16 ( c d d ) r8 c8 -. c16 f r4
	d8 -. c16 ( b d d ) r8 b8 -. b16 f' r4
	R1
	r4 \mf a,16 ( g a c -> ) ~ c4 r
	
	\tag #'sheet \break
	a16 ( \f a a a a a c a -> ) ~ a8. g16 -> ( ~ g a ) g8 -> ~
	g4 r4 r2
	a16 ( a a a a a c a -> ) ~ a8. g16 -> ( ~ g a ) g8 -> ~
	g4 r4 r2
	
	\key g \minor
	\tag #'sheet \break
	bf16 ( bf bf bf bf bf c bf -> ~ ) bf4 bf8 ( g )
	bf4. r8 r2
	bf16 ( bf bf bf bf bf c bf -> ~ ) bf4 bf8 ( d16 g -> ) ~
	g1 \>
	
	\tag #'sheet \pageBreak
	R1*8 \!
	
	d4 -- \pp d16 ( d d d -> ) ~ d4 r8 ef (
	c4 a8 c,16 bf' ^> ) ~ bf4 a8 ( f
	g4 ~ g8. ) g16 -> ~ g2
	r2 r4 ef8 ( f
	
	\tag #'sheet \break
	g2. ) r4 
	d'4 ~ d8. c16 -> ~ c2
	bf2 r2
	r4 f8 ( g16 d' ) bf8 ( g16 f' -> ) ~ f8 ( d16 g, )
	
	\tag #'sheet \break
	c4. -- r8 r4 bf8 ( ef
  a,2 ) r4 c8 ( bf16 a
	g4 ~ g8. bf16 -> ) ~ bf4 r
	r2 r4 r16 \mp g ( bf c
	
	\tag #'sheet \break
	c8 -. ) c16 ( bf c c ) r8 bf8 -. bf16 ef r4
	c8 -. bf16 ( a c c ) r8 a8 -. a16 ef' r4
	r4 c8 -. bf16 bf -> ~ bf4 r4
	r2 r4 r16 g ( bf c
	
	\tag #'sheet \break
	c8 -. ) c16 ( bf c c ) r8 bf8 -. bf16 ef r4
	c8 -. bf16 ( a c c ) r8 a8 -. a16 ef' r4
	r4 c8 -. bf16 c -> ~ c4 r4
	r4 c16 ( d f d -> ) ~ d4 c16 ( bf g g -> ) ~
	
	\tag #'sheet \break
	g2 \> r2 \!
	R1*15
	
	\tag #'sheet \break
	bf8. \p -> bf16 -> ~ bf8 bf8 -- c8. -> c16 -> ~ c8 c8 --
	d8. -> d16 -> ~ d8 f8 -. c8 -. g16 g -> ~ g4
	f8. f16 -> r4 r4 f8 -. bf16 g -> ~ 
	\tag #'sheet \break
	g2 \> r2 \!
	
	bf8. \mp -> bf16 -> ~ bf8 bf8 -- d8. -> bf16 -> ~ bf8 a8 --
	f8. -> f16 -> ~ f8 d8 -- f8. -> a16 -> ~ a4
	\tag #'sheet \break
	bf8. \mf -> bf16 -> ~ bf8 bf8 -- d8. -> bf16 -^ ~ bf8 a16 bf -> ~
	bf4 r4 r2
	
	\bar "||"
	\tag #'sheet \break
  R1 \moltorall 
  r4 r8. d,16 -> ~ d2 \! \fermata
  
  \bar "|."
}


tenorSaxOne = \relative c'' {
  
	\global
	\key bf \minor

	df2 \mp \( ef8 ( gf af ef -> ) ~
	ef4 r bf8 ( df df ef -> ) ~
	ef4 \) df8 \( ( ef gf bf4 -- ) bf8 (
	af gf ) bf ( af gf ) bf ( af4 -- ) \)
	
	\tag #'sheet \break
	R1*4
	bf,4 -- r4 bf8 \( ( c df ef -> ) ~
	ef4 ( ~ ef16 f ef df -> ) ~ df4 ( df )
	ef4. ( df16 c ) af4 ( gf )
	ef8 -. gf16 af ~ af2. ~
	
	\tag #'sheet \break
	af4 \) f16 \mf ( af df bf' -> \< ) ~ bf2 ~ 
	bf4 \f f16 ( g a a -> \< ) ~ a2 ~ 
	a2 \ff r2

  \bar "[|:"
	\key c \minor
	\tag #'sheet \break
	\repeat volta 2 {
	  c16 ( \f bf g8 -. ) bf16 ( g ef bf' -. ) ~ bf ef,8. -> f16 ( ef f f -. ) ~
	  f c ef,8 -. f8 ( g16 ) bf -> ~ bf4 r
	  \tag #'sheet \break
	  c'16 ( bf g8 -. ) bf16 ( d  c bf -. ) ~ bf c8. -> g16 ( f c8 -. )
    f16 ( bf,8. -- ) r8 c'16 bf -- -> ~ bf4 \< r \!
    
    \tag #'sheet \break
    c16 ( bf g8 -. ) f16 ( ef c ef -> ~ ) ef ( c bf g -> ~ ) g8 ( f )
	  ef'16 ( c8. ) c8 ( bf16 c ) ef4. -- r8
	}
	\alternative {
	  { \tag #'sheet \break
	  bf'16 ( g f ef -> ) ~ ef ( c bf' f -. ) ~ f ef8. -> c'16 ( bf g d' -. ) ~ 
	  d g,8. -> c16 ( bf g ef -> ) ~ ef4 r 
    \bar ":|]" }
	  { \tag #'sheet \break
	  r16 bf' ( g f ef c bf' f -. ) ~ f ef8. -> c'16 ( bf g d' -. ) ~ 
	  d g,8. -> c16 ( bf g ef -> ) ~ ef8. d16 ( c d a' ef ) }
	}
	
	\key d \minor
	\tag #'sheet \pageBreak
	d4 \mf f8 ( a16 e -> ) ~ e ( d a' f -. ) r16 e ( d8 -- )
	a8 ( a16 d -> ) ~ d16 ( a ) f'8 -- -> r4 f16 ( c' a d, )
	\tag #'sheet \break
	a'8. -> f16 -> ( ~ f e d8 -> ) ~ d8. a16 ( c8 d16 f )
	\time 2/4
	bf8 -- -> r8 r8. e,16 -> ( ~
	
	\time 4/4
	\tag #'sheet \break
  e16 d bf d e g8 -- f16 -> ) ~ f8 e -. r8 g -> ( ~
  g16 c f, e c8 d16 e -- ) r8 f16 ( c' c a f e
	\tag #'sheet \break
  d8 -. ) f16 ( c' -> ) ~ c f,8. -- -> r8. d'16 ( c f, d a )
  e'8 -- -> r r4 r4 r8 d -> ~ 
  
	\tag #'sheet \break
	d4 f8 ( a16 e -> ) ~ e ( d a' f -. ) r16 e ( d8 )
	a8 ( a16 d -> ) ~ d16 ( a ) f'8 -- -> r4 f16 ( c' a d, )
	\tag #'sheet \break
	a'8. -> f16 -> ( ~ f e d8 -> ) ~ d8. a16 ( c8 d16 f )
	\time 5/4
	bf8 -- -> r r4 r4 r2
	
	\time 4/4
	\tag #'sheet \break
  a8 ( e16 f ) c' ( a8. ~ a16 ) d ( c c a8 -. ) f16 ( e )
  a8 ( e16 f ) c' ( a8 e16 -> ) ~ e d8. -> f4 -- ->
	\tag #'sheet \break
  r8 g,16 ( \< d' ) f8 ( c16 f ) g ( a8. ) c8 ( a16 g )
  \time 7/4
  a8. -> -- \f \( ( f16 c d f ) a -> -- ( ~ a g d f g a ) c8 -- -> ( f,16 a c d ) a8 -- -> ( d,16 f a c a8 -. ) \)
	\tag #'sheet \break
	a8. -> -- \( ( f16 c d f ) a -> -- ( ~ a g d f g a ) c8 -- -> ( f,16 a c d ) c8 -- -> ( a16 f g d' c d -> \sfp ~ )
  \time 4/4
  d1 \) \<

  \bar "[|:"
	\tag #'sheet \pageBreak
  \repeat volta 2 {
    bf16 \( ( -> \ff bf bf bf -> ) ~ bf a8 -- -> f16 -- -> ~ f e8 -- -> d16 -- -> ~ d e f8 -. 
    d'8. -- -^ c16 -- -^ ~ c8 a -> ~ a4 \) r
   	bf16 \( d8 -^ c16 -^ ~ c g8 -^ a16 -^ ~ a g8 -^ f16 -- -> ~ f e8. -> \)
  }
  \alternative {
    { \tag #'sheet \break
      f16 ( -> f f f -> ) ~ f ( e c e ) f ( e ) d8 -> ~ d4 
      \bar ":|]" }
    { f16 ( -> f f f -> ) ~ f ( e d e ) f ( e ) d8 -> ~ d4 }
  }
  \bar "||"
  
	\tag #'sheet \break
	R1*8
	
	g4 -- \pp g16 ( g g g -> ) ~ g4 r8 f (
	e4 c8 e,16 d' ^> ) ~ d4 c8 ( a
	a4 ~ a8. ) a16 -> ~ a2
	r2 r4 g8 ( c
	
	\tag #'sheet \break
	d2. ) r4 
	e4 ~ e8. c16 -> ~ c2
	d2 r2
	r4 a8 ( c16 f ) d8 ( c16 a' -> ) ~ a8 ( g16 c, )
	
	\tag #'sheet \break
	f4. -- r8 r4 d8 ( g
	c,2 ) r4 e8 ( c16 bf
	a4 ~ a8. c16 -> ) ~ c4 r
	r2 r4 r16 \mp f ( g a
	
	\tag #'sheet \break
	d,8 -. ) d16 ( c d d ) r8 d8 -. d16 f r4
	e8 -. c16 ( c d d ) r8 c8 -. d16 e r4
	r4 d8 -. c16 d -> ~ d4 r4
	r2 r4 r16 f ( g a
	
	\tag #'sheet \break
	d,8 -. ) d16 ( c d d ) r8 d8 -. d16 f r4
	e8 -. c16 ( c d d ) r8 c8 -. d16 e r4
	r4 d8 -. \mf c16 f -> ~ f4 r4
	r4 d16 ( f g f -> ) ~ f4 r
	
	\tag #'sheet \break
	d16 ( \f d d d d d c f -> ) ~ f8. c16 -> ( ~ c a ) c8 -> ~
	c4 r4 r2
	d16 ( d d d d d c f -> ) ~ f8. e16 -> ( ~ e d ) f8 -> ~
	f4 r4 r2
	
	\key c \minor
	\tag #'sheet \break
	c16 ( c c c c c bf ef -> ~ ) ef4 bf8 ( g )
	c4. r8 r2
	c16 ( c c c c c bf ef -> ~ ) ef4 bf8 ( g16 g' -> ) ~
	g1 \>
	
	\tag #'sheet \pageBreak
	R1*8 \!
	
	f4 -- \pp f16 ( f f f -> ) ~ f4 r8 ef (
	d4 bf8 d,16 c' ^> ) ~ c4 bf8 ( g
	g4 ~ g8. ) g16 -> ~ g2
	r2 r4 f8 ( bf
	
	\tag #'sheet \break
	c2. ) r4 
	d4 ~ d8. bf16 -> ~ bf2
	c2 r2
	r4 g8 ( af16 c ) c8 ( bf16 g' -> ) ~ g8 ( f16 bf, )
	
	\tag #'sheet \break
	ef4. -- r8 r4 c8 ( f
  bf,2 ) r4 d8 ( bf16 af
	g4 ~ g8. bf16 -> ) ~ bf4 r
	r2 r4 r16 \mp ef ( f g
	
	\tag #'sheet \break
	c,8 -. ) c16 ( bf c c ) r8 c8 -. c16 ef r4
	d8 -. bf16 ( bf c c ) r8 bf8 -. c16 d r4
	r4 c8 -. bf16 ef -> ~ ef4 r4
	r2 r4 r16 ef ( f g
	
	\tag #'sheet \break
	c,8 -. ) c16 ( bf c c ) r8 c8 -. c16 ef r4
	d8 -. bf16 ( bf c c ) r8 bf8 -. c16 d r4
	r4 c8 -. bf16 ef -> ~ ef4 r4
	r4 c16 ( ef f ef -> ) ~ ef4 af16 ( g f c -> ) ~
	
	\tag #'sheet \break
	c2 \> r2 \!
	R1*15
	
	\tag #'sheet \break
	c8. \p -> c16 -> ~ c8 c8 -- ef8. -> ef16 -> ~ ef8 ef8 --
	f8. -> f16 -> ~ f8 bf8 -. ef,8 -. bf16 g -> ~ g4
	g8. g16 -> r4 r4 g8 -. bf16 g -> ~ 
	\tag #'sheet \break
	g2 \> r2 \!
	
	bf8. \mp -> bf16 -> ~ bf8 bf8 -- ef8. -> bf16 -> ~ bf8 c8 --
	g8. -> g16 -> ~ g8 f8 -- g8. -> bf16 -> ~ bf4
	\tag #'sheet \break
	bf8. \mf -> bf16 -> ~ bf8 bf8 -- ef8. -> bf16 -^ ~ bf8 c16 bf -> ~
	bf4 r4 r2
	
	\bar "||"
	\tag #'sheet \break
  r16 \moltorall bf' ( g f ef c bf' f -> ) ~ f ef8. -> c'16 ( bf g d' -> ) ~ 
  d g,8. -> c16 ( bf g ef -> ) ~ ef2 \! \fermata
  
  \bar "|."
}


tenorSaxTwo = \relative c'' {
  
	\global
	\key bf \minor

	bf2 \mp \( df8 ( gf af bf, -> ) ~
	bf4 r ef8 ( gf ef ef -> ) ~
	ef4 \) ef8 \( ( af bf gf4 -- ) gf8 (
	f ef ) gf ( f ef ) gf ( f4 -- ) \)
	
	\tag #'sheet \break
	R1*4
	df,4 \( -- r4 df8 ( ef gf af -> ) ~
	af4 ( ~ af16 bf af f -> ) ~ f4 ( ef )
	af4. ( f16 df ) c4 ( bf )
	bf8 -. bf16 df ~ df2. ~
	
	\tag #'sheet \break
	df4 \) bf16 \mf ( c f ef' -> \< ) ~ ef2 ~ 
	ef4 \f g,16 ( c ef f -> \< ) ~ f2 ~ 
	f2 \ff r2

  \bar "[|:"
	\key c \minor
	\tag #'sheet \break
	\repeat volta 2 {
	  << 
	  { c'16 ( ^"(2nd x only)" bf g8 -. ) } \\
	  { \override Rest #'staff-position = #-1
	    ef8 -. _"(Play 1st x only on segno)" r }
	  >> r4 r2
    r2 r16 ef ( bf' c d c bf c, )
    \tag #'sheet \break
    g'4 -. r4 r2
	  r2 r8 bf16 ( c ) bf8 g16 ( f
	  
    \tag #'sheet \break
    ef4 ) -- -. r4 r2
    r2 r16 ef8 -> g16 -> ~ g ( bf ) c8 -- -> 
	}
	\alternative {
	  { \tag #'sheet \break
	    R1
	    r2 r8 c,16 ( \mf ef g bf c d )
      \bar ":|]" }
	  { \tag #'sheet \break
	    R1
	    r2 r8. c,16 ( g c f d ) }
	}
	
	\key d \minor
	\tag #'sheet \pageBreak
	bf4 \mf d8 ( f16 c -> ) ~ c ( bf g d' -. ) r16 c ( bf8 -- )
	e,8 ( e16 c' ^> ) ~ c16 ( e, ) d'8 ^- ^> r4 d16 ( a' f c )
	\tag #'sheet \break
	f8. -> d16 -> ( ~ d c a8 -> ) ~ a8. e16 ( a8 c16 d )
	\time 2/4
	g8 -- -> r8 r8. d16 -> ( ~
	
	\time 4/4
	\tag #'sheet \break
  d16 c g c d e8 -- d16 -> ) ~ d8 c -. r8 e -> ( ~
  e16 a e c a8 c16 c -- ) r8 d16 ( a' g f d c
	\tag #'sheet \break
  c8 -. ) d16 ( a' -> ) ~ a d,8. -- -> r8. c'16 ( a d, c f, )
  bf8 -- -> r r4 r4 r8 bf -> ~ 
  
	\tag #'sheet \break
	bf4 d8 ( f16 c -> ) ~ c ( bf g d' -. ) r16 c ( bf8 )
	e,8 ( e16 c' -> ) ~ c16 ( e, ) d'8 -- -> r4 d16 ( a' f c )
	\tag #'sheet \break
	f8. -> d16 -> ( ~ d c a8 -> ) ~ a8. e16 ( a8 c16 d )
	\time 5/4
	g8 -- -> r r4 r4 r2
	
	\time 4/4
	\tag #'sheet \break
  f8 ( e16 f ) a ( f8. ~ f16 ) c' ( a g e8 -. ) d16 ( c )
  f8 ( c16 d ) a' ( f8 c16 -> ) ~ c a8. -> d4 -- ->
	\tag #'sheet \break
  r8 e,16 ( \< c' ) d8 ( a16 d ) e ( f8. ) a8 ( f16 d ) \!
  \time 7/4
  r4 r4 r4 r2 r2
	\tag #'sheet \break
  r4 r4 r4 r2 r2
  \time 4/4
  r4 r8 e,16 \mp \< ( g a bf c f g a c e \! )

  \bar "[|:"
	\tag #'sheet \pageBreak
  \repeat volta 2 {
    f,16 \( ( -> \ff f f f -> ) ~ f c8 -- -> a16 -- -> ~ a g8 -- -> e16 -- -> ~ e g a8 -. 
    f'8. -- -^ e16 -- -^ ~ e8 c -> ~ c4 \) r
   	f16 \( a8 -^ f16 -^ ~ f d8 -^ f16 -^ ~ f d8 -^ c16 -- -> ~ c a8. -> \)
  }
  \alternative {
    { \tag #'sheet \break
      d16 ( -> d d d -> ) ~ d ( d c e ) f ( e ) d8 -> ~ d4 
      \bar ":|]" }
    { d16 ( -> d d d -> ) ~ d ( d c e ) f ( e ) d8 -> ~ d4 }
  }
  \bar "||"
  
	\tag #'sheet \break
	R1*8 
	
	f4 -- \pp f16 ( f f f -> ) ~ f4 r8 d (
	c4 a8 e16 c' ^> ) ~ c4 a8 ( g
	f4 ~ f8. ) f16 -> ~ f2
	r2 r4 g8 ( c
	
	\tag #'sheet \break
	d2. ) r4 
	R1*2
	r4 e,8 ( a16 d ) c8 ( g16 f' -> ) ~ f8 ( e16 a, )
	
	\tag #'sheet \break
	d4. -- r8 r4 c8 ( f
	a,2 ) r4 c8 ( a16 g
	f4 ~ f8. a16 -> ) ~ a4 r
	R1
	
	\tag #'sheet \break
	bf8 -. \mp bf16 ( bf bf bf ) r8 bf8 -. g16 c r4
	c8 -. a16 ( a a a ) r8 a8 -. g16 c r4
	r4 c8 -. bf16 c -> ~ c4 r4
	R1
	
	\tag #'sheet \break
	bf8 -. bf16 ( bf bf bf ) r8 bf8 -. g16 c r4
	c8 -. a16 ( a a a ) r8 a8 -. g16 c r4
	r4 c8 -. \mf bf16 f' -> ~ f4 r4
	r4 a,16 ( c d d -> ) ~ d4 r
	
	\tag #'sheet \break
	bf16 ( \f bf bf bf bf bf ) r8 r8. a16 -> ( ~ a f ) a8 -> ~
	a4 r4 r2
	bf16 ( bf bf bf bf bf ) r8 r8. a16 -> ( ~ a f ) c'8 -> ~
	c4 r4 r2
	
	\key c \minor
	\tag #'sheet \break
	af16 ( af af af af af ) r8 r4 g8 ( ef )
	g4. r8 r2
	af16 ( af af af af af g c -> ~ ) c4 g8 ( f16 d' -> ) ~
	d1 \>
	
	\tag #'sheet \pageBreak
	R1*8 \!
	
	ef4 -- \pp ef16 ( ef ef ef -> ) ~ ef4 r8 c (
	bf4 g8 d16 bf' ^> ) ~ bf4 g8 ( f
	ef4 ~ ef8. ) ef16 -> ~ ef2
	r2 r4 f8 ( bf
	
	\tag #'sheet \break
	c2. ) r4 
	R1*2
	r4 d,8 ( g16 c ) bf8 ( f16 ef' -> ) ~ ef8 ( d16 g, )
	
	\tag #'sheet \break
	c4. -- r8 r4 bf8 ( ef
  g,2 ) r4 bf8 ( g16 f
	ef4 ~ ef8. g16 -> ) ~ g4 r
	R1
	
	\tag #'sheet \break
	af8 -. \mp af16 ( af af af ) r8 af8 -. f16 bf r4
	bf8 -. g16 ( g g g ) r8 g8 -. f16 bf r4
	r4 bf8 -. af16 bf -> ~ bf4 r4
	R1
	
	\tag #'sheet \break
	af8 -. af16 ( af af af ) r8 af8 -. f16 bf r4
	bf8 -. g16 ( g g g ) r8 g8 -. f16 bf r4
	r4 bf8 -. af16 ef' -> ~ ef4 r4
	r4 g,16 ( bf c c -> ) ~ c4 f16 ( ef c af -> ) ~
	
	\tag #'sheet \break
	af2 \> r2 \!
	R1*15
	
	\tag #'sheet \break
	g8. \p -> g16 -> ~ g8 g8 -- bf8. -> bf16 -> ~ bf8 bf8 --
	c8. -> c16 -> ~ c8 ef8 -. g,8 -. f16 ef -> ~ ef4
	c8. c16 -> r4 r4 c8 -. f16 ef -> ~ 
	\tag #'sheet \break
	ef2 \> r2 \!
	
	g8. \mp -> g16 -> ~ g8 g8 -- c8. -> g16 -> ~ g8 f8 --
	d8. -> d16 -> ~ d8 f8 -- d8. -> d16 -> ~ d4
	\tag #'sheet \break
	g8. \mf -> g16 -> ~ g8 g8 -- c8. -> g16 -^ ~ g8 f16 g -> ~
	g4 r8 ef'16 \< ( d c ) ef ( d c ) ef ( d c ef \!
	
	\bar "||"
  \cadenzaOn ef8 ) -. ^ \markup {
	  \column { \box "A" }
	  \column { \vspace #-.3 \tiny \musicglyph #'"scripts.varsegno" }
	} \cadenzaOff \bar ""
  
	\tag #'sheet \break
  R1 \moltorall 
  r4 r8. ef,16 -> ~ ef2 \! \fermata
  
  \bar "|."
}




baritoneSax = \relative c'' {
  
	\global
	\key f \minor

	af2 \( \mp af8 ( bf c af -> ) ~
	af4 r4 af8 ( bf c f -> ) ~
	f4 \) c8 \( ( ef f g4 -- ) df8 (
	c bf ) f' ( ef bf ) f' ( ef4 -- ) \)
	
	\tag #'sheet \break
	R1*4
	df4 -- r4 df8 \( ( c ef af, -> ) ~
	af4 ( ~ af16 bf af c -> ) ~ c4 ( af )
	f4. ( af16 f ) df4 ( df )
	c8 -. ef16 f ~ f2. ~
	
	\tag #'sheet \break
	f4 \) af16 \mf ( c ef d, -> \< ) ~ d2 ~ 
	d1 \f \< ~
	d2 \ff r2

  \bar "[|:"
	\key g \minor
	\tag #'sheet \break
	\repeat volta 2 {
	  <<
	    { \key g \minor
        bf8 ( \mf ^"(Play on 1st x)" -. r d8 d16 ef -> ~ ef4 f4 )
        ef16 ( d8. f8 g16 d -> )~ d8 bf d16 ( g f c
        \tag #'sheet \break
        bf8 -. ) f'16 ( g bf,8 g'16 f -. ~ f ef8. -> ) ~ ef4
        r8. f16 g d8. r8 g16 ( bf ) d,8 f16 ( d )
        
        \tag #'sheet \break
        bf16 bf ( c f d8 ) d16 f -> ~ f8. f16 -> ~ f4
        c4 -- r8. d16 f bf,8 -- -> bf16 -. ~ bf ef -. r8
      }
      \new Staff \with { alignBelowContext = #"baritoneSax" } {
	      \key g \minor
        \once \omit Staff.TimeSignature
        g16 ^"(Play on 2nd x)" f bf,8 -. r4 r4 d8. c16 -> ~
        c4 ef8. f16 -> ~ f4 r16 g ( d c )
        bf4 -. r4 r2
        r2 r8 c16 ( d ) c8 bf16 ( d )
        
        bf4 -- -. r r2
        r2 r16 bf8 -> bf16 -> ~ bf ( c ) d8 -- ->
      }
    >>
	}
	
	\alternative {
	  { \tag #'sheet \break
      g8. f16 -> ~ f4 r16 ef8. -> ~ ef8. d16 -> ~
      d4 r8. g16 -> ~ g8 d16 ( f g g c bf )
      \bar ":|]" }
	  { R1*2 }
	}
	
	\key a \minor
	\tag #'sheet \pageBreak
	f1
	e1
	a,1
	\time 2/4
	r4 r8. f'16 -> ~
	
	\time 4/4
	\tag #'sheet \break
  f1
  e1
  a,1
  r2 r4 r8 f'8 -> ~ 
  
	\tag #'sheet \break
	f1
	e1
	a,1
	\time 5/4
	r4 r4 r4 r2
	
	\time 4/4
	\tag #'sheet \break
  f'1
  e1
	a,1
  \time 7/4
  R1*7/4*2
  \time 4/4
  R1

  \bar "[|:"
	\tag #'sheet \break
  \repeat volta 2 {
    << 
	    {
        a1 ^"(Play on 1st x)" 
        c8. -- -^ e16 -- -^ ~ e8 g -> ~ g4 r
        c,4 -> ~ c8. c16 -> ~ c2
      }
      \new Staff \with { alignBelowContext = #"baritoneSax" } {
	      \key a \minor
        \once \omit Staff.TimeSignature
        a1 ^"(Play on 2nd x)" 
        e'8. -- -^ a16 -- -^ ~ a8 e' -> ~ e4 r
        c,4 -> ~ c8. c16 -> ~ c2
      }
    >>
  }
  \alternative {
    { \tag #'sheet \break
      a2 r4 c16 e g a
      \bar ":|]" }
    { a,2 r4 e'16 a g c, }
  }
  \bar "||"
  
	\tag #'sheet \break
	a4 r4 r2
	R1*35
	
	\tag #'sheet \break
	R1*28
	R1*16
	R1*8
	
	\bar "||"
	\tag #'sheet \break
  R1 \moltorall 
  r4 r8. g'16 -> ~ g2 \! \fermata
  
  \bar "|."
}



trumpetOne = \relative c'' {
  
	\global
	\key bf \minor

	af2 \mp \( bf8 ( df f df -> ) ~
	df2 df8 ( ef f df -> ) ~
	df4 \) df8 \( ( ef f gf4 -- ) af8 (
	gf f ) af ( gf f ) af ( gf4 -- ) \)
	
	\tag #'sheet \break
	R1*4
	af4 -- r4 af8 \( ( bf df ef -> ) ~
	ef4 ( ~ ef16 f ef df -> ) ~ df4 ( bf )
	df4. ( c16 af ) f4 ( ef )
	df8 -. ef16 f ~ f2. ~
	
	\tag #'sheet \break
	f4 \) af16 \mf ( bf df g, -> \< ) ~ g2 ~ 
	g4 \f f16 ( g c a -> \< ) ~ a2 ~ 
	a2 \ff r2

  \bar "[|:"
	\key c \minor
	\tag #'sheet \break
	\repeat volta 2 {
	  ef'16 ^"(Play 2nd x only)" ( \f d c8 -. ) d16 ( bf g c -. ) ~ c g8. -> g16 ( f g af -. ) ~
	  af f d8 -. ef8 ( c16 ) g' -> ~ g4 r
	  \tag #'sheet \break
	  ef'16 ( d c8 -. ) d16 ( f  ef d -> ) ~ d af8. -> g16 ( f ef8 -. )
    bf'16 ( g8. -- ) r8 d'16 bf -- -> ~ bf4 \< r \!
    
    \tag #'sheet \break
    ef16 ( d c8 -. ) bf16 ( g f g -> ~ ) g ( f ef d -> ~ ) d8 ( bf )
	  g'16 ( f8. ) f8 ( ef16 f ) g4. -- r8
	}
	\alternative {
	  { \tag #'sheet \break
	    R1*2
      \bar ":|]" }
	  { r16 c ( bf g f ef ef' d -. ) ~ d bf8. -> ef16 ( d c f -. ) ~ 
	    f d8. -> ef16 ( d c g -> ) ~ g2 }
	}
	
	\key d \minor
	\tag #'sheet \pageBreak
	R1*3
	\time 2/4
	d8. -- -> \f ( a16 e' f a ) d, -- -> \sfp ~
	
	\time 4/4
	\tag #'sheet \break
  d4 \> r4 \! r2
  R1*2
  d8. -- -> \f ( a16 e' f a ) d, -- -> ( ~ d a c f a c ) g8 -- -> ~
  
	\tag #'sheet \break
	g4 \> r4 \! r2
	R1*2
	\time 5/4
	d8. -- -> \f ( a16 e' f a ) d, -- -> ( ~ d a c f a e ) f8 -- -> ( a,16 e' a c )
	
	\time 4/4
	\tag #'sheet \break
  f,4 -- -> \> r4 \! r2
  R1*2
  \time 7/4
  f8. -> -- \f \( ( c16 g' a c ) f, -> -- ( ~ f d f a c g ) a8 -- -> ( c,16 g' c e ) a,8 -- -> ( f16 a c f e8 -. ) \)
	\tag #'sheet \break
	f,8. -> -- \f \( ( c16 g' a c ) f, -> -- ( ~ f d f a c g ) a8 -- -> ( c,16 g' c e ) a,8 -- -> ( f16 a c f e f -> \sfp ~ )
  \time 4/4
  f1 \) \<

  \bar "[|:"
	\tag #'sheet \break
  \repeat volta 2 {
    d16 \( ( -> \ff d d d -> ) ~ d c8 -- -> a16 -- -> ~ a g8 -- -> f16 -- -> ~ f g a8 -. 
    f'8. -- -^ e16 -- -^ ~ e8 c -> ~ c4 \) r
   	d16 \( f8 -^ e16 -^ ~ e c8 -^ d16 -^ ~ d c8 -^ a16 -- -> ~ a g8. -> \)
  }
  \alternative {
    { \tag #'sheet \break
      a16 ( -> a a a -> ) ~ a ( g f g ) a ( g ) f8 -> ~ f4 
      \bar ":|]" }
    { a16 ( -> a a a -> ) ~ a ( g f g ) a ( g ) f8 -> ~ f4  }
  }
  \bar "||"
  
	\tag #'sheet \break
	R1*8 
	
	g4 -- \pp g16 ( g g g -> ) ~ g4 r4
	R1*7
	
	R1*8
	
	\tag #'sheet \break
	R1*2
	r4 g8 -. \mf f16 a -> ~ a4 r4
	r4 g16 ( c d c -> ) ~ c4 r
	
	\tag #'sheet \pageBreak
	c16 ( \f c c c c c c d -> ) ~ d8. c16 -> ( ~ c8 ) a8 -> ~
	a4 r4 r2
	c16 ( c c c c c c d -> ) ~ d8. c16 -> ( ~ c g ) c8 -> ~
	c4 r4 r2
	
	\key c \minor
	\tag #'sheet \break
	c16 ( c c c c c c d -> ~ ) d4 c8 ( bf )
	d4. r8 r2
	c16 ( c c c c c c d -> ~ ) d4 d8 ( f16 g -> ) ~
	g1 \>
	
	\tag #'sheet \break
	R1*8 \!
	
	f,4 -- \pp f16 ( f f f -> ) ~ f4 r4
	R1*7
	
	R1*8
	
	\tag #'sheet \break
	R1*3
	r4 f16 ( bf c bf -> ) ~ bf4 g16 ( f d c -> ) ~
	
	\tag #'sheet \break
	c2 \> r2 \!
	R1*15
	
	\tag #'sheet \break
	g'8. \p -> g16 -> ~ g8 g8 -- bf8. -> bf16 -> ~ bf8 bf8 --
	c8. -> c16 -> ~ c8 ef8 -. g,8 -. f16 ef -> ~ ef4
	c8. c16 -> r4 r4 c8 -. f16 ef -> ~ 
	\tag #'sheet \break
	ef2 \> r2 \!
	
	g8. \mp -> g16 -> ~ g8 g8 -- bf8. -> g16 -> ~ g8 f8 --
	ef8. -> ef16 -> ~ ef8 c8 -- ef8. -> f16 -> ~ f4
	\tag #'sheet \break
	g8. \mf -> g16 -> ~ g8 g8 -- bf8. -> g16 -^ ~ g8 f16 g -> ~
	g2 r2
	
	\bar "||"
	\tag #'sheet \break
  r16 \moltorall c ( bf g f ef ef' d -> ) ~ d bf8. -> ef16 ( d c f -> ) ~ 
  f d8. -> ef16 ( d c a -> ) ~ a2 \! \fermata
  
  \bar "|."
}



trumpetTwo = \relative c'' {
  
	\global
	\key bf \minor

	ef,2 \mp \( f8 ( bf df af -> ) ~
	af2 af8 ( c df bf -> ) ~
	bf4 \) bf8 \( ( af bf ef4 -- ) ef8 (
	df c ) ef ( df c ) ef ( df4 -- ) \)
	
	\tag #'sheet \break
	R1*4
	ef4 -- r4 ef8 \( ( f af bf -> ) ~
	bf4 ( ~ bf16 af bf af -> ) ~ af4 ( gf )
	bf4. ( af16 gf ) df4 ( c )
	bf8 -. df16 df ~ df2. ~
	
	\tag #'sheet \break
	df4 \) ef16 \mf ( bf af' f -> \< ) ~ f2 ~ 
	f4 \f c16 ( ef g g -> \< ) ~ g2 ~ 
	g2 \ff r2

  \bar "[|:"
	\key c \minor
	\tag #'sheet \break
	\repeat volta 2 {
	  << 
	  { \override Rest #'staff-position = #10
	    ef'8 -. ^"(Play 1st x only on segno)" r } \\
	  { ef,16 _"(Play 2nd x only)" ( \f d c8 -. ) }
	  >> 
	  r4 r2 
	  r2 r16 g ( c ef f ef c f, )
	  \tag #'sheet \break
	  bf4 -. r4 r2
	  r2 r8 ef16 ( f ) d8 c16 ( bf
    
    \tag #'sheet \break
    g'4 ) -- -. \> r4 \! r2
    r2 r16 g,8 -> bf16 -> ~ bf ( c ) ef8 -- ->
	}
	\alternative {
	  { \tag #'sheet \break
	    R1
	    r2 r8 ef,16 ( \mf f bf c ef f )
      \bar ":|]" }
	  { R1*2 }
	}
	
	\key d \minor
	\tag #'sheet \pageBreak
	R1
	bf,8. ( -- -> \mf d16 -- -> ~ d8 f -- -> ~ f16 d8. -- -> ) r4
	r2 r8. d16 ( a'8 c16 d )
	\time 2/4
	c8 -- -> r8 r4
	
	\time 4/4
  R1*4
  
	\tag #'sheet \break
	R1
	bf,8. ( -- -> d16 -- ->~ d8 f -- -> ~ f16 d8. -- -> ) r4
	r2 r8. d16 ( a'8 c16 d )
	\time 5/4
	c8 -- -> r r4 r4 r2
	
	\time 4/4
	\tag #'sheet \break
  R1*2
  r2 c16 ( \mf \< a8. ) c8 ( a16 d ) \!
  \time 7/4
  R1*7/4*2
  \time 4/4
  r4 r8 g,,16 ( \mp \< bf c d f g a bf c d \! )

  \bar "[|:"
	\tag #'sheet \break
	\repeat volta 2 {
    << 
    { f2 \f ^"(Play 1st x only)" s2 
      s2. } \\
    { c16 \( _"(Play 2nd x only)" c c c ^> ~ c c8 ^- ^> bf16 ^- ^> ~ bf a8 ^- ^> g16 ^- ^> ~ g a c8 ^.
      c8. ^- ^^ bf16 ^- ^^ ~ bf8 a ^> ~ a4 \) }
    >> r
    \tag #'sheet \break
   	c16 \( _"(Play 2nd x only)" d8 -^ c16 -^ ~ c a8 -^ d16 -^ ~ d c8 -^ g16 -- -> ~ g d8. -> \)
  }
  \alternative {
    { R1 }
    { g16 ( -> g g g -> ) ~ g ( g f g ) a ( g ) f8 -> ~ f4  }
  }
  \bar "||"
  
	\tag #'sheet \break
	R1*16
	
	R1*8
	
	R1*2
	r4 f8 -. \mf d16 f -> ~ f4 r4
	r4 f16 ( g c a -> ) ~ a4 r
	
	\tag #'sheet \pageBreak
	g16 ( \f g g g g g g a -> ) ~ a8. g16 -> ( ~ g16 e ) g8 -> ~
	g4 r4 r2
	g16 ( g g g g g g a -> ) ~ a8. g16 -> ( ~ g e ) g8 -> ~
	g4 r4 r2
	
	\key c \minor
	\tag #'sheet \break
	g16 ( g g g g g g bf -> ~ ) bf4 g8 ( bf )
	bf4 r4 r2
	g16 ( g g g g g g bf -> ~ ) bf4 g8 ( d'16 d -> ) ~
	d1 \>
	
	\tag #'sheet \break
	R1*16 \!
	
	R1*8
	
	R1*3
	r4 ef,16 ( f bf g -> ) ~ g4 ef16 ( d c c -> ) ~
	
	\tag #'sheet \break
	c2 \> r2 \!
	R1*15
	
	\tag #'sheet \break
	ef8. \p -> ef16 -> ~ ef8 ef8 -- f8. -> f16 -> ~ f8 f8 --
	g8. -> g16 -> ~ g8 c8 -. f,8 -. c16 c -> ~ c4
	bf8. bf16 -> r4 r4 bf8 -. ef16 c -> ~ 
	\tag #'sheet \break
	c2 \> r2 \!
	
	ef8. \mp -> ef16 -> ~ ef8 ef8 -- g8. -> ef16 -> ~ ef8 d8 --
	bf8. -> bf16 -> ~ bf8 g8 -- bf8. -> d16 -> ~ d4
	\tag #'sheet \break
	ef8. \mf -> ef16 -> ~ ef8 g8 -- g8. -> ef16 -^ ~ ef8 d16 ef -> ~
	ef2 r4 d'16 ( \< c bf f' \!
	
	\bar "||"
  \cadenzaOn ef8 ) -. ^ \markup {
	  \column { \box "A" }
	  \column { \vspace #-.3 \tiny \musicglyph #'"scripts.varsegno" }
	} \cadenzaOff \bar ""
  
	\tag #'sheet \break
  R1
  R1 \fermataMarkup
  
  \bar "|."
}



trumpetThree = \relative c'' {
  
	\global
	\key f \minor

	af2 \mp \( bf8 ( df ef f, -> ) ~
	f2 f8 ( af af bf -> ) ~
	bf4 \) af8 \( ( bf df f4 -- ) f8 (
	ef df ) f ( ef df ) f ( ef4 -- ) \)
	
	\tag #'sheet \break
	R1*4
	af4 -- r4 af8 \( ( bf c df -> ) ~
	df4 ( ~ df16 c df c -> ) ~ c4 ( bf )
	df4. ( c16 bf ) f4 ( ef )
	ef8 -. df16 f ~ f2. ~
	
	\tag #'sheet \break
	f4 \) g16 \mf ( f af bf -> \< ) ~ bf2 ~ 
	bf4 \f r8. a16 ~ -> \< ~ a2 ~ 
	a2 \ff r2

  \bar "[|:"
	\key g \minor
	\tag #'sheet \break
	\repeat volta 2 {
	  << 
	  { % \override Rest #'staff-position = #10
	    g8 -. ^"(Play 1st x only on segno)" r } \\
	  { g16 _"(Play 2nd x only)" ( \f f d8 -. ) }
	  >> 
	  r4 r2 
	  r2 r16 bf ( f' g a g f bf, )
	  \tag #'sheet \break
	  d4 -. r4 r2
	  r2 r8 f16 ( g ) f8 d16 ( c
    
    \tag #'sheet \break
    d4 ) -- -. r4 \! r2
    r2 r16 bf8 -> d16 -> ~ d ( f ) g8 -- ->
	}
	\alternative {
	  { \tag #'sheet \break
	    R1
	    r2 r8 g,16 ( \mf bf d f g a )
      \bar ":|]" }
	  { R1*2 }
	}
	
	\key a \minor
	\tag #'sheet \pageBreak
	R1
	a,8. ( -- -> \mf c16 -- -> ~ c8 g' -- -> ~ g16 e8. -- -> ) r4
	r2 r8. e16 ( c'8 d16 e )
	\time 2/4
	e8 -- -> r8 r4
	
	\time 4/4
  R1*4
  
	\tag #'sheet \break
	R1
	a,,8. ( -- -> c16 -- -> ~ c8 g' -- -> ~ g16 e8. -- -> ) r4
	r2 r8. e16 ( c'8 d16 e )
	\time 5/4
	e8 -- -> r r4 r4 r2
	
	\time 4/4
	\tag #'sheet \break
  R1*2
  r2 d,16 ( \mf \< e8. ) e8 ( c16 b ) \!
  \time 7/4
  R1*7/4*2
  \time 4/4
  R1

  \bar "[|:"
	\tag #'sheet \break
	\repeat volta 2 {
    f'16 \( f f f ^> ~ f e8 ^- ^> d16 ^- ^> ~ d c8 ^- ^> r16 r4
    e8. ^- ^^ d16 ^- ^^ ~ d8 c ^> ~ c4 \) r
   	\tag #'sheet \break
   	e16 _( g8 -^ e16 -^ ~ e c8 -^ e16 -^ ~ e c8 -^ b16 -- -> ~ b a8. -> )
  }
  \alternative {
    { a16 _\( ( -> a a a -> ) ~ a ( a g b ) c ( b ) a8 -> ~ a4 \) }
    { a16 _\( ( -> a a a -> ) ~ a ( a g b ) c ( b ) a8 -> ~ a4 \) }
  }
  \bar "||"
  
	\tag #'sheet \break
	R1*8
	
	g'4 -- g16 g g g ~ g4 r8 f (
	d4 b8 e,16 a -> ) ~ a4 r
	R1*5
	r4 g8 ( a16 e' ) c8 ( a16 g' -> ) ~ g8 ( d16 a )
	
	\tag #'sheet \break
	d4. -- r8 r2
	R1*7
	
	R1*2
	r4 g8 -. \mf f16 g -> ~ g4 r4
	r4 a16 ( c e c -> ) ~ c4 r
	
	\tag #'sheet \pageBreak
	a16 ( \f a a a a a a b -> ) ~ b8. g16 -> ( ~ g16 f ) a8 -> ~
	a4 r4 r2
	a16 ( a a a a a a b -> ) ~ b8. g16 -> ( ~ g f ) c'8 -> ~
	c4 r4 r2
	
	\key g \minor
	\tag #'sheet \break
	bf16 ( bf bf bf bf bf bf d -> ~ ) d4 a8 ( c )
	d4 r4 r2
	bf16 ( bf bf bf bf bf bf d -> ~ ) d4 bf8 ( g16 f' -> ) ~
	f1 \>
	
	\tag #'sheet \break
	R1*8 \!
	
	f,4 -- f16 ( f f f -> ) ~ f4 r8 ef (
	c4 a8 d,16 g -> ) ~ g4 r
	R1*5
	r4 f8 ( g16 d' ) bf8 ( g16 f' -> ) ~ f8 ( c16 g )
	
	\tag #'sheet \break
	c4. -- r8 r2
	R1*7
	
	R1*3
	r4 g'16 ( a g a -> ) ~ a4 g16 ( f c d -> ) ~
	
	\tag #'sheet \break
	d2 \> r2 \!
	R1*15
	
	\tag #'sheet \break
	g8. \p -> g16 -> ~ g8 g8 -- bf8. -> bf16 -> ~ bf8 bf8 --
	c8. -> c16 -> ~ c8 f8 -. bf,8 -. f16 d -> ~ d4
	d8. d16 -> r4 r4 d8 -. f16 d -> ~ 
	\tag #'sheet \break
	d2 \> r2 \!
	
	f8. \mp -> f16 -> ~ f8 f8 -- bf8. -> f16 -> ~ f8 g8 --
	d8. -> d16 -> ~ d8 d8 -- d8. -> f16 -> ~ f4
	\tag #'sheet \break
	f8. \mf -> f16 -> ~ f8 f8 -- bf8. -> f16 -^ ~ f8 g16 f -> ~
	f2 r16 d'16 ( \< c bf ) d ( c bf c \!
	
	\bar "||"
  \cadenzaOn g8 -. ) ^ \markup {
	  \column { \box "A" }
	  \column { \vspace #-.3 \tiny \musicglyph #'"scripts.varsegno" }
	} \cadenzaOff \bar ""
  
	\tag #'sheet \break
  R1
  R1 \fermataMarkup
  
  \bar "|."
}



trumpetFour = \relative c'' {
  
	\global
	\key f \minor

	R1*8
	
	\tag #'sheet \break
	f4 -- r4 f8 \( ( g af bf -> ) ~
	bf4 ( ~ bf16 c bf af -> ) ~ af4  af --
	bf4. ( af16 g ) ef4 ( df )
	bf8 -. df16 ef ~ ef2. ~
	
	\tag #'sheet \break
	ef4 \) r8. f16 -> \mf \< ~ f2 ~ 
	f4 \f r8. e16 -> \< ~ e2 ~ 
	e2 \ff r2

  \bar "[|:"
	\key g \minor
	\tag #'sheet \break
	\repeat volta 2 {
	  d8 -. ^"(Play 1st x only on segno)" r r4 r2
	  R1*5
	}
	\alternative {
	  { \tag #'sheet \break
	    R1*2
      \bar ":|]" }
	  { R1*2 }
	}
	
	\key a \minor
	\tag #'sheet \pageBreak
	R1
	c,8. ( -- -> \mf f16 -- -> ~ f8 c'8 -- -> ~ c16 g8. -- -> ) r4
	R1
	\time 2/4
	c8. -- -> \f ( g16 d' e g ) c, -> \sfp ~
	
	\time 4/4
	\tag #'sheet \break
  c4 \> r4 \! r2
  R1*2
  c8. -- -> \f ( g16 d' e g ) c, -- -> ( ~ c a c e g d ) e8 -> ~
  
	\tag #'sheet \break
	e4 \> r4 \! r2
	c,8. ( -- -> \mf f16 -> ~ f8 c'8 -> ~ c16 g8. -> ) r4
	R1
	\time 5/4
	a8. ( -- -> \f e16 b' c e ) a, -- -> ( ~ a e g c e b ) c8 -- -> ( e,16 b' e g )
	
	\time 4/4
	\tag #'sheet \break
  e4 -> \> r4 \! r2
  R1*2
  \time 7/4
  R1*7/4
	\tag #'sheet \break
	c8. -> -- \f \( ( g16 d' e g ) c, -> -- ( ~ c a c e g d ) e8 -- -> ( g,16 d' g b ) e,8 -- -> ( c16 e g c b c -> \sfp ~ )
  \time 4/4
  c1 \) \<

  \bar "[|:"
	\tag #'sheet \break
  \repeat volta 2 {
    c,16 ( -> \mf c c c -> ) ~ c4 r2
    R1*2
  }
  \alternative {
    { R1
      \bar ":|]" }
    { R1  }
  }
  \bar "||"
  
	\tag #'sheet \break
	R1*8 
	
	e4 -- \pp e16 ( e e e -> ) ~ e4 r8 f (
	d4 b8 e,16 a -> ) ~ a4 r4
	R1*5
	r4 e8 ( g16 a ) g8 ( g16 e' -> ) ~ e8 ( d16 g, )
	
	\tag #'sheet \break
	c4. -- r8 r2
	R1*7
	
	R1*2
	r4 d8 -. \mf c16 e -> ~ e4 r4
	r4 e16 ( g a g -> ) ~ g4 r
	
	\tag #'sheet \pageBreak
	f16 ( \f f f f f f a g -> ) ~ g8. d16 -> ( ~ d16 f ) e8 -> ~
	e4 r4 r2
	f16 ( f f f f f a g -> ) ~ g8. d16 -> ( ~ d16 c ) f8 -> ~
	f4 r4 r2
	
	\key g \minor
	\tag #'sheet \break
	g16 ( g g g g g g a -> ~ ) a4 f8 ( g )
	bf4. r8 r2
	g16 ( g g g g g g a -> ~ ) a4 f8 ( d16 c' -> ) ~
	c1 \>
	
	\tag #'sheet \break
	R1*8 \!
	
	d,4 -- \pp d16 ( d d d -> ) ~ d4 r8 ef (
	c4 a8 d,16 g -> ) ~ g4 r
	R1*5
	r4 d8 ( f16 g ) f8 ( f16 d' -> ) ~ d8 ( c16 f, )
	
	\tag #'sheet \break
	bf4. -- r8 r2
	R1*7
	
	R1*3
	r4 d16 ( f c f -> ) ~ f4 ef16 ( d c bf -> ) ~
	
	\tag #'sheet \break
	bf2 \> r2 \!
	R1*15
	
	\tag #'sheet \break
	d8. \p -> d16 -> ~ d8 d8 -- f8. -> f16 -> ~ f8 f8 --
	g8. -> g16 -> ~ g8 bf8 -. d,8 -. c16 bf -> ~ bf4
	g8. g16 -> r4 r4 g8 -. c16 bf -> ~ 
	\tag #'sheet \break
	bf2 \> r2 \!
	
	d8. \mp -> d16 -> ~ d8 d8 -- g8. -> d16 -> ~ d8 c8 --
	a8. -> a16 -> ~ a8 g8 -- a8. -> c16 -> ~ c4
	\tag #'sheet \break
	d8. \mf -> d16 -> ~ d8 d8 -- g8. -> d16 -^ ~ d8 c16 d -> ~
	d4 r8 bf'16 ( a g ) bf ( a g ) bf ( a g bf
	
	\bar "||"
  \cadenzaOn d,8 -. ) ^ \markup {
	  \column { \box "A" }
	  \column { \vspace #-.3 \tiny \musicglyph #'"scripts.varsegno" }
	} \cadenzaOff \bar ""
	
	\tag #'sheet \break
  R1
  R1 \! \fermata
  
  \bar "|."
}



tromboneOne = \relative c'' {
  
	\global
	\key bf \minor

  R1*4

	gf'4 \mp \( r4 af8 ( bf df bf -> ) ~
	bf4 r bf8 ( af gf bf -> ) ~
	bf4 \) ef,8 \( ( gf bf c4 -- ) df8 (
	c bf ) df ( c bf ) df ( c4 -- ) \)
	
	\tag #'sheet \break
	df,4 -- r4 df8 \( ( ef f gf -> ) ~
	gf4 ( ~ gf16 af gf f -> ) ~ f4 ( ef )
	gf4. ( f16 ef ) bf4 ( af )
	gf8 -. bf16 bf ~ bf2. ~
	
	\tag #'sheet \break
	bf4 \) bf16 \mf ( af f' ef -> \< ) ~ ef2 ~ 
	ef4 \f g16 ( bf c d -> \< ) ~ d2 ~ 
	d2 \ff r2

  \bar "[|:"
	\key c \minor
	\tag #'sheet \break
	\repeat volta 2 {
	  ef16 ( \f d c8 -. ) d16 ( bf g c -> ) ~ c g8. -> g16 ( f g af -. ) ~
	  af f d8 -. ef8 ( c16 ) g' -> ~ g4 r
	  \tag #'sheet \break
	  ef'16 ( d c8 -. ) d16 ( f  ef d -> ) ~ d af8. -> g16 ( f ef8 -. )
    bf'16 ( g8. -- ) r8 d'16 bf -- -> ~ bf4 \< r \!
    
    \tag #'sheet \break
    ef16 ( d c8 -. ) bf16 ( g f g -> ~ ) g ( f ef d -> ~ ) d8 ( bf )
	  g'16 ( f8. ) f8 ( ef16 f ) g4. -- r8
	}
	\alternative {
	  { \tag #'sheet \break
	    c16 ( bf g f ) -> ~ f ( ef ef' d -. ) ~ d bf8. -> ef16 ( d c f -. ) ~ 
	    f d8. -> ef16 ( d c g -> ) ~ g2
      \bar ":|]" }
	  { r16 c ( bf g f ef ef' d -. ) ~ d bf8. -> ef16 ( d c f -. ) ~ 
	    f d8. -> ef16 ( d c g -> ) ~ g2 }
	}
	
	\key d \minor
	\tag #'sheet \pageBreak
	R1*3
	\time 2/4
	a8. ( -- -> \f f16 c d f ) a -> \sfp ~ 
	
	\time 4/4
	\tag #'sheet \break
  a4 \> r4 \! r2
  R1*2
  a8. ( -- -> \f f16 c d f ) a -- -> ~ ( a g d f a c ) g8 -> ~
  
	\tag #'sheet \break
	g4 \> r4 \! r2
	R1*2
	\time 5/4
	f8. ( -- -> \f c16 g' a c ) f, -- -> ~ ( f d f a c g ) a8 ( -- -> c,16 g' c d )
	
	\time 4/4
	\tag #'sheet \break
  c4 -> \> r4 \! r2
  R1*2
  \time 7/4
  f,8. -> -- \f \( ( c16 g' a c ) f, -> -- ( ~ f d f a c g ) a8 -- -> ( c,16 g' c e ) a,8 -- -> ( f16 a c f e8 -. ) \)
	\tag #'sheet \break
	f,8. -> -- \f \( ( c16 g' a c ) f, -> -- ( ~ f d f a c g ) a8 -- -> ( c,16 g' c e ) a,8 -- -> ( f16 a c f e a, -> \sfp ~ )
  \time 4/4
  a1 \) \<

  \bar "[|:"
	\tag #'sheet \break
  \repeat volta 2 {
    d16 \( ( -> \ff d d d -> ) ~ d c8 -- -> a16 -- -> ~ a g8 -- -> f16 -- -> ~ f g a8 -. 
    f'8. -- -^ e16 -- -^ ~ e8 c -> ~ c4 \) r
   	d16 \( f8 -^ e16 -^ ~ e c8 -^ d16 -^ ~ d c8 -^ a16 -- -> ~ a g8. -> \)
  }
  \alternative {
    { \tag #'sheet \break
      a16 ( -> a a a -> ) ~ a ( g f g ) a ( g ) f8 -> ~ f4 
      \bar ":|]" }
    { a16 ( -> a a a -> ) ~ a ( g f g ) a ( g ) f8 -> ~ f4  }
  }
  \bar "||"
  
	\tag #'sheet \pageBreak
  \improOn \repeat unfold 16 { r4 } \improOff 
  
	\tag #'sheet \break
  R1*4 ^"(repeat chords until E)"
	R1*8
  R1*8
	R1*8
	\improOff
	
	\key c \minor
	\tag #'sheet \break
	R1*4
	
	\tag #'sheet \break
	R1*8 \!
	
	bf4 -- \pp bf16 ( bf bf bf -> ) ~ bf4 r4
	R1*7
	
	R1*8
	
	\tag #'sheet \break
	R1*3
	r4 f16 ( bf c bf -> ) ~ bf4 g16 ( f d c -> ) ~
	
	\tag #'sheet \break
	c2 \> r2 \!
	R1*15
	
	\tag #'sheet \break
	g'8. \p -> g16 -> ~ g8 g8 -- bf8. -> bf16 -> ~ bf8 bf8 --
	c8. -> c16 -> ~ c8 ef8 -. g,8 -. f16 ef -> ~ ef4
	c8. c16 -> r4 r4 c8 -. f16 ef -> ~ 
	\tag #'sheet \break
	ef2 \> r2 \!
	
	g8. \mp -> g16 -> ~ g8 g8 -- bf8. -> g16 -> ~ g8 f8 --
	ef8. -> ef16 -> ~ ef8 c8 -- ef8. -> f16 -> ~ f4
	\tag #'sheet \break
	g8. \mf -> g16 -> ~ g8 g8 -- bf8. -> g16 -^ ~ g8 f16 g -> ~
	g4 r8 ef16 ( d c ) af' ( g f ) ef' ( d c ef )
	
	\bar "||"
	\tag #'sheet \break
  r16 \moltorall c ( bf g f ef ef' d -> ) ~ d bf8. -> ef16 ( d c f -> ) ~ 
  f d8. -> ef16 ( d c a -> ) ~ a2 \! \fermata
  
  \bar "|."
}



tromboneTwo = \relative c'' {
  
	\global
	\key bf \minor

  R1*4

	df4 \mp \( r4 ef8 ( gf af df, -> ) ~
	df4 r gf8 ( f ef gf -> ) ~
	gf4 \) bf,8 \( ( ef gf bf4 -- ) bf8 (
	af gf ) bf ( af gf ) bf ( gf4 -- ) \)
	
	\tag #'sheet \break
	gf,4 -- r4 gf8 \( ( af bf c -> ) ~
	c4 ( ~ c16 df c bf -> ) ~ bf4 ( af )
	bf4. ( af16 f ) ef4 ( df )
	bf8 -. df16 ef ~ ef2. ~
	
	\tag #'sheet \break
	ef4 \) df16 \mf ( f af gf -> \< ) ~ gf2 ~ 
	gf4 \f d'16 ( ef f g -> \< ) ~ g2 ~ 
	g2 \ff r2

  \bar "[|:"
	\key c \minor
	\tag #'sheet \break
	\repeat volta 2 {
	  << { g16 ^"(Play 2nd x only)" ( \f f ef8 -. ) } \\ { ef8 -. _"(Play 1st x only on segno)" r } >> r4 r2
	  r2 r16 c ( g' bf c g f bf, )
	  \tag #'sheet \break
	  f'4 -- r4 r2
	  r2 r8 f16 ( af ) f8 ef16 ( c
    
    \tag #'sheet \break
    bf4 ) -- -. r r2
    r2 r16 c8 -> ef16 -> ~ ef ( f ) g8 -- ->
	}
	\alternative {
	  { \tag #'sheet \break
	    R1
	    r2 r8 g,16 ( bf c f g bf )
      \bar ":|]" }
	  { R1*2 }
	}
	
	\key d \minor
	\tag #'sheet \pageBreak
	d,1 \p 
	bf8. ( \mf -- -> d16 -- -> ~ d8 f -- -> d4 -- -> ) r
	r2 r8. d16 ( a'8 c16 d )
	\time 2/4
	c8 -- -> r8 r8. \p c,16 -> ~
	
	\time 4/4
	\tag #'sheet \break
  c1
  c1
  d1
  r2 r4 r8 \p c ->  ~
  
	\tag #'sheet \break
	c1
	bf8. -- -> \mf d16 -- -> ~ d8 f -- -> d4 -- ->  r
	r2 r8. d16 ( a'8 c16 d )
	\time 5/4
	c8 -- -> r r4 r r2
	
	\time 4/4
	\tag #'sheet \break
  c,1 \p 
  c1
  r2 c'16 ( \mf \< a8. ) c8 ( a16 d ) \!
  \time 7/4
  R1*7/4*2
  \time 4/4
  r2 g,,16 ( \p \< a bf c f g bf d )

  \bar "[|:"
	\tag #'sheet \break
  \repeat volta 2 {
    d,16 \( ( -> \mf d d d -> ) ~ d c8 -- -> a16 -- -> ~ a g8 -- -> f16 -- -> ~ f g a8 -. 
    f'8. -- -^ e16 -- -^ ~ e8 c -> ~ c4 \) r
   	d16 \( f8 -^ e16 -^ ~ e c8 -^ d16 -^ ~ d c8 -^ a16 -- -> ~ a g8. -> \)
  }
  \alternative {
    { \tag #'sheet \break
      a16 ( -> a a a -> ) ~ a ( g f g ) a ( g ) f8 -> ~ f4 
      \bar ":|]" }
    { a16 ( -> a a a -> ) ~ a ( g f g ) a ( g ) f8 -> ~ f4  }
  }
  \bar "||"
  
	\tag #'sheet \pageBreak
  R1*8
  
  d'4 -- d16 d d d ~ d4 r8 bf (
  a4 f8 e16 a -> ) ~ a4 f8 ( e
  d4 ~ d8. ) d16 -> ~ d2
  R1
  
	\tag #'sheet \break
  R1*3
  r4 d8 ( f16 c' ) a8 ( f16 d' -> ) ~ d8 ( c16 a )
  
	\tag #'sheet \break
  bf4. -- r8 r2
	R1*10
	r4 \mf g'16 ( c d c -> ) ~ c4 r4
	
	\tag #'sheet \break
	c16 \f ( c c c c c c d -> ) ~ d8 r r4
	R1
	c16 ( c c c c c c d -> ) ~ d8 r r4
	R1
	
	\key c \minor
	\tag #'sheet \break
	c16 ( c c c c c c d -> ) ~ d8 r r4
	R1
	c16 ( c c c c c c d -> ) ~ d8 r r4
	R1
	
	\tag #'sheet \break
	R1*8
	
	c,4 -- c16 c c c ~ c4 r8 af (
  g4 ef8 d16 g -> ) ~ g4 ef8 ( d
  c4 ~ c8. ) c16 -> ~ c2
  R1
  
	\tag #'sheet \break
  R1*3
  r4 c8 ( ef16 bf' ) g8 ( ef16 c' -> ) ~ c8 ( bf16 g )
  
	\tag #'sheet \break
  af4. -- r8 r2
	R1*11
	
	\tag #'sheet \break
	R1*16
	
	R1*2
	g8. g16 -> r4 r2
	R1
	
	\tag #'sheet \break
	R1*3
	af16 ( \< g f ) af ( g f ) af ( g f ) af ( g f ) af ( g f af \!
	
	\bar "||"
  \cadenzaOn ef'8 -. ) ^ \markup {
	  \column { \box "A" }
	  \column { \vspace #-.3 \tiny \musicglyph #'"scripts.varsegno" }
	} \cadenzaOff \bar ""
	
	\tag #'sheet \break
  R1 \moltorall 
  r4 r8. g16 ~ g2 \! \fermata
  
  \bar "|."
}



tromboneThree = \relative c'' {
  
	\global
	\key f \minor

  R1*4

	f,4 \mp \( r4 f8 ( af bf f -> ) ~
	f4 r f8 ( af bf af -> ) ~
	af4 \) f8 \( ( af bf ef4 -- ) df8 (
	c bf ) df ( c bf ) df ( c4 -- ) \)
	
	\tag #'sheet \break
	R1*2
	df4. \( ( ef16 df ) ef4 ( df )
	af8 -. c16 ef ~ ef2. ~
	
	\tag #'sheet \break
	ef4 \) r4 r2
	R1*2

  \bar "[|:"
	\key g \minor
	\tag #'sheet \break
	\repeat volta 2 {
	  R1*6
	}
	\alternative {
	  { R1*2
      \bar ":|]" }
	  { R1
	    r2 r8. g,16 ( ef' g, c a ) }
	}
	
	\key a \minor
	\tag #'sheet \pageBreak
	f'1 \p 
	a,8. ( -- -> \mf c16 -- -> ~ c8 g' -- -> e4 -- -> ) r
	e1 \p
	\time 2/4
	r4 r8. e16 -> ~
	
	\time 4/4
	\tag #'sheet \break
  e1
  e1
  e1
  r2 r4 r8 \p e -> ~
  
	\tag #'sheet \break
	e1
	a,8. -- -> \mf c16 -- -> ~ c8 g' -- -> e4 -- -> r
	e1
	\time 5/4
	r4 r r r2
	
	\time 4/4
	\tag #'sheet \break
  e1 \p 
  e1
  r2 d16 ( \mf \< c8. ) g'8 ( c,16 b ) \!
  
	\tag #'sheet \break
  \time 7/4
  R1*7/4*2
  \time 4/4
  r2 r4 c,16 \mf \< ( f g c \! )

  \bar "[|:"
	\tag #'sheet \break
  \repeat volta 2 {
    <<
    { c,1 ^"(Play 1st x only)"
      g'8. -- -^ a16 -- -^ ~ a8 c -> ~ c4 r
   	  f,4 -> ~ f8. e16 -> ~ e2 }
    \new Staff {
	    \key a \minor
      \once \omit Staff.TimeSignature
      c1 ^"(Play 2nd x only)" 
      a'8. -- -^ d16 -- -^ ~ d8  g -> ~ g4 r
      f,4 -> ~ f8. e16 -> ~ e2
    }
    >>
  }
  \alternative {
    { \tag #'sheet \break
      c2 r4 e16 g a c
      \bar ":|]" }
    { c,2 r4 g'16 c b e,  }
  }
  \bar "||"
  
	\tag #'sheet \pageBreak
	c4 r4 r2
  R1*3
  
  R1*24
	
	\tag #'sheet \break
	f16 ( \f f f f f f f a -> ) ~ a8 r r4
	R1
	f16 ( f f f f f f a -> ) ~ a8 r r4
	R1
	
	\key g \minor
	\tag #'sheet \break
	ef16 ( ef ef ef ef ef ef g -> ) ~ g8 r r4
	R1
	ef16 ( ef ef ef ef ef ef g -> ) ~ g8 r r4
	R1
	
	\tag #'sheet \break
	R1*28
  
	\tag #'sheet \break
	R1*16
	R1*2
	g8. g16 -> r4 r2
	R1
	
	\tag #'sheet \break
	R1*3
	bf16 ( \< a g ) bf ( a g ) bf ( a g ) bf ( a g ) bf ( a g bf ) \!
	
	\bar "||"
	\tag #'sheet \break
	
  R1 \moltorall 
  R1 \! \fermataMarkup
  
  \bar "|."
}




tromboneFour = \relative c'' {
  
	\global
	\key f \minor

  R1*4
	df,4 \( r4 \mp df8 ( ef g df -> ) ~
	df4 r4 df8 ( ef g f -> ) ~
	f4 \) df8 \( ( f g c4 -- ) bf8 (
	af f ) bf ( af f ) bf ( af4 -- ) \)
	
	\tag #'sheet \break
	R1
	r2 r4 c \(
	bf4. ( c16 bf ) af4 ( f )
	f8 -. af16 bf ~ bf2. ~
	
	\tag #'sheet \break
	bf4 \) r4 r2
	r4 f16 \f ( af bf c -> \< ) ~ c2 ~ 
	c2 \ff r2

  \bar "[|:"
	\key g \minor
	\tag #'sheet \break
	\repeat volta 2 {
	  << 
	    {
        d,8 ( \mf ^"(Play on 1st x)" -. r f8 -- f16 g -> ~ g4 bf4 )
        g16 ( ef8. bf'8 c16 g -> ) ~ g8 d f16 ( bf g ef
        \tag #'sheet \break
        d8 -. ) bf'16 ( c d,8 c'16 bf -. ~ bf g8. -> ) ~ g4 
        r8. bf16 c g8. r8 bf16 ( d ) g,8 bf16 ( g )
        
        \tag #'sheet \break
        d16 d ( f bf f8 ) f16 bf -> ~ bf8. c16 -> ~ c4
        d,4 -- r8. g16 bf d,8 -- -> d16 -. ~ d g -. r8
      }
      \new Staff {
	      \key g \minor
        \once \omit Staff.TimeSignature
        a16 \mp ^"(Play on 2nd x)" g d8 -. r4 r4 f8. ef16 -> ~
        ef4 g8. bf16 ~ bf4 r16 bf ( g f )
        d8 -. r r4 r2
        r2 r8 f16 ( g ) f8 d16 ( f
        
        d4 ) -- -. r r2
        r2 r16 d8 -> d16 -> ~ d ( f ) g8 -- ->
      }
    >>
	}
	
	\alternative {
	  { \tag #'sheet \break
      bf8. c16 -> ~ c4 r16 g8. -> ~ g8. c16 -> ~
      c4 r8. f16 -> ~ f8 g,16 ( bf bf c d c )
      \bar ":|]" }
	  { R1*2 }
	}
	
	\key a \minor
	\tag #'sheet \pageBreak
	a1 \p
	g1
	c,1
	\time 2/4
	r4 r8. a'16 -> ~
	
	\time 4/4
	\tag #'sheet \break
  a1
  g1
  c,1
  r2 r4 r8 \p a'8 -> ~ 
  
	\tag #'sheet \break
	a1
	g1
	c,1
	\time 5/4
	r4 r4 r4 r2
	
	\time 4/4
	\tag #'sheet \break
  a'1
  g1
	c,1
  \time 7/4
  R1*7/4*2
  \time 4/4
  R1

  \bar "[|:"
	\tag #'sheet \break
  \repeat volta 2 {
    << 
	    {
        c1 ^"(Play on 1st x)" 
        g'8. -- -^ a16 -- -^ ~ a8 c -> ~ c4 r
        f,4 -> ~ f8. e16 -> ~ e2
      }
      \new Staff {
	      \key a \minor
        \once \omit Staff.TimeSignature
        c1 ^"(Play on 2nd x)" 
        a'8. -- -^ d16 -- -^ ~ d8 g -> ~ g4 r
        f,4 -> ~ f8. e16 -> ~ e2
      }
    >>
  }
  \alternative {
    { \tag #'sheet \break
      c2 r4 e16 g a c
      \bar ":|]" }
    { c,2 r4 g'16 \> c b e, }
  }
  \bar "||"
  
	\tag #'sheet \break
	c4 \! r4 r2
	R1*31
	
	\tag #'sheet \break
	R1*32
	
	R1*24
	
	\bar "||"
	\tag #'sheet \break
  R1 \moltorall 
  R1 \! \fermata
  
  \bar "|."
}

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

%{
\book {
	\paper { 
		% Descomentar para score
		#(set-paper-size "a4" 'landscape)
		% indent = 20
		% systems-per-page = 2
	}
	
	\score {
		% Descomentar para midi
		% \midi { \tempo 4 = 110 }
		
		% ATENCIÓN
		% NO USAR layout-set-staff-size O SE ROMPE lilyjazz!!!
		\layout { }
		
		\removeWithTag #'sheet <<
			\scoreMarkup
			\scoreChords
			\scoreBreaks
			\new StaffGroup <<
 				\new Staff \with { instrumentName = "" midiInstrument = #"" } { \altoSaxOne }
				\new Staff \with { instrumentName = "" midiInstrument = #"" } { \altoSaxTwo }
				\new Staff \with { instrumentName = "" midiInstrument = #"" } { \tenorSaxOne }
				\new Staff \with { instrumentName = "" midiInstrument = #"" } { \tenorSaxTwo }
				\new Staff = "baritoneSax" \with { instrumentName = "" midiInstrument = #"" } { \baritoneSax }
			>>
			\new StaffGroup <<
 				\new Staff \with { instrumentName = "" midiInstrument = #"" } { \trumpetOne }
				\new Staff \with { instrumentName = "" midiInstrument = #"" } { \trumpetTwo }
				\new Staff \with { instrumentName = "" midiInstrument = #"" } { \trumpetThree }
				\new Staff \with { instrumentName = "" midiInstrument = #"" } { \trumpetFour }
			>>
			\new StaffGroup <<
			  <<
			    \tromboneOneChords 
 				  \new Staff \with { instrumentName = "" midiInstrument = #"" } { \tromboneOne }
			  >>
				\new Staff \with { instrumentName = "" midiInstrument = #"" } { \tromboneTwo }
				\new Staff \with { instrumentName = "" midiInstrument = #"" } { \tromboneThree }
				\new Staff \with { instrumentName = "" midiInstrument = #"" } { \tromboneFour }
			>>
		>>
	}
}
%}

%{
\book {
	\header { instrument = "Alto Sax I" }
  \bookOutputName "Don Fernando - Alto Sax I"
	\score {
		<<
			\scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\new Staff { \altoSaxOne }
			>>
		>>
	}
}

\book {
	\header { instrument = "Alto Sax II" }
  \bookOutputName "Don Fernando - Alto Sax II"
	\score {
		<<
			\scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\new Staff { \altoSaxTwo }
			>>
		>>
	}
}

\book {
	\header { instrument = "Tenor Sax I" }
  \bookOutputName "Don Fernando - Tenor Sax I"
	\score {
		<<
			\scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\new Staff { \tenorSaxOne }
			>>
		>>
	}
}

\book {
	\header { instrument = "Tenor Sax II" }
  \bookOutputName "Don Fernando - Tenor Sax II"
	\score {
		<<
			\scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\new Staff { \tenorSaxTwo }
			>>
		>>
	}
}

\book {
	\header { instrument = "Baritone Sax" }
  \bookOutputName "Don Fernando - Baritone Sax"
	\score {
		<<
			\scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\new Staff { \baritoneSax }
			>>
		>>
	}
}

\book {
	\header { instrument = "Trumpet I" }
  \bookOutputName "Don Fernando - Trumpet I"
	\score {
		<<
			\scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\new Staff { \trumpetOne }
			>>
		>>
	}
}

\book {
	\header { instrument = "Trumpet II" }
  \bookOutputName "Don Fernando - Trumpet II"
	\score {
		<<
			\scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\new Staff { \trumpetTwo }
			>>
		>>
	}
}

\book {
	\header { instrument = "Trumpet III (Eb)" }
  \bookOutputName "Don Fernando - Trumpet III"
	\score {
		<<
			\scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\new Staff { \trumpetThree }
			>>
		>>
	}
}

\book {
	\header { instrument = "Trumpet IV (Eb)" }
  \bookOutputName "Don Fernando - Trumpet IV"
	\score {
		<<
			\scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\new Staff { \trumpetFour }
			>>
		>>
	}
}

\book {
	\header { instrument = "Trombone I (Bb)" }
  \bookOutputName "Don Fernando - Trombone I (Bb)"
	\score {
		<<
			\scoreMarkup
			\scoreChords
			\new StaffGroup <<
			  \tromboneOneChords 
				\new Staff { \tromboneOne }
			>>
		>>
	}
}

\book {
	\header { instrument = "Trombone II (Bb)" }
  \bookOutputName "Don Fernando - Trombone II (Bb)"
	\score {
		<<
			\scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\new Staff { \tromboneTwo }
			>>
		>>
	}
}

\book {
	\header { instrument = "Trombone III (Eb)" }
  \bookOutputName "Don Fernando - Trombone III (Eb)"
	\score {
		<<
			\scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\new Staff { \tromboneThree }
			>>
		>>
	}
}

\book {
	\header { instrument = "Trombone IV (Eb)" }
  \bookOutputName "Don Fernando - Trombone IV (Eb)"
	\score {
		<<
			\scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\new Staff { \tromboneFour }
			>>
		>>
	}
}