\version "2.12.3"

\header {
	title = "Hipnosis"
}

pnA = \relative c''
{
   \set Staff.instrumentName = "Piano"
   
   r1 
   r2 fis8 a b4 
   a1 ~ a1 ^\markup {x3}
   \bar ":|"
}

pnAE = \relative c''
{
   \set Staff.instrumentName = "Piano"
   
   r1 
   r2 fis8 a b4 
   a1 ~ a1
   \bar ":|"
}

sxA = \relative c''
{
   \set Staff.instrumentName = "Saxo"
  
   \partial 2
   \times 2/3 { r4 e fis }
   \bar "|:"
   d'1 ~ 
   d2 ~ \times 2/3 { d4 gis, fis }
   \times 2/3 { cis fis2 ~ } fis2 ~
   fis ~ \times 2/3 { fis4 e fis }
   \bar ":|"
}

sxBA = \relative c''
{
   \set Staff.instrumentName = "Saxo"
  
   cis4 a8 b ~ b2
   cis4 a8 b ~ b2
   \times 2/3 { a4 b cis ~ } cis2 ~
   cis1
   \bar ":|"
}

sxBB = \relative c''
{
   \set Staff.instrumentName = "Saxo"
  
   e4 cis8 d ~ d2
   e4 cis8 d ~ d2
   \times 2/3 { b4 a cis ~ } cis2 ~
   cis1
   \bar ":|"
}

sxC = \relative c'''
{
   \set Staff.instrumentName = "Saxo"
   
   d4 r d r
   d r d r
   cis1 ~ cis 
   \bar ":|"
}

\score { \pnA }
\score { \sxA }
\score { \sxBA }
\score { \sxA }
\score { \pnAE }

\markup { \hspace #8 "Violin + Contrabajo, crescendo hasta Fa sostenido agudo" }


\score { \sxC }
\score { \sxBA }
\score { \sxBB }
\score { \sxBA }
\score { \sxC }
\score { \pnAE }

