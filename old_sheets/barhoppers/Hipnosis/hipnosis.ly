\version "2.12.3"

\header {
	title = "Hipnosis"
}

sxA = \relative c''
{
   \set Staff.instrumentName = "Saxo"
   \times 2/3 { c4 e f } g2
   \times 2/3 { c4 bes g } g2
   \times 2/3 { c4 bes g } \times 2/3 { aes g f }
   \times 2/3 { g4 f des } c2
   \bar ":|"
}

cbA = \relative c
{
   \set Staff.instrumentName = "Contrabajo"
   
   \clef bass 
   \times 2/3 { c4 e f } g2
   \times 2/3 { c4 bes g } g2
   \times 2/3 { c4 bes g } \times 2/3 { aes g f }
   \times 2/3 { g4 f des } c2
   \bar ":|"
}

\score { \sxA }
\score { \cbA }
\score { \sxA }
\score { \cbA }

\markup { \hspace #8 Break }
\markup { \hspace #8 Impro }
\markup { \hspace #8 Break }