\version "2.12.3"

\header {
	title = "Last Tango in Paris"
}

pnA = \relative c''
{
   \set Staff.instrumentName = "Piano A"
   
   a4. bes16 c a2 ~
   a4. bes16 c \times 2/3 { a4 bes c }
   d4. ees16 f d2 ~
   d4. ees16 f \times 2/3 { d4 c bes }
   a4. bes16 c a2 ~
   a4. bes16 c \times 2/3 { a4 bes c }
   d4. ees16 f d2 ~
   d4. ees16 f \times 2/3 { d4 ees f }
   g4. aes16 bes g2 ~
   g4. aes16 bes \times 2/3 { g4 f ees }
   c4. des16 ees c2 ~
   c4. des16 ees \times 2/3 { c4 des ees }
   g4. aes16 bes g2 ~
   g4. aes16 bes \times 2/3 { g4 f ees }
   c4. des16 ees d4. e16 fis
   e4. fis16 g \times 2/3 { fis4 g a }
}

cdA = \relative c''
{
   \set Staff.instrumentName = "Cuerdas"
   
   a4 g d e a1
   d4 c g a d1
   a4 g d e a1
   d4 c g a d1
   g4 f c d g1
   c,4 bes f g c1
   g'4 f c d g1
   c,2 d2 a2 d2
}

cdBA = \relative c''
{
   \set Staff.instrumentName = "Violin (A)"
   
   r8 c bes f' c bes g f 
   r8 c' bes f' ~ f2
   r8 c bes ees d c g f 
   r8 g ees g ~ g
   \bar ":|:"
}

cdBB = \relative c''
{
   \set Staff.instrumentName = "Violin (B)"
   
   r8 c bes f' c bes g f 
   r8 c' bes f' ~ f2
   r8 c bes ees des c g f 
   r8 g ees g ~ g
   \bar ":|"  
}

pnAE = \relative c''
{
   \set Staff.instrumentName = "Piano"
   
   r1 
   r2 fis8 a b4 
   a1 ~ a1
   \bar ":|"
}

sxA = \relative c''
{
   \set Staff.instrumentName = "Saxo"
  
   \partial 2
   \times 2/3 { r4 e fis }
   \bar "|:"
   d'1 ~ 
   d2 ~ \times 2/3 { d4 gis, fis }
   \times 2/3 { cis fis2 ~ } fis2 ~
   fis ~ \times 2/3 { fis4 e fis }
   \bar ":|"
}

sxBA = \relative c''
{
   \set Staff.instrumentName = "Saxo"
  
   cis4 a8 b ~ b2
   cis4 a8 b ~ b2
   \times 2/3 { a4 b cis ~ } cis2 ~
   cis1
   \bar ":|"
}

sxBB = \relative c''
{
   \set Staff.instrumentName = "Saxo"
  
   e4 cis8 d ~ d2
   e4 cis8 d ~ d2
   \times 2/3 { b4 a cis ~ } cis2 ~
   cis1
   \bar ":|"
}

sxC = \relative c'''
{
   \set Staff.instrumentName = "Saxo"
   
   d4 r d r
   d r d r
   cis1 ~ cis 
   \bar ":|"
}

\markup { \hspace #8 "Violin harmonicos"  }
\markup { \hspace #8 "Bateria y contrabajo hasta loop del contrabajo." }
\markup { \hspace #8 "Una vez que contrabajo loopeado, melodia libre del contrabajo (x8)" }

\score { \pnA }
\score { \cdA }

\markup { \hspace #8 "Piano (A)" }

\score { \cdBA }
\score { \cdBB }

\markup { \hspace #8 "Cambio de base. Impro de violin (x8) mientras se loopea contrabajo (F C Bb F)" }
\markup { \hspace #8 "Impro de contrabajo (x8)" }

\score { \cdBB }

\markup { \hspace #8 "Base inicial. Impro de piano sobre melodia inicial (x8)" }
\markup { \hspace #8 "Pregunta respuesta piano/violin, piano/contrabajo, (piano/scratch?)" }



