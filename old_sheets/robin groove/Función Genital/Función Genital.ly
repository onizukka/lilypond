\version "2.18.2"
\include "imports/main.ly"


% Instrument Music
% -------------------------------------------------
scoreChords = \chords {
  c1:11 ~ c1:11 c ~ c
  c g c g
  c g c g
  bes:7 d:m7 a:m a:m g g:9
  
  \repeat volta 2 {
    f2:/c c2 f2:/c c2 e2:dim/bes bes2 e2:dim/bes bes2
    d:m/a a:m d:m/a a:m g f4:/g g4 ~ g1
  }
  
  \repeat volta 2 {
    f2:/c c2 f2:/c c2 e2:dim/bes bes2 e2:dim/bes bes2
    d:m/a a:m d:m/a a:m g f4:/g g4 ~ g1
  }
  
  c1 c g g
  a:m g f g
  c c
  
  g1:7.13 g1:7.13 a1:m7.13 a1:m7.13
  g f g g
  
  \repeat volta 2 {
    f2:/c c2 f2:/c c2 e2:dim/bes bes2 e2:dim/bes bes2
    d:m/a a:m d:m/a a:m g f4:/g g4 ~ g1
  }
}

instrument = {

  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0

  
  s1*4
  
  \break
  \bar "||"
  s1*8
  
  \break
  R1*6
  
  \break
  R1*8
  
  \break
  \mark \markup { \musicglyph #"scripts.segno" "Rock" }
  R1*8
  
  \break
  R1*7
  \rs r4 r2
  R1*2
  
  \break
  R1*6
  r8 g4 g8 a b d e
  g4 r4 r2
  
  \break
  R1*8
  
  \eolMarkup \markup "D.S."
}


% Instrument Sheet
% -------------------------------------------------

\header {
}

\paper {  	
  % distancia entre sistemas
  system-system-spacing #'basic-distance = #18
  
  % distancia entre el título y el primer sistema
  markup-system-spacing #'basic-distance = #20
}

<<
  \scoreChords
  \new Staff << \relative c'' \instrument >>
>>
