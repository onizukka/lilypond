\version "2.18.2"
\include "imports/main.ly"


% Instrument Music
% -------------------------------------------------
scoreChords = \chords {
  \repeat volta 2 {
    bes2:maj7 ees:maj7 bes2:maj7 ees:maj7
  }
  
  \repeat volta 4 {
    bes2:maj7 ees:maj7 bes2:maj7 ees:maj7
  }
  
  bes
}

instrument = {

  \jazzOn
  \compressFullBarRests
  
  \key bes \major
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0

  
  s1*4
  
  \break
  \bar "||"
  s1*8
  
  \break
  R1*6
  
  \break
  R1*8
  
  \break
  \mark \markup "Rock"
  R1*8
  
  \break
  R1*7
  \rs r4 r2
  R1*2
  
  \break
  R1*6
  r8 g4 g8 a b d e
  g4 r4 r2
  
  \break
  R1*8
}


% Instrument Sheet
% -------------------------------------------------

\header {
}

\paper {  	
  % distancia entre sistemas
  system-system-spacing #'basic-distance = #18
  
  % distancia entre el título y el primer sistema
  markup-system-spacing #'basic-distance = #20
}

<<
  \scoreChords
  \new Staff << \relative c'' \instrument >>
>>
