scoreMarkup = \new Staff \with {
   \RemoveEmptyStaves
}
{
  % Oculta el primer pentagrama del markup
  \override Staff.VerticalAxisGroup #'remove-first = ##t
  
  \mark \markup \boxed "intro ad. libitum"
  s1
  
  \mark \markup \boxed "\"1.. 2..\""
  s1
  
  \mark \markup \boxed "\"1, 2, 3, 4\""
  s1*2
  
  \break
  \mark \markup { \boxed \musicglyph #"scripts.segno" "as 2nd time" }
  s1*4
  
  \break
  s1*8
  
  \break
  \bar "||"
  s1*7
  
  \mark \markup \musicglyph #"scripts.coda"
  s1
  \eolMarkup \markup \musicglyph #"scripts.segno"
  
  \break
  \mark \markup \boxed \musicglyph #"scripts.coda"
  s1*3
  \bar "||"
  \mark \markup \boxed "\"A los hijos..\""
  s1*10
  
  \break
  \mark \markup \boxed "bridge"
  s1*12
  
  \break
  s1*8
  
  \mark \markup \boxed "Guitar solo"
  \break
  s1*6
  
  \break
  s1*6
  
  \break
  s1*8
  
  \break
  s1*7
  
  \break
  s1*3
  \bar "||"
  \mark \markup \boxed "\"A los hijos..\""
  s1*2
  s1*4
  \once \override Score.RehearsalMark #'X-offset = #-5
  \eolMarkup \markup \boxed "3 x's"
  
  \break
  s1*6
  
  \bar "|."
}