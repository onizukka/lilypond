\version "2.16.1"
%\include "imports/main.ly"


% Instrument Music
% -------------------------------------------------
tenor = {
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0

  \jazzOn
  \compressFullBarRests
  
  \key b \major
  
  R1
  
  R1*2
  r8 gis'-^ r8 gis-^ r8 gis-^ r8 ais->\sfp ~ 
  
  \repeat volta 2 {
    ais1 ~ 
    ais\< ~ 
    ais
    r8\! gis-^ r8 gis-^ r8 gis-^ r8 ais-> ~ 
  }
  
  \repeat volta 2 {
    ais8^\markup "tacet 2nd time" \bendAfter #-3 r r4 r2
    R1*3
    
    b1\sfp\< ~
    b
    r8\! gis-^ r8 gis-^ r8 gis-^ r8 ais->\> ~
    ais2. r4\!
  }
  
  R1*3
  r2 r4 r8 bis,8-^
  
  r2 r4 ais8( bis)-^
  r2 r4 bis8( cis)-^
  r2 r4 ais8( b?)-^
  r2 r4 r8 ais'->\sfp \laissezVibrer
  
  R1*3
  R1*2
  
  gis2 fis
  b4. ais8 ~ ais2 ~
  ais1\p\< ~
  ais1\mf
  
  gis2 fis
  b4. ais8 ~ ais2 ~
  ais1\> ~
  ais1
  
  R1*8\!
  
  d,8-^\mf r d8-^ r d4.-> ees8-^
  r8 ees-^ r ees8-> ~ ees4 ees8-^ r
  dis8-^ r dis-^ r dis4.-> eis8-> ~
  eis eis eis cis fis-> eis-^ r4
  
  gis2 fis
  b4. ais8 ~ ais2 ~
  ais1\p\< ~
  ais1\mf
  
  gis2 fis
  b4. ais8 ~ ais2 ~
  ais1\> ~
  ais1
  
  r4\mp bis,-^ r8 bis-^ r4
  r4 bis-^ r8 bis-^ r4
  r4 bis-^ r8 bis-^ r4
  r4 bis-^ r8 bis-^ r4
  
  r4 b?-^ r8 b-^ r4
  r4 b-^ r8 b-^ r4
  r4 bis-^ r8 bis-^ r4
  r4 bis-^ r8 bis-^ r4
  
  r4 dis-^ r8 dis-^ r4
  r4 cis-^ r8 cis-^ r4
  r4 bis-^ r8 dis-^ r4
  gis2 fis\bendAfter #-3
  
  \repeat volta 2 {
    R1*4
    
    b1\sfp\< ~
    b
    r8\! gis-^ r8 gis-^ r8 gis-^ r8 ais->\> ~
    ais2. r4\!
  }
  
  R1*3
  r2 r4 r8 bis,8-^
  
  r2 r4 ais8( bis)-^
  r2 r4 bis8( cis)-^
  r2 r4 ais8( b?)-^
  
  R1*3
  R1*2
  
  \repeat volta 3 {
    gis'2 fis
    b4. ais8 ~ ais2 ~
    ais1\sfp\< ~
    ais1
  }
  
  R1*2\!
  
  e'2 dis
  dis4. fis8 ~ fis2 ~
  fis1 ~
  fis1\fermata
}


% Instrument Sheet
% -------------------------------------------------

%{
\header {
}

\paper {  	
  % distancia entre sistemas
  system-system-spacing #'basic-distance = #18
  
  % distancia entre el título y el primer sistema
  markup-system-spacing #'basic-distance = #20
}

<<
  \scoreMarkup
  \new Staff << \relative c'' \instrument >>
>>
%}
