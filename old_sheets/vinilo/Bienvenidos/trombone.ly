\version "2.16.1"
%\include "imports/main.ly"


% Instrument Music
% -------------------------------------------------
trombone = {
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0

  \jazzOn
  \compressFullBarRests
  
  \clef bass
  \key b \major
  
  R1
  
  R1*2
  r8 e'-^ r8 e-^ r8 e-^ r8 fis->\sfp ~ 
  
  \repeat volta 2 {
    fis1 ~ 
    fis\< ~ 
    fis
    r8\! e-^ r8 e-^ r8 e-^ r8 fis-> ~ 
  }
  
  \repeat volta 2 {
    fis8^\markup "tacet 2nd time" \bendAfter #-3 r r4 r2
    R1*3
    
    dis1\sfp\< ~
    dis
    r8\! e-^ r8 e-^ r8 e-^ r8 fis->\> ~
    fis2. r4\!
  }
  
  R1*3
  r2 r4 r8 gis,8-^
  
  r2 r4 fis8( gis)-^
  r2 r4 gis8( ais)-^
  r2 r4 ais8( gis)-^
  r2 r4 r8 fis'->\sfp \laissezVibrer
  
  R1*3
  R1*2
  
  e2 dis
  dis4. cis8 ~ cis2 ~
  cis1\p\< ~
  cis1\mf
  
  e2 dis
  dis4. cis8 ~ cis2 ~
  cis1\> ~
  cis1
  
  R1*8\!
  
  bes8-^\mf r bes8-^ r bes4.-> bes8-^
  r8 bes-^ r bes8-> ~ bes4 bes8-^ r
  b8-^ r b-^ r b4.-> cis8-> ~
  cis cis gis gis cis-> cis-^ r4
  
  e2 dis
  dis4. cis8 ~ cis2 ~
  cis1\p\< ~
  cis1\mf
  
  e2 dis
  dis4. cis8 ~ cis2 ~
  cis1\> ~
  cis1
  
  r4\mp gis-^ r8 gis-^ r4
  r4 gis-^ r8 gis-^ r4
  r4 gis-^ r8 gis-^ r4
  r4 gis-^ r8 gis-^ r4
  
  r4 gis-^ r8 gis-^ r4
  r4 gis-^ r8 gis-^ r4
  r4 gis-^ r8 gis-^ r4
  r4 gis-^ r8 gis-^ r4
  
  r4 ais-^ r8 ais-^ r4
  r4 gis-^ r8 gis-^ r4
  r4 gis-^ r8 gis-^ r4
  e'2 b\bendAfter #-3
  
  \repeat volta 2 {
    R1*4
    
    dis1\sfp\< ~
    dis
    r8\! e-^ r8 e-^ r8 e-^ r8 fis->\> ~
    fis2. r4\!
  }
  
  R1*3
  r2 r4 r8 gis,8-^
  
  r2 r4 fis8( gis)-^
  r2 r4 gis8( ais)-^
  r2 r4 ais8( gis)-^
  
  R1*3
  R1*2
  
  \repeat volta 3 {
    e'2 dis
    dis4. cis8 ~ cis2 ~
    cis1\sfp\< ~
    cis1
  }
  
  R1*2\!
  
  gis'2 fis
  b4. ais8 ~ ais2 ~
  ais1 ~
  ais1\fermata
}


% Instrument Sheet
% -------------------------------------------------

%{
\header {
}

\paper {  	
  % distancia entre sistemas
  system-system-spacing #'basic-distance = #18
  
  % distancia entre el título y el primer sistema
  markup-system-spacing #'basic-distance = #20
}

<<
  \scoreMarkup
  \new Staff << \relative c'' \instrument >>
>>
%}
