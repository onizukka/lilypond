\version "2.16.1"
%\include "imports/main.ly"


% Instrument Music
% -------------------------------------------------
alto = {

  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0
  
  \key a \major
  
  a1
  e
  g
  e
  
  a
  e
  g
  d ~ d2 r
  
  R1*4
  R1*4
  
  R1*8
  
  R1*3
  R1*4
  R1*2
  
  \repeat volta 2 {
    R1*3
    r4 g16( a g c ~ c8-- a-^) g16( a-^) r8
    
    R1*3
  }
  \alternative {
    { r8 e-^ g16( a8-^ c16( ~ c-- a16 c d e-^) r8 }
    { r4 e,16( g a c e8-^) a16( c d e a8-^ }
  }
  
  R1*4
  R1*4
  
  R1*8
  
  R1*3
  R1*4
  R1*2
  
  \repeat volta 2 {
    R1*3
    r4 g16( a g c ~ c8-- a-^) g16( a-^) r8
    
    R1*3
  }
  \alternative {
    { r8 e-^ g16( a8-^ c16( ~ c-- a16 c d e-^) r8 }
    { r4 e,16( g a c e8-^) a16( c d e a8-> ~ }
  }
  
  \repeat volta 2 {
    a1
    e
    g
    e
    
    a
    e
    g
    d ~ d2 r
  }
  \alternative {
    { d }
    { d ~ d2 r }
  }
  
  \repeat volta 2 {
    R1*3
    r4 g16( a g c ~ c8-- a-^) g16( a-^) r8
    
    R1*3
  }
  \alternative {
    { r8 e-^ g16( a8-^ c16( ~ c-- a16 c d e-^) r8 }
    { r4 e,16( g a c e8-^) a16( c d e a8-> ~ }
  }
  
  a1
  R1*7
  
  \repeat volta 2 {
    R1*3
    r4 g16( a g c ~ c8-- a-^) g16( a-^) r8
    
    R1*3
  }
  \alternative {
    { r8 e-^ g16( a8-^ c16( ~ c-- a16 c d e-^) r8 }
    { r4 e,16( g a c e8-^) a16( c d e a8-> ~ }
  }
  
  R1*4
  R1*4
}


% Instrument Sheet
% -------------------------------------------------

%{
\header {
}

\paper {  	
  % distancia entre sistemas
  system-system-spacing #'basic-distance = #18
  
  % distancia entre el título y el primer sistema
  markup-system-spacing #'basic-distance = #20
}

<<
  \scoreMarkup
  \new Staff << \relative c'' \instrument >>
>>
%}
