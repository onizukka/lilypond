\version "2.16.1"
%\include "imports/main.ly"


% Instrument Music
% -------------------------------------------------
tenor = {
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0

  \jazzOn
  \compressFullBarRests
  
  \key c \minor
  
  R1*4
  
  \repeat volta 2 {
    ees8( fis g a bes a g fis 
    ees fis g a bes2)
  }
  
  \repeat volta 4 {
    c,,8( d ees fis g fis ees d
    c d ees fis g-^) r r4
  }
  
  R1*4
  
  \repeat volta 2 {
    R1*2
    \repeat percent 2 {
      r8 d'-^ r d-^ cis( d-^) cis( d-^)
    }
    
    R1*2
    \repeat percent 2 {
      r8 d-^ r d-^ cis( d-^) cis( d-^)
    }
    
    c1\sfp\< ~
    c2. d4
    c8(--\f ees-^)\mf r ees-^ d( ees-^) d( ees-^)
    r ees-^ r ees-^ d( ees-^) d( ees-^)
    
    c1\sfp ~
    c1\<
    d1\f ~
    d2 r
  }
  
  \repeat volta 2 {
    \repeat percent 3 {
      aes'2.-> ~ aes8 g->\! ~
      \endSpanners g1\> 
    }
    f1\<
  }
  \alternative {
    { r8\f bes-^ r bes-^ r bes-^ r a\laissezVibrer } 
    { r8\f bes-^ r bes-^ r bes-^ r4 }
  }
  
  \repeat volta 4 {
    ees,8( fis g a bes a g fis 
    ees fis g a bes2)
  }
  
  R1*4
  
  \repeat volta 2 {
    c,1\p ~
    c
    ces1 ~ 
    ces2.\< des4
    
    c1\p ~
    c
    ces1 ~ 
    ces2.\< des4
    
    c1\p ~
    c
    c8( ees-^) r ees-^ d( ees-^) d( ees-^)
    r ees-^ r ees-^ d( ees-^) d( ees-^)
    
    c1\sfp ~
    c1\<
    d1\f ~
    d2 r
  }
  
  \repeat volta 2 {
    \repeat percent 3 {
      aes'2.-> ~ aes8 g->\! ~
      \endSpanners g1\> 
    }
    f1\<
  }
  \alternative {
    { r8\f bes-^ r bes-^ r bes-^ r a \laissezVibrer } 
    { r8\f bes-^ r bes-^ r bes-^ r a-^ }
  }
  
  \repeat volta 2 {
    \repeat percent 3 {
      r4 aes2-> ~ aes8 g->\>
      \endSpanners g1\> 
    }
    f1\<
    
  }
  \alternative {
    { r8\f bes-^ r bes-^ r bes-^ r a-^ } 
    { r8\f bes-^ r bes-^ r bes-^ r4 }
  }
  
  \repeat volta 4 {
    ees,8( fis g a bes a g fis 
  }
  \alternative {
    { ees fis g a bes2) }
    { ees,8( fis g a bes4-^) b-^ }
  }
  
  c4-^ r4 r2\fermata
}


% Instrument Sheet
% -------------------------------------------------
%{
\paper {  	
  % distancia entre sistemas
  system-system-spacing #'basic-distance = #18
  
  % distancia entre el título y el primer sistema
  markup-system-spacing #'basic-distance = #20
}

<<
  \scoreMarkup
  \new Staff << \relative c'' \instrument >>
>>
%}
