\version "2.16.1"
%\include "imports/main.ly"


% Instrument Music
% -------------------------------------------------
trombone = {
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0
  \override Glissando #'style = #'line

  \jazzOn
  \compressFullBarRests
  
  \clef bass
  \key c \minor
  
  R1*4
  
  \repeat volta 2 {
    ees8( fis g a bes a g fis 
    ees fis g a bes2)
  }
  
  \repeat volta 4 {
    c4-^ r4 r2
    r2 r4 g\glissando
  }
  
  c4-^ r4 r2
  R1*3
  
  \repeat volta 2 {
    R1*2
    \repeat percent 2 {
      r8 bes-^ r bes-^ a( bes-^) a( bes-^)
    }
    
    R1*2
    \repeat percent 2 {
      r8 bes-^ r bes-^ a( bes-^) a( bes-^)
    }
    
    aes1\sfp\< ~
    aes1
    g8(--\f c-^)\mf r c-^ b( c-^) b( c-^)
    r c-^ r c-^ b( c-^) b( c-^)
    
    aes1\sfp ~
    aes1\<
    bes1\f ~
    bes2 r
  }
  
  \repeat volta 2 {
    \repeat percent 3 {
      c2.-> ~ c8 bes->\! ~
      \endSpanners bes1\> 
    }
    a1\<
  }
  \alternative {
    { r8\f d-^ r d-^ r d-^ r des\laissezVibrer } 
    { r8\f bes-^ r bes-^ r bes-^ r b\glissando }
  }
  
  \repeat volta 4 {
    c4-^ r4 r2
    r2 r4 g\glissando
  }
  
  c4-^ r4 r2
  R1*3
  
  \repeat volta 2 {
    g1\p ~
    g
    aes1 ~ 
    aes2\< ~ aes4\glissando bes4
    
    g1\p ~
    g
    aes1 ~ 
    aes2\< ~ aes4\glissando bes4
    
    aes1\p ~
    aes
    g8( c-^) r c-^ b( c-^) b( c-^)
    r c-^ r c-^ b( c-^) b( c-^)
    
    aes1\sfp ~
    aes1\<
    bes1\f ~
    bes2 r
  }
  
  \repeat volta 2 {
    \repeat percent 3 {
      c2.-> ~ c8 bes->\! ~
      \endSpanners bes1\> 
    }
    a1\<
  }
  \alternative {
    { r8\f d-^ r d-^ r d-^ r des \laissezVibrer } 
    { r8\f d-^ r d-^ r d-^ r des-^ }
  }
  
  \repeat volta 2 {
    \repeat percent 3 {
      r4 c2-> ~ c8 bes->\>
      \endSpanners bes1\> 
    }
    a1\<
    
  }
  \alternative {
    { r8\f d-^ r d-^ r d-^ r des-^ } 
    { r8\f bes-^ r bes-^ r bes-^ r b\glissando }
  }
  
  \repeat volta 4 {
    c4-^ r4 r2
  }
  \alternative {
    { r2 r4 g\glissando }
    { r2 bes4-^ g-^ }
  }
  
  c,4-^ r4 r2\fermata
}


% Instrument Sheet
% -------------------------------------------------

%{
\header {
}

\paper {  	
  % distancia entre sistemas
  system-system-spacing #'basic-distance = #18
  
  % distancia entre el título y el primer sistema
  markup-system-spacing #'basic-distance = #20
}

<<
  \scoreMarkup
  \new Staff << \relative c'' \instrument >>
>>
%}
