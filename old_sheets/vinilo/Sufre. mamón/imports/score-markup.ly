scoreMarkup = \new Staff \with {
   \RemoveEmptyStaves
}
{
  % Oculta el primer pentagrama del markup
  \override Staff.VerticalAxisGroup #'remove-first = ##t
  
  \mark \markup \boxed "intro guitar"
  s1*6
  
  \break
  s1*6
  
  \break
  \mark \markup \boxed "verse"
  s1*2
  \mark \markup "tacet 1st x"
  s1*6
  
  \break
  s1*8
  
  \break
  \mark \markup \boxed "chorus"
  s1*8
  
  \break
  s1*7
  
  \break
  \mark \markup \boxed "verse"
  s1*8
  
  \break
  s1*8
  
  \break
  \mark \markup \boxed "chorus"
  s1*9
  
  \break
  s1*8
  
  \break
  s1*5
  
  \bar "|."
}