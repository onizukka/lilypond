\version "2.16.1"

\include "imports/main.ly"

\include "tenor.ly"
\include "trombone.ly"


<<
  \scoreMarkup
  \new StaffGroup <<
    \new Staff  \with { instrumentName = #"Tenor Sax" } 
    { \transpose bes c' \relative c'' \tenor }
    
    \new Staff  \with { instrumentName = #"Trombone" }
    { \relative c \trombone }
  >>
>>

\layout {  
  indent = 1.5\cm
}