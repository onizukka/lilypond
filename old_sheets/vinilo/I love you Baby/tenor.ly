\version "2.16.1"
%\include "imports/main.ly"


% Instrument Music
% -------------------------------------------------
tenor = {

  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0
  
  \key c \major
  
  R1*4
  
  \repeat volta 2 {
    c1( ~ c
    b1 ~ b
    bes ~ bes
    a ~ a2.) r4
    
    aes1( ~ 
    aes4 c bes \grace { c32[ bes] } aes4 
    d2 g, ~ 
    g4) r g( gis
    
    a1\<
    d2 f
    e1\> ~
    e2) r\!
  }
  
  \repeat volta 2 {
    f4(-> f8)-^ r f4(-> f8)-^ r 
    f4(-> f8-- aes)-^ r g-^ r dis-^
    e4(-> e8)-^ r e4(-> e8)-^ r 
  }
  \alternative {
    { dis4(-> e8-- a)-^ r g-^ r e-^ }
    { \break dis4(-> e8-- a)\sfp\< ~ a2 ~ a1 }
  }
  R1*8\!
  R1*7
  r1\fermata
  
  c,1( ~ c
  b1 ~ b
  bes ~ bes
  a ~ a2.) r4
  
  aes1( ~ 
  aes4 c bes \grace { c32[ bes] } aes4 
  d2 g, ~ 
  g4) r g( gis
  
  a1\<
  d2 f
  e1\> ~
  e2) r\!
  
  \repeat volta 2 {
    f4(-> f8)-^ r f4(-> f8)-^ r 
    f4(-> f8-- aes)-^ r g-^ r dis-^
    e4(-> e8)-^ r e4(-> e8)-^ r 
  }
  \alternative {
    { dis4(-> e8-- a)-^ r g-^ r e-^ }
    { dis4(-> e8-- a)\sfp\< ~ a2 ~ a1 }
  }
  
  R1*8\!
  R1*7
  r1\fermata
  
  c,1( ~ c
  b1 ~ b
  bes ~ bes
  a\rit aes) \breathe
  
  e2\!\glissando b'\fermata
}


% Instrument Sheet
% -------------------------------------------------

%{
\header {
}

\paper {  	
  % distancia entre sistemas
  system-system-spacing #'basic-distance = #18
  
  % distancia entre el título y el primer sistema
  markup-system-spacing #'basic-distance = #20
}

<<
  \scoreMarkup
  \new Staff << \relative c'' \instrument >>
>>
%}
