scoreMarkup = \new Staff \with {
   \RemoveEmptyStaves
}
{
  % Oculta el primer pentagrama del markup
  \override Staff.VerticalAxisGroup #'remove-first = ##t
  
  s1*4
  
  s1*8
  
  \break
  s1*8
  
  \break
  s1*4
  
  \break
  s1*2
  \mark \markup \boxed "chorus"
  s1*16
  
  \bar "||"
  \break
  s1*8
  
  \break
  s1*8
  
  \break
  s1*4
  
  \break
  s1*2
  \mark \markup \boxed "chorus"
  s1*16
  
  \bar "||"
  \break
  s1*9
  
  \bar "|."
}