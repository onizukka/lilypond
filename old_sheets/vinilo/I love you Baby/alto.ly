\version "2.16.1"
%\include "imports/main.ly"


% Instrument Music
% -------------------------------------------------
alto = {

  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0
  
  \key c \major
  
  R1*4
  
  \repeat volta 2 {
    e1( ~ e2. ~ e8 d
    e1 ~ e2. ~ e8 d
    e1 ~ e2. ~ e8 c
    d1 ~ d2.) r8 c(
    
    d1 ~ 
    d2. c4
    e2 c ~
    c4) r c( cis
    
    d1\<
    f2 aes
    g1\> ~
    g2) r\!
  }
  
  \repeat volta 2 {
    gis4(-> a8)-^ r gis4(-> a8)-^ r 
    gis4(-> a8-- c)-^ r b-^ r a-^
    gis4(-> a8)-^ r gis4(-> a8)-^ r 
  }
  \alternative {
    { gis4(-> a8-- c)-^ r b-^ r a-^ }
    { gis4(-> a8-- e')\sfp\< ~ e2 ~ e1 }
  }
  
  R1*8\!
  R1*7
  r1\fermata
  
  e,1( ~ e2. ~ e8 d
  e1 ~ e2. ~ e8 d
  e1 ~ e2. ~ e8 c
  d1 ~ d2.) r8 c(
  
  d1 ~ 
  d2. c4
  e2 c ~
  c4) r c( cis
  
  d1\<
  f2 aes
  g1\> ~
  g2) r\!
  
  \repeat volta 2 {
    gis4(-> a8)-^ r gis4(-> a8)-^ r 
    gis4(-> a8-- c)-^ r b-^ r a-^
    gis4(-> a8)-^ r gis4(-> a8)-^ r 
  }
  \alternative {
    { gis4(-> a8-- c)-^ r b-^ r a-^ }
    { gis4(-> a8-- e')\sfp\< ~ e2 ~ e1 }
  }
  
  R1*8\!
  R1*7
  r1\fermata
  
  e,1( ~ e2. ~ e8 d
  e1 ~ e2. ~ e8 d
  e1 ~ e2. ~ e8 d
  e1 c) \breathe
  
  g2^\markup "lento & express." \!\glissando d'\fermata
}


% Instrument Sheet
% -------------------------------------------------

%{
\header {
}

\paper {  	
  % distancia entre sistemas
  system-system-spacing #'basic-distance = #18
  
  % distancia entre el título y el primer sistema
  markup-system-spacing #'basic-distance = #20
}

<<
  \scoreMarkup
  \new Staff << \relative c'' \instrument >>
>>
%}
