\version "2.16.1"

\include "imports/main.ly"

\include "alto.ly"
\include "tenor.ly"


<<
  \scoreMarkup
  \new StaffGroup <<
    \new Staff  \with { instrumentName = #"Alto Sax" }
    { \transpose ees c \relative c'' \alto }
    
    \new Staff \with { instrumentName = #"Tenor Sax" }
    { \transpose bes c' \relative c'' \tenor }
  >>
>>

\layout {  
  indent = 1.5\cm
}