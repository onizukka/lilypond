\version "2.16.1"
%\include "imports/main.ly"


% Instrument Music
% -------------------------------------------------
tenor = {
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0

  \jazzOn
  \compressFullBarRests
  
  \key d \minor
  
  R1*8
  
  \repeat volta 2 {
    d'4.\f-> c4.-> d4-^
    r1
    g,4.-> g4.-> g4-^
    a4.-> a4.-> a4-^	
  }
  
  \break
  \repeat volta 2 {
    d4.-> c4.-> d4-^
    r1
    g,4.-> g4.-> g4-^
  }
  \alternative {
    {a4.-> a4.-> a4-^}
    {a1}
  }
  
  \break
  e1
  R1*7
  R1*7
  r2 r8 a8-> ~ a16 aes8.->
  
  \break
  \repeat volta 4 {
    g1->\sfp(
  }
  \alternative {
    { a2\< ~ a8) a8->\f ~ a16 aes8.-> }
    { a1\< }
  }
  
  \break
   \repeat volta 2 {
    d4.\f-> c4.-> d4-^
    r1
    g,4.-> g4.-> g4-^
    a4.-> a4.-> a4-^	
  }
  
  \break
  \repeat volta 2 {
    d4.-> c4.-> d4-^
    r1
    g,4.-> g4.-> g4-^
  }
  \alternative {
    {a4.-> a4.-> a4-^}
    {a1}
  }
  
  \break
  R1*4
  R1*4
  \repeat volta 4 {
    d8.\ff c16 ~ c8 a ~ a16 g8. f4 ~
    f2 g
  }
  
  \break
  R1*8
  
  R1*8
  
  R1*7
  r2 r8 a8-> ~ a16 aes8.->
  
  \break
  \repeat volta 4 {
    g1->\sfp(
  }
  \alternative {
    { a2\< ~ a8) a8->\f ~ a16 aes8.-> }
    { a1\< }
  }
  
  \break
  R1*8\!
  
  R1*8
  
  R1*4
  
  R1*4
  
  \break
  \repeat volta 4 {
    d8.\ff c16 ~ c8 a ~ a16 g8. f4 ~
    f2 g
  }
  
  \repeat volta 4 {
    c1\sfp->\< ~ c\!
  }
  
  \break
  \repeat volta 2 {
    d,16-> c a f'-> ~ f8 d-^ r8 c ~ c16 d8.-^
    f,8.( fis16 ~ fis8 g gis4 a-^)
    \break
    d16-> c a c-> ~ c8 d-^ r8 f ~ f16 d8.-^
    aes'4-^ g-^ f--( d16 c a8-^) 
  }
  
  \break
  d16-> c a f'-> ~ f8 d-^ r2\fermata
  \override NoteHead #'style = #'slash
  e1\fermata
}


% Instrument Sheet
% -------------------------------------------------

%{
\header {
}

\paper {  	
  % distancia entre sistemas
  system-system-spacing #'basic-distance = #18
  
  % distancia entre el título y el primer sistema
  markup-system-spacing #'basic-distance = #20
}

<<
  \scoreMarkup
  \new Staff << \relative c'' \instrument >>
>>
%}
