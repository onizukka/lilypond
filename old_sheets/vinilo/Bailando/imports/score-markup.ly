scoreMarkup = \new Staff \with {
   \RemoveEmptyStaves
}
{
  % Oculta el primer pentagrama del markup
  \override Staff.VerticalAxisGroup #'remove-first = ##t
  
  s1*9
  
  \bar "||"
  \mark \markup \boxed "Verse"
  s1*8
  
  \bar "||"
  \mark \markup \boxed "Verse"
  s1*8
  
  s1*3
  s1*9
  
  \bar "||"
  \mark \markup \boxed "Bridge"
  s1*8
  
  s1 s1\eolMarkup "x4"
  
  \mark \markup \boxed "Tenor Sax solo"
  s1*8
  
  \bar "||"
  \mark \markup \boxed "Verse"
  s1*8
  
  \bar "||"
  \mark \markup \boxed "Verse"
  s1*11
  
  \bar "||"
  \mark \markup \boxed "Trombone solo"
  s1*8
  
  \bar "||"
  \mark \markup \boxed "Tenor Sax solo"
  s1*8
  
  \bar "||"
  \mark \markup \boxed "Bridge"
  s1*8
  
  s1 s1\eolMarkup "x4"
  
  s1 s1\eolMarkup "x4"
  
  s1*6
  \bar "|."
}