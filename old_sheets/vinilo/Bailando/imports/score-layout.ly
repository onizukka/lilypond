\header {
  title = "Bailando"
  instrument = "Tenor Sax & Trombone"
  composer = ""
  arranger = "Arr. By Vinilo"
  lyrics = ""
}

\paper {  
  % Definición de fuente. No sé para qué sirve "myStaffSize".
  #(define fonts (make-pango-font-tree "LilyJAZZText" "Nimbus Sans" "Luxi Mono" (/ 20 20)))
  
  % distancia entre sistemas
  system-system-spacing #'basic-distance = #20
  
  % distancia entre el título y el primer sistema
  markup-system-spacing #'basic-distance = #35
  
  top-markup-spacing #'basic-distance = #5
  
  top-system-spacing #'basic-distance = #15
  
  bottom-margin = 20
  
  oddHeaderMarkup = \markup {
    \on-the-fly #not-first-page \fill-line {
      \fromproperty #'page:page-number-string
      \fromproperty #'header:title
      \fromproperty #'header:instrument
    }
  }
  
  evenHeaderMarkup = \markup {
    \on-the-fly #not-first-page \fill-line {
      \fromproperty #'header:instrument
      \fromproperty #'header:title
      \fromproperty #'page:page-number-string
    }
  }
}