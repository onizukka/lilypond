scoreMarkup = \new Staff \with {
   \RemoveEmptyStaves
}
{
  % Oculta el primer pentagrama del markup
  \override Staff.VerticalAxisGroup #'remove-first = ##t
  
  \mark \markup \boxed "intro drums"
  s1*2
  
  \mark \markup \boxed "intro guitar"
  \bar "||"
  s1*4
  
  \break
  s1*3
  
  \break
  \mark \markup \boxed "verse"
  \bar "||"
  s1*10
  
  \break
  s1*3
  
  \break
  \mark \markup \boxed "verse"
  \bar "||"
  s1*8
  \mark \markup \boxed "bridge"
  \bar "||"
  s1*4
  
  \break
  s1*3
  
  \break
  \mark \markup \boxed "verse"
  \bar "||"
  s1*8
  \mark \markup \boxed "bridge"
  \bar "||"
  s1*4
  
  \break
  s1*3
  
  \break
  s1*4
  
  \break
  \mark \markup \boxed "verse"
  \bar "||"
  s1*8
  \mark \markup \boxed "bridge"
  \bar "||"
  s1*4
  \mark \markup \boxed "bridge"
  \bar "||"
  s1*6
  
  \break
  \mark \markup \boxed "Q&A solos 2x each"
  \bar "||"
  s1*16
  \mark \markup \boxed "All at the same time"
  \bar "||"
  s1*8
  
  \break
  s1*3
  
  \break
  s1*4
  
  \bar "|."
  
  
  
  
}