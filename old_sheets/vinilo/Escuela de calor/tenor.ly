\version "2.16.1"
%\include "imports/main.ly"


% Instrument Music
% -------------------------------------------------
tenor = {

  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0
  
  \key d \major
  
  R1*2
  
  R1*3
  r2 r4 r8 \grace fis g-> ~
  
  \repeat volta 2 {
    g8 fis-> e->[ d->] \grace c cis8.---> a16-^ r8 d,-> ~
  }
  \alternative {
    { d8 e-> g->[ a->] cis16-> d8-^ cis16-^ r8 \grace fis g-> \laissezVibrer }
    { \scoop d, d8 e8-> g->[ a->] e4 cis }
  }
  
  R1*8
  
  R1*1
  r2 r4 r8 \grace fis' g-> ~
  
  \repeat volta 2 {
    g8 fis-> e->[ d->] \grace c cis8.---> a16-^ r8 d,-> ~
  }
  \alternative {
    { d8 e-> g->[ a->] cis16-> d8-^ cis16-^ r8 \grace fis g-> \laissezVibrer }
    { \scoop d, d8 e8-> g->[ a->] e4 cis }
  }
  
  R1*8
  
  R1*3
  r2 r4 r8 \grace fis' g-> ~
  
  \repeat volta 2 {
    g8 fis-> e->[ d->] \grace c cis8.---> a16-^ r8 d,-> ~
  }
  \alternative {
    { d8 e-> g->[ a->] cis16-> d8-^ cis16-^ r8 \grace fis g-> \laissezVibrer }
    { \scoop d, d8 e8-> g->[ a->] e4 cis }
  }
  
  R1*8
  
  R1*3
  r2 r4 r8 \grace fis' g-> ~
  
  \repeat volta 2 {
    g8 fis-> e->[ d->] \grace c cis8.---> a16-^ r8 d,-> ~
  }
  \alternative {
    { d8 e-> g->[ a->] cis16-> d8-^ cis16-^ r8 \grace fis g-> \laissezVibrer }
    { \scoop d, d8 e8-> g->[ a->] cis16-> d8-^ cis16-^ r8 \grace a' b-> \laissezVibrer }
  }
  b8 a-> g->[ fis->] \grace dis e8.---> d16-^ r8 e,-> ~
  e8 g-> a->[ d->] g16-> a8-^ e16-^ r8 \grace fis g-> ~
  g8 fis-> e->[ d->] \grace c cis8.---> a16-^ r8 d,-> ~
  d8 e-^  g->[ a->] e4 cis
  
  R1*8
  
  R1*4
  
  R1*6
  
  R1*16
  
  R1*7
  r2 r4 r8 \grace fis' g-> ~
  
  \repeat volta 2 {
    g8 fis-> e->[ d->] \grace c cis8.---> a16-^ r8 d,-> ~
  }
  \alternative {
    { d8 e-> g->[ a->] cis16-> d8-^ cis16-^ r8 \grace fis g-> \laissezVibrer }
    { \scoop d, d8 e8-> g->[ a->] cis16-> d8-^ cis16-^ r8 \grace a' b-> \laissezVibrer }
  }
  b8 a-> g->[ fis->] \grace dis e8.---> d16-^ r8 e,-> ~
  e8 g-> a->[ d->] g16-> a8-^ e16-^ r8 \grace fis g-> ~
  g8 fis-> e->[ d->] \grace c cis8.---> a16-^ r4
  
  a16->( a a a-^) r4 a16->( a a a-^) r4
}


% Instrument Sheet
% -------------------------------------------------

%{
\header {
}

\paper {  	
  % distancia entre sistemas
  system-system-spacing #'basic-distance = #18
  
  % distancia entre el título y el primer sistema
  markup-system-spacing #'basic-distance = #20
}

<<
  \scoreMarkup
  \new Staff << \relative c'' \instrument >>
>>
%}
