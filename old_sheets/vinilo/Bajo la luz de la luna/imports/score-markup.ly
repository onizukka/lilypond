scoreMarkup = \new Staff \with {
   \RemoveEmptyStaves
}
{
  % Oculta el primer pentagrama del markup
  \override Staff.VerticalAxisGroup #'remove-first = ##t
  
  \mark \markup \boxed "intro"
  s1*10*12/8
  
  \break
  s1*3*12/8
  s1*6/8
  s1*1*12/8
  
  \break
  \mark \markup \boxed "intro"
  s1*6*12/8
  
  \break
  s1*3*12/8
  s1*6/8
  s1*3*12/8
  
  \break
  \mark \markup \boxed "bridge"
  s1*6*12/8
  
  \break
  s1*3*12/8
  s1*6/8
  s1*3*12/8
  
  \break
  \mark \markup \boxed "bridge"
  s1*4*12/8
  \bar "||"
  \mark \markup \boxed "alto sax solo"
  s1*8*12/8
  
  \break
  \bar "||"
  \mark \markup \boxed "bridge"
  s1*6*12/8
  
  \break
  s1*3*12/8
  s1*6/8
  s1*1*12/8
  
  \break
  s1*4*12/8
  
  s1*12/8
  \bar "|."
}