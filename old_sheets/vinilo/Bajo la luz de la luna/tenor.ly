\version "2.16.1"
%\include "imports/main.ly"


% Instrument Music
% -------------------------------------------------
tenor = {

  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0
  
  \time 12/8
  \key a \major
  
  R1*7*12/8
  r2. r4. r4 dis8(--
  
  \repeat volta 2 {
    e-^) r4 r g8(-- gis-^) r4 r g8(--
  }
  \alternative {
    { fis-^) r4 r f8(-- e-^) r4 gis4.--^\markup "gliss." }
    { fis8-^ r4 r g8(-- gis-^) r4 e4.--^\markup "gliss." }
  }
  
  a2. cis2.
  b2.\glissando d2.\>
  \time 6/8
  r2.\!
  \time 12/8
  a2. cis4.\< ~ cis4 b8
  
  a4.\> r4.\! r2.
  
  R1*2*12/8
  r2. r4. r4 dis,8(--
  
  \repeat volta 2 {
    e-^) r4 r g8(-- gis-^) r4 r g8(--
  }
  \alternative {
    { fis-^) r4 r f8(-- e-^) r4 gis4.--^\markup "gliss." }
    { fis8-^ r4 r g8(-- gis-^) r4 e4.--^\markup "gliss." }
  }
  
  a2. cis2.
  b2.\glissando d2.\>
  \time 6/8
  r2.\!
  \time 12/8
  a2. cis4.\< ~ cis4 b8
  a1.\< g
  
  R1*3*12/8\!
  r2. r4. r4 dis8(--
  
  \repeat volta 2 {
    e-^) r4 r g8(-- gis-^) r4 r g8(--
  }
  \alternative {
    { fis-^) r4 r f8(-- e-^) r4 gis4.--^\markup "gliss." }
    { fis8-^ r4 r g8(-- gis-^) r4 e4.--^\markup "gliss." }
  }
  
  a2. cis2.
  b2.\glissando d2.\>
  \time 6/8
  r2.\!
  \time 12/8
  a2. cis4.\< ~ cis4 b8
  a1.\< g
  
  R1*4*12/8\!
  
  R1*8*12/8
  
  R1*3*12/8
  r2. r4. r4 dis8(--
  
  \repeat volta 2 {
    e-^) r4 r g8(-- gis-^) r4 r g8(--
  }
  \alternative {
    { fis-^) r4 r f8(-- e-^) r4 gis4.--^\markup "gliss." }
    { fis8-^ r4 r g8(-- gis-^) r4 e4.--^\markup "gliss." }
  }
  
  a2. cis2.
  b2.\glissando d2.\>
  \time 6/8
  r2.\!
  \time 12/8
  a2. cis4.\< ~ cis4 b8
  
  a4.\> r4.\! r2.
  R1*3*12/8
  
  dis1.\fermata
}


% Instrument Sheet
% -------------------------------------------------

%{
\header {
}

\paper {  	
  % distancia entre sistemas
  system-system-spacing #'basic-distance = #18
  
  % distancia entre el título y el primer sistema
  markup-system-spacing #'basic-distance = #20
}

<<
  \scoreMarkup
  \new Staff << \relative c'' \instrument >>
>>
%}
