\version "2.16.1"
%\include "imports/main.ly"


% Instrument Music
% -------------------------------------------------
alto = {

  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0
  
  \time 12/8
  \key a \major
  
  R1*7*12/8
  r2. r4. r4 gis8(--
  
  \repeat volta 2 {
    a-^) r4 r c8(-- cis-^) r4 r ais8(--
  }
  \alternative {
    { b-^) r4 r c8(-- cis-^) r4 cis4.--^\markup "gliss." } 
    { b8-^ r4 r c8(-- cis-^) r4 cis4.--^\markup "gliss." } 
  }
  
  e2. eis2.
  fis2.\glissando b2.\>
  \time 6/8
  r2.\!
  \time 12/8
  e,2. ~ e4.\< ~ e8 ees d 
  
  cis4.\> r4.\! r2.
  
  R1*2*12/8
  r2. r4. r4 gis8(--
  
  \repeat volta 2 {
    a-^) r4 r c8(-- cis-^) r4 r ais8(--
  }
  \alternative {
    { b-^) r4 r c8(-- cis-^) r4 cis4.--^\markup "gliss." } 
    { b8-^ r4 r c8(-- cis-^) r4 cis4.--^\markup "gliss." } 
  }
  
  e2. eis2.
  fis2.\glissando b2.\>
  \time 6/8
  r2.\!
  
  \time 12/8
  e,2. ~ e4. ~ e8 ees d 
  cis1.\< ~ cis
  
  R1*3*12/8\!
  r2. r4. r4 gis8(--
  
  \repeat volta 2 {
    a-^) r4 r c8(-- cis-^) r4 r ais8(--
  }
  \alternative {
    { b-^) r4 r c8(-- cis-^) r4 cis4.--^\markup "gliss." } 
    { b8-^ r4 r c8(-- cis-^) r4 cis4.--^\markup "gliss." } 
  }
  
  e2. eis2.
  fis2.\glissando b2.\>
  \time 6/8
  r2.\!
  
  \time 12/8
  e,2. ~ e4. ~ e8 ees d 
  cis1.\< ~ cis
  
  R1*4*12/8\!
  
  R1*8*12/8
  
  R1*3*12/8
  r2. r4. r4 gis8(--
  
  \repeat volta 2 {
    a-^) r4 r c8(-- cis-^) r4 r ais8(--
  }
  \alternative {
    { b-^) r4 r c8(-- cis-^) r4 cis4.--^\markup "gliss." } 
    { b8-^ r4 r c8(-- cis-^) r4 cis4.--^\markup "gliss." } 
  }
  
  e2. eis2.
  fis2.\glissando b2.\>
  \time 6/8
  r2.\!
  \time 12/8
  e,2. ~ e4.\< ~ e8 ees d 
  
  cis4.\> r4.\! r2.
  R1*3*12/8
  
  gis'1.\fermata
}


% Instrument Sheet
% -------------------------------------------------

%{
\header {
}

\paper {  	
  % distancia entre sistemas
  system-system-spacing #'basic-distance = #18
  
  % distancia entre el título y el primer sistema
  markup-system-spacing #'basic-distance = #20
}

<<
  \scoreMarkup
  \new Staff << \relative c'' \instrument >>
>>
%}
