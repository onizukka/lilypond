\version "2.16.1"
\include "imports/main.ly"


% Instrument Music
% -------------------------------------------------
tenorSax = {

  % Para pintar todos los números de compás debajo del pentagrama
  %\override Score.BarNumber #'break-visibility = #end-of-line-invisible
  %\override Score.BarNumber #'direction = #DOWN
  %\override Score.BarNumber #'extra-offset = #'(1.5 . 0)

  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0
	
  \key f \minor
  \time 5/4
  
  r8 aes4-. r8 aes4-. r4 g
  r8 aes4-. r8 aes4-. r4 g
  \bar "||"
  \mark \markup { \musicglyph #"scripts.segno" }
  r8 aes4-. r8 aes4-. r4 g
  r8 aes4-. r8 aes4-. r4 g
  
  \break
  r8 aes4-. r8 aes4-. r4 g
  r8 aes4-. r8 aes4-. r4 g
  r8 aes4-. r8 aes4-. r4 g
  r8 aes4-. r8 aes4-. r4 g
  
  \break
  r8 aes4-. r8 aes4-. r4 g
  r8 aes4-. r8 aes4-. r4 g
  r8 aes4-. r8 aes4-. r4 g
  r8 aes4 r8 aes4 r4 g
  
  \break
  r8 aes4-. r8 aes4-. r4 des
  r8 g,4-. r8 g4-. r4 ees'
  r8 f,4-. r8 f4-. r4 des'
  \break
  r8 ees,4-. r8 ees4-. r4 g
  
  r8 aes4-. r8 aes4-. r4 des
  r8 g,4-. r8 g4-. r4 ees'
  \break
  r8 f,4-. r8 f4-. r4 des'
  r8 e,4-. r8 e4-. r4 g
  
  r8 aes4-. r8 aes4-. r4 g
  \break
  r8 aes4-. r8 aes4-. r4 g
  r8 aes4-. r8 aes4-. r4 g
  r8 aes4-. r8 aes4-. r4 g
  
  r8 aes4-. r8 aes4-. r4 g
  \break
  \bar "||"
  \eolMarkup "To Coda"
  r8 aes4-. r8 aes4-. r4 g
  r8 aes4-. r8 aes4-. r4 g
  r8 aes4-. r8 aes4-. r4 g
  
  \bar "||"
  r8 aes4-. r8 aes4-. r4 g
  \break
  r8 aes4-. r8 aes4-. r4 g
  r8 aes4-. r8 aes4-. r4 g
  r8 aes4-. r8 aes4-. r4 g
  
  r8 aes4-. r8 aes4-. r4 g
  \break
  r8 aes4-. r8 aes4-. r4 g
  r8 aes4-. r8 aes4-. r4 g
  r8 aes4-. r8 aes4-. r4 g
  
  r8 aes4-. r8 aes4-. r4 g
  r8 aes4-. r8 aes4-. r4 g
  \break
  r8 aes4-. r8 aes4-. r4 g
  r8 aes4-. r8 aes4-. r4 g
  
  r8 aes4-. r8 aes4-. r4 g
  r8 aes4-. r8 aes4-. r4 g
  \break
  r8 aes4-. r8 aes4-. r4 g
  r8 aes4-. r8 aes4-. r4 g
  
  r8 aes4-. r8 aes4-. r4 g
  r8 aes4-. r8 aes4-. r4 g
  \break
  r8 aes4-. r8 aes4-. r4 g
  r8 aes4-. r8 aes4-. r4 g
  
  r8 aes4-. r8 aes4-. r4 g
  r8 aes4-. r8 aes4-. r4 g
  \break
  r8 aes4-. r8 aes4-. r4 g
  r8 aes4-. r8 aes4-. r4 g
  
  r8 aes4-. r8 aes4-. r4 g
  r8 aes4-. r8 aes4-. r4 g
  \break
  \bar "||"
  \mark \markup { \musicglyph #"scripts.coda" }
  \eolMarkup "D.S. al Coda"
  r8 aes4-. r8 aes4-. r4 g
  r8 aes4-. r8 aes4-. r4 g
  r8 aes4-. r8 aes4-. r4 g
  
  r8 aes4-. r8 aes4-. r4 g
  \break
  r8 aes4-. r8 aes4-. r4 g
  r8 aes4-. r8 aes4-. r4 g
  r8 aes4-. r8 aes4-. r4 g
  
  r8 aes4-. r8 aes4-. r4 g
  \break
  r8 aes4-. r8 aes4-. r4 g
  r8 aes4-. r8 aes4-. r4 g
  aes4-> r1
  
  \bar "|."
}


% Instrument Sheet
% -------------------------------------------------

\header {
  instrument = "Tenor Sax"
}

\paper {  	
  % distancia entre sistemas
  markup-system-spacing #'basic-distance = #25
  
  system-system-spacing #'basic-distance = #18
}

<<
  \scoreMarkup
  \new Staff << \relative c'' \tenorSax >>
>>
