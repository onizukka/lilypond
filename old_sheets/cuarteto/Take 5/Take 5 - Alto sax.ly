\version "2.16.1"
\include "imports/main.ly"


% Instrument Music
% -------------------------------------------------
altoSax = {

  % Para pintar todos los números de compás debajo del pentagrama
  %\override Score.BarNumber #'break-visibility = #end-of-line-invisible
  %\override Score.BarNumber #'direction = #DOWN
  %\override Score.BarNumber #'extra-offset = #'(1.5 . 0)

  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0
	
  \key c \minor
  \time 5/4
  
  r8 g4-. r8 g4-. r4 f
  r8 g4-. r8 g4-. r4 f
  \bar "||"
  \mark \markup { \musicglyph #"scripts.segno" }
  r8 g4-. r8 g4-. r4 f
  r8 g4-. r8 g4-. r4 f
  
  \break
  r8 g4-. r8 g4-. r4 f
  r8 g4-. r8 g4-. r4 f
  r8 g4-. r8 g4-. r4 f
  r8 c'4-. r8 c4-. g8 c ees f
  
  \break
  fis g fis f ees4-. g,-- \appoggiatura {aes8 a} bes4-.
  c2. bes16 c bes g f4
  g2. d'16 ees d c bes4
  r8 g4 r8 g4 r4 f
  
  \break
  r8 c'4-. r8 c4-. r4 c
  r8 bes4-. r8 bes4-. r4 bes
  r8 aes4-. r8 aes4-. r4 c
  \break
  r8 g4-. r8 g4-. g4 r
  
  c'8 ees4-> c8 aes4-. f8 g aes a 
  bes8 d4-> bes8 g4-. ees8 f fis g 
  \break
  aes8 c4-> aes8 f4-. d8 f bes aes
  g2. \trill r2
  
  r8 g,4-. r8 g4-. r4 f
  \break
  r8 g4-. r8 g4-. r4 f
  r8 g4-. r8 g4-. r4 f
  r8 c'4-. r8 c4-. g8 c ees f
  
  fis g fis f ees4-. g,-- \appoggiatura {aes8 a} bes4-.
  \break
  \bar "||"
  \eolMarkup "To Coda"
  c2. bes16 c bes g f4
  g2. bes16 c bes g f4
  g2. r2
  
  \bar "||"
  r8 g4-. r8 g4-. r4 f
  \break
  r8 g4-. r8 g4-. r4 f
  r8 g4-. r8 g4-. r4 f
  r8 g4-. r8 g4-. r4 f
  
  r8 g4-. r8 g4-. r4 f
  \break
  r8 g4-. r8 g4-. r4 f
  r8 g4-. r8 g4-. r4 f
  r8 g4-. r8 g4-. r4 f
  
  r8 g4-. r8 g4-. r4 f
  r8 g4-. r8 g4-. r4 f
  \break
  r8 g4-. r8 g4-. r4 f
  r8 g4-. r8 g4-. r4 f
  
  r8 g4-. r8 g4-. r4 f
  r8 g4-. r8 g4-. r4 f
  \break
  r8 g4-. r8 g4-. r4 f
  r8 g4-. r8 g4-. r4 f
  
  r8 g4-. r8 g4-. r4 f
  r8 g4-. r8 g4-. r4 f
  \break
  r8 g4-. r8 g4-. r4 f
  r8 g4-. r8 g4-. r4 f
  
  r8 g4-. r8 g4-. r4 f
  r8 g4-. r8 g4-. r4 f
  \break
  r8 g4-. r8 g4-. r4 f
  r8 g4-. r8 g4-. r4 f
  
  r8 g4-. r8 g4-. r4 f
  r8 g4-. r8 g4-. r4 f
  \break
  \bar "||"
  \mark \markup { \musicglyph #"scripts.coda" }
  \eolMarkup "D.S. al Coda"
  c'2. r4 f,4
  r8 g4-. r8 g4-. r4 f
  r8 g4-. r8 g4-. r4 f
  
  r2. bes16 c bes g f4
  \break
  g2. ~ g2
  r2. bes16 c bes g f4
  g2. ~ g2
  
  r8 g4-. r8 g4-. r4 f
  \break
  r8 g4-. r8 g4-. r4 f
  r8 g4-. r8 g4-. r4 f
  g4-> r1
  
  \bar "|."
}


% Instrument Sheet
% -------------------------------------------------

\header {
  instrument = "Alto Sax"
}

\paper {  	
  % distancia entre sistemas
  markup-system-spacing #'basic-distance = #25
  
  system-system-spacing #'basic-distance = #18
}

<<
  \scoreMarkup
  \new Staff << \relative c'' \altoSax >>
>>
