\version "2.16.1"
\include "imports/main.ly"


% Instrument Music
% -------------------------------------------------
sopranoSax = {

  % Para pintar todos los números de compás debajo del pentagrama
  %\override Score.BarNumber #'break-visibility = #end-of-line-invisible
  %\override Score.BarNumber #'direction = #DOWN
  %\override Score.BarNumber #'extra-offset = #'(1.5 . 0)

  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0
	
  \key f \minor
  \time 5/4
  
  r8 f4-. r8 f4-. r4 ees
  r8 f4-. r8 f4-. r4 ees
  \bar "||"
  \mark \markup { \musicglyph #"scripts.segno" }
  r8 f4-. r8 f4-. r4 ees
  r8 f4-. r8 f4-. c8 f aes bes
  
  \break
  b8 c b bes aes4-. c,4-- \appoggiatura {cis8 d} ees4-.
  f2. g16 aes g f ees4
  f2. ees16 f ees c bes4
  c2. r4 bes
  
  \break
  r8 c4-. r8 c4-. r4 bes
  r8 c4-. r8 c4-. r4 bes
  r8 c4-. r8 c4-. r4 bes
  r8 f'4 r8 f4 r4 ees
  
  \break
  f8 aes4-> f8 des4-. bes8 c des d
  ees8 g4-> ees8 c4-. aes8 bes b c
  des8 f4-> des8 bes4-. g8 aes bes b
  \break
  c b c des ees4 ees8 d ees e
  
  f8 f4-. r8 f4-. r4 f
  r8 ees4-. r8 ees4-. r4 f
  \break
  r8 des4-. r8 des4-. r4 f
  r8 bes,4-. r8 bes4-. c8 f aes bes
  
  b8 c b bes a4-. c,4-- \appoggiatura {cis8 d} ees4-.
  \break
  f2. g16 aes g f ees4
  f2. ees16 f ees c bes4
  c2. r4 bes
  
  r8 c4-. r8 c4-. r4 bes
  \break
  \bar "||"
  \eolMarkup "To Coda"
  c2. g'16 aes g f ees4
  f2. g16 aes g f ees4
  f2. r2
  
  \bar "||"
  \slashedGrace b8 c2. \appoggiatura {f,8 ees} c4-. r
  \break
  f4 ~ f8 g aes4-. r2
  f'8 c bes aes bes f-> ~ f4 aes-.
  f4 ~ f8 g aes c-> r2
  
  f,4. aes8-> ~ aes f8 g4 bes
  \break
  f4. aes8 ~ aes f g ees c bes
  f'4. aes8-> ~ aes f g4 bes
  f4. aes8 ~ aes f g c4.->
  
  r4 \appoggiatura {g8 aes a} bes2.-> r4
  r8 aes4 g8 f2 bes,4
  \break
  \slashedGrace a'8 bes2. r8 ees c aes
  r8 f8 ~ f2. c4
  
  f,4. g8-.-> ~ g ees f4 bes4-.->
  f4. g8-.-> ~ g ees f g aes bes
  \break
  c4. d8-.-> ~ d bes c4 f
  c4. d8-.-> ~ d bes c des ees f
  
  g2.-> \appoggiatura {ees8 c bes} aes4-. r
  f'4-> ~ f8 d bes4 r2
  \break
  ees2 \appoggiatura {c8 aes} g ees f g aes bes
  \slashedGrace b8 c f,4. f2.->
  
  bes4.-. bes'8-.-> r8 bes, aes'8 g f ees
  bes4.-. bes'8-.-> r4 f8 g aes4
  \break
  bes,4.-. bes'8-.-> r8 bes, aes'8 g f ees
  bes4.-. g'8 ~ g ees f4 ~ f8 d
  
  c2. ~ c2 ~ 
  c2. ~ c2 
  \break
  \bar "||"
  \mark \markup { \musicglyph #"scripts.coda" }
  \eolMarkup "D.S. al Coda"
  r8 c4-. r8 c4-. ees16 f ees c bes4
  c2. g'16 aes g f ees4
  f2. ~ f2
  
  r8 c4-. r8 c4-. r4 bes
  \break
  r8 c4-. r8 c4-. r4 bes
  r8 c4-. r8 c4-. r4 bes
  r8 c4-. r8 c4-. r4 bes
  
  r2. g'16 aes g f ees4
  \break
  f2. ~ f2 ~ 
  f2. ~ f2
  f4-> r1
  
  \bar "|."
}


% Instrument Sheet
% -------------------------------------------------

\header {
  instrument = "Soprano Sax"
}

\paper {  	
  % distancia entre el título y el primer sistema
  markup-system-spacing #'basic-distance = #25
  
  system-system-spacing #'basic-distance = #18
}

<<
  \scoreMarkup
  \new Staff << \relative c'' \sopranoSax >>
>>
