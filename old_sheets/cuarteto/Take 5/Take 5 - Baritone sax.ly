\version "2.16.1"
\include "imports/main.ly"


% Instrument Music
% -------------------------------------------------
baritoneSax = {

  % Para pintar todos los números de compás debajo del pentagrama
  %\override Score.BarNumber #'break-visibility = #end-of-line-invisible
  %\override Score.BarNumber #'direction = #DOWN
  %\override Score.BarNumber #'extra-offset = #'(1.5 . 0)

  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0
	
  \key c \minor
  \time 5/4
  
  c,4-> r8 c r4 g'-> r4
  c,4-> r8 c r4 g'-> r4
  \bar "||"
  \mark \markup { \musicglyph #"scripts.segno" }
  c,4-> r8 c r4 g'-> r4
  c,4-> r8 c r4 g'-> r4
  
  \break
  c,4-> r8 c r4 g'-> r4
  c,4-> r8 c r4 g'-> r4
  c,4-> r8 c r4 g'-> r4
  c,4-> r8 c r4 g'-> r4
  
  \break
  c,4-> r8 c r4 g'-> r4
  c,4-> r8 c r4 g'-> r4
  c,4-> r8 c r4 g'-> r4
  c,4-> r8 c r4 g'-> r4
  
  \break
  aes8 r r aes r4 d f-.
  g,8 r r g r4 c ees-.
  f,8 r r f r4 bes d-.
  \break
  ees,8 r r ees bes'4-. bes ees-.
  
  aes,8 r r aes r4 d f-.
  g,8 r r g r4 c ees-.  
  \break
  f,8 r r f r4 b d-.
  g,8 r r g d'4-. d g,-.
  
  c,4-> r8 c r4 g'-> r4
  \break
  c,4-> r8 c r4 g'-> r4
  c,4-> r8 c r4 g'-> r4
  c,4-> r8 c r4 g'-> r4
  
  c,4-> r8 c r4 g'-> r4
  \break
  \bar "||"
  \eolMarkup "To Coda"
  c,4-> r8 c r4 g'-> r4
  c,4-> r8 c r4 g'-> r4
  c,8 g'4-. c,8 g'4-. g g-.
  
  \bar "||"
  c,4-> r8 c r4 g'-> r4
  \break
  c,4-> r8 c r4 g'-> r4
  c,4-> r8 c r4 g'-> r4
  c,4-> r8 c r4 g'-> r4
  
  c,4-> r8 c r4 g'-> r4
  \break
  c,4-> r8 c r4 g'-> r4
  c,4-> r8 c r4 g'-> r4
  c,4-> r8 c r4 g'-> r4
  
  c,4-> r8 c r4 g'-> r4
  c,4-> r8 c r4 g'-> r4
  \break
  c,4-> r8 c r4 g'-> r4
  c,4-> r8 c r4 g'-> r4
  
  c,4-> r8 c r4 g'-> r4
  c,4-> r8 c r4 g'-> r4
  \break
  c,4-> r8 c r4 g'-> r4
  c,4-> r8 c r4 g'-> r4
  
  c,4-> r8 c r4 g'-> r4
  c,4-> r8 c r4 g'-> r4
  \break
  c,4-> r8 c r4 g'-> r4
  c,4-> r8 c r4 g'-> r4
  
  c,4-> r8 c r4 g'-> r4
  c,4-> r8 c r4 g'-> r4
  \break
  c,4-> r8 c r4 g'-> r4
  c,4-> r8 c r4 g'-> r4
  
  c,4-> r8 c r4 g'-> r4
  c,4-> r8 c r4 g'-> r4
  \break
  \bar "||"
  \mark \markup { \musicglyph #"scripts.coda" }
  \eolMarkup "D.S. al Coda"
  c,4-> r8 c r4 g'-> r4
  c,4-> r8 c r4 g'-> r4
  c,4-> r8 c r4 g'-> r4
  
  c,4-> r8 c r4 g'-> r4
  \break
  c,4-> r8 c r4 g'-> r4
  c,4-> r8 c r4 g'-> r4
  c,4-> r8 c r4 g'-> r4
  
  c,4-> r8 c r4 g'-> r4
  \break
  c,4-> r8 c r4 g'-> r4
  c,4-> r8 c r4 g'-> r4
  c,4-> r1
  
  \bar "|."
}


% Instrument Sheet
% -------------------------------------------------

\header {
  instrument = "Baritone Sax"
}

\paper {  	
  % distancia entre sistemas
  markup-system-spacing #'basic-distance = #25
  
  system-system-spacing #'basic-distance = #18
}

<<
  \scoreMarkup
  \new Staff << \relative c'' \baritoneSax >>
>>
