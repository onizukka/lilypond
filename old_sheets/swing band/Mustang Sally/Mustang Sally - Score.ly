\version "2.18.2"


% Descripción de funcionalidades
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% PLUGINS
% - \eolMark:					mark al final de línea
% - \bendBefore: 				solución al bendBefore (i.e. dis8 \bendBefore)
% - \rit: 						crea una porción de ritardando que debe terminar como las dinámicas, con un \!
% - \improOn, \improOff:	negra de improvisación
% - \emptyStaff:				Imprime un pentagrama vacío
% - \emptyBar:					Imprime un compás invisible (útil para indentados)



% LAYOUT
% - El aspecto de los silencios multicompás es como los de las partituras de jazz
% - Margen extra de las articulaciones para evitar la colisión con las ligaduras
% - Glissandos bonitos



% PAPER
% - Se imprime el título, instrumento y número de página en todas las páginas
% - Solo se debe tocar el page-count (si eso). Sale bonito tal cual.

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Descripción de funcionalidades
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% 03-09-14: Layout
% Ahora las articulaciones van por dentro de las ligaduras. Parece que es la mejor solución



% 04-09-14: Layout
% Los creciendos y decreciendos tienen la línea un poco más gruesa
%
% Markup
% Añadidas guías para correcta posición y markup de la indicación de tempo
%
% Plugins
% Añadida función \emptyStave para imprimir un pentagrama vacío.
% En algunas partituras hay un pentagrama vacío antes del coda (lo cual mola bastante, por cierto)



% 05-09-14: Plugins
% Añadido a la función \emptyStave el esconder la armadura de la clave, que no estaba.



% 08-09-14: Layout
% Ahora no se muestra la cancelación de accidentales al haber un cambio de armadura



% 09-09-14: Chords
% Añadida sentencia para que los acordes menores salgan con un guión en vez de con una M minúscula



% 10-09-14: Plugins
% Mejorada la función \bendAfter.
% Ahora es genérica, y se puede definir cualquier texto en un lateral que haga las funciones de ornamento



% 15-09-14: Layout
% Añadido un poco más de margen a los reguladores



% 16-09-14: Plugins
% Mejorada la función para imprimir barras de improvisación.
% Antes se llamaba al comando \rs para pintar una barra que ocupaba una negra
% Se reemplaza \rs por \restImproOn y \restImproOff, siendo esta opción mucho más flexible.
% Cuando se activa \restImproOn, todo silencio se convierte en barra de improvisación, respetando su duración.



% 20-09-14: Plugins
% Mejorada la función para imprimir barras de improvisación.
% Se reemplaza \restImproOn y \restImproOff por \improOn y \improOff
% Ahora, también las notas del pentagrama salen como barra de improvisación.
% (los comandos \improvisationOn e \improvisationOff de lilypond no se llevan bien con LilyJAZZ)
% Se ha añadido un grob correspondiente a los slash de la fuente LilyJAZZ en el archivo lily-jazz.ly



% 20-09-14: Layout
% Comentada la línea que oculta la cancelación de accidentales al haber cambio de armadura.
% Realmente, la cancelación es muy bienvenida.



% 01-10-14: Plugins
% Renombrada la función \emptyStave a \emptyStaff (stave es el plural de staff) y 
% corregida (ya no muestra el número de compás).
% Recordar que después de usar \emptyStaff se necesita redefinir el 
% número de compás con \set Score.currentBarNumber = #xx
%
% Añadida la función \emptyBar que oculta un compás entero. Se puede usar para crear un 
% efecto de indentación en un pentagrama dado.
% Recordar que después de usar \emptyBar se necesita:
% - redefinir el número de compás con \set Score.currentBarNumber = #xx
% - volver a pintar la clave con \clef
% - volver a pintar la armadura con \key



% 15-10-14: Language
% Cambiado el lenguaje de entrada de notas del finlandes al inglés
% La diferencia es que ahora los sostenidos se escriben "-s" en vez de "-is"
% y los bemoles "-f" en vez de "-es".
%
% Estructura
% Para poder autoincluir los archivos de instrumento en otros 
% (para montar un midi, o un score), se han movido los plugins a un archivo separado
% y ahora se utiliza la función \includeOnce, evitando así el problema de las
% dependencias circulares.
%
% Global
% Todas las instrucciones que se colocaban justo antes de empezar la música están ahora
% recogidas en una función \global.
% A \global se le ha añadido instrucciones para que agrupe las corcheas de dos en dos
% en vez de cuatro en cuatro (el comportamiento por defecto).
%
% Plugins
% Se le ha aumentado el grosor de los bordes de la función \boxed



% 26-10-14: Chords
% Añadido el include "chord-exception.ily" que recoge una mejor notación de los acordes.
% Actualmente, recoge los acordes maj7, 7(b5), 7(#5), 7(b9), 7(#9), 7(add13), maj9(add13), 9(#11)
% Se irá añadiendo acordes a menudo.
 

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Dependencias
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% \includeOnce
% Incluye una dependencia solo una vez. Así se pueden incluir los archivos en otros 
% (para montar un score por ejemplo) sin liarla parda con dependencias cíclicas.

includeOnce = #(
	define-void-function (parser location filename) (string?) (
		if (not (defined? (string->symbol filename))) (
			begin (
				ly:parser-include-string parser (
					string-concatenate (list "\\include \"" filename "\"")
				)
			)
			(primitive-eval (list 'define (string->symbol filename) #t))
		)
	)
)



% LilyJAZZ
\includeOnce "includes/ignatzek-jazz-chords.ily"
\includeOnce "includes/lily-jazz.ily"



% Language
\language "english"



% Plugins
\includeOnce "includes/plugins-1.2.ily"



% Chord Exceptions
\includeOnce "includes/chord-exceptions.ily"

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~











% Disposición
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Cabecera
\header {
	title = "Mustang Sally"
	subtitle = "(as performed by Buddy Guy)"
	instrument = ""
	composer = "Mack Rice"
	arranger = "Arr: Gabriel Palacios"
	poet = ""
	tagline = "gabriel.ps.ms@gmail.com"
}

% Tamaño de las notas y pentagrama
#(set-global-staff-size 20)

% Márgenes, fuentes y distancias del papel
\paper {
	% El definir otro tipo de fuente no permite cambiar el tamaño global del staff. Es un bug conocigo
	#(define fonts (make-pango-font-tree "LilyJAZZText" "Nimbus Sans" "Luxi Mono" (/ 20 20)))

	% distancia entre la parte superior de la página y el título
	top-markup-spacing #'basic-distance = 5

	% distancia entre el título y el primer sistema
	markup-system-spacing #'basic-distance = 25

	% distancia entre el texto y el primer sistema 
	top-system-spacing #'basic-distance = 15

	% márgen inferior
	bottom-margin = 20
	
	% intentado del primer sistema
	indent = 0
	 
	% número de páginas
	% page-count = 1

	ragged-last-bottom = ##f 

	oddHeaderMarkup = \markup {
	 \on-the-fly #not-first-page \fill-line {
		\fromproperty #'page:page-number-string
		\fromproperty #'header:title
		\fromproperty #'header:instrument
	 }
	}

	evenHeaderMarkup = \markup {
	 \on-the-fly #not-first-page \fill-line {
		\fromproperty #'header:instrument
		\fromproperty #'header:title
		\fromproperty #'page:page-number-string
	 }
	}
}

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~







% Elementos comunes de la partitura
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
	% Oculta el primer pentagrama del markup
	\override Staff.VerticalAxisGroup #'remove-first = ##t
	
	% Indicación de tiempo
	\once \override Score.RehearsalMark.X-offset = #5
	\once \override Score.RehearsalMark.extra-offset = #'(0 . 3)
	\mark "Medium Rock"

	s8
	s1*4
	
	\mark \markup \boxed "A"
	s1*24
	
	\mark \markup \boxed "B"
	s1*24
	
	\mark \markup \boxed "C"
	s1*24
	
	\mark \markup \boxed "D"
	s1*24
	
	\mark \markup \boxed "E"
	s1*24
	
	\once \override Score.RehearsalMark.X-offset = #5
	\mark \markup {
		\column { \boxed "F" }
		\column { "(Guitar solo)" }
	}
	
	s1*8
	\mark \markup {
		\column { \boxed "G" }
		\column { "(x2)" }
	}
}



% Función musical global
global = {
	% Para pintar todos los números de compás debajo del pentagrama
	% \override Score.BarNumber #'break-visibility = #end-of-line-invisible
	% \override Score.BarNumber #'direction = #DOWN
	% \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

	\jazzOn
	\compressFullBarRests

	% Arregla el problema de tener el scoreMarkup añadido
	\revert MultiMeasureRest #'staff-position
	\override Voice.Rest #'staff-position = #0	

	\set Timing.beamExceptions = #'()
	\set Timing.baseMoment = #(ly:make-moment 1/4)
	\set Timing.beatStructure = #'(1 1 1 1)
	
	\override ParenthesesItem.font-size = #0
}

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Elementos específicos del instrumento
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Cifrado
scoreChords = \chords {    
	\set minorChordModifier = \markup { "-" }
	\set chordNameExceptions = #chordNameExceptions
	\override ChordName.font-size = #2.5


	% c1:m f2:m g2:m7
}

% Cifrado del solo de trombón
tromboneSoloChords = \chords {    
	\set minorChordModifier = \markup { "-" }
	\set chordNameExceptions = #chordNameExceptions
	\override ChordName.font-size = #2.5

	s8
	s1*68
	g1:7
	s1
	
	f1:7
	s1
	c1:7
	s1
	s1
}

% Música
trumpet = \relative c'' {
  
	\global
	\key c \major
	
	\partial 8
	r8

	% intro
	r4 \f c2 -> ~ c8 b (
	c8 -> ) r c -> r r2
	r4 c2 -> ~ c8 b (
	c8 -> ) r c -> r r2
	
	% verse I
	\break
	R1*16
	
	\break
	<< { \override Voice.Rest #'staff-position = #0
	b1 -> \mf ~
	b2. ~ b8 bf (
	a4 -^ ) r4 r2 } \\ { s1 s2 s4. \< s8 \f s1 } >>
	R1 
	
	\break
	e1 \mf 
	d1
	<< { \override Voice.Rest #'staff-position = #0
	bf1 ~
	bf2. r4 } \\ { s1 s2 s4 \< s4 \! } >>
	
	% verse II
	\break
	r4 \mf c'2 -> ~ c8 b (
	c8 -> ) r c -> r r2
	r4 c2 -> ~ c8 b (
	c8 -> ) r c -> r r2
	
	\break
	r4 c2 -> ~ c8 b (
	c8 -> ) r c -> r r2
	r4 c2 -> ~ c8 b (
	c8 -> ) r c -> r r2
	
	\break
	r4 a2 -> ~ a8 gs (
	a8 -> ) r a -> r r2
	r4 a2 -> ~ a8 gs (
	a8 -> ) r a -> r r2
	
	\break
	r4 c2 -> ~ c8 b (
	c8 -> ) r c -> r r2
	r4 c2 -> ~ c8 b (
	c8 -> ) r c -> r r2
	
	\break
	R1
	r2 bf'4. -> \mf \< g8 ( \f
	f4 -^ ) r4 r2
	R1
	
	\break
	e,1 \mf 
	d1
	bf2. \fp \< r4 \!
	c'8 ^ "(Shout)" \f -. c -. bf -. a -. g8 -. f -. f16 ( ef ) e -- c -^
	
	% Shout
	\break
	r4 r8 c'8 \bendBefore ~ c2
	r4 r16 g a [ c ] ef8 -. d -. c16 ( d32 c a16 -- ) c -^
	\break
	r16 g' -> \bendAfter -2 r8 r16 f ef [ ( e ] ) f fs g ( \parenthesize c, ) f ( ef ) e -- c -^
	R1
	
	\break
	r8 c8 -> ~ c2 \bendAfter -3 bf16 ( g ) a -- g -^
	r4 r16 g a [ c ] ef8 -. d -. c16 ( d32 c a16 -- ) c -^
	\break
	r4 c'8 -> \bendAfter -5 r f,16 ( ef ) e f fs ( g ) e \bendBefore -- c -^
	r2 r4 r8. c16 -> ~
	
	\break
	c4 ^ "(Drums solo background)" ~ c8. c16 -^ r2
	c4 -- c8 -^ r8 r2
	r4 r8 c8 -> \fp \< ~ c2
	r8 \f f -> ~ f16 fs8. -> g2 -> \bendAfter -6
	
	\break
	r4 \mf ^ "(Keys solo background)" bf,16 -> ( af a bf -^ ) r2
	r4 bf16 -> ( af a bf -^ ) r8 bf -^ r4
	r4 bf16 -> ( af a bf -^ ) r2
	r2 r8 cs -> ~ cs16 c8. ->
	
	\break
	<< { \override Voice.Rest #'staff-position = #0
	b1 -> ^ "(Trombone solo)" \mf ~
	b2. ~ b8 bf (
	a4 -^ ) r4 r2 } \\ { s1 s2 s4. \< s8 \f s1 } >>
	R1 
	
	\break
	e1 \mf 
	d1
	bf2. r4 \!
	r8 ^ "(Ensemble)" \f c' -. bf -. a -. g -. f -. fs16 g8. ->
	
	% verse III
	\break
	c8 -> r r4 r2
	R1*15
	
	\break
	<< { \override Voice.Rest #'staff-position = #0
	b1 -> \mf ~
	b2. ~ b8 bf (
	a4 -^ ) r4 r2 } \\ { s1 s2 s4. \< s8 \f s1 } >>
	R1 
	
	\break
	e1 \mf 
	d1
	<< { \override Voice.Rest #'staff-position = #0
	bf1 ~
	bf2. r4 } \\ { s1 s2 s4 \< s4 \! } >>
	
	% verse IV
	\break
	r4 r8 g' -> r g -> r f (
	e4 -. ) r4 r2
	r4 r8 g -> r g -> r f (
	e4 -. ) r4 r2
	
	\break
	r4 r8 g -> r g -> r f (
	e4 -. ) r4 r2
	r4 r8 g -> r g -> r fs (
	g4 -. ) r4 r2
	
	\break
	r4 r8 a -> r a -> r g (
	f4 -. ) r4 r2
	r4 r8 a -> r a -> r gs (
	a4 -. ) r4 r2
	
	\break
	r4 r8 g -> r g -> r f (
	e4 -. ) r4 r2
	r4 r8 g -> r g -> r fs (
	g4 -. ) r4 r2
	
	\break
	R1
	r2 bf'4. -> \mf \< g8 ( \f
	f4 -^ ) r4 r2
	R1
	
	\break
	e,1 \mf 
	d1
	bf2. \fp \< r4 \!
	c'8 \f -. c -. bf -. a -. g8 -. f -. f16 ( ef ) e -- c -^
	
	\break
	R1*8
	
	\break
	\repeat volta 2 {
		c'8 \mf ( -> b ) c4 -^ r8 c -> r c -> ~ (
		c8 b ) c4 -^ r2
		<< { \override Voice.Rest #'staff-position = #0
		c8 ( -> b ) c4 -^ r8 c -> r c -> ~ 
		c2 ~ c4 r } \\ { s2 s4. s8 \fp \< s2 s4 \mf s4 } >>
		
		\break
		c8 ( -> b ) c4 -^ r8 c -> r c -> ~ (
		c8 b ) c4 -^ r2
	}
	
	\alternative {
		{ << { \override Voice.Rest #'staff-position = #0
		  c8 ( -> b ) c4 -^ r8 c -> r c -> ~ 
		  c2 ~ c4 r } \\ { s2 s4. s8 \fp \< s2 s4 \mf s4 } >> }
		
		{ \break
		  << { \override Voice.Rest #'staff-position = #0
		  c8 ( -> b ) c4 -^ r8 c -> r c -> ~ 
		  c2 ~ c4 r } \\ { s2 s4. s8 \fp \< s2 s4 \mf s4 } >> }
	}
	
	c8 \mf ( -> b ) c4 -^ r2
	
	\bar "|."
}

altoSax = \relative c'' {
  
	\global
	\key c \major
	
	\partial 8
	r8

	% intro
	r4 \f g2 -> ~ g8 fs (
	g8 -> ) r g -> r r2
	r4 g2 -> ~ g8 fs (
	g8 -> ) r g -> r r2
	
	% verse I
	\break
	R1*16
	
	\break
	<< { \override Voice.Rest #'staff-position = #0
	g1 -> \mf ~
	g2. ~ g8 gf (
	f4 -^ ) r4 r2 } \\ { s1 s2 s4. \< s8 \f s1 } >>
	R1 
	
	\break
	e1 \mf 
	d1
	<< { \override Voice.Rest #'staff-position = #0
	bf1 ~
	bf2. r4 } \\ { s1 s2 s4 \< s4 \! } >>
	
	% verse II
	\break
	r4 \mf g'2 -> ~ g8 fs (
	g8 -> ) r g -> r r2
	r4 g2 -> ~ g8 fs (
	g8 -> ) r g -> r r2
	
	\break
	r4 g2 -> ~ g8 fs (
	g8 -> ) r g -> r r2
	r4 g2 -> ~ g8 fs (
	g8 -> ) r g -> r r2
	
	\break
	r4 f2 -> ~ f8 e (
	f8 -> ) r f -> r r2
	r4 f2 -> ~ f8 e (
	f8 -> ) r f -> r r2
	
	\break
	r4 g2 -> ~ g8 fs (
	g8 -> ) r g -> r r2
	r4 g2 -> ~ g8 fs (
	g8 -> ) r g -> r r2
	
	\break
	R1
	<< { \override Voice.Rest #'staff-position = #0
	b2. -> \mf ~ b8 bf8 (
	a4 -^ ) r4 r2 } \\ { s2 s4. \< s8 \f s1 } >>
	R1
	
	\break
	e1 \mf 
	d1
	bf2. \fp \< r4 \!
	c'8 ^ "(Shout)" \f -. c -. bf -. a -. g8 -. f -. f16 ( ef ) e -- c -^
	
	% Shout
	\break
	r4 r8 bf'8 \bendBefore ~ bf2
	r4 r16 g a [ c ] ef8 -. d -. c16 ( d32 c a16 -- ) c -^
	\break
	r16 g' -> \bendAfter -2 r8 r16 f ef [ ( e ] ) f fs g ( \parenthesize c, ) f ( ef ) e -- c -^
	r16 bf8. -> \bendBefore bf8 -. a -. bf4 \bendBefore -> a8. \bendBefore -> g16 -^
	
	\break
	r8 bf8 -> ~ bf2 \bendAfter -3 bf16 ( g ) a -- e -^
	r4 r16 g a [ c ] ef8 -. d -. c16 ( d32 c a16 -- ) c -^
	\break
	r4 d8 -> \bendAfter -5 r f16 ( ef ) e f fs ( g ) e \bendBefore -- g, -^
	r8 c -. bf -. a -. g8 -. f -. fs16 ( g c, )a' -> ~
	
	\break
	a4 ^ "(Drums solo background)" ~ a8. a16 -^ r2
	a4 -- g8 -^ r8 r2
	r4 r8 a8 -> \fp \< ~ a2
	r8 \f a -> ~ a16 as8. -> b2 -> \bendAfter -6
	
	\break
	r4 \mf ^ "(Keys solo background)" g16 -> ( f fs g -^ ) r2
	r4 g16 -> ( f fs g -^ ) r8 g -^ r4
	r4 g16 -> ( f fs g -^ ) r2
	r2 r8 g -> ~ g16 fs8. ->
	
	\break
	<< { \override Voice.Rest #'staff-position = #0
	f1 -> ^ "(Trombone solo)" \mf ~
	f2. ~ f8 gf (
	f4 -^ ) r4 r2 } \\ { s1 s2 s4. \< s8 \f s1 } >>
	R1 
	
	\break
	e1 \mf 
	d1
	bf2. r4 \!
	r8 ^ "(Ensemble)" \f c' -. bf -. a -. g -. f -. fs16 ef8. ->
	
	% verse III
	\break
	e8 -> r r4 r2
	R1*15
	
	\break
	<< { \override Voice.Rest #'staff-position = #0
	g1 -> \mf ~
	g2. ~ g8 gf (
	f4 -^ ) r4 r2 } \\ { s1 s2 s4. \< s8 \f s1 } >>
	R1 
	
	\break
	e1 \mf 
	d1
	<< { \override Voice.Rest #'staff-position = #0
	bf1 ~
	bf2. r4 } \\ { s1 s2 s4 \< s4 \! } >>
	
	% verse IV
	\break
	r4 r8 e -> r e -> r d (
	c4 -. ) r4 r2
	r4 r8 e -> r e -> r d (
	c4 -. ) r4 r2
	
	\break
	r4 r8 e -> r e -> r d (
	c4 -. ) r4 r2
	r4 r8 e -> r e -> r ds (
	e4 -. ) r4 r2
	
	\break
	r4 r8 f -> r f -> r f (
	ef4 -. ) r4 r2
	r4 r8 f -> r f -> r e? (
	f4 -. ) r4 r2
	
	\break
	r4 r8 e -> r e -> r d (
	c4 -. ) r4 r2
	r4 r8 e -> r e -> r ds (
	e4 -. ) r4 r2
	
	\break
	R1
	<< { \override Voice.Rest #'staff-position = #0
	b'2. -> \mf ~ b8 bf8 (
	a4 -^ ) r4 r2 } \\ { s2 s4. \< s8 \f s1 } >>
	R1
	
	\break
	e1 \mf 
	d1
	bf2. \fp \< r4 \!
	c'8 \f -. c -. bf -. a -. g8 -. f -. f16 ( ef ) e -- c -^
	
	\break
	R1*8
	
	\break
	\repeat volta 2 {
		e8 \mf ( -> ds ) e4 -^ r8 e -> r e -> ~ (
		e8 ds ) e4 -^ r2
		<< { \override Voice.Rest #'staff-position = #0
		e8 ( -> ds ) e4 -^ r8 e -> r e -> ~ 
		e2 ~ e4 r } \\ { s2 s4. s8 \fp \< s2 s4 \mf s4 } >>
		
		\break
		e8 ( -> ds ) e4 -^ r8 e -> r e -> ~ (
		e8 ds ) e4 -^ r2
	}
	
	\alternative {
		{ << { \override Voice.Rest #'staff-position = #0
		  	e8 ( -> ds ) e4 -^ r8 e -> r e -> ~ 
		  	e2 ~ e4 r } \\ { s2 s4. s8 \fp \< s2 s4 \mf s4 } >> }
		
		{ \break
		  << { \override Voice.Rest #'staff-position = #0
		  e8 ( -> ds ) e4 -^ r8 e -> r e -> ~ 
		  e2 ~ e4 r } \\ { s2 s4. s8 \fp \< s2 s4 \mf s4 } >> }
	}
	
	e8 \mf ( -> ds ) e4 -^ r2
	
	\bar "|."
}

tenorSax = \relative c' {
  
	\global
	\key c \major
	
	\partial 8
	bf,8 ( \f

	% intro
	c4 -. ) e'2 -> ~ e8 ds (
	e8 -> ) r e -> r r4 r8 bf, (
	c4 -. ) e'2 -> ~ e8 ds (
	e8 -> ) r e -> r r4 r8 \mf bf, (
	
	% verse I
	\break
	c4 -. ) r4 r2
	r2 r4 r8 bf (
	c4 -. ) r4 r2
	r2 r4 r8 bf (
	
	\break
	c4 -. ) r4 r2
	r2 r4 r8 bf (
	c4 -. ) r4 r2
	r2 r4 r8 ef (
	
	\break
	f4 -. ) r4 r2
	r2 r4 r8 ef (
	f4 -. ) r4 r2
	r2 r4 r8 bf, (
	
	\break
	c4 -. ) r4 r2
	r2 r4 r8 bf (
	c4 -. ) r4 r2
	R1
	
	\break
	<< { \override Voice.Rest #'staff-position = #0
	d'1 -> \mf ~
	d2. ~ d8 df (
	c4 -^ ) r4 r2 } \\ { s1 s2 s4. \< s8 \f s1 } >>
	R1 
	
	\break
	c,1 \mf 
	bf1
	<< { \override Voice.Rest #'staff-position = #0
	c1 ~
	c2. r8 bf \mf ( 
	
	
	% verse II
	\break
	c4 -. ) } \\ { s1 s2 s4 \< s4 \! s4 } >> e'2 -> ~ e8 ds (
	e8 -> ) r e -> r r4 r8 bf, (
	c4 -. ) e'2 -> ~ e8 ds (
	e8 -> ) r e -> r r4 r8 bf, (
	
	\break
	c4 -. ) e'2 -> ~ e8 ds (
	e8 -> ) r e -> r r4 r8 bf, (
	c4 -. ) e'2 -> ~ e8 ds (
	e8 -> ) r e -> r r4 r8 ef, (
	
	\break
	f4 -. ) ef'2 -> ~ ef8 d (
	ef8 -> ) r ef -> r r4 r8 ef, (
	f4 -. ) ef'2 -> ~ ef8 d (
	ef8 -> ) r ef -> r r4 r8 bf, (
	
	\break
	c4 -. ) e'2 -> ~ e8 ds (
	e8 -> ) r e -> r r4 r8 bf, (
	c4 -. ) e'2 -> ~ e8 ds (
	e8 -> ) r e -> r r2
	
	\break
	<< { \override Voice.Rest #'staff-position = #0
	r2 g2 \mf -> ~
	g2. ~ g8 gf8 (
	f4 -^ ) r4 r2 } \\ { s1 s2 s4. \< s8 \f s1 } >>
	R1
	
	\break
	c,1 \mf 
	bf1
	c2. \fp \< r4 \!
	c'8 ^ "(Shout)" \f -. c -. bf -. a -. g8 -. f -. f16 ( ef ) e -- c -^
	
	\break
	r4 r8 f'8 \bendBefore ~ f2
	r4 r16 g, a [ c ] ef8 -. d -. c16 ( d32 c a16 -- ) c -^
	\break
	r16 g' -> \bendAfter -2 r8 r16 f ef [ ( e ] ) f fs g ( \parenthesize c, ) f ( ef ) e -- c -^
	r16 f8. \bendBefore f8 -. e f4 \bendBefore e8. \bendBefore d16 -. 
	
	\break
	r8 fs8 -> ~ fs2 \bendAfter -3 bf,16 ( g ) a -- d -^
	r4 r16 g, a [ c ] ef8 -. d -. c16 ( d32 c a16 -- ) c -^
	\break
	r4 bf'8 -> \bendAfter -5 r f16 ( ef ) e f fs ( g ) e \bendBefore -- d -^
	r8 c -. bf -. a -. g8 -. f -. fs16 ( g c ) ef -> ~
	
	\break
	ef4 ^ "(Drums solo background)" ~ ef8. ef16 -^ r2
	ef4 -- ef8 -^ r8 r2
	r4 r8 ef8 -> \fp \< ~ ef2
	r8 \f f -> ~ f16 fs8. -> g2 -> \bendAfter -6
	
	\break
	r4 \mf ^ "(Keys solo background)" e16 -> ( d ds e -^ ) r2
	r4 e16 -> ( d ds e -^ ) r8 e -^ r4
	r4 e16 -> ( d ds e -^ ) r2
	r2 r8 e -> ~ e16 ef8. ->
	
	\break
	<< { \override Voice.Rest #'staff-position = #0
	d1 -> ^ "(Trombone solo)" \mf ~
	d2. ~ d8 df (
	c4 -^ ) r4 r2 } \\ { s1 s2 s4. \< s8 \f s1 } >>
	R1 
	
	\break
	c,1 \mf 
	bf1
	c2. r4 \!
	r8 ^ "(Ensemble)" \f c' -. bf -. a -. g -. f -. fs16 b,8. ->
	
	% verse III
	\break
	c4 -. r4 r2
	r2 r4 r8 \mf bf? (
	c4 -. ) r4 r2
	r2 r4 r8 bf (
	
	\break
	c4 -. ) r4 r2
	r2 r4 r8 bf (
	c4 -. ) r4 r2
	r2 r4 r8 ef (
	
	\break
	f4 -. ) r4 r2
	r2 r4 r8 ef (
	f4 -. ) r4 r2
	r2 r4 r8 bf, (
	
	\break
	c4 -. ) r4 r2
	r2 r4 r8 bf (
	c4 -. ) r4 r2
	R1
	
	\break
	<< { \override Voice.Rest #'staff-position = #0
	d'1 -> \mf ~
	d2. ~ d8 df (
	c4 -^ ) r4 r2 } \\ { s1 s2 s4. \< s8 \f s1 } >>
	R1 
	
	\break
	c,1 \mf 
	bf1
	<< { \override Voice.Rest #'staff-position = #0
	c1 ~
	c2. r8 bf \mf ( 
	
	% verse IV
	\break
	c4 -. ) } \\ { s1 s2 s4 \< s4 \! s4 } >> r8 c' -> r c -> r c (
	bf4 -. ) r4 r4 r8 bf, (
	c4 -. ) r8 c' -> r c -> r c (
	bf4 -. ) r4 r4 bf, (
	
	\break
	c4 -. ) r8 c' -> r c -> r c (
	bf4 -. ) r4 r4 r8 bf, (
	c4 -. ) r8 c' -> r c -> r b? (
	c4 -. ) r4 r4 r8 ef, (
	
	\break
	f4 -. ) r8 ef' -> r ef -> r d (
	c4 -. ) r4 r4 r8 ef, (
	f4 -. ) r8 ef' -> r ef -> r d (
	ef4 -. ) r4 r4 r8 bf, (
	
	\break
	c4 -. ) r8 c' -> r c -> r c (
	bf4 -. ) r4 r4 r8 bf, (
	c4 -. ) r8 c' -> r c -> r b? (
	c4 -. ) r4 r2
	
	\break
	<< { \override Voice.Rest #'staff-position = #0
	r2 g'2 \mf -> ~
	g2. ~ g8 gf8 (
	f4 -^ ) r4 r2 } \\ { s1 s2 s4. \< s8 \f s1 } >>
	R1
	
	\break
	c,1 \mf 
	bf1
	c2. \fp \< r4 \!
	c'8 \f -. c -. bf -. a -. g8 -. f -. f16 ( ef ) e -- c -^
	
	% solo guitarra
	\break
	R1
	r2 r4 r8 bf (
	c4 -. ) r4 r2
	r2 r4 r8 bf (
	
	\break
	c4 -. ) r4 r2
	r2 r4 r8 bf (
	c4 -. ) r4 r2
	r2 r4 r8 bf (
	
	\repeat volta 2 {
		\break
		c4 -. ) r4 r2
		r2 r4 r8 bf (
		c4 -. ) r4 r2
		r2 r4 r8 bf (
		
		\break
		c4 -. ) r4 r2
		r2 r4 r8 bf (
	}
	
	\alternative {
		{ c4 -. ) r4 r2
		  r2 r4 r8 bf \laissezVibrer }
		
		{ \break
		  c4 -. \repeatTie r4 r2
		  R1 }
	}
	
	c'4 c,4 -. r2
	
	\bar "|."
}

trombone = \relative c' {
  
	\global
	\clef bass
	\key c \major
	
	\partial 8
	r8

	% intro
	r4 \f bf2 -> ~ bf8 a (
	bf8 -> ) r bf -> r r2
	r4 bf2 -> ~ bf8 a (
	bf8 -> ) r bf -> r r2
	
	% verse I
	\break
	R1*16
	
	\break
	<< { \override Voice.Rest #'staff-position = #0
	g1 -> \mf ~
	g2. ~ g8 gf (
	f4 -^ ) r4 r2 } \\ { s1 s2 s4. \< s8 \f s1 } >>
	R1 
	
	\break
	e'1 \mf 
	d1
	<< { \override Voice.Rest #'staff-position = #0
	bf1 ~
	bf2. r4 } \\ { s1 s2 s4 \< s4 \! } >>
	
	% verse II
	\break
	r4 \mf bf2 -> ~ bf8 a (
	bf8 -> ) r bf -> r r2
	r4 bf2 ~ bf8 a (
	bf8 -> ) r bf -> r r2
	
	\break
	r4 bf2 -> ~ bf8 a (
	bf8 -> ) r bf -> r r2
	r4 bf2 ~ bf8 a (
	bf8 -> ) r bf -> r r2
	
	\break
	r4 c2 -> ~ c8 b (
	c8 -> ) r c -> r r2
	r4 c2 -> ~ c8 b (
	c8 -> ) r c -> r r2
	
	\break
	r4 bf2 -> ~ bf8 a (
	bf8 -> ) r bf -> r r2
	r4 bf2 ~ bf8 a (
	bf8 -> ) r bf -> r r2
	
	\break
	<< { \override Voice.Rest #'staff-position = #0
	d1 -> \mf ~
	d2. ~ d8 df (
	c4 -^ ) r4 r2 } \\ { s1 s2 s4. \< s8 \f s1 } >>
	R1 
	
	\break
	e1 \mf 
	d1
	bf2. \fp \< r4 \!
	c8 ^ "(Shout)" \f -. c -. bf -. a -. g8 -. f -. f16 ( ef ) e -- c -^
	
	% Shout
	\break
	r4 r8 d'8 \bendBefore ~ d2
	r4 r16 g, a [ c ] ef8 -. d -. c16 ( d32 c a16 -- ) c -^
	\break
	r16 g' -> \bendAfter -2 r8 r16 f ef [ ( e ] ) f fs g ( \parenthesize c, ) f ( ef ) e -- c -^
	r16 d8. \bendBefore d8 -. c d4 \bendBefore c8. \bendBefore bf16 -. 
	
	\break
	r8 d8 -> ~ d2 \bendAfter -3 bf16 ( g ) a -- d -^
	r4 r16 g, a [ c ] ef8 -. d -. c16 ( d32 c a16 -- ) c -^
	\break
	r4 f8 -> \bendAfter -5 r f16 ( ef ) e f fs ( g ) e \bendBefore -- bf -^
	r8 c -. bf -. a -. g8 -. f -. fs16 ( g c, ) f -> ~
	
	\break
	f4 ^ "(Drums solo backgrounds)" ~ f8. f16 -^ r2
	f4 -- f8 -^ r8 r8 g, -> ~ g16 gs8. ->
	a8 -> r8 r8 f' -> \fp \< ~ f2
	r8 \f f -> ~ f16 fs8. -> g2 -> \bendAfter -6
	
	\break
	R1*4
	
	\break
	\improOn 
	r4 ^ "(Trombone Solo)" \repeat unfold 7 { r4 } 
	r4 ^ "Break!" \repeat unfold 7 { r4 }
	
	\break
	\repeat unfold 12 { r4 }
	\improOff
	r8 ^ "(Ensemble)" \f c -. bf -. a -. g -. f -. fs16 a8. ->
	
	% verse III
	\break
	bf8 -> r r4 r2
	R1*15
	
	\break
	<< { \override Voice.Rest #'staff-position = #0
	g1 -> \mf ~
	g2. ~ g8 gf (
	f4 -^ ) r4 r2 } \\ { s1 s2 s4. \< s8 \f s1 } >>
	R1 
	
	\break
	e'1 \mf 
	d1
	<< { \override Voice.Rest #'staff-position = #0
	bf1 ~
	bf2. r4 } \\ { s1 s2 s4 \< s4 \! } >>
	
	% verse IV
	\break
	r4 r8 bf -> r bf -> r a (
	g4 -. ) r4 r2
	r4 r8 bf -> r bf -> r a (
	g4 -. ) r4 r2
	
	\break
	r4 r8 bf -> r bf -> r a (
	g4 -. ) r4 r2
	r4 r8 bf -> r bf -> r a (
	bf4 -. ) r4 r2
	
	\break
	r4 r8 c -> r c -> r bf (
	a4 -. ) r4 r2
	r4 r8 c -> r c -> r b (
	c4 -. ) r4 r2
	
	\break
	r4 r8 bf -> r bf -> r a (
	g4 -. ) r4 r2
	r4 r8 bf -> r bf -> r a (
	bf4 -. ) r4 r2
	
	\break
	<< { \override Voice.Rest #'staff-position = #0
	d1 -> \mf ~
	d2. ~ d8 df (
	c4 -^ ) r4 r2 } \\ { s1 s2 s4. \< s8 \f s1 } >>
	R1 
	
	\break
	e1 \mf 
	d1
	bf2. \fp \< r4 \!
	c8 \f -. c -. bf -. a -. g8 -. f -. f16 ( ef ) e -- c -^
	
	\break
	R1*8
	
	\break
	\repeat volta 2 {
		bf'8 \mf ( -> a ) bf4 -^ r8 bf -> r bf -> ~ (
		bf8 a ) bf4 -^ r2
		<< { \override Voice.Rest #'staff-position = #0
		bf8 ( -> a ) bf4 -^ r8 bf -> r bf -> ~ 
		bf2 ~ bf4 r } \\ { s2 s4. s8 \fp \< s2 s4 \mf s4 } >>
		
		\break
		bf8 ( -> a ) bf4 -^ r8 bf -> r bf -> ~ (
		bf8 a ) bf4 -^ r2
	}
	
	\alternative {
		{ << { \override Voice.Rest #'staff-position = #0
		  	bf8 ( -> a ) bf4 -^ r8 bf -> r bf -> ~ 
		  	bf2 ~ bf4 r } \\ { s2 s4. s8 \fp \< s2 s4 \mf s4 } >> }
		
		{ \break
		  << { \override Voice.Rest #'staff-position = #0
		  bf8 ( -> a ) bf4 -^ r8 bf -> r bf -> ~ 
		  bf2 ~ bf4 r } \\ { s2 s4. s8 \fp \< s2 s4 \mf s4 } >> }
	}
	
	bf8 \mf ( -> a ) bf4 -^ r2
	
	\bar "|."
}



% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

\book {
	\paper { 
		#(set-paper-size "a4" 'landscape)
		indent = 20
		systems-per-page = 2
	}
	
	\score {
		% \midi { \tempo 4 = 110 }
		\layout { #(layout-set-staff-size 20) }
		
		<<
			\scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\new Staff \with { instrumentName = "Trumpet" midiInstrument = #"trumpet" } { \trumpet }
				\new Staff \with { instrumentName = "Alto Sax" midiInstrument = #"alto sax" } { \altoSax }
				\new Staff \with { instrumentName = "Tenor Sax" midiInstrument = #"tenor sax" } { \tenorSax }
				\new Staff \with { instrumentName = "Trombone" midiInstrument = #"trombone" } << \tromboneSoloChords \trombone >>
			>>
		>>
	}
}

\book {
	\header { instrument = "Trumpet" }
   \bookOutputName "Mustang Sally - Trumpet"
	\score {
		<<
			\scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\new Staff { \transpose bf c' \trumpet }
			>>
		>>
	}
}

\book {
	\header { instrument = "Alto Sax" }
   \bookOutputName "Mustang Sally - Alto Sax"
	\score {
		<<
			\scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\new Staff { \transpose ef c' \altoSax }
			>>
		>>
	}
}

\book {
	\header { instrument = "Tenor Sax" }
   \bookOutputName "Mustang Sally - Tenor Sax"
	\score {
		<<
			\scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\new Staff { \transpose bf c'' \tenorSax }
			>>
		>>
	}
}

\book {
	\header { instrument = "Trombone" }
   \bookOutputName "Mustang Sally - Trombone"
	\score {
		<<
			\scoreMarkup
			\scoreChords
			\tromboneSoloChords
			\new StaffGroup <<
				\new Staff { \trombone }
			>>
		>>
	}
}