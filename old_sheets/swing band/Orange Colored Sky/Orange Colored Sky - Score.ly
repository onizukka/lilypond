\version "2.18.2"
\language "english"

\include "../.includes/drums.ily"
\include "../.includes/chords.ily"
\include "../.includes/lilyjazz.ily"
\include "../.includes/plugins-1.2.ily"


% Descripción de funcionalidades
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% PLUGINS
% - \eolMark:					mark al final de línea
% - \bendBefore: 				solución al bendBefore (i.e. dis8 \bendBefore)
% - \rit: 						crea una porción de ritardando que debe terminar como las dinámicas, con un \!
% - \improOn, \improOff:	negra de improvisación
% - \emptyStaff:				Imprime un pentagrama vacío
% - \emptyBar:					Imprime un compás invisible (útil para indentados)



% LAYOUT
% - El aspecto de los silencios multicompás es como los de las partituras de jazz
% - Margen extra de las articulaciones para evitar la colisión con las ligaduras
% - Glissandos bonitos



% PAPER
% - Se imprime el título, instrumento y número de página en todas las páginas
% - Solo se debe tocar el page-count (si eso). Sale bonito tal cual.

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Descripción de funcionalidades
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% 03-09-14: Layout
% Ahora las articulaciones van por dentro de las ligaduras. Parece que es la mejor solución



% 04-09-14: Layout
% Los creciendos y decreciendos tienen la línea un poco más gruesa
%
% Markup
% Añadidas guías para correcta posición y markup de la indicación de tempo
%
% Plugins
% Añadida función \emptyStave para imprimir un pentagrama vacío.
% En algunas partituras hay un pentagrama vacío antes del coda (lo cual mola bastante, por cierto)



% 05-09-14: Plugins
% Añadido a la función \emptyStave el esconder la armadura de la clave, que no estaba.



% 08-09-14: Layout
% Ahora no se muestra la cancelación de accidentales al haber un cambio de armadura



% 09-09-14: Chords
% Añadida sentencia para que los acordes menores salgan con un guión en vez de con una M minúscula



% 10-09-14: Plugins
% Mejorada la función \bendAfter.
% Ahora es genérica, y se puede definir cualquier texto en un lateral que haga las funciones de ornamento



% 15-09-14: Layout
% Añadido un poco más de margen a los reguladores



% 16-09-14: Plugins
% Mejorada la función para imprimir barras de improvisación.
% Antes se llamaba al comando \rs para pintar una barra que ocupaba una negra
% Se reemplaza \rs por \restImproOn y \restImproOff, siendo esta opción mucho más flexible.
% Cuando se activa \restImproOn, todo silencio se convierte en barra de improvisación, respetando su duración.



% 20-09-14: Plugins
% Mejorada la función para imprimir barras de improvisación.
% Se reemplaza \restImproOn y \restImproOff por \improOn y \improOff
% Ahora, también las notas del pentagrama salen como barra de improvisación.
% (los comandos \improvisationOn e \improvisationOff de lilypond no se llevan bien con LilyJAZZ)
% Se ha añadido un grob correspondiente a los slash de la fuente LilyJAZZ en el archivo lily-jazz.ly



% 20-09-14: Layout
% Comentada la línea que oculta la cancelación de accidentales al haber cambio de armadura.
% Realmente, la cancelación es muy bienvenida.



% 01-10-14: Plugins
% Renombrada la función \emptyStave a \emptyStaff (stave es el plural de staff) y 
% corregida (ya no muestra el número de compás).
% Recordar que después de usar \emptyStaff se necesita redefinir el 
% número de compás con \set Score.currentBarNumber = #xx
%
% Añadida la función \emptyBar que oculta un compás entero. Se puede usar para crear un 
% efecto de indentación en un pentagrama dado.
% Recordar que después de usar \emptyBar se necesita:
% - redefinir el número de compás con \set Score.currentBarNumber = #xx
% - volver a pintar la clave con \clef
% - volver a pintar la armadura con \key



% 15-10-14: Language
% Cambiado el lenguaje de entrada de notas del finlandes al inglés
% La diferencia es que ahora los sostenidos se escriben "-s" en vez de "-is"
% y los bemoles "-f" en vez de "-es".
%
% Estructura
% Para poder autoincluir los archivos de instrumento en otros 
% (para montar un midi, o un score), se han movido los plugins a un archivo separado
% y ahora se utiliza la función \includeOnce, evitando así el problema de las
% dependencias circulares.
%
% Global
% Todas las instrucciones que se colocaban justo antes de empezar la música están ahora
% recogidas en una función \global.
% A \global se le ha añadido instrucciones para que agrupe las corcheas de dos en dos
% en vez de cuatro en cuatro (el comportamiento por defecto).
%
% Plugins
% Se le ha aumentado el grosor de los bordes de la función \boxed



% 26-10-14: Chords
% Añadido el include "chord-exception.ily" que recoge una mejor notación de los acordes.
% Actualmente, recoge los acordes maj7, 7(b5), 7(#5), 7(b9), 7(#9), 7(add13), maj9(add13), 9(#11)
% Se irá añadiendo acordes a menudo.
 

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~











% Disposición
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

title = "Orange Colored Sky"

% Cabecera
\header {
	title = #title
	subtitle = "(as performed by Natalie Cole)"
	instrument = ""
	composer = "Willie Stein - Milton DeLugg"
	arranger = "Arr: Gabriel Palacios"
	poet = ""
	tagline = "gabriel.ps.ms@gmail.com"
}

% Márgenes, fuentes y distancias del papel
\paper {

	% distancia entre la parte superior de la página y el título
	top-markup-spacing #'basic-distance = 5

	% distancia entre el título y el primer sistema
	markup-system-spacing #'basic-distance = 25

	% distancia entre el texto y el primer sistema 
	top-system-spacing #'basic-distance = 15

	% márgen inferior
	bottom-margin = 20
	
	% intentado del primer sistema
	indent = 0
	 
	% número de páginas
	% page-count = 1

	ragged-last-bottom = ##f 

	oddHeaderMarkup = \markup {
	 \on-the-fly #not-first-page \fill-line {
		\fromproperty #'page:page-number-string
		\fromproperty #'header:title
		\fromproperty #'header:instrument
	 }
	}

	evenHeaderMarkup = \markup {
	 \on-the-fly #not-first-page \fill-line {
		\fromproperty #'header:instrument
		\fromproperty #'header:title
		\fromproperty #'page:page-number-string
	 }
	}
}

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~







% Elementos comunes de la partitura
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
	% Oculta el primer pentagrama del markup
	\override Staff.VerticalAxisGroup #'remove-first = ##t

	% Indicación de tiempo
	\once \override Score.RehearsalMark.X-offset = #5
	\once \override Score.RehearsalMark.extra-offset = #'(0 . 3) 
	\mark "Medium Swing"
	s1*4
	
	\tag #'sb \break
	s1*4
	
	\tag #'sb \break
	\mark \markup {
		\column { \boxed "A" }
		\column { "(x2)" }
	}
	s1*4
	
	\tag #'sb \break
	s1*6
	
	\tag #'sb \break
	\mark \markup \boxed "B"
	s1*4
	
	\tag #'sb \break
	s1*4
	
	\tag #'sb \break
	\mark \markup \boxed "C"
	s1*4
	
	\tag #'sb \break
	s1*4
	
	\tag #'sb \break
	\mark \markup \boxed "D"
	s1*4
	
	\tag #'sb \break
	s1*4
	
	\tag #'sb \break
	\once \override Score.RehearsalMark.X-offset = #5
	\mark \markup {
		\column { \boxed "E" }
		\column { "(Vocal)" }
	}
	s1*4
	
	\tag #'sb \break
	s1*4
	
	\tag #'sb \break
	\mark \markup \boxed "F"
	s1*6
	
	\tag #'sb \break
	s1*4
	
	\tag #'sb \break
	\mark \markup \boxed "G"
	s1*4
	
	\tag #'sb \break
	s1*4
}

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Elementos específicos del instrumento
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Cifrado
scoreChords = \chords {    

	% c1:m f2:m g2:m7
	g2 e2:7/gs
	a2:m7 fs2:7/as
	b2:m7 c2
	a1:7/cs 
	
	b2:m7 e2:m7
	a2:m7 b2:m7/d
	b4:m7 e4/bf a4:m7 af4:9.5-
	g4:6 c4:7.5- cs:7.5+ d:13
	
	g2 e2:7/gs
	a2:m7 fs2:7/as
	g1:6.9/b
	b2:m7 e2:7
	
	s4 a:m9/c s4 df:dim
	g2/d e2:11^9
	a2:m7 d:7
	g4:6 e4/gs a:m7 d:7
	
	a2:m7 d:7
	g4:6 af:7.5- g2:6
	
	c2 c:5+
	fs1:9
	b2:sus4.7 b2:7
	e4:m7 b:m7 e4:m7 b:m7
	
	e2:m e2:m/d
	a2:7/cs c4 g/b
	d2:7/a a:m7
	\times 2/3 { s4 f:13.11+ ef:13 } d2:7.5+
	
	g2 e2:7/gs
	a2:m7 fs2:7/as
	g1:6.9/b
	b2:m7 e2:7
	
	s4 a:m9/c s4 af:9
	g2 e2:11^9
	a2:m7 d:7
	g1:6
	
	g2 e2:7/gs
	a2:m7 fs2:7/as
	b1:m7
	ds8:7 e:7 s2.
	
	a2:m7 as:dim
	b2:m g4 c:9^7
	a4:m7 g/d e:m/d c/d
	fs8 g s4 g2:13
	
	c1:6
	fs1:7
	b:7
	e:m7
	
	e2:m c2/e
	a2/e c4 e4:m/b
	d2:7/a a:m7
	\times 2/3 { s4 f:13.11+ ef:13 } d2:7.5+
	
	g2 e2:7/gs
	a2:m7 fs2:7/as
	g1:6.9/b
	b2:m7 e2:7
	
	s4 g4/c s4 ef4:maj7.3-
	g2:6 ef:9+
	
	c1:maj7
	af1:maj7
	g2:6 f:7
	e1:7
	
	a1:m7
	a1:m9
	s1*2
	
	g2:6 f:13
	e4:7.9+.5+ ef2:9 af4:maj7
	a8:m7 g/bf s c s g:m9/d s4
	s4 s8 g:maj7 s2
	
}

% Música
trumpet = \relative c'' {
  
	\global
	\key g \major

	d2 \mf ^ \markup \box "Harmon. Mute" e
	e2 fs
	fs g4 fs8 g
	a8 a a a af g r4
	
	\tag #'b \break
	r8 fs4. -> g2
	e2 fs
	\parenthesize d4 -- e -> \parenthesize c -- d ->
	\parenthesize b4 -- c -> \parenthesize a -- b ->
	
	\tag #'b \break
	\repeat volta 2 {
		
		R1*4
		
		r4 ^ \markup \box "Open" \f a' -> r bf ->
		r2 r8 b?4. ->
	}
	\alternative {
		{ \tag #'b \break 
		  R1*2 }
		
		{ R1
		  r4 \p d,,8 g -> ~ g4 r }
	}
	
	\tag #'b \break
	R1*6
	r8 \f fs' -> r4 r8 g -> r4
	\times 2/3 { r4 b -^ c -^ } d8 -> r r4
	
	\tag #'b \break
	R1*4
		
	r4 \f a -> r bf ->
	r2 r8 b?4. ->
	b4 -^ r4 r2
	R1
	
	\tag #'b \break
	r8 \f ^ "(Shout)" cs, d e ~ e4 r
	r4 \times 2/3 { e8 fs g } fs e -^ r4
	r4 fs8 fs ( g ) g ( a ) a (
	as ) b -> ~ b4 r2
	
	\tag #'b \break
	r8 e, ( a ) -.g \times 2/3 { fs ( g a } \times 2/3 { af g fs ) }
	\times 2/3 { e ( fs g } \times 2/3 { fs g a } \times 2/3 { g a b } c4 ) -^
	r4 \times 2/3 { cs8 ( d ) d } \times 2/3 { as ( b ) b } \times 2/3 { fs ( g ) g }
	cs,8 \> d \! r4 r2
	
	\tag #'b \break
	R1*6
	r8 \f fs -> r4 r8 g -> r4
	\times 2/3 { r4 b -^ c -^ } d8 -> r r4
	 
	\tag #'b \break
	R1*4
	r8 \f b -- b4 -^ r8 bf \bendBefore -- r4
	r2 r8 b? -> ~ b4
	
	\tag #'b \break
	r2 r4 b -^
	r2 r8 g -> \bendAfter #-5 r4
	R1*2
	
	\tag #'b \break
	r4 \f g -^ r a -^
	r2 r8 b -> ~ b4 --
	c,8 \sp \< d r e r g r b 
	r \f d ~ d2 r4
	
	\tag #'b \break
	r4 g,2 -> ~ g8 g -> ~
	g2 ~ g8 g -> ~ g4 ~ 
	g8 g -> ~ g4 ~ g8 g -^ r4
	R1
	
	\bar "|."
}

altoSax = \relative c'' {
  
	\global
	\key g \major

	R1*4
	
	\tag #'b \break
	r8 a,4. -> d2
	g,2 b
	R1*2
	
	\tag #'b \break
	\repeat volta 2 {
		d1 \> ^ "(play 2nd x only)" \repeatTie
		R1*2 \!
		r8 \p b c d -> ~ d4 r4
		
		r4 \f ^ "(play both x's)" e' -> r df ->
		r2 r8 d?4. ->
	}
	\alternative {
		{ \tag #'b \break 
		  R1
		  r4 \mf b8 \bendBefore c \times 2/3 { b g ef } 
		  \shape #'((0 . 0) (1 . 0) (3 . 0) (4 . 0)) LaissezVibrerTie d4 \laissezVibrer }
		
		{ R1
		  r2 r4 \mf d }
	}
	
	\tag #'b \break
	c'1
	cs
	e2 ds4. \> e,8 -. \p
	r e ( d -. ) d ( d -. ) d ( d -. ) d 
	
	\tag #'b \break
	e2 \mp d
	cs c4 \< b
	a8 \mf c' r a, -> ~ a c' r4
	\times 2/3 { r4 \f g' -^ f -^ } fs8 -> r r4
	
	\tag #'b \break
	R1*4
	
	r4 \f e -> r ef ->
	r2 r8 g4. ->
	e4 -^ r4 r2
	R1
	
	\tag #'b \break
	r8 \f ^ "(Shout)" cs, d gs ~ gs4 r
	r4 \times 2/3 { e'8 fs g } fs as,4.
	\once \override Staff.BarLine.space-alist.next-note = #'(fixed-space . 5)
	b2 b8 -\glissBefore #'(20 1 1) b -- b b (
	ds8 ) e -> ~ e4 r2
	
	\tag #'b \break
	r8 e, ( a ) -.g \times 2/3 { fs ( g a } \times 2/3 { af g fs ) }
	\times 2/3 { e ( fs g } \times 2/3 { fs g a } \times 2/3 { g a b } c4 ) -^
	r4 \times 2/3 { fs8 ( g ) g } \times 2/3 { ds ( e ) e } \times 2/3 { b ( c ) c }
	as8 \> b \! r \mp g -> ~ g4 r
	
	\tag #'b \break
	R1*4
	
	b,2 \mp c
	cs c4 \< b
	a8 \mf c' r a, -> ~ a c' r4
	\times 2/3 { r4 \f g' -^ f -^ } fs8 -> r r4
	
	\tag #'b \break
	b,1 \mf
	c2 cs
	d1 ~
	d4 b8 \> e, \! r4 \p d -.
	r8 \f g' -- g4 -^ r8 gf -- \bendBefore r4
	r2 r8 g? -> ~ g4
	
	\tag #'b \break
	r2 r4 e -^
	r2 r8 c -> \bendAfter #-5 r4
	R1*2
	
	\tag #'b \break
	r4 \f e -^ r e -^
	r2 r8 e -> ~ e4 --
	c,8 \sp \< d r e r g r b 
	r \f d ~ d2 r4
	
	\tag #'b \break
	r4 e, -^ d4. -> c8 -> ~
	c4 bf -> ~ bf8 c -> ~ c4
	c8 d r e r f g4 -.
	r4 r8 b, -^ r2
	
	\bar "|."
}

tenorSax = \relative c'' {
  
	\global
	\key g \major

	R1*4
	
	\tag #'b \break
	r8 a,4. -> d2
	g,2 b
	R1*2
	
	\tag #'b \break
	\repeat volta 2 {
		d,1 \> ^ "(play 2nd x only)" \repeatTie
		R1*2 \!
		r8 \p b' a gs -> ~ gs4 r4
		
		r4 \f ^ \markup "(play both x's)" b' -> r g ->
		r2 r8 g4. ->
	}
	\alternative {
		{ \tag #'b \break 
		  R1
		  r4 \mf b,8 \bendBefore c \times 2/3 { b g ef } 
		  \shape #'((0 . 0) (1 . 0) (3 . 0) (4 . 0)) LaissezVibrerTie d4 \laissezVibrer }
		
		{ R1
		  r2 r4 \mf b' }
	}
	
	\tag #'b \break
	g'2 gs2 ~
	gs1
	a2 ~ a4. \> e8 -. \p
	r g, ( fs -. ) fs ( g -. ) g ( fs -. ) fs 
	
	\tag #'b \break
	e2 \mp d
	cs \< c4 b
	a8 \mf d' r a, -> ~ a e'' r4
	\times 2/3 { r4 \f d' -^ c -^ } b8 -> r r4
	
	\tag #'b \break
	r2 r8 \mf gs, -> r4
	R1
	fs4 -^ r8 b -> r4 r8 a ->
	r4 r8 gs -> ~ gs4 r
	
	\tag #'b \break
	r4 \f c' -> r c ->
	r2 r8 d4. ->
	c4 -^ r4 r2
	r8 e, \mf ( \times 2/3 { d ds e } \times 2/3 { d ds e } \times 2/3 { d \< ds e } 
	
	\tag #'b \break
	b2 \f ^ "(Shout)" ~ \times 2/3 { b8 ) d ds } \times 2/3 { fs ( f e ) }
	c2 r8 cs4.
	\once \override Staff.BarLine.space-alist.next-note = #'(fixed-space . 5)
	d2 g8 -\glissBefore #'(20 1 1) g ( fs ) fs (
	g8 ) gs -> ~ gs4 r2
	
	\tag #'b \break
	a,2 as4. b8
	r2 \times 2/3 { d8 e fs } g4 -^
	r4 \times 2/3 { as8 ( b ) b } \times 2/3 { fs ( g ) g } \times 2/3 { ds ( e ) e }
	cs8 \> d r \mp e -> ~ e4 r
	
	\tag #'b \break
	R1*4
	
	b,2 \mp c
	cs c4 \< b
	a8 \mf d' r a, -> ~ a e'' r4
	\times 2/3 { r4 \f d' -^ c -^ } b8 -> r r4
	
	\tag #'b \break
	g1 \mf
	a2 as
	a?1
	b4 a8 \> d, r4 \p b -.
	r8 \f d' -- d4 -^ r8 d -- \bendBefore r4
	r2 r8 d -> ~ d4
	
	\tag #'b \break
	r2 r4 g, -^
	r2 r8 ef -> \bendAfter #-5 r4
	R1
	r8 \f e, ( fs ) fs ( g ) g ( gs4 -^ )
	
	\tag #'b \break
	r4 b' -^ r b -^
	r2 r8 b -> ~ b4 --
	c,8 \sp \< d r e r g r b 
	r \f d, ~ d2 r4
	
	\tag #'b \break
	r4 b -^ a4. -> af8 -> ~
	af4 f -> ~ f8 g -> ~ g4
	g8 bf r c r bf bf4 -.
	r4 r8 fs -^ r2
	
	\bar "|."
}

trombone = \relative c'' {
  
	\global
	\clef bass
	\key g \major
	
	R1*8
	
	\tag #'b \break
	\repeat volta 2 {
		R1*4
		
		r4 \f c, -> r bf ->
		r2 r8 a4. ->
	}
	\alternative {
		{ \tag #'b \break 
		  R1*2 }
		
		{ R1
		  r4 \p c8 b -> ~ b4 r }
	}
	
	\tag #'b \break
	R1*6
	r8 \f fs' -> r4 r8 g -> r4
	\times 2/3 { r4 ef -^ df -^ } c8 -> r r4
	
	\tag #'b \break
	r2 r8 \mf e -> r4
	R1
	b4 -^ r8 d -> r4 r8 d ->
	r4 r8 d -> ~ d4 r
	
	\tag #'b \break
	r4 \f g -> r g ->
	r2 r8 gs4. ->
	g?4 -^ r4 r2
	R1
	
	\tag #'b \break
	r8 \f ^ "(Shout)" cs, d e ~ e4 r
	r4 \times 2/3 { e8 fs g } fs e -^ r4
	r4 fs8 fs ( d ) d -- d d (
	cs ) d -> ~ d4 r2
	
	\tag #'b \break
	c2 cs4. d8
	r2 r4 d -^
	r4 d,2. \sfz
	cs8 \> d r \mp f -> ~ f4 r
	
	\tag #'b \break
	R1*6
	r8 \f fs' -> r4 r8 g -> r4
	\times 2/3 { r4 ef -^ df -^ } c8 -> r r4
	
	\tag #'b \break
	d2 \mf e ~
	e2 fs
	g1 \bendBefore
	a4 e8 \> gs, r4 \p fs -.
	r8 \f c' -- c4 -^ r8 ef -- \bendBefore r4
	r2 r8 gs -> ~ gs4
	
	\tag #'b \break
	r2 r4 c, -^
	r2 r8 af -> \bendAfter #-5 r4
	R1
	r8 \f e, ( fs ) fs ( g ) g ( gs4 -^ )
	
	\tag #'b \break
	r4 c' -^ r c -^
	r2 r8 c -> ~ c4 --
	c,8 \sp \< d r e r g r b 
	r \f d ~ d2 r4
	
	\tag #'b \break
	r4 g, -^ ef4. -> d8 -> ~
	d4 df -> ~ df8 ef -> ~ ef4
	e?8 g r g r a a4 -.
	r4 r8 d, -^ r2
	
	\bar "|."
}

piano = \relative c' {
  
	\global
	\key g \major
	
	d'2 \mf ^ \markup \box "Harmon. Mute" e
	e2 fs
	fs g4 fs8 g
	a8 a a a af g r4
	
	\tag #'b \break
	r8 fs4. -> g2
	e2 fs
	\parenthesize d4 -- e -> \parenthesize c -- d ->
	\parenthesize b4 -- c -> \parenthesize a -- b ->
	
	\improOn
	\tag #'b \break
	\repeat volta 2 {
		r4 \p r4 r4 r4
		\repeat unfold 2 { r4 r4 r4 r4 }
		\tag #'b \break
		\repeat unfold 1 { r4 r4 r4 r4 }
		
		\of r4 \f c ->  \of r4  c ->
		r4 r4 \of r8 c -> ~ c4
	}
	\alternative {
		{ \tag #'b \break
		  r4 \p r4 r4 r4
		  r4 r4 r4 r4 }
		{ r4 \p r4 r4 r4
		  r4 c8 c -> ~ c4 r4 }
	}
	
	\tag #'b \break
	r4 \mf r4 r4 r4
	\repeat unfold 2 { r4 r4 r4 r4 }
	r4 \p r4 r4 r4
	
	\tag #'b \break
	r4 r4 r4 r4
	r4 \< r4 r4 r4
	\of r8 \f d -> \of r4 \of r8 d -> \of r4
	\times 2/3 { \of r4 c4 -^ c4 -^} c4 -^ \> r4
	
	\tag #'b \break
	r4 \p r4 r4 r4 
	\repeat unfold 3 { r4 r4 r4 r4 }
	
	\tag #'b \break
	\of r4 \f c ->  \of r4 c4
	r4 r4 \f r8 c -> ~ c4
	c4 -^ \mf r4 r4 r4
	\improOff r8 e \mf ( \times 2/3 { d ds e } \times 2/3 { d ds e } \times 2/3 { d \< ds e } 
	
	\tag #'b \break
	b2 \f ^ "(Shout)" ~ \times 2/3 { b8 ) d ds } \times 2/3 { fs ( f e ) }
	c2 \improOn r4 r4
	r4 \improOff fs8 fs ( g ) a ( a ) a 
	as8 b8 -> ~ b4 \improOn r4 r4
	
	\tag #'b \break
	\improOff r8 e, ( a ) -.g \times 2/3 { fs ( g a } \times 2/3 { af g fs ) }
	\times 2/3 { e ( fs g } \times 2/3 { fs g a } \times 2/3 { g a b } c4 ) -^
	r4 \times 2/3 { cs8 ( d ) d } \times 2/3 { as ( b ) b } \times 2/3 { fs ( g ) g }
	cs,8 \> d \! r g, ~ g4 r
	
	\improOn
	\tag #'b \break
	\repeat unfold 4 { r4 r4 r4 r4 }
	
	\tag #'b \break
	r4 r4 r4 r4
	r4 \< r4 r4 r4
	\of r8 \f d -> \of r4 \of r8 d -> \of r4
	\times 2/3 { \of r4 c4 -^ c4 -^ } c4 -^ \> r4
	
	\tag #'b \break
	r4 \mf r4 r4 r4
	\repeat unfold 3 { r4 r4 r4 r4 }
	\of r8 c -- \f c4 -^ \of r8 c -- r4
	r4 r4 \of r8 c -> ~ c4
	
	\tag #'b \break
	r4 r4 r4 c -^
	r4 r4 \of r8 c -> r4
	\repeat unfold 2 { r4 r4 r4 r4 }
	
	\tag #'b \break
	\of r4 d -^ \of r4 d -^ 
	r4 r4 \of r8 c -> ~ c4
	\improOff c8 \sp \< d r e r g r b 
	r \f <d, d'> ~ d2 r4
	
	\improOn
	\tag #'b \break
	r4 \f c -^ c4. -> c8 -> ~
	c4 c -> ~ c8 c -> ~ c4
	c8 c \of r c \of r c c4 -.
	\of r4 \of r8 c -^ \of r2
	
	\bar "|."
}

guitar = \relative c' {
  
	\global
	\key g \major
	
	\improOn
	r4 \mf r4 r4 r4
	r4 r4 r4 r4
	r4 r4 r4 r4
	r4 r4 c8 c \of r4
	
	\tag #'b \break
	\of r8 c8 ~ c4 r4 r4
	r4 r4 r4 r4
	r4 r4 r4 r4
  	r4 r4 r4 \> r4
	
	\tag #'b \break
	\repeat volta 2 {
		r4 \p r4 r4 r4
		\repeat unfold 2 { r4 r4 r4 r4 }
		\tag #'b \break
		\repeat unfold 1 { r4 r4 r4 r4 }
		
		\of r4 \f c ->  \of r4  r4
		r4 r4 r8 c ~ c4
	}
	\alternative {
		{ \tag #'b \break
		  r4 \p r4 r4 r4
		  r4 r4 r4 r4 }
		{ r4 \p r4 r4 r4
		  r4 c8 c -> ~ c4 r4 }
	}
	
	\tag #'b \break
	r4 \mf r4 r4 r4
	\repeat unfold 2 { r4 r4 r4 r4 }
	r4 \p r4 r4 r4
	
	\tag #'b \break
	r4 r4 r4 r4
	r4 \< r4 r4 r4
	r4 \f r4 r4 r4
	\times 2/3 { \of r4 c4 -^ c4 -^} c4 -^ \> r4
	
	\tag #'b \break
	r4 \p r4 r4 r4 
	\repeat unfold 3 { r4 r4 r4 r4 }
	
	\tag #'b \break
	\of r4 \f c ->  \of r4  r4
	r4 r4 c8 c ~ c4
	r4 \mf r4 r4 r4
	r4 \< r4 r4 r4
	
	\tag #'b \break
	r4 \f r4 r4 r4
	\repeat unfold 2 { r4 r4 r4 r4 }
	c8 c8 -> \laissezVibrer r4 r4 r4
	
	\tag #'b \break
	c4. c8 c4. c8
	\of r2 r4 r4
	r4 r4 r4 r4
	c8 \> c8 \of r \p c -> ~ c4 r4
	
	\tag #'b \break
	\repeat unfold 4 { r4 r4 r4 r4 }
	
	\tag #'b \break
	r4 r4 r4 r4
	r4 \< r4 r4 r4
	r4 \f r4 r4 r4
	\times 2/3 { \of r4 c4 -^ c4 -^ } c4 -^ \> r4
	
	\tag #'b \break
	r4 \mf r4 r4 r4
	\repeat unfold 3 { r4 r4 r4 r4 }
	\of r4 \f c ->  \of r4  r4
	r4 r4 c8 c ~ c4
	
	\tag #'b \break
	\repeat unfold 4 { r4 r4 r4 r4 }
	
	\tag #'b \break
	\repeat unfold 2 { r4 r4 r4 r4 }
	\improOff c8 \sp \< d r e r g r b 
	r \f d ~ d2 r4
	
	\improOn
	\tag #'b \break
	r4 \f c -^ c4. -> c8 -> ~
	c4 c -> ~ c8 c -> ~ c4
	c8 c \of r c \of r c c4 -.
	\of r4 \of r8 c -^ \of r2
	
	\bar "|."
}

bass = \relative c' {
  
	\global
	\clef bass
	\key g \major
	
	\improOn
	r4 \mf r4 r4 r4
	r4 r4 r4 r4
	r4 r4 r4 r4
	r4 r4 c8 c \of r4
	
	\tag #'b \break
	\of r8 c8 ~ c4 r4 r4
	r4 r4 r4 r4
	r4 r4 r4 r4
  	r4 r4 r4 \> r4
	
	\tag #'b \break
	\repeat volta 2 {
		r4 \p r4 r4 r4
		\repeat unfold 2 { r4 r4 r4 r4 }
		\tag #'b \break
		\repeat unfold 1 { r4 r4 r4 r4 }
		
		\of r4 \f c ->  \of r4  r4
		r4 r4 c8 c ~ c4
	}
	\alternative {
		{ \tag #'b \break
		  r4 \p r4 r4 r4
		  r4 r4 r4 r4 }
		{ r4 \p r4 r4 r4
		  r4 c8 c -> ~ c4 r4 }
	}
	
	\tag #'b \break
	r4 \mf r4 r4 r4
	\repeat unfold 2 { r4 r4 r4 r4 }
	r4 \p r4 r4 r4
	
	\tag #'b \break
	r4 r4 r4 r4
	r4 \< r4 r4 r4
	r4 \f r4 r4 r4
	\times 2/3 { \of r4 c4 -^ c4 -^} c4 -^ \> r4
	
	\tag #'b \break
	r4 \p r4 r4 r4 
	\repeat unfold 3 { r4 r4 r4 r4 }
	
	\tag #'b \break
	\of r4 \f c ->  \of r4  r4
	r4 r4 c8 c ~ c4
	r4 \mf r4 r4 r4
	r4 \< r4 r4 r4
	
	\tag #'b \break
	r4 \f r4 r4 r4
	\repeat unfold 2 { r4 r4 r4 r4 }
	c8 c8 -> \laissezVibrer r4 r4 r4
	
	\tag #'b \break
	c4. c8 c4. c8
	\of r2 r4 r4
	r4 r4 r4 r4
	c8 \> c8 \of r \p c -> ~ c4 r4
	
	\tag #'b \break
	\repeat unfold 4 { r4 r4 r4 r4 }
	
	\tag #'b \break
	r4 r4 r4 r4
	r4 \< r4 r4 r4
	r4 \f r4 r4 r4
	\times 2/3 { \of r4 c4 -^ c4 -^ } c4 -^ \> r4
	
	\tag #'b \break
	r4 \mf r4 r4 r4
	\repeat unfold 3 { r4 r4 r4 r4 }
	\of r4 \f c ->  \of r4  r4
	r4 r4 c8 c ~ c4
	
	\tag #'b \break
	\repeat unfold 4 { r4 r4 r4 r4 }
	
	\tag #'b \break
	\repeat unfold 2 { r4 r4 r4 r4 }
	\improOff c,8 \sp \< d r e r g r b 
	r \f d, ~ d2 r4
	
	\tag #'b \break
	r4 \f g -^ f4. -> e8 -> ~
	e4 ef -> ~ ef8 af, -> ~ af4
	a?8 bf r c r d d4 -.
	r4 r8 g, -^ r2
	
	\bar "|."
}

drumsUp = \drummode {
  	
	\revert RestCollision.positioning-done
	\override Rest.staff-position = #6
	
	\dynamicUp
	\teeny
	
	s1*3
	r2 dd8 dd r4
	r8 dd ~ dd4 r2
	s1*3
	
	s1*4
	s2. dd4 ->
	r2 r8 dd4. ->
	s1*3
	r4 dd8 dd -> ~ dd4 r4
	
	s1*3
	r8 dd ( dd -. ) dd ( dd -. ) dd ( dd4 -. )
	s1*2
	r8 dd -> r4 r8 dd -> r4 
	\times 2/3 { r4 dd -^ dd -^ } dd4 -^ r4
	
	s1*4
	s2. dd4 ->
	r2 r8 dd4. ->
	dd4 -^ r4 r2
	r8 dd \times 2/3 { dd dd dd } \times 2/3 { dd dd dd } \times 2/3 { dd dd dd }
	
	s1*1
	r2 r8 dd -^ r4
	dd2 dd8 dd dd dd
	dd dd -> ~ dd4 r2
	
	s1*2
	dd4 -- \times 2/3 { dd8 dd dd } \times 2/3 { dd dd dd } \times 2/3 { dd dd dd }
	dd8 dd r dd ~ dd4 r
	
	s1*6
	r8 dd -> r4 r8 dd -> r4 
	\times 2/3 { r4 dd -^ dd -^ } dd4 -^ r4
	
	s1*4
	r8 dd dd4 -^ r8 dd -- r4
	r2 r8 dd -> ~ dd4
	
	r2 r4 dd -^
	r2 r8 dd -> r4
	s1*2
	
	r4 dd -^ r dd -^
	r2 r8 dd -> ~ dd4
	s1*2
	
	r4 dd -^ dd4. -> dd8 -> ~
	dd4 dd -> ~ dd8 dd -> ~ dd4
	dd8 dd r dd r dd dd4 -.
	r4 r8 dd -^ s2
	
	\bar "|."
}

drumsDown = \drummode {

	\global
	
	\improOn
	\set countPercentRepeats = ##t
  	\set repeatCountVisibility = #(every-nth-repeat-count-visible 4)
	\stemDown
	\override MultiMeasureRest.staff-position = 2
	\override Stem.extra-offset = #'(.05 . 0)
	\override Beam.extra-offset = #'(.05 . 0)
  	\override PercentRepeatCounter.direction = #DOWN
  	
  	\repeat percent 8 { r4 r4 r4 r4 }
	
	\tag #'b \break
	\repeat volta 2 {
		\repeat percent 4 { r4 r4 r4 r4 }
		\of r4 sn4 -> \of r4 r4
		r4 r4 r4 r4
	}
	\alternative {
		{ \tag #'b \break
		  r4 r4 r4 r4
		  r4 r4 r4 r4 }
		{ r4 r4 r4 r4
		  r4 r4 r4 r4 }
	}
	
	\tag #'b \break
	\repeat percent 7 { r4 r4 r4 r4 }
	\times 2/3 { \of r4 sn4 -^ sn4 -^ } r4 r4
	
	\tag #'b \break
	\repeat percent 4 { r4 r4 r4 r4 }
	\of r4 sn4 -> \of r4 r4
	\repeat percent 3 { r4 r4 r4 r4 }
	
	\tag #'b \break
	\repeat percent 7 { r4 r4 r4 r4 }
	r4 r4 \fill { r4 r4 }
	
	\tag #'b \break
	\repeat percent 7 { r4 r4 r4 r4 }
	\times 2/3 { \of r4 sn4 -^ sn4 -^ } r4 r4
	
	\tag #'b \break
	\repeat percent 6 { r4 r4 r4 r4 }
	
	\tag #'b \break
	\repeat percent 6 { r4 r4 r4 r4 }
	sn8 sn \of r sn \of r sn \of r sn 
	\of r sn ~ sn4 \fill { r4 r4 }
	
	\tag #'b \break
	\repeat percent 3 { r4 r4 r4 r4 }
	\fill { r4 r4 } \of r2
	
	\bar "|."
}

drumsRepeats = \new DrumVoice \with { \RemoveEmptyStaves } {
	% Oculta el primer pentagrama del markup
	\override Staff.VerticalAxisGroup #'remove-first = ##t
	
	\improOn
	s1*7 \mf 
  	s2 s4 \> s4
	
	s1*4 \p
	s1*2 \f
	s1*2 \p
	s1*2 \p
	
	s1*3 \mf
	s1*2 \sp
	s1*1 \<
	s1*1 \f 
	s2 s2 \>
	
	s1*4 \p
	s1*2 \f
	s1*1 \mf 
	s1*1 \<
	
	s1*7 \f
	s4 \> s4 \p s2
	
	s1*5
	s1*1 \<
	s1*2 \f
	
	s1*4 \mf
	s1*2 \f
	
	s1*6
	s1 \sp \<
	s1 \f
	
	s1*4 \f
}
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~


\book {
	\paper { 
		#(set-paper-size "a4" 'landscape)
		indent = 20
		systems-per-page = 1
	}
	
	\score {
		% \midi { \tempo 4 = 60 }
		\layout {} 
		\removeWithTag #'b
		<<
			\scoreMarkup
			\new StaffGroup <<
				\new Staff \with { instrumentName = "Trumpet" midiInstrument = #"trumpet" } { \trumpet }
				\new Staff \with { instrumentName = "Alto Sax" midiInstrument = #"alto sax" } { \altoSax }
				\new Staff \with { instrumentName = "Tenor Sax" midiInstrument = #"tenor sax" } { \tenorSax } 
				\new Staff \with { instrumentName = "Trombone" midiInstrument = #"trombone" } { \trombone }
			>>
			
			\scoreChords
			\new StaffGroup <<
				\new Staff \with { instrumentName = "Piano" } { \piano }
				\new Staff \with { instrumentName = "Guitar" } { \guitar }
				\new Staff \with { instrumentName = "Bass" } { \bass }
			>>
			
			\new DrumStaff <<
  				\set DrumStaff.drumStyleTable = #(alist->hash-table nwDrums)
				\new DrumVoice { \voiceOne \drumsUp }
				\new DrumVoice { \voiceTwo \drumsDown }
  				\drumsRepeats
			>>
		>>
	}
}

instrumentName = "Trumpet"
\book {
	\header { instrument = #instrumentName }
   \bookOutputName  #(string-append title " - " instrumentName)
   
	\score {
		\transpose bf c' 
		<<
			\removeWithTag #'sb \scoreMarkup
			\new StaffGroup <<
				\new Staff { \trumpet }
			>>
		>>
	}
}

instrumentName = "Alto Sax"
\book {
	\header { instrument = #instrumentName }
   \bookOutputName  #(string-append title " - " instrumentName)
   
	\score {
		\transpose ef c'
		<<
			\removeWithTag #'sb \scoreMarkup
			\new StaffGroup <<
				\new Staff { \altoSax }
			>>
		>>
	}
}

instrumentName = "Tenor Sax"
\book {
	\header { instrument = #instrumentName }
   \bookOutputName  #(string-append title " - " instrumentName)
   
	\score {
		\transpose bf c''
		<<
			\removeWithTag #'sb \scoreMarkup
			\new StaffGroup <<
				\new Staff { \tenorSax }
			>>
		>>
	}
}

instrumentName = "Trombone"
\book {
	\header { instrument = #instrumentName }
   \bookOutputName  #(string-append title " - " instrumentName)
   
	\score {
		<<
			\removeWithTag #'sb \scoreMarkup
			\new StaffGroup <<
				\new Staff { \trombone }
			>>
		>>
	}
}

instrumentName = "Piano"
\book {
	\header { instrument = #instrumentName }
   \bookOutputName  #(string-append title " - " instrumentName)
   
	\score {
		<<
			\removeWithTag #'sb \scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\new Staff { \piano }
			>>
		>>
	}
}

instrumentName = "Guitar"
\book {
	\header { instrument = #instrumentName }
   \bookOutputName  #(string-append title " - " instrumentName)
   
	\score {
		<<
			\removeWithTag #'sb \scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\new Staff { \guitar }
			>>
		>>
	}
}

instrumentName = "Bass"
\book {
	\header { instrument = #instrumentName }
   \bookOutputName  #(string-append title " - " instrumentName)
   
	\score {
		<<
			\removeWithTag #'sb \scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\new Staff { \bass }
			>>
		>>
	}
}

instrumentName = "Drums"
\book {
	\header { instrument = #instrumentName }
   \bookOutputName  #(string-append title " - " instrumentName)
   
	\score {
		<<
			\removeWithTag #'sb \scoreMarkup
			\new DrumStaff <<
  				\set DrumStaff.drumStyleTable = #(alist->hash-table nwDrums)
				\new DrumVoice { \voiceOne \drumsUp }
				\new DrumVoice { \voiceTwo \drumsDown }
  				\drumsRepeats
			>>
		>>
	}
}