\version "2.18.2"
\language "english"

\include "../.includes/drums.ily"
\include "../.includes/chords.ily"
\include "../.includes/lilyjazz.ily"
\include "../.includes/plugins-1.2.ily"


% Descripción de funcionalidades
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% PLUGINS
% - \eolMark:					mark al final de línea
% - \bendBefore: 				solución al bendBefore (i.e. dis8 \bendBefore)
% - \rit: 						crea una porción de ritardando que debe terminar como las dinámicas, con un \!
% - \improOn, \improOff:	negra de improvisación
% - \emptyStaff:				Imprime un pentagrama vacío
% - \emptyBar:					Imprime un compás invisible (útil para indentados)
% - \glissBefore:				muestra un glissando hacia la nota
% - \shortGlissBefore:		muestra un glissando corto hacia la nota
% - \jazzTurn:					muestra el simbolo equivalente a un turn en jazz
% - \subp:						muestra "sp", subito piano
% - \ap:							(accidental padding), separa un poco una alteración de la siguiente nota
% 									(útil al usar parenthesize)
% - \optOttava:				octavaje opcional

% LAYOUT
% - El aspecto de los silencios multicompás es como los de las partituras de jazz
% - Margen extra de las articulaciones para evitar la colisión con las ligaduras
% - Glissandos bonitos

% PAPER
% - Se imprime el título, instrumento y número de página en todas las páginas
% - Solo se debe tocar el page-count (si eso). Sale bonito tal cual.

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Changelog de funcionalidades
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Plugins
% 23-09-85: Añadida función \emptyStave para imprimir un pentagrama vacío.
% 				En algunas partituras hay un pentagrama vacío antes del coda (lo cual mola bastante, por cierto)
% 05-09-14: Añadido a la función \emptyStave el esconder la armadura de la clave, que no estaba.
% 10-09-14: Mejorada la función \bendBefore
% 				Ahora es genérica, y se puede definir cualquier texto en un lateral que haga las funciones de ornamento
% 16-09-14: Mejorada la función para imprimir barras de improvisación.
% 				Antes se llamaba al comando \rs para pintar una barra que ocupaba una negra
% 				Se reemplaza \rs por \restImproOn y \restImproOff, siendo esta opción mucho más flexible.
% 				Cuando se activa \restImproOn, todo silencio se convierte en barra de improvisación, respetando su duración.
% 20-09-14: Mejorada la función para imprimir barras de improvisación.
% 				Se reemplaza \restImproOn y \restImproOff por \improOn y \improOff
% 				Ahora, también las notas del pentagrama salen como barra de improvisación.
% 				(los comandos \improvisationOn e \improvisationOff de lilypond no se llevan bien con LilyJAZZ)
% 				Se ha añadido un grob correspondiente a los slash de la fuente LilyJAZZ en el archivo lily-jazz.ly
% 01-10-14: Plugins
% 				Renombrada la función \emptyStave a \emptyStaff (stave es el plural de staff) y 
% 				corregida (ya no muestra el número de compás).
% 				Recordar que después de usar \emptyStaff se necesita redefinir el 
% 				número de compás con \set Score.currentBarNumber = #xx
% 01-10-14: Añadida la función \emptyBar que oculta un compás entero. Se puede usar para crear un 
% 				efecto de indentación en un pentagrama dado.
% 				Recordar que después de usar \emptyBar se necesita:
% 				- redefinir el número de compás con \set Score.currentBarNumber = #xx
% 				- volver a pintar la clave con \clef
% 				- volver a pintar la armadura con \key
% 15-10-14: Se le ha aumentado el grosor de los bordes de la función \boxed
% 18-10-15: Añadidas funciones de ornamento para antes de la nota:
% 				- \glissBefore: para mostrar un glissando antes de una nota
% 				- \shortGlissBefore: para mostrar un glissando corto antes de una nota
% 				- \jazzTurn: para mostrar el turn típico trompetero de jazz
% 24-10-15: Añadida funcion \subp para mostrar subito piano "sp"
% 27-10-15: Añadida funcion \ap (de accidental padding) para separar un poco el accidental de la siguiente nota
% 				al usar un paréntesis con \parenthesize, ya que se solapan el uno al otro
% 27-10-15: Añadida funcion \optOttava para imprimir octavaje opcional
% 				También he aprovechado para cambiar la fuente del texto "8va" que utilizaba Feta por defecto
% 28-10-15: Mejorada las funciones glissBefore y shortGlissBefore.
% 				Ahora se le pasa una lista #'(r x y) en la que "r" es la rotacion, "x" la posicion horizontal 
%				e "y" la positión vertical.

% Markup
% 23-09-85: Añadidas guías para correcta posición y markup de la indicación de tempo
% 27-10-15: Guía para posición de la indicación de tempo mejorada

% Layout
% 23-09-85: Ahora las articulaciones van por dentro de las ligaduras. Parece que es la mejor solución
% 04-09-14: Los creciendos y decreciendos tienen la línea un poco más gruesa
% 08-09-14: Ahora no se muestra la cancelación de accidentales al haber un cambio de armadura
% 15-09-14: Añadido un poco más de margen a los reguladores
% 20-09-14: Comentada la línea que oculta la cancelación de accidentales al haber cambio de armadura.
% 				Realmente, la cancelación es muy bienvenida.

% Chords
% 09-09-14: Añadida sentencia para que los acordes menores salgan con un guión en vez de con una M minúscula
% 26-10-14: Añadido el include "chord-exception.ily" que recoge una mejor notación de los acordes.
% 				Actualmente, recoge los acordes maj7, 7(b5), 7(#5), 7(b9), 7(#9), 7(add13), maj9(add13), 9(#11)
% 				Se irá añadiendo acordes a menudo.

% Global
% 15-10-14: Todas las instrucciones que se colocaban justo antes de empezar la música están ahora
% 				recogidas en una función \global.
% 				A \global se le ha añadido instrucciones para que agrupe las corcheas de dos en dos
% 				en vez de cuatro en cuatro (el comportamiento por defecto).

% Language
% 15-10-14: Cambiado el lenguaje de entrada de notas del finlandes al inglés
% 				La diferencia es que ahora los sostenidos se escriben "-s" en vez de "-is"
% 				y los bemoles "-f" en vez de "-es".

% Estructura
% 15-10-14: Para poder autoincluir los archivos de instrumento en otros 
% 				(para montar un midi, o un score), se han movido los plugins a un archivo separado
% 				y ahora se utiliza la función \includeOnce, evitando así el problema de las
% 				dependencias circulares. 
% 18-10-15: Mejorada la manera de trabajar con papeles separados y score al mismo tiempo

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~











% Disposición
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

title = "Moondance"

% Cabecera
\header {
	title = #title
	subtitle = "(as performed by Michael Bublé)"
	instrument = ""
	composer = "Van Morrison"
	arranger = "Arr: Gabriel Palacios"
	poet = ""
	tagline = "gabriel.ps.ms@gmail.com"
}

% Márgenes, fuentes y distancias del papel
\paper {

	% distancia entre la parte superior de la página y el título
	top-markup-spacing #'basic-distance = 5

	% distancia entre el título y el primer sistema
	markup-system-spacing #'basic-distance = 25

	% distancia entre el texto y el primer sistema 
	top-system-spacing #'basic-distance = 15

	% márgen inferior
	bottom-margin = 20
	
	% intentado del primer sistema
	indent = 0
	 
	% número de páginas
	% page-count = 1

	ragged-last-bottom = ##f 

	oddHeaderMarkup = \markup {
	 \on-the-fly #not-first-page \fill-line {
		\fromproperty #'page:page-number-string
		\fromproperty #'header:title
		\fromproperty #'header:instrument
	 }
	}

	evenHeaderMarkup = \markup {
	 \on-the-fly #not-first-page \fill-line {
		\fromproperty #'header:instrument
		\fromproperty #'header:title
		\fromproperty #'page:page-number-string
	 }
	}
}

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~







% Elementos comunes de la partitura
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
	% Oculta el primer pentagrama del markup
	\override Staff.VerticalAxisGroup #'remove-first = ##t

	% Indicación de tiempo
	\once \override Score.RehearsalMark.self-alignment-X = #LEFT
	\once \override Score.RehearsalMark.extra-offset = #'(0 . 3) 
	\mark \markup "Medium Swing"

	% s1*2
	% \once \override Score.RehearsalMark.X-offset = #5
	% \mark \markup \boxed "A"
	
	\partial 2
	s2
	
	s1*7
	
	\tag #'sb \break
	\mark \markup \boxed "A"
	s1*10
	
	\tag #'sb \break
	\mark \markup \boxed "B"
	s1*8
	
	\tag #'sb \break
	\mark \markup \boxed "C"
	s1*8
	
	\tag #'sb \break
	\mark \markup \boxed "D"
	s1*10
	
	\tag #'sb \break
	\mark \markup \boxed "E"
	s1*8
	
	\tag #'sb \break
	\mark \markup \boxed "F"
	s1*8
	
	\tag #'sb \break
	\once \override Score.RehearsalMark.self-alignment-X = #LEFT
	\mark \markup {
		\column { \boxed "G" }
		\column { "(Instrumental)" }
	}
	s1*9
	
	\tag #'sb \break
	s1*5
	
	\tag #'sb \break
	\mark \markup \boxed "H"
	s1*10
	
	\tag #'sb \break
	\mark \markup \boxed "I"
	s1*8
	
	\tag #'sb \break
	\mark \markup \boxed "J"
	s1*8
	
	\tag #'sb \break
	\mark \markup \boxed "K"
	s1*8
	
	\tag #'sb \break
	s1*8
	
	\tag #'sb \break
	s1*8
	
	\tag #'sb \break
	s1*5
}

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Elementos específicos del instrumento
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Cifrado
scoreChords = \chords {    
	
	\override ParenthesesItem.font-size = #-2
	
	\partial 2
	s2
	
	s4 g8:dim e:dim7/fs s2
	f8:9 e:dim7 s2.
	s4 a:9 ef8:maj7/af d:7.9+ s4
	
	g1:m7
	s1
	s1
	s1
	
	% estrofa
	g1:m7
	s1
	s1
	s1
	
	s1
	s1
	g1:m7
	s1
	
	g1:m7
	s1
	
	c2:m7 c2:m7/f
	bf1:maj7
	c1:m11^9
	g1:m9
	
	c1:m9
	g:m7
	a:m5-.7.13-
	d:5+.7.9+
	
	g2:m7 c2:m7
	g2:m7 c2:m7
	g2:m7 c2:m7
	s8 g8:m7 s8 d8:5+.7.9+ s2
	
	g2:m7 c2:m9
	g2:m7 bf2:13 
	a:m5-.7.9 d2:5+.7
	g4:m7 d2.:5+.7.9+
	
	% estrofa
	g1:m7
	s1
	s1
	s1
	
	s1
	s1
	g1:m7
	s1
	
	d2:m7/a d2:m7
	g2:m4^5 s8 g8:5+ s4
	
	c2:m7 c2:m7/f
	bf1:maj7
	a2:m7.5- d2:7.9-
	g2:m7 g2:7.9-
	
	c1:m7
	g1:m7
	a1:m5-.7.13-
	d1:5+.7.9+
	
	g2:m7 c2:m7
	g2:m7 c2:m7
	g2:m7 c2:m7
	d2:m7/a d2:7.9-
	
	c1:m7/f
	f2:m7 bf2:7.9-.13
	bf2:m7+.9/a d2:5+.7.9+
	s4 a8:m7.5-.9 c8:5-.7.9+/af s2
	
	f2:m7 bf2:9
	ef2:maj7 a4:m7.5- ef4:7
	af2:6 e4/d df4:7.5-/g
	c2:m9 gf4/e c4:7.5-
	f2:m9 bf4:maj7.9 df4:6
	s8 bf8/ef s8 af8:6 s2
	
	a1:m5-.7.13-
	a1:m5-.7.13-
	a2:m5-.7.13- d2:m/ef
	
	g4:m/d g8:dim e:dim7/fs s2
	f8:9 e:dim7 s2.
	s4 a:9 ef8:maj7/af d:7.9+ s4
	g:m7.4 s s2
	s1
	
	% estrofa
	g1:m7 
	s1
	s1
	s1
	
	s1
	s1
	g1:m7
	s1
	
	g1:m7
	g4:5- g4 g4:5- g4
	
	c2:m9 b2:13
	bf2:9 e2:m7.5-
	ef2:7 bf2:7 
	c2:5.9^7/g g2:m7.5-/df
	
	c2:m7 f2:9
	bf:6 f4:9/ef g:m7.5-
	a1:m5-.7.13-
	d1:5+.7.9.11+
	
	g2:m7 c2:m7
	g2:m7 ef2:7
	ef2/a d2:5+.7.9+
	g2:m9 d2:5+.7.9+
	
	g2:m7 c2:m7
	g2:m7 ef2:7
	ef2/a d2:5+.7.9+
	g2:m9 d2:5+.7.9-
	
	g2:m7 c2:m11
	g2:m7 ef2:7
	g2:m7 c2:m11
	g2:m7 d2:5+.7.9+
	
	ef2:13 af2:9^3/df
	bf2:7.5+/af g2:m13
	c1:9.5-
	s1
	
	g1:m7 
	s1
	s1
	s1
	
	g1:m7 
	s1
	s1
	s1
	
	g1:m7 
	s1
	s1
	s1
	
	g1:m7 
	s1
	s1
	s1
	
	g1:m7 
	s1
	s1
	s1
	
	s2 g:m7
}

% Música
trumpet = \relative c'' {
  
	\global
	\key g \minor

	\partial 2
	r2
	
	r4 \f cs8 d -> ~ d2
	c?8 bf -> ~ bf2.
	r4 g' -^ bf8 f -> ~ f4
	
	\tag #'b \break
	g4 -^ r r2
	R1*3
	
	\tag #'b \break
	\repeat volta 2 {
		R1*6
	}
	\alternative {
		{ R1*2 }
		{ R1*2 }
	}
	
	\tag #'b \break
	R1*6
	\times 2/3 { r4 \mf c, -- c -- } c4 -^ r4
	\times 2/3 { r4 bf -- bf -- } bf4 -^ r4
	
	\tag #'b \break
	r8 \p d, -> \> ~ d4 r8 \! ef -> \> ~ ef4
	r8 \! d -> \> ~ d4 r8 \! c -> \> ~ c4
	r8 \! d -> \> ~ d4 r8 \! ef -> \> ~ ef4
	r8 \! d -^ r f -> ~ f4 r
	
	\tag #'b \break
	r8 \p d -> \> ~ d4 r8 \! ef -> \> ~ ef4
	r8 \! d -^ r4 bf'4. \mf c8 -> ~
	c2 bf4 -- r4
	r4 r8 f -> ~ f4 r
	
	\tag #'b \break
	\repeat volta 2 {
		r4 ^\markup { \box "Straight mute" " (Play 2nd x only)" } r8 \p g -> ~ g2 ~
		g4 g8 g -. r2
		r2 r4 r8 bf -> ~
		bf4 a8 \jazzTurn g a g f4 -.
		
		\tag #'b \break
		r4 r8 g -> ~ g2 ~
		g4 g8 g -. r2 ^\markup \box "Open"
	}
	\alternative {
		{ R1*2 }
		{ R1*2 }
	}
	
	\tag #'b \break
	R1*6
	\times 2/3 { r4 \mf c -- c -- } c4 -^ r4
	\times 2/3 { r4 d -- d -- } d4 -^ r8 \p d -.
	
	\tag #'b \break
	r4 r8 bf -. r2
	g4 -. r8 bf -. r2
	d4 -. r8 bf -. r2
	r8 \mf c4 -- bf8 \jazzTurn c c bf d -^
	
	\tag #'b \break
	R1*2
	c4 -^ \mf r8 d -> \< ~ d2
	r4 \f g8 ef -> ~ ef4 \> r \!
	
	\tag #'b \break
	\once \override Staff.TextScript.X-offset = #2
	ef4 -. \sp _"cresc." r8 f -> ~ f4 d8 bf -.
	r4 r8 g' -> ~ g4 ef8 c -.
	r4 r8 af' -> ~ af4 f8 d -.
	
	r4 r8 bf' -> ~ bf4 gf8 ef? -.
	
	\tag #'b \break
	r4 r8 c' -> ~ c4 af8 bf -.
	r4 r8 \f c -> ~ c4 << { bf8  c -> ~
	c1 ~
 	c1 ^\markup \small "(long fall)" } \\ { s4 s1 \hideNotes c2. \glissando c,4 \unHideNotes} >>
	
	\tag #'b \break
	R1
	r4 \f cs8 d -> ~ d2
	c?8 bf -> ~ bf2.
	\tag #'b \break
	r4 g' -^ bf8 f -> ~ f4
	
	g4 -^ r r2
	R1
	
	\tag #'b \break
	\repeat volta 2 {
		R1*6
	}
	\alternative {
		{ R1*2 }
		{ R1 
		  g16 \f g -. r8 g16 g -. r8 g16 g -. r8 g16 g -. r8 }
	}
	
	\tag #'b \break
	c,4 --  r ef -- r
	c -- r bf -- r 
	ef -- r d -- r
	r8 g4. -> \prallprall bf8 g -^ r c,-^
	
	\tag #'b \break
	r4 r8 \mf c -^ r4 r8 d -^
	r2 a'4 -^ \< bf -^
	\times 2/3 { r4 \f c -- c -- } c4 -^ r4
	\times 2/3 { r4 d -- d -- } d4 -^ r4
	
	\tag #'b \break
	r8 \f g, -> \bendAfter -5 r4 r8 bf, c d 
	g, -. f' -> ~ f4 \times 2/3 { df8 c bf } c ef -> ~
	ef2 d4. g,8 -^
	r4 r8 bf' -> ~ bf4 r
	
	\tag #'b \break
	r8 g -> \bendAfter -5 r4 r8 bf, c d 
	g, -. f' -> ~ f4 \times 2/3 { df8 c bf } c ef -> ~
	ef2 d4. g,8 -^
	R1
	
	\tag #'b \break
	g4 -^ r8  f -> ~  f2
	g 4 -^ r8 bf -> ~ bf2
	g 4 -^ r8  f -> ~  f2
	g 4 -^ r8 bf -> ~ bf4 r
	
	\tag #'b \break
	f2 ^"Rit." \sp \< g
	af bf
	c1 \! \mf \fermata
	R1
	
	\tag #'b \break
	R1*4 ^\markup \box "Harmon. mute"
	r2 c \p ~
	c1 ~
	c1 ~
	c2 \> r \!
	
	\tag #'b \break
	df2 \p c ~
	c1 ~
	c1 ~
	c2 \> r \!
	
	\tag #'b \break
	r2 c \p ~
	c1 ~
	c1 ~
	c2 \> r \!
	
	\tag #'b \break
	df2 \p c ~
	c1 ~
	c1 ~
	c2 \> r \!
	
	r2 c \p \fermata
	
	\bar "|."
}

altoSax = \relative c'' {
  
	\global
	\key g \minor

	\partial 2
	r2
	
	r4 \f bf8 bf -> ~ bf2
	a8 g -> ~ g2.
	r4 cs -^ d8 bf -> ~ bf4
	
	\tag #'b \break
	g4 -^ r8 \mf a -. r2
	bf4 -. r8 a -. r2
	g4 -. \mp r8 a -. r2
	bf4 -. r8 a -. r2
	
	\tag #'b \break
	\repeat volta 2 {
		R1*6
	}
	\alternative {
		{ R1*2 }
		{ R1*2 }
	}
	
	\tag #'b \break
	bf,1 \p
	c8 d -> ~ d2 \> r4 \!
	r8 \p f -> ~ f2.
	g4 -. \times 2/3 { df8 c bf -> ~ } bf2
	
	\tag #'b \break
	r8 g' -> ~ g c, g' g r f -> ~
	f4 c -. d -. r4
	\times 2/3 { r4 \mf f -- f -- } f4 -^ r4
	\times 2/3 { r4 f -- f -- } f4 -^ r4
	
	\tag #'b \break
	r8 \p bf, -> \> ~ bf4 r8 \! bf -> \> ~ bf4
	r8 \! bf -> \> ~ bf4 r8 \! bf -> \> ~ bf4
	r8 \! bf -> \> ~ bf4 r8 \! bf -> \> ~ bf4
	r8 \! bf -^ r c -> ~ c4 r
	
	\tag #'b \break
	r8 \p bf -> \> ~ bf4 r8 \! bf -> \> ~ bf4
	r8 \! bf -^ r4 g'4. \mf g8 -> ~
	g2 fs4 -- r4
	r4 r8 c -> ~ c4 r
	
	\tag #'b \break
	\repeat volta 2 {
		r8 \p ^"(Play 2nd x only)" g' -> ~ g2. ~
		g4 r4 r2
		r8 d -> ~ d2. ~
		\tag #'b \break
		d2. r4
		
		r8 g -> ~ g2. ~
		g4 r r8 g, \times 2/3 { a \< bf d }
	}
	\alternative {
		{ \tag #'b \break
		  R1*2 \! }
		{ f2 \mf a4. g8 -> ~
		  g4 g16 g -. r8 r2 }
	}
	
	\tag #'b \break
	r8 \mp c4. -> \glissando c,8 c r c' -> ~ 
	c4 \times 2/3 { bf8 g d } f g r4
	r8 d4. -> \glissando bf'4. g8 -> ~
	g2 \glissando d'4. g,8
	
	\tag #'b \break
	c2 c,
	d1 \fp \<
	\times 2/3 { r4 \mf f -- f -- } f4 -^ r4
	\times 2/3 { r4 f -- f -- } f4 -^ r8 \p bf -.
	
	\tag #'b \break
	r4 r8 g -. r2
	f4 -. r8 g -. r2
	bf4 -. r8 g -. r2
	r8 \mf a4 -- g8 \jazzTurn a a fs bf -^
	
	\tag #'b \break
	r2 r8 g4. -> \bendBefore 
	r2 r8 g4. -> \bendBefore 
	bf4 -^ r8 bf -> \< ~ bf2
	r4 \f b8 bf -> ~ bf4 \> r \!
	
	\tag #'b \break
	\once \override Staff.TextScript.X-offset = #2
	af4 -. \sp _"cresc." r8 c -> ~ c4 bf8 g -.
	r4 r8 ef' -> ~ ef4 df8 af -.
	r4 r8 ef' -> ~ ef4 df8 bf -.
	r4 r8 gf' -> ~ gf4 ff8 c -.
	
	\tag #'b \break
	r4 r8 f -> ~ f4 df8 d -.
	r8 d -. r \f  f ~ f4 r
	\times 2/3 { r4 ef -- ef -- } ef -^ r4
	\times 2/3 { r4 ef -- ef -- } ef -^ r4
	
	\tag #'b \break
	\times 2/3 { r4 ef -- ef -- } \times 2/3 {f8 -^ cs, ( \sp \< d } \times 2/3 { f g a }
	bf8 \f \bendAfter -5 ) r bf8 bf -> ~ bf2
	a8 g -> ~ g2.
	\tag #'b \break
	r4 cs -^ d8 bf -> ~ bf4
	
	d4 -^ r4 r2
	R1
	
	\tag #'b \break
	\repeat volta 2 {
		R1*6
	}
	\alternative {
		{ R1*2 }
		{ R1 
		  cs16 \f cs -. r8 d16 d -. r8 cs16 cs -. r8 d16 d -. r8 }
	}
	
	\tag #'b \break
	g,4 --  r af -- r
	f -- r g -- r 
	bf -- r bf8 d bf g ~
	g2 ~ g4 r8 bf -^
	
	\tag #'b \break
	r4 r8 \mf a -^ r4 r8 bf -^
	r2 f'4 -^ \< f -^
	\times 2/3 { r4 \f g -- g -- } g4 -^ r4
	\times 2/3 { r4 fs -- fs -- } fs4 -^ r4
	
	\tag #'b \break
	d,4 -. \f r8 ef -> ~ ef2
	d4 -. r8 ef -> ~ ef4. bf'8 -> ~
	bf2 bf4. d,8 -^
	r4 r8 f' -> ~ f4 r
	
	\tag #'b \break
	d,4 -. r8 ef -> ~ ef2
	d4 -. r8 ef -> ~ ef4. bf'8 -> ~
	bf2 bf4. d,8 -^
	r4 ef2 -> r4
	
	\tag #'b \break
	d4 -^ r8 c -> ~ c2
	d 4 -^ r8 f -> ~ f2
	d 4 -^ r8 c -> ~ c2
	d 4 -^ r8 fs -> ~ fs4 r
	
	\tag #'b \break
	d2 ^"Rit." \sp \< ef
	gf f
	bf1 \! \mf \fermata
	R1
	
	\tag #'b \break
	R1*4
	r2 g \p ~
	g1 ~
	g1 ~
	g2 \> r \!
	
	\tag #'b \break
	af2 \p g ~
	g1 ~
	g1 ~
	g2 \> r \!
	
	\tag #'b \break
	r2 g \p ~
	g1 ~
	g1 ~
	g2 \> r \!
	
	\tag #'b \break
	gf2 \p g ~
	g1 ~
	g1 ~
	g2 \> r \!
	
	r2 g \p \fermata
	
	\bar "|."
}

tenorSax = \relative c' {
  
	\global
	\key g \minor

	\partial 2
	r2
	
	r4 \f g'8 g -> ~ g2
	g8 e -> ~ e2.
	r4 b' -^ bf8 fs -> ~ fs4
	
	\tag #'b \break
	d4 -^ r8 \mf e -. r2
	f4 -. r8 e -. r2
	d4 -. \mp r8 e -. r2
	f4 -. r8 e -. r2
	
	\tag #'b \break
	\repeat volta 2 {
		R1*6
	}
	\alternative {
		{ R1*2 }
		{ R1
		  r2 r4 \p g, ~ }
	}
	
	\tag #'b \break
	g1
	a8 bf -> ~ bf2 \> r4 \!
	r8 \p c -> ~ c2.
	d4 -. \times 2/3 { df8 c g -> ~ } g2
	
	\tag #'b \break
	r8 ef' -> ~ ef bf ef ef r d -> ~
	d4 a -. bf -. r4
	\times 2/3 { r4 \mf ef -- ef -- } ef4 -^ r4
	\times 2/3 { r4 c -- c -- } c4 -^ r4
	
	\tag #'b \break
	r2 g'2 ^"(Soli)" \mf ~
	g2 ~ g8 bf \times 2/3 { g d c ~ }
	c1 ~ 
	c4 r8 bf \times 2/3 { df c bf } \times 2/3 { g f g' -> ~ }
	
	\tag #'b \break
	g1 ~ 
	g4 r4 d4. \mf ^"(Ensemble)" ef8 -> ~
	ef2 c4 -- r4
	r4 r8 bf -> ~ bf4 r
	
	\tag #'b \break
	\repeat volta 2 {
		bf1 \p ^ "(Play 2nd x only)" \repeatTie ~
		bf4 r4 r2
		r8 g -> ~ g2. ~
		\tag #'b \break
		g2. r4
		
		r8 bf -> ~ bf2. ~ 
		bf4 r r8 g \times 2/3 { a \< bf d }
	}
	\alternative {
		{ \tag #'b \break
		  R1 \!
		  r4 r8 \p ^ "(Play)" d df c g bf -> \laissezVibrer}
		{ c2 \mf c4. bf8 -> ~
		  bf4 bf16 bf -. r8 r2 }
	}
	
	\tag #'b \break
	r8 \mp f'4. -> \glissando a,8 a r f' -> ~ 
	f4 \times 2/3 { d8 cs a } bf d r4
	r8 c4. -> \glissando fs4. d8 -> ~
	d2 \glissando af'4. d,8
	
	\tag #'b \break
	bf1
	bf \fp \<
	\times 2/3 { r4 \mf ef -- ef -- } ef4 -^ r4
	\times 2/3 { r4 c -- c -- } c4 -^ r8 \p g' -.
	
	\tag #'b \break
	r4 r8 ef -. r2
	d4 -. r8 ef -. r2
	g4 -. r8 ef -. r2
	r8 \mf f4 -- e8 \jazzTurn fs fs e g -^
	
	\tag #'b \break
	r2 r8 g4. -> \bendBefore 
	r2 r8 g4. -> \bendBefore 
	f4 -^ r8 fs -> \< ~ fs2
	r4 \f g8 gf -> ~ gf4 \> r \!
	
	\tag #'b \break
	\once \override Staff.TextScript.X-offset = #2
	f4 -. \sp _"cresc." r8 af -> ~ af4 af8 ef -.
	r4 r8 c' -> ~ c4 bf8 f -.
	r4 r8 cf' -> ~ cf4 cf8 g -.
	r4 r8 df' -> ~ df4 bf8 af -.
	
	\tag #'b \break
	r4 r8 d -> ~ d4 bf8 c -.
	r8 bf -. r \f ef ~ ef4 r
	\times 2/3 { r4 a, -- a -- } a -^ r4
	\times 2/3 { r4 a -- a -- } a -^ r4
	
	\tag #'b \break
	\times 2/3 { r4 a -- a -- } \times 2/3 {d8 -^ cs, ( \sp \< d } \times 2/3 { f g a }
	bf8 \f \bendAfter -5 ) r g8 g -> ~ g2
	g8 e -> ~ e2.
	\tag #'b \break
	r4 b' -^ bf8 fs -> ~ fs4
	
	bf4 -^ r r2
	R1
	
	\tag #'b \break
	\repeat volta 2 {
		R1*6
	}
	\alternative {
		{ R1*2 }
		{ r2 r4 r8 \f g, ->
		  r8 g -> r g -> r g -> r4 }
	}
	
	\tag #'b \break
	ef'4 --  r gf -- r
	d -- r d -- r 
	g -- r af8 d bf e, ~
	e2 f8 f -^ r ef-^
	
	\tag #'b \break
	r4 r8 \mf g -^ r4 r8 g -^
	r2 c4 -^ \< df -^
	\times 2/3 { r4 \f ef -- ef -- } ef4 -^ r4
	\times 2/3 { r4 c -- c -- } c4 -^ r4
	
	\tag #'b \break
	bf,4 -. \f r8 c -> ~ c2
	bf4 -. r8 df -> ~ df4. g8 -> ~
	g2 f4. bf,8 -^
	r4 r8 c' -> ~ c4 r
	
	\tag #'b \break
	bf,4 -. r8 c -> ~ c2
	bf4 -. r8 df -> ~ df4. g8 -> ~
	g2 f4. bf,8 -^
	r4 bf2 -> r4
	
	\tag #'b \break
	\times 2/3 { r8 ^"(Soli)" bf ( g } \times 2/3 { c bf d } \times 2/3 { c f d } \times 2/3 { c f g }
	bf ) df -> ~ df4 ~ df8 bf4. ->
	g4 -. c8 bf c bf f d
	f16 g8 c,16 cs8 d -> ~ d4 r
	
	\tag #'b \break
	c2 ^"(Ensemble)" _"Rit." \sp \< bf
	d d
	fs1 \! \mf \fermata
	R1
	
	\tag #'b \break
	R1*4
	r2 e \p ~
	e1 ~
	e1 ~
	e2 \> r \!
	
	\tag #'b \break
	f2 \p e ~
	e1 ~
	e1 ~
	e2 \> r \!
	
	\tag #'b \break
	r2 e \p ~
	e1 ~
	e1 ~
	e2 \> r \!
	
	\tag #'b \break
	f2 \p e ~
	e1 ~
	e1 ~
	e2 \> r \!
	
	r2 e \p \fermata
	
	\bar "|."
}

trombone = \relative c' {
  
	\global
	\clef bass
	\key g \minor

	\partial 2
	r2
	
	r4 \f g'8 e -> ~ e2
	ef?8 d -> ~ d2.
	r4 e -^ ef8 d -> ~ d4
	
	\tag #'b \break
	bf4 -^ r8 \mf c -. r2
	d4 -. r8 c -. r2
	bf4 -. \mp r8 c -. r2
	d4 -. r8 c -. r2
	
	\tag #'b \break
	\repeat volta 2 {
		R1*6
	}
	\alternative {
		{ R1*2 }
		{ R1
		  r2 f, \p ~ }
	}
	
	\tag #'b \break
	f1
	bf8 f -> ~ f2 \> r4 \!
	r8 \p bf -> ~ bf2.
	bf4 -. r8 f -> ~ f2
	
	\tag #'b \break
	r8 d' -> ~ d g, d' d r c -> ~
	c4 f, -. f -. r4
	\times 2/3 { r4 \mf g -- g -- } g4 -^ r4
	\times 2/3 { r4 fs -- fs -- } fs4 -^ r4
	
	\tag #'b \break
	r8 \p f? -> \> ~ f4 r8 \! g -> \> ~ g4
	r8 \! f -> \> ~ f4 r8 \! ef -> \> ~ ef4
	r8 \! f -> \> ~ f4 r8 \! g -> \> ~ g4
	r8 \! f -^ r fs -> ~ fs4 r
	
	\tag #'b \break
	r8 \p f? -> \> ~ f4 r8 \! g -> \> ~ g4
	r8 \! f -^ r4 af4. \mf bf8 -> ~
	bf2 a?4 -- r4
	r4 r8 fs -> ~ fs4 r
	
	\tag #'b \break
	\repeat volta 2 {
		r4 ^\markup { \box "Straight mute" " (Play 2nd x only)" } r8 \p d' -> ~ d2 ~
		d4 d8 d -. r2
		R1*2
		
		\tag #'b \break
		r4 r8 d -> ~ d2 ~
		d4 d8 d -. r2 ^\markup \box "Open"
	}
	\alternative {
		{ R1*2 }
		{ R1*2 }
	}
	
	\tag #'b \break
	r8 \mp ef4. -> \glissando ef,8 ef r d' -> ~ 
	d4 \times 2/3 { c8 bf f } g bf r4
	r8 g4. -> \glissando c4. bf8 -> ~
	bf2 \glissando b4. af8
	
	\tag #'b \break
	g1
	f \fp \<
	\times 2/3 { r4 \mf g -- g -- } g4 -^ r4
	\times 2/3 { r4 fs -- fs -- } fs4 -^ r8 \p f' -.
	
	\tag #'b \break
	r4 r8 d -. r2
	bf4 -. r8 d -. r2
	f4 -. r8 d -. r2
	r8 \mf d4 -- c8 \jazzTurn ef d d c -^
	
	\tag #'b \break
	R1*2
	df4 -^ \mf r8 c -> \< ~ c2
	r4 \! ef8 c -> ~ c4 \> r \!
	
	\tag #'b \break
	\once \override Staff.TextScript.X-offset = #2
	c4 -. \sp _"cresc. - - -" r8 d -> ~ d4 f8 d -.
	r4 r8 a' -> ~ a4 g8 ef -.
	r4 r8 ff -> ~ ff4 g8 f -.
	r4 r8 bf -> ~ bf4 gf8 f -.
	
	\tag #'b \break
	r8 g? ^"(Soli)" f4 ~ f2
	r8 f -. ^"(Ensemble)" r \f bf ~ bf4 r
	\times 2/3 { r4 g -- g -- } g -^ r4
	\times 2/3 { r4 g -- g -- } g -^ r4
	
	\tag #'b \break
	\times 2/3 { r4 g -- g -- } a -^ r4
	r4 \f g8 e -> ~ e2
	ef?8 d -> ~ d2.
	\tag #'b \break
	r4 e -^ ef8 d -> ~ d4
	
	c4 -^ r4 r2
	R1
	
	\tag #'b \break
	\repeat volta 2 {
		R1*6
	}
	\alternative {
		{ R1*2 }
		{ R1 
		  b16 \f b -. r8 b16 b -. r8 b16 b -. r8 b16 b -. r8 }
	}
	
	\tag #'b \break
	d4 --  r a -- r
	c -- r bf -- r 
	df -- r f -- r8 c -> ~
	c2 df8 df -^ r c-^
	
	\tag #'b \break
	r4 r8 \mf ef -^ r4 r8 f -^
	r2 a4 -^ \< g -^
	\times 2/3 { r4 \f a -- a -- } a4 -^ r4
	\times 2/3 { r4 af -- af -- } af4 -^ r4
	
	\tag #'b \break
	f,4 -. \f r8 g -> ~ g2
	f4 -. r8 g -> ~ g4. bf8 -> ~
	bf2 c4. a8 -^
	r4 r8 fs' -> ~ fs4 r
	
	\tag #'b \break
	f,4 -. r8 g -> ~ g2
	f4 -. r8 g -> ~ g4. bf8 -> ~
	bf2 c4. a8 -^
	r4 fs2 -> r4
	
	\tag #'b \break
	bf4 -^ r8 bf -> ~ bf2
	bf 4 -^ r8 c -> ~ c2
	bf 4 -^ r8 bf -> ~ bf2
	bf 4 -^ r8 c -> ~ c4 r
	
	\tag #'b \break
	g2 ^"Rit." \sp \< af
	bf c
	e1 \! \mf \fermata
	R1
	
	\tag #'b \break
	R1*4
	r2 d \p ~
	d1 ~
	d1 ~
	d2 \> r \!
	
	\tag #'b \break
	ef2 \p d ~
	d1 ~
	d1 ~
	d2 \> r \!
	
	\tag #'b \break
	r2 d \p ~
	d1 ~
	d1 ~
	d2 \> r \!
	
	\tag #'b \break
	ef2 \p d ~
	d1 ~
	d1 ~
	d2 \> r \!
	
	r2 d \p \fermata
	
	\bar "|."
}

piano = \relative c' {
  
	\global
	\key g \minor

	\partial 2
	r2
	
	r4 \f \improOn c8 c -> ~ c2
	c8 c -> ~ c2.
	\of r4 c -^ c8 c -> ~ c4
	
	\tag #'b \break
	r4 \mf r4 r4 r4
	r4 r4 r4 r4
	r4 \mp r4 r4 r4
	r4 r4 r4 r4
	
	\tag #'b \break
	\repeat volta 2 {
		r4 ^"(Tacet 1st x)" \p \repeat unfold 11 { r4 }
		\tag #'b \break
		\repeat unfold 4 { r4 }
		
		\repeat unfold 8 { r4 }
	}
	\alternative {
		{ \tag #'b \break
		  R1*2 }
		{ \repeat unfold 8 { r4 } }
	}
	
	\tag #'b \break
	r4 \p \repeat unfold 15 { r4 }
	
	\tag #'b \break
	\repeat unfold 8 { r4 }
	\of \times 2/3 { r4 \mf c -- c -- } c -^ r
	\of \times 2/3 { r4 c -- c -- } c -^ r
	
	\tag #'b \break
	r4 \mf \repeat unfold 11 { r4 }
	\of r8 c -^ \of r c -> ~ c2
	\tag #'b \break
	\repeat unfold 4 { r4 }
	r4 \< r4 c4. \mf c8 ~
	c2 c4 -- r4
	\of r4 \of r8 c -> ~ c2 \>
	
	\tag #'b \break
	\repeat volta 2 {
		r4 \p \repeat unfold 11 { r4 }
		\tag #'b \break
		\repeat unfold 4 { r4 }
		
		\repeat unfold 8 { r4 }
	}
	\alternative {
		{ \repeat unfold 8 { r4 } }
		{ r4 r4 r4 r4
		  r4 r4 \of r8 c ~ c4 }
	}
	
	\tag #'b \break
	\repeat unfold 16 { r4 }
	
	\tag #'b \break
	\repeat unfold 8 { r4 }
	\of \times 2/3 { r4 \mf c -- c -- } c -^ r
	\of \times 2/3 { r4 c -- c -- } c -^ r
	
	\tag #'b \break
	\of r4 c4 r r
	\repeat unfold 8 { r4 }
	\of r8 c -> ~ c4 r4 r4
	
	\tag #'b \break
	\repeat unfold 8 { r4 }
	r4 r4 r4 \< r4
	\of r4 \f c8 c -> ~ c2 \>
	
	\tag #'b \break
	r4 \sp _"cresc. - - -" \repeat unfold 11 { r4 }
	
	\tag #'b \break
	\repeat unfold 8 { r4 }
	\of r8 c -. \of r \f c ~ c4 r
	
	\tag #'b \break
	\of \times 2/3 { r4 c -- c -- } c -^ r
	\of \times 2/3 { r4 c -- c -- } c -^ r
	\of \times 2/3 { r4 c -- c -- } r r
	
	\tag #'b \break
	r4 \f c8 c -> ~ c2
	c8 c -> ~ c2.
	\of r4 c -^ c8 c -> ~ c4
	r4 \of r4 \of r2
	R1
	
	
	\tag #'b \break
	\repeat volta 2 {
		r4 ^"(Tacet 1st x)" \p \repeat unfold 11 { r4 }
		\tag #'b \break
		\repeat unfold 4 { r4 }
		
		\repeat unfold 8 { r4 }
	}
	\alternative {
		{ \tag #'b \break
		  R1*2 }
		{ r4 r4 r4 r4
		  c4 -^ \f c4 -^ c4 -^ c4 -^ }
	}
	
	\tag #'b \break
	r4 \f \repeat unfold 15 { r4 }
	
	\tag #'b \break
	\repeat unfold 4 { r4 }
	r4 \< r4 r4 r4 
	\of \times 2/3 { r4 \f c -- c -- } c -^ r
	\of \times 2/3 { r4 c -- c -- } c -^ r
	
	\tag #'b \break
	\repeat unfold 16 { r4 }
	
	\tag #'b \break
	\repeat unfold 12 { r4 }
	r4 c8 c ~ c4 r
	
	\tag #'b \break
	\repeat unfold 12 { r4 }
	r4 \of r8 c ~ c2
	
	\tag #'b \break
	c2 \rit c2 
	c2 c2
	c1 \! \fermata
	R1
	
	\tag #'b \break
	R1*4
	r4 \p \repeat unfold 15 { r4 }
	
	\tag #'b \break
	\repeat unfold 16 { r4 }
	
	\tag #'b \break
	\repeat unfold 16 { r4 }
	
	\tag #'b \break
	\repeat unfold 16 { r4 }
	
	\of r2 c2 \fermata
	
	\bar "|."
}

guitar = \relative c' {
  
	\global
	\key g \minor

	\partial 2
	r2
	
	r4 \f \improOn c8 c -> ~ c2
	c8 c -> ~ c2.
	\of r4 c -^ c8 c -> ~ c4
	
	\tag #'b \break
	\improOff
	<f, bf d>4 \mf r8 <g c e> r2
	<a d f>4 r8 <g c e> r2
	<f bf d>4 \mp r8 <g c e> r2
	<a d f>4 r8 <g c e> r2
	
	\tag #'b \break
	\repeat volta 2 {
		<f bf d>4 ^"(Tacet 1st x)" \p r8 <g c e> r2
		<a d f>4 r8 <g c e> r2
		<f bf d>4 r8 <g c e> r2
		\tag #'b \break
		<a d f>4 r8 <g c e> r2
		
		<f bf d>4 r8 <g c e> r2
		<a d f>4 r8 <g c e> r2
	}
	\alternative {
		{ \tag #'b \break
		  R1*2 }
		{ <f bf d>4 r8 <g c e> r2
		  r8 <df' f> ( ^"(Smooth disto sound)" \times 2/3 { c bf g } <bf d>4 g ) }
	}
	
	\tag #'b \break
	\improOn r4 \p \repeat unfold 15 { r4 }
	
	\tag #'b \break
	\repeat unfold 8 { r4 }
	\of \times 2/3 { r4 \mf c -- c -- } c -^ r
	\of \times 2/3 { r4 c -- c -- } c -^ r
	
	\tag #'b \break
	r4 \mf \repeat unfold 11 { r4 }
	\of r8 c -^ \of r c -> ~ c2
	\tag #'b \break
	\repeat unfold 4 { r4 }
	r4 \< r4 c4. \mf c8 ~
	c2 c4 -- r4
	\of r4 \of r8 c -> ~ c2 \>
	
	\tag #'b \break
	\repeat volta 2 {
		\improOff
		<f, bf d>4 ^"(Tacet 1st x)" \p r8 <g c e> r2
		<a d f>4 r8 <g c e> r2
		<f bf d>4 r8 <g c e> r2
		\tag #'b \break
		<a d f>4 r8 <g c e> r2
		
		<f bf d>4 r8 <g c e> r2
		<a d f>4 r8 <g c e> r2
	}
	\alternative {
		{ \tag #'b \break
		  R1*2 }
		{ \improOn r4 r4 r4 r4
		  r4 r4 \of r8 c ~ c4 }
	}
	
	\tag #'b \break
	\repeat unfold 16 { r4 }
	
	\tag #'b \break
	\repeat unfold 8 { r4 }
	\of \times 2/3 { r4 \mf c -- c -- } c -^ r
	\of \times 2/3 { r4 c -- c -- } c -^ r
	
	\tag #'b \break
	\of r4 c4 r r
	\repeat unfold 8 { r4 }
	\of r8 c -> ~ c4 r4 r4
	
	\tag #'b \break
	\repeat unfold 8 { r4 }
	r4 r4 r4 \< r4
	\of r4 \f c8 c -> ~ c2 \>
	
	\tag #'b \break
	r4 \sp _"cresc. - - -" \repeat unfold 11 { r4 }
	
	\tag #'b \break
	\repeat unfold 8 { r4 }
	\of r8 c -. \of r \f c ~ c4 r
	
	\tag #'b \break
	\of \times 2/3 { r4 c -- c -- } c -^ r
	\of \times 2/3 { r4 c -- c -- } c -^ r
	\of \times 2/3 { r4 c -- c -- } r r
	
	\tag #'b \break
	r4 \f c8 c -> ~ c2
	c8 c -> ~ c2.
	\of r4 c -^ c8 c -> ~ c4
	r4 \of r4 \of r2
	R1
	
	
	\tag #'b \break
	\repeat volta 2 {
		\improOff
		<f, bf d>4 ^"(Tacet 1st x)" \p r8 <g c e> r2
		<a d f>4 r8 <g c e> r2
		<f bf d>4 r8 <g c e> r2
		\tag #'b \break
		<a d f>4 r8 <g c e> r2
		
		<f bf d>4 r8 <g c e> r2
		<a d f>4 r8 <g c e> r2
	}
	\alternative {
		{ \tag #'b \break
		  R1*2 }
		{ <f bf d>4 \< r8 <g c e> r2
		  \improOn c4 -^ \f c4 -^ c4 -^ c4 -^ }
	}
	
	\tag #'b \break
	r4 \f \repeat unfold 15 { r4 }
	
	\tag #'b \break
	\repeat unfold 4 { r4 }
	r4 \< r4 r4 r4 
	\of \times 2/3 { r4 \f c -- c -- } c -^ r
	\of \times 2/3 { r4 c -- c -- } c -^ r
	
	\tag #'b \break
	\repeat unfold 16 { r4 }
	
	\tag #'b \break
	\repeat unfold 12 { r4 }
	r4 c8 c ~ c4 r
	
	\tag #'b \break
	\repeat unfold 12 { r4 }
	r4 \of r8 c ~ c2
	
	\tag #'b \break
	c2 \rit c2 
	c2 c2
	c1 \! \fermata
	R1
	
	\tag #'b \break
	R1*8
	R1*8
	
	\tag #'b \break
	R1*4
	
	\of r2 c2 \fermata
	
	\bar "|."
}

bass = \relative c' {
  
	\global
	\clef bass
	\key g \minor

	\partial 2
	r2
	
	r4 \f \improOn c8 c -> ~ c2
	c8 c -> ~ c2.
	\of r4 c -^ c8 c -> ~ c4
	
	\tag #'b \break
	r4 \mf \repeat unfold 15 { r4 }
	
	\tag #'b \break
	\repeat volta 2 {
		r4 \p ^"(Soli 1st x)" \repeat unfold 11 { r4 }
		
		\tag #'b \break
		\repeat unfold 12 { r4 }
	}
	\alternative {
		{ \tag #'b \break
		  \repeat unfold 8 { r4 } }
		{ \repeat unfold 8 { r4 } }
	}
	
	\tag #'b \break
	r4 \p \repeat unfold 15 { r4 }
	
	\tag #'b \break
	\repeat unfold 8 { r4 }
	\of \times 2/3 { r4 \mf c -- c -- } c -^ r
	\of \times 2/3 { r4 c -- c -- } c -^ r
	
	\tag #'b \break
	r4 \mf \repeat unfold 11 { r4 }
	c8 c -^ \of r c -> ~ c2
	\tag #'b \break
	\repeat unfold 4 { r4 }
	r4 \< r4 r4 \mf c8 c ~
	c4 r4 r4 r4
	r4 c8 c -> ~ c2 \>
	
	\tag #'b \break
	\repeat volta 2 {
		r4 \p ^"(Soli 1st x)" \repeat unfold 11 { r4 }
		
		\tag #'b \break
		\repeat unfold 12 { r4 }
	}
	\alternative {
		{ \tag #'b \break
		  \repeat unfold 8 { r4 } }
		{ \repeat unfold 4 { r4 }
		  r4 r4 \of r8 c8 -> ~ c4
		}
	}
	
	\tag #'b \break
	\repeat unfold 16 { r4 }
	
	\tag #'b \break
	\repeat unfold 8 { r4 }
	\of \times 2/3 { r4 \mf c -- c -- } c -^ r
	\of \times 2/3 { r4 c -- c -- } c -^ r
	
	\tag #'b \break
	\of r8 c c4 r r
	\repeat unfold 8 { r4 }
	c8 c -> ~ c c r4 r4
	
	\tag #'b \break
	\repeat unfold 8 { r4 }
	r4 r4 r4 \< r4
	\of r4 \f c8 c -> ~ c2 \>
	
	\tag #'b \break
	r4 \sp _"cresc. - - -" \repeat unfold 11 { r4 }
	
	\tag #'b \break
	\repeat unfold 8 { r4 }
	\of r8 c -. \of r \f c ~ c4 r
	
	\tag #'b \break
	\of \times 2/3 { r4 c -- c -- } c -^ r
	\of \times 2/3 { r4 c -- c -- } c -^ r
	\of \times 2/3 { r4 c -- c -- } r r
	
	\tag #'b \break
	r4 \f c8 c -> ~ c2
	c8 c -> ~ c2.
	\of r4 c -^ c8 c -> ~ c4
	r4 \of r4 \of r2
	R1
	
	\tag #'b \break
	\repeat volta 2 {
		r4 \p \repeat unfold 11 { r4 }
		
		\tag #'b \break
		\repeat unfold 12 { r4 }
	}
	\alternative {
		{ \tag #'b \break
		  \repeat unfold 8 { r4 } }
		{ r4 \< r4 r4 r4
		  c4 -^ \f c4 -^ c4 -^ c4 -^ }
	}
	
	\tag #'b \break
	r4 \f \repeat unfold 15 { r4 }
	
	\tag #'b \break
	\repeat unfold 4 { r4 }
	c2 \< r4 r4 
	\of \times 2/3 { r4 \f c -- c -- } c -^ r
	\of \times 2/3 { r4 c -- c -- } c -^ r
	
	\tag #'b \break
	\repeat unfold 16 { r4 }
	
	\tag #'b \break
	\repeat unfold 12 { r4 }
	r4 c8 c ~ c4 r
	
	\tag #'b \break
	\repeat unfold 12 { r4 }
	r4 c8 c ~ c2
	
	\tag #'b \break
	c2 \rit c2 
	c2 c2
	c1 \! \fermata
	R1
	
	\tag #'b \break
	r4 \p ^"(Soli)" \repeat unfold 15 { r4 }
	
	\tag #'b \break
	r4 ^"(Ensemble)" \repeat unfold 15 { r4 }
	
	\tag #'b \break
	\repeat unfold 16 { r4 }
	
	\tag #'b \break
	\repeat unfold 16 { r4 }
	
	\tag #'b \break
	\repeat unfold 16 { r4 }
	
	c2 c2 \fermata
	
	\bar "|."
}


drumsUp = \drummode {
  	
	\revert RestCollision.positioning-done
	\override Rest.staff-position = #6
	
	\dynamicUp
	
	\partial 2
	s2
	
	\teeny r4 dd8 dd -> ~ dd2
	dd8 dd -> ~ dd2.
	\of r4 dd -^ dd8 dd -> ~ dd4
	
	\tag #'b \break
	dd4 -^ r4 r2
	s1*3
	
	\tag #'b \break
	s1*10
	
	\tag #'b \break
	s1*6
	\times 2/3 { r4 dd -- dd -- } dd-^ r4
	\times 2/3 { r4 dd -- dd -- } dd-^ r4
	
	\tag #'b \break
	s1*3
	r8 dd -^ r \fill { dd -> ~ dd2 }
	
	s1
	r8 dd -^ s4 dd4. dd8 -> ~
	dd2 dd4 -- r
	r4 r8 dd -> ~ dd4 r4
	
	\tag #'b \break
	s1*9
	r2 r8 dd -> ~ dd4
	
	\tag #'b \break
	s1*6
	\times 2/3 { r4 dd -- dd -- } dd-^ r4
	\times 2/3 { r4 dd -- dd -- } dd-^ r8 dd -.
	
	\tag #'b \break
	r4 r8 dd -. r2
	s1*2
	r8 dd -> ~ dd4 dd4. dd8 -^
	
	s1*3
	r4 dd8 dd -> ~ dd4 r
	
	\tag #'b \break
	s1*3
	r4 r8 dd -> ~ dd4 dd8 dd8 -.
	r4 r8 dd -> ~ dd4 dd8 dd8 -.
	r4 r8 dd -> ~ dd4 r4
	
	\tag #'b \break
	\times 2/3 { r4 dd -- dd -- } dd-^ r4
	\times 2/3 { r4 dd -- dd -- } dd-^ r4
	\times 2/3 { r4 dd -- dd -- } dd-^ r4
	
	dd4 -- dd8 dd -> ~ dd2
	dd8 dd -> ~ dd2.
	\of r4 dd -^ dd8 dd -> ~ dd4
	dd4 -^ s4 s2
	s1
	
	\tag #'b \break
	s1*10
	
	\tag #'b \break
	s1*3
	r8 dd -> ~ dd4 dd8 dd -^ r dd -^
	s1
	s2 dd4 -^ dd -^
	\times 2/3 { r4 dd -- dd -- } dd-^ r4
	\times 2/3 { r4 dd -- dd -- } dd-^ r4
	
	\tag #'b \break
	r8 dd -> r4 r2
	r8 dd -> r4 r4 r8 dd -> 
	s1
	r4 r8 dd -> ~ dd4 r
	
	r8 dd -> r4 r2
	r8 dd -> r4 r4 r8 dd -> 
	r2 r4 r8 dd ->
	s1
	
	\tag #'b \break
	s1*8
	
	\tag #'b \break
	s1*8
	
	\tag #'b \break
	s1*5
	
	\bar "|."
}

drumsDown = \drummode {

	\global
	
	\stemDown
	\override MultiMeasureRest.staff-position = 2
	\override Stem.extra-offset = #'(.05 . 0)
	\override Beam.extra-offset = #'(.05 . 0)
	
	\improOn
	\partial 2
	\fill { r4 \< r4 }
	
	r4 \f r4 r4 r4
	r4 r4 \fill { r4 r4 }
	r4 r4 r4 r4
	r4 \mf r4 r4 r4
	r4 r4 r4 r4
	r4 \mp r4 r4 r4
	r4 r4 r4 \> r4 \!
	
	\repeat volta 2 {
		\repeat percent 6 { r4 \p ^"(Tacet 1st x)" r4 r4 r4 }
	}
	\alternative {
		{ R1*2 }
		{ \repeat percent 2 { r4 r4 r4 r4 } }
	}
	
	\repeat percent 5 { r4 \p r4 r4 r4 }
	\fill { r4 \< r4 r4 r4 }
	r4 \mf r4 \fill { r4 r4 }
	r4 r4 \fill { r4 r4 }
	
	
	<< {
		\improOn
		\override Rest.staff-position = 0
		\repeat percent 8 { r4 \mf r4 r4 r4 } 
		
	   } \\ { 
	   s1*7 s2 s4 \> s4 \! } >>
	
	\repeat volta 2 {
		\repeat percent 6 { r4 \p r4 r4 r4 }
	}
	\alternative {
		{ \repeat percent 2 { r4 r4 r4 r4 } }
		{ \repeat percent 2 { r4 r4 r4 r4 } }
	}
	
	\repeat percent 5 { r4 r4 r4 r4 }
	\fill { r4 \< r4 r4 r4 }
	r4 \mf r4 \fill { r4 r4 }
	r4 r4 \fill { r4 r4 }
	
	\set countPercentRepeats = ##f
	\repeat percent 4 { r4 r4 r4 r4 } 
	r4 \sp \< r4 r4 r4
	r4 r4 r4 r4
	r4 \mf r4 \fill { r4 r4 }
	r4 r4 \fill { r4 \> r4 }
	
	\set countPercentRepeats = ##t
	\override PercentRepeatCounter.direction = #DOWN
	\repeat percent 6 { r4 \pp _"cresc. - - -" r4 r4 r4 }
	r4 \f r4 \fill { r4 r4 }
	r4 r4 \fill { r4 r4 }
	r4 r4 \fill { r4 r4 }
	\revert PercentRepeatCounter.direction
	
	\repeat percent 3 { r4 \f r4 r4 r4 }
	r4 \of r \of r2
	R1
	
	\repeat volta 2 {
		\repeat percent 6 { r4 ^"(Tacet 1st x)" r4 r4 r4 }
	}
	\alternative {
		{ R1*2 }
		{ r4 \< r4 r4 r4
		  \improOff sn16 \f sn r8 sn16 sn r8 sn16 sn r8 sn16 sn r8 \improOn
		}
	}
	
	\override PercentRepeatCounter.direction = #DOWN
	\repeat percent 5 { r4 \f r4 r4 r4 }
	\fill { r4 \< r4 r4 r4 }
	r4 \f r4 \fill { r4 r4 }
	r4 r4 \fill { r4 r4 }
	\revert PercentRepeatCounter.direction
	
	\repeat percent 3 { r4 r4 r4 r4 }
	\fill { r4 r4 r4 r4 }
	\repeat percent 3 { r4 r4 r4 r4 }
	\fill { r4 r4 r4 r4 }
	
	\repeat percent 3 { r4 r4 r4 r4 }
	r4 r4 \fill { r4 r4 }
	dd2 \rit dd2
	dd2 dd2
	dd1 \! \fermata
	R1
	
	R1*4
	\repeat percent 4 { r4 r4 r4 r4 }
	
	\repeat percent 8 { r4 r4 r4 r4 }
	
	\repeat percent 4 { r4 r4 r4 r4 }
	
	dd2 dd2 \fermata
	
	\bar "|."
}

drumsRepeats = \new DrumVoice \with { \RemoveEmptyStaves } {
	% Oculta el primer pentagrama del markup
	\override Staff.VerticalAxisGroup #'remove-first = ##t
		
}

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~


\book {
	\paper { 
		#(set-paper-size "a4" 'landscape)
		indent = 20
		systems-per-page = 1
	}
	
	\score {
		% \midi { \tempo 4 = 110 }
		
		\removeWithTag #'b
		<<
			\scoreMarkup
			\new StaffGroup <<
				\new Staff \with { instrumentName = "Trumpet" midiInstrument = #"trumpet" } { \trumpet }
				\new Staff \with { instrumentName = "Alto Sax" midiInstrument = #"alto sax" } { \altoSax }
				\new Staff \with { instrumentName = "Tenor Sax" midiInstrument = #"tenor sax" } { \tenorSax }
				\new Staff \with { instrumentName = "Trombone" midiInstrument = #"trombone" } { \trombone }
			>>
			
			\scoreChords
			\new StaffGroup <<
				\new Staff \with { instrumentName = "Piano" } { \piano }
				\new Staff \with { instrumentName = "Guitar" } { \guitar }
				\new Staff \with { instrumentName = "Bass" } { \bass }
			>>
			
			\new DrumStaff <<
  				\set DrumStaff.drumStyleTable = #(alist->hash-table nwDrums)
				\new DrumVoice { \voiceOne \drumsUp }
				\new DrumVoice { \voiceTwo \drumsDown }
  				\drumsRepeats
			>>
		>>
	}
}

instrumentName = "Trumpet"
\book {
	\header { instrument = #instrumentName }
   \bookOutputName  #(string-append title " - " instrumentName)
   
	\score {
		\transpose bf c' 
		<<
			\removeWithTag #'sb \scoreMarkup
			\new StaffGroup <<
				\new Staff { \trumpet }
			>>
		>>
	}
}

instrumentName = "Alto Sax"
\book {
	\header { instrument = #instrumentName }
   \bookOutputName  #(string-append title " - " instrumentName)
   
	\score {
		\transpose ef c'
		<<
			\removeWithTag #'sb \scoreMarkup
			\new StaffGroup <<
				\new Staff { \altoSax }
			>>
		>>
	}
}

instrumentName = "Tenor Sax"
\book {
	\header { instrument = #instrumentName }
   \bookOutputName  #(string-append title " - " instrumentName)
   
	\score {
		\transpose bf c''
		<<
			\removeWithTag #'sb \scoreMarkup
			\new StaffGroup <<
				\new Staff { \tenorSax }
			>>
		>>
	}
}

instrumentName = "Trombone"
\book {
	\header { instrument = #instrumentName }
   \bookOutputName  #(string-append title " - " instrumentName)
   
	\score {
		<<
			\removeWithTag #'sb \scoreMarkup
			\new StaffGroup <<
				\new Staff { \trombone }
			>>
		>>
	}
}

instrumentName = "Piano"
\book {
	\header { instrument = #instrumentName }
   \bookOutputName  #(string-append title " - " instrumentName)
   
	\score {
		<<
			\removeWithTag #'sb \scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\new Staff { \piano }
			>>
		>>
	}
}

instrumentName = "Guitar"
\book {
	\header { instrument = #instrumentName }
   \bookOutputName  #(string-append title " - " instrumentName)
   
	\score {
		<<
			\removeWithTag #'sb \scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\new Staff { \guitar }
			>>
		>>
	}
}

instrumentName = "Bass"
\book {
	\header { instrument = #instrumentName }
   \bookOutputName  #(string-append title " - " instrumentName)
   
	\score {
		<<
			\removeWithTag #'sb \scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\new Staff { \bass }
			>>
		>>
	}
}

instrumentName = "Drums"
\book {
	\header { instrument = #instrumentName }
   \bookOutputName  #(string-append title " - " instrumentName)
   
	\score {
		<<
			\removeWithTag #'sb \scoreMarkup
			\new DrumStaff <<
  				\set DrumStaff.drumStyleTable = #(alist->hash-table nwDrums)
				\new DrumVoice { \voiceOne \drumsUp }
				\new DrumVoice { \voiceTwo \drumsDown }
  				\drumsRepeats
			>>
		>>
	}
}