\version "2.18.2"


% Descripción de funcionalidades
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% PLUGINS
% - \eolMark:					mark al final de línea
% - \bendBefore: 				solución al bendBefore (i.e. dis8 \bendBefore)
% - \rit: 						crea una porción de ritardando que debe terminar como las dinámicas, con un \!
% - \improOn, \improOff:	negra de improvisación
% - \emptyStaff:				Imprime un pentagrama vacío
% - \emptyBar:					Imprime un compás invisible (útil para indentados)
% - \glissBefore:				muestra un glissando hacia la nota
% - \shortGlissBefore:		muestra un glissando corto hacia la nota
% - \jazzTurn:					muestra el simbolo equivalente a un turn en jazz
% - \subp:						muestra "sp", subito piano
% - \ap:							(accidental padding), separa un poco una alteración de la siguiente nota
% 									(útil al usar parenthesize)
% - \optOttava:				octavaje opcional

% LAYOUT
% - El aspecto de los silencios multicompás es como los de las partituras de jazz
% - Margen extra de las articulaciones para evitar la colisión con las ligaduras
% - Glissandos bonitos

% PAPER
% - Se imprime el título, instrumento y número de página en todas las páginas
% - Solo se debe tocar el page-count (si eso). Sale bonito tal cual.

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Changelog de funcionalidades
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Plugins
% 23-09-85: Añadida función \emptyStave para imprimir un pentagrama vacío.
% 				En algunas partituras hay un pentagrama vacío antes del coda (lo cual mola bastante, por cierto)
% 05-09-14: Añadido a la función \emptyStave el esconder la armadura de la clave, que no estaba.
% 10-09-14: Mejorada la función \bendBefore
% 				Ahora es genérica, y se puede definir cualquier texto en un lateral que haga las funciones de ornamento
% 16-09-14: Mejorada la función para imprimir barras de improvisación.
% 				Antes se llamaba al comando \rs para pintar una barra que ocupaba una negra
% 				Se reemplaza \rs por \restImproOn y \restImproOff, siendo esta opción mucho más flexible.
% 				Cuando se activa \restImproOn, todo silencio se convierte en barra de improvisación, respetando su duración.
% 20-09-14: Mejorada la función para imprimir barras de improvisación.
% 				Se reemplaza \restImproOn y \restImproOff por \improOn y \improOff
% 				Ahora, también las notas del pentagrama salen como barra de improvisación.
% 				(los comandos \improvisationOn e \improvisationOff de lilypond no se llevan bien con LilyJAZZ)
% 				Se ha añadido un grob correspondiente a los slash de la fuente LilyJAZZ en el archivo lily-jazz.ly
% 01-10-14: Plugins
% 				Renombrada la función \emptyStave a \emptyStaff (stave es el plural de staff) y 
% 				corregida (ya no muestra el número de compás).
% 				Recordar que después de usar \emptyStaff se necesita redefinir el 
% 				número de compás con \set Score.currentBarNumber = #xx
% 01-10-14: Añadida la función \emptyBar que oculta un compás entero. Se puede usar para crear un 
% 				efecto de indentación en un pentagrama dado.
% 				Recordar que después de usar \emptyBar se necesita:
% 				- redefinir el número de compás con \set Score.currentBarNumber = #xx
% 				- volver a pintar la clave con \clef
% 				- volver a pintar la armadura con \key
% 15-10-14: Se le ha aumentado el grosor de los bordes de la función \boxed
% 18-10-15: Añadidas funciones de ornamento para antes de la nota:
% 				- \glissBefore: para mostrar un glissando antes de una nota
% 				- \shortGlissBefore: para mostrar un glissando corto antes de una nota
% 				- \jazzTurn: para mostrar el turn típico trompetero de jazz
% 24-10-15: Añadida funcion \subp para mostrar subito piano "sp"
% 27-10-15: Añadida funcion \ap (de accidental padding) para separar un poco el accidental de la siguiente nota
% 				al usar un paréntesis con \parenthesize, ya que se solapan el uno al otro
% 27-10-15: Añadida funcion \optOttava para imprimir octavaje opcional
% 				También he aprovechado para cambiar la fuente del texto "8va" que utilizaba Feta por defecto
% 28-10-15: Mejorada las funciones glissBefore y shortGlissBefore.
% 				Ahora se le pasa una lista #'(r x y) en la que "r" es la rotacion, "x" la posicion horizontal 
%				e "y" la positión vertical.

% Markup
% 23-09-85: Añadidas guías para correcta posición y markup de la indicación de tempo
% 27-10-15: Guía para posición de la indicación de tempo mejorada

% Layout
% 23-09-85: Ahora las articulaciones van por dentro de las ligaduras. Parece que es la mejor solución
% 04-09-14: Los creciendos y decreciendos tienen la línea un poco más gruesa
% 08-09-14: Ahora no se muestra la cancelación de accidentales al haber un cambio de armadura
% 15-09-14: Añadido un poco más de margen a los reguladores
% 20-09-14: Comentada la línea que oculta la cancelación de accidentales al haber cambio de armadura.
% 				Realmente, la cancelación es muy bienvenida.

% Chords
% 09-09-14: Añadida sentencia para que los acordes menores salgan con un guión en vez de con una M minúscula
% 26-10-14: Añadido el include "chord-exception.ily" que recoge una mejor notación de los acordes.
% 				Actualmente, recoge los acordes maj7, 7(b5), 7(#5), 7(b9), 7(#9), 7(add13), maj9(add13), 9(#11)
% 				Se irá añadiendo acordes a menudo.

% Global
% 15-10-14: Todas las instrucciones que se colocaban justo antes de empezar la música están ahora
% 				recogidas en una función \global.
% 				A \global se le ha añadido instrucciones para que agrupe las corcheas de dos en dos
% 				en vez de cuatro en cuatro (el comportamiento por defecto).

% Language
% 15-10-14: Cambiado el lenguaje de entrada de notas del finlandes al inglés
% 				La diferencia es que ahora los sostenidos se escriben "-s" en vez de "-is"
% 				y los bemoles "-f" en vez de "-es".

% Estructura
% 15-10-14: Para poder autoincluir los archivos de instrumento en otros 
% 				(para montar un midi, o un score), se han movido los plugins a un archivo separado
% 				y ahora se utiliza la función \includeOnce, evitando así el problema de las
% 				dependencias circulares. 
% 18-10-15: Mejorada la manera de trabajar con papeles separados y score al mismo tiempo

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Dependencias
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% \includeOnce
% Incluye una dependencia solo una vez. Así se pueden incluir los archivos en otros 
% (para montar un score por ejemplo) sin liarla parda con dependencias cíclicas.

includeOnce = #(
	define-void-function (parser location filename) (string?) (
		if (not (defined? (string->symbol filename))) (
			begin (ly:parser-include-string parser (string-concatenate (list "\\include \"" filename "\"")))
			(primitive-eval (list 'define (string->symbol filename) #t))
		)
	)
)



% LilyJAZZ
\includeOnce "includes/ignatzek-jazz-chords.ily"
\includeOnce "includes/lily-jazz.ily"

% Language
\language "english"

% Plugins
\includeOnce "includes/plugins-1.2.ily"

% Chord Exceptions
\includeOnce "includes/chord-exceptions.ily"

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~











% Disposición
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Cabecera
\header {
	title = "More"
	subtitle = "(as performed by Frank Sinatra)"
	instrument = ""
	composer = "Riz Ortolani - Nino Oliviero"
	arranger = "Arr: Gabriel Palacios"
	poet = ""
	tagline = "gabriel.ps.ms@gmail.com"
}

% Tamaño de las notas y pentagrama
#(set-global-staff-size 20)

% Márgenes, fuentes y distancias del papel
\paper {
	% El definir otro tipo de fuente no permite cambiar el tamaño global del staff. Es un bug conocigo
	#(define fonts (make-pango-font-tree "LilyJAZZText" "Nimbus Sans" "Luxi Mono" (/ 20 20)))

	% distancia entre la parte superior de la página y el título
	top-markup-spacing #'basic-distance = 5

	% distancia entre el título y el primer sistema
	markup-system-spacing #'basic-distance = 25

	% distancia entre el texto y el primer sistema 
	top-system-spacing #'basic-distance = 15

	% márgen inferior
	bottom-margin = 20
	
	% intentado del primer sistema
	indent = 0
	 
	% número de páginas
	% page-count = 1

	ragged-last-bottom = ##f 

	oddHeaderMarkup = \markup {
	 \on-the-fly #not-first-page \fill-line {
		\fromproperty #'page:page-number-string
		\fromproperty #'header:title
		\fromproperty #'header:instrument
	 }
	}

	evenHeaderMarkup = \markup {
	 \on-the-fly #not-first-page \fill-line {
		\fromproperty #'header:instrument
		\fromproperty #'header:title
		\fromproperty #'page:page-number-string
	 }
	}
}

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~







% Elementos comunes de la partitura
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
	% Oculta el primer pentagrama del markup
	\override Staff.VerticalAxisGroup #'remove-first = ##t

	% Indicación de tiempo
	\once \override Score.RehearsalMark.self-alignment-X = #LEFT
	\once \override Score.RehearsalMark.extra-offset = #'(0 . 3) 
	\mark \markup "Medium Swing"

	% s1*2
	% \once \override Score.RehearsalMark.X-offset = #5
	% \mark \markup \boxed "A"
	
	s1*8
	\mark \markup \boxed "A"
	
	s1*8
	\mark \markup \boxed "B"
	
	s1*8
	\mark \markup \boxed "C"
	
	s1*8
	\mark \markup \boxed "D"
	
	s1*8
	\mark \markup \boxed "E"
	
	s1*8
	\mark \markup \boxed "F"
	
	s1*8
	\mark \markup \boxed "G"
	
	s1*8
	\mark \markup \boxed "H"
	
	s1*8
	\mark \markup \boxed "I"
	
	s1*8
	\mark \markup \boxed "J"
	
	s1*8
	\mark \markup \boxed "K"
}



% Función musical global
global = {
	% Para pintar todos los números de compás debajo del pentagrama
	% \override Score.BarNumber #'break-visibility = #end-of-line-invisible
	% \override Score.BarNumber #'direction = #DOWN
	% \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

	\jazzOn
	\compressFullBarRests

	% Arregla el problema de tener el scoreMarkup añadido
	\revert MultiMeasureRest #'staff-position
	\override Voice.Rest #'staff-position = #0	

	\set Timing.beamExceptions = #'()
	\set Timing.baseMoment = #(ly:make-moment 1/4)
	\set Timing.beatStructure = #'(1 1 1 1) 
	
	\override ParenthesesItem.font-size = #0
}

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Elementos específicos del instrumento
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Cifrado
scoreChords = \chords {    
	\set minorChordModifier = \markup { "-" }
	\set chordNameExceptions = #chordNameExceptions
	\override ChordName.font-size = #2.5

	% c1:m f2:m g2:m7
}

trumpetFills = \chords {    
	\set minorChordModifier = \markup { "-" }
	\set chordNameExceptions = #chordNameExceptions
	\override ChordName.font-size = #2.5

	s1*18
	a1:m7
	d1:7
	
	s1*2
	a1:m7
	b:7
	
	s1*42
	e1:m/d
	cs:m5-.7
	
	s1*4
	af1:6
	f:m7
	bf:m7
	ef:7
}

% Música
trumpet = \relative c'' {
  
	\global
	\key g \major
	
	R1*8
	R1*8
	
	\break
	R1*2 ^\markup \box "Harmon."
	\revert Score.RestCollision.positioning-done
	<< { \small r4 \mf ^ \markup \normalsize "(Lazy & free fill)" r8 a' \bendBefore -> ~ a4 \bendAfter -3  r
	r4 r8 d, \bendBefore -> ~ d4 \bendAfter -3  r } \\ { \override Rest.staff-position = #-6 \improOn \repeat unfold 8 { r4 } } >>
	
	\break
	R1*2
	<< { \small r2 ^ \markup \normalsize "(Lazy & free fill)" \times 2/3 { b'4 a af }
	g4 -. e8 -- fs -. r b, -. r g -. } \\ { \teeny r8 _"(saxos)" e b' c b g16 e c8 b -> ~
	b4 r b8 b16 ds fs8 g -^ } >>
   \override Score.RestCollision #'positioning-done = #merge-rests-on-positioning
	
	\break
	R1*8 ^\markup \box "Open"
	R1*7 
	r4 r8 \f bf' -> ~ bf a( g ) e -^
	
	\break
	R1*4
	r4 r8 cs -> \fp \< ~ cs4. d8 -> \f
	R1
	r8 \mf \< g -- g4 -. a8 -- a -.  r b -> ~
	b4 b16 -- b8 -. b16 -- c8 -- c -> r d -> \f
	
	\break
	R1*8
	
	\break
	d,4 ^"(Ensemble)" \mp \< -. r8 e -> ~ e4 -- fs4 -.
	r8 fs -> ~ fs4 ds8 e r g -> ~
	g8 e16 c b4 -. c8 -- g' -. r e -> ~
	e4. f8 -> ~ f4 fs4 -. \f
	
	\break
	r4 r8 as, \mp \< b -- e -. r4
	r4 b8 as b e -> ~ e4
	c8 b c d e g r b \f -> ~
	b2 \times 2/3 { bf8 a4 -- } g8 -- e ->
	
	\break
	R1*2 ^\markup \box "Harmon."
	\revert Score.RestCollision.positioning-done
	<< { \small r2 \mf ^ \markup \normalsize "(Lazy & free fill)" \times 2/3 { b4 e b }
	e4 -. r8 b8 e b a g -. } \\ { \override Rest.staff-position = #-6 \improOn \repeat unfold 8 { r4 } } >>
   \override Score.RestCollision #'positioning-done = #merge-rests-on-positioning
   
   \break
   R1 ^\markup \box "Open" 
	r2 r8 \f g'8 -> ~ g4
	a4 -^ r4 r2 ^\markup \box "Harmon."
	R1 
	
	\break
	\key af \major
	\revert Score.RestCollision.positioning-done
	<< { \small r2 \mf ^ \markup \normalsize "(Lazy & free fill)" f8 af \times 2/3 { af, f af ~ }
	af4 r4 r2
	r2 f'8 af \times 2/3 { af, f af ~ }
	af4. c8 bf4 -. r4
	} \\ { \override Rest.staff-position = #-6 \improOn \repeat unfold 16 { r4 } } >>
   \override Score.RestCollision #'positioning-done = #merge-rests-on-positioning
   
   \break
   R1*3 ^\markup \box "Open"
   r4 \f c8 d -> ~ d e f f ->
   
   \break
   R1*2
   r2 af4 -- \mf ef8 c' -> ~
   c2 ~ c8 bf af ef -> ~
   
   \break
   ef1
   R1
   r8 \f ef f f gf af bf df ~
   df4. d8 -^ r2
   
   \break
   R1*2
   
   r8 \f af, -> ~ af4 ef' -> ~ ef8 af, -> ~ 
   \break
   af4 ef' -> ~ ef8 af, -> ~ af4
   ef'8 ef r4 r2
   
   r4 \ff f2. \fermata
   
	\bar "|."
}

altoSax = \relative c' {
  
	\global
	\key g \major
	
	fs8 -- \mf fs -. r4 r4 fs8 -- fs -.
	r4 r8 ds -> ~ ds4 e -.
	b'8 -- b -. r4 r4 b8 -- b -.
	r4 r8 gs -> ~ gs4 a -.
	
	\break
	fs8 -- \mp fs -. r4 r4 fs8 -- fs -.
	r4 r8 ds -> ~ ds4 e -.
	b'8 -- b -. r4 r4 b8 -- b -.
	r4 r8 gs -> ~ gs4 a -.
	
	\break
	fs8 -- \pp fs -. r4 r4 fs8 -- fs -.
	r4 r8 ds -> ~ ds4 e -.
	b'8 -- b -. r4 r4 b8 -- b -.
	r4 r8 gs -> ~ gs4 a -.
	
	\break
	a8 -- a -. r4 r4 a8 -- a -.
	r4 r8 ds, -> ~ ds4 e -.
	b'8 -- b -. r4 r4 b8 -- b -.
	r4 r8 gs -> ~ gs4 a -.
	
	\break
	fs8 -- fs -. r4 r2
	R1*3
	
	\break
	R1*2
	r8 \mf e b' c b g16 e c8 b -> ~
	b4 r b8 b16 ds fs8 g -^
	
	\break
	R1
	r8 \p a -> ~ a2.
	b1
	r8 \mf b,16 d e8 b' \times 2/3 { a b a } g e -> \fp ~
	
	\break
	e1 \<
	R1 \!
	g8 -- g -. r4 r8 fs -> ~ fs4
	e8 -- e -. r4 r2
	
	\break
	d8 -- \mp d -. r4 r4 d8 -- d -.
	r4 r8 as -> ~ as4 b -.
	e8 -- e -. r4 r4 e8 -- ef -.
	r4 r8 es -> ~ es4 fs -.
	
	\break
	a8 -- a -. r4 r4 a8 -- a -.
	r4 r8 ds, -> ~ ds4 e -.
	b'8 -- b -. r4 r4 b8 -- b -.
	r4 r8 \f g' -> ~ g8 fs ( ds ) cs -^
	
	\break
	R1*4
	
	r8 g f e -> \fp \< ~ e4. a8 -^ \!
	R1
	r8 \mf \< d -- d4 -. c8 -- c -.  r e -> ~
	e4 e16 -- e8 -. e16 -- ef8 -- ef -> r d ( -> \f ~
	
	\break
	d4 ^ "(Straight 8's)" \glissando d,2 \mf e8 fs
	g4 fs g b 
	c,2. b'4
	a2. ) r4
	
	\break
	b2. ( c8 d
	e4 ds e g
	a,2. g'4 \bendBefore 
	fs2. \> ) r4 \!
	
	\break
	b,4 ^"(Ensemble)" -. \mp \< r8 as -> ~ as4 -- a4 -.
	r8 b -> ~ b4 as8 b r c -> ~
	c4 -- gs4 -. g8 -- c -. r c -> ~
	c4. c8 -> ~ c4 ds4 -. \f
	
	\break
	r4 r8 fs, \mp \< g -- b -. r4
	r4 gs8 g gs b -> ~ b4
	a8 gs a b c e r e \f -> ~
	e2 \times 2/3 { e8 ds4 -- } ds8 -- cs ->
	
	\break
	r4 \mf e8 --  bf8 -> \bendBefore ~ bf4 \bendAfter -5 r
	R1*3
	
	\break
	R1 
	r2 r8 \f df8 -> ~ df4
	e4 -^ r4 r2
	R1
	
	\break
	\key af \major
	g,8 -- \mp g -. r4 r4 g8 -- g -.
	r4 r8 e -> ~ e4 f -.
	c'8 -- c -. r4 r4 c8 -- c -.
	r4 r8 a -> ~ a4 bf -.
	
	\break
	bf8 -- bf -. r4 r4 bf8 -- af -.
	r4 r8 e -> ~ e4 f -.
	c'8 -- c -. r4 r2
   r4 \f f,8 af -> ~ af c d d ->
   
   \break
   R1*2
   r2 r4 r8 \mf e -> ~
   e2 r2
   
   \break
   r4 \mp af ( -> -\glissBefore #'(20 1 1) ~ af8 ef16 c bf8 af -> )
   R1
   r8 \f c df df c gf' gf af ~
   af4. af8 -^ r2
   
   \break
	r4 r8 \mp c,8 -> ~ c bf af ef
	R1
   
   r8 \f f -> ~ f4 g -> ~ g8 ef -> ~ 
   \break
   ef4 cf' -> ~ cf8 d, -> ~ d4
   f8 f r4 r2
   
   r4 \ff d'2. \fermata
	
	\bar "|."
}

tenorSax = \relative c' {
  
	\global
	\key g \major
	
	d8 -- \mf d -. r4 r4 d8 -- d -.
	r4 r8 as -> ~ as4 b -.
	g'8 -- g -. r4 r4 g8 -- fs -.
	r4 r8 es -> ~ es4 fs -.
	
	\break
	d8 -- \mp d -. r4 r4 d8 -- d -.
	r4 r8 as -> ~ as4 b -.
	g'8 -- g -. r4 r4 g8 -- fs -.
	r4 r8 es -> ~ es4 fs -.
	
	\break
	d8 -- \pp d -. r4 r4 d8 -- d -.
	r4 r8 as -> ~ as4 b -.
	g'8 -- g -. r4 r4 g8 -- fs -.
	r4 r8 es -> ~ es4 fs -.
	
	\break
	d8 -- d -. r4 r4 d8 -- d -.
	r4 r8 as -> ~ as4 b -.
	g'8 -- g -. r4 r4 g8 -- fs -.
	r4 r8 es -> ~ es4 fs -.
	
	\break
	d8 -- d -. r4 r2
	R1*3
	
	\break
	R1*2
	r8 \mf e b' c b g16 e c8 b -> ~
	b4 r b8 b16 ds fs8 g -^
	
	\break
	R1
	r8 \p ds -> ~ ds2.
	d?1
	r8 \mf b16 d e8 b' \times 2/3 { a b a } g e -> \fp ~
	
	\break
	e1 \<
	r4 \! fs,8 -- f -> ~ f4 e -.
	c'8 -- c -. r4 r8 d -> ~ d4
	c8 -- c -. r4 r2
	
	\break
	b8 -- \mp b -. r4 r4 b8 -- b -.
	r4 r8 g -> ~ g4 g -.
	c8 -- c -. r4 r4 c8 -- c -.
	r4 r8 cs -> ~ cs4 d -.
	
	\break
	d8 -- d -. r4 r4 d8 -- d -.
	r4 r8 as -> ~ as4 b -.
	g'8 -- g -. r4 r4 g8 -- fs -.
	r4 r8 \f cs' -> ~ cs8 c ( b ) g -^
	
	\break
	r2 e,4 -. \mf e8 -- ds -> ~
	ds1
	r2 d4 -. d8 -- cs -> ~
	cs1
	
	\break
	r8 e' d cs -> \fp \< ~ cs4. e8 -^ \!
	R1
	r8 \mf \< g -- g4 -. g8 -- g -.  r c -> ~
	c4 c16 -- c8 -. c16 -- c8 -- c -> r b ( -> \f
	
	\break
	b4 ^ "(Straight 8's)" \glissando b,2 \mf c8 d
	e4 ds e g 
	a,2. g'4
	fs2. ) r4
	
	\break
	d2. ( e8 fs
	g4 fs g a
	c,2. b'4 \bendBefore 
	a2. \> ) r4 \!
	
	\break
	fs4 ^"(Ensemble)" -.\mp \< r8 fs -> ~ fs4 -- fs4 -.
	r8 g -> ~ g4 g8 g r b-> ~
	b4 -- d,4 -. e8 -- g -. r g -> ~
	g4. a8 -> ~ a4 b4 -. \f
	
	\break
	r4 r8 ds, \mp \< e -- g -. r4
	r4 e8 ds f gs -> ~ gs4
	e8 ds e gs a b r c \f -> ~
	c2 \times 2/3 { cs8 c4 -- } b8 -- g ->
	
	\break
	r4 \mf e8 --  bf8 -> \bendBefore ~ bf4 \bendAfter -5 r
	R1*3
	
	\break
	R1 
	r2 r8 \f bf'8 -> ~ bf4
	c4 -^ r4 r2
	R1
	
	\break
	\key af \major
	ef,8 -- \mp ef -. r4 r4 ef8 -- ef -.
	r4 r8 cf -> ~ cf4 c -.
	af'8 -- af -. r4 r4 af8 -- g -.
	r4 r8 fs -> ~ fs4 g -.
	
	\break
	ef8 -- ef -. r4 r4 ef8 -- ef -.
	r4 r8 cf -> ~ cf4 c -.
	af'8 -- af -. r4 r2
   r4 \f df,8 f -> ~ f g f f ->
   
   \break
   r2 c4 -- \mp \glissando f,8 e -> ~
   e2 ~ e8 ef df c
   r2 r4 r8 \mf af'' -> ~
   af2 r2
   
   \break
   r4 \mp df2 ( -> -\glissBefore #'(20 1 1) g,8 f -> )
   R1
   r8 \f bf bf bf bf d ef ef ~
   ef4. d8 -^ r2
   
   \break
	r4 r8 \mp af8 -> ~ af g f df
	R1
   
   r8 \f d -> ~ d4 ef -> ~ ef8 c -> ~ 
   \break
   c4 gf' -> ~ gf8 cf, -> ~ cf4
   df?8 df r4 r2
   
   r4 \ff c'2. \fermata
	
	\bar "|."
}

trombone = \relative c' {
  
	\global
	\clef bass
	\key g \major
	
	b8 -- \mf b -. r4 r4 b8 -- b -.
	r4 r8 g -> ~ g4 g -.
	c8 -- c -. r4 r4 c8 -- c -.
	r4 r8 b -> ~ b4 c -.
	
	\break
	b8 -- \mp b -. r4 r4 b8 -- b -.
	r4 r8 g -> ~ g4 g -.
	c8 -- c -. r4 r4 c8 -- c -.
	r4 r8 b -> ~ b4 c -.
	
	\break
	b8 -- \pp b -. r4 r4 b8 -- b -.
	r4 r8 g -> ~ g4 g -.
	c8 -- c -. r4 r4 c8 -- c -.
	r4 r8 b -> ~ b4 c -.
	
	\break
	b8 -- b -. r4 r4 b8 -- b -.
	r4 r8 g -> ~ g4 g -.
	c8 -- c -. r4 r4 c8 -- c -.
	r4 r8 b -> ~ b4 c -.
	
	\break
	b8 -- b -. r4 r2
	R1*7
	R1*4
	
	\break
	r4 ^\markup \tiny \box "Plunger?" r8 \mf a, ^\markup "\"Pwah\"" -> \> ~ a2
	r4 ^\markup \tiny \box "Open?" \! fs'8 -- f -> ~ f4 e -.
	a8 -- a -. r4 r8 af -> ~ af4
	g8 -- g -. r4 r2
	
	\break
	fs8 -- \mp fs -. r4 r4 fs8 -- fs -.
	r4 r8 ds -> ~ ds4 e -.
	g8 -- g -. r4 r4 g8 -- fs -.
	r4 r8 gs -> ~ gs4 a -.
	
	\break
	b8 -- b -. r4 r4 b8 -- b -.
	r4 r8 g -> ~ g4 g -.
	c8 -- c -. r4 r4 c8 -- c -.
	r4 r8 \f e -> ~ e4 ds8 cs -^
	
	\break
	r2 e,4 -. \mf e8 -- ds -> ~
	ds1
	r2 d4 -. d8 -- cs -> ~
	cs1
	
	\break
	r8 bf' af g -> \fp \< ~ g4. c8 -^ \!
	R1
	r8 \mf \< e -- e4 -. ef8 -- ef -.  r g -> ~
	g4 g16 -- g8 -. g16 -- fs8 -- fs -> r e -> \f
	
	\break
	R1*8
	
	\break
	b4 ^"(Ensemble)" -. \mp \< r8 d -> ~ d4 -- d4 -.
	r8 d -> ~ d4 b8 d r g-> ~
	g4 -- c,4 -. a8 -- e' -. r d -> ~
	d4. d8 -> ~ d4 fs4 -. \f
	
	\break
	r4 r8 cs \mp \< d -- d -. r4
	r4 d8 cs d f -> ~ f4
	c8 b c d e g r fs \f -> ~
	fs2 \times 2/3 { g8 fs4 -- } fs8 -- e ->
	
	\break
	R1*5
	r2 r8 \f ef8 -> ~ ef4
	d4 -^ r4 r2
	R1
	
	\break
	\key af \major
	c8 -- \mp c -. r4 r4 c8 -- c -.
	r4 r8 af -> ~ af4 af -.
	df8 -- df -. r4 r4 df8 -- df -.
	r4 r8 c -> ~ c4 df -.
	
	\break
	c8 -- c -. r4 r4 c8 -- c -.
	r4 r8 af -> ~ af4 af -.
	df8 -- df -. r4 r2
   r4 \f c8 b -> ~ b bf af af ->
   
   \break
   r2 c4 -- \mp \glissando f,8 e -> ~
   e2 ~ e8 ef df c
   r2 r4 r8 \mf d -> ~
   d2 r2
   
   \break
   r4 \mp bf''2 ( -> -\glissBefore #'(20 1 1) d,8 df -> )
   R1
   r8 g g g gf bf c f, ~
   f4. b8 -^ r2
   
   \break
	r4 r8 \mp df,8 -> ~ df e df bf
	R1
   
   r8 \f c -> ~ c4 b -> ~ b8 bf -> ~ 
   \break
   bf4 bff -> ~ bff8 f -> ~ f4
   af8 af r4 r2
   
   r4 \ff af'2. \fermata
	
	\bar "|."
}

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

\book {
	\paper { 
		#(set-paper-size "a4" 'landscape)
		indent = 20
		systems-per-page = 2
	}
	
	\score {
		% \midi { \tempo 4 = 120 }
		\layout { #(layout-set-staff-size 20) }
		
		<<
			\scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\new Staff \with { instrumentName = "Trumpet" midiInstrument = #"trumpet" } << \trumpetFills \trumpet >>
				\new Staff \with { instrumentName = "Alto Sax" midiInstrument = #"alto sax" } { \altoSax }
				\new Staff \with { instrumentName = "Tenor Sax" midiInstrument = #"tenor sax" } { \tenorSax }
				\new Staff \with { instrumentName = "Trombone" midiInstrument = #"trombone" } { \trombone }
			>>
		>>
	}
}

\book {
	\header { instrument = "Trumpet" }
   \bookOutputName "More - Trumpet"
	\score {
		<<
			\scoreMarkup
			\transpose bf c \scoreChords
			\transpose bf c \trumpetFills
			\new StaffGroup <<
				\new Staff { \transpose bf c' \trumpet }
			>>
		>>
	}
}
\book {
	\header { instrument = "Alto Sax" }
   \bookOutputName "More - Alto Sax"
	\score {
		<<
			\scoreMarkup
			\transpose ef c \scoreChords
			\new StaffGroup <<
				\new Staff { \transpose ef c' \altoSax }
			>>
		>>
	}
}

\book {
	\header { instrument = "Tenor Sax" }
   \bookOutputName "More - Tenor Sax"
	\score {
		<<
			\scoreMarkup
			\transpose bf c \scoreChords
			\new StaffGroup <<
				\new Staff { \transpose bf c'' \tenorSax }
			>>
		>>
	}
}

\book {
	\header { instrument = "Trombone" }
   \bookOutputName "More - Trombone"
	\score {
		<<
			\scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\new Staff { \trombone }
			>>
		>>
	}
}