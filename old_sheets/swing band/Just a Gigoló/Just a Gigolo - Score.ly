\version "2.18.2"
\language "english"

\include "../.includes/drums.ily"
\include "../.includes/chords.ily"
\include "../.includes/lilyjazz.ily"
\include "../.includes/plugins-1.2.ily"


% Descripción de funcionalidades
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% PLUGINS
% - \eolMark:					mark al final de línea
% - \bendBefore: 				solución al bendBefore (i.e. dis8 \bendBefore)
% - \rit: 						crea una porción de ritardando que debe terminar como las dinámicas, con un \!
% - \improOn, \improOff:	negra de improvisación
% - \emptyStaff:				Imprime un pentagrama vacío
% - \emptyBar:					Imprime un compás invisible (útil para indentados)



% LAYOUT
% - El aspecto de los silencios multicompás es como los de las partituras de jazz
% - Margen extra de las articulaciones para evitar la colisión con las ligaduras
% - Glissandos bonitos



% PAPER
% - Se imprime el título, instrumento y número de página en todas las páginas
% - Solo se debe tocar el page-count (si eso). Sale bonito tal cual.

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Descripción de funcionalidades
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% 03-09-14: Layout
% Ahora las articulaciones van por dentro de las ligaduras. Parece que es la mejor solución



% 04-09-14: Layout
% Los creciendos y decreciendos tienen la línea un poco más gruesa
%
% Markup
% Añadidas guías para correcta posición y markup de la indicación de tempo
%
% Plugins
% Añadida función \emptyStave para imprimir un pentagrama vacío.
% En algunas partituras hay un pentagrama vacío antes del coda (lo cual mola bastante, por cierto)



% 05-09-14: Plugins
% Añadido a la función \emptyStave el esconder la armadura de la clave, que no estaba.



% 08-09-14: Layout
% Ahora no se muestra la cancelación de accidentales al haber un cambio de armadura



% 09-09-14: Chords
% Añadida sentencia para que los acordes menores salgan con un guión en vez de con una M minúscula



% 10-09-14: Plugins
% Mejorada la función \bendAfter.
% Ahora es genérica, y se puede definir cualquier texto en un lateral que haga las funciones de ornamento



% 15-09-14: Layout
% Añadido un poco más de margen a los reguladores



% 16-09-14: Plugins
% Mejorada la función para imprimir barras de improvisación.
% Antes se llamaba al comando \rs para pintar una barra que ocupaba una negra
% Se reemplaza \rs por \restImproOn y \restImproOff, siendo esta opción mucho más flexible.
% Cuando se activa \restImproOn, todo silencio se convierte en barra de improvisación, respetando su duración.



% 20-09-14: Plugins
% Mejorada la función para imprimir barras de improvisación.
% Se reemplaza \restImproOn y \restImproOff por \improOn y \improOff
% Ahora, también las notas del pentagrama salen como barra de improvisación.
% (los comandos \improvisationOn e \improvisationOff de lilypond no se llevan bien con LilyJAZZ)
% Se ha añadido un grob correspondiente a los slash de la fuente LilyJAZZ en el archivo lily-jazz.ly



% 20-09-14: Layout
% Comentada la línea que oculta la cancelación de accidentales al haber cambio de armadura.
% Realmente, la cancelación es muy bienvenida.



% 01-10-14: Plugins
% Renombrada la función \emptyStave a \emptyStaff (stave es el plural de staff) y 
% corregida (ya no muestra el número de compás).
% Recordar que después de usar \emptyStaff se necesita redefinir el 
% número de compás con \set Score.currentBarNumber = #xx
%
% Añadida la función \emptyBar que oculta un compás entero. Se puede usar para crear un 
% efecto de indentación en un pentagrama dado.
% Recordar que después de usar \emptyBar se necesita:
% - redefinir el número de compás con \set Score.currentBarNumber = #xx
% - volver a pintar la clave con \clef
% - volver a pintar la armadura con \key



% 15-10-14: Language
% Cambiado el lenguaje de entrada de notas del finlandes al inglés
% La diferencia es que ahora los sostenidos se escriben "-s" en vez de "-is"
% y los bemoles "-f" en vez de "-es".
%
% Estructura
% Para poder autoincluir los archivos de instrumento en otros 
% (para montar un midi, o un score), se han movido los plugins a un archivo separado
% y ahora se utiliza la función \includeOnce, evitando así el problema de las
% dependencias circulares.
%
% Global
% Todas las instrucciones que se colocaban justo antes de empezar la música están ahora
% recogidas en una función \global.
% A \global se le ha añadido instrucciones para que agrupe las corcheas de dos en dos
% en vez de cuatro en cuatro (el comportamiento por defecto).
%
% Plugins
% Se le ha aumentado el grosor de los bordes de la función \boxed



% 26-10-14: Chords
% Añadido el include "chord-exception.ily" que recoge una mejor notación de los acordes.
% Actualmente, recoge los acordes maj7, 7(b5), 7(#5), 7(b9), 7(#9), 7(add13), maj9(add13), 9(#11)
% Se irá añadiendo acordes a menudo.
 

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~






% Disposición
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

title = "Just a Gigolo"

% Cabecera
\header {
	title = #title
	instrument = "(as performed by Louis Prima)"
	composer = "Irving Caesar (from Austrian tango by Leonello Casucci)"
	arranger = "Arr: Gabriel Palacios"
	poet = ""
	tagline = "gabriel.ps.ms@gmail.com"
}

% Márgenes, fuentes y distancias del papel
\paper {

	% distancia entre la parte superior de la página y el título
	top-markup-spacing #'basic-distance = 5

	% distancia entre el título y el primer sistema
	markup-system-spacing #'basic-distance = 25

	% distancia entre el texto y el primer sistema 
	top-system-spacing #'basic-distance = 15

	% márgen inferior
	bottom-margin = 20
	
	% intentado del primer sistema
	indent = 0
	 
	% número de páginas
	% page-count = 1

	ragged-last-bottom = ##f 

	oddHeaderMarkup = \markup {
	 \on-the-fly #not-first-page \fill-line {
		\fromproperty #'page:page-number-string
		\fromproperty #'header:title
		\fromproperty #'header:instrument
	 }
	}

	evenHeaderMarkup = \markup {
	 \on-the-fly #not-first-page \fill-line {
		\fromproperty #'header:instrument
		\fromproperty #'header:title
		\fromproperty #'page:page-number-string
	 }
	}
}

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~







% Elementos comunes de la partitura
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
	% Oculta el primer pentagrama del markup
	\override Staff.VerticalAxisGroup #'remove-first = ##t
	
	% Indicación de tiempo
	\once \override Score.RehearsalMark.self-alignment-X = #LEFT
	\once \override Score.RehearsalMark.extra-offset = #'(0 . 3)
	\mark "Medium Swing"
	s1*4
	
	\tag #'sb \break
	\mark \markup "(x2)"
	s1*8
	
	\tag #'sb \break
	s1*8
	
	\tag #'sb \break
	\once \override Score.RehearsalMark.self-alignment-X = #LEFT
	\mark \markup {
		\column { \boxed "A" }
		\column { "(x2)" }
	}
	s1*6
	
	\tag #'sb \break
	s1*6
	
	\tag #'sb \break
	\mark \markup \boxed "B"
	s1*8
	
	\tag #'sb \break
	\mark \markup \boxed "C"
	s1*8
	
	\once \override Score.RehearsalMark.self-alignment-X = #LEFT
	\mark \markup {
		\column { \boxed "D" }
		\column { "(Tenor Sax Solo)" }
	}
	s1*8
	
	\tag #'sb \break
	s1*8
	
	\tag #'sb \break
	\once \override Score.RehearsalMark.self-alignment-X = #LEFT
	\mark \markup {
		\column { \boxed "E" }
		\column { "(Trombone Solo)" }
	}
	s1*8
	
	\tag #'sb \break
	\mark \markup \boxed "F"
	s1*8
	
	\tag #'sb \break
	\once \override Score.RehearsalMark.self-alignment-X = #LEFT
	\mark \markup {
		\column { \boxed "G" }
		\column { "(x2)" }
	}
	s1*6
	
	\tag #'sb \break
	s1*6
	
	\tag #'sb \break
	\mark \markup \boxed "H"
	s1*6
	
	\tag #'sb \break
	s1*6
	
	\tag #'sb \break
	\mark \markup \boxed "I"
	s1*8
	
	\tag #'sb \break
	s1*8
}

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Elementos específicos del instrumento
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Cifrado
scoreChords = \chords {    
	\set minorChordModifier = \markup { "-" }
	\set chordNameExceptions = #chordNameExceptions
	\override ChordName.font-size = #2.5

	af1:6 
	bf2:m7 ef:7
	af1:6 
	bf2:m7 ef:7
	
	af1:6 
	af1:maj7
	af2:6 b:dim
	bf:m7 ef:7
	
	bf:m7 ef:7
	bf:m7 ef:7
	ef1:7
	af:6
	
	af:6
	gf:6
	f:7
	bf:m7
	
	bf2:m7 df:m7
	c:m7 b:m7
	bf:m7 ef:7
	af1:6
	
	af1:7
	af4:7 g:7 gf:7 f:7
	f1:9
	s1
	
	bf1:m7
	ef:7
	af:6
	bf2:m7 ef:7
	
	bf1:6
	s1
	bf:m7
	ef:7
	
	af1:6
	s1
	df:6
	s1
	
	f:7
	s1
	bf:m7
	s1
	
	af1:7
	af4:7 g:7 gf:7 f:7
	f1:9
	s1
	
	bf1:m7
	ef:7
	af:6
	bf2:m7 ef:7
	
	% Sax solo
	af1:6
	s1
	f1:9
	s1
	
	bf1:6
	ef:7
	af2:6 f:7
	bf2:m7 ef:7
	
	af1:6
	s1
	f1:9
	s1
	
	bf1:6
	s1
	bf1:m7
	ef:7
	
	% Trombone solo
	af1:6
	s1
	df:6
	s1
	
	f:7
	s1
	bf:m7
	ef:7
	
	af1:6
	s1
	f1:9
	s1
	
	bf1:m7
	bf2:m7 ef:7
	af1:6
	s1
	
	af1:7
	af4:7 g:7 gf:7 f:7
	f1:9
	s1
	
	bf1:m7
	ef:7
	af:6
	bf2:m7 ef:7
	
	bf1:6
	s1
	bf:m7
	ef:7
	
	af1:6
	s1
	df:6
	s1
	
	f:7
	s1
	bf1:m7
	bf2:m7 ef:m7
	
	af1:6
	s1
	f:7
	s1
	
	% Coda
	bf1:m7
	ef:7
	af:6
	f:7
	
	bf1:m7
	ef:7
	af:6
	f:7
	
	bf1:m7
	ef:7
	af:6
	f:7
	
	bf1:m7
	ef:7
	s1*2
}

tenorSaxSoloChords = \chords {    
	\set minorChordModifier = \markup { "-" }
	\set chordNameExceptions = #chordNameExceptions
	\override ChordName.font-size = #2.5

	s1*48
	
	af1:6
	s1
	f1:9
	s1
	
	bf1:6
	ef1:7
	af2:6 f:7
	bf:m7 ef:7
	
	af1:7
	s1
	f1:9
	s1
	
	bf1:6
	s1
	bf1:m7
	ef1:7
}

tromboneSoloChords = \chords {    
	\set minorChordModifier = \markup { "-" }
	\set chordNameExceptions = #chordNameExceptions
	\override ChordName.font-size = #2.5

	s1*64
	
	af1:6
	s1
	df1:6
	s1
	
	f1:7
	s1
}

% Música
trumpet = \relative c'' {
  
	\global
	\key af \major
  
	R1*4
	
	\repeat volta 2 {
  		R1*8
  		
  		af1 (\p \<
  		gf2. ) r4 \!
  		f1 \mf \> ~ 
  		f2 r4 \! r8 f ( \mp
  		
  		\tag #'b \break
  		f -- ) f -.  r f ( e -- ) e -. r e (
  		ef? -- ) ef -. r ef ( d -- ) d -. r d --
  		df? -. r8 r4 r2
  		R1
	}
	
  	\tag #'b \break
	\repeat volta 2 {
  		af'1 \mf ~
  		af4 -- g -- gf -- f --
  		f2. -> \sfp \< ~ f8 c' -^ \f \bendAfter -3
  		R1
	}
	\alternative {
   	{ \tag #'b \break
   	  R1*4 }
   	{ R1*3
   	  r8 \f af' -> r8 af -> ~ af4 f8 -> r } 
	}
	
	\tag #'b \break
	\repeat percent 4 { r4 r8 af -> ~ af4 f8 -> r }
	
	\tag #'b \break
	\repeat percent 2 { r4 r8 g -> ~ g4 f8 -> r }
	af8 -^ r8 r4 r2
	R1
	
	\tag #'b \break
	af,1 \mf ~
	af4 -- g -- gf -- f --
	f2. \sfp \< ~ f8 c' -^ \f
	R1
	
	R1*4
	
	\tag #'b \break
	R1*8
	R1*8
	
	\tag #'b \break
	R1*4
	R1
	r2 r4 r8 \mf c16 ( c
	c4 -> ) \bendAfter -5 r8 c16 ( c c4 -> ) \bendAfter -5 r8 c16 ( c
	c4 -> ) \bendAfter -5 \< r8 c16 ( c c4 -> ) \bendAfter -5 r4
	
	\tag #'b \break
	\repeat percent 2 {
  		c8 ^ "(Shout)" -- \f \bendBefore r c8 -- r c8 \bendAfter -5 r r c --
  		r8 c -- r8 c ( c -- ) [ c ( ] c -. ) r8
	}

	\tag #'b \break
	c8 -- r c8 -- r c8 \bendAfter -5 r r c --
	r8 c -- r8 c ( bf -- ) [ bf ( ] c -. ) r8
	R1*2
	
  	\tag #'b \break
	\repeat volta 2 {
  		\times 2/3 { af'4 \f af \bendBefore af \bendBefore } \times 2/3 { af \bendBefore af \bendBefore af \bendBefore }
  		af4 -- g -- gf -- f --
  		f2. -> \sfp \< ~ f8 c' -^ \f \bendAfter -3
  		R1
	}
	\alternative {
   	{ \tag #'b \break
   	  R1*4 }
   	{ R1*3
   	  r8 \f af -> r8 af -> ~ af4 f8 -> r }
	}
	
	\tag #'b \break
	\repeat percent 4 { r4 r8 af -> ~ af4 f8 -> r }
	
	\tag #'b \break
	\repeat percent 2 { r4 r8 g -> ~ g4 f8 -> r }
	r8 f8 -> \bendAfter -5 r4 r8 f8 -> \bendAfter -5 r4
	r8 f8 -> \bendAfter -5 r4 r8 af8 -> \bendAfter -5 r4
	
	\tag #'b \break
	r8 f8 -> \bendAfter -5 r4 r8 f8 -> \bendAfter -5 r4
	r8 f8 -> \bendAfter -5 r4 r8 af8 -> \bendAfter -5 r4
	r8 f8 -> \bendAfter -5 r4 r8 f8 -> \bendAfter -5 r4
	r8 f8 -> \bendAfter -5 r4 r8 f8 -> ~ f4 \bendAfter -5
	
	\tag #'b \break
	R1*8
	R1*6
	
	r4 \f \times 2/3 { af,8 ( bf c } ef -. ) df ( af -. ) bf (
	c8 -. ) r ef af -> ~ af2 \fermata
	
	\bar "|."
}

altoSax = \relative c' {
  
	\global
	\key af \major
	
	R1*4
	
	\repeat volta 2 {
  		R1*8
  		
  		f1 ( \p \<
  		ef2. ) r4 \!
  		ef1 \mf \>
  		f2 r4 \! r8 df ( \mp
  		
  		\tag #'b \break
  		df -- ) df -. r df ( df -- ) df -. r df (
  		c -- ) c -. r c ( b -- ) b -. r b --
  		bf? -. r8 r4 r2
  		R1
	}
	
  	\tag #'b \break
	\repeat volta 2 {
  		ef1 \mf ~
  		ef4 -- d -- df -- c --
  		d2. -> \sfp \< ~ d8 g -^ \f \bendAfter -3
  		R1
	}
	\alternative {
		{ \tag #'b \break
		  R1*4 }
   	{ R1*3
   	  r8 \f af -> r8 af -> ~ af4 f8 -> r } 
	}
	
	\tag #'b \break
	\repeat percent 4 { r4 r8 af -> ~ af4 f8 -> r }
	
	\tag #'b \break
	\repeat percent 2 { r4 r8 g -> ~ g4 f8 -> r }
	af8 -^ r8 r4 r2
	R1
	
	\tag #'b \break
  	ef1 \mf ~
  	ef4 -- d -- df -- c --
  	d2. -> \sfp \< ~ d8 g -^ \f
  	R1
	
	R1*4
	
	\tag #'b \break
	R1*8
	
	R1*7
	r4 r8 \mf af -> ~ af4 f8 -> r
	
	\tag #'b \break
	\repeat percent 4 { r4 r8 af -> ~ af4 f8 -> r }
	
	\tag #'b \break
	r4 r8 g -> ~ g4 f8 -> r
	r4 r8 g -> ~ g4 f8 -> r
	r8 ^ "(Ensemble)" af8 -> \bendAfter -5 r4 r8 af8 -> \bendAfter -5 r4
	r8 \< bff8 -> \bendAfter -5 r4 r8 bf8 -> ~ bf4
	
	\tag #'b \break
  	af8 ^ "(Shout)" \f -- \bendBefore r af8 -- r af8 \bendAfter -5 r r af --
  	r8 af -- r8 af ( af -- ) [ af ( ] af -. ) r8
  	a8 -- r a8 -- r a8 \bendAfter -5 r r a --
  	r8 a -- r8 a ( a -- ) [ a ( ] a -. ) r8

	\tag #'b \break
	af8 -- r af8 -- r af8 \bendAfter -5 r r g --
	r8 g -- r8 g ( g -- ) [ g ( ] g -. ) r8
	R1*2
	
  	\tag #'b \break
	\repeat volta 2 {
  		\times 2/3 { ef'4 \f ef \bendBefore ef \bendBefore } \times 2/3 { ef \bendBefore ef \bendBefore ef \bendBefore }
  		ef4 -- e -- ef -- d --
  		c2. -> \sfp \< ~ c8 f -^ \f \bendAfter -3
  		R1
	}
	\alternative {
   	{ \tag #'b \break
   	  R1*4 }
   	{ R1*3
   	  r8 \f af, -> r8 af -> ~ af4 f8 -> r } 
	}
	
	\tag #'b \break
	\repeat percent 4 { r4 r8 af -> ~ af4 f8 -> r }
	
	\tag #'b \break
	\repeat percent 2 { r4 r8 g -> ~ g4 f8 -> r }
	r8 df'8 -> \bendAfter -5 r4 r8 df8 -> \bendAfter -5 r4
	r8 df8 -> \bendAfter -5 r4 r8 f8 -> \bendAfter -5 r4
	
	\tag #'b \break
	r8 ef8 -> \bendAfter -5 r4 r8 ef8 -> \bendAfter -5 r4
	r8 ef8 -> \bendAfter -5 r4 r8 f8 -> \bendAfter -5 r4
	r8 d8 -> \bendAfter -5 r4 r8 d8 -> \bendAfter -5 r4
	r8 c8 -> \bendAfter -5 r4 r8 f8 -> ~ f4 \bendAfter -5
	
	\tag #'b \break
	R1*8
	R1*6
	
	r4 \f \times 2/3 { af,8 ( bf c } ef -. ) df ( af -. ) bf (
	c8 -. ) r b c -> ~ c2 \fermata
	
	\bar "|."
}

tenorSax = \relative c' {
  
	\global
	\key af \major
  
	R1*4
	
	\repeat volta 2 {
  		R1*8
  		
  		ef1 ( \p \<
  		df2. ) r4 \!
  		c1 ( \mf \>
  		df2 ) r4 \! r8 bf ( \mp
  		
  		\tag #'b \break
  		bf -- ) bf -. r bf ( cf -- ) cf -. r cf (
  		bf -- ) bf -. r bf ( a -- ) a -. r a --
  		af? -. r8 r4 r2
  		R1
	}
	
  	\tag #'b \break
	\repeat volta 2 {
  		c1 \mf ~
  		c4 -- b -- bf -- a --
  		a2. -> \sfp \< ~ a8 ef' -^ \f \bendAfter -3
  		R1
	}
	\alternative {
   	{ \tag #'b \break
   	  R1*4 }
   	{ R1*3
   	  r8 \f af -> r8 af -> ~ af4 f8 -> r } 
	}
	
	\tag #'b \break
	\repeat percent 4 { af,,4 -> \sfz r8 af'' -> ~ af4 f8 -> r }
	
	\tag #'b \break
	\repeat percent 2 { c,4 -> \sfz r8 g'' -> ~ g4 f8 -> r }
	af8 -^ r8 r4 r2
	R1
	
	\tag #'b \break
  	c,1 \mf ~
  	c4 -- b -- bf -- a --
  	a2. -> \sfp \< ~ a8 ef' -^ \f
  	R1
	
	R1*4
	
	\tag #'b \break
	\improOn
	\repeat unfold 16 { r4 }
	
	\tag #'b \break
	\repeat unfold 16 { r4 }
	
	\tag #'b \break
	\repeat unfold 16 { r4 }
	
	\tag #'b \break
	\repeat unfold 12 { r4 }
	r4 \improOff r8 \mf f ->  ~ f4 ef8 -> r
	
	\tag #'b \break
	\repeat percent 4 { af,,4 -> \sfz r8 af'' -> ~ af4 f8 -> r }
	
	\tag #'b \break
	\repeat percent 2 { c,4 -> \sfz r8 g'' -> ~ g4 f8 -> r }
	r8 ^ "(Ensemble)" ef8 -> \bendAfter -5 r4 r8 f8 -> \bendAfter -5 r4
	r8 \< gf8 -> \bendAfter -5 r4 r8 g8 -> ~ g4
	
	\tag #'b \break
	f8 ^ "(Shout)" \f -- \bendBefore r f8 -- r f8 \bendAfter -5 r r f --
	r8 f -- r8 f ( f -- ) [ f ( ] f -. ) r8
	
	g8 -- r g8 -- r g8 \bendAfter -5 r r g --
	r8 g -- r8 g ( g -- ) [ g ( ] f -. ) r8
	
	\tag #'b \break
	f8 -- r f8 -- r f8 \bendAfter -5 r r f --
	r8 f -- r8 f ( f -- ) [ f ( ] ef -. ) r8
	c,8 -^ r r4 r2
	R1
	
  	\tag #'b \break
	\repeat volta 2 {
  		\times 2/3 { c''4 \f c \bendBefore c \bendBefore } \times 2/3 { c \bendBefore c \bendBefore c \bendBefore }
  		c4 -- b -- bf -- a --
  		g2. -> \sfp \< ~ g8 a -^ \f \bendAfter -3
  		R1
	}
	\alternative {
   	{ \tag #'b \break
   	  R1*4 }
   	{ R1*3
   	  r8 \f af -> r8 af -> ~ af4 f8 -> r } 
	}
	
	\tag #'b \break
	\repeat percent 4 { af,,4 -> \sfz r8 af'' -> ~ af4 f8 -> r }
	
	\tag #'b \break
	\repeat percent 2 { c,4 -> \sfz r8 g'' -> ~ g4 f8 -> r }
	r8 c'8 -> \bendAfter -5 r4 r8 c8 -> \bendAfter -5 r4
	r8 bf8 -> \bendAfter -5 r4 r8 df8 -> \bendAfter -5 r4
	
	\tag #'b \break
	r8 c8 -> \bendAfter -5 r4 r8 c8 -> \bendAfter -5 r4
	r8 c8 -> \bendAfter -5 r4 r8 ef8 -> \bendAfter -5 r4
	r8 a,8 -> \bendAfter -5 r4 r8 a8 -> \bendAfter -5 r4
	r8 g8 -> \bendAfter -5 r4 r8 f8 -> ~ f4 \bendAfter -5
	
	\tag #'b \break
	R1*8 
	R1*6
	
	r4 \f \times 2/3 { af,8 ( bf c } ef -. ) df ( af -. ) bf (
	c8 -. ) r g' f -> ~ f2 \fermata
	
	\bar "|."
}

trombone = \relative c' {
  
	\global
	\clef bass
	\key af \major
	
	R1*4
	
	\repeat volta 2 {
  		R1*8
  		
  		c1 ( \p \<
  		bf2. ) r4 \!
  		a1 ( \mf \>
  		bf2 ) r4\! r8 af ( \mp
  		
  		\tag #'b \break
  		af -- ) af -. r af ( af -- ) af -. r af (
  		g -- ) g -. r g ( fs -- ) fs -. r fs --
  		f? -. r8 r4 r2
  		R1
	}
	
  	\tag #'b \break
	\repeat volta 2 {
  		gf1 \mf ~
  		gf4 -- f -- e -- ef --
  		ef2. -> \sfp \< ~ ef8 a -^ \f \bendAfter -3
  		R1
	}
	\alternative {
   	{ \tag #'b \break
   	  R1*4 }
   	{ R1*3
   	  r8 \f c -> r8 c -> ~ c4 af8 -> r } 
	}
	
	\tag #'b \break
	\repeat percent 2 { ef4 -> r8 c' -> ~ c4 af8 -> r }
	\repeat percent 2 { f4 -> r8 df' -> ~ df4 af8 -> r }
	
	\tag #'b \break
	f4 -> r8 d' -> ~ d4 c8 -> r
	f,4 -> r8 d' -> ~ d4 bf8 -> r
	df?8 -^ r8 r4 r2
	R1
	
	\tag #'b \break
  	gf,1 \mf ~
  	gf4 -- f -- e -- ef --
  	ef2. -> \sfp \< ~ ef8 a -^ \f
  	R1
	
	R1*4
	
	\tag #'b \break
	R1*8
	R1*8
	
	\tag #'b \break
	\improOn
	\repeat unfold 16 { r4 }
	
	\tag #'b \break
	\repeat unfold 8 { r4 }
	\improOff
	r8 ^ "(Ensemble)" \mf df8 -> \bendAfter -5 r4 r8 df8 -> \bendAfter -5 r4
	r8 \< df8 -> \bendAfter -5 r4 r8 df8 -> ~ df4
	
	\tag #'b \break
	ef8 ^ "(Shout)" \f -- \bendBefore r ef8 -- r ef8 \bendAfter -5 r r ef --
	r8 ef -- r8 ef ( ef -- ) [ ef ( ] ef -. ) r8
	
	ef8 -- r ef8 -- r ef8 \bendAfter -5 r r ef --
	r8 ef -- r8 ef ( ef -- ) [ ef ( ] ef -. ) r8
	
	\tag #'b \break
	df8 -- r df8 -- r df8 \bendAfter -5 r r df --
	r8 df -- r8 df ( df -- ) [ df ( ] ef -. ) r8
	af,,8 -^ r r4 r2
	R1
	
  	\tag #'b \break
	\repeat volta 2 {
  		\times 2/3 { gf''4 \f gf \bendBefore gf \bendBefore } \times 2/3 { gf \bendBefore gf \bendBefore gf \bendBefore }
  		gf4 -- f -- e -- ef --
  		ef2. -> \sfp \< ~ ef8 ef -^ \f \bendAfter -3
  		R1
	}
	\alternative {
   	{ \tag #'b \break
   	  R1*4 }
   	{ R1*3
   	  r8 \f c -> r8 c -> ~ c4 af8 -> r } 
	}
	
	\tag #'b \break
	\repeat percent 2 { ef4 -> r8 c' -> ~ c4 af8 -> r }
	\repeat percent 2 { f4 -> r8 df' -> ~ df4 af8 -> r }
	
	\tag #'b \break
	f4 -> r8 d' -> ~ d4 c8 -> r
	f,4 -> r8 d' -> ~ d4 c8 -> r
	r8 af'8 -> \bendAfter -5 r4 r8 af8 -> \bendAfter -5 r4
	r8 af8 -> \bendAfter -5 r4 r8 bf8 -> \bendAfter -5 r4
	
	\tag #'b \break
	r8 af8 -> \bendAfter -5 r4 r8 af8 -> \bendAfter -5 r4
	r8 af8 -> \bendAfter -5 r4 r8 c8 -> \bendAfter -5 r4
	r8 ef,8 -> \bendAfter -5 r4 r8 ef8 -> \bendAfter -5 r4
	r8 ef8 -> \bendAfter -5 r4 r8 f8 -> ~ f4 \bendAfter -5
	
	\tag #'b \break
	R1*8 
	R1*6
	
	r4 \f \times 2/3 { af,8 ( bf c } ef -. ) df ( af -. ) bf (
	c8 -. ) r ef af, -> ~ af2 \fermata
	
	\bar "|."
}

piano = \relative c' {
  
	\global
	\key af \major
	
	\improOn
	\of r8 \mf c \of r c \of r c \of r c
	\of r8 c \of r c \of r c \of r c
	\repeat unfold 8 { r4 }
	
	\tag #'b \break
	\repeat volta 2 {
		r4 \p \repeat unfold 15 { r4 }
		
		\tag #'b \break
		\repeat unfold 16 { r4 }
		
		\tag #'b \break
		\repeat unfold 16 { r4 }
		
		\tag #'b \break
		\repeat unfold 16 { r4 }
	}
	
	\tag #'b \break
	\repeat volta 2 {
		r4 \mf \repeat unfold 15 { r4 }
	}
	\alternative {
		{ \repeat unfold 8 { r4 }
		  \tag #'b \break
		  \repeat unfold 8 { r4 } }
		
		{ \repeat unfold 12 { r4 }
		  r4 \f r4 r4 r4 }
	}
	
	\tag #'b \break
	r4 \f \repeat unfold 15 { r4 }
	
	\tag #'b \break
	\repeat unfold 8 { r4 }
	r4 \improOff r4 r2
	R1
	
	\improOn
	\tag #'b \break
	r4 \mf \repeat unfold 15 { r4 }
	
	\tag #'b \break
	\repeat unfold 16 { r4 }
	
	% Alto sax solo
	\tag #'b \break
	r4 \mf \repeat unfold 15 { r4 }
	
	\tag #'b \break
	\repeat unfold 16 { r4 }
	
	\tag #'b \break
	\repeat unfold 16 { r4 }
	
	\tag #'b \break
	\repeat unfold 16 { r4 }
	
	% Bone Solo
	\tag #'b \break
	\repeat unfold 16 { r4 }
	
	\tag #'b \break
	\repeat unfold 12 { r4 }
	r4 \< r4 r4 r4
	
	\tag #'b \break
	r4 \f \repeat unfold 15 { r4 }
	
	\tag #'b \break
	\repeat unfold 8 { r4 }
	r4 \improOff r4 r2
	R1
	
	\improOn
	\tag #'b \break
	\repeat volta 2 {
		r4 \mf \repeat unfold 15 { r4 }
	}
	\alternative {
		{ \repeat unfold 8 { r4 }
		  \tag #'b \break
		  \repeat unfold 8 { r4 } }
		
		{ \repeat unfold 12 { r4 }
		  r4 \f r4 r4 r4 }
	}
	
	\tag #'b \break
	r4 \f \repeat unfold 15 { r4 }
	
	\tag #'b \break
	\repeat unfold 12 { r4 }
	r4 r4 r4 r4
	
	\tag #'b \break
	r4 \repeat unfold 15 { r4 }
	
	% CODA
	\tag #'b \break
	\repeat unfold 16 { r4 }
	
	\tag #'b \break
	\repeat unfold 16 { r4 }
	
	\tag #'b \break
	\repeat unfold 16 { r4 }
	
	\tag #'b \break
	\repeat unfold 8 { r4 }
	\improOff
	r4 \times 2/3 { <af af'>8 ( <bf bf'> <c c'> } <ef ef'> -. ) <df df'> ( <af af'> -. ) <bf bf'> (
	<c c'>8 -. ) r \improOn c8 c -> ~ c2 \fermata
	
	\bar "|."
}

guitar = \relative c' {
  
	\global
	\key af \major
	
	\improOn
	r4 \mf \repeat unfold 15 { r4 }
	
	\tag #'b \break
	\repeat volta 2 {
		r4 \p \repeat unfold 15 { r4 }
		
		\tag #'b \break
		\repeat unfold 16 { r4 }
		
		\tag #'b \break
		\repeat unfold 16 { r4 }
		
		\tag #'b \break
		\repeat unfold 16 { r4 }
	}
	
	\tag #'b \break
	\repeat volta 2 {
		r4 \mf \repeat unfold 15 { r4 }
	}
	\alternative {
		{ \repeat unfold 8 { r4 }
		  \tag #'b \break
		  \repeat unfold 8 { r4 } }
		
		{ \repeat unfold 12 { r4 }
		  r4 \f r4 r4 r4 }
	}
	
	\tag #'b \break
	r4 \f \repeat unfold 15 { r4 }
	
	\tag #'b \break
	\repeat unfold 8 { r4 }
	r4 \improOff r4 r2
	R1
	
	\improOn
	\tag #'b \break
	r4 \mf \repeat unfold 15 { r4 }
	
	\tag #'b \break
	\repeat unfold 16 { r4 }
	
	% Alto sax solo
	\tag #'b \break
	r4 \mf \repeat unfold 15 { r4 }
	
	\tag #'b \break
	\repeat unfold 16 { r4 }
	
	\tag #'b \break
	\repeat unfold 16 { r4 }
	
	\tag #'b \break
	\repeat unfold 16 { r4 }
	
	% Bone Solo
	\tag #'b \break
	\repeat unfold 16 { r4 }
	
	\tag #'b \break
	\repeat unfold 12 { r4 }
	r4 \< r4 r4 r4
	
	\tag #'b \break
	r4 \f \repeat unfold 15 { r4 }
	
	\tag #'b \break
	\repeat unfold 8 { r4 }
	r4 \improOff r4 r2
	R1
	
	\improOn
	\tag #'b \break
	\repeat volta 2 {
		r4 \mf \repeat unfold 15 { r4 }
	}
	\alternative {
		{ \repeat unfold 8 { r4 }
		  \tag #'b \break
		  \repeat unfold 8 { r4 } }
		
		{ \repeat unfold 12 { r4 }
		  r4 \f r4 r4 r4 }
	}
	
	\tag #'b \break
	r4 \f \repeat unfold 15 { r4 }
	
	\tag #'b \break
	\repeat unfold 12 { r4 }
	r4 r4 r4 r4
	
	\tag #'b \break
	r4 \repeat unfold 15 { r4 }
	
	% CODA
	\tag #'b \break
	\repeat unfold 16 { r4 }
	
	\tag #'b \break
	\repeat unfold 16 { r4 }
	
	\tag #'b \break
	\repeat unfold 16 { r4 }
	
	\tag #'b \break
	\repeat unfold 8 { r4 }
	r4 \improOff \times 2/3 { af8 ( bf c } ef -. ) df ( af -. ) bf (
	c8 -. ) r \improOn c c -> ~ c2 \fermata
	
	\bar "|."
}

bass = \relative c' {
  
	\global
	\clef bass
	\key af \major
	
	af,4 \mf c ef c
	bf bf ef ef
	af,4 c ef c
	bf bf ef ef
	
	\improOn
	\tag #'b \break
	\repeat volta 2 {
		r4 \p \repeat unfold 15 { r4 }
		
		\tag #'b \break
		\repeat unfold 16 { r4 }
		
		\tag #'b \break
		\repeat unfold 16 { r4 }
		
		\tag #'b \break
		\repeat unfold 16 { r4 }
	}
	
	\tag #'b \break
	\repeat volta 2 {
		r4 \mf \repeat unfold 15 { r4 }
	}
	\alternative {
		{ \repeat unfold 8 { r4 }
		  \tag #'b \break
		  \repeat unfold 8 { r4 } }
		
		{ \repeat unfold 12 { r4 }
		  r4 \f r4 r4 r4 }
	}
	
	\tag #'b \break
	r4 \f \repeat unfold 15 { r4 }
	
	\tag #'b \break
	\repeat unfold 8 { r4 }
	r4 \improOff r4 r2
	R1
	
	\improOn
	\tag #'b \break
	r4 \mf \repeat unfold 15 { r4 }
	
	\tag #'b \break
	\repeat unfold 16 { r4 }
	
	% Alto sax solo
	\tag #'b \break
	r4 \mf \repeat unfold 15 { r4 }
	
	\tag #'b \break
	\repeat unfold 16 { r4 }
	
	\tag #'b \break
	\repeat unfold 16 { r4 }
	
	\tag #'b \break
	\repeat unfold 16 { r4 }
	
	% Bone Solo
	\tag #'b \break
	\repeat unfold 16 { r4 }
	
	\tag #'b \break
	\repeat unfold 12 { r4 }
	r4 \< r4 r4 r4
	
	\tag #'b \break
	r4 \f \repeat unfold 15 { r4 }
	
	\tag #'b \break
	\repeat unfold 8 { r4 }
	r4 \improOff r4 r2
	R1
	
	\improOn
	\tag #'b \break
	\repeat volta 2 {
		r4 \mf \repeat unfold 15 { r4 }
	}
	\alternative {
		{ \repeat unfold 8 { r4 }
		  \tag #'b \break
		  \repeat unfold 8 { r4 } }
		
		{ \repeat unfold 12 { r4 }
		  r4 \f r4 r4 r4 }
	}
	
	\tag #'b \break
	r4 \f \repeat unfold 15 { r4 }
	
	\tag #'b \break
	\repeat unfold 12 { r4 }
	r4 r4 r4 r4
	
	\tag #'b \break
	r4 \repeat unfold 15 { r4 }
	
	% CODA
	\tag #'b \break
	\repeat unfold 16 { r4 }
	
	\tag #'b \break
	\repeat unfold 16 { r4 }
	
	\tag #'b \break
	\repeat unfold 16 { r4 }
	
	\tag #'b \break
	\repeat unfold 8 { r4 }
	\improOff
	af,4 -. \times 2/3 { af8 ( bf c } ef -. ) df ( af -. ) bf (
	c8 -. ) r ef af, -> ~ af2 \fermata
	
	\bar "|."
}

drumsUp = \drummode {
  	
	\revert RestCollision.positioning-done
	\override Rest.staff-position = #6
	
	\dynamicUp
	
	s1*4
	
	\tag #'b \break
	s1*8
	
	\tag #'b \break
	s1*8
	
	\tag #'b \break
	s1*6
	
	\tag #'b \break
	s1*5
	\teeny r8 ^\markup \normalsize "(Open)" dd -> r dd -> ~ dd4 dd4 -^
	
	\tag #'b \break
	s1*5
	r2 r4 dd4 -.
	dd4 -^ s s2
	s1
	
	\tag #'b \break
	s1*8 ^\markup \normalsize "(Close)"
	
	\tag #'b \break
	s1*8
	
	\tag #'b \break
	s1*8
	
	\tag #'b \break
	s1*6
	r8 dd -> r4 r8 dd -> r4
	r8 dd -> r4 r8 dd -> r4
	
	\tag #'b \break
	s1*5 ^\markup \normalsize "(Open)"
	r2 r4 dd4 -.
	dd4 -^ s s2
	s1
	
	\tag #'b \break
	s1*6 ^\markup \normalsize "(Close)"
	
	\tag #'b \break
	s1*5
	r8 ^\markup \normalsize "(Open)" dd -> r dd -> ~ dd4 dd4 -^
	
	\tag #'b \break
	s1*6
	
	\tag #'b \break
	r8 dd -> r4 r8 dd -> r4
	r8 dd -> r4 r8 dd -> r4
	r8 dd -> r4 r8 dd -> r4
	r8 dd -> r4 r8 dd -> r4
	r8 dd -> r4 r8 dd -> r4
	r8 dd -> r4 r8 dd -> r4
	
	\tag #'b \break
	s1*8
	
	\tag #'b \break
	s1*6
	r4 \times 2/3 { dd8 ( dd dd } dd -. ) dd ( dd -. ) dd (
	dd4 -^ ) dd8 dd -> ~ dd2 \fermata
	
	\bar "|."
}

drumsDown = \drummode {

	\global
	
	\stemDown
	\override MultiMeasureRest.staff-position = 2
	\override Stem.extra-offset = #'(.05 . 0)
	\override Beam.extra-offset = #'(.05 . 0)
	
	\improOn
	\repeat percent 4 { r4 \mf r4 r4 r4 }
	
	\repeat volta 2 {
		\repeat percent 16 { r4 \p r4 r4 r4 }
	}
	
	\repeat volta 2 {
		\repeat percent 4 { r4 \mf r4 r4 r4 }
	}
	\alternative {
		{ \set countPercentRepeats = ##f
		  \repeat percent 4 { r4 \improOff sn \improOn r4 \improOff sn \improOn }
		  \set countPercentRepeats = ##t }
		{ \repeat percent 3 { r4 \improOff sn \improOn r4 \improOff sn \improOn } 
		  r4 \f r4 r4 r4 }
	}
	
	\repeat percent 6 { r4 \f r4 r4 r4 }
	r4 \improOff r4 r2
	R1
	
	\improOn
	\repeat percent 4 { r4 \mf r4 r4 r4 }
	\set countPercentRepeats = ##f
	\repeat percent 4 { r4 \improOff sn \improOn r4 \improOff sn \improOn }
	
	\set countPercentRepeats = ##t
	\repeat percent 16 { r4 \mf r4 r4 r4 }
	
	<< { 
		\improOn
		\set countPercentRepeats = ##t
  		\set repeatCountVisibility = #(every-nth-repeat-count-visible 4)
  		\override PercentRepeatCounter.direction = #DOWN
		\override Rest.staff-position = 0
		
		\repeat percent 8 { r4 r4 r4 r4 } 
		
		\revert PercentRepeatCounter.direction
		\repeat percent 6 { r4 \f r4 r4 r4 }
		r4 \improOff r4 r2
		R1
		
	   } \\ { s1*7 s1 \< s1 \! } 
	>>
	
	\improOn
	\set countPercentRepeats = ##t
	
	
	\repeat volta 2 {
		\repeat percent 4 { r4 \mf r4 r4 r4 }
	}
	\alternative {
		{ \set countPercentRepeats = ##f
		  \repeat percent 4 { r4 \improOff sn \improOn r4 \improOff sn \improOn }
		  \set countPercentRepeats = ##t }
		{ \repeat percent 3 { r4 \improOff sn \improOn r4 \improOff sn \improOn } 
		  r4 \f r4 r4 r4 }
	}
	
	\override PercentRepeatCounter.direction = #DOWN
	\repeat percent 12 { r4 \f r4 r4 r4 }
	
	\revert PercentRepeatCounter.direction
	\repeat percent 14{ r4 \p r4 r4 r4 }
	r4 \mf r4 r4 r4
	r4 r4 \f r2
	
	\bar "|."
}

drumsRepeats = \new DrumVoice \with { \RemoveEmptyStaves } {
	% Oculta el primer pentagrama del markup
	\override Staff.VerticalAxisGroup #'remove-first = ##t
	
	s1*44
	s1*3 \pmc #8
	
}



% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

\book {
	\paper { 
		#(set-paper-size "a4" 'landscape)
		indent = 20
		systems-per-page = 1
	}
	
	\score {
		% \midi { \tempo 4 = 110 }
		
		\removeWithTag #'b
		<<
			\scoreMarkup
			\new StaffGroup <<
				\new Staff \with { instrumentName = "Trumpet" midiInstrument = #"trumpet" } { \trumpet }
				\new Staff \with { instrumentName = "Alto Sax" midiInstrument = #"alto sax" } { \altoSax }
				\new Staff \with { instrumentName = "Tenor Sax" midiInstrument = #"tenor sax" } << \tenorSaxSoloChords \tenorSax >>
				\new Staff \with { instrumentName = "Trombone" midiInstrument = #"trombone" } << \tromboneSoloChords \trombone >>
			>>
			
			\scoreChords
			\new StaffGroup <<
				\new Staff \with { instrumentName = "Piano" } { \piano }
				\new Staff \with { instrumentName = "Guitar" } { \guitar }
				\new Staff \with { instrumentName = "Bass" } { \bass }
			>>
			
			\new DrumStaff <<
  				\set DrumStaff.drumStyleTable = #(alist->hash-table nwDrums)
				\new DrumVoice { \voiceOne \drumsUp }
				\new DrumVoice { \voiceTwo \drumsDown }
  				\drumsRepeats
			>>
		>>
	}
}

instrumentName = "Trumpet"
\book {
	\header { instrument = #instrumentName }
   \bookOutputName  #(string-append title " - " instrumentName)
   
	\score {
		\transpose bf c' 
		<<
			\removeWithTag #'sb \scoreMarkup
			\new StaffGroup <<
				\new Staff { \trumpet }
			>>
		>>
	}
}

instrumentName = "Alto Sax"
\book {
	\header { instrument = #instrumentName }
   \bookOutputName  #(string-append title " - " instrumentName)
   
	\score {
		\transpose ef c'
		<<
			\removeWithTag #'sb \scoreMarkup
			\new StaffGroup <<
				\new Staff { \altoSax }
			>>
		>>
	}
}

instrumentName = "Tenor Sax"
\book {
	\header { instrument = #instrumentName }
   \bookOutputName  #(string-append title " - " instrumentName)
   
	\score {
		\transpose bf c''
		<<
			\removeWithTag #'sb \scoreMarkup
			\tenorSaxSoloChords
			\new StaffGroup <<
				\new Staff { \tenorSax }
			>>
		>>
	}
}

instrumentName = "Trombone"
\book {
	\header { instrument = #instrumentName }
   \bookOutputName  #(string-append title " - " instrumentName)
   
	\score {
		<<
			\removeWithTag #'sb \scoreMarkup
			\tromboneSoloChords
			\new StaffGroup <<
				\new Staff { \trombone }
			>>
		>>
	}
}

instrumentName = "Piano"
\book {
	\header { instrument = #instrumentName }
	\paper { page-count = 3 }
   \bookOutputName  #(string-append title " - " instrumentName)
   
	\score {
		<<
			\removeWithTag #'sb \scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\new Staff { \piano }
			>>
		>>
	}
}

instrumentName = "Guitar"
\book {
	\header { instrument = #instrumentName }
	\paper { page-count = 3 }
   \bookOutputName  #(string-append title " - " instrumentName)
   
	\score {
		<<
			\removeWithTag #'sb \scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\new Staff { \guitar }
			>>
		>>
	}
}

instrumentName = "Bass"
\book {
	\header { instrument = #instrumentName }
   \bookOutputName  #(string-append title " - " instrumentName)
   
	\score {
		<<
			\removeWithTag #'sb \scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\new Staff { \bass }
			>>
		>>
	}
}

instrumentName = "Drums"
\book {
	\header { instrument = #instrumentName }
   \bookOutputName  #(string-append title " - " instrumentName)
   
	\score {
		<<
			\removeWithTag #'sb \scoreMarkup
			\new DrumStaff <<
  				\set DrumStaff.drumStyleTable = #(alist->hash-table nwDrums)
				\new DrumVoice { \voiceOne \drumsUp }
				\new DrumVoice { \voiceTwo \drumsDown }
  				\drumsRepeats
			>>
		>>
	}
}