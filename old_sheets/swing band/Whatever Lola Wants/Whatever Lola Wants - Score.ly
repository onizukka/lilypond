\version "2.18.2"


% Descripción de funcionalidades
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% PLUGINS
% - \eolMark:					mark al final de línea
% - \bendBefore: 				solución al bendBefore (i.e. dis8 \bendBefore)
% - \rit: 						crea una porción de ritardando que debe terminar como las dinámicas, con un \!
% - \improOn, \improOff:	negra de improvisación
% - \emptyStaff:				Imprime un pentagrama vacío
% - \emptyBar:					Imprime un compás invisible (útil para indentados)



% LAYOUT
% - El aspecto de los silencios multicompás es como los de las partituras de jazz
% - Margen extra de las articulaciones para evitar la colisión con las ligaduras
% - Glissandos bonitos



% PAPER
% - Se imprime el título, instrumento y número de página en todas las páginas
% - Solo se debe tocar el page-count (si eso). Sale bonito tal cual.

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Descripción de funcionalidades
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% 03-09-14: Layout
% Ahora las articulaciones van por dentro de las ligaduras. Parece que es la mejor solución



% 04-09-14: Layout
% Los creciendos y decreciendos tienen la línea un poco más gruesa
%
% Markup
% Añadidas guías para correcta posición y markup de la indicación de tempo
%
% Plugins
% Añadida función \emptyStave para imprimir un pentagrama vacío.
% En algunas partituras hay un pentagrama vacío antes del coda (lo cual mola bastante, por cierto)



% 05-09-14: Plugins
% Añadido a la función \emptyStave el esconder la armadura de la clave, que no estaba.



% 08-09-14: Layout
% Ahora no se muestra la cancelación de accidentales al haber un cambio de armadura



% 09-09-14: Chords
% Añadida sentencia para que los acordes menores salgan con un guión en vez de con una M minúscula



% 10-09-14: Plugins
% Mejorada la función \bendAfter.
% Ahora es genérica, y se puede definir cualquier texto en un lateral que haga las funciones de ornamento



% 15-09-14: Layout
% Añadido un poco más de margen a los reguladores



% 16-09-14: Plugins
% Mejorada la función para imprimir barras de improvisación.
% Antes se llamaba al comando \rs para pintar una barra que ocupaba una negra
% Se reemplaza \rs por \restImproOn y \restImproOff, siendo esta opción mucho más flexible.
% Cuando se activa \restImproOn, todo silencio se convierte en barra de improvisación, respetando su duración.



% 20-09-14: Plugins
% Mejorada la función para imprimir barras de improvisación.
% Se reemplaza \restImproOn y \restImproOff por \improOn y \improOff
% Ahora, también las notas del pentagrama salen como barra de improvisación.
% (los comandos \improvisationOn e \improvisationOff de lilypond no se llevan bien con LilyJAZZ)
% Se ha añadido un grob correspondiente a los slash de la fuente LilyJAZZ en el archivo lily-jazz.ly



% 20-09-14: Layout
% Comentada la línea que oculta la cancelación de accidentales al haber cambio de armadura.
% Realmente, la cancelación es muy bienvenida.



% 01-10-14: Plugins
% Renombrada la función \emptyStave a \emptyStaff (stave es el plural de staff) y 
% corregida (ya no muestra el número de compás).
% Recordar que después de usar \emptyStaff se necesita redefinir el 
% número de compás con \set Score.currentBarNumber = #xx
%
% Añadida la función \emptyBar que oculta un compás entero. Se puede usar para crear un 
% efecto de indentación en un pentagrama dado.
% Recordar que después de usar \emptyBar se necesita:
% - redefinir el número de compás con \set Score.currentBarNumber = #xx
% - volver a pintar la clave con \clef
% - volver a pintar la armadura con \key



% 15-10-14: Language
% Cambiado el lenguaje de entrada de notas del finlandes al inglés
% La diferencia es que ahora los sostenidos se escriben "-s" en vez de "-is"
% y los bemoles "-f" en vez de "-es".
%
% Estructura
% Para poder autoincluir los archivos de instrumento en otros 
% (para montar un midi, o un score), se han movido los plugins a un archivo separado
% y ahora se utiliza la función \includeOnce, evitando así el problema de las
% dependencias circulares.
%
% Global
% Todas las instrucciones que se colocaban justo antes de empezar la música están ahora
% recogidas en una función \global.
% A \global se le ha añadido instrucciones para que agrupe las corcheas de dos en dos
% en vez de cuatro en cuatro (el comportamiento por defecto).
%
% Plugins
% Se le ha aumentado el grosor de los bordes de la función \boxed



% 26-10-14: Chords
% Añadido el include "chord-exception.ily" que recoge una mejor notación de los acordes.
% Actualmente, recoge los acordes maj7, 7(b5), 7(#5), 7(b9), 7(#9), 7(add13), maj9(add13), 9(#11)
% Se irá añadiendo acordes a menudo.
 

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Dependencias
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% \includeOnce
% Incluye una dependencia solo una vez. Así se pueden incluir los archivos en otros 
% (para montar un score por ejemplo) sin liarla parda con dependencias cíclicas.

includeOnce = #(
	define-void-function (parser location filename) (string?) (
		if (not (defined? (string->symbol filename))) (
			begin (
				ly:parser-include-string parser (
					string-concatenate (list "\\include \"" filename "\"")
				)
			)
			(primitive-eval (list 'define (string->symbol filename) #t))
		)
	)
)



% LilyJAZZ
\includeOnce "includes/ignatzek-jazz-chords.ily"
\includeOnce "includes/lily-jazz.ily"



% Language
\language "english"



% Plugins
\includeOnce "includes/plugins-1.2.ily"



% Chord Exceptions
\includeOnce "includes/chord-exceptions.ily"

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~











% Disposición
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Cabecera
\header {
	title = "Whatever Lola Wants"
	instrument = ""
	composer = "Sarah Vaughan"
	arranger = "Arr: Gabriel Palacios"
	poet = ""
	tagline = "gabriel.ps.ms@gmail.com"
}

% Tamaño de las notas y pentagrama
#(set-global-staff-size 20)

% Márgenes, fuentes y distancias del papel
\paper {
	% El definir otro tipo de fuente no permite cambiar el tamaño global del staff. Es un bug conocigo
	#(define fonts (make-pango-font-tree "LilyJAZZText" "Nimbus Sans" "Luxi Mono" (/ 20 20)))

	% distancia entre la parte superior de la página y el título
	top-markup-spacing #'basic-distance = 5

	% distancia entre el título y el primer sistema
	markup-system-spacing #'basic-distance = 25

	% distancia entre el texto y el primer sistema 
	top-system-spacing #'basic-distance = 15

	% márgen inferior
	bottom-margin = 20
	
	% intentado del primer sistema
	indent = 0
	 
	% número de páginas
	% page-count = 1

	ragged-last-bottom = ##f 

	oddHeaderMarkup = \markup {
	 \on-the-fly #not-first-page \fill-line {
		\fromproperty #'page:page-number-string
		\fromproperty #'header:title
		\fromproperty #'header:instrument
	 }
	}

	evenHeaderMarkup = \markup {
	 \on-the-fly #not-first-page \fill-line {
		\fromproperty #'header:instrument
		\fromproperty #'header:title
		\fromproperty #'page:page-number-string
	 }
	}
}

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~







% Elementos comunes de la partitura
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
	% Oculta el primer pentagrama del markup
	\override Staff.VerticalAxisGroup #'remove-first = ##t
	
	% Indicación de tiempo
	\once \override Score.RehearsalMark.X-offset = #5
	\once \override Score.RehearsalMark.extra-offset = #'(0 . 3)
	\mark "Medium Mambo"
	
	s1*4
	\mark \markup \boxed "A"
	
	s1*8
	\mark \markup {
		\column { \boxed "B" }
		\column { "(x2)" }
	}
	
	s1*8
	\once \override Score.RehearsalMark.X-offset = #8
	\mark \markup \boxed "C"
	
	s1*8
	\mark \markup \boxed "D"
	
	s1*15
	\mark \markup \boxed "E"
}



% Función musical global
global = {
	% Para pintar todos los números de compás debajo del pentagrama
	% \override Score.BarNumber #'break-visibility = #end-of-line-invisible
	% \override Score.BarNumber #'direction = #DOWN
	% \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

	\jazzOn
	\compressFullBarRests

	% Arregla el problema de tener el scoreMarkup añadido
	\revert MultiMeasureRest #'staff-position
	\override Voice.Rest #'staff-position = #0

	\set Timing.beamExceptions = #'()
	\set Timing.baseMoment = #(ly:make-moment 1/4)
	\set Timing.beatStructure = #'(1 1 1 1)
	
	\override ParenthesesItem.font-size = #0
}

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Elementos específicos del instrumento
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Cifrado
scoreChords = \chords {    
	\set minorChordModifier = \markup { "-" }
	\set chordNameExceptions = #chordNameExceptions
	\override ChordName.font-size = #2.5

	% c1:m f2:m g2:m7
}

% Música
trumpet = \relative c'' {
  
	\global
	\key bf \minor
	
	R1*2
	bf4 \f -. r8 df -. f8 -- -> r f8 -- -> r 
	f2 -> \bendAfter #-7 r
	
	R1*8
	
	\break
	\repeat volta 2 {
		R1*6
		r4 r8 df8 \f f8 -- -> r f -- -> r
		bf8 -- -> r r4 r2
		
		\break
		R1*4
		
		r2 r4 r8 \f ds,8 -^
		fs -> r r4 r2
		R1*2
		
		\break
		R1*8
		
		R1*3
	}
	\alternative {
		{ r4 r8 c'8 \f -> r2
		  R1*2 }
		
		{ r4 r8 b8 \f -> r2 }
	}
	
	\break
	R1*4
	r4 r8 gf8 \f ( f -> ) r r gf
	f gf f -> r r4 bf8 -> r
	
	\bar "|."
}

altoSax = \relative c'' {
  
	\global
	\key bf \minor

	R1*2
	bf,4 \f -. r8 df -. f8 -- -> r f8 -- -> r 
	f2 -> \bendAfter #-7 r
	
	R1*8
	
	\break
	\repeat volta 2 {
		R1*6
		r4 r8 df'8 \f f8 -- -> r df -- -> r
		g8 -- -> r r4 r2
		
		\break
		r4 r8 \ap \parenthesize fs,,8 \mp ( gs -^ ) r r \ap \parenthesize fs8 (
		gs -> \ap \parenthesize fs gs -^ ) r r2
		r2 r4 r8 \parenthesize f?8 (
		g -> \parenthesize f g -^ ) r r2
		
		\break
		r2 r4 r8 \f b'8 -^
		ds -> r r4 r2
		R1*2
		
		\break
		R1*8
		
		R1*3
	}
	\alternative {
		{ r4 r8 fs8 \f -> r2
		  R1*2 }
		
		{ r4 r8 e8 \f -> r2 }
	}
	
	\break
	R1*4
	r4 r8 gf8 \f ( f -> ) r r gf
	f gf f -> r r4 f8 -> r
	
	\bar "|."
}

tenorSax = \relative c' {
  
	\global
	\key bf \minor
	
	R1*2
	bf4 \f -. r8 df -. f8 -- -> r f8 -- -> r 
	f2 \bendAfter #-7 r
	
	R1*8
	
	\break
	\repeat volta 2 {
		R1*6
		r4 r8 df8 \f f8 -- -> r a -- -> r
		df8 -- -> r r4 r2
		
		\break
		r4 r8 \ap \parenthesize ds,,8 \mp ( fs -^ ) r r \ap \parenthesize ds8 (
		fs -> \ap \parenthesize ds fs -^ ) r r2
		r2 r4 r8 \parenthesize d?8 (
		f? -> \parenthesize d f -^ ) r r2
		
		\break
		r2 r4 r8 \f fs'8 -^
		b -> r r4 r2
		R1*2
		
		\break
		R1*8
		
		R1*3
	}
	\alternative {
		{ r4 r8 gs8 \f -> r2
		  R1*2 }
		{ r4 r8 gs8 \f -> r2 }
	}
	
	\break
	R1*4
	r4 r8 df8 \f ( c -> ) r r df
	c df c -> r r4 g'8 -> r
	
	\bar "|."
}

trombone = \relative c' {
  
	\global
	\clef bass
	\key bf \minor
	
	bf,4 \mf -. r8 df -. f4 -. r
	bf,4 -. r8 df -. f4 -. r
	bf4 \f -. r8 df -. f8 -- -> r f8 -- -> r 
	f2 -> \bendAfter #-7 r
	
	\break
	R1
	bf,,4 \mf -. r8 df -. f4 -. r
	R1
	c4 -. r8 e -. g4 -. r
	
	\break
	R1*2
	r2 r8 gf, \f -> r4
	f8 -> r r4 r2
	
	\break
	\repeat volta 2 {
		R1*6
		r4 r8 df''8 \f f8 -- -> r f -- -> r
		bf,8 -- -> r bf, -- -> r r2
		
		\break
		r2 ^ \markup \box "Plunger" b'8 \mp -> ^ \markup \center-align "Wah" r r4
		b8 -> ^ \markup \center-align "Wah" r b -> ^ \markup \center-align "Wah" r r2
		R1
		bf?8 -> ^ \markup \center-align "Wah" r bf -> ^ \markup \center-align "Wah" r r2
		
		\break
		r2 ^ \markup \box "Open" r4 r8 \f fs8 -^
		gs -> r r4 r2
		R1*2
		
		\break
		R1*8
		
		R1
		\once \revert Score.Glissando.style
    	\once \revert Score.Glissando.bound-details.left.padding
		r4 \mf f8 \glissando ef ~ ef4 r
		R1
	}
	\alternative {
		{ r4 \f b'8 -. e -> r2
		  R1*2 }
		{ r4 r8 cs8 \f -> r2 }
	}
	
	\break
	R1*4
	r4 r8 df8 \f ( c -> ) r r df
	c df c -> r r4 df8 -> r
	
	\break
	
	\bar "|."
}



% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

\book {
	\paper { 
		#(set-paper-size "a4" 'landscape)
		indent = 20
		systems-per-page = 2
	}
	
	\score {
		%% \midi { \tempo 4 = 130 }
		\layout { #(layout-set-staff-size 20) }
		
		<<
			\scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\new Staff \with { instrumentName = "Trumpet" midiInstrument = #"trumpet" } { \trumpet }
				\new Staff \with { instrumentName = "Alto Sax" midiInstrument = #"alto sax" } { \altoSax }
				\new Staff \with { instrumentName = "Tenor Sax" midiInstrument = #"tenor sax" } { \tenorSax }
				\new Staff \with { instrumentName = "Trombone" midiInstrument = #"trombone" } { \trombone }
			>>
		>>
	}
}

\book {
	\header { instrument = "Trumpet" }
   \bookOutputName "Whatever Lola Wants - Trumpet"
	\score {
		<<
			\scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\new Staff { \transpose bf c' \trumpet }
			>>
		>>
	}
}

\book {
	\header { instrument = "Alto Sax" }
   \bookOutputName "Whatever Lola Wants - Alto Sax"
	\score {
		<<
			\scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\new Staff { \transpose ef c' \altoSax }
			>>
		>>
	}
}

\book {
	\header { instrument = "Tenor Sax" }
   \bookOutputName "Whatever Lola Wants - Tenor Sax"
	\score {
		<<
			\scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\new Staff { \transpose bf c'' \tenorSax }
			>>
		>>
	}
}

\book {
	\header { instrument = "Trombone" }
   \bookOutputName "Whatever Lola Wants - Trombone"
	\score {
		<<
			\scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\new Staff { \trombone }
			>>
		>>
	}
}