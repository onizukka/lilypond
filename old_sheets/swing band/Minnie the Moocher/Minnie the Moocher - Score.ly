\version "2.18.2"


% Descripción de funcionalidades
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% PLUGINS
% - \eolMark:					mark al final de línea
% - \bendBefore: 				solución al bendBefore (i.e. dis8 \bendBefore)
% - \rit: 						crea una porción de ritardando que debe terminar como las dinámicas, con un \!
% - \improOn, \improOff:	negra de improvisación
% - \emptyStaff:				Imprime un pentagrama vacío
% - \emptyBar:					Imprime un compás invisible (útil para indentados)
% - \glissBefore:				muestra un glissando hacia la nota
% - \shortGlissBefore:		muestra un glissando corto hacia la nota
% - \jazzTurn:					muestra el simbolo equivalente a un turn en jazz
% - \subp:						muestra "sp", subito piano
% - \ap:							(accidental padding), separa un poco una alteración de la siguiente nota
% 									(útil al usar parenthesize)
% - \optOttava:				octavaje opcional

% LAYOUT
% - El aspecto de los silencios multicompás es como los de las partituras de jazz
% - Margen extra de las articulaciones para evitar la colisión con las ligaduras
% - Glissandos bonitos

% PAPER
% - Se imprime el título, instrumento y número de página en todas las páginas
% - Solo se debe tocar el page-count (si eso). Sale bonito tal cual.

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Changelog de funcionalidades
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Plugins
% 23-09-85: Añadida función \emptyStave para imprimir un pentagrama vacío.
% 				En algunas partituras hay un pentagrama vacío antes del coda (lo cual mola bastante, por cierto)
% 05-09-14: Añadido a la función \emptyStave el esconder la armadura de la clave, que no estaba.
% 10-09-14: Mejorada la función \bendBefore
% 				Ahora es genérica, y se puede definir cualquier texto en un lateral que haga las funciones de ornamento
% 16-09-14: Mejorada la función para imprimir barras de improvisación.
% 				Antes se llamaba al comando \rs para pintar una barra que ocupaba una negra
% 				Se reemplaza \rs por \restImproOn y \restImproOff, siendo esta opción mucho más flexible.
% 				Cuando se activa \restImproOn, todo silencio se convierte en barra de improvisación, respetando su duración.
% 20-09-14: Mejorada la función para imprimir barras de improvisación.
% 				Se reemplaza \restImproOn y \restImproOff por \improOn y \improOff
% 				Ahora, también las notas del pentagrama salen como barra de improvisación.
% 				(los comandos \improvisationOn e \improvisationOff de lilypond no se llevan bien con LilyJAZZ)
% 				Se ha añadido un grob correspondiente a los slash de la fuente LilyJAZZ en el archivo lily-jazz.ly
% 01-10-14: Plugins
% 				Renombrada la función \emptyStave a \emptyStaff (stave es el plural de staff) y 
% 				corregida (ya no muestra el número de compás).
% 				Recordar que después de usar \emptyStaff se necesita redefinir el 
% 				número de compás con \set Score.currentBarNumber = #xx
% 01-10-14: Añadida la función \emptyBar que oculta un compás entero. Se puede usar para crear un 
% 				efecto de indentación en un pentagrama dado.
% 				Recordar que después de usar \emptyBar se necesita:
% 				- redefinir el número de compás con \set Score.currentBarNumber = #xx
% 				- volver a pintar la clave con \clef
% 				- volver a pintar la armadura con \key
% 15-10-14: Se le ha aumentado el grosor de los bordes de la función \boxed
% 18-10-15: Añadidas funciones de ornamento para antes de la nota:
% 				- \glissBefore: para mostrar un glissando antes de una nota
% 				- \shortGlissBefore: para mostrar un glissando corto antes de una nota
% 				- \jazzTurn: para mostrar el turn típico trompetero de jazz
% 24-10-15: Añadida funcion \subp para mostrar subito piano "sp"
% 27-10-15: Añadida funcion \ap (de accidental padding) para separar un poco el accidental de la siguiente nota
% 				al usar un paréntesis con \parenthesize, ya que se solapan el uno al otro
% 27-10-15: Añadida funcion \optOttava para imprimir octavaje opcional
% 				También he aprovechado para cambiar la fuente del texto "8va" que utilizaba Feta por defecto

% Markup
% 23-09-85: Añadidas guías para correcta posición y markup de la indicación de tempo
% 27-10-15: Guía para posición de la indicación de tempo mejorada

% Layout
% 23-09-85: Ahora las articulaciones van por dentro de las ligaduras. Parece que es la mejor solución
% 04-09-14: Los creciendos y decreciendos tienen la línea un poco más gruesa
% 08-09-14: Ahora no se muestra la cancelación de accidentales al haber un cambio de armadura
% 15-09-14: Añadido un poco más de margen a los reguladores
% 20-09-14: Comentada la línea que oculta la cancelación de accidentales al haber cambio de armadura.
% 				Realmente, la cancelación es muy bienvenida.

% Chords
% 09-09-14: Añadida sentencia para que los acordes menores salgan con un guión en vez de con una M minúscula
% 26-10-14: Añadido el include "chord-exception.ily" que recoge una mejor notación de los acordes.
% 				Actualmente, recoge los acordes maj7, 7(b5), 7(#5), 7(b9), 7(#9), 7(add13), maj9(add13), 9(#11)
% 				Se irá añadiendo acordes a menudo.

% Global
% 15-10-14: Todas las instrucciones que se colocaban justo antes de empezar la música están ahora
% 				recogidas en una función \global.
% 				A \global se le ha añadido instrucciones para que agrupe las corcheas de dos en dos
% 				en vez de cuatro en cuatro (el comportamiento por defecto).

% Language
% 15-10-14: Cambiado el lenguaje de entrada de notas del finlandes al inglés
% 				La diferencia es que ahora los sostenidos se escriben "-s" en vez de "-is"
% 				y los bemoles "-f" en vez de "-es".

% Estructura
% 15-10-14: Para poder autoincluir los archivos de instrumento en otros 
% 				(para montar un midi, o un score), se han movido los plugins a un archivo separado
% 				y ahora se utiliza la función \includeOnce, evitando así el problema de las
% 				dependencias circulares. 
% 18-10-15: Mejorada la manera de trabajar con papeles separados y score al mismo tiempo

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Dependencias
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% \includeOnce
% Incluye una dependencia solo una vez. Así se pueden incluir los archivos en otros 
% (para montar un score por ejemplo) sin liarla parda con dependencias cíclicas.

includeOnce = #(
	define-void-function (parser location filename) (string?) (
		if (not (defined? (string->symbol filename))) (
			begin (ly:parser-include-string parser (string-concatenate (list "\\include \"" filename "\"")))
			(primitive-eval (list 'define (string->symbol filename) #t))
		)
	)
)



% LilyJAZZ
\includeOnce "includes/ignatzek-jazz-chords.ily"
\includeOnce "includes/lily-jazz.ily"

% Language
\language "english"

% Plugins
\includeOnce "includes/plugins-1.2.ily"

% Chord Exceptions
\includeOnce "includes/chord-exceptions.ily"

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~











% Disposición
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Cabecera
\header {
	title = "Minnie the Moocher"
	subtitle = "(as performed by Big Bad Voodoo Daddy)"
	instrument = ""
	composer = "Cab Calloway"
	arranger = "Arr: Gabriel Palacios"
	poet = ""
	tagline = "gabriel.ps.ms@gmail.com"
}

% Tamaño de las notas y pentagrama
#(set-global-staff-size 20)

% Márgenes, fuentes y distancias del papel
\paper {
	% El definir otro tipo de fuente no permite cambiar el tamaño global del staff. Es un bug conocigo
	#(define fonts (make-pango-font-tree "LilyJAZZText" "Nimbus Sans" "Luxi Mono" (/ 20 20)))

	% distancia entre la parte superior de la página y el título
	top-markup-spacing #'basic-distance = 5

	% distancia entre el título y el primer sistema
	markup-system-spacing #'basic-distance = 25

	% distancia entre el texto y el primer sistema 
	top-system-spacing #'basic-distance = 15

	% márgen inferior
	bottom-margin = 20
	
	% intentado del primer sistema
	indent = 0
	 
	% número de páginas
	% page-count = 1

	ragged-last-bottom = ##f

	oddHeaderMarkup = \markup {
	 \on-the-fly #not-first-page \fill-line {
		\fromproperty #'page:page-number-string
		\fromproperty #'header:title
		\fromproperty #'header:instrument
	 }
	}

	evenHeaderMarkup = \markup {
	 \on-the-fly #not-first-page \fill-line {
		\fromproperty #'header:instrument
		\fromproperty #'header:title
		\fromproperty #'page:page-number-string
	 }
	}
}

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~







% Elementos comunes de la partitura
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
	% Oculta el primer pentagrama del markup
	\override Staff.VerticalAxisGroup #'remove-first = ##t
	
	% Indicación de tiempo
	\once \override Score.RehearsalMark.self-alignment-X = #LEFT
	\once \override Score.RehearsalMark.extra-offset = #'(0 . 3)
	\mark "Slow Swing"
	
	s2
	\once \override Score.RehearsalMark.X-offset = #3
	\mark \markup \fontsize #2 \jazzglyph #'"scripts.varsegnojazz"
	
	s1*8
	\once \override Score.RehearsalMark.self-alignment-X = #LEFT
	\eolMark \markup "(Al coda)"
	\mark \markup {
		\column { \boxed "A" }
		\column { "(Trumpet solo)" }
	}
	
	s1*8
	\once \override Score.RehearsalMark.self-alignment-X = #LEFT
	\mark \markup {
		\column { \boxed "B" }
		\column { "(x2)" }
	}
	
	s1*16
	\mark \markup \boxed "C"
	
	s1*6
	\mark \jazzTempoChange c4 c2
	
	s1*4
	\mark \markup \boxed "D"
	
	s1*4
	\mark \jazzTempoChange c2 c4
	
	s1*2
	\mark \markup \boxed "E"
	
	s1*8
	\mark \markup \boxed "F"
	
	s1*16
	\eolMark \markup "(D.S. al Coda)"
	\mark \markup \fontsize #2 \jazzglyph #'"scripts.varcodajazz"
}



% Función musical global
global = {
	% Para pintar todos los números de compás debajo del pentagrama
	% \override Score.BarNumber #'break-visibility = #end-of-line-invisible
	% \override Score.BarNumber #'direction = #DOWN
	% \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

	\jazzOn
	\compressFullBarRests

	% Arregla el problema de tener el scoreMarkup añadido
	\revert MultiMeasureRest #'staff-position
	\override Voice.Rest #'staff-position = #0	

	\set Timing.beamExceptions = #'()
	\set Timing.baseMoment = #(ly:make-moment 1/4)
	\set Timing.beatStructure = #'(1 1 1 1)
	
	\override ParenthesesItem.font-size = #0
}

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Elementos específicos del instrumento
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Cifrado
scoreChords = \chords {    
	\set minorChordModifier = \markup { "-" }
	\set chordNameExceptions = #chordNameExceptions
	\override ChordName.font-size = #2.5


	% c1:m f2:m g2:m7
}

tromboneSoloChords = \chords {    
	\set minorChordModifier = \markup { "-" }
	\set chordNameExceptions = #chordNameExceptions
	\override ChordName.font-size = #2.5

	s2
	
	s1*44
	e1:m6
	s1
	e2:m6 c:7
	b1:7
}

trumpetSoloChords = \chords {    
	\set minorChordModifier = \markup { "-" }
	\set chordNameExceptions = #chordNameExceptions
	\override ChordName.font-size = #2.5

	s2
	
	s1*8
	
	e1:m6
	c2:7 b2:7
	e1:m6
	c2:7 b2:7
	
	e1:m6
	c2:7 b2:7
	e1:m6
	c2:7 b2:7
}
% Música
trumpet = \relative c'' {
  
	\global
	\key e \minor
	
	\partial 2
	r4 r4
	
	\bar "||"
	r4 \f b' -> a8 ( a -^ ) r4
	r8 b4 -> as8 ( b4 -- ) e, ->
	r4 b' -> a?8 ( a -> ) r g ->
	r b, -> r e -> r g4. -- \prallprall
	
	\break
	r8 b4. -- \prallprall r8 a4. -- \prallprall
	r8 g -> r b -> r4 b,8 -. e (
	g -. ) g ( e -. ) e ( c -. ) \> c ( b -. ) b (
	g8 fs -. ) r e \! -> \mp ~ e4 \bendAfter #-5 r4
	
	\break
	\bar "||"
	\improOn \repeat unfold 32 { r4 } \improOff
	
	\break
	\repeat volta 2 {
  		R1
  		r4 \mp e g \< e
  		g1 \mf
  		ds4. cs8 \> ~ cs4 r \!
  		
  		\break
  		r8 \mp g' -> \> ~ g \! r r g -> \> ~ g \! r 
  		r8 g -> \> ~ g \! r r g -> \> ~ g \! r 
  		r4 g8( e -> ) ~ e2
  		ds8 ds -> r cs -> ~ cs4 \> r4 \!
  		
  		\break
  		R1*8
	}
	
	\break
	e1 \mp ~
	e2 g4 e
	g1
	ds4. cs8 ~ cs2
	
	\break
	b'2 g
	g b
	
	\bar "||"
	R1*3
	r8 \f <g \parenthesize g'> ( <a \parenthesize a'> \ap <as \parenthesize as'> <b \parenthesize b'> <d \parenthesize d'> \ap <ds \parenthesize ds'> <e \parenthesize e'> -> ) \bendAfter #-3
	
	\break
	\repeat volta 3 {
  		R1*4
	}
	
	R1*2
	
	\break
	r8 ^ "(Shout)" b' \f -> r b -> r b -> r g (
	bf4 -- -> ) a8 -> ( \parenthesize g ) a -- -> g -^ r4
	r8 b? -> r b -> ~ b4 \prallprall g8 -> ( \parenthesize g )
	bf4 -> \prallprall g8 -- a -^ r2
	
	\break
	r4 g8 ( b? -- -> ~ ) b4 g ->
	bf16 ( bf16 -. ) r8 bf16 ( bf16 -. ) r8 a -- g -^ r4
	r2 \times 2/3 { r8 b,? ( d } \times 2/3 { e ) g ( a } 
	bf4 -> ) bf4 -> a -> b8 ( < e, \parenthesize e' > -> ) 
	
	\break
  	R1
  	r4 \mp e, g \< e
  	g1 \!
  	ds4. cs8 \> ~ cs4 r \!
	
	R1*4
	
	\break
	R1*8
	
	\bar "||"
	\break
	c'1 \mf ~
	c2 \> r \!
	
	b1 \mf ~
	b2 \> \fermata r \! \fermata
	
	e,1 \p \fermata
	
	\bar "|."
}

altoSax = \relative c' {
  
	\global
	\key e \minor
	
	\partial 2
	r4 r4
	
	\bar "||"
	r4 \f g'' -> fs8 ( fs -^ ) r4
	r8 g4 -> fs8 ( g4 -- ) e ->
	r4 g -> fs8 ( fs -> ) r e ->
	r g, -> r b -> r cs4. -- \prallprall
	
	\break
	r8 g'4. -- \prallprall r8 fs4. -- \prallprall
	r8 e -> r g -> r4 g,8 -. b (
	e -. ) e ( d -. ) d ( bf -. ) bf ( a -. ) a (
	ds,8 ds -. ) r cs \! -> \mp ~ cs4 \bendAfter #-5 r4
	
	\break
	\bar "||"
	r2 e \bendBefore \p \<
	g2 \bendBefore \mf fs4 \> \glissando ds
	e2 \! r2 
	g2 \bendBefore \mf fs4 \> \glissando ds
	
	\break
	e2 \! r2 
	g2 \bendBefore \mf fs4 \> \glissando ds
	e2 \! r2 
	d2 \bendBefore \mf cs2 \>
	
	\break
	\repeat volta 2 {
  		b2 \mp ^ "(Play 1st x only)" r2
  		r4 b' ^ "(Play both x's)" e \< b
  		bf1 \mf
  		a4. \bendBefore g8 \> ~ g4 r \!
  		
  		\break
  		r8 \mp b? -> \> ~ b \! r r b -> \> ~ b \! r
  		r8 b -> \> ~ b \! r r b -> \> ~ b \! r 
  		r4 bf8( g -> ) ~ g2
  		a8 g -> r e -> ~ e4 \> r4 \!
  		
  		\break
  		R1*8
	}
	
	\break
	g1 \mp ~
	g2 b4 g
	bf1
	a4. g8 ~ g2
	
	\break
	e'2 b? 
	b e2
	
	\bar "||"
	R1*3
	r8 \f g, ( a bf b d ds e8 -> ) \bendAfter #-3
	
	\break
	\repeat volta 3 {
  		R1*4
	}
	
	R1*2
	
	\break
	r8 ^ "(Shout)" e \f -> r e -> r e -> r b (
	e4 -- -> ) e8 -> ( \parenthesize d ) ds -- -> b -^ r4
	r8 e -> r e -> ~ e4 \prallprall \times 2/3 { d8 -> ( e ) \parenthesize e }
	e4 -- -> \prallprall d8 -- ds -^ r2
	
	\break
	r4 b8 ( e -- -> ~ ) e4 b ->
	e16 ( e16 -. ) r8 e16 ( e16 -. ) r8 ds -- cs -^ r4
	r2 \times 2/3 { r8 b ( d } \times 2/3 { e ) ds ( cs } 
	d?4 -> ) d4 -> cs -> ds8 ( e -> ) \bendAfter #-3
	
	\break
	R1
  	r4 \mp b e \< b
  	bf1 \!
  	a4. \bendBefore g8 \> ~ g4 r \!
	
	R1*4
	
	\break
	R1*8
	
	\bar "||"
	\break
	bf1 \mf ~
	bf2 \> r \!
	
	a1 \mf ~
	a2 \> \fermata r \! \fermata
	
	cs,1 \p \fermata
	
	\bar "|."
}

tenorSax = \relative c' {
  
	\global
	\key e \minor
	
	\partial 2
	r4 r4
	
	\bar "||"
	r4 \f cs' -> c8 ( c -^ ) r4
	r8 cs4 -> c8 ( cs4 -- ) e, ->
	r4 cs' -> c8 ( c -> ) r b ->
	r e, -> r g -> r b4. -- \prallprall
	
	\break
	r8 cs4. -- \prallprall r8 c4. -- \prallprall
	r8 b -> r b -> r4 e,8 -. g (
	d' -. ) d ( bf -. ) bf ( g -. ) \> g ( fs -. ) fs (
	cs8 cs -. ) r b \! -> \mp ~ b4 \bendAfter #-5 r4
	
	\break
	\bar "||"
	r2 b \p \bendBefore \<
	d2 \bendBefore \mf c4 \> \glissando a 
	b2 \! r
	d2 \bendBefore \mf c4 \> \glissando a 
	
	\break
	b2 \! r
	d2 \bendBefore \mf c4 \> \glissando a
	b2 \! r
	bf2 \bendBefore \mf a2 \> 
	
	\break
	\repeat volta 2 {
  		g2 \mp ^ "(Play 1st x only)" r2
  		r4 g' ^ "(Play both x's)" b \< g
  		d1 \mf
  		cs4. b8 \> ~ b4 r \!
  		
  		\break
  		r8 \mp e -> \> ~ e \! r r ds -> \> ~ ds \! r 
  		r8 d? -> \> ~ d \! r r cs -> \> ~ cs \! r 
  		r4 e8( d -> ) ~ d2
  		cs8 c -> r b -> ~ b4 \> r4 \!
  		
  		\break
  		R1*8
	}
	
	\break
	b1 \mp ~
	b2 e4 b
	d1
	cs4. b8 ~ b2
	
	\break
	g'2 g 
	e g
	
	\bar "||"
	R1*3
	r8 \f g, ( a as b d ds e8 -> ) \bendAfter #-3
	
	\break
	\repeat volta 3 {
  		R1*4
	}
	
	R1*2
	
	\break
	r8 ^ "(Shout)" cs' \f -> r cs -> r cs -> r b (
	d4 -- -> ) c?8 -> ( \ap \parenthesize bf ) c -- -> a -^ r4
	r8 cs -> r cs -> ~ cs4 \prallprall b8 ( -> \parenthesize b )
	c?4 -- -> \prallprall bf8 -- b -^ r2
	
	\break
	r4 g8 ( b -- -> ~ ) b4 g ->
	d'16 ( d16 -. ) r8 d16 ( d16 -. ) r8 c -- a -^ r4
	\times 2/3 { r8 b, ( d } \times 2/3 { e ) b ( d }  \times 2/3 { e) b ( d } \times 2/3 { e ) b' ( b } 
	bf4 -> ) bf4 -> a -> b8 ( e, -> ) \bendAfter #-3
	
	\break
  	R1
  	r4 \mp g b \< g
  	d1 \!
  	cs4. b8 \> ~ b4 r \!
	
	R1*4
	
	\break
	R1*8
	
	\bar "||"
	\break
	e1 \mf ~
	e2 \> r \!
	
	ds1 \mf ~
	ds2 \> \fermata r \! \fermata
	
	g,1 \p \fermata
	
	\bar "|."
}

trombone = \relative c' {
  
	\global
	\clef bass
	\key e \minor
	
	\partial 2
	r4 r4
	
	\bar "||"
	r4 \f b' -> a8 ( a -^ ) r4
	r8 b4 -> as8 ( b4 -- ) e, ->
	r4 b' -> a?8 ( a -> ) r g ->
	r cs, -> r e -> r g4. -- \prallprall
	
	\break
	r8 b4. -- \prallprall r8 a4. -- \prallprall
	r8 g -> r e -> r4 cs8 -. e (
	bf' -. ) bf ( g -. ) g ( e -. ) \> e ( ds -. ) ds (
	b?8 a -. ) r g \! -> \mp ~ g4 \bendAfter #-5 r4
	
	\break
	\bar "||"
	r2 g \bendBefore \<
	bf2 \mf \bendBefore a4 \> \glissando fs
  	g2 \! r2 
	bf2 \mf \bendBefore a4 \> \glissando fs
	
	\break
  	g2 \! r2
	bf2 \mf \bendBefore a4 \> \glissando fs
  	g2 \! r2
	c,2 \mf \bendBefore b2 \> 
	
	\break
	\repeat volta 2 {
  		e,2 \mp ^ "(Play 1st x only)" r2
  		r4 b'' ^ "(Play both x's)" g \< e
  		c1 \mf
  		b4. e,8 \> ~ e4 r \!
  		
  		\break
  		r8 \mp b'' -> \> ~ b \! r r b -> \> ~ b \! r 
  		r8 b -> \> ~ b \! r r b -> \> ~ b \! r 
  		r4 d8( bf -> ) ~ bf2
  		b?8 a -> r g -> ~ g4 \> r4 \!
  		
  		\break
  		R1*8
	}
	
	\break
	b1 \mp ~
	b2 g4 e 
	c1
	b4. e,8 ~ e2
	
	\break
	e''2 ds
	d cs
	
	\bar "||"
	R1*3
	r8 \f g ( a bf b d ds e8 -> ) \bendAfter #-3
	
	\break
	\repeat volta 3 {
  		R1*2
  		\improOn r4 ^ "(Trombone Solo answering vocal)" \repeat unfold 7 { r4 } \improOff
	}
	
	\improOn r4 ^ "Break! (prepare for shout)" \repeat unfold 7 { r4 } \improOff
	
	\break
	r8 ^ "(Shout)" g -> r g -> r g -> r e (
	g4 -- -> ) g8 -> ( \parenthesize e ) fs -- -> ds -^ r4
	r8 g -> r g -> ~ g4 e8 -> ( \parenthesize e )
	g4 -- -> g8 -- fs -^ r2
	
	\break
	r4 e8 ( g -- -> ~ ) g4 e ->
	g16 ( g16 -. ) r8 g16 ( g16 -. ) r8 fs -- ds -^ r4
	\times 2/3 { r8 b ( d } \times 2/3 { e ) b ( d } \times 2/3 { e ) b ( d } \times 2/3 { e ) ds ( cs } 
	c?4 -> ) c4 -> b -> ds8 ( e -> ) \bendAfter #-3
	
	\break
	R1
  	r4 \mp b g \< e
  	c1 \!
  	b4. e,8 \> ~ e4 r \!
	
	R1*4
	
	\break
	R1*8
	
	\bar "||"
	\break
	c''1 \mf ~
	c2 \> r \!
	
	b1 \mf ~
	b2 \> \fermata r \! \fermata
	
	e,1 \fermata
	
	\bar "|."
}



% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

\book {
	\paper { 
		#(set-paper-size "a4" 'landscape)
		indent = 20
		systems-per-page = 2
	}
	
	\score {
		% \midi { \tempo 4 = 110 }
		\layout { #(layout-set-staff-size 20) }
		
		<<
			\scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\new Staff \with { instrumentName = "Trumpet" midiInstrument = #"trumpet" } << \trumpetSoloChords \trumpet >>
				\new Staff \with { instrumentName = "Alto Sax" midiInstrument = #"alto sax" } { \altoSax }
				\new Staff \with { instrumentName = "Tenor Sax" midiInstrument = #"tenor sax" } { \tenorSax }
				\new Staff \with { instrumentName = "Trombone" midiInstrument = #"trombone" } << \tromboneSoloChords \trombone >>
			>>
		>>
	}
}

\book {
	\header { instrument = "Trumpet" }
   \bookOutputName "Minnie the Moocher - Trumpet"
	\score {
		<<
			\scoreMarkup
			\scoreChords
			\transpose bf c \trumpetSoloChords
			\new StaffGroup <<
				\new Staff { \transpose bf c' \trumpet }
			>>
		>>
	}
}

\book {
	\header { instrument = "Alto Sax" }
   \bookOutputName "Minnie the Moocher - Alto Sax"
	\score {
		<<
			\scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\new Staff { \transpose ef c' \altoSax }
			>>
		>>
	}
}

\book {
	\header { instrument = "Tenor Sax" }
   \bookOutputName "Minnie the Moocher - Tenor Sax"
	\score {
		<<
			\scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\new Staff { \transpose bf c'' \tenorSax }
			>>
		>>
	}
}

\book {
	\header { instrument = "Trombone" }
   \bookOutputName "Minnie the Moocher - Trombone"
	\score {
		<<
			\scoreMarkup
			\scoreChords
			\tromboneSoloChords
			\new StaffGroup <<
				\new Staff { \trombone }
			>>
		>>
	}
}