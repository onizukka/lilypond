\version "2.18.2"
\language "english"


% Descripción de funcionalidades
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% PLUGINS
% - \eolMark:					mark al final de línea
% - \bendBefore: 				solución al bendBefore (i.e. dis8 \bendBefore)
% - \rit: 						crea una porción de ritardando que debe terminar como las dinámicas, con un \!
% - \improOn, \improOff:	negra de improvisación
% - \emptyStaff:				Imprime un pentagrama vacío
% - \emptyBar:					Imprime un compás invisible (útil para indentados)
% - \glissBefore:				muestra un glissando hacia la nota
% - \shortGlissBefore:		muestra un glissando corto hacia la nota
% - \jazzTurn:					muestra el simbolo equivalente a un turn en jazz
% - \subp:						muestra "sp", subito piano
% - \ap:							(accidental padding), separa un poco una alteración de la siguiente nota
% 									(útil al usar parenthesize)
% - \optOttava:				octavaje opcional

% LAYOUT
% - El aspecto de los silencios multicompás es como los de las partituras de jazz
% - Margen extra de las articulaciones para evitar la colisión con las ligaduras
% - Glissandos bonitos

% PAPER
% - Se imprime el título, instrumento y número de página en todas las páginas
% - Solo se debe tocar el page-count (si eso). Sale bonito tal cual.

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Changelog de funcionalidades
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Plugins
% 23-09-85: Añadida función \emptyStave para imprimir un pentagrama vacío.
% 				En algunas partituras hay un pentagrama vacío antes del coda (lo cual mola bastante, por cierto)
% 05-09-14: Añadido a la función \emptyStave el esconder la armadura de la clave, que no estaba.
% 10-09-14: Mejorada la función \bendBefore
% 				Ahora es genérica, y se puede definir cualquier texto en un lateral que haga las funciones de ornamento
% 16-09-14: Mejorada la función para imprimir barras de improvisación.
% 				Antes se llamaba al comando \rs para pintar una barra que ocupaba una negra
% 				Se reemplaza \rs por \restImproOn y \restImproOff, siendo esta opción mucho más flexible.
% 				Cuando se activa \restImproOn, todo silencio se convierte en barra de improvisación, respetando su duración.
% 20-09-14: Mejorada la función para imprimir barras de improvisación.
% 				Se reemplaza \restImproOn y \restImproOff por \improOn y \improOff
% 				Ahora, también las notas del pentagrama salen como barra de improvisación.
% 				(los comandos \improvisationOn e \improvisationOff de lilypond no se llevan bien con LilyJAZZ)
% 				Se ha añadido un grob correspondiente a los slash de la fuente LilyJAZZ en el archivo lily-jazz.ly
% 01-10-14: Plugins
% 				Renombrada la función \emptyStave a \emptyStaff (stave es el plural de staff) y 
% 				corregida (ya no muestra el número de compás).
% 				Recordar que después de usar \emptyStaff se necesita redefinir el 
% 				número de compás con \set Score.currentBarNumber = #xx
% 01-10-14: Añadida la función \emptyBar que oculta un compás entero. Se puede usar para crear un 
% 				efecto de indentación en un pentagrama dado.
% 				Recordar que después de usar \emptyBar se necesita:
% 				- redefinir el número de compás con \set Score.currentBarNumber = #xx
% 				- volver a pintar la clave con \clef
% 				- volver a pintar la armadura con \key
% 15-10-14: Se le ha aumentado el grosor de los bordes de la función \boxed
% 18-10-15: Añadidas funciones de ornamento para antes de la nota:
% 				- \glissBefore: para mostrar un glissando antes de una nota
% 				- \shortGlissBefore: para mostrar un glissando corto antes de una nota
% 				- \jazzTurn: para mostrar el turn típico trompetero de jazz
% 24-10-15: Añadida funcion \subp para mostrar subito piano "sp"
% 27-10-15: Añadida funcion \ap (de accidental padding) para separar un poco el accidental de la siguiente nota
% 				al usar un paréntesis con \parenthesize, ya que se solapan el uno al otro
% 27-10-15: Añadida funcion \optOttava para imprimir octavaje opcional
% 				También he aprovechado para cambiar la fuente del texto "8va" que utilizaba Feta por defecto
% 28-10-15: Mejorada la función glissBefore y shortGlissBefore.
% 				Ahora se le pasa una lista #'(r x y) en la que "r" es la rotacion, "x" la posicion horizontal 
%				e "y" la positión vertical.

% Markup
% 23-09-85: Añadidas guías para correcta posición y markup de la indicación de tempo
% 27-10-15: Guía para posición de la indicación de tempo mejorada
% 30-10-15: El texto que se coloque sobre silencios de multicompás POR FIN coge la fuente LilyJAZZ
% 31-10-15: El texto de letras (lyricmode) ya coge la fuente LilyJAZZ

% Layout
% 23-09-85: Ahora las articulaciones van por dentro de las ligaduras. Parece que es la mejor solución
% 04-09-14: Los creciendos y decreciendos tienen la línea un poco más gruesa
% 08-09-14: Ahora no se muestra la cancelación de accidentales al haber un cambio de armadura
% 15-09-14: Añadido un poco más de margen a los reguladores
% 20-09-14: Comentada la línea que oculta la cancelación de accidentales al haber cambio de armadura.
% 				Realmente, la cancelación es muy bienvenida.

% Chords
% 09-09-14: Añadida sentencia para que los acordes menores salgan con un guión en vez de con una M minúscula
% 26-10-14: Añadido el include "chord-exception.ily" que recoge una mejor notación de los acordes.
% 				Actualmente, recoge los acordes maj7, 7(b5), 7(#5), 7(b9), 7(#9), 7(add13), maj9(add13), 9(#11)
% 				Se irá añadiendo acordes a menudo.

% Global
% 15-10-14: Todas las instrucciones que se colocaban justo antes de empezar la música están ahora
% 				recogidas en una función \global.
% 				A \global se le ha añadido instrucciones para que agrupe las corcheas de dos en dos
% 				en vez de cuatro en cuatro (el comportamiento por defecto).

% Language
% 15-10-14: Cambiado el lenguaje de entrada de notas del finlandes al inglés
% 				La diferencia es que ahora los sostenidos se escriben "-s" en vez de "-is"
% 				y los bemoles "-f" en vez de "-es".

% Estructura
% 15-10-14: Para poder autoincluir los archivos de instrumento en otros 
% 				(para montar un midi, o un score), se han movido los plugins a un archivo separado
% 				y ahora se utiliza la función \includeOnce, evitando así el problema de las
% 				dependencias circulares. 
% 18-10-15: Mejorada la manera de trabajar con papeles separados y score al mismo tiempo
% 02-11-15: Actualizado a lilyjazz v2
%				Nuevos archivos de fuente para musica y acordes
%				Mucha limpieza en lilyjazz.ily, chords.ily y main.ly

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Dependencias
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% \includeOnce
% Incluye una dependencia solo una vez. Así se pueden incluir los archivos en otros 
% (para montar un score por ejemplo) sin liarla parda con dependencias cíclicas.

%{
includeOnce = #(
	define-void-function (parser location filename) (string?) (
		if (not (defined? (string->symbol filename))) (
			begin (ly:parser-include-string parser (string-concatenate (list "\\include \"" filename "\"")))
			(primitive-eval (list 'define (string->symbol filename) #t))
		)
	)
)
%}

% includes
\include "../.includes/drums.ily"
\include "../.includes/chords.ily"
\include "../.includes/lilyjazz.ily"
\include "../.includes/plugins-1.2.ily"

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~











% Disposición
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

title = "Feeling Good"

% Cabecera
\header {
	title = #title
	instrument = ""
	composer = "Anthony Newley - Leslie Bricusse"
	arranger = "Arr: Gabriel Palacios"
	poet = ""
	tagline = "gabriel.ps.ms@gmail.com"
}

% Márgenes, fuentes y distancias del papel
\paper {
	
	% distancia entre la parte superior de la página y el título
	top-markup-spacing #'basic-distance = 5

	% distancia entre el título y el primer sistema
	markup-system-spacing #'basic-distance = 25

	% distancia entre el texto y el primer sistema 
	top-system-spacing #'basic-distance = 15

	% márgen inferior
	bottom-margin = 20
	
	% intentado del primer sistema
	indent = 0
	 
	% número de páginas
	% page-count = 1

	ragged-last-bottom = ##f 

	oddHeaderMarkup = \markup {
	 \on-the-fly #not-first-page \fill-line {
		\fromproperty #'page:page-number-string
		\fromproperty #'header:title
		\fromproperty #'header:instrument
	 }
	}

	evenHeaderMarkup = \markup {
	 \on-the-fly #not-first-page \fill-line {
		\fromproperty #'header:instrument
		\fromproperty #'header:title
		\fromproperty #'page:page-number-string
	 }
	}
}

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~







% Elementos comunes de la partitura
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
	% Oculta el primer pentagrama del markup
	\override Staff.VerticalAxisGroup #'remove-first = ##t

	% Indicación de tiempo
	\once \override Score.RehearsalMark.self-alignment-X = #LEFT
	\once \override Score.RehearsalMark.extra-offset = #'(0 . 3) 
	\mark "Recitativo"
	
	s1
	
	s1*8
	\eolMark \markup \box "count-in \". . 3, 4\""
	\once \override Score.RehearsalMark.self-alignment-X = #LEFT
	\mark \markup {
		\column { \boxed "A" }
		\column { "Medium Shuffle" }
	}
	s1*7
	
	\mark \markup \boxed "B"
	s1*18
	
	\mark \markup \boxed "C"
	s1*21
	
	\mark \markup \boxed "D"
	s1*4
	s1 s2
	
	\once \override Score.RehearsalMark.self-alignment-X = #LEFT
	\mark \markup {
		\column { \boxed "E" }
		\column { "(E.Guitar solo)" }
	}
	s1*16
	
	\mark \markup \boxed "F"
	s1*17
	
	\once \override Score.RehearsalMark.self-alignment-X = #LEFT
	\mark \markup "(no rit.)"
	

	% s1*2
	% \once \override Score.RehearsalMark.X-offset = #5
	% \mark \markup \boxed "A"
}

scoreBreaks = \new Staff \with { \RemoveEmptyStaves } {
	
	% Oculta el primer pentagrama del markup
	\override Staff.VerticalAxisGroup #'remove-first = ##t
	s1*9
	
	% A
	\break
	s1*7
	
	% B
	\break
	s1*6
	
	\break
	s1*6
	
	\break
	s1*6
	
	% C
	\break
	s1*9
	
	\break
	s1*6
	
	\break
	s1*6
	
	% D
	\break
	s1*4
	s1*6/4
	
	% E
	\break
	s1*8
	
	\break
	s1*8
	
	% F
	\break
	s1*6
	
	\break
	s1*6
	
	\break
	s1*6
}

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Elementos específicos del instrumento
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Cifrado
scoreChords = \chords {
	
	\partial 1
	s1
	
	s1*8
	
	s1*7
	
	f1/g
	s1
	c:m7/g
	c:7/g
	
	g:m
	s1
	c:m7/g
	c:7/g
	
	f/g
	s1
	c:m7/g
	c:7/g
	
	g2.:m s8 g:m/f
	s2. s8 e:m7.5-
	s2. s8 ef:maj7
	s2 d4:m7 s8 c:m11^5
	s2. s8 bf:5+/af
	s1
	
	g2.:m s8 c:9.5-
	s1
	ef2.:maj7/f \times 2/3 { s8 f4/g }
	
	s1
	s1
	c2.:m7/g s8 a:m/g
	s1
	
	g:m
	s1
	c:m7/g
	c:7/g
	
	f/g
	s1
	c:m7/g
	c:7/g
	
	bf2. s8 g:m
	s2. s8 e:m7.5-
	s2. s8 ef:maj7
	s2 d4:m7 s8 c:m11^5
	s2. s8 bf:5+/af
	s1
	
	s1*4
	s1*6/4
	
	a1:m s1
	f/a s
	a:m6 s
	e:m/a s
	
	a1:m9 s1
	d/a s
	f/a s
	a:m6 s
	
	g1/a
	s1
	d:m7/a
	d:7/a
	
	a:m7
	s1
	d:m7/a
	d:7/a
	
	g/a
	s1
	d:m7/a
	d:7/a
	
	a2.:m s8 a:m/g
	s2. s8 fs:m7.5-
	s2. s8 f:maj7
	s2 e4:m7 s8 d:m11^5
	s2. s8 e:5+.7
	s1
	
	f/g
}

% Música
trumpet = \relative c'' {
  
	\global
	\key g \minor
	
	\partial 1
	r2 r2
	
	\bar "||"
	R1*8 
	
	\tag #'b \break
	R1*3
	r4 ef,4 -> \fp \< ~ ef2
	
	\tag #'b \break
	ef8 \mf -^ r g4 -> _"cresc. - - -" ~ g8 a -> ~ a4
	bf4. -> c8 -^ r bf a d -^ \f
	R1
	
	\tag #'b \break
	R1*18
	
	\tag #'b \break
	g,4 ( \f a bf c8 d -> ~
	d2. ) r4 
	d4 -> ~ \times 2/3 { d8 d, ( f } \times 2/3 { g a d } \times 2/3 { \parenthesize g, ) c4 \sfz -> ~ }
	
	\tag #'b \break
	c1 ~
	c
	bf2. -> g8 c -> ~
	c2. \> r4 \!
	
	\tag #'b \break
	R1*11
	r2 r4 r8 d -> \fp \< ~
	d2. ~ d8 d -^ \f
	R1
	
	\tag #'b \break
	R1*2
	r4 f,4 -> \fp \< ~ f2
	f8 \mf -^ r a4 -> _"cresc. - - -" ~ a8 b -> ~ b4
	c4. -> d8 -^ r c b e -^ \f 
	\time 2/4
	R1*1/2
	
	\tag #'b \break
	\time 4/4
	\key a \minor
	a,,4 -- \mf g8 a -. r g' -. r a -.
	r2 a,2 -> ~
	a4 g8 a-. r d -. r4
	a'4 -. r8 d, c g -> ~ g4
	
	\tag #'b \break
	a4 -- g8 a -. r g' -. r a -.
	r4 r8 a,8 -> ~ a2 ~
	a4 g8 a-. r d -. r4
	a4 -. r8 g -. r g gs4 --
	
	\tag #'b \break
	a4 -- g8 a -. r g' -. r a -.
	r2 a,2 -> ~
	a4 g8 a-. r d -. r4
	a'4 -. r8 d, c g -> ~ g4
	
	\tag #'b \break
	a4 -- g8 a -. r g' -. r a -.
	r4 r8 a,8 -> ~ a2 ~
	a4 g8 a-. r d -. r4
	a4 -. r8 g -. r g gs4 --
	
	\tag #'b \break
	a4 -- \p g8 a -. r g' -. r a -.
	R1
	r2 r8 d, -. r4
	a'4 -. r4 r2
	
	\tag #'b \break
	r2 r8 g -. r a -.
	R1
	r2 r8 d, -. r4
	a4 -. r8 g -. r g gs4 --
	
	\tag #'b \break
	a4 -- g8 a -. r g' -. r a -.
	R1
	r2 r8 d, -. r4
	a'4 -. r4 r2
	
	\tag #'b \break
	R1*3
	r2 r4 r8 e' -> \fp \< ~
	e2. ~ e8 e -^ \f
	R1
	
	R1 \fermataMarkup
	
	
	
	
	\bar "|."
}

altoSax = \relative c' {
  
	\global
	\key g \minor
	
	\partial 1
	g'2 ( \p ~ g8 a bf c
	
	\bar "||"
	d2. \fermata ) \breathe r4
	bf2. \fermata \breathe r4
	g2 ( e
	d2. g4
	
	\tag #'b \break
	c,2 \> ) r \!
	R1*3
	
	\tag #'b \break
	R1*3
	r4 bf4 -> \fp \< ~ bf2
	
	\tag #'b \break
	bf8 \mf -^ r ef4 -> _"cresc. - - -" ~ ef8 f -> ~ f4
	gf4. -> f8 -^ r gf -^ r bf -^ \f
	R1
	
	\tag #'b \break
	R1*18
	
	\tag #'b \break
	g2 ( \f g4 a8 bf -> ~
	bf2. ) r4 
	bf4 -> ~ \times 2/3 { bf8 d, ( f } \times 2/3 { g a d } \times 2/3 { \parenthesize g, ) a4 \sfz -> ~ }
	
	\tag #'b \break
	a1 ~
	a
	g2. -> g8 a -> ~
	a2. \> r4 \!
	
	\tag #'b \break
	R1*11
	r2 r4 r8 bf -> \fp \< ~
	bf2. ~ bf8 bf -^ \f
	R1
	
	\tag #'b \break
	R1*2
	r4 c,4 -> \fp \< ~ c2
	c8 \mf -^ r f4 -> _"cresc. - - -" ~ f8 g -> ~ g4
	a4. -> g8 -^ r af -^ r c -^ \f 
	\time 2/4
	R1*1/2
	
	\tag #'b \break
	\time 4/4
	\key a \minor
	R1*16
	
	\tag #'b \break
	b,1 ( \p ~
	b1
	c1 ~
	c2 ) d (
	
	\tag #'b \break
	c1 ~
	c1 ~
	c1 ~
	c2 ) << { d (
	
	\tag #'b \break
	b1 ~
	b 
	c ~
	c \> ) 
	
	\tag #'b \break
	R1*3 \! } \\ { \hideNotes d4... \glissando b32 \unHideNotes } >>
	r2 r4 r8 c' -> \fp \< ~
	c2. ~ c8 c -^ \f
	R1
	
	R1 \fermataMarkup
	
	\bar "|."
}

tenorSax = \relative c' {
  
	\global
	\key g \minor
	
	\partial 1
	g'2. \p ( ~ g8 a
	
	\bar "||"
	bf2. \fermata ) \breathe r4
	g2. \fermata \breathe r4
	e2 ( d
	bf2 c
	
	\tag #'b \break
	bf2 \> ) r \!
	R1*3
	
	\tag #'b \break
	R1*3
	r4 af4 -> \fp \< ~ af2
	
	\tag #'b \break
	g8 \mf -^ r bf4 -> _"cresc. - - -" ~ bf8 c -> ~ c4
	df4. -> ef8 -^ r df -^ r fs -^ \f
	R1
	
	\tag #'b \break
	R1*18
	
	\tag #'b \break
	g2 ( \f g4. fs8 -> ~
	fs2. ) r4 
	g4 -> ~ \times 2/3 { g8 d ( f } \times 2/3 { g a d } \times 2/3 { \parenthesize g, ) f4 \sfz -> ~ }
	
	\tag #'b \break
	f1 ~
	f
	ef2. -> d8 e -> ~
	e2. \> r4 \!
	
	\tag #'b \break
	R1*11
	r2 r4 r8 f -> \fp \< ~
	f2. ~ f8 fs -^ \f
	R1
	
	\tag #'b \break
	R1*2
	r4 bf,4 -> \fp \< ~ bf2
	a8 \mf -^ r c4 -> _"cresc. - - -" ~ c8 d -> ~ d4
	ef4. -> f8 -^ r ef -^ r gs -^ \f 
	\time 2/4
	R1*1/2
	
	\tag #'b \break
	\time 4/4
	\key a \minor
	R1*16
	
	\tag #'b \break
	g,1 ( \p ~
	g1
	f1 
	fs1
	
	\tag #'b \break
	g1 ~
	g1 )
	f1 (
	fs1
	
	\tag #'b \break
	g1
	g
	a ~
	a \> )
	
	\tag #'b \break
	R1*3 \!
	r2 r4 r8 g' -> \fp \< ~
	g2. ~ g8 gs -^ \f
	R1
	
	R1 \fermataMarkup
	
	\bar "|."
}

trombone = \relative c' {
  
	\global
	\clef bass
	\key g \minor
	
	\partial 1
	g'2 \p ( f
	
	\bar "||"
	e2. \fermata ) \breathe r4
	ef2. \fermata \breathe r4
	d2 ( g,
	f2 e
	
	\tag #'b \break
	ef2 \> ) r \!
	R1*3
	
	\tag #'b \break
	R1*3
	r4 f4 -> \fp \< ~ f2
	
	\tag #'b \break
	f8 \mf -^ r f4 -> _"cresc. - - -" ~ f8 f -> ~ f4
	gf4. -> af8 -^ r bf -^ r c -^ \f
	R1
	
	\tag #'b \break
	R1*18
	
	\tag #'b \break
	g'2 ( \f f4. e8 -> ~
	e2. ) r4 
	ef4 -> ~ \times 2/3 { ef8 d, ( f } \times 2/3 { g a d } \times 2/3 { \parenthesize g, ) c4 \sfz -> ~ }
	
	\tag #'b \break
	c1 ~
	c
	d2. -> c8 g -> ~
	g2. \> r4 \!
	
	\tag #'b \break
	R1*11
	r2 r4 r8 ef' -> \fp \< ~
	ef2. ~ ef8 d -^ \f
	R1
	
	\tag #'b \break
	R1*2
	r4 g,4 -> \fp \< ~ g2
	g8 \mf -^ r g4 -> _"cresc. - - -" ~ g8 g -> ~ g4
	af4. -> bf8 -^ r af -^ r d -^ \f 
	\time 2/4
	R1*1/2
	
	\tag #'b \break
	\time 4/4
	\key a \minor
	a,4 -- \mf g8 a -. r g' -. r a -.
	r2 a,2 -> ~
	a4 g8 a-. r g' -. r4
	a4 -. r8 d, c g -> ~ g4
	
	\tag #'b \break
	a4 -- g8 a -. r g' -. r a -.
	r4 r8 a,8 -> ~ a2 ~
	a4 g8 a-. r d -. r4
	a4 -. r8 g -. r g gs4 --
	
	\tag #'b \break
	a4 -- g8 a -. r g' -. r a -.
	r2 a,2 -> ~
	a4 g8 a-. r g' -. r4
	a4 -. r8 d, c g -> ~ g4
	
	\tag #'b \break
	a4 -- g8 a -. r g' -. r a -.
	r4 r8 a,8 -> ~ a2 ~
	a4 g8 a-. r d -. r4
	a4 -. r8 g -. r g gs4 --
	
	\tag #'b \break
	a4 -- \p g8 a -. r g' -. r a -.
	R1
	r2 r8 g -. r4
	a4 -. r4 r2
	
	\tag #'b \break
	r2 r8 g -. r a -.
	R1
	r2 r8 d, -. r4
	a4 -. r8 g -. r g gs4 --
	
	\tag #'b \break
	a4 -- g8 a -. r g' -. r a -.
	R1
	r2 r8 g -. r4
	a4 -. r4 r2
	
	\tag #'b \break
	R1*3
	r2 r4 r8 f' -> \fp \< ~
	f2. ~ f8 e -^ \f
	R1
	
	R1 \fermataMarkup
	
	\bar "|."
}

piano = \relative c' {
  
	\global
	\key g \minor
	
	\partial 1
	r1
	
	R1*8
	
	\clef bass
	\tag #'b \break
	<g, g'>4 -. \mf r4 r8 <g g'> -> ~ <g g'>4
	<f f'>4 -. r4 r8 <f f'> -> ~ <f f'>4
	<ef ef'>4 -. r4 r8 <ef ef'> -> ~ <ef ef'>4
	<df df'>4 -. r4 r8 <df df'> -> ~ <df df'>4
	
	\tag #'b \break
	<c c'>4 -. r4 _"cresc. - - -" r8 <c c'> -> ~ <c c'>4
	<bf bf'>4 -. r4 r8 <bf bf'> r <af af'> -^ \f
	R1
	
	\tag #'b \break
	\clef treble
	\improOn
	c''1 ~
	c1
	c1
	c1
	
	c1 ~
	c1
	\tag #'b \break
	c1
	c1
	
	c1 ~
	c1
	c1
	c1
	
	\tag #'b \break
	c1 ~
	c2. ~ c8 c8 -> ~ 
	c2. ~ c8 c8 -> ~ 
	c2 c4. c8 -> ~ 
	c2. ~ c8 c8 -^
	R1
	
	\tag #'b \break
	\improOff
	g'4 ( a bf c8 <e, fs bf d> -> ) ~
	<e fs bf d>2. r4
	<ef g bf d>4 ~ 
	\times 2/3 { <ef g bf d>8 \improOff d ( f } \times 2/3 { g a d } \times 2/3 { g, ) <c, f a c>4 \sfz -> ~ }
	
	\tag #'b \break
	<c f a c>1 ~
	<c f a c>1
	\improOn 
	c2. ~ c8 c -> ~
	c1
	
	c1 ~ 
	c1
	\tag #'b \break
	c1
	c1
	
	c1 ~ 
	c1
	c1
	c1
	
	\tag #'b \break
	c1 ~
	c2. ~ c8 c8 -> ~ 
	c2. ~ c8 c8 -> ~ 
	c2 c4. c8 -> ~ 
	c2. ~ c8 c8 -^
	R1
	
	\clef bass
	\tag #'b \break
	\improOff
	<g, g'>4 -. \mf r4 r8 <g g'> -> ~ <g g'>4
	<f f'>4 -. r4 r8 <f f'> -> ~ <f f'>4
	<ef ef'>4 -. r4 r8 <ef ef'> -> ~ <ef ef'>4
	<d d'>4 -. r4 _"cresc. - - -" r8 <d d'> -> ~ <d d'>4
	<c c'>4 -. r4 r8 <c c'> r <bf bf'> -^ \f 
	\time 2/4
	R1*1/2
	
	\time 4/4
	\tag #'b \break
	\improOn
	\repeat unfold 16 { r4 }
	
	\tag #'b \break
	\repeat unfold 16 { r4 }
	
	\tag #'b \break
	\repeat unfold 16 { r4 }
	
	\tag #'b \break
	\repeat unfold 16 { r4 }
	
	\tag #'b \break
	c''1 ~
	c1
	c1
	c1
	
	c1 ~
	c1
	\tag #'b \break
	c1
	c1
	
	c1 ~
	c1
	c1
	c1
	
	\tag #'b \break
	c1 ~
	c2. ~ c8 c8 -> ~ 
	c2. ~ c8 c8 -> ~ 
	
	\tag #'b \break
	c2 c4. c8 -> ~ 
	c2. ~ c8 c8 -^
	R1
	
	c2 \mf \fermata \> s4 s4 \!
	\improOff
	
	\bar "|."
}

guitar = \relative c' {
  
	\global
	\key g \minor
	
	\partial 1
	r1
	
	R1*8
	
	\tag #'b \break
	R1*7
	
	\tag #'b \break
	g4 -- \mf f8 g r f' r g
	r2 g,2 ~
	g4 f8 g r c r4
	g'4 -. r8 c, bf f -> ~ f4
	
	\tag #'b \break
	g4 -- f8 g r f' r g
	r4 r8 g,8 -> ~ g2 ~
	g4 f8 g  r c r4
	g4 -. r8 f r f fs4 --
	
	\tag #'b \break
	g4 -- f8 g r f' r g
	r2 g,2 ~
	g4 f8 g r c r4
	g'4 -. r8 c, bf f -> ~ f4
	
	\tag #'b \break
	g2. g'8 f -> ~
	f2 f4 -. f8 e -> ~
	e2. e8 ef -> ~
	
	\tag #'b \break
	ef2 d4 -. g,8 c -> ~
	c2 ~ c8 c bf af -^
	R1
	
	\tag #'b \break
	g'2 \f g4. c,8 -> ~
	c2 bf8 c r4
	f4 ~ \times 2/3 { f8 d ( f } \times 2/3 { g a d } \times 2/3 { g, ) g,4 \sfz -> ~ }
	
	\tag #'b \break
	g4 \mf f8 g -. r f' r g
	r2 g,2 ~
	g4 f8 g r4 g'8 a
	r4 r8 c, bf f -> ~ f4
	
	\tag #'b \break
	g4 -- f8 g r f' r g
	r4 r8 g,8 -> ~ g2 ~
	g4 f8 g r c r4
	g4 -. r8 f r f fs4 --
	
	\tag #'b \break
	g4 -- f8 g r f' r g
	r2 g,2 ~
	g4 f8 g r c r4
	g'4 -. r8 c, bf f -> ~ f4
	
	\tag #'b \break
	bf2. bf'8 g -> ~
	g2 g4 -. f8 e -> ~
	e2. e8 ef -> ~
	
	\tag #'b \break
	ef2 d4 -. g,8 c -> ~
	c2 ~ c8 \< c bf af -^ \!
	R1
	
	\tag #'b \break
	R1*5
	\time 2/4
	R1*1/2
	
	\time 4/4
	\key a \minor
	\tag #'b \break
	\improOn
	\repeat unfold 16 { r4 }
	
	\tag #'b \break
	\repeat unfold 16 { r4 }
	
	\tag #'b \break
	\repeat unfold 16 { r4 }
	
	\tag #'b \break
	\repeat unfold 16 { r4 }
	
	\improOff
	\tag #'b \break
	a4 -- \mf g8 a r g' r a
	r2 a,2 ~
	a4 g8 a r d r4
	a'4 -. r8 d, c g -> ~ g4
	
	\tag #'b \break
	a4 -- g8 a r g' r a
	r4 r8 a,8 -> ~ a2 ~
	a4 g8 a  r d r4
	a4 -. r8 g r g gs4 --
	
	\tag #'b \break
	a4 -- g8 a r g' r a
	r2 a,2 ~
	a4 g8 a r d r4
	a'4 -. r8 d, c g -> ~ g4
	
	\tag #'b \break
	a2. a'8 g -> ~
	g2 g4 -. g8 fs -> ~
	fs2. fs8 f -> ~
	
	\tag #'b \break
	f2 e4 -. a,8 d -> ~
	d2 ~ d8 \< d ds e -^ \!
	R1
	
	\improOn
	c2 \mf \fermata \> s4 s4 \!
	\improOff
	
	\bar "|."
}

bass = \relative c' {
  
	\global
	\clef bass
	\key g \minor
	
	\partial 1
	r1
	
	R1*8
	
	\tag #'b \break
	g4 -. \mf r4 r8 g -> ~ g4
	f4 -. r4 r8 f -> ~ f4
	ef4 -. r4 r8 ef -> ~ ef4
	df4 -. r4 r8 df -> ~ df4
	
	\tag #'b \break
	c4 -. r4 _"cresc. - - -" r8 c -> ~ c4
	bf4 -. r4 r8 bf r af -^ \f
	R1
	
	\tag #'b \break
	g4 -- \mf f8 g r f' r g
	r2 g,2 ~
	g4 f8 g r f' r4
	g4 -. r8 c, bf f -> ~ f4
	
	\tag #'b \break
	g4 -- f8 g r f' r g
	r4 r8 g,8 -> ~ g2 ~
	g4 f8 g  r c r4
	g4 -. r8 f r f fs4 --
	
	\tag #'b \break
	g4 -- f8 g r f' r g
	r2 g,2 ~
	g4 f8 g r f' r4
	g4 -. r8 c, bf f -> ~ f4
	
	\tag #'b \break
	g2. g'8 f -> ~
	f2 f4 -. f8 e -> ~
	e2. e8 ef -> ~
	
	\tag #'b \break
	ef2 d4 -. g,8 c -> ~
	c2 ~ c8 c bf af -^
	R1
	
	\tag #'b \break
	g'2 \f g4. c,8 -> ~
	c2 bf8 c r4
	f2 g8 -. g \times 2/3 { d g,4 -> ~ }
	
	\tag #'b \break
	g4 \mf f8 g -. r f' r g
	r2 g,2 ~
	g4 f8 g r4 f'8 g
	r4 r8 c, bf f -> ~ f4
	
	\tag #'b \break
	g4 -- f8 g r f' r g
	r4 r8 g,8 -> ~ g2 ~
	g4 f8 g r c r4
	g4 -. r8 f r f fs4 --
	
	\tag #'b \break
	g4 -- f8 g r f' r g
	r2 g,2 ~
	g4 f8 g r f' r4
	g4 -. r8 c, bf f -> ~ f4
	
	\tag #'b \break
	bf2. bf'8 g -> ~
	g2 g4 -. f8 e -> ~
	e2. e8 ef -> ~
	
	\tag #'b \break
	ef2 d4 -. g,8 c -> ~
	c2 ~ c8 \< c bf af -^ \!
	R1
	
	\tag #'b \break
	g'4 -. \mf r4 r8 g -> ~ g4
	f4 -. r4 r8 f -> ~ f4
	ef4 -. r4 r8 ef -> ~ ef4
	d4 -. r4 _"cresc. - - -" r8 d -> ~ d4
	c4 -. r4 r8 c r bf -^ \f 
	\time 2/4
	R1*1/2
	
	\time 4/4
	\key a \minor
	\tag #'b \break
	a4 -- \f g8 a r g' r a
	r2 a,2 ~
	a4 g8 a r g' r4
	a4 -. r8 d, c g -> ~ g4
	
	\tag #'b \break
	a4 -- g8 a r g' r a
	r4 r8 a,8 -> ~ a2 ~
	a4 g8 a  r d r4
	a4 -. r8 g r g gs4 --
	
	\tag #'b \break
	a4 -- g8 a r g' r a
	r2 a,2 ~
	a4 g8 a r g' r4
	a4 -. r8 d, c g -> ~ g4
	
	\tag #'b \break
	a4 -- g8 a r g' r a
	r4 r8 a,8 -> ~ a2 ~
	a4 g8 a  r d r4
	a4 -. \> r8 g r g gs4 --
	
	\tag #'b \break
	a4 -- \mf g8 a r g' r a
	r2 a,2 ~
	a4 g8 a r g' r4
	a4 -. r8 d, c g -> ~ g4
	
	\tag #'b \break
	a4 -- g8 a r g' r a
	r4 r8 a,8 -> ~ a2 ~
	a4 g8 a  r d r4
	a4 -. r8 g r g gs4 --
	
	\tag #'b \break
	a4 -- g8 a r g' r a
	r2 a,2 ~
	a4 g8 a r g' r4
	a4 -. r8 d, c g -> ~ g4
	
	\tag #'b \break
	a2. a'8 g -> ~
	g2 g4 -. g8 fs -> ~
	fs2. fs8 f -> ~
	
	\tag #'b \break
	f2 e4 -. a,8 d -> ~
	d2 ~ d8 \< d ds e -^ \!
	R1
	
	g,1 \mf \fermata
	
	\bar "|."
}

drumsUp = \drummode {
  	
	\revert RestCollision.positioning-done
	\override Rest.staff-position = #6
	
	\override BreathingSign.text = \markup \musicglyph #'"scripts.caesura.curved"
	\dynamicUp
	
	\partial 1
	s1
	
	\tiny dd2. \p \fermata ^\markup "(Fill lightly with cymbals all intro)" \breathe r4
	dd2. \fermata \breathe r4
	dd2 dd
	dd2. dd4
	
	dd2 r2
	
	s1*3
	
	\tag #'b \break
	\crn hho4 hhc8 hhc hho4 hhc8 hhc \crf
	s1*4
	\teeny r2 r8 dd -^ r dd -^ 
	s1
	
	\tag #'b \break
	s1
	r2 dd2
	s1
	dd4 -. r4 r8 dd -> ~ dd4
	
	s1
	r4 r8 dd8 ~ dd2
	\tag #'b \break
	s1
	dd4 -. r4 r8 dd8 dd4 --
	
	s1
	r2 dd2
	s1
	dd4 -. r4 r8 dd -> ~ dd4
	
	\tag #'b \break
	r2 r4 r8 dd -> ~ 
	dd2 dd4 -. r8 dd -> ~
	dd2 r4 r8 dd -> ~ 
	dd2 dd4 -. r8 dd -> ~
	dd2 r4 r8 dd -^
	s1
	
	\tag #'b \break
	dd2 dd4. dd8 -> 
	s1
	dd4 -> ~ \times 2/3 { dd8 dd dd } \times 2/3 { dd dd dd } \times 2/3 { dd dd4 -> ~ }
	
	\tag #'b \break
	dd1
	r2 dd2
	r2 r4 dd8 dd
	r2 r8 dd -> ~ dd4
	
	s1
	r4 r8 dd8 ~ dd2
	\tag #'b \break
	s1
	dd4 -. r4 r8 dd8 dd4 --
	
	s1
	r2 dd2
	s1
	dd4 -. r4 r8 dd -> ~ dd4
	
	\tag #'b \break
	r2 r4 r8 dd -> ~ 
	dd2 dd4 -. r8 dd -> ~
	dd2 r4 r8 dd -> ~ 
	dd2 dd4 -. r8 dd -> ~
	dd2. \fp \< ~ dd8 dd -^ \f
	s1
	
	\tag #'b \break
	s1*4
	r2 r8 dd -^ r dd -^ 
	\time 2/4
	s2
	
	\time 4/4
	\tag #'b \break
	s1
	r2 dd2
	s1
	dd4 -. r4 r8 dd -> ~ dd4
	
	s1
	r4 r8 dd8 ~ dd2
	s1
	dd4 -. r4 r8 dd8 dd4 --
	
	\tag #'b \break
	s1
	r2 dd2
	s1
	dd4 -. r4 r8 dd -> ~ dd4
	
	s1
	r4 r8 dd8 ~ dd2
	s1
	dd4 -. r4 r8 dd8 dd4 --
	
	\tag #'b \break
	s1
	r2 dd2
	s1
	dd4 -. r4 r8 dd -> ~ dd4
	
	s1
	r4 r8 dd8 ~ dd2
	\tag #'b \break
	s1
	dd4 -. r4 r8 dd8 dd4 --
	
	s1
	r2 dd2
	s1
	dd4 -. r4 r8 dd -> ~ dd4
	
	\tag #'b \break
	r2 r4 r8 dd -> ~ 
	dd2 dd4 -. r8 dd -> ~
	dd2 r4 r8 dd -> ~ 
	dd2 dd4 -. r8 dd \fp \< -> ~
	dd2. ~ dd8 dd -^ \f
	s1
	s1
	
	\bar "|."
}

drumsDown = \drummode {

	\global
	
	\stemDown
	\override MultiMeasureRest.staff-position = 2
	\override Stem.extra-offset = #'(.05 . 0)
	\override Beam.extra-offset = #'(.05 . 0)
	
	\partial 1
	r1
	
	R1*8
	bd4 \mf r8 bd sn4 r
	\improOn \repeat percent 3 { r4 r4 r4 r4 }
	r4 r4 _"cresc. - - -" r4 r4
	r4 r4 r4 r4 \f 
	\improOff r2 \improOn \fill { r4 r4 }
	
	\repeat percent 12 { r4 \mf r4 r4 r4 }
	\repeat percent 5  { r4 r4 r4 r4 }
	\improOff r2 \improOn \fill { r4 r4 }
	
	r4 \f r4 r4 r4
	r4 r4 \fill { r4 r4 }
	r4 r4 r4 r4
	
	\repeat percent 12 { r4 \mf r4 r4 r4 }
	\repeat percent 5  { r4 r4 r4 r4 }
	R1
	
	\improOn \repeat percent 3 { r4 \mf r4 r4 r4 }
	r4 r4 _"cresc. - - -" r4 r4
	r4 r4 r4 r4 \f 
	\time 2/4
	\fill { r4 r4 }
	
	\time 4/4
	\repeat percent 15 { r4 \f r4 r4 r4 }
	r4 \> r4 r4 r4
	
	\repeat percent 12 { r4 \mf r4 r4 r4 }
	\repeat percent 5  { r4 r4 r4 r4 }
	R1
	bd2 ^\fermata \mf \> s4 s4 \!
	
	\bar "|."
}

drumsRepeats = \new DrumVoice \with { \RemoveEmptyStaves } {
	% Oculta el primer pentagrama del markup
	\override Staff.VerticalAxisGroup #'remove-first = ##t
	
	s1*9
	
	\override MeasureCounter.direction = #DOWN
	s1*3 \pmc #4
	s1*2 \pmc #7
	
	s1*3 \pmc #4
	s1*3 \pmc #8
	s1*3 \pmc #12
	
	s1*3 \pmc #4
	
	s1*2
	s1*3
	
	s1*3 \pmc #4
	s1*3 \pmc #8
	s1*3 \pmc #12
	
	s1*3 \pmc #4
	s1*2
	
	s1*4
	s1*6/4
	
	s1*3 \pmc #4
	s1*3 \pmc #8
	s1*3 \pmc #12
	s1*3 \pmc #16
	
	s1*3 \pmc #4
	s1*3 \pmc #8
	s1*3 \pmc #12
	
	s1*3 \pmc #4
}

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

\book {
	\header { instrument = "Score" }
	
	\paper { 
		#(set-paper-size "a4" 'landscape)
		left-margin = 20
		right-margin = 20
		systems-per-page = 1
	}
	
	\score {
		% \midi { \tempo 4 = 55 }
		\layout {}
		\removeWithTag #'b
		<<
			\scoreMarkup
			\scoreBreaks
			\new StaffGroup <<
				\new Staff \with { instrumentName = "Trumpet" midiInstrument = #"trumpet" } { \trumpet }
				\new Staff \with { instrumentName = "Alto Sax" midiInstrument = #"alto sax" } { \altoSax }
				\new Staff \with { instrumentName = "Tenor Sax" midiInstrument = #"tenor sax" } { \tenorSax }
				\new Staff \with { instrumentName = "Trombone" midiInstrument = #"trombone" } { \trombone }
			>>
			
			\scoreChords
			\new StaffGroup <<
				\new Staff \with { instrumentName = "Piano" } { \piano }
				\new Staff \with { instrumentName = "Guitar" } { \guitar }
				\new Staff \with { instrumentName = "Bass" } { \bass }
			>>
			
			\new DrumStaff <<
  				\set DrumStaff.drumStyleTable = #(alist->hash-table nwDrums)
				\new DrumVoice { \voiceOne \drumsUp }
				\new DrumVoice { \voiceTwo \drumsDown }
  				\drumsRepeats
			>>
		>>
	}
}

instrumentName = "Trumpet"
\book {
	\header { instrument = #instrumentName }
   \bookOutputName  #(string-append title " - " instrumentName)
   
	\score {
		\transpose bf c' 
		<<
			\scoreMarkup
			\new StaffGroup <<
				\new Staff { \trumpet }
			>>
		>>
	}
}

instrumentName = "Alto Sax"
\book {
	\header { instrument = #instrumentName }
   \bookOutputName  #(string-append title " - " instrumentName)
   
	\score {
		\transpose ef c' 
		<<
			\scoreMarkup
			\new StaffGroup <<
				\new Staff { \altoSax }
			>>
		>>
	}
}

instrumentName = "Tenor Sax"
\book {
	\header { instrument = #instrumentName }
	\paper { page-count = 2 }
   \bookOutputName  #(string-append title " - " instrumentName)
   
	\score {
		\transpose bf c''
		<<
			\scoreMarkup
			\new StaffGroup <<
				\new Staff { \tenorSax }
			>>
		>>
	}
}

instrumentName = "Trombone"
\book {
	\header { instrument = #instrumentName }
   \bookOutputName  #(string-append title " - " instrumentName)
   
	\score {
		<<
			\scoreMarkup
			\new StaffGroup <<
				\new Staff { \trombone }
			>>
		>>
	}
}

instrumentName = "Piano"
\book {
	\header { instrument = #instrumentName }
   \bookOutputName  #(string-append title " - " instrumentName)
   
	\score {
		<<
			\scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\new Staff { \piano }
			>>
		>>
	}
}

instrumentName = "Guitar"
\book {
	\header { instrument = #instrumentName }
   \bookOutputName  #(string-append title " - " instrumentName)
   
	\score {
		<<
			\scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\new Staff { \guitar }
			>>
		>>
	}
}

instrumentName = "Bass"
\book {
	\header { instrument = #instrumentName }
   \bookOutputName  #(string-append title " - " instrumentName)
   
	\score {
		<<
			\scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\new Staff { \bass }
			>>
		>>
	}
}

instrumentName = "Drums"
\book {
	\header { instrument = #instrumentName }
   \bookOutputName  #(string-append title " - " instrumentName)
   
	\score {
		<<
			\scoreMarkup
			\new DrumStaff <<
  				\set DrumStaff.drumStyleTable = #(alist->hash-table nwDrums)
				\new DrumVoice { \voiceOne \drumsUp }
				\new DrumVoice { \voiceTwo \drumsDown }
  				\drumsRepeats
			>>
		>>
	}
}