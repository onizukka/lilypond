chordNameExceptionsMusic = {
	
	%% Acordes mayores
	% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
	
	
	
	
	
	%% Mayores: Acordes de sétpima mayor
	% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
	% maj7
	<c e g b>1 ^\markup \super { 
		\hspace #.4 
		\column { \override #'(baseline-skip . 2.8) \override #'(thickness . .2) \triangle ##f }
	}
	% maj7 9(add13)
	<c e g b d' a'>1 ^\markup \super { 
		\hspace #.4 
		\column { \override #'(baseline-skip . 2.8) \override #'(thickness . .2) \triangle ##f }
		\hspace #.3
		\column { "9(add13)"}
	}
	
	
	%% Mayores: Acordes de séptima menor
	% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
	% 7(b5)
	<c e gf bf>1 ^\markup \super { 
		\hspace #.4
		\column { "7" }
		\hspace #.3
		\column { "(" }
		\hspace #.1
		\column { \vspace #-.2 \tiny \jazzglyph #"accidentals.flatjazz" }
		\column { "5)" }
	}
	% 7(#5)
	<c e gs bf> ^\markup \super { 
		\hspace #.4
		\column { "7" }
		\hspace #.3
		\column { "(" }
		\hspace #.1
		\column { \vspace #-.2 \tiny \jazzglyph #"accidentals.sharpjazz" }
		\column { "5)" }
	}	
	
	
	%% Mayores 7a: Acordes de novena
	% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
	% 9
	<c e gs bf d' f'> ^\markup \super { 
		\hspace #.4
		\column { "9" }
	}
	% 7(b9)
	<c e g bf df'>1 ^\markup \super { 
		\hspace #.4
		\column { "7" }
		\hspace #.3
		\column { "(" }
		\hspace #.1
		\column { \vspace #-.2 \tiny \jazzglyph #"accidentals.flatjazz" }
		\column { "9)" }
	}
	% (add9)
	<c e g d'> ^\markup \super { 
		\hspace #.4
		\column { "(add9)" }
	}
	% 7(#9)
	<c e g bf ds'>1 ^\markup \super { 
		\hspace #.4
		\column { "7" }
		\hspace #.3
		\column { "(" }
		\hspace #.1
		\column { \vspace #-.2 \tiny \jazzglyph #"accidentals.sharpjazz" }
		\column { "9)" }
	}
	
	
	%% Mayores 7a: Acordes de oncena
	% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
	% 7(#11)
	<c e g bf d' fs'> ^\markup \super { 
		\hspace #.4
		\column { "7" }
		\hspace #.3
		\column { "(" }
		\hspace #.1
		\column { \vspace #-.2 \tiny \jazzglyph #"accidentals.sharpjazz" }
		\column { "11)" }
	}
	
	
	%% Acordes de trecena
	% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
	% 13
	<c e g bf d' f' a'>1 ^\markup \super { 
		\hspace #.4
		\column { "13" }
	}
	% 7(add13)
	<c e g bf a'>1 ^\markup \super { 
		\hspace #.4
		\column { "7" }
		\hspace #.3
		\column { "(add13)" }
	}
	
	
	%% Mayores 7a: Acordes compuestos
	% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
	% 7(b13b9)
	<c e g bf df' f' af'>1 ^\markup \super { 
		\hspace #.4
		\column { "7" }
		\hspace #.5
		\column { \huge "(" }
		\center-column {
			\vspace #-.4
			\line {
				\hspace #.6
				\column { \vspace #-.2 \tiny \jazzglyph #"accidentals.flatjazz" }
				\column { "13" }
			}
			\vspace #-.4
			\line {
				\column { \vspace #-.2 \tiny \jazzglyph #"accidentals.flatjazz" }
				\column { "9" }
			}
		}
		\hspace #.1
		\column { \huge ")" }
	}
	
	
	
	
	
	
	%% Acordes menores
	% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
	
	%% Menores: Disminuidos
	% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
	% Semidisminuido
	<c ef gf bf>1 ^\markup {
		\hspace #.4
		\column { \super "o" } 
		\hspace #-.7
		\column { \vspace #.05 \super "/" }
	}
	
	%% Menores: Acordes con oncena
	% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
	% - 11
	<c ef g bf d' f'>1 ^\markup {
		\column { "-" }
		\column { \super "11" } 
	}
}


% Convert music to list and prepend to existing exceptions.
chordNameExceptions = #( append
  ( sequential-music-to-chord-exceptions chordNameExceptionsMusic #t)
  ignatzekExceptions)