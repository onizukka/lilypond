\version "2.18.2"


% Descripción de funcionalidades
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% PLUGINS
% - \eolMark:					mark al final de línea
% - \bendBefore: 				solución al bendBefore (i.e. dis8 \bendBefore)
% - \rit: 						crea una porción de ritardando que debe terminar como las dinámicas, con un \!
% - \improOn, \improOff:	negra de improvisación
% - \emptyStaff:				Imprime un pentagrama vacío
% - \emptyBar:					Imprime un compás invisible (útil para indentados)
% - \glissBefore:				muestra un glissando hacia la nota
% - \shortGlissBefore:		muestra un glissando corto hacia la nota
% - \jazzTurn:					muestra el simbolo equivalente a un turn en jazz
% - \subp:						muestra "sp", subito piano
% - \ap:							(accidental padding), separa un poco una alteración de la siguiente nota
% 									(útil al usar parenthesize)
% - \optOttava:				octavaje opcional

% LAYOUT
% - El aspecto de los silencios multicompás es como los de las partituras de jazz
% - Margen extra de las articulaciones para evitar la colisión con las ligaduras
% - Glissandos bonitos

% PAPER
% - Se imprime el título, instrumento y número de página en todas las páginas
% - Solo se debe tocar el page-count (si eso). Sale bonito tal cual.

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Changelog de funcionalidades
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Plugins
% 23-09-85: Añadida función \emptyStave para imprimir un pentagrama vacío.
% 				En algunas partituras hay un pentagrama vacío antes del coda (lo cual mola bastante, por cierto)
% 05-09-14: Añadido a la función \emptyStave el esconder la armadura de la clave, que no estaba.
% 10-09-14: Mejorada la función \bendBefore
% 				Ahora es genérica, y se puede definir cualquier texto en un lateral que haga las funciones de ornamento
% 16-09-14: Mejorada la función para imprimir barras de improvisación.
% 				Antes se llamaba al comando \rs para pintar una barra que ocupaba una negra
% 				Se reemplaza \rs por \restImproOn y \restImproOff, siendo esta opción mucho más flexible.
% 				Cuando se activa \restImproOn, todo silencio se convierte en barra de improvisación, respetando su duración.
% 20-09-14: Mejorada la función para imprimir barras de improvisación.
% 				Se reemplaza \restImproOn y \restImproOff por \improOn y \improOff
% 				Ahora, también las notas del pentagrama salen como barra de improvisación.
% 				(los comandos \improvisationOn e \improvisationOff de lilypond no se llevan bien con LilyJAZZ)
% 				Se ha añadido un grob correspondiente a los slash de la fuente LilyJAZZ en el archivo lily-jazz.ly
% 01-10-14: Plugins
% 				Renombrada la función \emptyStave a \emptyStaff (stave es el plural de staff) y 
% 				corregida (ya no muestra el número de compás).
% 				Recordar que después de usar \emptyStaff se necesita redefinir el 
% 				número de compás con \set Score.currentBarNumber = #xx
% 01-10-14: Añadida la función \emptyBar que oculta un compás entero. Se puede usar para crear un 
% 				efecto de indentación en un pentagrama dado.
% 				Recordar que después de usar \emptyBar se necesita:
% 				- redefinir el número de compás con \set Score.currentBarNumber = #xx
% 				- volver a pintar la clave con \clef
% 				- volver a pintar la armadura con \key
% 15-10-14: Se le ha aumentado el grosor de los bordes de la función \boxed
% 18-10-15: Añadidas funciones de ornamento para antes de la nota:
% 				- \glissBefore: para mostrar un glissando antes de una nota
% 				- \shortGlissBefore: para mostrar un glissando corto antes de una nota
% 				- \jazzTurn: para mostrar el turn típico trompetero de jazz
% 24-10-15: Añadida funcion \subp para mostrar subito piano "sp"
% 27-10-15: Añadida funcion \ap (de accidental padding) para separar un poco el accidental de la siguiente nota
% 				al usar un paréntesis con \parenthesize, ya que se solapan el uno al otro
% 27-10-15: Añadida funcion \optOttava para imprimir octavaje opcional
% 				También he aprovechado para cambiar la fuente del texto "8va" que utilizaba Feta por defecto
% 28-10-15: Mejorada las funciones glissBefore y shortGlissBefore.
% 				Ahora se le pasa una lista #'(r x y) en la que "r" es la rotacion, "x" la posicion horizontal 
%				e "y" la positión vertical.

% Markup
% 23-09-85: Añadidas guías para correcta posición y markup de la indicación de tempo
% 27-10-15: Guía para posición de la indicación de tempo mejorada

% Layout
% 23-09-85: Ahora las articulaciones van por dentro de las ligaduras. Parece que es la mejor solución
% 04-09-14: Los creciendos y decreciendos tienen la línea un poco más gruesa
% 08-09-14: Ahora no se muestra la cancelación de accidentales al haber un cambio de armadura
% 15-09-14: Añadido un poco más de margen a los reguladores
% 20-09-14: Comentada la línea que oculta la cancelación de accidentales al haber cambio de armadura.
% 				Realmente, la cancelación es muy bienvenida.

% Chords
% 09-09-14: Añadida sentencia para que los acordes menores salgan con un guión en vez de con una M minúscula
% 26-10-14: Añadido el include "chord-exception.ily" que recoge una mejor notación de los acordes.
% 				Actualmente, recoge los acordes maj7, 7(b5), 7(#5), 7(b9), 7(#9), 7(add13), maj9(add13), 9(#11)
% 				Se irá añadiendo acordes a menudo.

% Global
% 15-10-14: Todas las instrucciones que se colocaban justo antes de empezar la música están ahora
% 				recogidas en una función \global.
% 				A \global se le ha añadido instrucciones para que agrupe las corcheas de dos en dos
% 				en vez de cuatro en cuatro (el comportamiento por defecto).

% Language
% 15-10-14: Cambiado el lenguaje de entrada de notas del finlandes al inglés
% 				La diferencia es que ahora los sostenidos se escriben "-s" en vez de "-is"
% 				y los bemoles "-f" en vez de "-es".

% Estructura
% 15-10-14: Para poder autoincluir los archivos de instrumento en otros 
% 				(para montar un midi, o un score), se han movido los plugins a un archivo separado
% 				y ahora se utiliza la función \includeOnce, evitando así el problema de las
% 				dependencias circulares. 
% 18-10-15: Mejorada la manera de trabajar con papeles separados y score al mismo tiempo

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Dependencias
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% \includeOnce
% Incluye una dependencia solo una vez. Así se pueden incluir los archivos en otros 
% (para montar un score por ejemplo) sin liarla parda con dependencias cíclicas.

includeOnce = #(
	define-void-function (parser location filename) (string?) (
		if (not (defined? (string->symbol filename))) (
			begin (ly:parser-include-string parser (string-concatenate (list "\\include \"" filename "\"")))
			(primitive-eval (list 'define (string->symbol filename) #t))
		)
	)
)



% LilyJAZZ
\includeOnce "includes/ignatzek-jazz-chords.ily"
\includeOnce "includes/lily-jazz.ily"

% Language
\language "english"

% Plugins
\includeOnce "includes/plugins-1.2.ily"

% Chord Exceptions
\includeOnce "includes/chord-exceptions.ily"

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~











% Disposición
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Cabecera
\header {
	title = "Smile"
	subtitle = "(as performed by Tony Bennett & Barbra Streisand)"
	instrument = ""
	composer = "Charles Chaplin - John Turner - Geoffrey Parsons"
	arranger = "Arr: Gabriel Palacios"
	poet = ""
	tagline = "gabriel.ps.ms@gmail.com"
}

% Tamaño de las notas y pentagrama
#(set-global-staff-size 20)

% Márgenes, fuentes y distancias del papel
\paper {
	% El definir otro tipo de fuente no permite cambiar el tamaño global del staff. Es un bug conocigo
	#(define fonts (make-pango-font-tree "LilyJAZZText" "Nimbus Sans" "Luxi Mono" (/ 20 20)))

	% distancia entre la parte superior de la página y el título
	top-markup-spacing #'basic-distance = 5

	% distancia entre el título y el primer sistema
	markup-system-spacing #'basic-distance = 25

	% distancia entre el texto y el primer sistema 
	top-system-spacing #'basic-distance = 15

	% márgen inferior
	bottom-margin = 20
	
	% intentado del primer sistema
	indent = 0
	 
	% número de páginas
	% page-count = 1

	ragged-last-bottom = ##f 

	oddHeaderMarkup = \markup {
	 \on-the-fly #not-first-page \fill-line {
		\fromproperty #'page:page-number-string
		\fromproperty #'header:title
		\fromproperty #'header:instrument
	 }
	}

	evenHeaderMarkup = \markup {
	 \on-the-fly #not-first-page \fill-line {
		\fromproperty #'header:instrument
		\fromproperty #'header:title
		\fromproperty #'page:page-number-string
	 }
	}
}

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~







% Elementos comunes de la partitura
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
	% Oculta el primer pentagrama del markup
	\override Staff.VerticalAxisGroup #'remove-first = ##t

	% Indicación de tiempo
	\once \override Score.RehearsalMark.self-alignment-X = #LEFT
	\once \override Score.RehearsalMark.extra-offset = #'(0 . 3) 
	\mark \markup "Slow Ballad"

	% s1*2
	% \once \override Score.RehearsalMark.X-offset = #5
	% \mark \markup \boxed "A"
	
	s1*8
	\mark \markup \boxed "A"
	
	s1*8
	\mark \markup \boxed "B"
	
	s1*12
	\mark \markup \boxed "C"
	
	s1*12
	\mark \markup \boxed "D"
}



% Función musical global
global = {
	% Para pintar todos los números de compás debajo del pentagrama
	% \override Score.BarNumber #'break-visibility = #end-of-line-invisible
	% \override Score.BarNumber #'direction = #DOWN
	% \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

	\jazzOn
	\compressFullBarRests

	% Arregla el problema de tener el scoreMarkup añadido
	\revert MultiMeasureRest #'staff-position
	\override Voice.Rest #'staff-position = #0	

	\set Timing.beamExceptions = #'()
	\set Timing.baseMoment = #(ly:make-moment 1/4)
	\set Timing.beatStructure = #'(1 1 1 1) 
	
	\override ParenthesesItem.font-size = #0
}

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Elementos específicos del instrumento
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Cifrado
scoreChords = \chords {    
	\set minorChordModifier = \markup { "-" }
	\set chordNameExceptions = #chordNameExceptions
	\override ChordName.font-size = #2.5

	% c1:m f2:m g2:m7
}

% Música
trumpet = \relative c'' {
  
	\global
	\key fs \major
	
	R1*7
	r2 b2 ( \pp \<
	
	\break
	\key ef \major
	bf?1 \p
	c )
	d ( 
	ef2 ) c (
	
	\break
	bf1
	a2 ) b4 \< ( c 
	ef2 \mp \> ef, \p
	df ) f4 ( g
	
	\break
	ef1
	g2 ~ g8 ) af \< ( bf g
	ef'1 \mf
	ef2 ~ ef8 ) ef4 ( f8
	
	\break
	d2 \> ) r2 \!
	ef,2 \p d4 \< ef
	f1 \mf
	g2 g \>
	
	\break
	R1 \!
	gf2 ( \p ef4 f4
	g2 ) \times 2/3 { g4 ( \< af bf }
	b1
	
	\break
	\key g \major
	a2. \mf \> ) r4 \!
	c8 ( \p d e2. \> )
	r8 \p g8 ( -> ~ g4 ~ g2 ~
	g2 e
	
	\break
	d2. \> ) r4 \!
	r2 r8 \mf fs ( ds e
	g2 c,4 g'
	fs2 f4 gs, 
	
	\break
	e'2. \> ) r4 \!
	a,2 ( \p \< b 
	c4 \mf b bf2 )
	b?4 ( \> a g a 
	
	\break
	d,2 \p ) af'4 ( g
	fs2 ds
	e2. \> ) r4 \!
	R1 \fermataMarkup
	
	\break
	<< { \override Rest #'staff-position = #0	
	r4 d2.
	r4 d2. } \\ { s4 s2 \pp \< s4 \mf s4 s2 \pp \< s4 \mf } >> 
	\override BreathingSign.text = \markup { \musicglyph #"scripts.caesura.curved" } \breathe
	d1 \p \fermata
   
	\bar "|."
}

altoSax = \relative c' {
  
	\global
	\key fs \major
	
	R1*7
	r2 g'2 ( \pp \<
	
	\break
	\key ef \major
	g1 \p
	af )
	b ( 
	c2 ) g4 ( gf
	
	\break
	f1
	gf2 ) << { a ( \<
	af?4 \mf bf g af
	e2 \> ) f4 \p ( e
	
	\break
	c1
	ef?1 )
	gf1 ( \mf
	gf4 af ~ af8 ) bf4 ( af8
	
	\break
	f4 g? \> ) r2 \! } \\ { s2 s1 s1 s1 s2 s8 s4. \< s1 \! } >>
	bf,2 \p a2 \<
	af?1 \mf
	ef'2 ( d \> )
	
	\break
	r4 \! c2. ( \pp \<
	ef2 \p cf
	f4 ef ) \times 2/3 { ef ( \< f ef }
	e2 ef
	
	\break
	\key g \major
	d2. \> ) r4 \!
	e4 ( \p \glissando g2. \> )
	r8 \p bf8 ( -> ~ bf4 ~ bf2
	b?4 c b c
	
	\break
	fs,2. \< ~ fs8 ) \mf b,
	e2 ( ~ e8 ds fs e ~
	e1 )
	d?2 g8 f ~ f4
	
	\break
	e2. \> r4 \!
	c2 ( \p \< d 
	ef1 \mf )
	ef1 ( \>
	
	\break
	d2 \p ) ds (
	d?2 cs
	c?2. \> ) r4 \!
	R1 \fermataMarkup
	
	\break
	<< { \override Rest #'staff-position = #0
	r4 a2.
	r4 c2. } \\ { s4 s2 \pp \< s4 \mf s4 s2 \pp \< s4 \mf } >>
	\override BreathingSign.text = \markup { \musicglyph #"scripts.caesura.curved" } \breathe
	b1 \p \fermata
	
	\bar "|."
}

tenorSax = \relative c' {
  
	\global
	\key fs \major
	
	R1*7
	r2 d2 ( \pp \<
	
	\break
	\key ef \major
	d1 \p
	ef )
	ef ( 
	g2 ) ef2 ( 
	
	\break
	g1
	ef2 ) gf (
	f2 c 
	bf2 ) << { df (
	
	\break
	af1
	c1 )
	b ( \mf
	b2 ~ b8 ) f'8 ( ~ f4
	
	\break
	d4. ) } \\ { s2 s1 s2 s8 s4. \< s1 \! } >> bf8 ( b4 ef
	d c b c
	c1 )
	df2 ( cf ) \>
	
	\break
	r4 \! g2. ( \pp \<
	bf4 \p af bf af
	bf2 ) c \< ( 
	c1
	
	\break
	\key g \major
	b2. \mf \> ) r4 \!
	g'4 ( \p \glissando c2. \> )
	r8 \p df8 ( -> ~ df4 ~ df2
	d?2 fs,
	
	\break
	e2. \> ) r4 \!
	cs1 ( \p \<
	c?1 \mf )
	cs2 b
	
	\break
	c?2. \> r4 \!
	g2 ( \p \< a 
	bf4 \mf b d4 c )
	g1 ( \>
	
	\break
	fs2 \p ) b (
	a2 g
	g2. \> ) r4 \!
	R1 \fermataMarkup
	
	\break
	<< { \override Rest #'staff-position = #0	
	r4 g2.
	r4 g2. } \\ { s4 s2 \pp \< s4 \mf s4 s2 \pp \< s4 \mf } >>
	\override BreathingSign.text = \markup { \musicglyph #"scripts.caesura.curved" } \breathe
	fs1 \p \fermata
	
	\bar "|."
}

trombone = \relative c' {
  
	\global
	\clef bass
	\key fs \major
	
	R1*7
	r2 bf,2 \pp \< ~
	
	\break
	\key ef \major
	bf4. \! ef16 \mf ( f g8 d' ~ d bf
	c4 f,2. ~
	f8 ) f ( gf a d b ~ b c16 d 
	ef4 af,2. )
	
	\break
	f'4 ( ef \times 2/3 { d f ef }
	d4 c ef2 ) \>
	c2 ( \p bf4 f
	a2 ) << { c4 ( bf 
	
	\break
	g1 
	af? ) 
	bf4 \mf ( af gf2 
	f2 ~ f8 ) } \\ { s2 s1 s2 s8 s4. \< s1 \! } >> b8 ( ~ b4
	
	\break
	bf?2 \> ) f \p (
	g f \<
	ef1 \mf )
	a2 ( af \> )
	
	\break
	r4 \! ef2. ( \pp \<
	cf'2 \p f,4 ef
	d4 ef ) af2 ( \<
	g fs
	
	\break
	\key g \major
	e2. \mf \> ) r4 \!
	g'8 ( \p a b2. \> )
	r8 \p e,8 ( -> ~ e4 ~ e2 ~
	e2 c4 bf
	
	\break
	a2. \> ) r4 \!
	g1 \p \< ~ 
	g8 e ( \mf b' g e4. g8
	gs2 a4 gs
	
	\break
	b2. \> ) r4 \!
	e,2 ( \p \< fs
	g1 \mf )
	f2 ( f4 ef
	
	\break
	d2 ) b (
	e2 a,
	d2. \> ) r4 \!
	R1 \fermataMarkup
	
	\break
	<< { \override Rest #'staff-position = #0	
	r4 e2.
	r4 ef2. } \\ { s4 s2 \pp \< s4 \mf s4 s2 \pp \< s4 \mf } >>
	\override BreathingSign.text = \markup { \musicglyph #"scripts.caesura.curved" } \breathe
	d1 \p \fermata
	
	\bar "|."
}

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

\book {
	\paper { 
		#(set-paper-size "a4" 'landscape)
		indent = 20
		systems-per-page = 2
	}
	
	\score {
		% \midi { \tempo 4 = 60 }
		\layout { #(layout-set-staff-size 20) }
		
		<<
			\scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\new Staff \with { instrumentName = "Trumpet" midiInstrument = #"trumpet" } { \trumpet }
				\new Staff \with { instrumentName = "Alto Sax" midiInstrument = #"alto sax" } { \altoSax }
				\new Staff \with { instrumentName = "Tenor Sax" midiInstrument = #"tenor sax" } { \tenorSax }
				\new Staff \with { instrumentName = "Trombone" midiInstrument = #"trombone" } { \trombone }
			>>
		>>
	}
}

\book {
	\header { instrument = "Trumpet" }
   \bookOutputName "Smile - Trumpet"
	\score {
		<<
			\scoreMarkup
			\transpose bf c \scoreChords
			\new StaffGroup <<
				\new Staff { \transpose bf c' \trumpet }
			>>
		>>
	}
}
\book {
	\header { instrument = "Alto Sax" }
   \bookOutputName "Smile - Alto Sax"
	\score {
		<<
			\scoreMarkup
			\transpose ef c \scoreChords
			\new StaffGroup <<
				\new Staff { \transpose ef c' \altoSax }
			>>
		>>
	}
}

\book {
	\header { instrument = "Tenor Sax" }
   \bookOutputName "Smile - Tenor Sax"
	\score {
		<<
			\scoreMarkup
			\transpose bf c \scoreChords
			\new StaffGroup <<
				\new Staff { \transpose bf c'' \tenorSax }
			>>
		>>
	}
}

\book {
	\header { instrument = "Trombone" }
   \bookOutputName "Smile - Trombone"
	\score {
		<<
			\scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\new Staff { \trombone }
			>>
		>>
	}
}