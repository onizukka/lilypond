\version "2.18.2"


% Descripción de funcionalidades
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% PLUGINS
% - \eolMark:					mark al final de línea
% - \bendBefore: 				solución al bendBefore (i.e. dis8 \bendBefore)
% - \rit: 						crea una porción de ritardando que debe terminar como las dinámicas, con un \!
% - \improOn, \improOff:	negra de improvisación
% - \emptyStaff:				Imprime un pentagrama vacío
% - \emptyBar:					Imprime un compás invisible (útil para indentados)
% - \glissBefore:				muestra un glissando hacia la nota
% - \shortGlissBefore:		muestra un glissando corto hacia la nota
% - \jazzTurn:					muestra el simbolo equivalente a un turn en jazz
% - \subp:						muestra "sp", subito piano
% - \ap:							(accidental padding), separa un poco una alteración de la siguiente nota
% 									(útil al usar parenthesize)
% - \optOttava:				octavaje opcional

% LAYOUT
% - El aspecto de los silencios multicompás es como los de las partituras de jazz
% - Margen extra de las articulaciones para evitar la colisión con las ligaduras
% - Glissandos bonitos

% PAPER
% - Se imprime el título, instrumento y número de página en todas las páginas
% - Solo se debe tocar el page-count (si eso). Sale bonito tal cual.

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Changelog de funcionalidades
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Plugins
% 23-09-85: Añadida función \emptyStave para imprimir un pentagrama vacío.
% 				En algunas partituras hay un pentagrama vacío antes del coda (lo cual mola bastante, por cierto)
% 05-09-14: Añadido a la función \emptyStave el esconder la armadura de la clave, que no estaba.
% 10-09-14: Mejorada la función \bendBefore
% 				Ahora es genérica, y se puede definir cualquier texto en un lateral que haga las funciones de ornamento
% 16-09-14: Mejorada la función para imprimir barras de improvisación.
% 				Antes se llamaba al comando \rs para pintar una barra que ocupaba una negra
% 				Se reemplaza \rs por \restImproOn y \restImproOff, siendo esta opción mucho más flexible.
% 				Cuando se activa \restImproOn, todo silencio se convierte en barra de improvisación, respetando su duración.
% 20-09-14: Mejorada la función para imprimir barras de improvisación.
% 				Se reemplaza \restImproOn y \restImproOff por \improOn y \improOff
% 				Ahora, también las notas del pentagrama salen como barra de improvisación.
% 				(los comandos \improvisationOn e \improvisationOff de lilypond no se llevan bien con LilyJAZZ)
% 				Se ha añadido un grob correspondiente a los slash de la fuente LilyJAZZ en el archivo lily-jazz.ly
% 01-10-14: Plugins
% 				Renombrada la función \emptyStave a \emptyStaff (stave es el plural de staff) y 
% 				corregida (ya no muestra el número de compás).
% 				Recordar que después de usar \emptyStaff se necesita redefinir el 
% 				número de compás con \set Score.currentBarNumber = #xx
% 01-10-14: Añadida la función \emptyBar que oculta un compás entero. Se puede usar para crear un 
% 				efecto de indentación en un pentagrama dado.
% 				Recordar que después de usar \emptyBar se necesita:
% 				- redefinir el número de compás con \set Score.currentBarNumber = #xx
% 				- volver a pintar la clave con \clef
% 				- volver a pintar la armadura con \key
% 15-10-14: Se le ha aumentado el grosor de los bordes de la función \boxed
% 18-10-15: Añadidas funciones de ornamento para antes de la nota:
% 				- \glissBefore: para mostrar un glissando antes de una nota
% 				- \shortGlissBefore: para mostrar un glissando corto antes de una nota
% 				- \jazzTurn: para mostrar el turn típico trompetero de jazz
% 24-10-15: Añadida funcion \subp para mostrar subito piano "sp"
% 27-10-15: Añadida funcion \ap (de accidental padding) para separar un poco el accidental de la siguiente nota
% 				al usar un paréntesis con \parenthesize, ya que se solapan el uno al otro
% 27-10-15: Añadida funcion \optOttava para imprimir octavaje opcional
% 				También he aprovechado para cambiar la fuente del texto "8va" que utilizaba Feta por defecto
% 28-10-15: Mejorada las funciones glissBefore y shortGlissBefore.
% 				Ahora se le pasa una lista #'(r x y) en la que "r" es la rotacion, "x" la posicion horizontal 
%				e "y" la positión vertical.

% Markup
% 23-09-85: Añadidas guías para correcta posición y markup de la indicación de tempo
% 27-10-15: Guía para posición de la indicación de tempo mejorada

% Layout
% 23-09-85: Ahora las articulaciones van por dentro de las ligaduras. Parece que es la mejor solución
% 04-09-14: Los creciendos y decreciendos tienen la línea un poco más gruesa
% 08-09-14: Ahora no se muestra la cancelación de accidentales al haber un cambio de armadura
% 15-09-14: Añadido un poco más de margen a los reguladores
% 20-09-14: Comentada la línea que oculta la cancelación de accidentales al haber cambio de armadura.
% 				Realmente, la cancelación es muy bienvenida.

% Chords
% 09-09-14: Añadida sentencia para que los acordes menores salgan con un guión en vez de con una M minúscula
% 26-10-14: Añadido el include "chord-exception.ily" que recoge una mejor notación de los acordes.
% 				Actualmente, recoge los acordes maj7, 7(b5), 7(#5), 7(b9), 7(#9), 7(add13), maj9(add13), 9(#11)
% 				Se irá añadiendo acordes a menudo.

% Global
% 15-10-14: Todas las instrucciones que se colocaban justo antes de empezar la música están ahora
% 				recogidas en una función \global.
% 				A \global se le ha añadido instrucciones para que agrupe las corcheas de dos en dos
% 				en vez de cuatro en cuatro (el comportamiento por defecto).

% Language
% 15-10-14: Cambiado el lenguaje de entrada de notas del finlandes al inglés
% 				La diferencia es que ahora los sostenidos se escriben "-s" en vez de "-is"
% 				y los bemoles "-f" en vez de "-es".

% Estructura
% 15-10-14: Para poder autoincluir los archivos de instrumento en otros 
% 				(para montar un midi, o un score), se han movido los plugins a un archivo separado
% 				y ahora se utiliza la función \includeOnce, evitando así el problema de las
% 				dependencias circulares. 
% 18-10-15: Mejorada la manera de trabajar con papeles separados y score al mismo tiempo

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Dependencias
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% \includeOnce
% Incluye una dependencia solo una vez. Así se pueden incluir los archivos en otros 
% (para montar un score por ejemplo) sin liarla parda con dependencias cíclicas.

includeOnce = #(
	define-void-function (parser location filename) (string?) (
		if (not (defined? (string->symbol filename))) (
			begin (ly:parser-include-string parser (string-concatenate (list "\\include \"" filename "\"")))
			(primitive-eval (list 'define (string->symbol filename) #t))
		)
	)
)



% LilyJAZZ
\includeOnce "includes/ignatzek-jazz-chords.ily"
\includeOnce "includes/lily-jazz.ily"

% Language
\language "english"

% Plugins
\includeOnce "includes/plugins-1.2.ily"

% Chord Exceptions
\includeOnce "includes/chord-exceptions.ily"

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~











% Disposición
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Cabecera
\header {
	title = "Sway"
	subtitle = "(\"Quién será\" as performed by Michael Bublé)"
	instrument = ""
	composer = "Pablo Beltrán Ruiz - Luis Demetrio"
	arranger = "Arr: Gabriel Palacios"
	poet = ""
	tagline = "gabriel.ps.ms@gmail.com"
}

% Tamaño de las notas y pentagrama
#(set-global-staff-size 20)

% Márgenes, fuentes y distancias del papel
\paper {
	% El definir otro tipo de fuente no permite cambiar el tamaño global del staff. Es un bug conocigo
	#(define fonts (make-pango-font-tree "LilyJAZZText" "Nimbus Sans" "Luxi Mono" (/ 20 20)))

	% distancia entre la parte superior de la página y el título
	top-markup-spacing #'basic-distance = 5

	% distancia entre el título y el primer sistema
	markup-system-spacing #'basic-distance = 25

	% distancia entre el texto y el primer sistema 
	top-system-spacing #'basic-distance = 15

	% márgen inferior
	bottom-margin = 20
	
	% intentado del primer sistema
	indent = 0
	 
	% número de páginas
	% page-count = 1

	ragged-last-bottom = ##f 

	oddHeaderMarkup = \markup {
	 \on-the-fly #not-first-page \fill-line {
		\fromproperty #'page:page-number-string
		\fromproperty #'header:title
		\fromproperty #'header:instrument
	 }
	}

	evenHeaderMarkup = \markup {
	 \on-the-fly #not-first-page \fill-line {
		\fromproperty #'header:instrument
		\fromproperty #'header:title
		\fromproperty #'page:page-number-string
	 }
	}
}

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~







% Elementos comunes de la partitura
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
	% Oculta el primer pentagrama del markup
	\override Staff.VerticalAxisGroup #'remove-first = ##t

	% Indicación de tiempo
	\once \override Score.RehearsalMark.self-alignment-X = #LEFT
	\once \override Score.RehearsalMark.extra-offset = #'(0 . 3) 
	\mark \markup "Medium Latin"

	% s1*2
	% \once \override Score.RehearsalMark.X-offset = #5
	% \mark \markup \boxed "A"
	
	s4.
	
	s1*8
	\mark \markup \boxed "A"
	
	s1*8
	\mark \markup \boxed "B"
	
	s1*8
	\mark \markup \boxed "C"
	
	s1*8
	\mark \markup \boxed "D"
	
	s1*8
	\once \override Score.RehearsalMark.self-alignment-X = #LEFT
	\mark \markup {
		\column { \boxed "E" }
		\column { "(Open Solos: Trumpet first)" }
	}
	
	s1*18
	\mark \markup \boxed "F"
	
	s1*8
	\mark \markup \boxed "G"
	
	s1*8
	\mark \markup \boxed "H"
	
	s1*9
	\mark \markup \boxed "I"
	
	s1*8
	\mark \markup \boxed "J"
}



% Función musical global
global = {
	% Para pintar todos los números de compás debajo del pentagrama
	% \override Score.BarNumber #'break-visibility = #end-of-line-invisible
	% \override Score.BarNumber #'direction = #DOWN
	% \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

	\jazzOn
	\compressFullBarRests

	% Arregla el problema de tener el scoreMarkup añadido
	\revert MultiMeasureRest #'staff-position
	\override Voice.Rest #'staff-position = #0	

	\set Timing.beamExceptions = #'()
	\set Timing.baseMoment = #(ly:make-moment 1/4)
	\set Timing.beatStructure = #'(1 1 1 1) 
	
	\override ParenthesesItem.font-size = #0
}

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Elementos específicos del instrumento
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Cifrado
scoreChords = \chords {    
	\set minorChordModifier = \markup { "-" }
	\set chordNameExceptions = #chordNameExceptions
	\override ChordName.font-size = #2.5

	% c1:m f2:m g2:m7
}

trumpetBreakChords = \chords {
	\set minorChordModifier = \markup { "-" }
	\set chordNameExceptions = #chordNameExceptions
	\override ChordName.font-size = #2.5
	
	s4.
	
	s1*39
	d1:m6
}

soloChords = \chords {
	\set minorChordModifier = \markup { "-" }
	\set chordNameExceptions = #chordNameExceptions
	\override ChordName.font-size = #2.5

	s4.
	
	s1*40
	
	e2:m5-.7 a:7
	e2:m5-.7 a:7
	d1:m6
	s1
	
	e2:m5-.7 a:7
	e2:m5-.7 a:7
	d1:m6
	s1
	
	e2:m5-.7 a:7
	e2:m5-.7 a:7
	d1:m6
	s1
	
	e2:m5-.7 a:7
	e2:m5-.7 a:7
	d1:m6
	s1
}


% Música
trumpet = \relative c'' {
  
	\global
	\key d \minor

	\partial 4.
	
	a8 \f -. a -. a -.

	\bbp bf4 \bendBefore a8 -- g -. r4 r8 a -.
	\bbp bf4 \bendBefore a8 -- g -. r g -. f -. g -.
	\bbp a4 \bendBefore g8 -- f -. r4 d'16 -^ d8 -^ d16 -^
	r2 r8 a -. g -. a -.
	
	\break
	bf4 -- a8 -- g -. r2
	bf4 -- a8 -- \jazzTurn g -. r cs -^ r g -.
	a4 -- g8 -- f -. r4 a ( \glissando
	d4 -^ ) r4 r2
	
	\break
	R1*8
	R1*6
	r8 \mf d, -. r e -- \times 2/3 { f4 \< -> g -> a -> }
	d4 -^ \f r4 r2
	
	\break
	r2 \times 2/3 { e,4 -- \mp f -- fs -- }
	g2. \> r4 \!
	r2 \times 2/3 { g4 -- a -- e -- }
	d2. r4
	
	\break
	r2 \times 2/3 { e4 \mp \< a b }
	cs1 \sfp \<
	d2 -- \f \times 2/3 { d4 -> f -> bf -> }
	a4 -. r4 r2
	
	\break
	R1
	r2 r8 \mp e, -. e16 -. e8 -. d16 -.
	R1
	r2 r8 d -. d16 -. d8 -. e16 -.
	
	\break
	R1
	r2 r8 e -. e16 -. e8 -. d16 -^
	R1
	\improOn r4 ^ "(Break)" r4 r4 r4
	
	\break
	\repeat volta 2 {
		\repeat unfold 16 { r4 }
		\break
		\repeat unfold 16 { r4 }
		\break
		\repeat unfold 16 { r4 }
		\break
		\repeat unfold 8 { r4 }
	}
	
	\alternative {
		{ \repeat unfold 4 { r4 }
		  \break
		  \repeat unfold 4 { r4 }
		  \improOff }
		{ r2 \times 2/3 { d'4 ->  e -> f -> } 
		  d4 -^ r4 r2 }
	}
	
	\break
	r2 \times 2/3 { e,4 -- \mp f -- fs -- }
	g2. \> r4 \!
	r2 \times 2/3 { g4 -- a -- e -- }
	d2. r4
	
	\break
	r2 \times 2/3 { e4 \mp \< a b }
	cs1 \sfp \<
	d2 -- \f \times 2/3 { d4 f bf }
	a4 -. r4 r2
	
	\break
	R1*8
	
	\break
	d,4. \f -- -> cs8 -> ~ cs4 d8 -. ds -.
	e4. -> d8 cs2 ->
	cs4. -> \bendBefore b8 r c r cs ~
	cs4 -- r8 cs ( d e f -. ) a -. 
	
	\break
	g4 -> r8 a -^ r af -^ r g -> ~
	g4. f8 e4. \startTrillSpan d16 e
	d4 \stopTrillSpan -> r8 d16 cs \times 2/3 { d8 -> f -> a -> } \times 2/3 { e -^ a -^ cs -^ } 
	d4 -^ r4 r2
	r2 r8 \f bf, -. bf -. bf -.
	
	\break
	\key ef \minor
	\bbp cf4 \bendBefore bf8 -- af -. r4 r8 bf -.
	\bbp cf4 \bendBefore bf8 -- af -. r af -. gf -. af -.
	\bbp bf4 \bendBefore af8 -- gf -. r4 ef'16 -^ ef8 -^ ef16 -^
	r4 bf -. r8 bf -. af -. bf -.
	
	\break
	cf4 -- bf8 -- af -. r2
	cf4 -- bf8 --  \jazzTurn af -. r2
	bf4 -- af8 -- gf -. r8 gf4. ( \glissando
	ef'4 -^ ) r4 r8 \f bf -. bf -. bf -.
	
	\break
	\bbp cf4 \bendBefore bf8 -- af -. r4 r8 bf -.
	\bbp cf4 \bendBefore bf8 -- af -. r af -. gf -. af -.
	\bbp bf4 \bendBefore af8 -- gf -. r4 ef'16 -^ ef8 -^ ef16 -^
	r4 bf -. r8 bf -. af -. bf -.
	
	\break
	cf4 -- bf8 -- af -. r2
	cf4 -- bf8 -- af -. r2
	bf4 -- af8 -- ef' -. r4 r8 ef -> ~
	ef8 ef -> ef4 -^ r2
	
	\bar "|."
}

altoSax = \relative c'' {
  
	\global
	\key d \minor

	\partial 4.
	
	a8 \f -. a -. a -.

	\bbp bf4 \bendBefore a8 -- g -. r4 r8 a -.
	\bbp bf4 \bendBefore a8 -- g -. r g -. f -. g -.
	\bbp a4 \bendBefore g8 -- f -. r8 b -. b16 -^ b8 -^ b16 -^
	r4 d,4 -. r8 a' -. g -. a -.
	
	\break
	g4 -- f8 -- cs -. r e -. e4 -.
	g4 -- f8 -- cs -. r cs -. cs -. e -.
	d4. d8 -. r d -. d16 -. d8. -- 
	b'4 -^ r4 r2
	
	\break
	R1*8
	
	\break
	r8 \mf g -. r f -- e4 -. e -.
	r8 g -. r f -- e4 -. e -.
	r8 f -. r \times 2/3 { e16 ( f e ) } d4 -. d -.
	r8 f -. r e -- d4 -. d -.
	
	\break
	r8 g -. r f -- e4 -. e -.
	r8 g -. r g -- e4 -. e -.
	r8 f -. r \times 2/3 { e16 ( f e ) } \times 2/3 { d4 \< -> e -> e -> }
	f4 -^ \f r4 r2
	
	\break
	r2 \times 2/3 { c4 -- \mp d -- ds -- }
	e2. \> r4 \!
	r2 \times 2/3 { e4 -- e -- c -- }
	c2. r4
	
	\break
	r2 \times 2/3 { e4 \mp \< a b }
	a1 \sfp \<
	bf2 -- \f \times 2/3 { bf4 -> d -> d -> }
	cs4 -. r4 r2
	
	\break
	R1
	r2 r8 \mp cs, -. cs16 -. cs8 -. b16 -.
	R1
	r2 r8 b -. b16 -. b8 -. bf16 -.
	
	\break
	R1
	r2 r8 bf -. bf16 -. bf8 -. b16 -^
	r4 r8 \f a' -> ~ a2 \startTrillSpan
	d4 -> \stopTrillSpan -\glissBefore #'(18 1.5 1) r4 r2
	
	\break
	\repeat volta 2 {
		\improOn
		\repeat unfold 16 { r4 }
		\break
		\repeat unfold 16 { r4 }
		\break
		\repeat unfold 16 { r4 }
		\break
		\repeat unfold 8 { r4 }
	}
	
	\alternative {
		{ \repeat unfold 4 { r4 }
		  \break
		  \repeat unfold 4 { r4 }
		  \improOff }
		{ r2 \times 2/3 { a4 ->  cs -> d -> } 
		  b4 -^ r4 r2 }
	}
	
	\break
	r2 \times 2/3 { c,4 -- \mp d -- ds -- }
	e2. \> r4 \!
	r2 \times 2/3 { e4 -- f -- c -- }
	c2. r4
	
	\break
	r2 \! \times 2/3 { e4 \mp \< a b }
	a1 \sfp \<
	bf2 -- \f \times 2/3 { bf4 d d }
	cs4 -. r4 r2
	
	\break
	r4 r8 ^ "(Soli)" \mf a, ( cs e g a 
	bf2 a \> )
	r4 \! r8 \! a, ( b d f a 
	bf2 a \> )
	
	\break
	r4 \! r8 \mf cs, ( e g a cs 
	d2 cs \> )
	R1*2 \!
	
	\break
	d,4. ^ "(Ensemble)" \f -- -> cs8 -> ~ cs4 d8 -. ds -.
	e4. -> d8 cs2 ->
	cs4. -> \bendBefore b8 r c r cs ~
	cs4 -- r8 cs ( d e f -. ) a -. 
	
	\break
	g4 -> r8 a -^ r af -^ r g -> ~
	g4. f8 e4. \startTrillSpan d16 e
	f4 \stopTrillSpan -> r8 f -. r d -- \times 2/3 { cs -^ cs -^ e -^ }
	f4 -^ r4 r2
	r2 r8 \f bf -. bf -. bf -.
	
	\break
	\key ef \minor
	\bbp cf4 \bendBefore bf8 -- af -. r4 r8 bf -.
	\bbp cf4 \bendBefore bf8 -- af -. r af -. gf -. af -.
	\bbp bf4 \bendBefore af8 -- gf -. r8 c -. c16 -^ c8 -^ c16 -^
	r4 gf -. r8 bf -. af -. bf -.
	
	\break
	af4 -- gf8 -- d -. r8 f -. f4 -.
	af4 -- gf8 -- d -. r d -. d -. f -.
	ef4. ef8 -. r ef -. ef16 -. ef8. --
	c'4 -^ r4 r8 \f bf -. bf -. bf -.
	
	\break
	\bbp cf4 \bendBefore bf8 -- af -. r4 r8 bf -.
	\bbp cf4 \bendBefore bf8 -- af -. r af -. gf -. af -.
	\bbp bf4 \bendBefore af8 -- gf -. r8 c -. c16 -^ c8 -^ c16 -^
	
	r8 ^ "(Easy)" \times 2/3 { ef,16 ( e f ) } \times 4/6 { gf ( -> g af a bf cf ) } 
	\times 4/6 { c ( -> a? bf cf c df ) } \times 4/6 { d ( -> df c cf bf a? ) }
	
	\break
	af4 -- gf8 -- d -. r8 f -. f4 -.
	af4 -- gf8 -- d -. r d -. d -. f -.
	gf4. c8 -. r4 r8 c8 -> ~
	c8 c -> c4 -^ r2
	
	\bar "|."
}

tenorSax = \relative c' {
  
	\global
	\key d \minor

	\partial 4.
	
	a8 \f -. a -. a -.

	\bbp bf4 \bendBefore a8 -- g -. r8 a -. a -. a -.
	\bbp bf4 \bendBefore a8 -- g -. r g -. f -. g -.
	\bbp a4 \bendBefore g8 -- f -. r8 f' -. f16 -^ f8 -^ f16 -^
	r4 d,4 -. r8 a' -. g -. a -.
	
	\break
	d4 -- d8 -- a -. r a -. cs4 -. 
	d4 -- a8 -- a -. r a -. a -. a -.
	b4. b8 -. r b -. b16 -. b8. -- 
	f'4 -^ r4 r2
	
	\break
	R1*8
	
	\break
	r8 \mf e -. r d -- cs4 -. cs -.
	r8 e -. r d -- cs4 -. cs -.
	r8 d -. r \times 2/3 { cs16 ( d cs ) } b4 -. b -.
	r8 d -. r cs -- b4 -. b -.
	
	\break
	r8 e -. r d -- cs4 -. cs -.
	r8 e -. r d -- cs4 -. cs -.
	r8 d -. r \times 2/3 { cs16 ( d cs ) } d8 -. \< bf -. a -. cs -.
	d4 -^ \f r4 r2
	
	\break
	r2 \times 2/3 { c4 -- \mp c -- c -- }
	c2. \> r4 \!
	r2 \times 2/3 { c4 -- c -- a -- }
	a2. r4
	
	\break
	r2 \times 2/3 { e'4 \mp \< a b }
	e,1 \sfp \<
	f2 -- \f \times 2/3 { f4 -> f -> f -> }
	e4 -. r4 r2
	
	\break
	R1
	r2 r8 \mp g -. g16 -. g8 -. f16 -.
	R1
	r2 r8 f -. f16 -. f8 -. g16 -.
	
	\break
	R1
	r2 r8 g -. g16 -. g8 -. f16 -^
	r4 r8 \f f -> ( ~ f4 g )
	a4 -> r4 r2
	
	\break
	\repeat volta 2 {
		\improOn
		\repeat unfold 16 { r4 }
		\break
		\repeat unfold 16 { r4 }
		\break
		\repeat unfold 16 { r4 }
		\break
		\repeat unfold 8 { r4 }
	}
	
	\alternative {
		{ \repeat unfold 4 { r4 }
		  \break
		  \repeat unfold 4 { r4 }
		  \improOff }
		{ r2 \times 2/3 { f4 ->  g -> a -> } 
		  f4 -^ r4 r2 }
	}
	
	\break
	r2 \times 2/3 { c4 -- \mp c -- c -- }
	c2. \> r4 \!
	r2 \times 2/3 { c4 -- c -- a -- }
	a2. r4
	
	\break
	r2 \times 2/3 { e'4 \mp \< a b }
	e,1 \sfp \<
	f2 -- \f \times 2/3 { f4 -> f -> f -> }
	e4 -. r4 r2
	
	\break
	R1*8
	
	\break
	d4. \f -- -> cs8 -> ~ cs4 d8 -. ds -.
	e4. -> d8 cs2 ->
	cs4. -> \bendBefore b8 r c r cs ~
	cs4 -- r8 cs ( d e f -. ) a -. 
	
	\break
	g4 -> r8 a -^ r af -^ r g -> ~
	g4. f8 e4. \startTrillSpan d16 e
	d4 \stopTrillSpan -> r8 d -. r d -- \times 2/3 { a -^ a -^ a -^ }
	a4 -^ r4 r2
	r2 r8 \f bf -. bf -. bf -.
	
	\break
	\key ef \minor
	\bbp cf4 \bendBefore bf8 -- af -. r8 bf -. bf -. bf -.
	\bbp cf4 \bendBefore bf8 -- af -. r af -. gf -. af -.
	\bbp bf4 \bendBefore af8 -- gf -. r8 gf' -. gf16 -^ gf8 -^ gf16 -^
	r4 c, -. r8 bf -. af -. bf -.
	
	\break
	ef4 -- ef8 -- bf -. r8 bf -. d4 -.
	ef4 -- ef8 -- bf -. r bf -. bf -. bf -.
	c4. c8 -. r c -. c16 -. c8. --
	gf'4 -^ r4 r8 \f bf, -. bf -. bf -.
	
	\break
	\bbp cf4 \bendBefore bf8 -- af -. r8 bf -. bf -. bf -.
	\bbp cf4 \bendBefore bf8 -- af -. r af -. gf -. af -.
	\bbp bf4 \bendBefore af8 -- gf -. r8 gf' -. gf16 -^ gf8 -^ gf16 -^
	
	r8 ^ "(Easy)" \times 2/3 { ef,16 ( e f ) } \times 4/6 { gf ( -> g af a bf cf ) } 
	\times 4/6 { c ( -> a? bf cf c df ) } \times 4/6 { d ( -> df c cf bf a? ) }
	
	\break
	af4 -- ef'8 -- bf -. r8 bf -. d4 -.
	ef4 -- ef8 -- bf -. r bf -. bf -. bf -.
	ef4. gf8 -. r4 r8 gf8 -> ~
	gf8 gf -> gf4 -^ r2
	
	\bar "|."
}

trombone = \relative c' {
  
	\global
	\clef bass
	\key d \minor

	\partial 4.
	
	a8 \f -. a -. a -.

	\bbp bf4 \bendBefore a8 -- g -. r4 r8 a -.
	\bbp bf4 \bendBefore a8 -- g -. r g -. f -. g -.
	\bbp a4 \bendBefore g8 -- f -. r d' -. d16 -^ d8 -^ d16 -^
	r2 r8 a -. g -. a -.
	
	\break
	g4 -- f8 -- e -. r4 e8 -. e -.
	g4 -- f8 -- e -. r a, -. cs -. e -.
	f4 -- e8 -- d -. r8 a' -^ a16 -^ a8. --
	a4 -^ r4 r2
	
	\break
	R1*8
	R1*6
	r8 \mf d, -. r cs -- d4 \< -^ a -^
	d4 -^ \f r4 r2
	
	\break
	r2 \times 2/3 { bf'4 -- \mp bf -- bf -- }
	bf2. \> r4 \!
	r2 \times 2/3 { a4 -- a -- g -- }
	f2. r4
	
	\break
	e2 \mp ~ \times 2/3 { e4 f fs }
	g1 \sfp \<
	af2 -- \f \times 2/3 { af4 -> af -> af -> }
	g4 -. r4 r2
	
	\break
	r8 \mf cs \bendAfter -2 r bf \bendAfter -2 r a \bendAfter -2 r a -.
	\bbp e'4 -- \bendBefore d8 -- cs -. r2
	r8 f \bendAfter -2 r e \bendAfter -2 r d \bendAfter -2 r d -.
	\bbp a'4 -- \bendBefore g8 -- f -. r2
	
	\break
	r8 g \bendAfter -2 r f \bendAfter -2 r e \bendAfter -2 r e -.
	\bbp g4 -- \bendBefore f8 -- e -. r4 r8. d16 -^
	r4 r8 \f d -> ( ~ d4 cs )
	b4 -> r4 r2
	
	\break
	\repeat volta 2 {
		\improOn
		\repeat unfold 16 { r4 }
		\break
		\repeat unfold 16 { r4 }
		\break
		\repeat unfold 16 { r4 }
		\break
		\repeat unfold 8 { r4 }
	}
	
	\alternative {
		{ \repeat unfold 4 { r4 }
		  \break
		  \repeat unfold 4 { r4 }
		  \improOff }
		{ r2 d,4 -> a -> 
		  d4 -^ r4 r2 }
	}
	
	\break
	r2 \times 2/3 { bf'4 -- \mp bf -- bf -- }
	bf2. \> r4 \!
	r2 \times 2/3 { a4 -- a -- a -- }
	f2. r4
	
	\break
	e2 \mp ~ \times 2/3 { e4 f fs }
	g1 \sfp \<
	af2 -- \f \times 2/3 { af4 -> af -> af -> }
	g4 -. r4 r2
	
	\break
	R1*8
	
	\break
	d4. \f -- -> cs8 -> ~ cs4 d8 -. ds -.
	e4. -> d8 cs2 ->
	cs4. -> \bendBefore b8 r c r cs ~
	cs4 -- r8 cs ( d e f -. ) a -. 
	
	\break
	g4 -> r8 a -^ r af -^ r g -> ~
	g4. f8 e4. \startTrillSpan r8 \stopTrillSpan
	d4 -> r8 g -. a -. f -. g -. e -.
	d4 -^ r4 r2
	r2 r8 \f bf' -. bf -. bf -.
	
	\break
	\key ef \minor
	\bbp cf4 \bendBefore bf8 -- af -. r4 r8 bf -.
	\bbp cf4 \bendBefore bf8 -- af -. r af -. gf -. af -.
	\bbp bf4 \bendBefore af8 -- gf -. r8 bf -. bf16 -^ bf8 -^ bf16 -^
	r4 bf -. r8 bf -. af -. bf -.
	
	\break
	af4 -- gf8 -- f -. r4 f8 -. f -.
	af4 -- gf8 -- f -. r bf, -. d -. f -.
	gf4 -- f8 -- ef -. r8 bf' -^ bf16 -^ bf8. --
	bf4 -^ r4 r8 bf -. bf -. bf -.
	
	\break
	\bbp cf4 \bendBefore bf8 -- af -. r4 r8 bf -.
	\bbp cf4 \bendBefore bf8 -- af -. r af -. gf -. af -.
	\bbp bf4 \bendBefore af8 -- gf -. r8 bf -. bf16 -^ bf8 -^ bf16 -^
	r4 bf -. r8 bf -. af -. bf -.
	
	\break
	af4 -- gf8 -- f -. r4 f8 -. f -.
	af4 -- gf8 -- f -. r4 bf8 -. d -.
	c4 -- bf8 -- bf -. r4 r8 bf -> ~
	bf8 bf -> bf4 -^ r2
	
	\bar "|."
}

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

\book {
	\paper { 
		#(set-paper-size "a4" 'landscape)
		indent = 20
		systems-per-page = 2
	}
	
	\score {
		% \midi { \tempo 4 = 130 }
		\layout { #(layout-set-staff-size 20) }
		
		<<
			\scoreMarkup
			\scoreChords
			\soloChords
			\new StaffGroup <<
				\new Staff \with { instrumentName = "Trumpet" midiInstrument = #"trumpet" } << \trumpetBreakChords \trumpet >>
				\new Staff \with { instrumentName = "Alto Sax" midiInstrument = #"alto sax" } { \altoSax }
				\new Staff \with { instrumentName = "Tenor Sax" midiInstrument = #"tenor sax" } { \tenorSax }
				\new Staff \with { instrumentName = "Trombone" midiInstrument = #"trombone" } { \trombone }
			>>
		>>
	}
}

\book {
	\header { instrument = "Trumpet" }
   \bookOutputName "Sway - Trumpet"
	\score {
		<<
			\scoreMarkup
			\scoreChords
			\transpose bf c \soloChords
			\transpose bf c \trumpetBreakChords
			\new StaffGroup <<
				\new Staff { \transpose bf c' \trumpet }
			>>
		>>
	}
}

\book {
	\header { instrument = "Alto Sax" }
   \bookOutputName "Sway - Alto Sax"
	\score {
		<<
			\scoreMarkup
			\scoreChords
			\transpose ef c \soloChords
			\new StaffGroup <<
				\new Staff { \transpose ef c' \altoSax }
			>>
		>>
	}
}

\book {
	\header { instrument = "Tenor Sax" }
   \bookOutputName "Sway - Tenor Sax"
	\score {
		<<
			\scoreMarkup
			\scoreChords
			\transpose bf c \soloChords
			\new StaffGroup <<
				\new Staff { \transpose bf c'' \tenorSax }
			>>
		>>
	}
}

\book {
	\header { instrument = "Trombone" }
   \bookOutputName "Sway - Trombone"
	\score {
		<<
			\scoreMarkup
			\scoreChords
			\soloChords
			\new StaffGroup <<
				\new Staff { \trombone }
			>>
		>>
	}
}
%}