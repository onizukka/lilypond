\version "2.18.2"


% Descripción de funcionalidades
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% PLUGINS
% - \eolMark:					mark al final de línea
% - \bendBefore: 				solución al bendBefore (i.e. dis8 \bendBefore)
% - \rit: 						crea una porción de ritardando que debe terminar como las dinámicas, con un \!
% - \improOn, \improOff:	negra de improvisación
% - \emptyStaff:				Imprime un pentagrama vacío
% - \emptyBar:					Imprime un compás invisible (útil para indentados)
% - \glissBefore:				muestra un glissando hacia la nota
% - \shortGlissBefore:		muestra un glissando corto hacia la nota
% - \jazzTurn:					muestra el simbolo equivalente a un turn en jazz
% - \subp:						muestra "sp", subito piano
% - \ap:							(accidental padding), separa un poco una alteración de la siguiente nota
% 									(útil al usar parenthesize)
% - \optOttava:				octavaje opcional

% LAYOUT
% - El aspecto de los silencios multicompás es como los de las partituras de jazz
% - Margen extra de las articulaciones para evitar la colisión con las ligaduras
% - Glissandos bonitos

% PAPER
% - Se imprime el título, instrumento y número de página en todas las páginas
% - Solo se debe tocar el page-count (si eso). Sale bonito tal cual.

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Changelog de funcionalidades
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Plugins
% 23-09-85: Añadida función \emptyStave para imprimir un pentagrama vacío.
% 				En algunas partituras hay un pentagrama vacío antes del coda (lo cual mola bastante, por cierto)
% 05-09-14: Añadido a la función \emptyStave el esconder la armadura de la clave, que no estaba.
% 10-09-14: Mejorada la función \bendBefore
% 				Ahora es genérica, y se puede definir cualquier texto en un lateral que haga las funciones de ornamento
% 16-09-14: Mejorada la función para imprimir barras de improvisación.
% 				Antes se llamaba al comando \rs para pintar una barra que ocupaba una negra
% 				Se reemplaza \rs por \restImproOn y \restImproOff, siendo esta opción mucho más flexible.
% 				Cuando se activa \restImproOn, todo silencio se convierte en barra de improvisación, respetando su duración.
% 20-09-14: Mejorada la función para imprimir barras de improvisación.
% 				Se reemplaza \restImproOn y \restImproOff por \improOn y \improOff
% 				Ahora, también las notas del pentagrama salen como barra de improvisación.
% 				(los comandos \improvisationOn e \improvisationOff de lilypond no se llevan bien con LilyJAZZ)
% 				Se ha añadido un grob correspondiente a los slash de la fuente LilyJAZZ en el archivo lily-jazz.ly
% 01-10-14: Plugins
% 				Renombrada la función \emptyStave a \emptyStaff (stave es el plural de staff) y 
% 				corregida (ya no muestra el número de compás).
% 				Recordar que después de usar \emptyStaff se necesita redefinir el 
% 				número de compás con \set Score.currentBarNumber = #xx
% 01-10-14: Añadida la función \emptyBar que oculta un compás entero. Se puede usar para crear un 
% 				efecto de indentación en un pentagrama dado.
% 				Recordar que después de usar \emptyBar se necesita:
% 				- redefinir el número de compás con \set Score.currentBarNumber = #xx
% 				- volver a pintar la clave con \clef
% 				- volver a pintar la armadura con \key
% 15-10-14: Se le ha aumentado el grosor de los bordes de la función \boxed
% 18-10-15: Añadidas funciones de ornamento para antes de la nota:
% 				- \glissBefore: para mostrar un glissando antes de una nota
% 				- \shortGlissBefore: para mostrar un glissando corto antes de una nota
% 				- \jazzTurn: para mostrar el turn típico trompetero de jazz
% 24-10-15: Añadida funcion \subp para mostrar subito piano "sp"
% 27-10-15: Añadida funcion \ap (de accidental padding) para separar un poco el accidental de la siguiente nota
% 				al usar un paréntesis con \parenthesize, ya que se solapan el uno al otro
% 27-10-15: Añadida funcion \optOttava para imprimir octavaje opcional
% 				También he aprovechado para cambiar la fuente del texto "8va" que utilizaba Feta por defecto
% 28-10-15: Mejorada las funciones glissBefore y shortGlissBefore.
% 				Ahora se le pasa una lista #'(r x y) en la que "r" es la rotacion, "x" la posicion horizontal 
%				e "y" la positión vertical.

% Markup
% 23-09-85: Añadidas guías para correcta posición y markup de la indicación de tempo
% 27-10-15: Guía para posición de la indicación de tempo mejorada

% Layout
% 23-09-85: Ahora las articulaciones van por dentro de las ligaduras. Parece que es la mejor solución
% 04-09-14: Los creciendos y decreciendos tienen la línea un poco más gruesa
% 08-09-14: Ahora no se muestra la cancelación de accidentales al haber un cambio de armadura
% 15-09-14: Añadido un poco más de margen a los reguladores
% 20-09-14: Comentada la línea que oculta la cancelación de accidentales al haber cambio de armadura.
% 				Realmente, la cancelación es muy bienvenida.

% Chords
% 09-09-14: Añadida sentencia para que los acordes menores salgan con un guión en vez de con una M minúscula
% 26-10-14: Añadido el include "chord-exception.ily" que recoge una mejor notación de los acordes.
% 				Actualmente, recoge los acordes maj7, 7(b5), 7(#5), 7(b9), 7(#9), 7(add13), maj9(add13), 9(#11)
% 				Se irá añadiendo acordes a menudo.

% Global
% 15-10-14: Todas las instrucciones que se colocaban justo antes de empezar la música están ahora
% 				recogidas en una función \global.
% 				A \global se le ha añadido instrucciones para que agrupe las corcheas de dos en dos
% 				en vez de cuatro en cuatro (el comportamiento por defecto).

% Language
% 15-10-14: Cambiado el lenguaje de entrada de notas del finlandes al inglés
% 				La diferencia es que ahora los sostenidos se escriben "-s" en vez de "-is"
% 				y los bemoles "-f" en vez de "-es".

% Estructura
% 15-10-14: Para poder autoincluir los archivos de instrumento en otros 
% 				(para montar un midi, o un score), se han movido los plugins a un archivo separado
% 				y ahora se utiliza la función \includeOnce, evitando así el problema de las
% 				dependencias circulares. 
% 18-10-15: Mejorada la manera de trabajar con papeles separados y score al mismo tiempo

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Dependencias
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% \includeOnce
% Incluye una dependencia solo una vez. Así se pueden incluir los archivos en otros 
% (para montar un score por ejemplo) sin liarla parda con dependencias cíclicas.

includeOnce = #(
	define-void-function (parser location filename) (string?) (
		if (not (defined? (string->symbol filename))) (
			begin (ly:parser-include-string parser (string-concatenate (list "\\include \"" filename "\"")))
			(primitive-eval (list 'define (string->symbol filename) #t))
		)
	)
)



% LilyJAZZ
\includeOnce "includes/ignatzek-jazz-chords.ily"
\includeOnce "includes/lily-jazz.ily"

% Language
\language "english"

% Plugins
\includeOnce "includes/plugins-1.2.ily"

% Chord Exceptions
\includeOnce "includes/chord-exceptions.ily"

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~











% Disposición
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Cabecera
\header {
	title = "Mambo Italiano"
	instrument = ""
	composer = "Bob Merrill - Rosemary Clooney"
	arranger = "Arr: Gabriel Palacios"
	poet = ""
	tagline = "gabriel.ps.ms@gmail.com"
}

% Tamaño de las notas y pentagrama
#(set-global-staff-size 20)

% Márgenes, fuentes y distancias del papel
\paper {
	% El definir otro tipo de fuente no permite cambiar el tamaño global del staff. Es un bug conocigo
	#(define fonts (make-pango-font-tree "LilyJAZZText" "Nimbus Sans" "Luxi Mono" (/ 20 20)))

	% distancia entre la parte superior de la página y el título
	top-markup-spacing #'basic-distance = 5

	% distancia entre el título y el primer sistema
	markup-system-spacing #'basic-distance = 25

	% distancia entre el texto y el primer sistema 
	top-system-spacing #'basic-distance = 15

	% márgen inferior
	bottom-margin = 20
	
	% intentado del primer sistema
	indent = 0
	 
	% número de páginas
	% page-count = 1

	ragged-last-bottom = ##f 

	oddHeaderMarkup = \markup {
	 \on-the-fly #not-first-page \fill-line {
		\fromproperty #'page:page-number-string
		\fromproperty #'header:title
		\fromproperty #'header:instrument
	 }
	}

	evenHeaderMarkup = \markup {
	 \on-the-fly #not-first-page \fill-line {
		\fromproperty #'header:instrument
		\fromproperty #'header:title
		\fromproperty #'page:page-number-string
	 }
	}
}

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~







% Elementos comunes de la partitura
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
	% Oculta el primer pentagrama del markup
	\override Staff.VerticalAxisGroup #'remove-first = ##t

	% Indicación de tiempo
	\once \override Score.RehearsalMark.self-alignment-X = #LEFT
	\once \override Score.RehearsalMark.extra-offset = #'(0 . 3) 
	\mark \markup "Recitativo"
	
	s4
	s1*8
	
	\bar "||"
	\mark \markup "Medium Mambo"
	s1*4
	
	\mark \markup \boxed "A"
	s1*8
	
	\mark \markup \boxed "B"
	s1*8
	
	\mark \markup \boxed "C"
	s1*8
	
	\mark \markup \boxed "D"
	s1*12
	
	\mark \markup \boxed "E"
	s1*4
	
	\mark \markup \boxed "F"
	s1*8
	
	\mark \markup \boxed "G"
	s1*8
	
	\mark \markup \boxed "H"
	s1*8
	
	\mark \markup \boxed "I"
	s1*8
	
	\mark \markup \boxed "J"

	% s1*2
	% \once \override Score.RehearsalMark.X-offset = #5
	% \mark \markup \boxed "A"
}



% Función musical global
global = {
	% Para pintar todos los números de compás debajo del pentagrama
	% \override Score.BarNumber #'break-visibility = #end-of-line-invisible
	% \override Score.BarNumber #'direction = #DOWN
	% \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

	\jazzOn
	\compressFullBarRests

	% Arregla el problema de tener el scoreMarkup añadido
	\revert MultiMeasureRest #'staff-position
	\override Voice.Rest #'staff-position = #0	

	\set Timing.beamExceptions = #'()
	\set Timing.baseMoment = #(ly:make-moment 1/4)
	\set Timing.beatStructure = #'(1 1 1 1) 
	
	\override ParenthesesItem.font-size = #0
}

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Elementos específicos del instrumento
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Cifrado
scoreChords = \chords {    
	\set minorChordModifier = \markup { "-" }
	\set chordNameExceptions = #chordNameExceptions
	\override ChordName.font-size = #2.5

	% c1:m f2:m g2:m7
}

% Música
trumpet = \relative c'' {
  
	\global
	\key a \minor
	
	\partial 4
	r4
	
	R1*8 
	
	\break
	\bar "||"
	R1*4
	
	\break
	a,4 -. ^"(Sing)" e -. e -. r
	R1
	a4 -. e -. e -. r
	R1
	
	\break
	a4 -. e -. e -. r
	R1*3
	
	\break
	a4 -. e -. e -. r
	R1
	a4 -. e -. e -. r
	R1
	
	\break
	a4 -. e -. e -. r
	R1*3
	
	\break
	c''4 -. \mf e -. g -- b,8 cs -^ ~
	cs8 e -^ r g -- e4 -. r4
	a,4 -. d -. f -- a,8 b -^ ~
	b8 d -^ r f -- d4 -. r4
	
	\break
	a2 ( gs
	g? fs
	f?4 -> ) r4 r2
	R1
	
	\pageBreak
	a,4 -. e -. e -. r
	R1
	a4 -. e -. e -. r
	R1
	
	\break
	a4 -. e -. e -. r
	R1*3
	
	\break
	b''8 \mf as b cs d cs d e
	fs2 e4 -> d ->
	b4. a8 -> ~ a4 e8 a -- ~
	a a4 -^ gs8 ( a bf cf df
	
	\break
	\key ef \minor
	ef4 -> ) r4 r2
	R1*3
	
	\break
	ef4 -. \f bf -. bf -. r
	af8 ( g af bf cf bf cf df )
	ef4 -. bf -. bf -. r
	r8 af4 -^ g8 ( af bf cf af )
	
	\break
	ef'4 -. bf -. bf -. r
	af8 ( g af bf cf af ~ af4 )
	ef4 -. r4 r2
	R1
	
	\break
	ef'4 -. \f bf -. bf -. r8 af -^ ~
	af8 af4 -^ g8 ( af bf cf af )
	ef'4 -. bf -. bf -. r
	af4 -^ af8 ( g af bf cf af )
	
	\break
	ef'4 -. bf -. bf -. r8 af -^ ~
	af8 af4 -^ g8 ( af bf cf af )
	ef4 -. r4 r2
	R1
	
	\pageBreak
	df'4 -. \mf f -. af -- c,8 d -^ ~
	d8 f -^ r af -- f4 -. r4
	bf,4 -. ef -. gf -- bf,8 c -^ ~
	c8 ef -^ r gf -- ef4 -. r4
	
	\break
	bf2 ( a
	af? g
	gf?4 -> ) r4 r2
	R1
	
	\break
	bf,4 -. f -. f -. r
	R1
	bf4 -. f -. f -. r
	R1
	
	\break
	bf4 -. f -. f -. r
	R1*3
	
	\break
	c''8 \mf b c d ef d ef f
	g4 -. r r2
	
	bf,4 -. \mf r8 df g f r g
	\break
	bf,4 -- df8 g -^ ~ g f4 -^ g8
	bf8 \f bf bf bf r bf bf bf
	bf4 -^ r4 r2
	
	\improOn af,4 ^ "(Shout)" \improOff r4 r2
	
   
	\bar "|."
}

altoSax = \relative c' {
  
	\global
	\key a \minor
	
	\partial 4
	r4
	
	R1*8 
	
	\break
	\bar "||"
	R1*4
	
	\break
	a4 -. e -. e -. r
	R1
	a4 -. e -. e -. r
	R1
	
	\break
	a4 -. e -. e -. r
	R1*3
	
	\break
	a4 -. e -. e -. r
	R1
	a4 -. e -. e -. r
	R1
	
	\break
	a4 -. e -. e -. r
	R1*3
	
	\break
	g'4 -. \mf a -. c -- g8 g -^ ~
	g8 cs -^ r cs -- cs4 -. r4
	d,4 -. b' -. b -- f8 a -^ ~
	a8 b -^ r d -- b4 -. r4
	
	\break
	a2 ( gs
	g? fs
	d4 -> ) r4 r2
	R1
	
	\pageBreak
	a4 -. e -. e -. r
	R1
	a4 -. e -. e -. r
	R1
	
	\break
	a4 -. e -. e -. r
	R1*3
	
	\break
	b''8 \mf as b cs d cs d e
	b2 b4 -> b ->
	gs4. fs8 -> ~ fs4 e8 fs -> \fp ~
	fs1 \<
	
	\break
	\key ef \minor
	ef?4 -. \mf r8 gf c bf r c
	ef,4 -- gf8 c -^ ~ c bf4 -^ c8
	d,4 -. r8 f cf' bf r cf
	d,4 -- f8 cf' -^ ~ cf bf4 -^ cf8
	
	\break
	ef,4 -. \f ef -. ef -. r4
	ef2 ( \mp f )
	ef4 -. \f ef -. ef -. r4
	ef2 ( \mp f )
	
	\break
	ef4 -. \f ef -. ef -. r4
	ef2 ( \mp f )
	c4 -. \f r r2
	R1
	
	\break
	ef2 ( \mp df
	cf2 bf4 ) r4
	ef2 ( df
	cf2 bf4 ) r4
	
	\break
	ef2 ( df
	cf2 bf )
	c4 -. r4 r2
	R1
	
	\pageBreak
	af'4 -. \mf bf -. df -- af8 af -^ ~
	af8 d -^ r d -- d4 -. r4
	ef,4 -. c' -. c -- gf8 bf -^ ~
	bf8 c -^ r ef -- c4 -. r4
	
	\break
	bf2 ( a
	af? g
	ef4 -> ) r4 r2
	R1
	
	\break
	bf4 -. f -. f -. r
	R1
	bf4 -. f -. f -. r
	R1
	
	\break
	bf4 -. f -. f -. r
	R1*3
	
	\break
	c''8 \mf b c d ef d ef f
	ef4 -. r4 r2
	
	bf4 -. \mf r8 df g f r g
	\break
	bf,4 -- df8 g -^ ~ g f4 -^ g8
	f8 \f f f f r f f f
	f4 -^ r4 r2
	
	\improOn df,4 ^ "(Shout)" \improOff r4 r2
	
	\bar "|."
}

tenorSax = \relative c' {
  
	\global
	\key a \minor
	
	\partial 4
	r4
	
	R1*8 
	
	\break
	\bar "||"
	R1*4
	
	\break
	a4 -. e -. e -. r
	R1
	a4 -. e -. e -. r
	R1
	
	\break
	a4 -. e -. e -. r
	R1*3
	
	\break
	a4 -. e -. e -. r
	R1
	a4 -. e -. e -. r
	R1
	
	\break
	a4 -. e -. e -. r
	R1*3
	
	\break
	e'4 -. \mf g -. a -- cs,8 e -^ ~
	e8 g -^ r a -- g4 -. r4
	b,4 -. f' -. a -- b,8 d -^ ~
	d8 f -^ r a -- f4 -. r4
	
	\break
	a,2 ( gs
	g? fs
	a4 -> ) r4 r2
	R1
	
	\pageBreak
	a4 -. e -. e -. r
	R1
	a4 -. e -. e -. r
	R1
	
	\break
	a4 -. e -. e -. r
	R1*3
	
	\break
	b'8 \mf as b cs d cs d e
	gs2 gs4 -. gs -.
	e2. -> ~ e8 c -> \fp ~
	c1 \<
	
	\break
	\key ef \minor
	c4 -. \mf r8 gf c bf r c
	ef,4 -- gf8 c -^ ~ c bf4 -^ c8
	d,4 -. r8 f cf' bf r cf
	d,4 -- f8 cf' -^ ~ cf bf4 -^ cf8
	
	\break
	c4 -. \f c -. c -. r4
	cf?2 ( \mp d )
	c4 -. \f c -. c -. r4
	cf?2 ( \mp d )
	
	\break
	c4 -. \f c -. c -. r4
	cf?2 ( \mp d )
	bf4 -. \f r r2
	R1
	
	\break
	ef,2 ( \mp df
	cf2 bf4 ) r4
	ef2 ( df
	cf2 bf4 ) r4
	
	\break
	ef2 ( df
	cf2 bf )
	bf'4 -. r4 r2
	R1
	
	\pageBreak
	f'4 -. \mf af -. bf -- d,8 f -^ ~
	f8 af -^ r bf -- af4 -. r4
	c,4 -. gf' -. bf -- c,8 ef -^ ~
	ef8 gf -^ r bf -- gf4 -. r4
	
	\break
	bf,2 ( a
	af? g
	bf4 -> ) r4 r2
	R1
	
	\break
	bf4 -. f -. f -. r
	R1
	bf4 -. f -. f -. r
	R1
	
	\break
	bf4 -. f -. f -. r
	R1*3
	
	\break
	c'8 \mf b c d ef d ef f
	a4 -. r r2
	
	bf,4 -. \mf r8 df g f r g
	\break
	bf,4 -- df8 g -^ ~ g f4 -^ f8
	df'8 \f df df df r df df df
	df4 -> r4 r2
	
	\improOn af,4 ^ "(Shout)" \improOff r4 r2
	
	\bar "|."
}

trombone = \relative c' {
  
	\global
	\clef bass
	\key a \minor
	
	\partial 4
	r4
	
	R1*8 
	
	\break
	\bar "||"
	R1*4
	
	\break
	a4 -. e -. e -. r
	R1
	a4 -. e -. e -. r
	R1
	
	\break
	a4 -. e -. e -. r
	R1*3
	
	\break
	a4 -. e -. e -. r
	R1
	a4 -. e -. e -. r
	R1
	
	\break
	a4 -. e -. e -. r
	R1*3
	
	\break
	a4 -. \mf c -. e -- e,8 a -^ ~
	a8 b -^ r b -- a4 -. r4
	f4 -. a -. d -- d,8 f -^ ~
	f8 a -^ r b -- a4 -. r4
	
	\break
	a2 ( gs
	g? fs
	f?4 -> ) r4 r2
	R1
	
	\pageBreak
	a4 -. e -. e -. r
	R1
	a4 -. e -. e -. r
	R1
	
	\break
	a4 -. e -. e -. r
	R1*3
	
	\break
	b'8 \mf as b cs d cs d e
	fs2 e4 -> d ->
	c2. -> ~ c8 a -> \fp ~
	a1 \<
	
	\break
	\key ef \minor
	bf?4 -. \mf r8 gf c bf r c
	ef,4 -- gf8 c -^ ~ c bf4 -^ c8
	d,4 -. r8 f cf' bf r cf
	d,4 -- f8 cf' -^ ~ cf bf4 -^ cf8
	
	\break
	gf4 -. \f gf -. gf -. r4
	f2 ( \mp af )
	gf4 -. \f gf -. gf -. r4
	f2 ( \mp af )
	
	\break
	gf4 -. \f gf -. gf -. r4
	f2 ( \mp af )
	gf4 -. \f r r2
	R1
	
	\break
	ef2 ( \mp df
	cf2 bf4 ) r4
	ef2 ( df
	cf2 bf4 ) r4
	
	\break
	ef2 ( df
	cf2 bf )
	gf'4 -. r4 r2
	R1
	
	\pageBreak
	bf4 -. \mf df -. f -- f,8 bf -^ ~
	bf8 c -^ r c -- bf4 -. r4
	gf4 -. bf -. ef -- ef,8 gf -^ ~
	gf8 bf -^ r c -- bf4 -. r4
	
	\break
	bf2 ( a
	af? g
	gf?4 -> ) r4 r2
	R1
	
	\break
	bf4 -. f -. f -. r
	R1
	bf4 -. f -. f -. r
	R1
	
	\break
	bf4 -. f -. f -. r
	R1*3
	
	\break
	c'8 \mf b c d ef d ef f
	g4 -. r r2
	
	bf,4 -. \mf r8 df g f r g
	\break
	bf,4 -- df8 g -^ ~ g f4 -^ f8
	g8 \f g g g r g g g
	g4 -> r4 r2
	
	\improOn df,4 ^ "(Shout)" \improOff r4 r2
	
	\bar "|."
}

trumpetLyrics = \lyricmode {
  "\"Hey!" Mam -- "bo\""
  "\"Hey!" Mam -- "bo\""
  "\"Go" Go "Go!\""
  
  "\"Hey!" Mam -- "bo\""
  "\"Hey!" Mam -- "bo\""
  "\"Hey!" Mam -- "bo\""
  
  \repeat unfold 17 { \skip 1 }
  
  "\"Hey!" Mam -- "bo\""
  "\"Hey!" Mam -- "bo\""
  "\"Go" Go "Joe!\""
  
  \repeat unfold 66 { \skip 1 }
  
  "\"Hey!" Mam -- "bo\""
  "\"Hey!" Mam -- "bo\""
  "\"Ho" Ho "Ho!\""
  
  \repeat unfold 27 { \skip 1 }
  
  "\"Ugh\""
}

altoSaxLyrics = \lyricmode {
  "\"Hey!" Mam -- "bo\""
  "\"Hey!" Mam -- "bo\""
  "\"Go" Go "Go!\""
  
  "\"Hey!" Mam -- "bo\""
  "\"Hey!" Mam -- "bo\""
  "\"Hey!" Mam -- "bo\""
  
  \repeat unfold 17 { \skip 1 }
  
  "\"Hey!" Mam -- "bo\""
  "\"Hey!" Mam -- "bo\""
  "\"Go" Go "Joe!\""
  
  \repeat unfold 69 { \skip 1 }
  
  "\"Hey!" Mam -- "bo\""
  "\"Hey!" Mam -- "bo\""
  "\"Ho" Ho "Ho!\""
  
  \repeat unfold 27 { \skip 1 }
  
  "\"Ugh\""
}

tenorSaxLyrics = \lyricmode {
  "\"Hey!" Mam -- "bo\""
  "\"Hey!" Mam -- "bo\""
  "\"Go" Go "Go!\""
  
  "\"Hey!" Mam -- "bo\""
  "\"Hey!" Mam -- "bo\""
  "\"Hey!" Mam -- "bo\""
  
  \repeat unfold 17 { \skip 1 }
  
  "\"Hey!" Mam -- "bo\""
  "\"Hey!" Mam -- "bo\""
  "\"Go" Go "Joe!\""
  
  \repeat unfold 67 { \skip 1 }
  
  "\"Hey!" Mam -- "bo\""
  "\"Hey!" Mam -- "bo\""
  "\"Ho" Ho "Ho!\""
  
  \repeat unfold 27 { \skip 1 }
  
  "\"Ugh\""
}

tromboneLyrics = \lyricmode {
  "\"Hey!" Mam -- "bo\""
  "\"Hey!" Mam -- "bo\""
  "\"Go" Go "Go!\""
  
  "\"Hey!" Mam -- "bo\""
  "\"Hey!" Mam -- "bo\""
  "\"Hey!" Mam -- "bo\""
  
  \repeat unfold 17 { \skip 1 }
  
  "\"Hey!" Mam -- "bo\""
  "\"Hey!" Mam -- "bo\""
  "\"Go" Go "Joe!\""
  
  \repeat unfold 67 { \skip 1 }
  
  "\"Hey!" Mam -- "bo\""
  "\"Hey!" Mam -- "bo\""
  "\"Ho" Ho "Ho!\""
  
  \repeat unfold 27 { \skip 1 }
  
  "\"Ugh\""
}

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

\book {
	\paper { 
		#(set-paper-size "a4" 'landscape)
		indent = 20
		systems-per-page = 2
	}
	
	\score {
		% \midi { \tempo 4 = 120 }
		\layout { #(layout-set-staff-size 20) }
		
		<<
			\scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\new Staff \with { instrumentName = "Trumpet" midiInstrument = #"trumpet" } { \trumpet \addlyrics \trumpetLyrics }
				\new Staff \with { instrumentName = "Alto Sax" midiInstrument = #"alto sax" } { \altoSax \addlyrics \altoSaxLyrics }
				\new Staff \with { instrumentName = "Tenor Sax" midiInstrument = #"tenor sax" } { \tenorSax \addlyrics \tenorSaxLyrics }
				\new Staff \with { instrumentName = "Trombone" midiInstrument = #"trombone" } { \trombone \addlyrics \tromboneLyrics }
			>>
		>>
	}
}

\book {
	\header { instrument = "Trumpet" }
   \bookOutputName "Mambo Italiano - Trumpet"
	\score {
		<<
			\scoreMarkup
			\transpose bf c \scoreChords
			\new StaffGroup <<
				\new Staff { \transpose bf c' \trumpet \addlyrics \trumpetLyrics }
			>>
		>>
	}
}
\book {
	\header { instrument = "Alto Sax" }
   \bookOutputName "Mambo Italiano - Alto Sax"
	\score {
		<<
			\scoreMarkup
			\transpose ef c \scoreChords
			\new StaffGroup <<
				\new Staff { \transpose ef c' \altoSax \addlyrics \altoSaxLyrics }
			>>
		>>
	}
}

\book {
	\header { instrument = "Tenor Sax" }
   \bookOutputName "Mambo Italiano - Tenor Sax"
	\score {
		<<
			\scoreMarkup
			\transpose bf c \scoreChords
			\new StaffGroup <<
				\new Staff { \transpose bf c'' \tenorSax \addlyrics \tenorSaxLyrics }
			>>
		>>
	}
}

\book {
	\header { instrument = "Trombone" }
   \bookOutputName "Mambo Italiano - Trombone"
	\score {
		<<
			\scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\new Staff { \trombone \addlyrics \tromboneLyrics }
			>>
		>>
	}
}