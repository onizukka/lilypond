\version "2.18.2"
\language "english"

\include "../.includes/drums.ily"
\include "../.includes/chords.ily"
\include "../.includes/lilyjazz.ily"
\include "../.includes/plugins-1.2.ily"


% Descripción de funcionalidades
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% PLUGINS
% - \eolMark:					mark al final de línea
% - \bendBefore: 				solución al bendBefore (i.e. dis8 \bendBefore)
% - \rit: 						crea una porción de ritardando que debe terminar como las dinámicas, con un \!
% - \improOn, \improOff:	negra de improvisación
% - \emptyStaff:				Imprime un pentagrama vacío
% - \emptyBar:					Imprime un compás invisible (útil para indentados)
% - \glissBefore:				muestra un glissando hacia la nota
% - \shortGlissBefore:		muestra un glissando corto hacia la nota
% - \jazzTurn:					muestra el simbolo equivalente a un turn en jazz
% - \subp:						muestra "sp", subito piano
% - \ap:							(accidental padding), separa un poco una alteración de la siguiente nota
% 									(útil al usar parenthesize)
% - \optOttava:				octavaje opcional

% LAYOUT
% - El aspecto de los silencios multicompás es como los de las partituras de jazz
% - Margen extra de las articulaciones para evitar la colisión con las ligaduras
% - Glissandos bonitos

% PAPER
% - Se imprime el título, instrumento y número de página en todas las páginas
% - Solo se debe tocar el page-count (si eso). Sale bonito tal cual.

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Changelog de funcionalidades
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Plugins
% 23-09-85: Añadida función \emptyStave para imprimir un pentagrama vacío.
% 				En algunas partituras hay un pentagrama vacío antes del coda (lo cual mola bastante, por cierto)
% 05-09-14: Añadido a la función \emptyStave el esconder la armadura de la clave, que no estaba.
% 10-09-14: Mejorada la función \bendBefore
% 				Ahora es genérica, y se puede definir cualquier texto en un lateral que haga las funciones de ornamento
% 16-09-14: Mejorada la función para imprimir barras de improvisación.
% 				Antes se llamaba al comando \rs para pintar una barra que ocupaba una negra
% 				Se reemplaza \rs por \restImproOn y \restImproOff, siendo esta opción mucho más flexible.
% 				Cuando se activa \restImproOn, todo silencio se convierte en barra de improvisación, respetando su duración.
% 20-09-14: Mejorada la función para imprimir barras de improvisación.
% 				Se reemplaza \restImproOn y \restImproOff por \improOn y \improOff
% 				Ahora, también las notas del pentagrama salen como barra de improvisación.
% 				(los comandos \improvisationOn e \improvisationOff de lilypond no se llevan bien con LilyJAZZ)
% 				Se ha añadido un grob correspondiente a los slash de la fuente LilyJAZZ en el archivo lily-jazz.ly
% 01-10-14: Plugins
% 				Renombrada la función \emptyStave a \emptyStaff (stave es el plural de staff) y 
% 				corregida (ya no muestra el número de compás).
% 				Recordar que después de usar \emptyStaff se necesita redefinir el 
% 				número de compás con \set Score.currentBarNumber = #xx
% 01-10-14: Añadida la función \emptyBar que oculta un compás entero. Se puede usar para crear un 
% 				efecto de indentación en un pentagrama dado.
% 				Recordar que después de usar \emptyBar se necesita:
% 				- redefinir el número de compás con \set Score.currentBarNumber = #xx
% 				- volver a pintar la clave con \clef
% 				- volver a pintar la armadura con \key
% 15-10-14: Se le ha aumentado el grosor de los bordes de la función \boxed
% 18-10-15: Añadidas funciones de ornamento para antes de la nota:
% 				- \glissBefore: para mostrar un glissando antes de una nota
% 				- \shortGlissBefore: para mostrar un glissando corto antes de una nota
% 				- \jazzTurn: para mostrar el turn típico trompetero de jazz
% 24-10-15: Añadida funcion \subp para mostrar subito piano "sp"
% 27-10-15: Añadida funcion \ap (de accidental padding) para separar un poco el accidental de la siguiente nota
% 				al usar un paréntesis con \parenthesize, ya que se solapan el uno al otro
% 27-10-15: Añadida funcion \optOttava para imprimir octavaje opcional
% 				También he aprovechado para cambiar la fuente del texto "8va" que utilizaba Feta por defecto
% 28-10-15: Mejorada las funciones glissBefore y shortGlissBefore.
% 				Ahora se le pasa una lista #'(r x y) en la que "r" es la rotacion, "x" la posicion horizontal 
%				e "y" la positión vertical.

% Markup
% 23-09-85: Añadidas guías para correcta posición y markup de la indicación de tempo
% 27-10-15: Guía para posición de la indicación de tempo mejorada

% Layout
% 23-09-85: Ahora las articulaciones van por dentro de las ligaduras. Parece que es la mejor solución
% 04-09-14: Los creciendos y decreciendos tienen la línea un poco más gruesa
% 08-09-14: Ahora no se muestra la cancelación de accidentales al haber un cambio de armadura
% 15-09-14: Añadido un poco más de margen a los reguladores
% 20-09-14: Comentada la línea que oculta la cancelación de accidentales al haber cambio de armadura.
% 				Realmente, la cancelación es muy bienvenida.

% Chords
% 09-09-14: Añadida sentencia para que los acordes menores salgan con un guión en vez de con una M minúscula
% 26-10-14: Añadido el include "chord-exception.ily" que recoge una mejor notación de los acordes.
% 				Actualmente, recoge los acordes maj7, 7(b5), 7(#5), 7(b9), 7(#9), 7(add13), maj9(add13), 9(#11)
% 				Se irá añadiendo acordes a menudo.

% Global
% 15-10-14: Todas las instrucciones que se colocaban justo antes de empezar la música están ahora
% 				recogidas en una función \global.
% 				A \global se le ha añadido instrucciones para que agrupe las corcheas de dos en dos
% 				en vez de cuatro en cuatro (el comportamiento por defecto).

% Language
% 15-10-14: Cambiado el lenguaje de entrada de notas del finlandes al inglés
% 				La diferencia es que ahora los sostenidos se escriben "-s" en vez de "-is"
% 				y los bemoles "-f" en vez de "-es".

% Estructura
% 15-10-14: Para poder autoincluir los archivos de instrumento en otros 
% 				(para montar un midi, o un score), se han movido los plugins a un archivo separado
% 				y ahora se utiliza la función \includeOnce, evitando así el problema de las
% 				dependencias circulares. 
% 18-10-15: Mejorada la manera de trabajar con papeles separados y score al mismo tiempo

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~











% Disposición
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

title = "Body and Soul"

% Cabecera
\header {
	title = #title
	subtitle = "(as performed by Tony Bennet and Amy Winehouse)"
	instrument = ""
	composer = "Johnny B. Green"
	arranger = "Arr: Gabriel Palacios"
	poet = ""
	tagline = "gabriel.ps.ms@gmail.com"
}

% Márgenes, fuentes y distancias del papel
\paper {

	% distancia entre la parte superior de la página y el título
	top-markup-spacing #'basic-distance = 5

	% distancia entre el título y el primer sistema
	markup-system-spacing #'basic-distance = 25

	% distancia entre el texto y el primer sistema 
	top-system-spacing #'basic-distance = 15

	% márgen inferior
	bottom-margin = 20
	
	% intentado del primer sistema
	indent = 0
	 
	% número de páginas
	% page-count = 1

	ragged-last-bottom = ##f 

	oddHeaderMarkup = \markup {
	 \on-the-fly #not-first-page \fill-line {
		\fromproperty #'page:page-number-string
		\fromproperty #'header:title
		\fromproperty #'header:instrument
	 }
	}

	evenHeaderMarkup = \markup {
	 \on-the-fly #not-first-page \fill-line {
		\fromproperty #'header:instrument
		\fromproperty #'header:title
		\fromproperty #'page:page-number-string
	 }
	}
}

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~







% Elementos comunes de la partitura
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
	% Oculta el primer pentagrama del markup
	\override Staff.VerticalAxisGroup #'remove-first = ##t

	% Indicación de tiempo
	\once \override Score.RehearsalMark.self-alignment-X = #LEFT
	\once \override Score.RehearsalMark.extra-offset = #'(0 . 3) 
	\mark \markup "Medium Slow Swing"
	s1*2
	
	\tag #'sb \break
	\mark \markup \boxed "A"
	s1*8
	
	\tag #'sb \break
	\mark \markup \boxed "B"
	s1*8
	
	\tag #'sb \break
	\once \override Score.RehearsalMark.self-alignment-X = #LEFT
	\mark \markup {
		\column { \boxed "C" }
		\column { "(Straight 8's)" }
	}
	s1*2
	
	\once \override Score.RehearsalMark.self-alignment-X = #LEFT
	\mark \markup "(Swing 8's)"
	s1*6
	
	\mark \markup \boxed "D"
	\tag #'sb \break
	s1*8
	
	\once \override Score.RehearsalMark.self-alignment-X = #LEFT
	\mark \markup {
		\column { \boxed "E" }
		\column { "(Instrument Solo)" }
	}
	\tag #'sb \break
	s1*4
	
	\mark \markup "(Vocal)"
	s1*4
	
	\mark \markup \boxed "F"
	\tag #'sb \break
	s1*10
}

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Elementos específicos del instrumento
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Cifrado
scoreChords = \chords {
	
	ef2:m9 bf2:7.9-.13-
	ef2:m9 bf2:7.9-.13-
	
	ef2:m9 bf2:7.9-.13-
	ef2:m9 af4:13 af4:13-
	df2:6.9 gf2:7.5-
	df2:6.9/f e2:dim
	
	ef2:m7 ef2:m7/df
	gf2:maj7.5+/c f4:7.9+ f4:7.9-
	bf2:m9 ef4:m9 af4:13
	df2:maj7 f4:7.9+ f4:7.9-
	
	bf2:m9 f2:7.9-.13-
	bf2:m9 ef4:13 e:5+/g
	af2:6.9 df2:7.5-
	af2:6.9/c b2:dim
	
	bf2:m7 bf2:m7/af
	df2:maj7.5+/g c4:7.9+ c4:7.9-
	f2:m9 bf4:m9 ef4:13
	af2:maj7 e4:m11 a4:7.13
	
	d2:maj7 g2:maj7.9/a
	d2:maj7 g2:m6.9/a
	c4:7.5-.13 b4:7 e4:m11^9 a4:9.5+
	d2:maj7 e4:m11^9 ef4:m9
	
	d2:m9 g4:13 g4:13-
	c2:maj7.9 e4:m9 ef4:dim
	d2:m7 g4:13 g4:7.9+.13-
	c4:9 b:9 bf:9 bf:13
	
	ef2:m9 bf2:7.9-.13-
	ef2:m9 af4:13 af4:13-
	df2:6.9 gf2:7.5-
	df2:6.9/f e2:dim
	
	ef2:m7 ef2:m7/df
	gf2:maj7.5+/c f4:7.9+ f4:7.9-
	bf2:m9 ef4:m9 af4:13
	df2:maj7 e4:m9 a4:13
	
	d2:maj7 g2:m7+.9/a
	d2:maj7/fs g4:m9 c4:9
	fs4:7 b4:7.13- g4:m6/bf a4:13
	d2:maj7 ef2:9.5-
	
	d2:m9 g2:13
	e2:m11^9 ef2:dim
	d2:m7 g4:13 g4:7.9+.13-
	c4:9 b:9 bf:9 bf:13
	
	ef2:m9 bf2:7.9-.13-
	ef2:m9 af4:13 af4:13-
	df2:6.9 gf2:7.5-
	df2:6.9/f e2:dim
	
	ef2:m7 ef2:m7/df
	gf2:maj9.5+/c f4:7.9+ f4:7.9-
	bf2:m7 gf/af
	gf:sus4/af af:13
	
	df2:9 gf/af
	df1:9
}

piano = \relative c' {
  
	\global
	\key df \major
	
	f'4 \mf ~ \times 2/3 { f8 ef f } gf4 d
	f4 ~ \times 2/3 { f8 ef f } gf4 \glissando d
	
	\improOn
	\bar "||"
	\tag #'b \break
	r4 \> r4 \sp \repeat unfold 6 { r4 }
	
	\tag #'b \break
	\repeat unfold 8 { r4 }
	
	\tag #'b \break
	\repeat unfold 8 { r4 }
	
	\tag #'b \break
	\repeat unfold 8 { r4 }
	
	\tag #'b \break
	\repeat unfold 8 { r4 }
	
	\tag #'b \break
	\repeat unfold 8 { r4 }
	
	\tag #'b \break
	\repeat unfold 8 { r4 }
	
	\tag #'b \break
	\repeat unfold 4 { r4 }
	r4 r4 r4 \< r4
	
	\tag #'b \pageBreak
	r4 \mf \repeat unfold 7 { r4 }
	
	\tag #'b \break
	\repeat unfold 8 { r4 }
	
	\tag #'b \break
	\repeat unfold 8 { r4 }
	
	\tag #'b \break
	\repeat unfold 4 { r4 }
	r4 r4 r4 r4 \>
	
	\tag #'b \break
	r4 \p \repeat unfold 7 { r4 }
	
	\tag #'b \break
	\repeat unfold 8 { r4 }
	
	\tag #'b \break
	r4 \< r4 r4 r4
	r4 \f r4 r4 r4
	
	\tag #'b \break
	\repeat unfold 4 { r4 }
	r4 r4 r4 \> r4
	
	\tag #'b \pageBreak
	r4 \mf \repeat unfold 7 { r4 }
	
	\tag #'b \break
	\repeat unfold 4 { r4 }
	r4 \< r4 \! r4 \> r4
	
	\tag #'b \break
	r4 \p \repeat unfold 7 { r4 }
	
	\tag #'b \break
	\repeat unfold 8 { r4 }
	
	\tag #'b \break
	\repeat unfold 8 { r4 }
	
	\tag #'b \break
	\repeat unfold 8 { r4 }
	
	\tag #'b \break
	r4 \< r4 r4 r4
	r4 \f r4 r4 r4
	
	\tag #'b \break
	c2 \fermata \breathe c2 \fermata \breathe
	c2 \fermata \breathe c2 \fermata \breathe
	
	\tag #'b \break
	\repeat unfold 4 { r4 }
	c1 \fermata
	
	\bar "|."
}

guitar = \relative c' {
  
	\global
	\key df \major
	
	\improOn
	r4 \mf \repeat unfold 7 { r4 }
	
	\bar "||"
	\tag #'b \break
	r4 \> r4 \sp \repeat unfold 6 { r4 }
	
	\tag #'b \break
	\repeat unfold 8 { r4 }
	
	\tag #'b \break
	\repeat unfold 8 { r4 }
	
	\tag #'b \break
	\repeat unfold 8 { r4 }
	
	\tag #'b \break
	\repeat unfold 8 { r4 }
	
	\tag #'b \break
	\repeat unfold 8 { r4 }
	
	\tag #'b \break
	\repeat unfold 8 { r4 }
	
	\tag #'b \break
	\repeat unfold 4 { r4 }
	r4 r4 r4 \< r4
	
	\tag #'b \pageBreak
	r4 \mf \repeat unfold 7 { r4 }
	
	\tag #'b \break
	\repeat unfold 8 { r4 }
	
	\tag #'b \break
	\repeat unfold 8 { r4 }
	
	\tag #'b \break
	\repeat unfold 4 { r4 }
	r4 r4 r4 r4 \>
	
	\tag #'b \break
	r4 \p \repeat unfold 7 { r4 }
	
	\tag #'b \break
	\repeat unfold 8 { r4 }
	
	\tag #'b \break
	r4 \< r4 r4 r4
	r4 \f r4 r4 r4
	
	\tag #'b \break
	\repeat unfold 4 { r4 }
	r4 r4 r4 \> r4
	
	\tag #'b \pageBreak
	r4 \mf \repeat unfold 7 { r4 }
	
	\tag #'b \break
	\repeat unfold 4 { r4 }
	r4 \< r4 \! r4 \> r4
	
	\tag #'b \break
	r4 \p \repeat unfold 7 { r4 }
	
	\tag #'b \break
	\repeat unfold 8 { r4 }
	
	\tag #'b \break
	\repeat unfold 8 { r4 }
	
	\tag #'b \break
	\repeat unfold 8 { r4 }
	
	\tag #'b \break
	r4 \< r4 r4 r4
	r4 \f r4 r4 r4
	
	\tag #'b \break
	c2 \fermata \breathe c2 \fermata \breathe
	c2 \fermata \breathe c2 \fermata \breathe
	
	\tag #'b \break
	\repeat unfold 4 { r4 }
	c1 \fermata
	
	\bar "|."
}

bass = \relative c' {
  
	\global
	\clef bass
	\key df \major
	
	\improOn
	r4 \mf \repeat unfold 7 { r4 }
	
	\bar "||"
	\tag #'b \break
	r4 \> r4 \sp \repeat unfold 6 { r4 }
	
	\tag #'b \break
	\repeat unfold 8 { r4 }
	
	\tag #'b \break
	\repeat unfold 8 { r4 }
	
	\tag #'b \break
	\repeat unfold 8 { r4 }
	
	\tag #'b \break
	\repeat unfold 8 { r4 }
	
	\tag #'b \break
	\repeat unfold 8 { r4 }
	
	\tag #'b \break
	\repeat unfold 8 { r4 }
	
	\tag #'b \break
	\repeat unfold 4 { r4 }
	r4 r4 r4 \< r4
	
	\tag #'b \pageBreak
	r4 \mf \repeat unfold 7 { r4 }
	
	\tag #'b \break
	\repeat unfold 8 { r4 }
	
	\tag #'b \break
	\repeat unfold 8 { r4 }
	
	\tag #'b \break
	\repeat unfold 4 { r4 }
	r4 r4 r4 r4 \>
	
	\tag #'b \break
	r4 \p \repeat unfold 7 { r4 }
	
	\tag #'b \break
	\repeat unfold 8 { r4 }
	
	\tag #'b \break
	r4 \< r4 r4 r4
	r4 \f r4 r4 r4
	
	\tag #'b \break
	\repeat unfold 4 { r4 }
	r4 r4 r4 \> r4
	
	\tag #'b \pageBreak
	r4 \mf \repeat unfold 7 { r4 }
	
	\tag #'b \break
	\repeat unfold 4 { r4 }
	r4 \< r4 \! r4 \> r4
	
	\tag #'b \break
	r4 \p \repeat unfold 7 { r4 }
	
	\tag #'b \break
	\repeat unfold 8 { r4 }
	
	\tag #'b \break
	\repeat unfold 8 { r4 }
	
	\tag #'b \break
	\repeat unfold 8 { r4 }
	
	\tag #'b \break
	r4 \< r4 r4 r4
	r4 \f r4 r4 r4
	
	\tag #'b \break
	c2 \fermata \breathe c2 \fermata \breathe
	c2 \fermata \breathe c2 \fermata \breathe
	
	\tag #'b \break
	\repeat unfold 4 { r4 }
	c1 \fermata
	
	\bar "|."
}

drumsUp = \drummode {
  	
	\revert RestCollision.positioning-done
	\override Rest.staff-position = #6
	
	\dynamicUp
	\teeny
	
	
	
	\bar "|."
}

drumsDown = \drummode {

	\global
	
	\improOn
	\repeat unfold 8 { r4 }
	
	\tag #'b \break
	\repeat percent 8 { r4 r4 r4 r4 }
	
	\tag #'b \break
	\repeat percent 8 { r4 r4 r4 r4 }
	
	\tag #'b \break
	\repeat percent 8 { r4 r4 r4 r4 }
	
	\tag #'b \break
	\repeat percent 8 { r4 r4 r4 r4 }
	
	\tag #'b \break
	\repeat percent 8 { r4 r4 r4 r4 }
	
	\tag #'b \break
	\repeat percent 6 { r4 r4 r4 r4 }
	sn2 ^\fermata \breathe sn2 ^\fermata \breathe
	sn2 ^\fermata \breathe sn2 ^\fermata \breathe
	
	\tag #'b \break
	\repeat unfold 4 { r4 }
	sn1 ^\fermata
	
	\bar "|."
}

drumsRepeats = \new DrumVoice \with { \RemoveEmptyStaves } {
	% Oculta el primer pentagrama del markup
	\override Staff.VerticalAxisGroup #'remove-first = ##t
	
	s1 \mf s1
	
	s4 \> s4 \sp s2 s1*3
	s1*4
	
	s1*4
	s1*3
	s2 s2 \<
	
	s1*4 \mf
	s1*3
	s2 s4 s4 \>
	
	s1*4 \p
	s1 \< 
	s1*2 \f
	s2 s4 \> s4
	
	s1*3 \mf 
	s4 \< s4 \! s4 \> s4
	
	s1*8 \p
	
	s1 \<
	s1*3 \f
	
	s1*2
}

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~


\book {
	\header { instrument = "Score" }
	
	\paper { 
		#(set-paper-size "a4" 'landscape)
		left-margin = 20
		right-margin = 20
		systems-per-page = 1
	}
	
	\score {
		% \midi { \tempo 4 = 60 }
		\layout {} 
		\removeWithTag #'b
		<<
			\scoreMarkup			
			\scoreChords
			\new StaffGroup <<
				\new Staff \with { instrumentName = "Piano" } { \piano }
				\new Staff \with { instrumentName = "Guitar" } { \guitar }
				\new Staff \with { instrumentName = "Bass" } { \bass }
			>>
			
			\new DrumStaff <<
  				\set DrumStaff.drumStyleTable = #(alist->hash-table nwDrums)
				\new DrumVoice { \voiceOne \drumsUp }
				\new DrumVoice { \voiceTwo \drumsDown }
  				\drumsRepeats
			>>
		>>
	}
}

instrumentName = "Piano"
\book {
	\header { instrument = #instrumentName }
   \bookOutputName  #(string-append title " - " instrumentName)
   
	\score {
		<<
			\removeWithTag #'sb \scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\new Staff { \piano }
			>>
		>>
	}
}

instrumentName = "Guitar"
\book {
	\header { instrument = #instrumentName }
   \bookOutputName  #(string-append title " - " instrumentName)
   
	\score {
		<<
			\removeWithTag #'sb \scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\new Staff { \guitar }
			>>
		>>
	}
}

instrumentName = "Bass"
\book {
	\header { instrument = #instrumentName }
   \bookOutputName  #(string-append title " - " instrumentName)
   
	\score {
		<<
			\removeWithTag #'sb \scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\new Staff { \bass }
			>>
		>>
	}
}

instrumentName = "Drums"
\book {
	\header { instrument = #instrumentName }
   \bookOutputName  #(string-append title " - " instrumentName)
   
	\score {
		<<
			\removeWithTag #'sb \scoreMarkup
			\new DrumStaff <<
  				\set DrumStaff.drumStyleTable = #(alist->hash-table nwDrums)
				\new DrumVoice { \voiceOne \drumsUp }
				\new DrumVoice { \voiceTwo \drumsDown }
  				\drumsRepeats
			>>
		>>
	}
}