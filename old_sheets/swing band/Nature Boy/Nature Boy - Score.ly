\version "2.18.2"
\language "english"


% Descripción de funcionalidades
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% PLUGINS
% - \eolMark:					mark al final de línea
% - \bendBefore: 				solución al bendBefore (i.e. dis8 \bendBefore)
% - \rit: 						crea una porción de ritardando que debe terminar como las dinámicas, con un \!
% - \improOn, \improOff:	negra de improvisación
% - \emptyStaff:				Imprime un pentagrama vacío
% - \emptyBar:					Imprime un compás invisible (útil para indentados)
% - \glissBefore:				muestra un glissando hacia la nota
% - \shortGlissBefore:		muestra un glissando corto hacia la nota
% - \jazzTurn:					muestra el simbolo equivalente a un turn en jazz
% - \subp:						muestra "sp", subito piano
% - \ap:							(accidental padding), separa un poco una alteración de la siguiente nota
% 									(útil al usar parenthesize)
% - \optOttava:				octavaje opcional

% LAYOUT
% - El aspecto de los silencios multicompás es como los de las partituras de jazz
% - Margen extra de las articulaciones para evitar la colisión con las ligaduras
% - Glissandos bonitos

% PAPER
% - Se imprime el título, instrumento y número de página en todas las páginas
% - Solo se debe tocar el page-count (si eso). Sale bonito tal cual.

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Changelog de funcionalidades
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Plugins
% 23-09-85: Añadida función \emptyStave para imprimir un pentagrama vacío.
% 				En algunas partituras hay un pentagrama vacío antes del coda (lo cual mola bastante, por cierto)
% 05-09-14: Añadido a la función \emptyStave el esconder la armadura de la clave, que no estaba.
% 10-09-14: Mejorada la función \bendBefore
% 				Ahora es genérica, y se puede definir cualquier texto en un lateral que haga las funciones de ornamento
% 16-09-14: Mejorada la función para imprimir barras de improvisación.
% 				Antes se llamaba al comando \rs para pintar una barra que ocupaba una negra
% 				Se reemplaza \rs por \restImproOn y \restImproOff, siendo esta opción mucho más flexible.
% 				Cuando se activa \restImproOn, todo silencio se convierte en barra de improvisación, respetando su duración.
% 20-09-14: Mejorada la función para imprimir barras de improvisación.
% 				Se reemplaza \restImproOn y \restImproOff por \improOn y \improOff
% 				Ahora, también las notas del pentagrama salen como barra de improvisación.
% 				(los comandos \improvisationOn e \improvisationOff de lilypond no se llevan bien con LilyJAZZ)
% 				Se ha añadido un grob correspondiente a los slash de la fuente LilyJAZZ en el archivo lily-jazz.ly
% 01-10-14: Plugins
% 				Renombrada la función \emptyStave a \emptyStaff (stave es el plural de staff) y 
% 				corregida (ya no muestra el número de compás).
% 				Recordar que después de usar \emptyStaff se necesita redefinir el 
% 				número de compás con \set Score.currentBarNumber = #xx
% 01-10-14: Añadida la función \emptyBar que oculta un compás entero. Se puede usar para crear un 
% 				efecto de indentación en un pentagrama dado.
% 				Recordar que después de usar \emptyBar se necesita:
% 				- redefinir el número de compás con \set Score.currentBarNumber = #xx
% 				- volver a pintar la clave con \clef
% 				- volver a pintar la armadura con \key
% 15-10-14: Se le ha aumentado el grosor de los bordes de la función \boxed
% 18-10-15: Añadidas funciones de ornamento para antes de la nota:
% 				- \glissBefore: para mostrar un glissando antes de una nota
% 				- \shortGlissBefore: para mostrar un glissando corto antes de una nota
% 				- \jazzTurn: para mostrar el turn típico trompetero de jazz
% 24-10-15: Añadida funcion \subp para mostrar subito piano "sp"
% 27-10-15: Añadida funcion \ap (de accidental padding) para separar un poco el accidental de la siguiente nota
% 				al usar un paréntesis con \parenthesize, ya que se solapan el uno al otro
% 27-10-15: Añadida funcion \optOttava para imprimir octavaje opcional
% 				También he aprovechado para cambiar la fuente del texto "8va" que utilizaba Feta por defecto

% Markup
% 23-09-85: Añadidas guías para correcta posición y markup de la indicación de tempo
% 27-10-15: Guía para posición de la indicación de tempo mejorada

% Layout
% 23-09-85: Ahora las articulaciones van por dentro de las ligaduras. Parece que es la mejor solución
% 04-09-14: Los creciendos y decreciendos tienen la línea un poco más gruesa
% 08-09-14: Ahora no se muestra la cancelación de accidentales al haber un cambio de armadura
% 15-09-14: Añadido un poco más de margen a los reguladores
% 20-09-14: Comentada la línea que oculta la cancelación de accidentales al haber cambio de armadura.
% 				Realmente, la cancelación es muy bienvenida.

% Chords
% 09-09-14: Añadida sentencia para que los acordes menores salgan con un guión en vez de con una M minúscula
% 26-10-14: Añadido el include "chord-exception.ily" que recoge una mejor notación de los acordes.
% 				Actualmente, recoge los acordes maj7, 7(b5), 7(#5), 7(b9), 7(#9), 7(add13), maj9(add13), 9(#11)
% 				Se irá añadiendo acordes a menudo.

% Global
% 15-10-14: Todas las instrucciones que se colocaban justo antes de empezar la música están ahora
% 				recogidas en una función \global.
% 				A \global se le ha añadido instrucciones para que agrupe las corcheas de dos en dos
% 				en vez de cuatro en cuatro (el comportamiento por defecto).

% Language
% 15-10-14: Cambiado el lenguaje de entrada de notas del finlandes al inglés
% 				La diferencia es que ahora los sostenidos se escriben "-s" en vez de "-is"
% 				y los bemoles "-f" en vez de "-es".

% Estructura
% 15-10-14: Para poder autoincluir los archivos de instrumento en otros 
% 				(para montar un midi, o un score), se han movido los plugins a un archivo separado
% 				y ahora se utiliza la función \includeOnce, evitando así el problema de las
% 				dependencias circulares. 
% 18-10-15: Mejorada la manera de trabajar con papeles separados y score al mismo tiempo

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Dependencias
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% \includeOnce
% Incluye una dependencia solo una vez. Así se pueden incluir los archivos en otros 
% (para montar un score por ejemplo) sin liarla parda con dependencias cíclicas.

%{
includeOnce = #(
	define-void-function (parser location filename) (string?) (
		if (not (defined? (string->symbol filename))) (
			begin (ly:parser-include-string parser (string-concatenate (list "\\include \"" filename "\"")))
			(primitive-eval (list 'define (string->symbol filename) #t))
		)
	)
)
%}

% includes
\include "../.includes/drums.ily"
\include "../.includes/chords.ily"
\include "../.includes/lilyjazz.ily"
\include "../.includes/plugins-1.2.ily"

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~











% Disposición
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Cabecera
title = "Nature Boy"
\header {
	title = #title
	subtitle = "(as performed by Kurt Elling)"
	instrument = ""
	composer = "Eden Ahbez"
	arranger = "Arr: Gabriel Palacios"
	poet = ""
	tagline = "gabriel.ps.ms@gmail.com"
}

% Márgenes, fuentes y distancias del papel
\paper {
	% El definir otro tipo de fuente no permite cambiar el tamaño global del staff. Es un bug conocigo
	% #(define fonts (make-pango-font-tree "LilyJAZZText" "Nimbus Sans" "Luxi Mono" (/ 20 20)))

	% distancia entre la parte superior de la página y el título
	top-markup-spacing #'basic-distance = 5

	% distancia entre el título y el primer sistema
	markup-system-spacing #'basic-distance = 25

	% distancia entre el texto y el primer sistema 
	top-system-spacing #'basic-distance = 15

	% márgen inferior
	bottom-margin = 20
	
	% intentado del primer sistema
	indent = 0
	 
	% número de páginas
	% page-count = 2

	ragged-last-bottom = ##f 

	oddHeaderMarkup = \markup {
	 \on-the-fly #not-first-page \fill-line {
		\fromproperty #'page:page-number-string
		\fromproperty #'header:title
		\fromproperty #'header:instrument
	 }
	}

	evenHeaderMarkup = \markup {
	 \on-the-fly #not-first-page \fill-line {
		\fromproperty #'header:instrument
		\fromproperty #'header:title
		\fromproperty #'page:page-number-string
	 }
	}
}

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~







% Elementos comunes de la partitura
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
	% Oculta el primer pentagrama del markup
	\override Staff.VerticalAxisGroup #'remove-first = ##t

	% Indicación de tiempo
	\once \override Score.RehearsalMark.self-alignment-X = #LEFT
	\once \override Score.RehearsalMark.extra-offset = #'(0 . 3) 
	\mark \markup "(1st x: Recitativo  - 2nd x: Medium Fast Latin)"

	s1*16
	\mark \markup \boxed "A"
	
	s1*10
	\eolMark \markup "(Al Coda)"
	
	s1*4
	\eolMark \markup \box \small "on 1st x count-in \". . 3, 4\""
	\once \override Score.RehearsalMark.extra-offset = #'(1 . 0) 
	\once \override Score.RehearsalMark.self-alignment-X = #LEFT
	\mark \markup "(Medium Fast Latin)"
	
	s1*10
	\once \override Score.RehearsalMark.self-alignment-X = #LEFT
	\mark \markup {
		\column { \boxed "B" }
		\column { "(Open Solos: Piano First - Vocal Last)" }
	}
	
	s1*16
	\once \override Score.RehearsalMark.X-offset = #2
	\mark \markup \boxed "C"
	
	s1*24
	\eolMark \markup "(D.C. al Coda)"
	\mark \markup \fontsize #3 \musicglyph #'"scripts.varcoda"
}

scoreBreaks = \new Staff \with { \RemoveEmptyStaves } {
	
	% Oculta el primer pentagrama del markup
	\override Staff.VerticalAxisGroup #'remove-first = ##t
	s1*8
	
	\break
	s1*8
	
	% A
	\break
	s1*8
	
	\break
	s1*8
	
	\break
	s1*8
	
	% B
	\break
	s1*8
	
	\break
	s1*8
	
	% C
	\break
	s1*8
	
	\break
	s1*8
	
	\break
	s1*8
	
	% Coda
	\break
	s1*6
	
	\break
	s1*6
}

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Elementos específicos del instrumento
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Cifrado
scoreChords = \chords {    
	
	e1:m
	c/e
	a/e
	c/e
	
	e2:m b:7/ds
	e2:m a:7
	fs2:3-.5-.7 e:m/g
	cs2:m7.9.11 fs:7
	
	fs1:3-.5-.7
	b1:7.9+
	e2:m7 g/d 
	c1:maj7.9
	
	cs1:3-.5-.7
	fs1:7
	fs1:3-.5-.7
	b1:7
	
	e1:m
	c/e
	a/e
	c/e
	
	e2:m b:7/ds
	e2:m a:7
	fs2:3-.5-.7 e:m/g
	cs2:m7.9.11 fs:7
	
	fs1:3-.5-.7
	b1:7.9+
	e2:m7 g/d 
	c1:maj7.9
	
	fs1:7
	fs2:3-.5-.7 b:7
	e4.:sus4 cs8:m7 s4. c8:maj7 
	s2 es:maj7.5-
	
	e4.:sus4 cs8:m7 s4. c8:maj7 
	s2 es:maj7.5-
	e4.:sus4 cs8:m7 s4. c8:maj7 
	s2 es:maj7.5-
	
	e4.:sus4 cs8:m7 s c:maj7 s b:7.9-.11+
	s1
	e4.:sus4 cs8:m7.5- s c:7.9.13 s b:9^3
	s1
	
	% solo
	e1:m
	c/e
	a/e
	c/e
	
	e2:m b:7/ds
	e2:m a:7
	fs2:3-.5-.7 e:m/g
	cs2:m7.9.11 fs:7
	
	fs1:3-.5-.7
	b1:7.9+
	e2:m7 g/d 
	c1:maj7.9
	
	cs1:3-.5-.7
	fs1:7
	fs1:3-.5-.7
	b1:7
	
	e1:m
	c/e
	a/e
	c:maj7/e
	
	e2:m b:7/ds
	e2:m a:7
	fs2:3-.5-.7 e:m/g
	cs2:m7.9.11 fs:7
	
	fs1:3-.5-.7
	b1:7.9+
	e2:m7 g/d 
	c1:maj7.9
	
	fs1:7
	fs2:3-.5-.7 b:7
	e2:m cs:m7
	c2:maj7 b:7
	
	% solo ending
	e4.:sus4 cs8:m7 s4. c8:maj7 
	s2 es:maj7.5-
	e4.:sus4 cs8:m7 s4. c8:maj7 
	s2 es:maj7.5-
	
	e4.:sus4 cs8:m7 s4. c8:maj7 
	s2 es:maj7.5-
	e4.:sus4 cs8:m7.5- s c:7.9.13 s b:7.9-.11+
	s1
	
	% coda
	e4.:m9 d8:m11 s4. cs8:m7.5-
	s1
	fs1:7
	fs2:3-.5-.7 b:7
	
	e4.:sus4 cs8:m7 s4. c8:maj7 
	s2 f:maj7.5-
	e4.:sus4 cs8:m7 s4. c8:maj7 
	s2 f:maj7.5-
	
	e4.:sus4 cs8:m7 s4. c8:maj7 
	s2 f:maj7.5-
	e4.:sus4 cs8:m7 s c:maj7 s b:7.9-.11+
	s1
	
	e4.:sus4 cs8:m7 s4. c8:maj7 
	s2 es:maj7.5-
	e4.:sus4 cs8:m7 s4. c8:maj7 
	s2 es:maj7.5-
	
	e4.:sus4 cs8:m7 s4. c8:maj7 
	s2 es:maj7.5-
	e4.:sus4 cs8:m7.5- s b:7.9- s e:m6.9
	s1
}

soloChords = \chords {   

	s1*40
	
	e1:m
	c/e
	a/e
	c/e
	
	e2:m b:7/ds
	e2:m a:7
	fs2:3-.5-.7 e:m/g
	cs2:m7.9.11 fs:7
	
	fs1:3-.5-.7
	b1:7.9+
	e2:m7 g/d 
	c1:maj7.9
	
	cs1:3-.5-.7
	fs1:7
	fs1:3-.5-.7
	b1:7
	
	e1:m
	c/e
	a/e
	c/e
	
	e2:m b:7/ds
	e2:m a:7
	fs2:3-.5-.7 e:m/g
	cs2:m7.9.11 fs:7
	
	fs1:3-.5-.7
	b1:7.9+
	e2:m7 g/d 
	c1:maj7.9
	
	fs1:7
	fs2:3-.5-.7 b:7
	e2:m cs:m7
	c2:maj7 b:7
}

% Música
trumpet = \relative c'' {
  
	\global
	\key e \minor

	\repeat volta 2 {
		R1*16
		
		\tag #'b \break
		r2 ^ "(Play 2nd x and on D.C. only)" b \mp 
		g e
		fs1 ~ 
		fs2. \> r4 \!
		
		\tag #'b \break
		R1*2
		r2 r8 b, e b' 
		cs ds b fs gs4 r4
		
		\tag #'b \break
		r2 \appoggiatura { d16 c } b8 \mf \< c e fs
		b8 \f b a b r2
		R1*2
		
		\tag #'b \break
		g2 \fp \< ~ g8 fs -^ \f r4
		r8 a -^ r4 g -^ r
		e1 \>
		R1 \!
		
		\tag #'b \break
		R1*2
		e1 \fp ^"(Play both xs)"~
		e1 \<
	}
	
	\alternative {
		{ \tag #'b \break e4. \f e8 -^ r g -^ r a -^
		  R1 }
		{ e4. \f g8 -^ r a -^ r b -^
		  R1 }
	}

	\tag #'b \break
	\repeat volta 2 {
		\improOn
		\repeat unfold 16 { r4 }
		
		\tag #'b \break
		\repeat unfold 16 { r4 }
		
		\tag #'b \break
		\repeat unfold 16 { r4 }
		
		\tag #'b \break
		\repeat unfold 16 { r4 }
		\improOff
		
		\break
		r2 ^ "(Play only on 1st & last solo)" b \mp 
		g fs
		fs1
		e2. \> r4 \!
		
		\tag #'b \break
		R1*2
		r2 e
		fs1
		
		\tag #'b \break
		e1
		b'4. b8 \bendAfter -5 r2
		R1*2
		
		\tag #'b \break
		R1*2
	}
	
	\alternative {
		{ e,1 ~ 
		  e1 \> }
		
		{ \tag #'b \break
		  R1*4 \! }
	}
	
	e,1 ~
	e1
	e'4. \f g8 -^ r a -^ r b -^
	R1
	
	\bar "||"
	\tag #'b \break
	e,4 -^ r8 g -^ r4 r8 b -> ~ 
	b4 a8 b r2
	R1*2
	
	\tag #'b \break
	R1*4
	e,,1 \fp ~
	e1 \<
	e'4. \f e8 -^ r g -^ r a -^
	R1
	
	\tag #'b \break
	e,1 \fp ~
	e1 \<
	e1 \fp ~
	e4. d8 e4 -^ e8 d
	
	\tag #'b \break
	e1 \fp ~
	e1 \<
	e'4. \f g8 -^ r a -^ r b -> ~ 
	b1 \fermata
	
	\bar "|."
}

altoSax = \relative c'' {
  
	\global
	\key e \minor

	\repeat volta 2 {
		R1*16
		
		\tag #'b \break
		r2 ^ "(Play 2nd x and on D.C. only)" b, \mp 
		g e
		ds'1
		e2. \> r4 \!
		
		\tag #'b \break
		R1*3
		e2 e8 r ds8 e
		
		\tag #'b \break
		fs1 \fp  \<
		gs8 \f gs fs g r2
		R1*2
		
		\tag #'b \break
		e2 \fp \< ~ e8 ds -^ \f r4
		r8 f -^ r4 ds -^ r
		b1 \>
		R1 \!
		
		\tag #'b \break
		R1*2
		b1 \fp ^"(Play both xs)" ~
		b1 \<
	}
	
	\alternative {
		{ \tag #'b \break 
		  b4. \f b8 -^ r e -^ r f -^
		  R1 }
		{ b,4. \f e8 -^ r d -^ r cs -^
		  R1 }
	}

	\tag #'b \break
	\repeat volta 2 {
		\improOn
		\repeat unfold 16 { r4 }
		
		\tag #'b \break
		\repeat unfold 16 { r4 }
		
		\tag #'b \break
		\repeat unfold 16 { r4 }
		
		\tag #'b \break
		\repeat unfold 16 { r4 }
		\improOff
		
		\break
		r2 ^ "(Play only on 1st & last solo)" b' \mp 
		g fs
		e1
		c2. \> r4 \!
		\tag #'b \break
		R1*2
		r2 e
		ds2 cs
		
		\tag #'b \break
		c1
		gs'4. g8 \bendAfter -5 r2
		R1*2
		
		\tag #'b \break
		R1*2
	}
	
	\alternative {
		{ b,1 ~ 
		  b1 \> }
		
		{ \tag #'b \break
		  R1*4 \! }
	}
	
	e,1 ~
	e1
	b'4. \f e8 -^ r e -^ r f -^
	R1
	
	\bar "||"
	\tag #'b \break
	b,4 -^ r8 c -^ r4 r8 e -> ~ 
	e4 ds8 e r2
	R1*2
	
	\tag #'b \break
	R1*4
	e,1 \fp ~
	e1 \<
	b'4. \f b8 -^ r e -^ r f -^
	R1
	
	\tag #'b \break
	e,1 \fp ~
	e1 \<
	e1 \fp ~
	e4. d8 e4 -^ e8 d
	
	\tag #'b \break
	e1 \fp ~
	e1 \<
	b'4. \f e8 -^ r fs -^ r g -> ~
	g1 \fermata
	
	\bar "|."
}

tenorSax = \relative c' {
  
	\global
	\key e \minor

	\repeat volta 2 {
		R1*16
		
		\tag #'b \break
		r2 ^ "(Play 2nd x and on D.C. only)" b \mp 
		g e
		cs'1
		c?2. \> r4 \!
		
		\tag #'b \break
		R1*3
		ds2 cs8 r ds8 e
		
		\tag #'b \break
		c?1 \fp \< 
		ds8 \f ds cs ds r2
		R1*2
		
		\tag #'b \break
		b2 \fp \< ~ b8 as -^ \f r4
		r8 e' -^ r4 b -^ r
		e,1 \>
		R1 \!
		
		\tag #'b \break
		R1*2
		e1 \fp ^"(Play both xs)" ~
		e1 \<
	}
	
	\alternative {
		{ \tag #'b \break 
		  e4. \f gs8 -^ r b -^ r c -^
		  R1 }
		{ e,4. \f b'8 -^ r bf -^ r a -^
		  R1 }
	}

	\tag #'b \break
	\repeat volta 2 {
		\improOn
		\repeat unfold 16 { r4 }
		
		\tag #'b \break
		\repeat unfold 16 { r4 }
		
		\tag #'b \break
		\repeat unfold 16 { r4 }
		
		\tag #'b \break
		\repeat unfold 16 { r4 }
		\improOff
		
		\break
		r2 ^ "(Play only on 1st & last solo)" b \mp 
		g fs
		cs'1
		b2. \> r4 \!
		
		\tag #'b \break
		R1*2
		r2 b
		b2 as
		
		\tag #'b \break
		a?1
		ds4. ds8 \bendAfter -5 r2
		R1*2
		
		\tag #'b \break
		R1*2
	}
	
	\alternative {
		{ e,1 ~ 
		  e1 \> }
		
		{ \tag #'b \break
		  R1*4 \! }
	}
	
	b1 ~
	b1
	e4. \f b'8 -^ r d -^ r c -^
	R1
	
	\bar "||"
	\tag #'b \break
	g4 -^ r8 a -^ r4 r8 cs -> ~ 
	cs4 b8 cs r2
	R1*2
	
	\tag #'b \break
	R1*4
	e,1 \fp ~
	e1 \<
	e4. \f gs8 -^ r b -^ r c -^
	R1
	
	\tag #'b \break
	b,1 \fp ~
	b1 \<
	b1 \fp ~
	b4. a8 b4 -^ b8 a
	
	\tag #'b \break
	b1 \fp ~
	b1 \<
	e4. \f b'8 -^ r c -^ r cs -> ~
	cs1 \fermata
	
	\bar "|."
}

trombone = \relative c' {
  
	\global
	\clef bass
	\key e \minor

	\repeat volta 2 {
		R1*16
		
		\tag #'b \break
		r2 ^ "(Play 2nd x and on D.C. only)" b \mp 
		g e
		b'1
		e,2. \> r4 \!
		
		\tag #'b \break
		R1*3
		b'2 as8 r ds, e
		
		\tag #'b \break
		a?1 \fp \<
		a8 \f a g? a r2
		R1*2
		
		\tag #'b \break
		fs2 \fp \< ~ fs8 fs -^ \f r4
		r8 c' -^ r4 a -^ r
		e1 \>
		R1 \!
		
		\tag #'b \break
		R1*2
		e1 \fp ^"(Play both xs)" ~
		e1 \<
	}
	
	\alternative {
		{ \tag #'b \break 
		  e4. \f cs8 -^ r c -^ r b -^
		  R1 }
		{ e4. \f cs8 -^ r c -^ r b -^
		  R1 }
	}

	\tag #'b \break
	\repeat volta 2 {
		\improOn
		\repeat unfold 16 { r4 }
		
		\tag #'b \break
		\repeat unfold 16 { r4 }
		
		\tag #'b \break
		\repeat unfold 16 { r4 }
		
		\tag #'b \break
		\repeat unfold 16 { r4 }
		\improOff
		
		\break
		r2 ^ "(Play only on 1st & last solo)" b' \mp 
		g fs
		a1
		g2. \> r4 \!
		
		\tag #'b \break
		R1*2
		r2 g
		gs1
		
		\tag #'b \break
		fs1
		a4. a8 \bendAfter -5 r2
		R1*2
		
		\tag #'b \break
		R1*2
	}
	
	\alternative {
		{ e1 ~ 
		  e1 \> }
		
		{ \tag #'b \break
		  R1*4 \! }
	}
	
	e,1 ~
	e1
	e'4. \f cs8 -^ r c -^ r b -^
	R1
	
	\bar "||"
	\tag #'b \break
	d4 -^ r8 f -^ r4 r8 g -> ~ 
	g4. g8 r2
	R1*2
	
	\tag #'b \break
	R1*4
	e,1 \fp ~
	e1 \<
	e'4. \f cs8 -^ r c -^ r b -^
	R1
	
	\tag #'b \break
	e,1 \fp ~
	e1 \<
	e1 \fp ~
	e2 a4 -^ a -^
	
	\tag #'b \break
	e1 \fp ~
	e1 \<
	e'4. \f c8 -^ r b -^ r e -> ~
	e1 \fermata
	
	\bar "|."
}

rhythm = \relative c' {
  
	\global
	\clef bass
	\key e \minor

	\improOn
	\repeat volta 2 {
		r4 \p ^ "(Play 2nd x and on D.C. only)"
		\repeat unfold 15 { r4 }
		
		\tag #'b \break
		\repeat unfold 16 { r4 }
		
		\tag #'b \break
		\repeat unfold 16 { r4 }
		
		\tag #'b \break
		\repeat unfold 16 { r4 }
		
		\tag #'b \break
		\repeat unfold 16 { r4 }
		
		\tag #'b \break
		\repeat unfold 16 { r4 }
		
		\tag #'b \break
		\repeat unfold 16 { r4 }
		
		\tag #'b \break
		\repeat unfold 8 { r4 }
		c4 -> \f ^"(Play both xs)" \of r8 c -> ~ c4 \of r8 c -> ~
		c2 c4 -. c -.
		
		\tag #'b \break
		c4 -> \of r8 c -> ~ c4 \of r8 c -> ~
		c2 c4 -. c -.
		c4 -> \of r8 c -> ~ c4 \of r8 c -> ~
		c2 c4 -. c -.
	}
	
	\alternative {
		{ \tag #'b \break c4. \f c8 -^ \< \of r c -^ \of r c -^ \!
		  R1 }
		{ c4. c8 -^ \< \of r c -^ \of r c -^ \!
		  R1 }
	}

	\tag #'b \break
	\repeat volta 2 {
		r4 \p \repeat unfold 15 { r4 }
		
		\tag #'b \break
		\repeat unfold 16 { r4 }
		
		\tag #'b \break
		\repeat unfold 16 { r4 }
		
		\tag #'b \break
		\repeat unfold 16 { r4 }
		
		\tag #'b \break
		\repeat unfold 16 { r4 }
		
		\tag #'b \break
		\repeat unfold 16 { r4 }
		
		\tag #'b \break
		\repeat unfold 16 { r4 }
		
		\tag #'b \break
		\repeat unfold 8 { r4 }
	}
	
	\alternative {
		{ c4 -> \f \of r8 c -> ~ c4 \of r8 c -> ~
		  c2 c4 -. c -. }
		
		{ \tag #'b \break
		  c4 -> \f \of r8 c -> ~ c4 \of r8 c -> ~
		  c2 c4 -. c -. 
		  c4 -> \of r8 c -> ~ c4 \of r8 c -> ~
		  c2 c4 -. c -. }
	}
	
	\tag #'b \break
	c4 -> \of r8 c -> ~ c4 \of r8 c -> ~
	c2 c4 -. c -.
	c4. c8 -^ \< \of r c -^ \of r a -^ \!
	R1
	
	\bar "||"
	\tag #'b \break
	c4 -^ \f \of r8 c -^ \of r4 \of r8 c -> ~ 
	c2 r4 r4
	\repeat unfold 8 { r4 }
	
	\tag #'b \break
	c4 -> \of r8 c -> ~ c4 \of r8 c -> ~
	c2 c4 -. c -.
	c4 -> \of r8 c -> ~ c4 \of r8 c -> ~
	c2 c4 -. c -.
	
	\tag #'b \break
	c4 -> \of r8 c -> ~ c4 \of r8 c -> ~
	c2 c4 -. c -.
	c4. c8 -^ \< \of r c -^ \of r c -^
	R1 \!
	
	\tag #'b \break
	c4 -> \f \of r8 c -> ~ c4 \of r8 c -> ~
	c2 c4 -. c -.
	c4 -> \of r8 c -> ~ c4 \of r8 c -> ~
	c2 c4 -. c -.
	
	\tag #'b \break
	c4 -> \of r8 c -> ~ c4 \of r8 c -> ~
	c2 c4 -. c -.
	c4. c8 -^ \< \of r c -^ \of r c -^ \! ~
	c1 \fermata
	\improOff
	
	\bar "|."
}

bass = \relative c' {
  
	\global
	\clef bass
	\key e \minor

	\repeat volta 2 {
		e,2. \p ^ "(Play 2nd x and on D.C. only)" ~ e8 b
		c4. b8 g4. b8
		e2.  ~ e8 b
		c4. b8 g4 b
		
		\improOn
		\tag #'b \break
		\repeat unfold 16 { r4 }
		
		\tag #'b \break
		\repeat unfold 16 { r4 }
		
		\tag #'b \break
		\repeat unfold 16 { r4 }
		
		\tag #'b \break
		\repeat unfold 16 { r4 }
		
		\tag #'b \break
		\repeat unfold 16 { r4 }
		
		\tag #'b \break
		\repeat unfold 16 { r4 }
		
		\tag #'b \break
		\repeat unfold 8 { r4 }
		c4 -> \f ^"(Play both xs)" \of r8 c -> ~ c4 \of r8 c -> ~
		c2 c4 -. c -.
		
		\tag #'b \break
		c4 -> \of r8 c -> ~ c4 \of r8 c -> ~
		c2 c4 -. c -.
		c4 -> \of r8 c -> ~ c4 \of r8 c -> ~
		c2 c4 -. c -.
	}
	
	\alternative {
		{ \tag #'b \break c4. \f c8 -^ \< \of r c -^ \of r c -^ \!
		  R1 }
		{ c4. c8 -^ \< \of r c -^ \of r c -^ \!
		  R1 }
	}

	\tag #'b \break
	\repeat volta 2 {
		r4 \p \repeat unfold 15 { r4 }
		
		\tag #'b \break
		\repeat unfold 16 { r4 }
		
		\tag #'b \break
		\repeat unfold 16 { r4 }
		
		\tag #'b \break
		\repeat unfold 16 { r4 }
		
		\tag #'b \break
		\repeat unfold 16 { r4 }
		
		\tag #'b \break
		\repeat unfold 16 { r4 }
		
		\tag #'b \break
		\repeat unfold 16 { r4 }
		
		\tag #'b \break
		\repeat unfold 8 { r4 }
	}
	
	\alternative {
		{ c4 -> \f \of r8 c -> ~ c4 \of r8 c -> ~
		  c2 c4 -. c -. }
		
		{ \tag #'b \break
		  c4 -> \f \of r8 c -> ~ c4 \of r8 c -> ~
		  c2 c4 -. c -. 
		  c4 -> \of r8 c -> ~ c4 \of r8 c -> ~
		  c2 c4 -. c -. }
	}
	
	\tag #'b \break
	c4 -> \of r8 c -> ~ c4 \of r8 c -> ~
	c2 c4 -. c -.
	c4. c8 -^ \< \of r c -^ \of r a -^ \!
	R1
	
	\bar "||"
	\tag #'b \break
	c4 -^ \f \of r8 c -^ \of r4 \of r8 c -> ~ 
	c2 r4 r4
	\repeat unfold 8 { r4 }
	
	\tag #'b \break
	c4 -> \of r8 c -> ~ c4 \of r8 c -> ~
	c2 c4 -. c -.
	c4 -> \of r8 c -> ~ c4 \of r8 c -> ~
	c2 c4 -. c -.
	
	\tag #'b \break
	c4 -> \of r8 c -> ~ c4 \of r8 c -> ~
	c2 c4 -. c -.
	c4. c8 -^ \< \of r c -^ \of r c -^
	R1 \!
	
	\tag #'b \break
	c4 -> \f \of r8 c -> ~ c4 \of r8 c -> ~
	c2 c4 -. c -.
	c4 -> \of r8 c -> ~ c4 \of r8 c -> ~
	c2 c4 -. c -.
	
	\tag #'b \break
	c4 -> \of r8 c -> ~ c4 \of r8 c -> ~
	c2 c4 -. c -.
	c4. c8 -^ \< \of r c -^ \of r c -^ \! ~
	c1 \fermata
	\improOff
	
	\bar "|."
}

drumsUp = \drummode {
  	
	\revert RestCollision.positioning-done
	\override Rest.staff-position = #6
	
	\dynamicUp
	\repeat volta 2 {
		\crn
		cymr8 ^ "(Play 2nd x and on D.C. only) Driven Latin" cymr8 cymr8 cymr8 cymr8 cymr8 cymr8 cymr8
		cymr8 cymr8 cymr8 cymr8 cymr16 cymr16 cymr8 cymr8 cymr8
		\crf
		s1*6
		
		\tag #'b \break
		s1*8
		
		\tag #'b \break
		s1*8
		
		\tag #'b \break
		\teeny r2 r2 \mf \< 
		dd8 \f dd dd dd r2
		s1*2
		dd2 \fp \< ~ dd8 dd -^ \f r4
		r8 dd -^ r4 dd4 -^ r
		
		\tag #'b \break
		dd4 ^\markup \normalsize "(Play both xs)" r8 dd -> ~ dd4 r8 dd -> ~
		dd2 dd4 -. dd -.
		s1*4
	}
	
	\alternative {
		{ \tag #'b \break dd4. dd8 -^ \< r dd -^  r dd -^ \!
		  s1 }
		{ dd4. dd8 -^ \< r dd -^ r dd -^
		  s1 \! }
	}

	\tag #'b \break
	\repeat volta 2 {
		\crn
		cymr8 cymr8 cymr8 cymr8 cymr8 cymr8 cymr8 cymr8
		cymr8 cymr8 cymr8 cymr8 cymr16 cymr16 cymr8 cymr8 cymr8
		\crf
		s1*6
		
		\tag #'b \break
		s1*8
		
		\tag #'b \break
		s1*8
		
		\tag #'b \break
		s1
		\teeny dd4. dd8 r2
		s1*4
	}
	
	\alternative {
		{ \tag #'b \break
		  dd4 r8 dd -> ~ dd4 r8 dd -> ~
		  dd2 dd4 -. dd -. }
		
		{ \tag #'b \break
		  dd4 r8 dd -> ~ dd4 r8 dd -> ~
		  dd2 dd4 -. dd -.
		  dd4 r8 dd -> ~ dd4 r8 dd -> ~
		  dd2 dd4 -. dd -. }
	}
	
	\tag #'b \break
	dd4 r8 dd -> ~ dd4 r8 dd -> ~
	dd2 dd4 -. dd -.
	dd4. dd8 -^ \< r dd -^ r dd -^ \!
	s1
	
	\bar "||"
	\tag #'b \break
	
	\crn
	\normalsize cymc4 s8 cymc s4 s8 cymc ~
	cymc4 \crf \teeny s4 s2
	s1*2
	
	\tag #'b \break
	dd4 r8 dd -> ~ dd4 r8 dd -> ~
	dd2 dd4 -. dd -.
	s1*4
	dd4. dd8 \< r dd r dd \!
	s1
	
	\tag #'b \break
	dd4 r8 dd -> ~ dd4 r8 dd -> ~
	dd2 dd4 -. dd -.
	s1*4
	dd4. dd8 \< r dd r dd ~ \!
	dd1 \fermata
	
	\bar "|."
}

drumsDown = \drummode {

	\global
	
	\stemDown
	\override MultiMeasureRest.staff-position = 2
	\override Stem.extra-offset = #'(.05 . 0)
	\override Beam.extra-offset = #'(.05 . 0)
	\set countPercentRepeats = ##f

	\repeat volta 2 {
		
		\override NoteHead.font-size = 1.5
  		ss4 \p ss ss ss
  		ss ss ss ss
  		\revert NoteHead.font-size
		\improOn
		\repeat percent 	14	{ r4 r4 r4 r4 }
		
		\repeat percent 	9 	{ r4 r4 r4 r4 }
		\repeat percent 	3 	{ r4 r4 r4 r4 }
		r4 r4 r4 r4
		r4 \< r4 r4 r4
		
		\repeat percent 	3	{ r4 \f r4 r4 r4 r4 r4 r4 r4 }
	}
	
	\alternative {
		{ r4 r4 r4 r4
		  R1 }
		{ r4 r4 r4 r4
		  R1 }
	}

	\repeat volta 2 {
		\improOff
  		ss4 \p ss ss ss
  		ss ss ss ss
		\improOn
		\repeat percent 	14	{ r4 r4 r4 r4 }
		
		\repeat percent 	12	{ r4 r4 r4 r4 }
		r4 r4 r4 r4
		r4 \< r4 r4 r4
	}
	
	\alternative {
		{ \repeat percent 2	{ r4 \f r4 r4 r4 } }
		
		{ \tag #'b \break
		  \repeat percent 4 { r4 r4 r4 r4 } }
	}
	
	\repeat unfold 	2	{ r4 r4 r4 r4 }
	
	\tag #'b \break
	sn4 \of r8 sn \of r sn \of r8 sn
	R1
	sn4 \of r8 sn \of r4 \of r8 sn
	r4 r4 r4 r4
	
	\repeat percent	4	{ r4 r4 r4 r4 r4 r4 r4 r4 }
	r4 r4 r4 r4
	R1
	
	\repeat percent	3	{ r4 r4 r4 r4 r4 r4 r4 r4 }
	r4 r4 r4 r4
	r4 \fermata s s2
	
	\improOff
	
	\bar "|."
}

drumsRepeats = \new DrumVoice \with { \RemoveEmptyStaves } {
	% Oculta el primer pentagrama del markup
	\override Staff.VerticalAxisGroup #'remove-first = ##t
	
  	s1*3 \pmc #4
  	s1*3 \pmc #8
  	s1*3 \pmc #12
  	s1*3 \pmc #16
  	
  	s1*3 \pmc #4
  	s1*3 \pmc #8
  	s1*3 \pmc #12
  	
  	s1*12
  	
  	s1*3 \pmc #4
  	s1*3 \pmc #8
  	s1*3 \pmc #12
  	s1*3 \pmc #16
  	
  	s1*3 \pmc #4
  	s1*3 \pmc #8
  	s1*3 \pmc #12
  	
}

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Impresión de la partitura!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~
% (Sólo se imprime el primer \book que encuentra Lilypond.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

\book {
	\header { instrument = "Score" }
	
	\paper { 
		#(set-paper-size "a4" 'landscape)
		indent = 20
		left-margin = 10
		right-margin = 10
		systems-per-page = 1
	}
	
	\score {
			% \midi { \tempo 4 = 110 }
		
			\removeWithTag #'b 
			\transpose e bf
			<<
			\scoreMarkup
			\scoreBreaks
			\new StaffGroup <<
				\new Staff \with { instrumentName = "Trumpet" midiInstrument = #"trumpet" } { \trumpet }
				\new Staff \with { instrumentName = "Alto Sax" midiInstrument = #"alto sax" } { \altoSax }
				\new Staff \with { instrumentName = "Tenor Sax" midiInstrument = #"tenor sax" } { \tenorSax }
				\new Staff \with { instrumentName = "Trombone" midiInstrument = #"trombone" } { \trombone }
			>>
			
			\scoreChords
			\new StaffGroup <<
				\new Staff \with { instrumentName = "Chords" } { \rhythm }
				\new Staff \with { instrumentName = "Bass" } { \bass }
			>>
			
			\new DrumStaff <<
  				\set DrumStaff.drumStyleTable = #(alist->hash-table nwDrums)
				\new DrumVoice { \voiceOne \drumsUp }
				\new DrumVoice { \voiceTwo \drumsDown }
  				\drumsRepeats
			>>
		>>
	}
}

instrumentName = "Trumpet"
\book {
	\header { instrument = #instrumentName }
   \bookOutputName  #(string-append title " - " instrumentName)
   
	\score {
		\transpose e bf \transpose bf c' 
		<<
			\scoreMarkup
			\soloChords
			\new StaffGroup <<
				\new Staff { \trumpet }
			>>
		>>
	}
}

instrumentName = "Alto Sax"
\book {
	\header { instrument = #instrumentName }
   \bookOutputName  #(string-append title " - " instrumentName)
   
	\score {
		\transpose e bf \transpose ef c'
		<<
			\scoreMarkup
			\soloChords
			\new StaffGroup <<
				\new Staff { \altoSax }
			>>
		>>
	}
}

instrumentName = "Tenor Sax"
\book {
	\header { instrument = #instrumentName }
	\paper { page-count = 2 }
   \bookOutputName  #(string-append title " - " instrumentName)
   
	\score {
		\transpose e bf \transpose bf c'' 
		<<
			\scoreMarkup
			\soloChords
			\new StaffGroup <<
				\new Staff { \tenorSax }
			>>
		>>
	}
}

instrumentName = "Trombone"
\book {
	\header { instrument = #instrumentName }
   \bookOutputName  #(string-append title " - " instrumentName)
   
	\score {
		\transpose e bf 
		<<
			\scoreMarkup
			\soloChords
			\new StaffGroup <<
				\new Staff { \trombone }
			>>
		>>
	}
}

instrumentName = "Chords"
\book {
	\header { instrument = #instrumentName }
   \bookOutputName  #(string-append title " - " instrumentName)
   
	\score {
		\transpose e bf
		<<
			\scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\new Staff { \rhythm }
			>>
		>>
	}
}

instrumentName = "Bass"
\book {
	\header { instrument = #instrumentName }
   \bookOutputName  #(string-append title " - " instrumentName)
   
	\score {
		\transpose e bf
		<<
			\scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\new Staff { \bass }
			>>
		>>
	}
}

instrumentName = "Drums"
\book {
	\header { instrument = #instrumentName }
   \bookOutputName  #(string-append title " - " instrumentName)
   
	\score {
		<<
			\scoreMarkup
			\new DrumStaff <<
  				\set DrumStaff.drumStyleTable = #(alist->hash-table nwDrums)
				\new DrumVoice { \voiceOne \drumsUp }
				\new DrumVoice { \voiceTwo \drumsDown }
  				\drumsRepeats
			>>
		>>
	}
}