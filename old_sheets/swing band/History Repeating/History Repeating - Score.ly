\version "2.18.2"
\language "english"


% Descripción de funcionalidades
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% PLUGINS
% - \eolMark:					mark al final de línea
% - \bendBefore: 				solución al bendBefore (i.e. dis8 \bendBefore)
% - \rit: 						crea una porción de ritardando que debe terminar como las dinámicas, con un \!
% - \improOn, \improOff:	negra de improvisación
% - \emptyStaff:				Imprime un pentagrama vacío
% - \emptyBar:					Imprime un compás invisible (útil para indentados)



% LAYOUT
% - El aspecto de los silencios multicompás es como los de las partituras de jazz
% - Margen extra de las articulaciones para evitar la colisión con las ligaduras
% - Glissandos bonitos



% PAPER
% - Se imprime el título, instrumento y número de página en todas las páginas
% - Solo se debe tocar el page-count (si eso). Sale bonito tal cual.

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Descripción de funcionalidades
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% 03-09-14: Layout
% Ahora las articulaciones van por dentro de las ligaduras. Parece que es la mejor solución



% 04-09-14: Layout
% Los creciendos y decreciendos tienen la línea un poco más gruesa
%
% Markup
% Añadidas guías para correcta posición y markup de la indicación de tempo
%
% Plugins
% Añadida función \emptyStave para imprimir un pentagrama vacío.
% En algunas partituras hay un pentagrama vacío antes del coda (lo cual mola bastante, por cierto)



% 05-09-14: Plugins
% Añadido a la función \emptyStave el esconder la armadura de la clave, que no estaba.



% 08-09-14: Layout
% Ahora no se muestra la cancelación de accidentales al haber un cambio de armadura



% 09-09-14: Chords
% Añadida sentencia para que los acordes menores salgan con un guión en vez de con una M minúscula



% 10-09-14: Plugins
% Mejorada la función \bendAfter.
% Ahora es genérica, y se puede definir cualquier texto en un lateral que haga las funciones de ornamento



% 15-09-14: Layout
% Añadido un poco más de margen a los reguladores



% 16-09-14: Plugins
% Mejorada la función para imprimir barras de improvisación.
% Antes se llamaba al comando \rs para pintar una barra que ocupaba una negra
% Se reemplaza \rs por \restImproOn y \restImproOff, siendo esta opción mucho más flexible.
% Cuando se activa \restImproOn, todo silencio se convierte en barra de improvisación, respetando su duración.



% 20-09-14: Plugins
% Mejorada la función para imprimir barras de improvisación.
% Se reemplaza \restImproOn y \restImproOff por \improOn y \improOff
% Ahora, también las notas del pentagrama salen como barra de improvisación.
% (los comandos \improvisationOn e \improvisationOff de lilypond no se llevan bien con LilyJAZZ)
% Se ha añadido un grob correspondiente a los slash de la fuente LilyJAZZ en el archivo lily-jazz.ly



% 20-09-14: Layout
% Comentada la línea que oculta la cancelación de accidentales al haber cambio de armadura.
% Realmente, la cancelación es muy bienvenida.



% 01-10-14: Plugins
% Renombrada la función \emptyStave a \emptyStaff (stave es el plural de staff) y 
% corregida (ya no muestra el número de compás).
% Recordar que después de usar \emptyStaff se necesita redefinir el 
% número de compás con \set Score.currentBarNumber = #xx
%
% Añadida la función \emptyBar que oculta un compás entero. Se puede usar para crear un 
% efecto de indentación en un pentagrama dado.
% Recordar que después de usar \emptyBar se necesita:
% - redefinir el número de compás con \set Score.currentBarNumber = #xx
% - volver a pintar la clave con \clef
% - volver a pintar la armadura con \key



% 15-10-14: Language
% Cambiado el lenguaje de entrada de notas del finlandes al inglés
% La diferencia es que ahora los sostenidos se escriben "-s" en vez de "-is"
% y los bemoles "-f" en vez de "-es".
%
% Estructura
% Para poder autoincluir los archivos de instrumento en otros 
% (para montar un midi, o un score), se han movido los plugins a un archivo separado
% y ahora se utiliza la función \includeOnce, evitando así el problema de las
% dependencias circulares.
%
% Global
% Todas las instrucciones que se colocaban justo antes de empezar la música están ahora
% recogidas en una función \global.
% A \global se le ha añadido instrucciones para que agrupe las corcheas de dos en dos
% en vez de cuatro en cuatro (el comportamiento por defecto).
%
% Plugins
% Se le ha aumentado el grosor de los bordes de la función \boxed



% 26-10-14: Chords
% Añadido el include "chord-exception.ily" que recoge una mejor notación de los acordes.
% Actualmente, recoge los acordes maj7, 7(b5), 7(#5), 7(b9), 7(#9), 7(add13), maj9(add13), 9(#11)
% Se irá añadiendo acordes a menudo.
 

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Dependencias
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% \includeOnce
% Incluye una dependencia solo una vez. Así se pueden incluir los archivos en otros 
% (para montar un score por ejemplo) sin liarla parda con dependencias cíclicas.

%{
includeOnce = #(
	define-void-function (parser location filename) (string?) (
		if (not (defined? (string->symbol filename))) (
			begin (
				ly:parser-include-string parser (
					string-concatenate (list "\\include \"" filename "\"")
				)
			)
			(primitive-eval (list 'define (string->symbol filename) #t))
		)
	)
)%}

% includes
\include "../.includes/drums.ily"
\include "../.includes/chords.ily"
\include "../.includes/lilyjazz.ily"
\include "../.includes/plugins-1.2.ily"

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~











% Disposición
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

title = "History Repeating"

% Cabecera
\header {
	title = #title
	instrument = "(as performed by Propellerheads featuring Shirley Bassey)"
	composer = "Alex Gifford"
	arranger = "Arr: Gabriel Palacios"
	poet = ""
	tagline = "gabriel.ps.ms@gmail.com"
}

% Márgenes, fuentes y distancias del papel
\paper {
	% El definir otro tipo de fuente no permite cambiar el tamaño global del staff. Es un bug conocigo
	% #(define fonts (make-pango-font-tree "LilyJAZZText" "Nimbus Sans" "Luxi Mono" (/ 20 20)))

	% distancia entre la parte superior de la página y el título
	top-markup-spacing #'basic-distance = 5

	% distancia entre el título y el primer sistema
	markup-system-spacing #'basic-distance = 25

	% distancia entre el texto y el primer sistema 
	top-system-spacing #'basic-distance = 15

	% márgen inferior
	bottom-margin = 20
	
	% intentado del primer sistema
	indent = 0
	 
	% número de páginas
	% page-count = 1

	ragged-last-bottom = ##f 

	oddHeaderMarkup = \markup {
	 \on-the-fly #not-first-page \fill-line {
		\fromproperty #'page:page-number-string
		\fromproperty #'header:title
		\fromproperty #'header:instrument
	 }
	}

	evenHeaderMarkup = \markup {
	 \on-the-fly #not-first-page \fill-line {
		\fromproperty #'header:instrument
		\fromproperty #'header:title
		\fromproperty #'page:page-number-string
	 }
	}
}

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~







% Elementos comunes de la partitura
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
	% Oculta el primer pentagrama del markup
	\override Staff.VerticalAxisGroup #'remove-first = ##t
	
	% Indicación de tiempo
	\once \override Score.RehearsalMark.self-alignment-X = #LEFT
	\once \override Score.RehearsalMark.extra-offset = #'(0 . 3) 
	\mark \markup {
		\column { "(B" }
		\hspace #-.5
		\column { \vspace #-.2 \teeny \flat }
		\hspace #-.5
		\column { "-'s diminished scale)" }
	}
	\mark "Fast Funk"

	s1*8
	\once \override Score.RehearsalMark.self-alignment-X = #LEFT
	\mark \markup {
		\column { \boxed "A" }
		\column { "(Vocal)" }
	}
	
	s1*16
	\eolMark \markup \box "1st x count in . . 3, 4"
	\once \override Score.RehearsalMark.self-alignment-X = #LEFT
	\mark \markup {
		\column { \vspace #-.3 \fontsize #2 \musicglyph #'"scripts.varsegno" } 
		\hspace #1
		\column { \boxed "B" }
		\column { "(Groove)" }
	}	
	
	s1*8
	\once \override Score.RehearsalMark.self-alignment-X = #LEFT
	\mark \markup {
		\column { \boxed "C" }
		\column { "(Vocal)" }
	}
	
	s1*16
	\mark \markup \boxed "D"
	
	s1*10
	\eolMark \markup "(D.S.)"
	\mark \markup \boxed "E"
	s1*16
	
	\mark \markup \boxed "F"
}

scoreBreaks = \new Staff \with { \RemoveEmptyStaves } {
	
	% Oculta el primer pentagrama del markup
	\override Staff.VerticalAxisGroup #'remove-first = ##t
	s1*8
	
	% A
	\break
	s1*8
	
	\break
	s1*8
	
	% B
	\break
	s1*8
	
	% C
	\break
	s1*8
	
	\break
	s1*8
	
	% D
	\break
	s1*4
	
	\break
	s1*6
	
	% E
	\break
	s1*8
	
	\break
	s1*8
	
	% F
	\break
	s1*8
	
	\break
	s1*8
	
	\break
	s1*5
}

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Elementos específicos del instrumento
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Cifrado
scoreChords = \chords {    
	\set minorChordModifier = \markup { "-" }
	\set chordNameExceptions = #chordNameExceptions
	\override ChordName.font-size = #2.5

	s1*48
	
	gf1:maj7
	s
	s
	s
	
	ef1:m7
	s
	s
	s
}

% Música
trumpet = \relative c'' {
  
	\global
	\key bf \minor
	
	R1*8
	
	R1*16
	
	\tag #'b \break
	R1*8
	
	R1*16
	
	\tag #'b \break
	\repeat volta 2 {
		bf4 \mf ^ "(ensemble)" -. r8 df -> ~ df4 bf -.
		df16 ( ef df bf ) df2 -> bf4 \jazzTurn -.
		e8 ( f -. ) r8. bf,16 ^ "(soli)" ( df8  -. ) ef ef ( f -. )
		\tag #'b \break
		f8. ( gf16 f8. gf16 f8. gf 16 f ef df c )
		
		bf4 \mf ^ "(ensemble)" -. r8 df -> ~ df4 bf -.
		df16 ( ef df bf ) df2 -> bf4 \jazzTurn -.
	}
	\alternative {
		{ \tag #'b \break
		  e8 ( f -. ) r8. bf,16 ^ "(soli)" ( df8  -. ) ef ef ( f -. )
		  f8. ( gf16 f8. gf16 f8. gf 16 f ef df c ) }
		
		{ e8 -- -> r r4 e8 ( f -. ) r4
		  R1 }
	}
	\bar "||"
	
	\tag #'b \break
	R1*16
	
	\tag #'b \break
	bf,,1 \fp ~
	bf ~ 
	bf ~
	bf2. \< r4 \!
	
	\tag #'b \break
	d1 \fp ~
	d ~ 
	d ~
	d2. \< r4 \!
	
	\tag #'b \break
	af'1 \fp ~
	af ~ 
	af \< ~
	af2. r4 \!
	
	\tag #'b \break
	bf1 \fp ~
	bf \< ~ 
	bf ~
	bf2. r4 \!
	
	R1*4
	R1 \fermataMarkup
	
	\bar "|."
}

altoSax = \relative c' {
  
	\global
	\key bf \minor
	
	R1*8
	
	R1*16
	
	\tag #'b \break
	R1*8
	
	R1*16
	
	\tag #'b \break
	\repeat volta 2 {
		bf'4 \mf -. r8 df -> ~ df4 bf -.
		df16 ( ef df bf ) df2 -> bf4 -.
		e8 ( f -. ) r4 r2
		\tag #'b \break
		R1
		
		bf,4 \mf -. r8 df -> ~ df4 bf -.
		df16 ( ef df bf ) df2 -> bf4 -.
	}
	\alternative {
		{ \tag #'b \break
		  e8 ( f -. ) r4 r2
		  R1 }
		
		{ e8 -- -> r r4 e8 ( f -. ) r4
		  R1 }
	}
	\bar "||"
	
	\tag #'b \break
	R1*16
	
	\tag #'b \break
	bf,,1 \fp ~
	bf ~ 
	bf ~
	bf2. \< r4 \!
	
	\tag #'b \break
	bf1 \fp ~
	bf ~ 
	bf ~
	bf2. \< r4 \!
	
	\tag #'b \break
	d1 \fp ~
	d ~ 
	d \< ~
	d2. r4 \!
	
	\tag #'b \break
	af'1 \fp ~
	af \< ~ 
	af ~
	af2. r4 \!
	
	R1*4
	R1 \fermataMarkup

	
	\bar "|."
}

tenorSax = \relative c' {
  
	\global
	\key bf \minor
	
	R1*8
	
	R1*16
	
	\tag #'b \break
	R1*8
	
	R1*16
	
	\tag #'b \break
	\repeat volta 4 {
		bf4 \mf -. r8 df -> ~ df4 bf -.
		df16 ( ef df bf ) df2 -> bf4 -.
		e8 ( f -. ) r4 r2
		\tag #'b \break
		R1
		
		bf,4 \mf -. r8 df -> ~ df4 bf -.
		df16 ( ef df bf ) df2 -> bf4 -.
	}
	\alternative {
		{ \tag #'b \break
		  e8 ( f -. ) r4 r2
		  R1 }
		
		{ e8 -- -> r r4 e8 ( f -. ) r4
		  R1 }
	}
	\bar "||"
	
	\tag #'b \break
	R1*16
	
	\tag #'b \break
	bf,,1 \fp ~
	bf ~ 
	bf ~
	bf2. \< r4 \!
	
	\tag #'b \break
	bf1 \fp ~
	bf ~ 
	bf ~
	bf2. \< r4 \!
	
	\tag #'b \break
	bf'1 \fp ~
	bf ~ 
	bf \< ~
	bf2. r4 \!
	
	\tag #'b \break
	e1 \fp ~
	e \< ~ 
	e ~
	e2. r4 \!
	
	R1*4
	R1 \fermataMarkup

	
	\bar "|."
}

trombone = \relative c' {
  
	\global
	\clef bass
	\key bf \minor
	
	R1*8
	
	R1*16
	
	\tag #'b \break
	R1*8
	
	R1*16
	
	\tag #'b \break
	\repeat volta 4 {
		bf4 \mf -. r8 df -> ~ df4 bf -.
		df16 ( ef df bf ) df2 -> bf4 -.
		e8 ( f -. ) r4 r2
		\tag #'b \break
		R1
		
		bf,4 \mf -. r8 df -> ~ df4 bf -.
		df16 ( ef df bf ) df2 -> bf4 -.
	}
	\alternative {
		{ \tag #'b \break
		  e8 ( f -. ) r4 r2
		  R1 }
		
		{ e8 -- -> r r4 e8 ( f -. ) r4
		  R1 }
	}
	\bar "||"
	
	\tag #'b \break
	R1*16
	
	\tag #'b \break
	bf,,1 \fp ~
	bf ~ 
	bf ~
	bf2. \< r4 \!
	
	\tag #'b \break
	bf1 \fp ~
	bf ~ 
	bf ~
	bf2. \< r4 \!
	
	\tag #'b \break
	bf1 \fp ~
	bf ~ 
	bf \< ~
	bf2. r4 \!
	
	\tag #'b \break
	d'1 \fp ~
	d \< ~ 
	d ~
	d2. r4 \!
	
	R1*4
	R1 \fermataMarkup
	
	
	\bar "|."
}

piano = \relative c' {
	
	\global
	\key bf \minor
	
	
	
	\clef bass
	<< {
		bf4. _\markup \box "E. Piano w/ reverb & delay (25% amount, 60% feedback, 4's triplet tempo)" -- bf8 -^ r4 r8 bf -^
		r4 bf4 -- df4 -- ef8 ( e -^ )
		r4 bf8 ( df ef e -^ ) r4
		R1
		
		\tag #'b \break
		bf4 -^ r8 bf8 -> ~ bf4 r8 bf -^
		r4 bf4 -- df4 -- ef8 ( e -^ )
		r4 bf8 ( df ef e -^ ) r4
		R1 
	   
	   } \\ {
		bf,4. -- \mf r8 r bf -^ r4
		r8 bf8 ~ bf bf8 -- df4 -- ef8 ( e -^ )
		r8 bf8 ( ~ bf df8 ef e -^ ) r4
		R1
		
		bf4 -^ r8 bf8 -> ~ bf4 r4
		r8 bf8 ~ bf bf8 -- df4 -- ef8 ( e -^ )
		r8 bf8 ( ~ bf df8 ef e -^ ) r4
		R1 } >>
	
	\tag #'b \break
	R1*8
	
	\tag #'b \break
	\repeat percent 3 {
		<< {
			bf'4 -^ r8 bf -> ~ bf4 bf4 -^
			r4 bf -^ r8 bf -^ r4
		   
		   } \\ {
		   bf,4 -^ \mf r8 bf -> ~ bf4 r4
			bf -^ r4 bf8 ~ bf -^ r4 } >>
	}
	
	R1*2
	
	\clef treble
	\tag #'b \break
	<e' af c>4 -. ^\markup \box "Acoustic Piano" \mf r4 <ef f af bf>4 -- f,8 <e' af c>8 ~
	<e af c>2 r8 <ef f af bf> -> ~ <ef f af bf> af,
	<e' af c>4 -. <e af c> -. <ef f af bf>4 -- f,8 <e' af c>8 ~
	<e af c>2 r8 <ef f af bf> -> ~ <ef f af bf> af,
	
	\tag #'b \break
	<e' af c>4 -. r4 <ef f af bf>4 -- f,8 <e' af c>8 ~
	<e af c>2 r8 <ef f af bf> -> ~ <ef f af bf> af,
	<e' af c>4 -. <e af c> -. <ef f af bf>4 -- f,8 <e' af c>8 ~
	<e af c>2 r2
  	
  	\clef bass
	\tag #'b \break
	\repeat percent 3 {
		<< {
			bf4 -^ ^"(Tacet on segno)" ^\markup \box "E. Piano"  r8 bf -> ~ bf4 bf4 -^
			r4 bf -^ r8 bf -^ r4
		   
		   } \\ {
		   bf,4 -^ \pp r8 bf -> ~ bf4 r4
			bf -^ r4 bf8 ~ bf -^ r4 } >>
	}
	
	\tag #'b \break
	<< {
		bf'4 -^ r8 bf8 -> ~ bf4 r8 bf -^
		r4 bf4 -- df4 -- ef8 ( e -^ )
	   
	   } \\ {
		
		bf,4 -^ r8 bf8 -> ~ bf4 r4
		r8 bf8 ~ bf bf8 -- df4 -- ef8 ( e -^ ) } >>
  	
	\tag #'b \break
	\repeat percent 3 {
		<< {
			bf'4 -^ ^"(Soli, Improvise a melody instead of written music on segno)" r8 bf -> ~ bf4 bf4 -^
			r4 bf -^ r8 bf -^ r4
		   
		   } \\ {
		   bf,4 -^ r8 bf -> ~ bf4 r4
			bf -^ r4 bf8 ~ bf -^ r4 } >>
	}
	
	\tag #'b \break
	<< {
		bf'4 -^ r8 bf8 -> ~ bf4 r8 bf -^
		r4 bf4 -- df4 -- ef8 ( e -^ )
	   
	   } \\ {
		
		bf,4 -^ r8 bf8 -> ~ bf4 r4
		r8 bf8 ~ bf bf8 -- df4 -- ef8 ( e -^ ) } >>
	
	\tag #'b \break
	\repeat volta 2 {
		\repeat percent 3 {
		<< {
			bf'4 -^ \p r8 bf -> ~ bf4 bf4 -^
			r4 bf -^ r8 bf -^ r4
		   
		   } \\ {
		   bf,4 -^ r8 bf -> ~ bf4 r4
			bf -^ r4 bf8 ~ bf -^ r4 } >>
	}
	}
	\alternative {
		{ R1*2 }
		{ R1*2 }
	}
	
	\set countPercentRepeats = ##t
  	\set repeatCountVisibility = #(every-nth-repeat-count-visible 8)
  	
  	<<
		\repeat percent 8 {
			\tag #'b \break
			<< {
				r8 ^\markup \box "E. Piano" bf df4 \portato df \portato ef \portato
				e \portato e \portato ef \portato df8 bf ~
				bf bf df4 \portato df8 ef r e -> ~
				e2. r4
				
			   } \\ {
			   <bf,, bf'>1 _\markup \box "Acoustic Piano" \sfz -> ~
			   <bf bf'> ~
			   <bf bf'> ~
			   <bf bf'>2. r4 } >>
		}
		
		\\ {
		s1*16
		
		\tag #'b \break 
		s1*16 } >>
  	
	\tag #'b \break 
	<< {
		\improOn
		r4 ^ \markup { 
			\column { "(Play random notes upwards on B" }
			\hspace #-.5
			\column { \vspace #-.2 \teeny \flat }
			\hspace #-.5
			\column { "-'s diminished scale)" }
		} ^\markup \box "Acoustic Piano" \repeat unfold 15 { r4 }
		
	   } \\ {
		<bf bf'>1 ~
		<bf bf'> ~
		<bf bf'> ~
		<bf bf'>2. r4 } >>
	
	<bf bf'>1\fermata
}

guitar = \relative c' {
  
	\global
	\key bf \minor
	
	R1*8
	
	\tag #'b \break
	\set countPercentRepeats = ##t
  	\set repeatCountVisibility = #(every-nth-repeat-count-visible 4)
  	
  	<< \repeat percent 14 { bf'1:16 \pp ^"(Very subtle w/ delay)" }
  	   
  	   \\ {
  	   s1*8
  	   
		\tag #'b \break
  	   s1*6 } >>
  	
	R1*2
	
	\tag #'b \break
	\repeat percent 8 { r4 \mf ^ "(Dirty sound like a rotatory organ)" <bf, df f> r8 <bf df f> r4 }
	
	\set countPercentRepeats = ##f
	\tag #'b \break
	<< { 
		\override Rest.staff-position = 0 
		\repeat percent 8 { r4 ^"(Tacet 1st x)" <bf df f> r8 <bf df f> r4 }
		\repeat percent 8 { r4 ^"(Tacet on segno)" <bf df f> r8 <bf df f> r4 }
	   
	   } \\ { 
	   \setManualRepeats
		s1*3 R1 ^"4"
		s1*3 R1 ^"8"
		
		\tag #'b \break 
		s1*3 R1 ^"12"
		s1*3 R1 ^"16"
	} >>
	
	
	\tag #'b \break
	
	\tag #'b \break
	\repeat volta 2 {
		\repeat percent 3 {  
			r4 r8 <bf df f> r4 <bf df f>
			r4 <bf df f> r8 <bf df f> r4
		}
	}
	\alternative {
		{ \tag #'b \break
		  r4 r8 <bf df f> r4 <bf df f>
		  r4 <bf df f> r8 <bf df f> r4 }
		{ r4 r8 <bf df f> r4 <bf df f>
		  r4 <bf df f> r8 <bf df f> r4 }
	}
	
	\tag #'b \break 
	<< { 
	   \repeat percent 16 { r4 <bf df f> r8 <bf df f> r4 } 
	   \repeat percent 16 { r4 <bf df f> r8 <bf df f> r4 } 
	   
	   } \\ { 
		s1*3 R1 ^"4"
		s1*3 R1 ^"8"
		
		\tag #'b \break 
		s1*3 R1 ^"12"
		s1*3 R1 ^"16"
		
		\tag #'b \break 
		s1*3 R1 ^"4"
		s1*3 R1 ^"8"
		
		\tag #'b \break 
		s1*3 R1 ^"12"
		s1*3 R1 ^"16"
	} >>
	
	\tag #'b \break 
	R1*4
	
	R1 \fermataMarkup
	
	\bar "|."
}

bass = \relative c' {
  
	\global
	\clef bass
	\key bf \minor
	
	R1*8
	
	\tag #'b \break
	R1*16
	
	\tag #'b \break
	bf,4. e8 f4. bf,8
	bf4. e8 f4. bf,8
	
	
	<< { 
		\override Rest.staff-position = 0 \improOn
	   \repeat percent 6 { r4 r4 r4 r4 }
	   \repeat percent 16 { r4 r4 r4 r4 }
	   
	   } \\ { \setManualRepeats
		s1 R1 ^"4"
		s1*3 R1 ^"8"
	     
		\tag #'b \break 
		s1*3 R1 ^"4"
		s1*3 R1 ^"8"
		
		\tag #'b \break 
		s1 ^"(Tacet on segno)" s1*2 R1 ^"12"
		s1*3 R1 ^"16"
	} >>
	
	\improOn
	\tag #'b \break 
	\repeat volta 2 {
		<< \repeat percent 6 { r4 ^\markup {
			\column { "(Ignore chords and play B" }
			\hspace #-.5
			\column { \vspace #-.2 \teeny \flat }
			\hspace #-.5
			\column { "-'s diminished scale on segno)" } } r4 r4 r4 }
		   
		   \\ { 
			s1*3 R1 ^"4"
			
		   s1
			\tag #'b \break 
			R1 ^"6"
		} >>
	}
	\alternative {
		{ << \repeat unfold 8 { r4 } \\ { s1 R1 ^"8" } >> }
		{ << \repeat unfold 8 { r4 } \\ { s1 R1 ^"8" } >> }
	}
	
	\tag #'b \break 
	<< { 
	   \repeat percent 16 { r4 r4 r4 r4 } 
	   \repeat percent 16 { r4 r4 r4 r4 } 
	   
	   } \\ { 
		s1*3 R1 ^"4"
		s1*3 R1 ^"8"
		
		\tag #'b \break 
		s1*3 R1 ^"12"
		s1*3 R1 ^"16"
		
		\tag #'b \break 
		s1*3 R1 ^"4"
		s1*3 R1 ^"8"
		
		\tag #'b \break 
		s1*3 R1 ^"12"
		s1*3 R1 ^"16"
	} >>
	
	\tag #'b \break
	R1*4
	
	bf1 \fermataMarkup
	
	\bar "|."
}

drumsUp = \drummode {
  	
	\revert RestCollision.positioning-done
	\override Rest.staff-position = #6
	
	\override BreathingSign.text = \markup \musicglyph #'"scripts.caesura.curved"
	\dynamicUp
	
	s1*8
	
	% A
	\tag #'b \break
	s1*8
	
	\tag #'b \break
	s1*8
	
	% B
	\tag #'b \break 
	\crn cymr4 cymr cymr cymr
	s1*7
	
	\tag #'b \break 
	cymr4 cymr cymr cymr
	s1*7
	
	\tag #'b \break 
	s1 ^"(Subito switch to very soft latin groove on segno)"
	s1*5
	s1 ^"(Tacet on segno)"
	s1
	
	\tag #'b \break 
	s1 ^"(Play on segno)"
	s1*5
	
	\tag #'b \break 
	s1*4
	
	\tag #'b \break 
	cymr4 cymr cymr cymr
	s1*7
	
	\tag #'b \break 
	s1*8
	
	\tag #'b \break 
	cymch1 -> ~ cymch ~ cymch ~ cymch
	cymch1 -> ~ cymch ~ cymch ~ cymch
	
	\tag #'b \break 
	cymch1 -> ~ cymch ~ cymch ~ cymch
	cymch1 -> ~ cymch ~ cymch ~ cymch
	
	\tag #'b \break 
	cymch1 ~ cymch ~ cymch ~ cymch
	
	cymch1 \fermata
	
	\bar "|."
}

drumsDown = \drummode {

	\global
	
	\stemDown
	\override MultiMeasureRest.staff-position = 2
	\override Stem.extra-offset = #'(.05 . 0)
	\override Beam.extra-offset = #'(.05 . 0)
	
	R1*4
	\improOn 
	\repeat percent 4 { r4 \pp ^"(very soft latin groove)" r4 r4 r4 }
	
	% A
	\repeat percent 14 { r4 r4 r4 r4 }
	R1
	R1
	
	% B
	\improOff <bd hhp>8 hhp <sn hhp> <bd hhp> <bd hhp> hhp <sn hhp> hhp
	\improOn \repeat percent 7 { r4 r4 r4 r4 }
	
	\improOff <bd hhp>8 hhp <sn hhp> <bd hhp> hhp <bd hhp> <sn hhp> hhp
	\improOn \repeat percent 7 { r4 r4 r4 r4 }
	\improOn \repeat percent 7 { r4 r4 r4 r4 }
	r4 r4 \fill { r4 r4 }
	
	\repeat volta 2 {
		\repeat percent 6 { r4 r4 r4 r4 }
	}
	\alternative {
		{ \repeat unfold 8 { r4 } }
		{ \repeat unfold 6 { r4 } \fill { r4 r4 } }
	}
	
	\improOff \repeat percent 15 { <bd hhp>8 hhp <sn hhp> <bd hhp> hhp <bd hhp> <sn hhp> hhp }
	\improOn r4 r4 \fill { r4 r4 }
	
	\repeat percent 15 { r4 r4 r4 r4 }
	r4 r4 \fill { r4 r4 }
	
	\improOff 
	\repeat percent 4 { hh8 hh hh hh hh hh hh hh }
	
	s1
	
	\bar "|."
}

drumsRepeats = \new DrumVoice \with { \RemoveEmptyStaves } {
	% Oculta el primer pentagrama del markup
	\override Staff.VerticalAxisGroup #'remove-first = ##t
	
	s1*4
	s1*3 \pmc #4
	
	s1*3 \pmc #4
	s1*3 \pmc #8
	s1*3 \pmc #12
	s1*3 \pmc #16
	
	s1*3 \pmc #4
	s1*3 \pmc #8
	
	s1*3 \pmc #4
	s1*3 \pmc #8
	s1*3 \pmc #12
	s1*3 \pmc #16
	
	s1*3 \pmc #4
	s1   \pmc #6
	s1   \pmc #8
	s1	  \pmc #8
	
	s1*3 \pmc #4
	s1*3 \pmc #8
	s1*3 \pmc #12
	s1*3 \pmc #16
	
	s1*3 \pmc #4
	s1*3 \pmc #8
	s1*3 \pmc #12
	s1*3 \pmc #16
	
	s1*3 \pmc #4
}


% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

\book {
	\paper { 
		#(set-paper-size "a4" 'landscape)
		indent = 20
		systems-per-page = 1
	}
	
	\score {
		% \midi { \tempo 4 = 55 }
		\layout {}
		\removeWithTag #'b
		<<
			\scoreMarkup
			\scoreBreaks
			\new StaffGroup <<
				\new Staff \with { instrumentName = "Trumpet" midiInstrument = #"trumpet" } { \trumpet }
				\new Staff \with { instrumentName = "Alto Sax" midiInstrument = #"alto sax" } { \altoSax }
				\new Staff \with { instrumentName = "Tenor Sax" midiInstrument = #"tenor sax" } { \tenorSax }
				\new Staff \with { instrumentName = "Trombone" midiInstrument = #"trombone" } { \trombone }
			>>
			
			\scoreChords
			\new StaffGroup <<
				\new Staff \with { instrumentName = "Piano" } { \piano }
				\new Staff \with { instrumentName = "Guitar" } { \guitar }
				\new Staff \with { instrumentName = "Bass" } { \bass }
			>>
			
			\new DrumStaff <<
  				\set DrumStaff.drumStyleTable = #(alist->hash-table nwDrums)
				\new DrumVoice { \voiceOne \drumsUp }
				\new DrumVoice { \voiceTwo \drumsDown }
  				\drumsRepeats
			>>
		>>
	}
}

instrumentName = "Trumpet"
\book {
	\header { instrument = #instrumentName }
   \bookOutputName  #(string-append title " - " instrumentName)
   
	\score {
		\transpose bf c' 
		<<
			\scoreMarkup
			\new StaffGroup <<
				\new Staff { \trumpet }
			>>
		>>
	}
}

instrumentName = "Alto Sax"
\book {
	\header { instrument = #instrumentName }
   \bookOutputName  #(string-append title " - " instrumentName)
   
	\score {
		\transpose ef c'
		<<
			\scoreMarkup
			\new StaffGroup <<
				\new Staff { \altoSax }
			>>
		>>
	}
}

instrumentName = "Tenor Sax"
\book {
	\header { instrument = #instrumentName }
   \bookOutputName  #(string-append title " - " instrumentName)
   
	\score {
		\transpose bf c''
		<<
			\scoreMarkup
			\new StaffGroup <<
				\new Staff { \tenorSax }
			>>
		>>
	}
}

instrumentName = "Trombone"
\book {
	\header { instrument = #instrumentName }
   \bookOutputName  #(string-append title " - " instrumentName)
   
	\score {
		<<
			\scoreMarkup
			\new StaffGroup <<
				\new Staff { \trombone }
			>>
		>>
	}
}

instrumentName = "Piano"
\book {
	\header { instrument = #instrumentName }
   \bookOutputName  #(string-append title " - " instrumentName)
   
	\score {
		<<
			\scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\new Staff { \piano }
			>>
		>>
	}
}

instrumentName = "Guitar"
\book {
	\header { instrument = #instrumentName }
   \bookOutputName  #(string-append title " - " instrumentName)
   
	\score {
		<<
			\scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\new Staff { \guitar }
			>>
		>>
	}
}

instrumentName = "Bass"
\book {
	\header { instrument = #instrumentName }
   \bookOutputName  #(string-append title " - " instrumentName)
   
	\score {
		<<
			\scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\new Staff { \bass }
			>>
		>>
	}
}

instrumentName = "Drums"
\book {
	\header { instrument = #instrumentName }
   \bookOutputName  #(string-append title " - " instrumentName)
   
	\score {
		<<
			\scoreMarkup
			\new DrumStaff <<
  				\set DrumStaff.drumStyleTable = #(alist->hash-table nwDrums)
				\new DrumVoice { \voiceOne \drumsUp }
				\new DrumVoice { \voiceTwo \drumsDown }
  				\drumsRepeats
			>>
		>>
	}
}