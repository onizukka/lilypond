\version "2.18.2"
\language "english"

\include "../.includes/drums.ily"
\include "../.includes/chords.ily"
\include "../.includes/lilyjazz.ily"
\include "../.includes/plugins-1.2.ily"


% Descripción de funcionalidades
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% PLUGINS
% - \eolMark:					mark al final de línea
% - \bendBefore: 				solución al bendBefore (i.e. dis8 \bendBefore)
% - \rit: 						crea una porción de ritardando que debe terminar como las dinámicas, con un \!
% - \improOn, \improOff:	negra de improvisación
% - \emptyStaff:				Imprime un pentagrama vacío
% - \emptyBar:					Imprime un compás invisible (útil para indentados)
% - \glissBefore:				muestra un glissando hacia la nota
% - \shortGlissBefore:		muestra un glissando corto hacia la nota
% - \jazzTurn:					muestra el simbolo equivalente a un turn en jazz
% - \subp:						muestra "sp", subito piano
% - \ap:							(accidental padding), separa un poco una alteración de la siguiente nota
% 									(útil al usar parenthesize)
% - \optOttava:				octavaje opcional

% LAYOUT
% - El aspecto de los silencios multicompás es como los de las partituras de jazz
% - Margen extra de las articulaciones para evitar la colisión con las ligaduras
% - Glissandos bonitos

% PAPER
% - Se imprime el título, instrumento y número de página en todas las páginas
% - Solo se debe tocar el page-count (si eso). Sale bonito tal cual.

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Changelog de funcionalidades
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Plugins
% 23-09-85: Añadida función \emptyStave para imprimir un pentagrama vacío.
% 				En algunas partituras hay un pentagrama vacío antes del coda (lo cual mola bastante, por cierto)
% 05-09-14: Añadido a la función \emptyStave el esconder la armadura de la clave, que no estaba.
% 10-09-14: Mejorada la función \bendBefore
% 				Ahora es genérica, y se puede definir cualquier texto en un lateral que haga las funciones de ornamento
% 16-09-14: Mejorada la función para imprimir barras de improvisación.
% 				Antes se llamaba al comando \rs para pintar una barra que ocupaba una negra
% 				Se reemplaza \rs por \restImproOn y \restImproOff, siendo esta opción mucho más flexible.
% 				Cuando se activa \restImproOn, todo silencio se convierte en barra de improvisación, respetando su duración.
% 20-09-14: Mejorada la función para imprimir barras de improvisación.
% 				Se reemplaza \restImproOn y \restImproOff por \improOn y \improOff
% 				Ahora, también las notas del pentagrama salen como barra de improvisación.
% 				(los comandos \improvisationOn e \improvisationOff de lilypond no se llevan bien con LilyJAZZ)
% 				Se ha añadido un grob correspondiente a los slash de la fuente LilyJAZZ en el archivo lily-jazz.ly
% 01-10-14: Plugins
% 				Renombrada la función \emptyStave a \emptyStaff (stave es el plural de staff) y 
% 				corregida (ya no muestra el número de compás).
% 				Recordar que después de usar \emptyStaff se necesita redefinir el 
% 				número de compás con \set Score.currentBarNumber = #xx
% 01-10-14: Añadida la función \emptyBar que oculta un compás entero. Se puede usar para crear un 
% 				efecto de indentación en un pentagrama dado.
% 				Recordar que después de usar \emptyBar se necesita:
% 				- redefinir el número de compás con \set Score.currentBarNumber = #xx
% 				- volver a pintar la clave con \clef
% 				- volver a pintar la armadura con \key
% 15-10-14: Se le ha aumentado el grosor de los bordes de la función \boxed
% 18-10-15: Añadidas funciones de ornamento para antes de la nota:
% 				- \glissBefore: para mostrar un glissando antes de una nota
% 				- \shortGlissBefore: para mostrar un glissando corto antes de una nota
% 				- \jazzTurn: para mostrar el turn típico trompetero de jazz
% 24-10-15: Añadida funcion \subp para mostrar subito piano "sp"
% 27-10-15: Añadida funcion \ap (de accidental padding) para separar un poco el accidental de la siguiente nota
% 				al usar un paréntesis con \parenthesize, ya que se solapan el uno al otro
% 27-10-15: Añadida funcion \optOttava para imprimir octavaje opcional
% 				También he aprovechado para cambiar la fuente del texto "8va" que utilizaba Feta por defecto
% 28-10-15: Mejorada las funciones glissBefore y shortGlissBefore.
% 				Ahora se le pasa una lista #'(r x y) en la que "r" es la rotacion, "x" la posicion horizontal 
%				e "y" la positión vertical.

% Markup
% 23-09-85: Añadidas guías para correcta posición y markup de la indicación de tempo
% 27-10-15: Guía para posición de la indicación de tempo mejorada

% Layout
% 23-09-85: Ahora las articulaciones van por dentro de las ligaduras. Parece que es la mejor solución
% 04-09-14: Los creciendos y decreciendos tienen la línea un poco más gruesa
% 08-09-14: Ahora no se muestra la cancelación de accidentales al haber un cambio de armadura
% 15-09-14: Añadido un poco más de margen a los reguladores
% 20-09-14: Comentada la línea que oculta la cancelación de accidentales al haber cambio de armadura.
% 				Realmente, la cancelación es muy bienvenida.

% Chords
% 09-09-14: Añadida sentencia para que los acordes menores salgan con un guión en vez de con una M minúscula
% 26-10-14: Añadido el include "chord-exception.ily" que recoge una mejor notación de los acordes.
% 				Actualmente, recoge los acordes maj7, 7(b5), 7(#5), 7(b9), 7(#9), 7(add13), maj9(add13), 9(#11)
% 				Se irá añadiendo acordes a menudo.

% Global
% 15-10-14: Todas las instrucciones que se colocaban justo antes de empezar la música están ahora
% 				recogidas en una función \global.
% 				A \global se le ha añadido instrucciones para que agrupe las corcheas de dos en dos
% 				en vez de cuatro en cuatro (el comportamiento por defecto).

% Language
% 15-10-14: Cambiado el lenguaje de entrada de notas del finlandes al inglés
% 				La diferencia es que ahora los sostenidos se escriben "-s" en vez de "-is"
% 				y los bemoles "-f" en vez de "-es".

% Estructura
% 15-10-14: Para poder autoincluir los archivos de instrumento en otros 
% 				(para montar un midi, o un score), se han movido los plugins a un archivo separado
% 				y ahora se utiliza la función \includeOnce, evitando así el problema de las
% 				dependencias circulares. 
% 18-10-15: Mejorada la manera de trabajar con papeles separados y score al mismo tiempo

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~











% Disposición
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

title = "Going to Chicago"

% Cabecera
\header {
	title = #title
	subtitle = "(as performed by Jarreau, Hendricks, Elling & the Metrople Orchestra)"
	instrument = ""
	composer = ""
	arranger = "Arr: Gabriel Palacios"
	poet = ""
	tagline = "gabriel.ps.ms@gmail.com"
}

% Márgenes, fuentes y distancias del papel
\paper {

	% distancia entre la parte superior de la página y el título
	top-markup-spacing #'basic-distance = 5

	% distancia entre el título y el primer sistema
	markup-system-spacing #'basic-distance = 25

	% distancia entre el texto y el primer sistema 
	top-system-spacing #'basic-distance = 15

	% márgen inferior
	bottom-margin = 20
	
	% intentado del primer sistema
	indent = 0
	 
	% número de páginas
	% page-count = 1

	ragged-last-bottom = ##f 

	oddHeaderMarkup = \markup {
	 \on-the-fly #not-first-page \fill-line {
		\fromproperty #'page:page-number-string
		\fromproperty #'header:title
		\fromproperty #'header:instrument
	 }
	}

	evenHeaderMarkup = \markup {
	 \on-the-fly #not-first-page \fill-line {
		\fromproperty #'header:instrument
		\fromproperty #'header:title
		\fromproperty #'page:page-number-string
	 }
	}
}

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~







% Elementos comunes de la partitura
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
	% Oculta el primer pentagrama del markup
	\override Staff.VerticalAxisGroup #'remove-first = ##t

	% Indicación de tiempo
	\once \override Score.RehearsalMark.self-alignment-X = #LEFT
	\once \override Score.RehearsalMark.extra-offset = #'(0 . 3) 
	\mark \markup "Slow 16's Swing"

	% s1*2
	% \once \override Score.RehearsalMark.X-offset = #5
	% \mark \markup \boxed "A"
	
	s1*12
	
	\tag #'sb \break
	s1*12
	
	\tag #'sb \break
	s1*12
	
	\tag #'sb \break
	\once \override Score.RehearsalMark.self-alignment-X = #LEFT
	\mark \markup {
		\column { \boxed "A" }
		\column { "(Drums start playing)" }
	}
	s1*6
	
	\tag #'sb \break
	s1*6
	
	\tag #'sb \break
	\once \override Score.RehearsalMark.self-alignment-X = #LEFT
	\mark \markup {
		\column { \boxed "B" }
		\column { "(Bass start playing)" }
	}
	s1*6
	
	\tag #'sb \break
	s1*6
	
	\tag #'sb \break
	\mark \markup \boxed "C"
	s1*2
	
	\once \override Score.RehearsalMark.self-alignment-X = #LEFT
	\mark \markup "(Double feel)"
	s1*2
	
	\once \override Score.RehearsalMark.self-alignment-X = #LEFT
	\mark \markup "(Normal feel - Trombone Solo)"
	s1*2
	
	\tag #'sb \break
	s1*6
	
	\tag #'sb \break
	\mark \markup \boxed "D"
	s1*6
	
	\tag #'sb \break
	s1*6
	
	\tag #'sb \break
	\once \override Score.RehearsalMark.self-alignment-X = #LEFT
	\mark \markup {
		\column { \boxed "E" }
		\column { "(Instrumental)" }
	}
	s1*6
	
	\tag #'sb \break
	s1*6
	
	\tag #'sb \break
	\mark \markup \boxed "F"
	s1*12
	
	\tag #'sb \break
	\mark \markup \boxed "G"
	s1*12
}

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Elementos específicos del instrumento
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Cifrado
scoreChords = \chords {
	
	s1*48
	
	ef1:7
	af:7
	ef:7
	bf2:sus4.7 ef:7.9+
	
	af1:7
	s1
	ef:7
	ef2:6 ef:7.9+
	
	f1:m7
	bf:7
	ef:7
	ef2:6/bf b:9
	
	df4:9 ef4:m7.5-/d ef2:7.9+
	af4:5.9/gf ef4:m6/g bf:9/af bf:7
	ef4 ef4/g c2:m7
	s16 e8.:maj7.13 ef16:7 d8:7 af8:m7/df d16:7.9- s8 ef4:6
	
	bf1:7.5-
	e2:7.9.11+.13 f4:m7 bf:7
	ef1:7
	ef2.:7 c4:7.9-
	
	f1:7
	bf1:7
	ef4:7 ef:7/g af:7 a:m5-.7
	f1:m7/bf
	
	ef1:7
	af1:7
	ef1:7
	s1
	
	af1:7
	s1
	ef1:7
	s1
	
	f1:m7
	bf:7
	ef4:7 gf:m6 af:7 a:m5-.7
	ef/bf c:7.9- f:7 bf:7
	
	ef8:7 f:7.9- bf4:7.9- ef2:7.9+
	af4:7 ef:m7.5-/a af8:6 a:7 f:m7/bf e:maj7
	af4:7.9-/ef ef2.:7.9+
	bf2:m11 ef2:7.9+
	
	af4 ef4:7.9/bf af4:9 a4:7.9+
	af4:6 d4:maj7/g af4:5.9/gf e:maj7.13
	ef2:maj7.9+.11+ af:9/d
	a2:7.5+/cs c2:7.5-
	
	fs2:m7+.9/b s8 ef:m7.5- e:maj7.5- df:7/f
	bf4:7.5+ /af b:9/a bf2:9.11+
	ef4:7 ef:7/g af:7 a:m5-.7
	f1:m7/bf
	
	s1*12
	s1*10
	ef4:7 ef:7/g af:7 a:m5-.7
	f4:m7/bf e4:maj7.9+ ef2:7.9+.11+.13
}

tromboneSoloChords = \chords {

	s1*64

	bf1:7.5-
	e2:7.9.11+.13 f4:m7 bf:7
	ef1:7
	ef2.:7 c4:7.9-
	
	f1:7
	bf1:7
	ef4:7 ef:7/g af:7 a:m5-.7
	f1:m7/bf
	
	ef1:7
	af1:7
	ef1:7
	s1
	
	af1:7
	s1
	ef1:7
	s1
	
	f1:m7
	bf:7
	ef4:7 gf:m6 af:7 a:m5-.7
	ef/bf c:7.9- f:7 bf:7
}

trumpetFillChords = \chords {
	
	s1*119
	s4 s4 ef2:7.9+.11+.13
	
}

% Música
trumpet = \relative c'' {
  
	\global
	\key ef \major
	
	R1*12
	R1*12
	R1*12
	
	% batería
	\tag #'b \break
	R1*12
	R1*11
	r2. \times 2/3 { ef8 \f gf af } 
	
	\tag #'b \break
	af4 -- gf8 bf -^ r8 ef, gf -^ af
	af4 -- gf8 bf -> ~ bf4 \times 2/3 { af8 bf df }
	ef8 -^ c16 gf -> ~ gf ef c bf ef2 -> \fp \<
	\tag #'b \break
	r16 \! gf -> ~ gf8 ef16 c8 -. gf'16 -. ~ gf ef16 -. r8 \times 2/3 { c8 -- ef -. f -. }
	
	gf2. \startTrillSpan r8 \stopTrillSpan df' -> ~
	df2. \bendAfter -8 r4
	R1*2
	
	\tag #'b \break
	R1*4
	
	\tag #'b \break
	R1*12
	
	\tag #'b \break
	r8 \f gf, f ef -> ~ ef4 ef ->
	r4 gf -> ef8 df ef ef
	ef4 -> r ^"(Sax soli)" r2
	\tag #'b \break
	R1
	
	r8 \f ^"(Ensemble)" gf f ef -> ~ ef4 c ->
	r4 gf'8 c -> ~ c4 ef,8 gf -> ~
	\tag #'b \break
	gf4 r8 af -> ~ af4 r8 a -> ~ 
	a4 r8 bf -> ~ bf4 ef,8 af -> ~
	
	af2 r8 gf ef f
	\tag #'b \break
	af4 --  gf8 bf -^ r8 gf \times 2/3 { af bf df }
	\times 2/3 { ef8 df4 -- } \times 2/3 { bf8 af4 -- } \times 2/3 { gf8 ef4 -- } \times 2/3 { df8 bf4 -- }
	R1
	
	\tag #'b \break
	R1*12
	R1*11
	r4 ^\markup \box "Harmon." r4 \p \improOn r2 ^\fermata _"(Fill lightly)" \improOff
	
	\bar "|."
}

altoSax = \relative c' {
  
	\global
	\key ef \major
	
	R1*12
	R1*12
	R1*12
	
	\tag #'b \break
	R1*12
	
	\tag #'b \break
	R1*3
	ef4 \p -> -. r8 gf -> ~ gf2 \startTrillSpan _"(easy)"
	R1*3 \stopTrillSpan
	ef4 -> -. r8 gf -> ~ gf2 \startTrillSpan  _"(easy)"
	
	\tag #'b \break
	R1*3 \stopTrillSpan
	\times 2/3 { r8 ef4 -. } ef8 gf -> ~ gf4 \times 2/3 { bf8 \f df ef } 
	
	\tag #'b \break
	ef4 -- d8 gf -^ r8 ef, gf -^ af
	af4 -- c8 d -> ~ d4 \times 2/3 { af8 bf df }
	ef8 -^ c16 gf -> ~ gf ef c bf ef2 -> \fp \<
	\tag #'b \break
	r16 \! df' -> ~ df8 bf16 a8 -. ef'16 -. ~ ef a,16 -. r8 \times 2/3 { c8 -- ef -. f -. }
	
	d2. \startTrillSpan r8 \stopTrillSpan gf -> ~
	gf2. \bendAfter -8 r4
	R1*2
	
	\tag #'b \break
	R1*4
	
	\tag #'b \break
	g,,2. \p \times 2/3 { g8 ef gf -> ~ }  
	gf2. ef8 g -> ~
	g1 \>
	r2 \! r4 \p \times 2/3 { ef8 bf' ef, }
	
	\tag #'b \break
	gf2. \times 2/3 { af8 df, ef -> ~ }
	ef2 ef8 df -. ef g? -> ~
	g1 \>
	r2 \! r4 \p e8 ef -> ~
	
	\tag #'b \break
	ef2. r4
	d1 \>
	R1*2 \!
	
	\tag #'b \break
	r8 \f ef'' d df -> ~ df4 c ->
	r4 df -> c8 g af cf
	af8 -^ ef'8-> ^"(Sax soli)" \bendBefore ~ ef2. ~ 
	\tag #'b \break
	ef4 df16 ( ^"(Straight 16's)" ef df bf af bf af gf \times 2/3 { ef'8 df ef -> ) }
	
	r8 \f ^"(Ensemble - Swing 16's)" df c bf -> ~ bf4 g ->
	r4 df'8 ef -> ~ ef4 cf8 d -> ~
	\tag #'b \break
	d4 r8 c -> ~ c4 r8 f -> ~ 
	f4 r8 e -> ~ e4 r8 f -> ~
	
	f2 r8 ef bf cf
	\tag #'b \break
	d?4 --  df8 e -^ r8 gf, \times 2/3 { af bf df }
	\times 2/3 { ef8 df4 -- } \times 2/3 { bf8 af4 -- } \times 2/3 { gf8 ef4 -- } \times 2/3 { df8 bf4 -- }
	\times 2/3 { af'8 \> gf ef } \times 2/3 { df bf af } gf8 ef r \p ef ~
	
	\tag #'b \break
	ef1 ~
	ef2 r2
	R1*10
	
	\tag #'b \break
	R1*11
	r4 g'4 \p gf2 \fermata
	
	\bar "|."
}

tenorSax = \relative c' {
  
	\global
	\key ef \major
	
	R1*12
	R1*12
	R1*12
	
	% batería
	\tag #'b \break
	R1*12
	
	% bajo
	\tag #'b \break
	R1*3
	bf4 \p -> -. r8 df -> ~ df2 \startTrillSpan _"(easy)"
	R1*3 \stopTrillSpan
	c4 -> -. r8 df -> ~ df2 \startTrillSpan _"(easy)"
	
	\tag #'b \break
	r8 \stopTrillSpan \p ef, ~ ef2 af8 d, -> ~
	d2. r4
	ef1 ~
	\times 2/3 { ef8 c'4 -. } c8 df -> ~ df4 \times 2/3 { ef8 \f gf af } 
	
	\tag #'b \break
	cf4 -- a8 df -^ r2
	c?4 -- ef,8 f -> ~ f4 r
	r2 ef'8 -^ c16 gf -> ~ gf ef c bf 
	ef16 -. \! af -> ~ af8 g?16 gf8 -. af16 -. ~ af gf16 -. r8 \times 2/3 { c,8 -- ef -. f -. }
	
	\tag #'b \break
	bf2. \startTrillSpan r8 \stopTrillSpan eff -> ~
	eff2. \bendAfter -8 r4
	R1*2
	
	\tag #'b \break
	R1*3
	r2 r4 \p \times 2/3 { bf,,8 c ef }
	
	\tag #'b \break
	g2. \times 2/3 { g8 ef gf -> ~ }  
	gf2. ef8 g -> ~
	g1 \>
	r2 \! r4 \p \times 2/3 { ef8 bf' ef, }
	
	\tag #'b \break
	gf2. \times 2/3 { af8 df, ef -> ~ }
	ef2 ef8 df -. ef g? -> ~
	g1 \>
	r2 \! r4 \p e8 ef -> ~
	
	\tag #'b \break
	ef2. ~ \times 2/3 { ef8 c cs }
	d1 \>
	R1*2 \!
	
	\tag #'b \break
	r8 \f a'' af g -> ~ g4 g ->
	r4 ef -> f8 e f e
	c8 -^ ef'8-> ^"(Sax soli)" \bendBefore ~ ef2. ~ 
	\tag #'b \break
	ef4 df16 ( ^"(Straight 16's)" ef df bf af bf af gf \times 2/3 { ef8 df ef -> ) }
	
	r8 \f ^"(Ensemble - Swing 16's)" g? g gf -> ~ gf4 df ->
	r4 a'8 af -> ~ af4 gf8 a -> ~
	\tag #'b \break
	a4 r8 bf -> ~ bf4 r8 df -> ~ 
	df4 r8 c -> ~ c4 r8 df -> ~
	
	
	df2 r8 a af af
	\tag #'b \break
	bf4 --  cf8 c -^ r2
	ef,,8 -> r g -> r af -> r a -> r 
	\times 2/3 { af'8 \> gf ef } \times 2/3 { df bf af } gf8 ef r \p ef ~
	
	\tag #'b \break
	ef1 ~
	ef2 r2
	R1*10
	
	\tag #'b \break
	R1*11
	r4 ef'4 \p df2 \fermata
	
	\bar "|."
}

trombone = \relative c' {
  
	\global
	\clef bass
	\key ef \major
	
	R1*12
	R1*12
	R1*12
	
	% batería
	\tag #'b \break
	R1*12
	
	% bajo
	\tag #'b \break
	R1*3
	af4 \p -> -. r8 g -> ~ g2
	R1*3
	bf4 -> -. r8 g -> ~ g 2
	
	\tag #'b \break
	r8 \p ef ~ ef2 af8 d, -> ~
	d2. r4
	ef1 ~
	\times 2/3 { ef8 g4 -. } g8 a -> ~ a4 \times 2/3 { ef'8 \f gf af } 
	
	\tag #'b \break
	f4 -- ef8 g -^ r2
	bf,4 -- bf8 c -> ~ c4 r
	r2 ef8 -^ c16 gf -> ~ gf4
	r16 d' -> ~ d8 df16 c8 -. cf16 -. ~ cf c16 -. r8 r4 ^\markup \box "Plunger"
	
	\tag #'b \break
	\improOn
	\repeat unfold 16 { r4 }
	\tag #'b \break
	\repeat unfold 16 { r4 }
	
	\tag #'b \break
	\repeat unfold 16 { r4 }
	\tag #'b \break
	\repeat unfold 16 { r4 }
	\tag #'b \break
	\repeat unfold 16 { r4 }
	\improOff
	
	\tag #'b \break
	r8 \f c b bf -> ~ bf4 bf ->
	r4 a -> af8 a bf af
	a4 -> r ^"(Sax soli)" r2
	\tag #'b \break
	R1
	
	r8 \f ^"(Ensemble)" df df c -> ~ c4 a ->
	r4 d?8 bf -> ~ bf4 df8 ef -> ~
	\tag #'b \break
	ef4 r8 ef -> ~ ef4 r8 g -> ~ 
	g4 r8 fs -> ~ fs4 r8 a -> ~
	
	a2 r8 df, ff df
	\tag #'b \break
	gf4 --  ef8 d? -^ r2
	ef,8 -> r g -> r af -> r a -> r 
	r2. bf,8 \p ef ~
	
	\tag #'b \break
	ef1 ~
	ef2 r2
	R1*10
	
	\tag #'b \break
	R1*11
	r4 b'4 \p g?2 \fermata
	
	\bar "|."
}

piano = \relative c' {
  
	\global
	\key ef \major
	
	R1*12
	R1*12
	R1*12
	% batería
	\tag #'b \break
	R1*12
	
	% bajo
	\improOn
	\tag #'b \break
	\repeat unfold 12 { r4 }
	r4 \of r8 c \laissezVibrer r4 r4
	\tag #'b \break
	\repeat unfold 12 { r4 }
	r4 \of r8 c \laissezVibrer r4 r4
	\tag #'b \break
	r4 r4 r4 \of r8 c \laissezVibrer
	\repeat unfold 8 { r4 }
	r4 r4 r4 \times 2/3 { c8 \f c c }
	
	\tag #'b \break
	r4 c8 c r4 r4
	r4 c8 c \laissezVibrer r4 r4
	\repeat unfold 4 { r4 }
	\of r16 c -> ~ c8 c16 c8 -. c16 -. ~ c c16 -. \of r8 \times 2/3 { c8 -- \< c -. c -. }
	\tag #'b \break
	r4 \ff r4 r4 \of r8 c -> ~
	c4 \> r4 r4 r4
	r4 \p \repeat unfold 7 { r4 }
	\tag #'b \break
	\repeat unfold 16 { r4 }
	
	\tag #'b \break
	\repeat unfold 16 { r4 }
	\tag #'b \break
	\repeat unfold 16 { r4 }
	\tag #'b \break
	\repeat unfold 8 { r4 }
	\times 2/3 { c8 \< c c } \repeat unfold 7 { \times 2/3 { c8 c c } }
	
	\tag #'b \break
	\of r8 \f c c c -> ~ c4 r
	r r c8 c c c
	r4 r4 r4 r4
	r4 r4 r4 \times 2/3 { c8 c c }
	\tag #'b \break
	\of r8 c c c -> \laissezVibrer r4 r4
	r4 c8 c -> \laissezVibrer r4 c8 c8 ~
	c4 \of r8 c -> ~ c4 \of r8 c -> ~ 
	c4 \of r8 c -> ~ c4 \of r8 c -> \laissezVibrer
	\tag #'b \break
	r4 r4 \of r8 c c c
	c4 -- c8 c -> \of r2
	r4 \mf r4 r4 r4
	r4 \> r4 r4 \of r4 \!
	
	\tag #'b \break
	R1*12
	
	\improOn
	\tag #'b \break
	R1*10
	r4 r4 r4 r4
	r4 r4 r2 \fermata
	
	\bar "|."
}

guitar = \relative c' {
  
	\global
	\key ef \major
	
	R1*12
	R1*12
	R1*12
	% batería
	\tag #'b \break
	R1*12
	
	% bajo
	\improOn
	\tag #'b \break
	\repeat unfold 12 { r4 }
	r4 \of r8 c \laissezVibrer r4 r4
	\tag #'b \break
	\repeat unfold 12 { r4 }
	r4 \of r8 c \laissezVibrer r4 r4
	\tag #'b \break
	r4 r4 r4 \of r8 c \laissezVibrer
	\repeat unfold 8 { r4 }
	r4 r4 r4 \times 2/3 { c8 \f c c }
	
	\tag #'b \break
	r4 c8 c r4 r4
	r4 c8 c \laissezVibrer r4 r4
	\repeat unfold 4 { r4 }
	\of r16 c -> ~ c8 c16 c8 -. c16 -. ~ c c16 -. \of r8 \times 2/3 { c8 -- \< c -. c -. }
	\tag #'b \break
	r4 \ff r4 r4 \of r8 c -> ~
	c4 \> r4 r4 r4
	r4 \p \repeat unfold 7 { r4 }
	\tag #'b \break
	\repeat unfold 16 { r4 }
	
	\tag #'b \break
	\repeat unfold 16 { r4 }
	\tag #'b \break
	\repeat unfold 16 { r4 }
	\tag #'b \break
	\repeat unfold 8 { r4 }
	\times 2/3 { c8 \< c c } \repeat unfold 7 { \times 2/3 { c8 c c } }
	
	\tag #'b \break
	\of r8 \f c c c -> ~ c4 r
	r r c8 c c c
	\improOff \times 2/3 { r8 c _( ^"(Soli)" ef } \times 2/3 { g ef f } \times 2/3 { gf df'4 _> ~ ) } df8 ef8 -> ~
	ef4 df16 ( ^"(Straight 16's)" ef df bf af bf af gf \times 2/3 { ef8 df ef -> ) }
	\tag #'b \break
	\improOn \of r8 c' c c -> \laissezVibrer r4 r4
	r4 c8 c -> \laissezVibrer r4 c8 \improOff gf8 ^"(Soli)" ~
	\times 2/3 { gf8 af bf } gf af -> ~ \times 2/3 { af8 bf c } af a -> ~ 
	\times 2/3 { a8 b cs } a bf -> ~ \times 2/3 { bf8 c d } bf b -> ~
	\tag #'b \break
	\times 2/3 { b8 cs ds } b cs -> ~ cs2
	R1
	\improOn
	r4 \mf r4 r4 r4
	r4 \> r4 r4 \of r4 \!
	
	\tag #'b \break
	R1*12
	
	\improOn
	\tag #'b \break
	R1*10
	r4 r4 r4 r4
	r4 r4 r2 \fermata
	
	\bar "|."
}

bass = \relative c' {
  
	\global
	\clef bass
	\key ef \major
	
	R1*12
	R1*12
	R1*12
	% batería
	\tag #'b \break
	R1*12
	
	% bajo
	\improOn
	\tag #'b \break
	\repeat unfold 12 { r4 }
	r4 c8 c \laissezVibrer r4 r4
	\tag #'b \break
	\repeat unfold 12 { r4 }
	r4 c8 c \laissezVibrer r4 r4
	\tag #'b \break
	r4 r4 r4 c8 c \laissezVibrer
	\repeat unfold 8 { r4 }
	r4 r4 r4 \times 2/3 { c8 \f c c }
	
	\tag #'b \break
	r4 c8 c r4 r4
	r4 c8 c \laissezVibrer r4 r4
	\repeat unfold 4 { r4 }
	\of r16 c -> ~ c8 c16 c8 -. c16 -. ~ c c16 -. \of r8 \times 2/3 { c8 -- \< c -. c -. }
	\tag #'b \break
	r4 \ff r4 r4 c8 -. c -> ~
	c4 \> r4 r4 r4
	r4 \! \repeat unfold 7 { r4 }
	\tag #'b \break
	\repeat unfold 16 { r4 }
	
	\tag #'b \break
	\repeat unfold 16 { r4 }
	\tag #'b \break
	\repeat unfold 16 { r4 }
	\tag #'b \break
	\repeat unfold 8 { r4 }
	\times 2/3 { c8 \< c c } \repeat unfold 7 { \times 2/3 { c8 c c } }
	
	\tag #'b \break
	c8 \f c c c -> ~ c4 r
	r r c8 c c c
	r4 r4 r4 r4
	r4 r4 r4 \times 2/3 { c8 c c }
	\tag #'b \break
	\of r8 c c c -> \laissezVibrer r4 r4
	r4 c8 c -> \laissezVibrer r4 c8 c8 ~
	c4 c8 c -> ~ c4 c8 c -> ~ 
	c4 c8 c -> ~ c4 c8 c -> \laissezVibrer
	\tag #'b \break
	r4 r4 c8 c c c
	c4 -- c8 c -> \of r2
	r4 \mf r4 r4 r4
	r4 \> r4 r4 c8 c ~
	
	\tag #'b \break
	c1 ~ \!
	c2 \of r2
	R1*10
	
	\improOn
	\tag #'b \break
	R1*10
	r4 r4 r4 r4
	r4 r4 r2 \fermata
	
	\bar "|."
}

drumsUp = \drummode {
  	
	\revert RestCollision.positioning-done
	\override Rest.staff-position = #6
	
	\dynamicUp
	\teeny
	
	\tag #'b \break
	s1*12
	s1*12
	s1*12
	
	\tag #'b \break
	s1*2 ^\markup \normalsize "(Dbl = Double Feel, Nrm = Normal feel)"
	s1*2 ^\markup \normalsize "(Dbl)"
	s1*2 ^\markup \normalsize "(Nrm)"
	\tag #'b \break
	s1*2 ^\markup \normalsize "(Dbl)"
	s1*2 ^\markup \normalsize "(Nrm)"
	s1*2 ^\markup \normalsize "(Dbl)"
	
	\tag #'b \break
	s1*3 ^\markup \normalsize "(Nrm)"
	r4 r8 dd ~ dd2
	s1*2
	\tag #'b \break
	s1
	r4 r8 dd ~ dd2
	r2 r4 r8 dd
	s1*2
	r2 r4 \times 2/3 { dd8 ^\markup \normalsize "(Nrm)" dd dd }
	
	\tag #'b \break
	r4 r8 dd -> r2
	r4 r8 dd -> ~ dd2
	s1 ^\markup \normalsize "(Dbl)"
	r16 dd -> ~ dd8 dd16 dd8 -. dd16 -. ~ dd dd16 -. r8 \times 2/3 { dd8 -- ^\markup \normalsize "(Nrm)" dd -. dd -. }
	\tag #'b \break
	dd2. r8 dd -> ~
	dd1
	s1*2
	s1*4
	
	\tag #'b \break
	s1*10
	\times 2/3 { dd8 dd dd } \repeat unfold 7 { \times 2/3 { dd8 dd dd } }
	
	\tag #'b \break
	r8 dd dd dd -> ~ dd4 dd -^
	s1*2
	r2 r4 \times 2/3 { dd8 dd dd }
	\tag #'b \break
	r8 dd dd dd -> ~ dd4 dd -^
	r4 dd8 dd -> dd4 dd8 dd8 ~
	dd4 r8 dd -> ~ dd4 r8 dd -> ~ 
	dd4 r8 dd -> ~ dd4 r8 dd -> ~
	\tag #'b \break
	dd2 r2
	r4 r8 dd -> r2
	s1*2
	
	\tag #'b \break
	s1*12
	
	\tag #'b \break
	s1*11
	r4 dd dd2 \fermata
	
	\bar "|."
}

drumsDown = \drummode {

	\global
	
	
	R1*12
	R1*12
	R1*12
	
	\improOn
	\set countPercentRepeats = ##t
  	\set repeatCountVisibility = #(every-nth-repeat-count-visible 4)
	\stemDown
	\override MultiMeasureRest.staff-position = 2
	\override Stem.extra-offset = #'(.05 . 0)
	\override Beam.extra-offset = #'(.05 . 0)
  	\override PercentRepeatCounter.direction = #DOWN
  	\override Rest.staff-position = 0
  	
	\repeat percent 12 { r4 r4 r4 r4 }
	
	\repeat percent 12 { r4 r4 r4 r4 }
	
	\repeat percent 12 { r4 r4 r4 r4 }
	
	\repeat percent 12 { r4 r4 r4 r4 }
	
	\repeat percent 12 { r4 r4 r4 r4 }
	
	r4 \improOff r4 r2
	
	R1*11
	R1*10
	\improOn
	r4 r4 r4 r4
	r4 r4 sn2 \fermata
	
	\bar "|."
}

drumsRepeats = \new DrumVoice \with { \RemoveEmptyStaves } {
	% Oculta el primer pentagrama del markup
	\override Staff.VerticalAxisGroup #'remove-first = ##t
	
	s1*12
	s1*12
	s1*12
	
	s1 \pp
	s1*11
	
	s1 \p
	s1*10
	s1 \<
	
	s1 \f s1*2 s2 s4 s4 \<
	s1 \ff s1 \> s1 \p s1
	s1*4
	
	s1*10 s1 \< s1
	
	s1 \f 
	s1*10
	s1 \>
	
	s1 \p
}

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~


\book {
	\paper { 
		#(set-paper-size "a4" 'landscape)
		indent = 20
		systems-per-page = 1
	}
	
	\score {
		% \midi { \tempo 4 = 60 }
		\layout {} 
		\removeWithTag #'b
		<<
			\scoreMarkup
			\new StaffGroup <<
				\trumpetFillChords 
				\new Staff \with { instrumentName = "Trumpet" midiInstrument = #"trumpet" } { \trumpet }
				\new Staff \with { instrumentName = "Alto Sax" midiInstrument = #"alto sax" } { \altoSax }
				\new Staff \with { instrumentName = "Tenor Sax" midiInstrument = #"tenor sax" } { \tenorSax }
				\tromboneSoloChords 
				\new Staff \with { instrumentName = "Trombone" midiInstrument = #"trombone" } { \trombone }
			>>
			
			\scoreChords
			\new StaffGroup <<
				\new Staff \with { instrumentName = "Piano" } { \piano }
				\new Staff \with { instrumentName = "Guitar" } { \guitar }
				\new Staff \with { instrumentName = "Bass" } { \bass }
			>>
			
			\new DrumStaff <<
  				\set DrumStaff.drumStyleTable = #(alist->hash-table nwDrums)
				\new DrumVoice { \voiceOne \drumsUp }
				\new DrumVoice { \voiceTwo \drumsDown }
  				\drumsRepeats
			>>
		>>
	}
}

instrumentName = "Trumpet"
\book {
	\header { instrument = #instrumentName }
   \bookOutputName  #(string-append title " - " instrumentName)
   
	\score {
		\transpose bf c' 
		<<
			\removeWithTag #'sb \scoreMarkup
			\trumpetFillChords
			\new StaffGroup <<
				\new Staff { \trumpet }
			>>
		>>
	}
}

instrumentName = "Alto Sax"
\book {
	\header { instrument = #instrumentName }
   \bookOutputName  #(string-append title " - " instrumentName)
   
	\score {
		\transpose ef c'
		<<
			\removeWithTag #'sb \scoreMarkup
			\new StaffGroup <<
				\new Staff { \altoSax }
			>>
		>>
	}
}

instrumentName = "Tenor Sax"
\book {
	\header { instrument = #instrumentName }
   \bookOutputName  #(string-append title " - " instrumentName)
   
	\score {
		\transpose bf c''
		<<
			\removeWithTag #'sb \scoreMarkup
			\new StaffGroup <<
				\new Staff { \tenorSax }
			>>
		>>
	}
}

instrumentName = "Trombone"
\book {
	\header { instrument = #instrumentName }
   \bookOutputName  #(string-append title " - " instrumentName)
   
	\score {
		<<
			\removeWithTag #'sb \scoreMarkup
			\tromboneSoloChords
			\new StaffGroup <<
				\new Staff { \trombone }
			>>
		>>
	}
}

instrumentName = "Piano"
\book {
	\header { instrument = #instrumentName }
   \bookOutputName  #(string-append title " - " instrumentName)
   
	\score {
		<<
			\removeWithTag #'sb \scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\new Staff { \piano }
			>>
		>>
	}
}

instrumentName = "Guitar"
\book {
	\header { instrument = #instrumentName }
   \bookOutputName  #(string-append title " - " instrumentName)
   
	\score {
		<<
			\removeWithTag #'sb \scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\new Staff { \guitar }
			>>
		>>
	}
}

instrumentName = "Bass"
\book {
	\header { instrument = #instrumentName }
   \bookOutputName  #(string-append title " - " instrumentName)
   
	\score {
		<<
			\removeWithTag #'sb \scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\new Staff { \bass }
			>>
		>>
	}
}

instrumentName = "Drums"
\book {
	\header { instrument = #instrumentName }
   \bookOutputName  #(string-append title " - " instrumentName)
   
	\score {
		<<
			\removeWithTag #'sb \scoreMarkup
			\new DrumStaff <<
  				\set DrumStaff.drumStyleTable = #(alist->hash-table nwDrums)
				\new DrumVoice { \voiceOne \drumsUp }
				\new DrumVoice { \voiceTwo \drumsDown }
  				\drumsRepeats
			>>
		>>
	}
}