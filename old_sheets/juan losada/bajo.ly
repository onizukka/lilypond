﻿bajoA = \relative c
{
   e,4. b'8 ~ b4 e,4 ~
   e4. b'8 ~ b4 g8 gis
   a4. e'8 ~ e4 a4 ~
   a4. a,8  ~ a8 a b c8
   
   d4. a'8 ~ a4 d4 ~
   d4. d,8 ~ d8 c a b
   g4. d'8 ~ d4 g,8 g ~ 
   g4. b8 ~ b4 g8 fis
}

bajoP = \relative c
{
   a2 e'2 
   c4. c8 ~ c a bes b ~
   b4 fis r8 g'8 fis b,
}

bajoR = \relative c
{
   e,4. b'8 ~ b4 e,4 ~
   e4. b'8 ~ b4 g8 gis
   a4. e'8 ~ e4 a4 ~
   a4. a,8  ~ a8 a b c8
   
   d4. a'8 ~ a4 d4 ~
   d4. d,8 ~ d8 c a b
   g4. d'8 ~ d4 a8 ais 
   b fis' e dis ~ dis b g fis 
}

bajoRF = \relative c
{
   e,4. b'8 ~ b4 e,4 ~
   e4. b'8 ~ b4 g8 gis
   a4. e'8 ~ e4 a4 ~
   a4. a,8  ~ a8 a b c8
   
   d4. a'8 ~ a4 d4 ~
   d4. d,8 ~ d8 c a b
   g4. d'8 ~ d4 a8 ais 
   b fis' e dis ~ dis b g fis 
   
   e4. e'8 ~ e4 b4 ~
   b4. e8 ~ e4 b4 ~
   b4 fis'8  fis ~ fis b, b4
   e,4 b'8 b'8 ~ b2
   dis,4. b'8 ~ b2
   d,4. b'8 ~ b2
   cis,4. b'8 ~ b4. e,8
   c4. g'8 ~ g e4 a,8 ~
   a8 fis4 d'8 ~ d c8 a ais 
   b b b4 r8 b4.
   b'8 r8 r4 r2
}

bajo = \new Staff
{
   \set Staff.instrumentName = #"Bajo"
   \clef bass
   \bajoA
   \bajoA
   \bajoA
   
   \bajoP
   r1
   \bajoR
   \bajoR
   
   \bajoA
   \bajoA
   \bajoA
   
   \bajoP
   r1
   \bajoR
   \bajoRF
   
   \bajoR
   \bajoR
   \relative c
   {
      e,4. b'8 ~ b4 e,4 ~
      e4. b'8 ~ b4 g8 gis
      a4. e'8 ~ e4 a4 ~
      a4. a,8  ~ a8 a b c8
      
      d4. a'8 ~ a4 d4 ~
      d4. d,8 ~ d8 c a b
      g4. d'8 ~ d4 a8 b ~
      b1 ~ b1
   }
}