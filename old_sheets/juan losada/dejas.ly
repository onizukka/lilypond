﻿
\include "voz.ly"
\include "coro.ly"
\include "big.ly"
\include "guitarra.ly"
\include "bajo.ly"

\score
{
   <<
      \voz
      \letra
      \coro
      \trompetas
      \altos
      \trombones
      \tenores
      \bajo
   >>
   \layout {}
   \midi {
      \context {
         \Score
         tempoWholesPerMinute = #(ly:make-moment 160 4)
      }
   }
}