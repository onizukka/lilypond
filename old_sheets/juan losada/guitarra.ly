﻿guitarraAA = \chordmode
{
   e8:m5.8 e4:m5.8 
   e4:m5.8 e4:m5.8 
   e4:m5.8 e4:m5.8
   e4:m7.10- e8:m7.10- r4
   
   a8:m7 a4:m7 
   a4:m7 a4:m7 
   a4:m7 a4:m7 
   a4:m7 a8:m7 r4
   
   d8:5.8 d4:5.8 
   d4:5.8 d4:5.8 
   d4:5.8 d4:5.8 
   d4:5.8 d8:5.8 r4
   
   g8:5.8 g4:5.8 
   g4:5.8 g4:5.8 
   g4:5.8 g4:5.8
   g4:5.8 g8:5.8 r4
}

guitarraPA = \chordmode
{
   c8:5.8 c4:5.8 
   c4:5.8 c4:5.8 
   c4:5.8 c4:5.8  
   c4:5.6 c8:5.6 r4
   
   b8:sus4.7 b4:sus4.7 
   b4:sus4.7 b8:sus4.7 r4
   b8:7.8^5 r r4 r2
}

guitarraRA = \chordmode
{
   e8:m5.8 e4:m5.8 
   e4:m5.8 e4:m5.8  
   e4:m5.8 e4:m5.8  
   e4:m7.10- e8:m7.10- r4
   
   a8:m7 a4:m7 
   a4:m7 a4:m7  
   a4:m7 a4:m7  
   a4:m7 a8:m7 r4
   
   d8:5.8 d4:5.8 
   d4:5.8 d4:5.8  
   d4:5.8 d4:5.8  
   d4:5.8 d8:5.8 r4
   
   g8:5.8 g4:5.8 
   g4:5.8 g4:5.8  
   b4:7.8^5 b4:7.8^5 
   b4:7.8^5 b8:7.8^5 r4
}

guitarraRAF = \chordmode
{
   e8:m5.8 e4:m5.8 
   e4:m5.8 e4:m5.8  
   e4:m5.8 e4:m5.8  
   e4:m7.10- e8:m7.10- r4
   
   a8:m7 a4:m7 
   a4:m7 a4:m7  
   a4:m7 a4:m7  
   a4:m7 a8:m7 r4
   
   d8:5.8 d4:5.8 
   d4:5.8 d4:5.8  
   d4:5.8 d4:5.8  
   d4:5.8 d8:5.8 r4
   
   g8:5.8 g4:5.8 
   g4:5.8 g4:5.8  
   b4:7.8^5 b4:7.8^5 
   b4:7.8^5 b4:7.8^5 
   b4:7.8^5 b4:7.8^5 
   b4:7.8^5 b8:7.8^5 r4
   
   e8:m5.8 e4:m5.8 
   e4:m5.8 e4:m5.8  
   b4:7.8^5 b4:7.8^5 
   b4:7.8^5 b8:7.8^5 r4
   
   e8:m7.10- e4:m7.10-
   e4:m7.10- e8:m7.10- r4
   e8:m7.10-^1/dis e4:m7.10-^1/dis
   e4:m7.10-^1/dis e8:m7.10-^1/dis r4
   e8:m7.10-^1/d e4:m7.10-^1/d
   e4:m7.10-^1/d e8:m7.10-^1/d r4
   e8:m7.10-^1/cis e4:m7.10-^1/cis
   e4:m7.10-^1/cis e8:m7.10-^1/cis r4
   
   c8:5.8 c4:5.8 
   c4:5.8 c4:5.8 
   c4:5.8 c4:5.8  
   c4:5.6 c8:5.6 r4
   
   b8:sus4.7 b4:sus4.7 
   b4:7.8^5 b8:7.8^5 r4
}

guitarra = \new Staff \transpose c c,
{
   \guitarraAA
   \guitarraAA
   \guitarraAA
   
   \guitarraPA
   \guitarraRA
   \guitarraRA
   
   \guitarraAA
   \guitarraAA
   \guitarraAA
   
   \guitarraPA
   \guitarraRA
   \guitarraRAF
   
   \guitarraRA
   \guitarraRA
   \guitarraRA
   \chordmode
   {
      e8:m5.8 e4:m5.8 
      e4:m5.8 e4:m5.8  
      e4:m5.8 e4:m5.8  
      e4:m7.10- e8:m7.10- r4
      
      a8:m7 a4:m7 
      a4:m7 a4:m7  
      a4:m7 a4:m7  
      a4:m7 a8:m7 r4
      
      d8:5.8 d4:5.8 
      d4:5.8 d4:5.8  
      d4:5.8 d4:5.8  
      d4:5.8 d8:5.8 r4
      
      g8:5.8 g4:5.8 
      g4:5.8 g4:5.8  
      b4:7.8^5 b4:7.8^5 
      b1:7.8^5 ~ b1:7.8^5
   }
}