﻿vozAA = \relative c' 
{
   r2 r4 fis8 fis 
   g fis4 e8 ~ e e4 e8 ~
   e2 ~ e4 b'8 b
   b c4 a8 ~ a g4 a8 ~
   
   a2 ~ a4 a8 a
   a g4 g8 ~ g fis4 fis8 ~
   fis2 ~ fis4 a8 g 
   a b4 g8 ~ g fis4 g8 ~
   
   g2 ~ g4 g8 g 
   g fis4 e8 ~ e e4 e8 ~
   e2 ~ e4 b'8 b
   b c4 a8 ~ a g4 a8 ~
   
   a2 ~ a4 a8 a
   a g4 g8 ~ g fis4 fis8 ~
   fis2 ~ fis4 a8 g 
   a b4 g8 ~ g fis4 g8 ~
   
   g4 g8 fis g fis g4 ~
   \times 2/3 { g4 fis g } \times 2/3 { fis g fis }
   g8 g8 ~ g4 r4 g8 fis 
   \times 2/3 { g4 a fis ~ } fis2
}

vozRA = \relative c'
{
   r4 b'8 b e fis g4 ~ 
   g8 b, b a b4 a8 b ~
   b r b b e4 g8 g ~ 
   g fis ~ fis4 ~ fis2 ~
   
   fis2 r8 g4 g8
   g fis4 fis8 fis e4 e8
   e8 d4 d8 d4 b8 b 
   c b4 a8 b4 c8 b
   
   r4 b8 b e fis g4 ~ 
   g8 b, b a b4 a8 b ~
   \times 2/3 { b4 b b } e4 g8 g ~ 
   g fis ~ fis4 ~ fis2 ~
   
   fis2 r8 g4 g8
   g fis4 fis8 fis e4 e8
   e8 d4 d8 d4. b8
   c4 b4 \times 2/3 { a4 b c ~ }
   
   c4 b4 ~ b2
}

vozRAtBA = \relative c'
{
   r4 b'8 b e fis g4 ~ 
   g8 b, b a b4 a8 b
   r4 b8 b e4 g8 g ~ 
   g fis ~ fis4 ~ fis2 ~
   
   fis2 r8 g4 g8
   g fis4 fis8 fis e4 e8
   e8 d4 d8 d4 b8 b 
   c b4 a8 b4 c8 b
   
   r4 b8 b e fis g4 ~ 
   g8 b, b a b4 a8 b ~
   \times 2/3 { b4 b b } e4 g8 g ~ 
   g fis ~ fis4 ~ fis2 ~
   
   fis2 r8 g4 g8
   g fis4 fis8 fis e4 e8
   e8 d4 d8 d4. b8
   c4 b4 \times 2/3 { a4 b c ~ }
   
   c4 b4 ~ b2 ~
}

vozBA = \relative c'
{
   b'2 r4 b8 b
   b4 a8 a ~ a4 g8 g ~
   g8 fis4 fis4 ~ fis4 r8
   
   fis'4. g8 ~ g4. g,8
   fis'4. g8 ~ g4. g,8
   fis'4. g8 ~ g4. g,8
   fis'4. g8 ~ g4. g,8
   fis'4. g8 ~ g2
   r4 e8 e ~ e e4 e8 ~
   e4 fis4 ~ fis2
}

vozN = \relative c'
{
   r1 r1 r1 r1
   r1 r1 r1
}

voz = \new Voice = "principal"
{
   r1 r1 r1 r1
   r1 r1 r1
   \vozAA
   \mark \markup {A}
   \vozRA
   \mark \markup {B}
   \vozN
   \vozAA
   \mark \markup {C}
   \vozRAtBA
   \mark \markup {D}
   \vozBA
   \mark \markup {E}
   \vozRA  
   \vozN
   r1 r1
}

letra = \new Lyrics \lyricsto "principal"
{
   Si me de -- jas de que -- rer
   vol -- ve -- rá_a -- pa -- gar -- se_el sol
   La nos -- tal -- gia de tu piel
   ca -- la -- rá mi co -- ra -- zón
   
   Si me de -- jas de que -- rer
   di -- me qué sen -- ti -- do_ha -- brá
   Pa -- ra qué_el a -- ma -- ne -- cer
   trai -- ga ga -- nas de lu -- char
   
   Si me_ha da -- do tan -- to que_al mar -- char -- te pier -- do
   to -- do lo que tengo
   
   Y_en las no -- ches frí -- as que me ser -- vi -- rán
   lu -- na de ca -- ri -- cias
   El pu -- ña -- do de be -- sos que_a -- guar -- do sin más
   co -- mo ai -- re por res -- pi -- rar
   
   Y_en -- los dí -- as gri -- ses que me con -- ta -- rán
   que to -- do_es po -- si -- ble
   Que los sue -- ños son ra -- mos de flo -- res de_a -- zahar
   dis -- pues -- tas pa -- ra cor -- tar
   
   Si me de -- jas de que -- rer
   en -- nu -- de -- ce -- rá mi voz
   mi -- ro_al cie -- lo pa -- ra ver
   ar -- co -- i -- ris sin co -- lor
   
   Si me de -- jas de que -- rer
   tal vez ya no cre -- a_en dios
   pa -- ra qué sir -- ve la fé
   si mi cre -- do_e -- ra tu_a -- mor
   
   Si te_he da -- do tan -- to que_al mar -- char -- te pier -- do
   to -- da mi ra -- rón
   
   Y_en las no -- ches frí -- as que me ser -- vi -- rán
   lu -- na de ca -- ri -- cias
   El pu -- ña -- do de be -- sos que_a -- guar -- do sin más
   co -- mo ai -- re por res -- pi -- rar
   
   Y_en -- los dí -- as gri -- ses que me con -- ta -- rán
   que to -- do_es po -- si -- ble
   Que los sue -- ños son ra -- mos de flo -- res de_a -- zahar
   dis -- pues -- tas pa -- ra cor -- tar
   
   Si te_he da -- do tan -- to que_al mar -- char
   pier -- do la vi -- da mi tiem -- po mi al -- ma los sue -- ños
   to -- do se me va
   
   Y_en las no -- ches frí -- as que me ser -- vi -- rán
   lu -- na de ca -- ri -- cias
   El pu -- ña -- do de be -- sos que_a -- guar -- do sin más
   co -- mo ai -- re por res -- pi -- rar
   
   Y_en -- los dí -- as gri -- ses que me con -- ta -- rán
   que to -- do_es po -- si -- ble
   Que los sue -- ños son ra -- mos de flo -- res de_a -- zahar
   dis -- pues -- tas pa -- ra cor -- tar
}