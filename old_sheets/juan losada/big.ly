﻿trumpetOne =  \relative c''
{
   \voiceOne
   b'4. g8 ~ g2 ~ g4. b8 ~ b8 b a4
   c4. a8 ~ a2 ~ a1
   d,4. d8 ~ d e4 fis8 ~fis g4 a8 ~ a4 d8 c
   b4 c8 b ~ b8 c4 b8 ~ b1
   
   r1 r1 r1 r1
   r1 r1 r1 r1
   
   r1 r1 r1 r1
   r1 r1 r1 r1
   
   r1 r1 r4. b,16 c d8 e fis b
   r1 r1 g8 r8 r4 r2 r1
   
   r1 a8 a a r8 r4 r8 fis ~ fis g e fis r2
   g4. g8 r c a b r8 b8 r4 r2
   
   r1 r1 r1 r1 r1 r1 r1 r1 
   b4. g8 ~ g2 ~ g4. b8 ~ b8 b a4
   c4. a8 ~ a2 ~ a1
   d,4. d8 ~ d e4 fis8 ~fis g4 a8 ~ a4 d8 c
   b4 c8 b ~ b8 c4 b8 ~ b1
   
   r1 r1 r1 r1
   r1 r1 r1 r1
   
   r1 r1 r1 r1
   r1 r1 r1 r1
   
   r1 r1 r4. b,16 c d8 e fis b
   r1 r1 g8 r8 r4 r2 r1
   
   r1 a8 a a r8 r4 r8 fis ~ fis g e fis r2
   g4. g8 r c a b r8 b8 r4 r2
   
   r1 r1 r1 r1 
   r1 r1 r1 r1 
   r1 r1 r1
   r8 a b b r2 
   r8 a4 b8 ~ b8 g8 ~ g4
   r8 a b b r2 
   r8 a4 b8 ~ b8 g8 ~ g4
   a8 b b4 r8 b8 r4
   r1
   b8 a b4 r8 a4.
   b8 r8 r4 r2
   
   r1 g8 r8 r4 r2 r1
   
   r1 a8 a a r8 r4 r8 fis ~ fis g e fis r2
   g4. g8 r c a b r8 b8 r4 r2
   
   r1 r1 r1 r1 r1 r1 r1 r1 
   b4. g8 ~ g2 ~ g4. b8 ~ b8 b a4
   c4. a8 ~ a2 ~ a1
   d,4. d8 ~ d e4 fis8 ~fis g4 a8 ~ a4 d8 c
   b4 c8 b ~ b8 c4 b8 ~ b1 ~ b1
}

trumpetTwo = \relative c''
{
   \voiceTwo
   b4. b8 ~ b2 ~ b4. b8 ~ b8 b a4
   c4. c8 ~ c2 ~ c1
   d,4. d8 ~ d e4 fis8 ~fis g4 a8 ~ a4 d8 c
   b4 c8 b ~ b4 c8 b8 ~ b1
   
   r1 r1 r1 r1
   r1 r1 r1 r1
   
   r1 r1 r1 r1
   r1 r1 r1 r1
   
   r1 r1 r4. b16 c d8 bes d g
   r1 r1 d8 r8 r4 r2 r1
   
   r1 r1 r1 
   d4. d8 r2 r8 b8 r4 r2
   
   r1 r1 r1 r1 r1 r1 r1 r1 
   b4. b8 ~ b2 ~ b4. b8 ~ b8 b a4
   c4. c8 ~ c2 ~ c1
   d,4. d8 ~ d e4 fis8 ~fis g4 a8 ~ a4 d8 c
   b4 c8 b ~ b4 c8 b8 ~ b1
   
   r1 r1 r1 r1
   r1 r1 r1 r1
   
   r1 r1 r1 r1
   r1 r1 r1 r1
   
   r1 r1 r4. b16 c d8 bes d g
   r1 r1 d8 r8 r4 r2 r1
   
   r1 r1 r1 
   d4. d8 r2 r8 b8 r4 r2
   
   r1 r1 r1 r1 
   r1 r1 r1 r1 
   r1 r1 r1
   r8 fis' g g r2 
   r8 fis4 g8 ~ g8 e8 ~ e4
   r8 fis g g r2 
   r8 fis4 g8 ~ g8 e8 ~ e4
   fis8 g g4 r8 e8 r4
   r1
   g8 g g4 r8 g4.
   g8 r8 r4 r2
   
   r1 d8 r8 r4 r2 r1
   
   r1 r1 r1 
   d4. d8 r2 r8 b8 r4 r2
   
   r1 r1 r1 r1 r1 r1 r1 r1 
   b4. b8 ~ b2 ~ b4. b8 ~ b8 b a4
   c4. c8 ~ c2 ~ c1
   d,4. d8 ~ d e4 fis8 ~fis g4 a8 ~ a4 d8 c
   b4 c8 b ~ b4 c8 b8 ~ b1 ~ b1
}

altoOne = \relative c''
{ 
   \voiceOne
   g'4. e8 ~ e2 ~ e4. g8 ~ g8 g fis4
   a4. fis8 ~ fis2 ~ fis1
   d4. d8 ~ d d4 d8 ~ d e4 fis8 ~ fis4 e8 fis
   g4 a8 g ~ g8 a4 g8 ~ g1
   
   r1 r1 r1 r1
   r1 r1 r1 r1
   
   r1 r1 r1 r1
   r1 r1 r1 r1
   
   r1 r1 r4. b,16 c d8 e fis g
   r1 r1 g8 r8 r4 r2 r1
   
   r1 a8 a a r8 r4 r8 fis ~ fis g e fis r2
   g4. g8 r c a b r8 b8 r4 r2
   
   r1 r1 r1 r1 r1 r1 r1 r1
   g4. e8 ~ e2 ~ e4. g8 ~ g8 g fis4
   a4. fis8 ~ fis2 ~ fis1
   d4. d8 ~ d d4 d8 ~ d e4 fis8 ~ fis4 e8 fis
   g4 a8 g ~ g8 a4 g8 ~ g1
   
   r1 r1 r1 r1
   r1 r1 r1 r1
   
   r1 r1 r1 r1
   r1 r1 r1 r1
   
   r1 r1 r4. b,16 c d8 e fis g
   r1 r1 g8 r8 r4 r2 r1
   
   r1 a8 a a r8 r4 r8 fis ~ fis g e fis r2
   g4. g8 r c a b r8 b8 r4 r2
   
   r1 r1 r1 r1 
   r1 r1 r1 r1 
   r1 r1 r1
   r8 e, e e r2 
   r8 e4 e8 ~ e dis8 ~ dis4
   r8 e e e r2 
   r8 e4 e8 ~ e d ~ d4
   d8 d d4 r8 c8 r4
   r1
   dis8 cis dis4 r8 cis4.
   dis8 r8 r4 r2
   
   r1 g8 r8 r4 r2 r1
   
   r1 a8 a a r8 r4 r8 fis ~ fis g e fis r2
   g4. g8 r c a b r8 b8 r4 r2
   
   r1 r1 r1 r1 r1 r1 r1 r1
   g4. e8 ~ e2 ~ e4. g8 ~ g8 g fis4
   a4. fis8 ~ fis2 ~ fis1
   d4. d8 ~ d d4 d8 ~ d e4 fis8 ~ fis4 e8 fis
   g4 a8 g ~ g8 a4 g8 ~ g1 ~ g1
}

altoTwo = \relative c''
{
   \voiceTwo
   cis4. cis8 ~ cis2 ~ cis4. cis8 ~ cis8 cis cis4
   e4. e8 ~ e2 ~ e1
   a,4. a8 ~ a a4 a8 ~ a a4 d8 ~ d4 d8 d
   d4 c8 g ~ g4 a8 g ~ g1
   
   r1 r1 r1 r1
   r1 r1 r1 r1
   
   r1 r1 r1 r1
   r1 r1 r1 r1
   
   r1 r1 r4. g16 a b8 bes a b
   r1 r1 b8 r8 r4 r2 r1
   
   r1 r1 r1 
   d4. d8 r2 r8 b8 r4 r2
   
   r1 r1 r1 r1 r1 r1 r1 r1
   cis4. cis8 ~ cis2 ~ cis4. cis8 ~ cis8 cis cis4
   e4. e8 ~ e2 ~ e1
   a,4. a8 ~ a a4 a8 ~ a a4 d8 ~ d4 d8 d
   d4 c8 g ~ g4 a8 g ~ g1
   
   r1 r1 r1 r1
   r1 r1 r1 r1
   
   r1 r1 r1 r1
   r1 r1 r1 r1
   
   r1 r1 r4. g16 a b8 bes a b
   r1 r1 b8 r8 r4 r2 r1
   
   r1 a8 a a r8 r4 r8 fis ~ fis g e fis r2
   g4. g8 r c a b r8 b8 r4 r2
   
   r1 r1 r1 r1 
   r1 r1 r1 r1 
   r1 r1 r1
   r8 a b b r2 
   r8 a4 b8 ~ b8 g8 ~ g4
   r8 a b b r2 
   r8 a4 b8 ~ b8 g8 ~ g4
   a8 b b4 r8 b8 r4
   r1
   b8 a b4 r8 a4.
   b8 r8 r4 r2
   
   r1 b8 r8 r4 r2 r1
   
   r1 r1 r1 
   d4. d8 r2 r8 b8 r4 r2
   
   r1 r1 r1 r1 r1 r1 r1 r1
   cis4. cis8 ~ cis2 ~ cis4. cis8 ~ cis8 cis cis4
   e4. e8 ~ e2 ~ e1
   a,4. a8 ~ a a4 a8 ~ a a4 d8 ~ d4 d8 d
   d4 c8 g ~ g4 d dis8 ~ dis1 ~ dis1
}

tromboneOne = \relative c'
{
   \voiceOne
   r2 r8 b' a b r e r4 r2
   a,1 ~ a1
   c4. d8 ~ d c4 d8 ~ d c4 d8 ~ d4 c8 b
   d2 ~ d4. e8 ~ e1 
   
   r1 r1 r1 r1
   r1 r1 r1 r1
   
   r1 r1 r1 r1
   r1 r1 r1 r1
   
   r1 r1 r4. b,8 g'4 b8 dis
   r1 r1 b8 r8 r4 r2 r1
   
   r8 c4. d4 e 
   d2 c2 a1 g2 a8 c a b
   r8 a8 r4 r2
   
   r1 r1 r1 r1 r1 r1 r1 r1
   r2 r8 b a b r e r4 r2
   a,1 ~ a1
   c4. d8 ~ d c4 d8 ~ d c4 d8 ~ d4 c8 b
   d2 ~ d4. e8 ~ e1 
   
   r1 r1 r1 r1
   r1 r1 r1 r1
   
   r1 r1 r1 r1
   r1 r1 r1 r1
   
   r1 r1 r4. b,8 g'4 b8 dis
   r1 r1 b8 r8 r4 r2 r1
   
   r8 c4. d4 e 
   d2 c2 a1 g2 a8 c a b
   r8 a8 r4 r2
   
   r1 r1 r1 r1 
   r1 r1 r1 r1 
   r1 r1 r1
   r2 r8 e'4 fis8 ~
   fis1
   r2 r8 e4 fis8 ~
   fis1
   e2 r8 e8 r4
   r1
   fis8 fis fis4 r8 fis4.
   fis8 r8 r4 r2
   
   r1 b,8 r8 r4 r2 r1
   
   r8 c4. d4 e 
   d2 c2 a1 g2 a8 c a b
   r8 a8 r4 r2
   
   r1 r1 r1 r1 r1 r1 r1 r1
   r2 r8 b a b r e r4 r2
   a,1 ~ a1
   c4. d8 ~ d c4 d8 ~ d c4 d8 ~ d4 c8 b
   d2 ~ d4. b8 ~ b1 ~ b1
}

tromboneTwo = \relative c'
{
   \voiceTwo
   r2 r8 g' fis g r g r4 r2
   e1 ~ e1
   a4. b8 ~ b a4 b8 ~ b c4 b8 ~ b4 a8 g
   g2 ~ g4. g8 ~ g1 
   
   r1 r1 r1 r1
   r1 r1 r1 r1
   
   r1 r1 r1 r1
   r1 r1 r1 r1
   
   r1 r1 r4. b,8 d4 d8 dis
   r1 r1 g8 r8 r4 r2 r1
   
   r8 a4. b4 c 
   a2 g2 fis2 d2 d2 d8 r8 r4
   r8 dis8 r4 r2
   
   r1 r1 r1 r1 r1 r1 r1 r1
   r2 r8 g fis g r g r4 r2
   e1 ~ e1
   a4. b8 ~ b a4 b8 ~ b c4 b8 ~ b4 a8 g
   g2 ~ g4. g8 ~ g1 
   
   r1 r1 r1 r1
   r1 r1 r1 r1
   
   r1 r1 r1 r1
   r1 r1 r1 r1
   
   r1 r1 r4. b,8 d4 d8 dis
   r1 r1 g8 r8 r4 r2 r1
   
   r8 a4. b4 c 
   a2 g2 fis2 d2 d2 d8 r8 r4
   r8 dis8 r4 r2
   
   r1 r1 r1 r1 
   r1 r1 r1 r1 
   r1 r1 r1
   r2 r8 cis'4 dis8 ~
   dis1
   r2 r8 cis4 d8 ~
   d1
   c2 r8 c8 r4
   r1
   b8 b b4 r8 b4.
   b8 r8 r4 r2
   
   r1 g8 r8 r4 r2 r1
   
   r8 a4. b4 c 
   a2 g2 fis2 d2 d2 d8 r8 r4
   r8 dis8 r4 r2
   
   r1 r1 r1 r1 r1 r1 r1 r1
   r2 r8 g fis g r g r4 r2
   e1 ~ e1
   a4. b8 ~ b a4 b8 ~ b c4 b8 ~ b4 a8 g
   g2 ~ g4. fis8 ~ fis1 
}

tenorOne = \relative c'
{
   \voiceOne
   b'1 ~ b4. b8 ~ b8 b b4
   r2 r4. c8 ~ c8 c d e ~ e g4.
   a,4. a8 ~ a g e fis8 ~ fis a b c8 ~ c2
   b2 ~ b4. d8 ~ d1 
   
   r1 r1 r1 r1
   r1 r1 r1 r1
   
   r1 r1 r1 r1
   r1 r1 r1 r1
   
   r1 r1 r4. b,8 g'4 b8 a
   r1 r1 b8 r8 r4 r2 r1
   
   r8 a4. a4 a
   d2 c2 a1 g2 g8 r8 r4
   r8 a8 r4 r2
   
   r1 r1 r1 r1 r1 r1 r1 r1
   b1 ~ b4. b8 ~ b8 b b4
   r2 r4. c8 ~ c8 c d e ~ e g4.
   a,4. a8 ~ a g e fis8 ~ fis a b c8 ~ c2
   b2 ~ b4. d8 ~ d1 
   
   r1 r1 r1 r1
   r1 r1 r1 r1
   
   r1 r1 r1 r1
   r1 r1 r1 r1
   
   r1 r1 r4. b,8 g'4 b8 a
   r1 r1 b8 r8 r4 r2 r1
   
   r8 a4. a4 a
   d2 c2 a1 g2 g8 r8 r4
   r8 a8 r4 r2
   
   r1 r1 r1 r1 
   r1 r1 r1 r1 
   r1 r1 r1
   b4. b8 ~ b b8 r4
   r4. g8 ~ g a4 g8
   b4. b8 ~ b b8 r4
   r4. g8 ~ g a4 g8
   a2 r8 a8 r4
   r1
   fis8 fis fis4 r8 fis4.
   fis8 r8 r4 r2
   
   r1 b8 r8 r4 r2 r1
   
   r8 a4. a4 a
   d2 c2 a1 g2 g8 r8 r4
   r8 a8 r4 r2
   
   r1 r1 r1 r1 r1 r1 r1 r1
   b1 ~ b4. b8 ~ b8 b b4
   r2 r4. c8 ~ c8 c d e ~ e g4.
   a,4. a8 ~ a g e fis8 ~ fis a b c8 ~ c2
   b2 ~ b4. dis8 ~ dis1 ~ dis1
}

tenorTwo = \relative c'
{
   \voiceTwo
   e1 ~ e4. e8 ~ e8 e e4
   r2 r4. g8 ~ g8 g g c ~ c b a4
   g4. e8 ~ e g e fis8 ~ fis a g fis8 ~ fis2
   g2 ~ g4. g8 ~ g1 
   
   r1 r1 r1 r1
   r1 r1 r1 r1
   
   r1 r1 r1 r1
   r1 r1 r1 r1
   
   r1 r1 r4. b,8 c4 d8 dis
   r1 r1 g8 r8 r4 r2 r1

   r1 a2 g2 fis2 d2 d2 d8 r8 r4
   r8 dis8 r4 r2
   
   r1 r1 r1 r1 r1 r1 r1 r1
   e1 ~ e4. e8 ~ e8 e e4
   r2 r4. g8 ~ g8 g g c ~ c b a4
   g4. e8 ~ e g e fis8 ~ fis a g fis8 ~ fis2
   g2 ~ g4. g8 ~ g1 
   
   r1 r1 r1 r1
   r1 r1 r1 r1
   
   r1 r1 r1 r1
   r1 r1 r1 r1
   
   r1 r1 r4. b,8 c4 d8 dis
   r1 r1 g8 r8 r4 r2 r1

   r1 a2 g2 fis2 d2 d2 d8 r8 r4
   r8 dis8 r4 r2
   
   r1 r1 r1 r1 
   r1 r1 r1 r1 
   r1 r1 r1
   e4. e8 ~ e e8 r4
   r4. dis8 ~ dis dis4 dis8
   d4. d8 ~ d d8 r4
   r4. cis8 ~ cis cis4 cis8
   c2 r8 a8 r4
   r1
   b8 b b4 r8 b4.
   b8 r8 r4 r2
   
   r1 g'8 r8 r4 r2 r1

   r1 a2 g2 fis2 d2 d2 d8 r8 r4
   r8 dis8 r4 r2
   
   r1 r1 r1 r1 r1 r1 r1 r1
   e1 ~ e4. e8 ~ e8 e e4
   r2 r4. g8 ~ g8 g g c ~ c b a4
   g4. e8 ~ e g e fis8 ~ fis a g fis8 ~ fis2
   g2 ~ g4. b8 ~ b1 ~ b1
}

trompetas = \new Staff
{
   \set Staff.instrumentName = #"Tp.1y2"
   <<
      \trumpetOne \\
      \trumpetTwo \\
   >>
}

trombones = \new Staff
{
   \set Staff.instrumentName = #"Tb.1y2"
   <<
      \tromboneOne \\
      \tromboneTwo
   >>
}

altos = \new Staff
{
   \set Staff.instrumentName = #"SxAlt.1y2"
   <<
      \altoOne \\ 
      \altoTwo \\
   >>
}

tenores = \new Staff
{
   \set Staff.instrumentName = #"Sax.Ten1y2"
   <<
      \tenorOne \\
      \tenorTwo \\
   >>
}