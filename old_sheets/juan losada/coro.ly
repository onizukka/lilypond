﻿coroA = \relative c'
{
   \voiceOne
   r1 r1 r1 r1
   r1 r1 r1 r1
   
   r1 r1 r1 r1
   r1 r1 r1 r1
   
   r1 e8 fis4 g8 ~ g b4.
   a4 r4 r2 r1
   
   r1 d2 d4 c8 b ~
   b2 r2 r1
   
   c1 a1 r1 r1
   g'4. g8 ~ g2 r1
   r2 d4 c8 b ~ b8 a8 ~ a4 r2
   
   r1 r1 r1 r1
   g'4. g8 ~ g2
   g2 d2 ~ d2 d4 c8 c
   r4 e2. d1 a1 
   
   r1 r1 r1 r1
   r1 r1 r1 r1
   r1 r1 r1 r1
   r1 r1 r1 r1
   r1 r1 
   
   r1 e8 fis4 g8 ~ g b4.
   a4 r4 r2 r1
   
   r1 d2 d4 c8 b ~
   b2 r2 r1
   
   c1 a1 r1 r1
   g'4. g8 ~ g2 r1
   r2 d4 c8 b ~ b8 a8 ~ a4 r2
   
   r1 r1 r1 r1
   g'4. g8 ~ g2
   g2 d2 ~ d2 d4 c8 c
   r4 e2. d1 a1 
   
   r1 r1 r1 r1
   r1 r1 r1 r1
   r1 r1 r1 r1 r1
   g'4. g8 ~ g2 r1
   r2 d4 c8 b ~ b8 a8 ~ a4 r2
   
   r1 r1 r1 r1
   g'4. g8 ~ g2
   g2 d2 ~ d2 d4 c8 c
   r4 e2. d1 a1 
   
   r1 r1 r1 r1
   r1 r1 r1 r1
   r1 r1 r1  
}

coroB = \relative c'
{
   \voiceTwo
   r1 r1 r1 r1
   r1 r1 r1 r1
   
   r1 r1 r1 r1
   r1 r1 r1 r1
   
   r1 e8 e4 e8 ~ e g4.
   e4 r4 r2 r1
   
   r1 a2 a4 d,8 g ~
   g2 r2 r1
   
   a1 e1 r1 r1
   e'4. e8 ~ e2 r1
   r2 d4 c8 b ~ b8 a8 ~ a4 r2
   
   r1 r1 r1 r1
   e'4. e8 ~ e2
   d2 g,2 ~ g2 g4 g8 a
   r4 c2. a2. g4 fis1
   
   r1 r1 r1 r1
   r1 r1 r1 r1
   r1 r1 r1 r1
   r1 r1 r1 r1
   r1 r1 
   
   r1 e8 e4 e8 ~ e g4.
   e4 r4 r2 r1
   
   r1 a2 a4 d,8 g ~
   g2 r2 r1
   
   a1 e1 r1 r1
   e'4. e8 ~ e2 r1
   r2 d4 c8 b ~ b8 a8 ~ a4 r2
   
   r1 r1 r1 r1
   e'4. e8 ~ e2
   d2 g,2 ~ g2 g4 g8 a
   r4 c2. a2. g4 fis1
   
   r1 r1 r1 r1
   r1 r1 r1 r1
   r1 r1 r1 r1 r1
   e'4. e8 ~ e2 r1
   r2 d4 c8 b ~ b8 a8 ~ a4 r2
   
   r1 r1 r1 r1
   e'4. e8 ~ e2
   d2 g,2 ~ g2 g4 g8 a
   r4 c2. a2. g4 fis1
   
   r1 r1 r1 r1
   r1 r1 r1 r1
   r1 r1 r1 
}

coro = \new Staff 
{
   \set Staff.instrumentName = #"Coro"
   <<
      \coroA \\
      \coroB
   >>
}