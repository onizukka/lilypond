\version "2.18.2"

\include "lily-jazz/ignatzek-jazz-chords.ily"
\include "lily-jazz/lily-jazz.ily"





% PLUGINS 1.2
% -----------
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~





% multi-mark-engraver
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
% Aporta la posibilidad de pintar un rehearsal mark al final de una línea y 
% al comienzo de la siguiente.
% http://lists.gnu.org/archive/html/lilypond-user/2011-08/msg00157.html
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
#(define (multi-mark-engraver ctx)
   (let ((texts '())
         (final-texts '())
         (events '()))

     `((start-translation-timestep
        . ,(lambda (trans)
             (set! final-texts '())))

       (listeners
        (mark-event
         . ,(lambda (trans ev)
              (set! events (cons ev events)))))

       (acknowledgers
        (break-alignment-interface
         . ,(lambda (trans grob source)
              (for-each (lambda (mark)
                          (set! (ly:grob-parent mark X) grob))
                        texts))))

       (process-music
        . ,(lambda (trans)
             (for-each
              (lambda (ev)
                (let* ((mark-grob
                        (ly:engraver-make-grob trans 'RehearsalMark ev))
                       (label (ly:event-property ev 'label))
                       (formatter (ly:context-property ctx 'markFormatter)))

                  (if (and (procedure? formatter)
                           (not (markup? label)))
                      (begin
                       (if (not (number? label))
                           (set! label
                                 (ly:context-property ctx 'rehearsalMark)))

                       (if (and (integer? label)
                                (exact? label))
                           (set! (ly:context-property ctx 'rehearsalMark)
                                 (1+ label)))

                       (if (number? label)
                           (set! label (apply formatter (list label ctx)))
                           (ly:warning "rehearsalMark must have
integer value"))))

                  (if (markup? label)
                      (begin
                       (set! (ly:grob-property mark-grob 'text) label)
                       (let ((dir (ly:event-property ev 'direction)))
                         (and (ly:dir? dir)
                              (set! (ly:grob-property mark-grob 'direction)
                                    dir))))
                      (ly:warning "mark label must be a markup object"))

                  (set! texts (cons mark-grob texts))))
              (reverse events))))

       (stop-translation-timestep
        . ,(lambda (trans)
             (if (pair? texts)
                 (let ((staves (ly:context-property ctx 'stavesFound))
                       (priority-index 0))
                   (for-each (lambda (grob)
                               (let ((my-priority (ly:grob-property
grob 'outside-staff-priority 1500)))
                                 (for-each (lambda (stave)

(ly:pointer-group-interface::add-grob grob 'side-support-elements
                                               stave))
                                           staves)
                                 (set! (ly:grob-property grob
'outside-staff-priority) (+ my-priority priority-index))
                                 (set! priority-index (1+ priority-index))
                                 (set! final-texts (cons grob final-texts))))
                             (reverse texts))
                     (set! texts '())
                     (set! events '())))))

        (finalize
         . ,(lambda (trans)
              (and (pair? final-texts)
                   (for-each (lambda (grob)
                               (set! (ly:grob-property grob 'break-visibility)
                                     end-of-line-visible))
                             final-texts)))))))


% Funciones originales
%{
myMark =
#(define-music-function (parser location text) (markup?)
  (make-music 'MarkEvent
              'label text))
			  
toCoda = {
	\tweak #'self-alignment-X #RIGHT
	\tweak #'break-visibility #begin-of-line-invisible
	\myMark \markup { \hspace #1.25 \raise #1.25 \musicglyph #"scripts.coda" }
}
%}

% Función maqueada.
% Se apoya en el código obtenido por consola al ejecutar { \displayMusic { \toCoda }}
eolMark =
#(define-music-function (parser location text) (markup?)
	(make-music 
		'MarkEvent 
		'tweaks (list 
			(cons (quote self-alignment-X) 1)
			(cons (quote break-visibility) #(#t #t #f))
		) 
		'label (markup
			#:line (
				#:hspace 1.25
				#:raise 1.25
				text
			)
		)
	)
)










% rest-score y merge-rests-on-positioning
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
% Permite realizar la fusión de los silencios entre dos voces en un mismo pentagrama.
% https://code.google.com/p/lilypond/issues/detail?id=1228 (fichero merge-rests.ily)
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]

#(define (rest-score r)
  (let ((score 0)
	(yoff (ly:grob-property-data r 'Y-offset))
	(sp (ly:grob-property-data r 'staff-position)))
    (if (number? yoff)
	(set! score (+ score 2))
	(if (eq? yoff 'calculation-in-progress)
	    (set! score (- score 3))))
    (and (number? sp)
	 (<= 0 2 sp)
	 (set! score (+ score 2))
	 (set! score (- score (abs (- 1 sp)))))
    score))

#(define (merge-rests-on-positioning grob)
  (let* ((can-merge #f)
	 (elts (ly:grob-object grob 'elements))
	 (num-elts (and (ly:grob-array? elts)
			(ly:grob-array-length elts)))
	 (two-voice? (= num-elts 2)))
    (if two-voice?
	(let* ((v1-grob (ly:grob-array-ref elts 0))
	       (v2-grob (ly:grob-array-ref elts 1))
	       (v1-rest (ly:grob-object v1-grob 'rest))
	       (v2-rest (ly:grob-object v2-grob 'rest)))
	  (and
	   (ly:grob? v1-rest)
	   (ly:grob? v2-rest)
	   (let* ((v1-duration-log (ly:grob-property v1-rest 'duration-log))
		  (v2-duration-log (ly:grob-property v2-rest 'duration-log))
		  (v1-dot (ly:grob-object v1-rest 'dot))
		  (v2-dot (ly:grob-object v2-rest 'dot))
		  (v1-dot-count (and (ly:grob? v1-dot)
				     (ly:grob-property v1-dot 'dot-count -1)))
		  (v2-dot-count (and (ly:grob? v2-dot)
				     (ly:grob-property v2-dot 'dot-count -1))))
	     (set! can-merge
		   (and
		    (number? v1-duration-log)
		    (number? v2-duration-log)
		    (= v1-duration-log v2-duration-log)
		    (eq? v1-dot-count v2-dot-count)))
	     (if can-merge
		 ;; keep the rest that looks best:
		 (let* ((keep-v1? (>= (rest-score v1-rest)
				      (rest-score v2-rest)))
			(rest-to-keep (if keep-v1? v1-rest v2-rest))
			(dot-to-kill (if keep-v1? v2-dot v1-dot)))
		   ;; uncomment if you're curious of which rest was chosen:
		   ;;(ly:grob-set-property! v1-rest 'color green)
		   ;;(ly:grob-set-property! v2-rest 'color blue)
		   (ly:grob-suicide! (if keep-v1? v2-rest v1-rest))
		   (if (ly:grob? dot-to-kill)
		       (ly:grob-suicide! dot-to-kill))
		   (ly:grob-set-property! rest-to-keep 'direction 0)
		   (ly:rest::y-offset-callback rest-to-keep)))))))
    (if can-merge
	#t
	(ly:rest-collision::calc-positioning-done grob))))










% startParenthesis y endParenthesis
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
% http://comments.gmane.org/gmane.comp.gnu.lilypond.general/64087
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]

% ELIMINADO










% bendBefore
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
% Adaptación
% http://lilypondblog.org/2013/08/adding-ornamentations-to-note-heads-part-1/
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]

%%%
%%% Utilities for defining new grobs, grob properties and music event types
%%% (there should be built-in commands to do that in LilyPond)
%%%
#(define (define-grob-type grob-name grob-entry)
   (let* ((meta-entry   (assoc-get 'meta grob-entry))
          (class        (assoc-get 'class meta-entry))
          (ifaces-entry (assoc-get 'interfaces meta-entry)))
     (set-object-property! grob-name 'translation-type? list?)
     (set-object-property! grob-name 'is-grob? #t)
     (set! ifaces-entry (append (case class
                                  ((Item) '(item-interface))
                                  ((Spanner) '(spanner-interface))
                                  ((Paper_column) '((item-interface
                                                     paper-column-interface)))
                                  ((System) '((system-interface
                                               spanner-interface)))
                                  (else '(unknown-interface)))
                                ifaces-entry))
     (set! ifaces-entry (uniq-list (sort ifaces-entry symbol<?)))
     (set! ifaces-entry (cons 'grob-interface ifaces-entry))
     (set! meta-entry (assoc-set! meta-entry 'name grob-name))
     (set! meta-entry (assoc-set! meta-entry 'interfaces
                                  ifaces-entry))
     (set! grob-entry (assoc-set! grob-entry 'meta meta-entry))
     (set! all-grob-descriptions
           (cons (cons grob-name grob-entry)
                 all-grob-descriptions))))

#(define-public (define-grob-property symbol type? description)
  (set-object-property! symbol 'backend-type? type?)
  (set-object-property! symbol 'backend-doc description)
  symbol)

#(defmacro*-public define-music-type (type-name parents #:rest properties)
   (let ((gproperties (gensym "properties")))
     `(let ((,gproperties (list-copy ',properties)))
        ,@(map (lambda (parent)
                `(define-event-class ',(ly:camel-case->lisp-identifier type-name)
                   ',parent))
               parents)
        (set-object-property! ',type-name
                              'music-description
                              (cdr (assq 'description ,gproperties)))
        (set! ,gproperties (assoc-set! ,gproperties 'name ',type-name))
        (set! ,gproperties (assq-remove! ,gproperties 'description))
        (hashq-set! music-name-to-property-table ',type-name ,gproperties)
        (set! music-descriptions
              (cons (cons ',type-name ,gproperties)
                    music-descriptions)))))

%%%
%%% Grob definition
%%%
#(define (head-ornamentation::print me)
   (let* ((notes (ly:grob-object me 'elements))
          (y-ref (ly:grob-common-refpoint-of-array me notes Y))
          (x-ref (ly:grob-common-refpoint-of-array me notes X))
          (x-ext (ly:relative-group-extent notes x-ref X))
          (y-ext (ly:relative-group-extent notes y-ref Y))
          (y-coord (interval-center y-ext))
          (text (ly:text-interface::print me))
          (width (/ (interval-length (ly:stencil-extent text X)) 2.0))
          (x-coord (- (car x-ext) width)))
     (ly:stencil-translate
      text
      (cons
       (- (- x-coord (ly:grob-relative-coordinate me x-ref X)) .7)
       (- (- y-coord (ly:grob-relative-coordinate me y-ref Y)) 1.5)))))

#(define-grob-type 'HeadOrnamentation
  `((text . ,#{ \markup \fontsize #-4 \musicglyph #"brackettips.up" #})
    (stencil . ,head-ornamentation::print)
    (meta . ((class . Item)
             (interfaces . (font-interface))))))

\layout {
  \context {
    \Global
    \grobdescriptions #all-grob-descriptions
  }
}

%%%
%%% Engraver
%%%
%%% The head-ornamentation engraver, with its note-head acknowledger
%%% (which add HeadOrnamentation grobs to note heads)
#(define head-ornamentation-engraver
   (make-engraver
    (acknowledgers
     ((note-head-interface engraver note-grob source)
      ;; When the note-head event attached to the note-head grob has
      ;; ornamentation events among its articulations, then create a
      ;; HeadOrnamentation grob
      (let* ((note-event (ly:grob-property note-grob 'cause)))
        (for-each
         (lambda (articulation)
           (if (memq 'head-ornamentation-event
                     (ly:event-property articulation 'class))
               (let ((ornament-grob (ly:engraver-make-grob engraver
                                                           'HeadOrnamentation
                                                           note-grob)))
                 (ly:pointer-group-interface::add-grob ornament-grob
                                                       'elements
                                                       note-grob)
                 (set! (ly:grob-parent ornament-grob Y) note-grob)
                 (set! (ly:grob-property ornament-grob 'font-size)
                       (+ (ly:grob-property ornament-grob 'font-size 0.0)
                          (ly:grob-property note-grob 'font-size 0.0))))))
         (ly:event-property note-event 'articulations)))))))

\layout {
  \context {
    \Voice
    \consists #head-ornamentation-engraver
  }
}

%%%
%%% Event type definition
%%%
#(define-music-type HeadOrnamentationEvent (music-event)
   (description . "Print an ornamentation at a note head side")
   (types . (general-music post-event event head-ornamentation-event)))

bendBefore = #(make-music 'HeadOrnamentationEvent)










% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
% Funciones propias
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]





% Índice con borde circular y un cómodo espacio interior (y no el que viene por defecto)
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
#(define-markup-command (boxindex layout props text) (markup?)
	(interpret-markup layout props
		(markup #:huge #:override '(box-padding . 0.5) #:box text)
	)
)

#(define-markup-command (boxed layout props text) (markup?)
	(interpret-markup layout props
		(markup #:huge #:override '(box-padding . 0.9) #:box text)
	)
)

% Ritardando
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
rit =
#(make-music 'CrescendoEvent
             'span-direction START
             'span-type 'text
             'span-text "rit.")

% Negra oblicua
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
rs = {
  \once \override Rest #'stencil = #ly:percent-repeat-item-interface::beat-slash
  \once \override Rest #'thickness = #0.48
  \once \override Rest #'slope = #1.7
  r4
}










% Añadido de funciones al contexto
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
\layout {
  \context {
    \Score		
    
    % multi-mark-engraver
    \remove "Mark_engraver"
    \consists #multi-mark-engraver
    \consists "Tweak_engraver"
    
    % merge-rests-on-positioning
    \override RestCollision #'positioning-done = #merge-rests-on-positioning
    
    % aspecto de los silencios multicompás
    \override MultiMeasureRest.expand-limit = #1
    
    % margen extra de las articulaciones para evitar la colisión con las ligaduras
    \override Script.padding = #0.5
    \override Script.avoid-slur = #'inside
    
    % glissando bonito
    \override Glissando.style = #'trill
    \override Glissando.bound-details.left.padding = #1.7
    
    % dinámicas un poco mas gruesas
    \override Hairpin.thickness = #1.75
    
    % Esconde la cancelacion de accidentales en el cambio de armadura
    % \override KeyCancellation.break-visibility = #'#(#f #f #f)
  }
}












% LAYOUT
% ------
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~






\header {
  title = "Amado Mio"
  instrument = "Piano"
  composer = "Allan ROBERTS & Doris Fischer"
  arranger = "Arr: Paul Weirick"
  poet = ""
  tagline = ""
}

\paper {
  % El definir otro tipo de fuente no permite cambiar el tamaño global del staff. Es un bug conocigo
  #(define fonts (make-pango-font-tree "LilyJAZZText" "Nimbus Sans" "Luxi Mono" (/ 20 20)))
  
  % distancia entre la parte superior de la página y el título
  top-markup-spacing #'basic-distance = 5
  
  % distancia entre el título y el primer sistema
  markup-system-spacing #'basic-distance = 25
  
  % distancia entre el texto y el primer sistema 
  top-system-spacing #'basic-distance = 15
  
  % márgen inferior
  bottom-margin = 20
    
  % número de páginas
  % page-count = 1
  
  ragged-last-bottom = ##f 
  
  oddHeaderMarkup = \markup {
    \on-the-fly #not-first-page \fill-line {
      \fromproperty #'page:page-number-string
      \fromproperty #'header:title
      \fromproperty #'header:instrument
    }
  }
  
  evenHeaderMarkup = \markup {
    \on-the-fly #not-first-page \fill-line {
      \fromproperty #'header:instrument
      \fromproperty #'header:title
      \fromproperty #'page:page-number-string
    }
  }
}










% MUSIC PARTS
% -----------
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~





% scoreMarkup
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
scoreMarkup = \new Staff \with {
   \RemoveEmptyStaves
}
{
  % Oculta el primer pentagrama del markup
  \override Staff.VerticalAxisGroup #'remove-first = ##t
  
  % Indicación de tiempo
  \once \override Score.RehearsalMark.X-offset = #5
  \mark \markup \normalsize "Rumba Tempo - With Expression"
  s1*4
  
  \bar ".|:-||"
  \mark \markup \boxed "A"
  s1*16
  
  \mark \markup {
    \column { \boxed "B" }
    \hspace #2
    \column { \vspace #-0.4 \jazzglyph #'"scripts.varsegnojazz" }
  }
  s1*16
  
  \mark \markup \boxed "C" 
  s1*18
  
  \once \override Score.RehearsalMark.X-offset = #5
  \mark \markup "(Solos ad. lib)"
  s1*4
  
  \eolMark \markup "last time D.S."
}





% scoreChords
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
scoreChords = \chords {
  
  \set minorChordModifier = \markup {"-"}
  
  s1*54
  
  e1:7
  e1:7
  a:m
  a:m
}





% music
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
vocalGuide = \relative c''' {

  \jazzOn
  \compressFullBarRests
  \voiceOne
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #4
  
  
  \key d \minor
  
  \teeny
  
  %{ f4. ( e8 ~ e4 d
  c4. bes8 ~ bes4 c )
  a1 -^ ~
  a2 ~ a8 a ( c bes
  
  a bes d f ~ f4 d8 bes
  a4 ) a ( d f,%}
  s1*3
  r4 a ( d f,
  
  a4. g8 ~ g2 ~ 
  g4 ) g ( a bes 
  bes4. a8 ~ a2 ~ 
  a4 ) d, ( e f
  
  f4. e8 ~ e2 ~ 
  e4 ) e ( c'? bes
  a1 ~ 
  a4 ) a ( d f,
  
  a4. g8 ~ g2 ~ 
  g4 ) g ( a bes 
  bes4. a8 ~ a2 ~ 
  a4 ) d, ( e f
  
  f4. e8 ~ e2 ~ 
  e4 ) e ( d cis
  d1 ~ 
  d2. ) r4
  
  cis4 ( d e f 
  d2 a' )
  r4 bes ( bes bes
  bes2 a )
  
  r8 g ( a bes -> ) ~ bes a ( g4 )
  r8 f ( g a -> ) ~ a g ( f4 )
  r8 e ( f g -> ) ~ g f ( e4
  d1 )
  
  cis4 ( d e f 
  d2 a' )
  r4 bes ( bes bes
  bes2 a )
  
  r8 g ( a bes -> ) ~ bes a ( g4 )
  r8 f ( g a -> ) ~ a g ( f4 )
  r8 e ( f g -> ) ~ g f ( e4
  d4 -. ) a' ( d f,
  
  a4. g8 ~ g2 ~ 
  g4 ) g ( a bes 
  bes4. a8 ~ a2 ~ 
  a4 ) d, ( e f
  
  f4. e8 ~ e2 ~ 
  e4 ) e ( c'? bes
  a1 ~ 
  a4 ) a ( d f,
  
  a4. g8 ~ g2 ~ 
  g4 ) g ( a bes 
  bes4. a8 ~ a2 ~ 
  a4 ) d, ( e f
  
  bes4. g8 ~ g2 ~ 
  g4 ) a ( cis d
  d4. ) f8 -. e -. d -. c -. bes -.
  a4 -. a ( d f, )
  
  d'4. ( e8 ~ e4 f
  g1 )
  
  %{g2. ) f8 ( e
  f2. ees8 des
  des2 c )
  
  s1*3
  s4 c ( f aes,
  
  c4. bes8 ~ bes2 ~ 
  bes4 ) bes ( c des 
  des4. c8 ~ c2 ~
  c4 ) f, ( g aes
  
  aes4. g8 ~ g2 ~
  g4 ) g ( ees' des
  c1 ~ 
  c4 ) c ( f aes,
  
  c4. bes8 ~ bes2 ~ 
  bes4 ) bes ( c des 
  des4. c8 ~ c2 ~
  c4 ) f, ( g aes
  
  aes4. g8 ~ g2 ~
  g4 ) g ( f e
  f1 ~ 
  f2. ) r4
  
  s1*4
  
  r8 bes ( c des -> ) ~ des c ( bes4 )
  r8 aes ( bes c -> ) ~ c bes ( aes4 )
  r8 g ( aes bes -> ) ~ bes aes ( g4 
  f1 )
  
  s1*4
  
  r8 bes ( c des -> ) ~ des c ( bes4 )
  r8 aes ( bes c -> ) ~ c bes ( aes4 )
  r8 g ( aes bes -> ) ~ bes aes ( g4 
  f4 -. ) d' ( g bes,
  
  d4. c8 ~ c2 ~ 
  c4 ) c ( d ees 
  ees4. d8 ~ d2 ~ 
  d4 ) g, ( a bes 
  
  ees4. c8 ~ c2 ~ 
  c4 ) d ( fis g
  g2. ) d4 (
  c2. ) a4 (
  g4. -. ) bes8 -. c -. d -. e -. fis-. 
  g2. -> r4
  %}
}

rightHand = \relative c' {

  % Para pintar todos los números de compás debajo del pentagrama
  % \override Score.BarNumber #'break-visibility = #end-of-line-invisible
  % \override Score.BarNumber #'direction = #DOWN
  % \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

  \jazzOn
  \compressFullBarRests
  \voiceTwo
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #-4
  \slurUp
  
  \key d \minor
  
  %{ r8 bes ( d g ) r8 <bes, d e g> [ r <bes d e g> ]
  r8 g ( bes d ) r <bes d e> [ r <bes d g> ]
  r <g cis f> <g cis f> <g cis e> r <g cis e> <g cis e> <f c' ees>
  \break
  r \! <fis c' ees> <fis c' ees> <fis c' d> r <fis c' d> [ r <fis c' ees> ]
  r bes ( d f? ) r \! <bes, d f> [ r <bes d f> ]
  <cis g' a>4 -. \! r r2%}
  
  s1*3
  R1
  
  \repeat volta 2 {
    
    \break
    r8 f, ( bes d ) r <g, bes d f> [ r <g bes d f> ]
    r g ( a cis ) r <a cis g'> [ r <bes cis g'> ]
    r bes ( d f ) r <d f a> [ r <d f a> ]
    r a ( d f ) r <bes, cis e g> [ r <a d f> ]
    
    \break
    r bes ( d f ) r <bes, d e> [ r <bes d e> ]
    r g ( a cis ) r <g cis e> [ r <g cis e> ]
    r a ( d f ) r <a, d f a> [ r <a d f a> ]
    <a d f a>4 -. r r2
    
    \break
    r8 f ( bes d ) r <g, bes d f> [ r <g bes d f> ]
    r g ( a cis ) r <a cis g'> [ r <bes cis g'> ]
    r bes ( d f ) r <d f a> [ r <d f a> ]
    r a ( d f ) r <bes, cis e g> [ r <a d f> ]
    
    \break
    r bes ( d f ) r <bes, d e> [ r <bes d e> ]
    r g ( a cis ) r <g bes d> [ r <g a cis> ]
    r a ( d f ) r <bes, d e g> [ r <bes d e g> ]
    r a ( d f ) r <a, d f> [ r <a d f> ]
    
    \break
    r8 <cis e g> [ r <bes d g> ] r <g cis e> [ r <g cis f> ]
    r8 <a d f> [ r <a d f> ] r <a d f> [ r <a d f> ]
    r8 <bes d f> [ r <bes d f> ] r <bes d g> [ r <bes d g> ]
    \break
    r8 <bes d f> [ r <bes d f> ] r <a d f> s4
    
    r8 <cis e g>8 <cis f a> <cis g' bes> -> r <cis f a> [ r <cis e g> ]
    r8 <a d f> <a e' g> <d f a> -> r <a e' g> [ r <a d f> ]
    r8 <bes d e> <bes d f> <a cis g'> -> r <g cis f> [ r <g cis e> ]
    \break
    r8 a ( d f ) r <d f a> [ r <d f a> ]
    
    
    r8 <cis e g> [ r <bes d g> ] r <g cis e> [ r <g cis f> ]
    r8 <a d f> [ r <a d f> ] r <a d f> [ r <a d f> ]
    \break
    r8 <bes d f> [ r <bes d f> ] r <bes d g> [ r <bes d g> ]
    r8 <bes d f> [ r <bes d f> ] r <a d f> s4
    
    r8 <cis e g> <cis f a> <cis g' bes> -> r <cis f a> [ r <cis e g> ]
    \break
    r8 <a d f> <a e' g> <d f a> -> r <a e' g> [ r <a d f> ]
    r8 <bes d e> <bes d f> <a cis g'> -> r <g cis f> [ r <g cis e> ]
    <f a d>4 -. r r2
    
    \break
    r8 f ( bes d ) r <g, bes d f> [ r <g bes d f> ]
    r g ( a cis ) r <a cis g'> [ r <bes cis g'> ]
    r bes ( d f ) r <d f a> [ r <d f a> ]
    r a ( d f ) r <bes, cis e g> [ r <a d f> ]
    
    \break
    r bes ( d f ) r <bes, d e> [ r <bes d e> ]
    r g ( a cis ) r <g cis e> [ r <g cis e> ]
    r a ( d f ) r <a, d f a> [ r <a d f a> ]
    <a d f a>4 -. r r2
    
    \break
    r8 f ( bes d ) r <g, bes d f> [ r <g bes d f> ]
    r g ( a cis ) r <a cis g'> [ r <bes cis g'> ]
    r bes ( d f ) r <d f a> [ r <d f a> ]
    \break
    r a ( d f ) r <bes, cis e g> [ r <a d f> ]
    
    r bes ( d g ) r <bes, d e g> [ r <bes d e g> ]
    r cis ( e g ) r <cis, e g a> [ r <d e g bes> ]
  }
  
  \alternative {
    { \break
      r <d f a> <d f a> <d f a> <d g bes> -. <d g bes> -. <c d g> -. <bes d g> -.
      <a d f a>4 -. r r2 }
    
    { r8 <d f a> <d f a> <d f a> r <d f a> [ r <d f a> ] 
      r8 <d f a> <d f a> <d f a> r <d f a> [ r <d f a> ] }
  }
  
  \repeat volta 2 {
    \break
    s1*4 
  }
  
  %{
  r <d e g bes> <d e g bes> <d e g bes> r <d e g bes> [ r <d e g bes> ] 
  r <des f ces'> <des f ces'> <des f ces'> r <des f ces'> [ r <des f ces'> ] 
  r <des e bes'> <des e bes'> <des e bes'> r \! <c e bes'> [ r <c e bes'> ]
  
  \break
  \stemUp
  r4 \appoggiatura { c'16 aes f } c8 -. <c f aes?> r <c f aes> [ r <c f aes> ]
  r4 \appoggiatura { c'16 g e } c8 -. <bes e g> r <bes e g> [ r <bes e g> ]
  r4 \appoggiatura { c'16 aes f } c8 -. <c f aes?> r <c f aes> [ r <bes des e g> ]
  <a? c f>4 -. r r2
  
  \key f \minor
  
  \break
  \stemDown
  r4 \appoggiatura { c'16 aes f } des8 -. <des f aes> r <des f aes> [ r <des f aes> ]
  r4 \appoggiatura { c'16 g e } c8 -. <c e bes'> r <c e bes'> [ r <d e bes'> ]
  r4 \appoggiatura { des'16 aes f } des8 -. <c f aes> r <c f aes> [ r <c f aes> ]
  \break
  r4 \appoggiatura { c'16 aes f } c8 -. <c f aes> r <aes c g'> [ r <aes c f> ]
  
  r4 \appoggiatura { des'16 aes f } des8 -. <bes des f g> r <bes des f g> [ r <bes des f g> ]
  r4 \appoggiatura { c'16 g e } c8 -. <bes e? g> r <bes e g> [ r <bes e g> ]
  \break
  r4 \appoggiatura { c'16 aes f } c8 -. <c f aes> r <aes c g'> [ r <aes c f> ]
  <aes c f>4 -. r r2
  
  r4 \appoggiatura { c'16 aes f } des8 -. <des f aes> r <des f aes> [ r <des f aes> ]
  r4 \appoggiatura { c'16 g e } c8 -. <c e bes'> r <c e bes'> [ r <d e bes'> ]
  \break
  r4 \appoggiatura { des'16 aes f } des8 -. <c f aes> r <c f aes> [ r <c f aes> ]
  r4 \appoggiatura { des'16 aes f } c8 -. <c f aes> r <aes c g'> [ r <aes c f> ]
  
  r4 \appoggiatura { des'16 aes f } des8 -. <bes des f g> r <bes des f g> [ r <bes des f g> ]
  \break
  r4 \appoggiatura { c'16 g e } c8 -. <bes e? g> r <bes des? f> [ r <bes c e> ]
  r4 \appoggiatura { c'16 aes f } c8 -. <c f aes> r <des f g bes> [ r <des f g bes> ]
  r4 \appoggiatura { c'16 aes f } c8 -. <c f aes> r <c f aes> [ r <c f aes> ]
  
  \break
  \ottava #+1
  <e' e'>4 ( <f f'> <g g'> <aes aes'> 
  <f f'>2 <c' c'> )
  r4 <des des'> ( <des des'> <des des'>
  <des des'>2 <c c'> )
  
  \break
  \ottava #0
  r8 <e,, g bes> <e aes c> <e bes' des> -> r <e aes c> [ r <e g bes> ]
  r8 <c f aes> <c g' bes> <f aes c> -> r <c g' bes> [ r <c f aes> ]
  r8 <des f g> <des f aes> <c e bes'> -> r <bes e aes> [ r <bes e g> ]
  r8 c ( f aes ) r <c, f aes c> [ r <c f aes c> ]
  
  \break
  \ottava #+1
  <e' e'>4 ( <f f'> <g g'> <aes aes'> 
  <f f'>2 <c' c'> )
  r4 <des des'> ( <f f'> <des des'>
  <des des'>2 <c c'> )
  
  \break
  \ottava #0
  r8 <e,, g bes> <e aes c> <e bes' des> -> r <e aes c> [ r <e g bes> ]
  r8 <c f aes> <c g' bes> <f aes c> -> r <c g' bes> [ r <c f aes> ]
  r8 <des f g> <des f aes> <c e bes'> -> r <bes e aes> [ r <bes e g> ]
  <aes c f>4 -. r r2
  
  \key g \minor
  
  \break
  r8 <bes ees g> <bes ees g> <bes ees g> r <bes ees g> [ r <bes ees g> ]
  r8 <c d fis> <c d fis> <c d fis> r <c d fis> [ r <c ees fis> ]
  r8 <bes ees g> <bes ees g> <bes d g> r <bes d g> [ r <bes d g> ]
  \break
  r8 <bes d g> <bes d g> <bes d g> r <c ees fis a> [ r <d e g bes> ]
  
  r8 <ees? g bes> <ees g bes> <ees g a> r <c ees g a> [ r <c ees g a> ]
  r8 <c fis a> <c fis a> <c fis a> r <c fis a> [ r <c ees g> ]
  \break
  r8  <bes d g> <bes d g> <bes d g> r <bes d g> [ r <bes d g> ]
  r8 <a c ees g> <a c ees g> <a c ees g> r <a c ees g> [ r <c ees fis> ]
  
  r8 \! <bes d g> <bes d g> <d g bes> <c g' a c> -. <f aes b d> -. <g a c ees> -. <fis c' ees fis> -.
  <b d e g>4 s2 s4
  %}
}

leftHand = \relative c, {
  
  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0	
  
  \clef bass
  \key d \minor
  
  %{<g g'>4 r <d' d'> d
  e r g e
  <a, a'> -^ r <a a'> a
  d r d d 
  
  <g, g'> r <g g'> r
  <a a'> r4 r2%}
  s1*3
  R1 \fermataMarkup
  
  <g g'>4 r <bes bes'> <g g'>
  <a a'> r <e' e'> <a, a'>
  <d d'> r <a' a'> <a, a'>
  <d d'> r <a a'> d
  
  <g, g'> r <bes bes'> <g g'>
  <a a'> r <a a'> r
  <d d'> r <d d'> <a a'>
  d -. r r2
  
  <g, g'>4 r <bes bes'> <g g'>
  <a a'> r <e' e'> <a, a'>
  <d d'> r <a' a'> <a, a'>
  <d d'> r <a a'> d
  
  <g, g'>4 r <bes bes'> <g g'>
  <a a'> r <e' e'> <a, a'>
  <d d'> r g g
  d r <a a'> <d d'>
  
  <a a'> r <a a'> r
  d r d d 
  <g, g'> r e' e
  d r d 
  << 
    { \change Staff = "rightHand" \ottava #+1 \stemDown a'''''4 -> \glissando \ottava #0
      \change Staff = "leftHand" \stemNeutral a,,,,4 -> } \\
     { \stemUp d, s }
  >>
  
  \stemNeutral r a' a,
  d r a' a,
  <g g'> r <a a'> r
  <d d'> r <d d'> r
  
  <a a'> r <a a'> r
  d r d d
  <g, g'> r e' e
  d r d 
  << 
    { \change Staff = "rightHand" \ottava #+1 \stemDown a'''''4 -> \glissando \ottava #0
      \change Staff = "leftHand" \stemNeutral a,,,,4 -> } \\
     { \stemUp d, s }
  >>
  
  \stemNeutral r a' a,
  d r a' a,
  <g g'> r <a a'> r
  <d d'> -. r4 r2
  
  <g, g'>4 r <bes bes'> <g g'>
  <a a'> r <e' e'> <a, a'>
  <d d'> r <a' a'> <a, a'>
  <d d'> r <a a'> d
  
  <g, g'> r <bes bes'> <g g'>
  <a a'> r <a a'> r
  <d d'> r <d d'> <a a'>
  d4 -. r4 r2
  
  <g, g'>4 r <bes bes'> <g g'>
  <a a'> r <e' e'> <a, a'>
  <d d'> r <a' a'> <a, a'>
  <d d'> r <a a'> d
  
  <g, g'> r <g g'> <bes bes'>
  <a a'> r <a a'> r
  <d d'> r <g g'>8 -. <g g'>8 -. <e e'>8 -. <e e'>8 -.
  <d d'>4 -. r r2
  
  <d d'>4 r <a a'> d
  <d d'>4 _"(Fine)" r <a a'> d
  
  s1*4
  %{
  <g, g'> r d' <g, g'>
  <aes aes'> r des <aes aes'>
  <g g'> r c? c
  
  <f, f'> r <f f'> c' 
  <c c'> r <c c'> c
  <f, f'> r <f f'> c'
  <f, f'> -. r4 r2
  
  \key f \minor
  
  <bes bes'>4 r <des des'> <bes bes'>
  <c c'> r <g' g'> <c, c'>
  <f f'> r <c c'> c
  f r <c c'> <f, f'>
  
  <bes bes'> r <des des'> <bes bes'>
  <c c'> r c c
  f r <c c'> c
  f -. r r2
  
  <bes, bes'>4 r <des des'> <bes bes'>
  <c c'> r <g' g'> <c, c'>
  <f f'> r <c c'> c
  f r <c c'> <f, f'>
  
  <bes bes'> r <des des'> <bes bes'>
  <c c'> r g' c,
  f r <bes, bes'> r
  f' -. r c f
  
  e' ( f g aes 
  f2 c' )
  r4 des ( des des
  des2 c )
  
  <c,, c'>4 r c' c, 
  f r c' c, 
  bes r c c
  f r f f
  
  e' ( f g aes 
  f2 c' )
  r4 des ( f des
  des2 c )
  
  <c,, c'>4 r c' c, 
  f r c' c, 
  bes r c c
  f -. r r2
  
  \key g \minor
  
  <c c'>4 r <ees ees'> <c c'>
  <d d'> r <a' a'> <d, d'>
  g r d' d,
  g r d' g,
  
  <c, c'> r <c c'> <ees ees'>
  <d d'> r <d d'> r
  <g, g'> r <g g'> r
  c r <c c'> <d d'>
  <g g'>8 -. <fis fis'> -. <f f'> -. <e e'> -. <ees ees'> -. <d d'> -. <c c'> -. <aes aes'> -.
  <g g'>4 -> 
  <<
    { d'''16 ( c bes a g4 -. ) r } \\
    { d16 ( c bes a g4 -. ) r }
  >>
  %}
}

dynamics = {
  
  \jazzOn
  
  s1 \ff
  s8 s4. \< s2
  s1 \fz \>
  s4 s4 \! s2
  
  s1 -\markup { \dynamic "f" "-" \dynamic "mf" }
  s1*3
  
  s1*12
  
  s1 \p
  s1
  s4 s4 \< s2
  s2 \> s4 \! s4 \ff
  
  s1*4
  
  s1 \p
  s1
  s4 s4 \< s2
  s2 \> s4 \! s4 \ff
  
  s1*4
  
  s1 -\markup { \dynamic "f" "-" \dynamic "mf" }
  s1*3
  
  s1*8
  
  s1
  s8 s4. \< s2
  s1 \ff
  s1
  
  s8 \mf s4. \< s2
  s1 \ff
  
  s1*4
  
  %{s4 s4 \> s2
  s2 s2 \!
  
  s1*4
  
  s1 \p
  s1*3
  
  s1*12
  
  s1 \f
  s1*3
  
  s1 \ff
  s1*3
  
  s1 \f
  s1*3
  
  s1 \ff
  s1*3
  
  s1*4
  
  s1
  s4 s4 \< s2
  s4 \fff \> s2.
  s1
  
  s8 \! s4. \< s2
  s1 \fff%}
}



% score
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
\score {
  <<
    \scoreMarkup
    \scoreChords
    \new PianoStaff <<
      \new Staff = "rightHand" <<
        \new Voice \transpose d a \vocalGuide
        \new Voice \transpose d a \rightHand
      >>
      \new Dynamics { \dynamics }
      \new Staff = "leftHand" { \transpose d a \leftHand }
    >>
  >>
}


