\version "2.18.2"

\include "lily-jazz/ignatzek-jazz-chords.ily"
\include "lily-jazz/lily-jazz.ily"

% PLUGINS
% - \eolMark: 		mark al final de línea
% - \bendBefore: 	solución al bendBefore (i.e. dis8 \bendBefore)
% - \rit: 			crea una porción de ritardando que debe terminar como las dinámicas, con un \!
% - \rs: 			negra de improvisación
% - \emptyStave:	Imprime un pentagrama vacío

% LAYOUT
% - El aspecto de los silencios multicompás es como los de las partituras de jazz
% - Margen extra de las articulaciones para evitar la colisión con las ligaduras
% - Glissandos bonitos

% PAPER
% - Se imprime el título, instrumento y número de página en todas las páginas
% - Solo se debe tocar el page-count (si eso). Sale bonito tal cual.

% CAMBIOS
% 03-09-14: Layout
% Ahora las articulaciones van por dentro de las ligaduras. Parece que es la mejor solución
%
% 04-09-14: Layout
% Los creciendos y decreciendos tienen la línea un poco más gruesa
%
% Markup
% Añadidas guías para correcta posición y markup de la indicación de tempo
%
% Plugins
% Añadida función \emptyStave para imprimir un pentagrama vacío.
% En algunas partituras hay un pentagrama vacío antes del coda (lo cual mola bastante, por cierto)
%
% 05-09-14: Plugins
% Añadido a la función \emptyStave el esconder la armadura de la clave, que no estaba.
%
% 08-09-14: Layout
% Ahora no se muestra la cancelación de accidentales al haber un cambio de armadura
%
% 09-09-14: Chords
% Añadida sentencia para que los acordes menores salgan con un guión en vez de con una M minúscula
%
% 10-09-14: Plugins
% Mejorada la función \bendAfter.
% Ahora es genérica, y se puede definir cualquier texto en un lateral que haga las funciones de ornamento
%
% 15-09-14: Layout
% Añadido un poco más de margen a los reguladores
%
% 16-09-14: Plugins
% Mejorada la función para imprimir barras de improvisación.
% Antes se llamaba al comando \rs para pintar una barra que ocupaba una negra
% Se reemplaza \rs por \restImproOn y \restImproOff, siendo esta opción mucho más flexible.
% Cuando se activa \restImproOn, todo silencio se convierte en barra de improvisación, respetando su duración.
%
% 20-09-14: Plugins
% Mejorada la función para imprimir barras de improvisación.
% Se reemplaza \restImproOn y \restImproOff por \improOn y \improOff
% Ahora, también las notas del pentagrama salen como barra de improvisación.
% (los comandos \improvisationOn e \improvisationOff de lilypond no se llevan bien con LilyJAZZ)
% Se ha añadido un grob correspondiente a los slash de la fuente LilyJAZZ en el archivo lily-jazz.ly
%
% 20-09-14: Layout
% Comentada la línea que oculta la cancelación de accidentales al haber cambio de armadura.
% Realmente, la cancelación es muy bienvenida.


% PLUGINS 1.2
% -----------
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~





% multi-mark-engraver
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
% Aporta la posibilidad de pintar un rehearsal mark al final de una línea y 
% al comienzo de la siguiente.
% http://lists.gnu.org/archive/html/lilypond-user/2011-08/msg00157.html
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
#(define (multi-mark-engraver ctx)
   (let ((texts '())
         (final-texts '())
         (events '()))

     `((start-translation-timestep
        . ,(lambda (trans)
             (set! final-texts '())))

       (listeners
        (mark-event
         . ,(lambda (trans ev)
              (set! events (cons ev events)))))

       (acknowledgers
        (break-alignment-interface
         . ,(lambda (trans grob source)
              (for-each (lambda (mark)
                          (set! (ly:grob-parent mark X) grob))
                        texts))))

       (process-music
        . ,(lambda (trans)
             (for-each
              (lambda (ev)
                (let* ((mark-grob
                        (ly:engraver-make-grob trans 'RehearsalMark ev))
                       (label (ly:event-property ev 'label))
                       (formatter (ly:context-property ctx 'markFormatter)))

                  (if (and (procedure? formatter)
                           (not (markup? label)))
                      (begin
                       (if (not (number? label))
                           (set! label
                                 (ly:context-property ctx 'rehearsalMark)))

                       (if (and (integer? label)
                                (exact? label))
                           (set! (ly:context-property ctx 'rehearsalMark)
                                 (1+ label)))

                       (if (number? label)
                           (set! label (apply formatter (list label ctx)))
                           (ly:warning "rehearsalMark must have
integer value"))))

                  (if (markup? label)
                      (begin
                       (set! (ly:grob-property mark-grob 'text) label)
                       (let ((dir (ly:event-property ev 'direction)))
                         (and (ly:dir? dir)
                              (set! (ly:grob-property mark-grob 'direction)
                                    dir))))
                      (ly:warning "mark label must be a markup object"))

                  (set! texts (cons mark-grob texts))))
              (reverse events))))

       (stop-translation-timestep
        . ,(lambda (trans)
             (if (pair? texts)
                 (let ((staves (ly:context-property ctx 'stavesFound))
                       (priority-index 0))
                   (for-each (lambda (grob)
                               (let ((my-priority (ly:grob-property
grob 'outside-staff-priority 1500)))
                                 (for-each (lambda (stave)

(ly:pointer-group-interface::add-grob grob 'side-support-elements
                                               stave))
                                           staves)
                                 (set! (ly:grob-property grob
'outside-staff-priority) (+ my-priority priority-index))
                                 (set! priority-index (1+ priority-index))
                                 (set! final-texts (cons grob final-texts))))
                             (reverse texts))
                     (set! texts '())
                     (set! events '())))))

        (finalize
         . ,(lambda (trans)
              (and (pair? final-texts)
                   (for-each (lambda (grob)
                               (set! (ly:grob-property grob 'break-visibility)
                                     end-of-line-visible))
                             final-texts)))))))


% Funciones originales
%{
myMark =
#(define-music-function (parser location text) (markup?)
  (make-music 'MarkEvent
              'label text))
			  
toCoda = {
	\tweak #'self-alignment-X #RIGHT
	\tweak #'break-visibility #begin-of-line-invisible
	\myMark \markup { \hspace #1.25 \raise #1.25 \musicglyph #"scripts.coda" }
}
%}

% Función maqueada.
% Se apoya en el código obtenido por consola al ejecutar { \displayMusic { \toCoda }}
eolMark =
#(define-music-function (parser location text) (markup?)
	(make-music 
		'MarkEvent 
		'tweaks (list 
			(cons (quote self-alignment-X) 1)
			(cons (quote break-visibility) #(#t #t #f))
		) 
		'label (markup
			#:line (
				#:hspace 1.25
				#:raise 1.25
				text
			)
		)
	)
)










% rest-score y merge-rests-on-positioning
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
% Permite realizar la fusión de los silencios entre dos voces en un mismo pentagrama.
% https://code.google.com/p/lilypond/issues/detail?id=1228 (fichero merge-rests.ily)
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]

#(define (rest-score r)
  (let ((score 0)
	(yoff (ly:grob-property-data r 'Y-offset))
	(sp (ly:grob-property-data r 'staff-position)))
    (if (number? yoff)
	(set! score (+ score 2))
	(if (eq? yoff 'calculation-in-progress)
	    (set! score (- score 3))))
    (and (number? sp)
	 (<= 0 2 sp)
	 (set! score (+ score 2))
	 (set! score (- score (abs (- 1 sp)))))
    score))

#(define (merge-rests-on-positioning grob)
  (let* ((can-merge #f)
	 (elts (ly:grob-object grob 'elements))
	 (num-elts (and (ly:grob-array? elts)
			(ly:grob-array-length elts)))
	 (two-voice? (= num-elts 2)))
    (if two-voice?
	(let* ((v1-grob (ly:grob-array-ref elts 0))
	       (v2-grob (ly:grob-array-ref elts 1))
	       (v1-rest (ly:grob-object v1-grob 'rest))
	       (v2-rest (ly:grob-object v2-grob 'rest)))
	  (and
	   (ly:grob? v1-rest)
	   (ly:grob? v2-rest)
	   (let* ((v1-duration-log (ly:grob-property v1-rest 'duration-log))
		  (v2-duration-log (ly:grob-property v2-rest 'duration-log))
		  (v1-dot (ly:grob-object v1-rest 'dot))
		  (v2-dot (ly:grob-object v2-rest 'dot))
		  (v1-dot-count (and (ly:grob? v1-dot)
				     (ly:grob-property v1-dot 'dot-count -1)))
		  (v2-dot-count (and (ly:grob? v2-dot)
				     (ly:grob-property v2-dot 'dot-count -1))))
	     (set! can-merge
		   (and
		    (number? v1-duration-log)
		    (number? v2-duration-log)
		    (= v1-duration-log v2-duration-log)
		    (eq? v1-dot-count v2-dot-count)))
	     (if can-merge
		 ;; keep the rest that looks best:
		 (let* ((keep-v1? (>= (rest-score v1-rest)
				      (rest-score v2-rest)))
			(rest-to-keep (if keep-v1? v1-rest v2-rest))
			(dot-to-kill (if keep-v1? v2-dot v1-dot)))
		   ;; uncomment if you're curious of which rest was chosen:
		   ;;(ly:grob-set-property! v1-rest 'color green)
		   ;;(ly:grob-set-property! v2-rest 'color blue)
		   (ly:grob-suicide! (if keep-v1? v2-rest v1-rest))
		   (if (ly:grob? dot-to-kill)
		       (ly:grob-suicide! dot-to-kill))
		   (ly:grob-set-property! rest-to-keep 'direction 0)
		   (ly:rest::y-offset-callback rest-to-keep)))))))
    (if can-merge
	#t
	(ly:rest-collision::calc-positioning-done grob))))










% startParenthesis y endParenthesis
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
% http://comments.gmane.org/gmane.comp.gnu.lilypond.general/64087
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]

% ELIMINADO










% bendBefore
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
% Adaptación
% http://lilypondblog.org/2013/08/adding-ornamentations-to-note-heads-part-1/
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]

%%%
%%% Utilities for defining new grobs, grob properties and music event types
%%% (there should be built-in commands to do that in LilyPond)
%%%
#(define (define-grob-type grob-name grob-entry)
   "Define a new grob and add it to `all-grob-definitions', after
scm/define-grobs.scm fashion.
After grob definitions are added, use:

\\layout {
  \\context {
    \\Global
    \\grobdescriptions #all-grob-descriptions
  }
}

to register them."
   (let* ((meta-entry   (assoc-get 'meta grob-entry))
          (class        (assoc-get 'class meta-entry))
          (ifaces-entry (assoc-get 'interfaces meta-entry)))
     (set-object-property! grob-name 'translation-type? list?)
     (set-object-property! grob-name 'is-grob? #t)
     (set! ifaces-entry (append (case class
                                  ((Item) '(item-interface))
                                  ((Spanner) '(spanner-interface))
                                  ((Paper_column) '((item-interface
                                                     paper-column-interface)))
                                  ((System) '((system-interface
                                               spanner-interface)))
                                  (else '(unknown-interface)))
                                ifaces-entry))
     (set! ifaces-entry (uniq-list (sort ifaces-entry symbol<?)))
     (set! ifaces-entry (cons 'grob-interface ifaces-entry))
     (set! meta-entry (assoc-set! meta-entry 'name grob-name))
     (set! meta-entry (assoc-set! meta-entry 'interfaces
                                  ifaces-entry))
     (set! grob-entry (assoc-set! grob-entry 'meta meta-entry))
     (set! all-grob-descriptions
           (cons (cons grob-name grob-entry)
                 all-grob-descriptions))))

#(define-public (define-grob-property symbol type? description)
   "Define a new grob property.
`symbol': the property name
`type?': the type predicate for this property
`description': the type documentation"
  (set-object-property! symbol 'backend-type? type?)
  (set-object-property! symbol 'backend-doc description)
  symbol)

#(defmacro*-public define-music-type (type-name parents #:rest properties)
     "Add a new music type description to `music-descriptions'
and `music-name-to-property-table'.

`type-name': the music type name (a symbol)
`parents': the parent event classes (a list of symbols)
`properties': a (key . value) property set, which shall contain at least
   description and type properties.
"
   (let ((gproperties (gensym "properties")))
     `(let ((,gproperties (list-copy ',properties)))
        ,@(map (lambda (parent)
                `(define-event-class ',(ly:camel-case->lisp-identifier type-name)
                   ',parent))
               parents)
        (set-object-property! ',type-name
                              'music-description
                              (cdr (assq 'description ,gproperties)))
        (set! ,gproperties (assoc-set! ,gproperties 'name ',type-name))
        (set! ,gproperties (assq-remove! ,gproperties 'description))
        (hashq-set! music-name-to-property-table ',type-name ,gproperties)
        (set! music-descriptions
              (cons (cons ',type-name ,gproperties)
                    music-descriptions)))))

%%%
%%% Grob definition
%%%
#(define (head-ornamentation::print me)
   "Prints a HeadOrnamentation grob, on the left or right side of the
note head, depending on the grob direction."
   (let* ((notes (ly:grob-object me 'elements))
          (y-ref (ly:grob-common-refpoint-of-array me notes Y))
          (x-ref (ly:grob-common-refpoint-of-array me notes X))
          (x-ext (ly:relative-group-extent notes x-ref X))
          (y-ext (ly:relative-group-extent notes y-ref Y))
          (staff-position (ly:grob-staff-position (ly:grob-array-ref notes 0)))
          (y-coord (+ (interval-center y-ext)
                      (if (and (eq? (ly:grob-property me 'shift-when-on-line) #t)
                               (memq staff-position '(-4 -2 0 2 4)))
                          0.5
                          0)))
          (text (ly:text-interface::print me))
          (width (/ (interval-length (ly:stencil-extent text X)) 2.0))
          (x-coord (if (= (ly:grob-property me 'direction) LEFT)
                       (- (car x-ext) width)
                       (+ (cdr x-ext) width))))
     (ly:stencil-translate
      text
      (cons
       (- (- x-coord (ly:grob-relative-coordinate me x-ref X)) 
          (ly:grob-property me 'x-position))
       (- (- y-coord (ly:grob-relative-coordinate me y-ref Y)) 
          (ly:grob-property me 'y-position))
       ))))

%% a new grob property (used to shift an ornamentation when the
%% note head is on a staff line)
#(define-grob-property 'shift-when-on-line boolean?
   "If true, then the ornamentation is vertically shifted when
the note head is on a staff line.")

#(define-grob-property 'x-position number?
   "X position")

#(define-grob-property 'y-position number?
   "Y position")

#(define-grob-type 'HeadOrnamentation
  `((font-size . 0)
    (shift-when-on-line . #f)
    (x-position . 1)
    (y-position . 1)
    (stencil . ,head-ornamentation::print)
    (meta . ((class . Item)
             (interfaces . (font-interface))))))

\layout {
  \context {
    \Global
    \grobdescriptions #all-grob-descriptions
  }
}

%%%
%%% Engraver
%%%
%% The head-ornamentation engraver, with its note-head acknowledger
%% (which add HeadOrnamentation grobs to note heads)
#(define head-ornamentation-engraver
   (make-engraver
    (acknowledgers
     ((note-head-interface engraver note-grob source)
      ;; helper function to create HeadOrnamentation grobs
      (define (make-ornament-grob text direction shift-when-on-line x-position y-position)
        (let ((ornament-grob (ly:engraver-make-grob engraver
                                                    'HeadOrnamentation
                                                    note-grob)))
          ;; use the ornament event text as the grob text property
          (set! (ly:grob-property ornament-grob 'text) text)
          ;; set the grob direction (either LEFT or RIGHT)
          (set! (ly:grob-property ornament-grob 'direction) direction)
          ;; set the shift-when-on-line property using the given value
          (set! (ly:grob-property ornament-grob 'shift-when-on-line)
                shift-when-on-line)
          ;; set the grob x-pos
          (set! (ly:grob-property ornament-grob 'x-position) x-position)
          ;; set the grob y-pos
          (set! (ly:grob-property ornament-grob 'y-position) y-position)
          (ly:pointer-group-interface::add-grob ornament-grob
                                                'elements
                                                note-grob)
          
          ;; the ornamentation is vertically aligned with the note head
          (set! (ly:grob-parent ornament-grob Y) note-grob)
          ;; compute its font size
          (set! (ly:grob-property ornament-grob 'font-size)
                (+ (ly:grob-property ornament-grob 'font-size 0.0)
                   (ly:grob-property note-grob 'font-size 0.0)))
          ;; move accidentals and dots to avoid collision
          (let* ((ornament-stencil (ly:text-interface::print ornament-grob))
                 (ornament-width (interval-length
                                  (ly:stencil-extent ornament-stencil X)))
                 (note-column (ly:grob-object note-grob 'axis-group-parent-X))
                 ;; accidentals attached to the note:
                 (accidentals (and (ly:grob? note-column)
                                   (ly:note-column-accidentals note-column)))
                 ;; dots attached to the note:
                 (dot-column (and (ly:grob? note-column)
                                  (ly:note-column-dot-column note-column))))
            (cond ((and (= direction LEFT) (ly:grob? accidentals))
                   ;; if the ornament is on the left side, and there are
                   ;; accidentals, then increase padding between note
                   ;; and accidentals to make room for the ornament
                   (set! (ly:grob-property accidentals 'padding)
                         ornament-width))
                  ((and (= direction RIGHT) (ly:grob? dot-column))
                   ;; if the ornament is on the right side, and there
                   ;; are dots, then translate the dots to make room for
                   ;; the ornament
                   (set! (ly:grob-property dot-column 'positioning-done)
                         (lambda (grob)
                           (ly:dot-column::calc-positioning-done grob)
                           (ly:grob-translate-axis! grob ornament-width X))))))
          ornament-grob))
      ;; When the note-head event attached to the note-head grob has
      ;; ornamentation events among its articulations, then create a
      ;; HeadOrnamentation grob
      (for-each
       (lambda (articulation)
         (if (memq 'head-ornamentation-event
                   (ly:event-property articulation 'class))
             ;; this articulation is an ornamentation => make the grob
             ;; (either on LEFT or RIGHT direction)
             (begin
               (if (markup? (ly:event-property articulation 'left-text))
                   (make-ornament-grob
                    (ly:event-property articulation 'left-text)
                    LEFT
                    (ly:event-property articulation 'shift-when-on-line)
                    (ly:event-property articulation 'x-position)
                    (ly:event-property articulation 'y-position)))
               (if (markup? (ly:event-property articulation 'right-text))
                   (make-ornament-grob
                   (ly:event-property articulation 'right-text)
                   RIGHT
                   (ly:event-property articulation 'shift-when-on-line)
                   (ly:event-property articulation 'x-position)
                   (ly:event-property articulation 'y-position))))))
         (ly:event-property (ly:grob-property note-grob 'cause) 'articulations))))))

\layout {
  \context {
    \Score
    \consists #head-ornamentation-engraver
  }
}

%%%
%%% Event type definition
%%%
#(define-music-type HeadOrnamentationEvent (music-event)
   (description . "Print an ornamentation at a note head side")
   (types . (general-music post-event event head-ornamentation-event)))

bendBefore =
#(make-music 'HeadOrnamentationEvent
  'x-position 1
  'y-position 2
  'shift-when-on-line #f
  'left-text #{ \markup { \fontsize #0 \rotate #5 \musicglyph #"brackettips.up"} #})










% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
% Funciones propias
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]





% Índice con borde circular y un cómodo espacio interior (y no el que viene por defecto)
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
#(define-markup-command (boxindex layout props text) (markup?)
	(interpret-markup layout props
		(markup #:huge #:override '(box-padding . 0.5) #:box text)
	)
)

#(define-markup-command (boxed layout props text) (markup?)
	(interpret-markup layout props
		(markup #:huge #:override '(box-padding . 0.9) #:box text)
	)
)

% Ritardando
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
rit =
#(make-music 'CrescendoEvent
             'span-direction START
             'span-type 'text
             'span-text "rit.")

% Barras de improvisacion
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
improOn = {
  \override Rest.stencil = #ly:text-interface::print
  \override Rest.text = \markup \jazzglyph #"noteheads.s2slashjazz"
  \override NoteHead.stencil = #jazz-slashednotehead
}

improOff = {
  \revert Rest.stencil
  \revert Rest.text
  \revert NoteHead.stencil
}

% Pentagrama vacío
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
emptyStave = {
  \once \override Staff.KeySignature.stencil = ##f
  \once \override Staff.Clef.stencil = ##f
  \break
  s1
  \once \override Staff.BarLine.stencil = ##f
  \break
}










% Añadido de funciones al contexto
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
\layout {
  \context {
    \Score		
    
    % multi-mark-engraver
    \remove "Mark_engraver"
    \consists #multi-mark-engraver
    \consists "Tweak_engraver"
    
    % merge-rests-on-positioning
    \override RestCollision #'positioning-done = #merge-rests-on-positioning
    
    % aspecto de los silencios multicompás
    \override MultiMeasureRest.expand-limit = #1
    
    % margen extra de las articulaciones para evitar la colisión con las ligaduras
    \override Script.padding = #0.5
    \override Script.avoid-slur = #'inside
    
    % glissando bonito
    \override Glissando.style = #'trill
    \override Glissando.bound-details.left.padding = #1.7
    
    % dinámicas un poco mas gruesas
    \override Hairpin.thickness = #1.75
    \override Hairpin.bound-padding = #2
    
    % Esconde la cancelacion de accidentales en el cambio de armadura
    % \override KeyCancellation.break-visibility = #'#(#f #f #f)
  }
}












% LAYOUT
% ------
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~






\header {
  title = "Sway"
  subtitle = "(Michael Buble Version)"
  instrument = "Baritone Sax"
  composer = "Pablo BELTRÁN"
  arranger = ""
  poet = \markup \tiny "Reducción y edición: Gabriel PALACIOS"
  meter = \markup \tiny "gabriel.ps.ms@gmail.com"
  tagline = \markup {
    \center-column {
      \line { \tiny "Reducción y edición: Gabriel PALACIOS" }
      \line { \tiny "gabriel.ps.ms@gmail.com" }
    }
  }
}

\paper {
  % El definir otro tipo de fuente no permite cambiar el tamaño global del staff. Es un bug conocigo
  #(define fonts (make-pango-font-tree "LilyJAZZText" "Nimbus Sans" "Luxi Mono" (/ 20 20)))
  
  % distancia entre la parte superior de la página y el título
  top-markup-spacing #'basic-distance = 5
  
  % distancia entre el título y el primer sistema
  markup-system-spacing #'basic-distance = 25
  
  % distancia entre el texto y el primer sistema 
  top-system-spacing #'basic-distance = 15
  
  % márgen inferior
  bottom-margin = 20
    
  % número de páginas
  page-count = 3
  
  ragged-last-bottom = ##f 
  
  oddHeaderMarkup = \markup {
    \on-the-fly #not-first-page \fill-line {
      \fromproperty #'page:page-number-string
      \fromproperty #'header:title
      \fromproperty #'header:instrument
    }
  }
  
  evenHeaderMarkup = \markup {
    \on-the-fly #not-first-page \fill-line {
      \fromproperty #'header:instrument
      \fromproperty #'header:title
      \fromproperty #'page:page-number-string
    }
  }
}










% MUSIC PARTS
% -----------
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~





% scoreMarkup
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
scoreMarkup = \new Staff \with {
   \RemoveEmptyStaves
}
{
  % Oculta el primer pentagrama del markup
  \override Staff.VerticalAxisGroup #'remove-first = ##t
  
  % Indicación de tiempo
  \once \override Score.RehearsalMark.X-offset = #5
  \mark \markup \normalsize "(Count 1, 2, 3, 4 into bar 1)"
  \mark \jazzTempoMarkup "Latin" c4 "127"
  
  \partial 4.
  s4.
  
  s1*8
  
  \bar "||"
  \mark \markup \boxed "V1"
  s1*8
  
  \bar "||"
  \mark \markup \boxed "V2"
  s1*8
  
  \bar "||"
  \mark \markup \boxed "Bridge"
  s1*8
  
  \bar "||"
  \mark \markup \boxed "V3"
  s1*8
  
  \bar "||"
  \mark \markup \boxed "Bass Break"
  s1*8
  
  \bar "||"
  \mark \markup \boxed "Bridge"
  s1*8
  
  \bar "||"
  \mark \markup \boxed "V4 - Violins"
  s1*8
  
  \bar "||"
  \mark \markup \boxed "V5"
  s1*9
  
  \bar "||"
  \mark \markup \boxed "V6 - KEY CH"
  s1*8
  
  \bar "||"
  s1*8
  
  \bar "||"
  s1*8
  
  \bar "|."
}





% scoreChords
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
scoreChords = \chords {
    
  \set minorChordModifier = \markup { "-" }
  
  % c1:m f2:m g2:m7
}





% music
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
baritoneSax = \relative c'' {

  % Para pintar todos los números de compás debajo del pentagrama
  % \override Score.BarNumber #'break-visibility = #end-of-line-invisible
  % \override Score.BarNumber #'direction = #DOWN
  % \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0	
  \set Score.explicitKeySignatureVisibility = #begin-of-line-visible
  
  \set Score.tempoHideNote = ##t
  \tempo 4 = 110
  
  % 00: tacet, Trumpet I, Bone I&II, Baritone Sax
  \relative c' {
  
    \key b \minor
    
    \partial 4.
    r8 \f r b ( --
    
    cis4 ) -. r4 r4 r8 b ( --
    cis4 ) -. r4 r2
    r2 r8 b'8 -. b16 -. b8 -. b16-.
    r4 r8 \ff b, -. -^ r8 fis''8 -. \f e [ -. fis ] -.
    
    \break
    g4. e8 -. r4 r8 b, ( --
    cis4. ) -- fis8 -. r2
    b,4 -. r8 b -. r b -. b16 -. b8. -- 
    b4 -. -^ r4 r2
    
    \break
    R1*7
    r2 r4 r8 b ( --
  }
  
  % V2: tacet, saxs
  \relative c' {
  
    \key b \minor
  
    \break
    cis4 ) -. r4 r4 r8 b ( --
    cis4 ) -. r4 r4 r8 ais ( --
    b4 ) -. r4 r4 r8 ais ( --
    b4 ) -. r4 r8 cis -. r8 b ( --
  
    \break
    cis4 ) -. r4 r4 r8 b ( --
    cis4 ) -. r8 fis -- fis4 -. fis8 -. ais, ( --
    b4 ) -. r4 r8 b' -. cis [ -. cis ] -.
    b4 -. r r2
  }
  
  % BG: trumpet II, trumpets
  \transpose ees bes \relative c' {
  
    \key e \minor
  
    \break
    R1*4
    
    r2 \mp \times 2/3 { fis,4 b cis \mf }
    a2 \sfz \< b
    c?2 -- \f \times 2/3 { c4 -> c -> e -> }
    dis4 -. r r2
  }
  
  % V3: bone I, saxs (39: trumpet I)
  \relative c'' {
    
    \key b \minor
    
    \break
    R1
    r2 r8 \mp e, -. e16 -. e8 -. fis16 -.
    R1
    r2 r8 \mf fis -. fis16 -. fis8 -. g16 -.
    
    
    \break
    R1
    r2 r8 fis -. fis16 -. fis8 -. fis16 -.
    b,4 -. -^ \f \times 2/3 { r8 r b ( -> \fp \< ~ } b4 ais
    b4 ) -. \! r4 r2
  }
  
  % BB: tacet (solos)
  \relative c'' {
    
    \break
    R1*8  
  }
  
  % BG: tacet, trumpets
  \transpose ees bes \relative c' {
    
    \key e \minor
    
    \break
    r2 \mp \times 2/3 { d,4 e f }
    fis?1 \>
    r2 \! \times 2/3 { a4 b fis }
    e1 \>
    
    \break
    r2 \mp \times 2/3 { fis4 b cis \mf }
    a2 \sfz \< b
    c?2 -- \f \times 2/3 { c4 -> c -> e -> }
    dis4 -. r r2
  }
  
  % V4: violin, tacet
  \relative c'' {
    
    \key b \minor
    
    \break
    R1*8
  }
  
  % V5: tacet, saxs
  \relative c'' {
    
    \key b \minor
    
    \break
    b,4. -- \fp ais8 \fp ~ ais4 b8 -. c -.
    cis?4. -- \fp b8 -. ais2 \fp
    ais4. \bendBefore gis8 r a r ais -- ~
    ais4 r8 ais ( b cis d -. ) fis --
    
    \break
    e4 -> r8 fis -^ r f -^ r e -> \fp ~
    e4. d8 cis2
    b4 r8 b r b -- \times 2/3 { fis -^ fis -^ fis -^ }
    b4 -^ r r2
  }
  
  % V6: tacet, Trumpet I, Bone I&II, Baritone Sax (85: saxs)
  \transpose ees dis \relative c' {
    
    r2 r4 r8 c \f ( --
    
    \set Score.explicitKeySignatureVisibility = #all-visible
    \key c \minor
    
    \break
    d4 -. ) r4 r4 r8 c ( --
    d4 -. ) r4 r2
    r2 r8 c' -. c16 -. c8 -. c16 -.
    r4 r8 c, -. -^ r g'' -. f [ -. g ] -.
    
    \break
    f4. -- g,8 -. r g' -. b [ -. c,, ] ( -- 
    d4. -- ) g8 -. r2
    c,4 -. r8 c -. r c -. c16 -. c8. --
    c4 -. r4 r4 r8 c ( --
    
    \break
    d4 -. ) r4 r4 r8 c ( --
    d4 -. ) r4 r2
    r2 r8 c' -. c16 -. c8 -. c16 -.
  }
  
  \transpose ees bes, \transpose ees dis \relative c'' {
    
    \set Score.explicitKeySignatureVisibility = #begin-of-line-visible
    \key f \minor
    
    \break
    r8 \mp \times 2/3 { a16 bes d } \times 4/6 { ees ( -> aes, bes b c des ) } \times 4/6 { d ( -> bes c des d ees ) } \times 4/5 { e ( -> ees d des b ) }
  }
   
  \transpose ees dis \relative c' {
    
    \break
    d4. -- \f g8 -. r g' -. b [ -. c,, ] ( -- 
    d4. -- ) g8 -. r2
    c,4 -. r8 c -. r c -. c16 -. c8. --
    c4 -. r8 c' -. r8 g' -. f [ -. g ] -.
    
    \break
    f4 -- r8 g, -. r f' -. d [ -. c, ] ( --
    d4. ) -- g8 -. r4 r8 b, ( --
    c4 ) -. r8 c -. r c'4. \prallprall
    c4 -. r8 c -. r4 r8 c, ( --
    
    \break
    d4. -- ) g8 -. r4 r8 c, ( --
    d4. -- ) g8 -. r4 r8 b, ( --
    c4 -. ) r8 c -. r4 r8 \ff c' -> ~
    c c -> c4 -^ r2
  }
}





% score
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
\score {
  <<
    \scoreMarkup
    \scoreChords
    \new StaffGroup <<
      \new Staff { \transpose d bes \baritoneSax }
    >>
  >>
}


tenorSaxII = \relative c'' {

  % Para pintar todos los números de compás debajo del pentagrama
  % \override Score.BarNumber #'break-visibility = #end-of-line-invisible
  % \override Score.BarNumber #'direction = #DOWN
  % \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0	
  \set Score.explicitKeySignatureVisibility = #begin-of-line-visible
  
  % 00: tacet, Trumpet I, Bone I&II, Baritone Sax
  \transpose bes c \relative c''' {
  
    \key d \minor
    
    \partial 4.
    a8 -. \f a [ -. a ] -.
    
    bes4 -- \bendBefore a8 -- g -. r4 r8 a -.
    bes4 -- \bendBefore a8 -- g -. r8 g-. f [ -. g ] -.
    a4 -- \bendBefore g8 -- f -. r8 b8 -. b16 -. -^ b8 -. -^ b16 -. -^
    r2 r8 a8 -. \f g [ -. a ] -.
    
    \break
    g4 -- f8 -- e -. r4 cis8 -. cis -.
    d4 -- f,8 -- cis' -. r8 a' -. cis [ -. e ] -.
    d4 -- \bendBefore e8 -- d -. r b -^ b16 -^ b8. ->
    b4 -^ r r2
    
    \break
    R1*8
  }
  
  % V2: tacet, saxs
  \relative c'' {
  
    \key e \minor
  
    \break
    r8 \mf a, -. r a -- a4 -. a -.
    r8 a -. r a -- a4 -. a -.
    r2 g4 -. g -.
    r2 g4 -. g -.
  
    \break
    r8 a -. r a -- a4 -. a -.
    r8 a -. r4 a4 -. a -.
    r2 g8 -. g -. dis' -. dis -.
    e4 -. r4 r2
  }
  
  % BG: trumpet II, trumpets
  \relative c' {
  
    \key e \minor
  
    \break
    R1*4
    
    r2 \mp \times 2/3 { fis,4 b cis \mf }
    a2 \sfz \< b
    c?2 -- \f \times 2/3 { c4 -> c -> e -> }
    dis4 -. r r2
  }
  
  % V3: bone I, saxs (39: trumpet I)
  \relative c'' {
    
    \key e \minor
    
    \break
    R1
    r2 r8 \mp b, -. b16 -. b8 -. cis16 -.
    R1
    r2 r8 \mf cis -. cis16 -. cis8 -. e16 -.
    
    
    \break
    R1
    r2 r8 dis -. dis16 -. dis8 -. eis16 -.
    b4 -. -^ \f \times 2/3 { r8 r b ( -> \fp \< ~ } b2
    b4 ) -. \! r4 r2
  }
  
  % BB: tacet (solos)
  \relative c'' {
    
    \key e \minor
    
    \break
    R1*8  
  }
  
  % BG: tacet, trumpets
  \relative c' {
    
    \key e \minor
    
    \break
    r2 \mp \times 2/3 { d,4 e f }
    fis?1 \>
    r2 \! \times 2/3 { a4 b fis }
    e1 \>
    
    \break
    r2 \mp \times 2/3 { fis4 b cis \mf }
    a2 \sfz \< b
    c?2 -- \f \times 2/3 { c4 -> c -> e -> }
    dis4 -. r r2
  }
  
  % V4: violin, tacet
  \transpose bes c \relative c'' {
    
    \key d \minor
    
    \break
    R1*8
  }
  
  % V5: tacet, saxs
  \relative c' {
    
    \key e \minor
    
    \break
    e4. -- \fp dis8 \fp ~ dis4 e8 -. f -.
    fis?4. -- \fp e8 -. dis2 \fp
    dis4. \bendBefore cis8 r d r dis -- ~
    dis4 r8 dis ( e fis g -. ) b --
    
    \break
    a4 -> r8 b -^ r bes -^ r a -> \fp ~
    a4. g8 fis2
    g4 r8 g r g -- \times 2/3 { fis -^ fis -^ fis -^ }
    g4 -^ r r2
  }
  
  % V6: tacet, Trumpet I, Bone I&II, Baritone Sax (85: saxs)
  \transpose bes c \relative c''' {
    
    \key d \minor
    
    r2 r8 \f bes8 -. bes [ -. bes ] -.
  }
  
  \transpose ees dis \transpose bes c \relative c''' {
    
    \set Score.explicitKeySignatureVisibility = #end-of-line-visible
    \key ees \minor
    
    \pageBreak
    ces?4 -- \bendBefore bes8 -- aes -. r4 r8 bes-.
    ces4 -- \bendBefore bes8 -- aes -. r aes -. ges [ -. aes ] -.
    bes4-- \bendBefore aes8 -- ges -. r8 c -. c16 -. c8 -. c16 -.
    r4 r8 c -. r8 bes8 -. aes [ -. bes ] -.
    
    \break
    aes4 -- ges8 -- f -. r4 d'8 -. d-.
    ees4 -- ges8 -- d -. r bes -. d [ -. f ] -.
    ees4-- bes8 -- ees -. r c -^ c16 -^ c8. ->
    c4 -^ r r8 bes -. bes [ -. bes ] -.
    
    \break
    ces4 -- \bendBefore bes8 -- aes -. r4 r8 bes-.
    ces4 -- \bendBefore bes8 -- aes -. r aes -. ges [ -. aes ] -.
    bes4 -- \bendBefore aes8 -- ges -. r8 c c16 -. c8 -. c16 -.
  }
  
  \transpose ees dis \relative c'' {
    
    \set Score.explicitKeySignatureVisibility = #begin-of-line-visible
    \key c \minor
    
    \break
    r8 \mp \times 2/3 { a16 bes d } \times 4/6 { ees ( -> aes, bes b c des ) } \times 4/6 { d ( -> bes c des d ees ) } \times 4/5 { e ( -> ees d des b ) }
  }
  
  \transpose ees dis \transpose bes c \relative c'' {
    
    \key ees \minor
    
    \break
    aes'4 \f -- ges8 -- f -. r4 d'8 -. d-.
    ees4 -- ges8 -- d -. r bes -. d -. f -.
    ees4-- f8 -- ees -. r c -^ c16 -^ c8. ->
    c4 -^ r8 c -. r8 bes -. aes [ -. bes ] -.
    
    \break
    aes4 -- ges8 -- d -. r4 r8 d-.
    ees4 -- ges8 -- d -. r4 d'8 -. d -.
    ees4-- ees8 -- c -. r c4. \prallprall
    bes4 -^ r8 bes -. r bes -. aes [ -. bes ] -.
    
    \break
    aes4 -- ges8 -- d -. r4 d8 -. d-.
    aes'4 -- ges8 -- d -. r4 d'8 -. d -.
    c4 -- bes8 -- bes -. r4 r8 bes -> \ff ~
    bes bes -> bes4 -^ r2
  }
}
tenorSaxI = \relative c'' {

  % Para pintar todos los números de compás debajo del pentagrama
  % \override Score.BarNumber #'break-visibility = #end-of-line-invisible
  % \override Score.BarNumber #'direction = #DOWN
  % \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0	
  \set Score.explicitKeySignatureVisibility = #begin-of-line-visible
  
  % 00: tacet, Trumpet I, Bone I&II, Baritone Sax
  \transpose bes c \relative c''' {
  
    \key d \minor
    
    \partial 4.
    a8 -. \f a [ -. a ] -.
    
    bes4 -- \bendBefore a8 -- g -. r4 r8 a -.
    bes4 -- \bendBefore a8 -- g -. r8 g-. f [ -. g ] -.
    a4 -- \bendBefore g8 -- f -. r8 d'8 -. d16 -. -^ d8 -. -^ d16 -. -^
    r2 r8 a8 -. \f g [ -. a ] -.
    
    \break
    g4 -- f8 -- e -. r4 e8 -. e -.
    g4 -- f8 -- e -. r8 a -. cis [ -. e ] -.
    f4 -- \bendBefore e8 -- d -. r d -^ d16 -^ d8. ->
    d4 -^ r r2
    
    \break
    R1*8
  }
  
  % V2: tacet, saxs
  \relative c'' {
  
    \key e \minor
  
    \break
    r8 \mf c, -. r b -- b4 -. b -.
    r8 c -. r b -- b4 -. b -.
    r8 b -. r \times 2/3 { b16 ( c b ) } b4 -. b -.
    r8 b -. r b -- b4 -. b -.
  
    \break
    r8 c -. r b -- b4 -. b -.
    r8 c -. r b -- b4 -. b -.
    r8 b -. r \times 2/3 { b16 ( c b ) } b8 -. b -. fis' -. fis -.
    g4 -. r4 r2
  }
  
  % BG: trumpet II, trumpets
  \relative c' {
  
    \key e \minor
  
    \break
    R1*4
    
    r2 \mp \times 2/3 { fis,4 b cis \mf }
    << dis1 \sfz { s2. \< s4 \! } >>
    << { e2 -- \f \times 2/3 { g4 -> c -> e -> } } \\ { \hideNotes s4 e,4 \glissando g4 s } >>
    dis'4 -. r r2
  }
  
  % V3: bone I, saxs (39: trumpet I)
  \relative c'' {
    
    \key e \minor
    
    \break
    R1
    r2 r8 \mp dis, -. dis16 -. dis8 -. e16 -.
    R1
    r2 r8 \mf e -. e16 -. e8 -. fis16 -.
    
    
    \break
    R1
    r2 r8 fis -. fis16 -. fis8 -. gis16 -.
    e4 -. -^ \f \times 2/3 { r8 r e ( -> \fp \< ~ } e4 dis
    e4 ) -. \! r4 r2
  }
  
  % BB: tacet (solos)
  \relative c'' {
    
    \key e \minor
    
    \break
    R1*8  
  }
  
  % BG: tacet, trumpets
  \relative c' {
    
    \key e \minor
    
    \break
    r2 \mp \times 2/3 { fis,4 g gis }
    a1 \>
    r2 \! \times 2/3 { a4 b fis }
    e1 \>
    
    \break
    r2 \mp \times 2/3 { fis4 b cis \mf }
    << dis1 \sfz { s2. \< s4 \! } >>
    << { e2 -- \f \times 2/3 { g4 -> c -> e -> } } \\ { \hideNotes s4 e,4 \glissando g4 s } >>
    dis'4 -. r r2
  }
  
  % V4: violin, tacet
  \transpose bes c \relative c'' {
    
    \key d \minor
    
    \break
    R1*8
  }
  
  % V5: tacet, saxs
  \relative c' {
    
    \key e \minor
    
    \break
    e4. -- \fp dis8 \fp ~ dis4 e8 -. f -.
    fis?4. -- \fp e8 -. dis2 \fp
    dis4. \bendBefore cis8 r d r dis -- ~
    dis4 r8 dis ( e fis g -. ) b --
    
    \break
    a4 -> r8 b -^ r bes -^ r a -> \fp ~
    a4. g8 fis2
    b4 r8 b r b -- \times 2/3 { a -^ a -^ a -^ }
    b4 -^ r r2
  }
  
  % V6: tacet, Trumpet I, Bone I&II, Baritone Sax (85: saxs)
  \transpose bes c \relative c''' {
    
    \key d \minor
    
    r2 r8 \f bes8 -. bes [ -. bes ] -.
  }
  
  \transpose ees dis \transpose bes c \relative c''' {
    
    \set Score.explicitKeySignatureVisibility = #all-visible
    \key ees \minor
    
    \pageBreak
    ces?4 -- \bendBefore bes8 -- aes -. r4 r8 bes-.
    ces4 -- \bendBefore bes8 -- aes -. r aes -. ges [ -. aes ] -.
    bes4-- \bendBefore aes8 -- ges -. r8 ees' -. ees16 -. ees8 -. ees16 -.
    r4 r8 bes -. r8 bes8 -. aes [ -. bes ] -.
    
    \break
    aes4 -- ges8 -- f -. r4 f8 -. f-.
    aes4 -- ges8 -- f -. r bes -. d [ -. f ] -.
    ges4-- bes,8 -- ees -. r ees -^ ees16 -^ ees8. ->
    ees4 -^ r r8 bes -. bes [ -. bes ] -.
    
    \break
    ces4 -- \bendBefore bes8 -- aes -. r4 r8 bes-.
    ces4 -- \bendBefore bes8 -- aes -. r aes -. ges [ -. aes ] -.
    bes4 -- \bendBefore aes8 -- ges -. r8 ees' ees16 -. ees8 -. ees16 -.
  }
  
  \transpose ees dis \transpose bes ees \relative c'' {
    
    \set Score.explicitKeySignatureVisibility = #begin-of-line-visible
    \key c \minor
    
    \break
    r4 \mp \times 4/6 { ees16 ( -> e f ges g aes ) } \times 4/6 { a ( -> ges g aes a bes ) } \times 4/5 { b ( -> bes a aes ges ) }
  }
  
  \transpose ees dis \transpose bes c \relative c'' {
    
    \key ees \minor
    
    \break
    aes'4 -- \f ges8 -- f -. r4 f8 -. f-.
    aes4 -- ges8 -- f -. r bes -. d -. f -.
    ges4-- f8 -- ees -. r ees -^ ees16 -^ ees8. ->
    ees4 -^ r8 ees -. r8 bes -. aes [ -. bes ] -.
    
    \break
    aes4 -- ges8 -- f -. r4 r8 f-.
    aes4 -- ges8 -- f -. r4 d'8 -. f -.
    ges4-- f8 -- ees -. r ees4. \prallprall
    ees4 -^ r8 bes -. r bes -. aes [ -. bes ] -.
    
    \break
    aes4 -- ges8 -- f -. r4 f8 -. f-.
    aes4 -- ges8 -- f -. r4 d'8 -. \ff f -.
    ees4 -- c8 -- c -. r4 r8 c -> \ff ~
    c c -> c4 -^ r2
  }
}
altoSaxI = \relative c'' {

  % Para pintar todos los números de compás debajo del pentagrama
  % \override Score.BarNumber #'break-visibility = #end-of-line-invisible
  % \override Score.BarNumber #'direction = #DOWN
  % \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0	
  \set Score.explicitKeySignatureVisibility = #begin-of-line-visible
  
  % 00: tacet, Trumpet I, Bone I&II, Baritone Sax
  \transpose ees bes \relative c''' {
  
    \key e \minor
    
    \partial 4.
    b8 -. \f b [ -. b ] -.
    
    c4 -- \bendBefore b8 -- a -. r4 r8 b -.
    c4 -- \bendBefore b8 -- a -. r8 a-. g [ -. a ] -.
    b4 -- \bendBefore a8 -- g -. r4 e'16 -. -^ e8 -. -^ e16 -. -^
    r2 r8 b8 -. \f b [ -. b ] -.
    
    \break
    c4 -- b8 -- a -. r4 r8 b -.
    c4 -- \turn b8 -- a -. r8 dis -. -^ r8 a -.
    b4 -- \bendBefore a8 -- g -. r8 g16 ( gis a4 \glissando
    e' -^ ) r r2
    
    \break
    R1*8
  }
  
  % V2: tacet, saxs
  \relative c'' {
  
    \key b \minor
  
    \break
    r8 \mf e -. r d -- cis4 -. cis -.
    r8 e -. r d -- cis4 -. cis -.
    r8 d -. r \times 2/3 { cis16 ( d cis ) } b4 -. b -.
    r8 d -. r cis -- b4 -. b -.
  
    \break
    r8 e -. r d -- cis4 -. cis -.
    r8 e -. r e -- cis4 -. cis -.
    r8 d -. r \times 2/3 { cis16 ( d cis ) } b8 -. g' -. fis -. ais -.
    b4 -. r4 r2
  }
  
  % BG: trumpet II, trumpets
  \transpose ees bes \relative c' {
  
    \key e \minor
  
    \break
    R1*4
    
    r2 \mp \times 2/3 { fis4 b cis \mf }
    << dis1 \sfz { s2. \< s4 \! } >>
    << { e2 -- \f \times 2/3 { g4 -> c -> e -> } } \\ { \hideNotes s4 e,4 \glissando g4 s } >>
    dis'4 -. r r2
  }
  
  % V3: bone I, saxs (39: trumpet I)
  \relative c'' {
    
    \key b \minor
    
    \break
    R1
    r2 r8 \mp fis -. fis16 -. fis8 -. fis16 -.
    R1
    r2 r8 \mf fis -. fis16 -. fis8 -. g16 -.
    
    
    \break
    R1
    r2 r8 g -. g16 -. g8 -. g16 -.
    fis4 -. -^ \f \times 2/3 { r8 r fis -> \fp \< ~ } fis2
    fis4 -. \! r4 r2
  }
  
  % BB: tacet (solos)
  \relative c'' {
    
    \break
    R1*8  
  }
  
  % BG: tacet, trumpets
  \transpose ees bes \relative c' {
    
    \key e \minor
    
    \break
    r2 \mp \times 2/3 { fis4 g gis }
    a1 \>
    r2 \! \times 2/3 { a4 b fis }
    e1 \>
    
    \break
    r2 \mp \times 2/3 { fis4 b cis \mf }
    << dis1 \sfz { s2. \< s4 \! } >>
    << { e2 -- \f \times 2/3 { g4 -> c -> e -> } } \\ { \hideNotes s4 e,4 \glissando g4 s } >>
    dis'4 -. r r2
  }
  
  % V4: violin, tacet
  \relative c'' {
    
    \key b \minor
    
    \break
    R1*8
  }
  
  % V5: tacet, saxs
  \relative c'' {
    
    \key b \minor
    
    \break
    b'4. -- \fp ais8 \fp ~ ais4 b8 -. c -.
    cis?4. -- \fp b8 -. ais2 \fp
    ais4. \bendBefore gis8 r a r ais -- ~
    ais4 r8 ais ( b cis d -. ) fis --
    
    \break
    e4 -> r8 fis -^ r f -^ r e -> \fp ~
    e4. d8 cis2
    b4 r8 b r b -- \times 2/3 { ais -^ ais -^ ais -^ }
    b4 -^ r r2
  }
  
  % V6: tacet, Trumpet I, Bone I&II, Baritone Sax (85: saxs)
  \transpose ees dis \transpose ees bes \relative c''' {
    
    r2 r8 \f c8 -. c [ -. c ] -.
    
    \set Score.explicitKeySignatureVisibility = #all-visible
    \key f \minor
    
    \pageBreak
    des?4 -- c8 -- bes -. r4 r8 c-.
    des4 -- c8 -- bes -. r bes -. aes [ -. bes ] -.
    c4-- bes8 -- aes -. r4 f'16 -. f8 -. f16 -.
    r4 r8 c -. r8 c8 -. bes [ -. c ] -.
    
    \break
    des4 -- c8 -- bes -. r4 r8 c-.
    des4 -- \turn c8 -- bes -. r e -. -^ r4
    c4-- bes8 -- aes -. r c4. \glissando
    f4 -^ r r8 c -. c [ -. c ] -.
    
    \break
    des4 -- \bendBefore c8 -- bes -. r4 r8 c-.
    des4 -- \bendBefore c8 -- bes -. r bes -. aes [ -. bes ] -.
    c4-- bes8 -- aes -. r4 f'16 -. f8 -. f16 -.
    r4 r8 c -. r8 c8 -. bes [ -. c ] -.
    
    \break
    des4 -- c8 -- bes -. r4 r8 c-.
    des4 -- c8 -- bes -. r e -. -^ r4
    c4-- bes8 -- aes -. r c4. \glissando
    f4 -^ \bendAfter #-4 r8 c -. r8 c -. bes [ -. c ] -.
    
    \break
    des4 -- c8 -- bes -. r4 r8 c-.
    des4 -- c8 -- bes -. r e -. -^ r bes -.
    c4-- bes8 -- aes -. r c4. \glissando
    f4 -^ \bendAfter #-2 r8 c -. r c -. bes [ -. c ] -.
    
    \break
    des4 -- c8 -- bes -. r4 r8 c-.
    des4 -- \turn c8 -- bes -. r e -. -^ r bes -. \ff
    c4-- bes8 -- f' -. r4 r8 f -> \ff ~
    f f -> f4 -^ r2
  }
}
violin = \relative c'' {

  % Para pintar todos los números de compás debajo del pentagrama
  % \override Score.BarNumber #'break-visibility = #end-of-line-invisible
  % \override Score.BarNumber #'direction = #DOWN
  % \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0	
  \set Score.explicitKeySignatureVisibility = #begin-of-line-visible
  
  % 00: tacet, Trumpet I, Bone I&II, Baritone Sax
  \relative c'' {
  
    \key d \minor
    
    \partial 4.
    r8 r r  
    
    R1*8
    
    \break
    R1*8
  }
  
  % V2: tacet, saxs
  \relative c'' {
  
    \key d \minor
  
    \break
    R1*8
  }
  
  % BG: trumpet II, trumpets
  \transpose c bes, \relative c' {
  
    \key e \minor
  
    \break
    r2 \mp \times 2/3 { fis4 g gis }
    a1 \>
    r2 \! \times 2/3 { a4 b fis }
    e1 \>
    
    \break
    r2 \mp \times 2/3 { fis4 b cis \mf }
    << dis1 \sfz { s2. \< s4 \! } >>
    << { e2 -- \f \times 2/3 { g4 -> c -> e -> } } \\ { \hideNotes s4 e,4 \glissando g4 s } >>
    dis'4 -. r r2
  }
  
  % V3: bone I, saxs (39: trumpet I)
  \relative c' {
    
    \key d \minor
    
    \break
    r8 \mf cis' \bendAfter #-2 r8 b \bendAfter #-2 r8 a r a
    e'4 -- \bendBefore d8 -- cis -. r2
    r8 f \bendAfter #-2 r8 e \bendAfter #-2 r8 d r d
    a'4 -- \bendBefore g8 -- f -. r2
    
    \break
    r8 g \bendAfter #-2 r8 f \bendAfter #-2 r8 e r e
    g4 -- \bendBefore f8 -- e -. r2
    a2. a4 --
    a4 -. r4 r2
  }
  
  % BB: tacet (solos)
  \relative c'' {
    
    \break
    R1*8  
  }
  
  % BG: tacet, trumpets
  \relative c'' {
    
    \break
    R1*8  
  }
  
  % V4: violin, tacet
  \relative c'' {
    
    \key d \minor
    
    \break
    r4 r8 \f a cis e g a
    bes2 a
    r4 r8 a, b d f a
    bes2 a
    
    \break
    r4 r8 \f cis, e g a cis
    d2 cis
    R1*2
  }
  
  % V5: tacet, saxs
  \relative c'' {
    
    \break
    R1*9
  }
  
  % V6: tacet, Trumpet I, Bone I&II, Baritone Sax (85: saxs)
  \transpose ees dis \transpose c bes, \relative c''' {
    
    \set Score.explicitKeySignatureVisibility = #all-visible
    \key f \minor
    
    \break
    R1*8
    
    \break
    R1*7
    r2 r8 c8 -. bes -. c -.
    
    \break
    des4 -- c8 -- bes -. r4 r8 c-.
    des4 -- c8 -- bes -. r e -. -^ r bes -.
    c4-- bes8 -- aes -. r c4. \glissando
    f4 -^ \bendAfter #-2 r8 c -. r c -. bes [ -. c ] -.
    
    \break
    des4 -- c8 -- bes -. r4 r8 c-.
    des4 -- \turn c8 -- bes -. r e -. -^ r bes -. \ff
    c4-- bes8 -- f' -. r4 r8 f -> \ff ~
    f f -> f4 -^ r2
  }
}

bass = \relative c, {

  % Para pintar todos los números de compás debajo del pentagrama
  % \override Score.BarNumber #'break-visibility = #end-of-line-invisible
  % \override Score.BarNumber #'direction = #DOWN
  % \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0	
  
  \clef bass
  \key d \minor
  
  \partial 4.
  r8 r r
  
  \set Score.currentBarNumber = #1
  
  e4 -- \mf r8 a8 ~ a4 a --
  e4 -- r8 a8 -- a8 -- r a4 --
  d,4 -- r8 a'8 ~ a8 r a'4 --
  d,4 -- r8 a8 -- d, e a4 --
  
  \break
  e4 -- r8 a8 ~ a8 r a4 --
  e4 -- r8 a8 -- a8 -- r a4 --
  d,4 -- r8 f' d a f ( e )
  d4 -^ r r r8 d --
  
  \break
  e4 -- r8 a8 ~ a4 \bendAfter #-4 a --
  e4 -- r8 a8 ~ a8 gis a ( g )
  d4 -- r8 a'8 ~ a8 r a4 --
  d4 -- r8 d a gis d4 --
  
  \break
  e4 -- r8 a8 ~ a r a4 --
  e4 -- r8 a8 ~ a8 r a4 -- \bendAfter #-4
  d,4 -- r8 d' -- r a f [ e ]
  d8 -- \bendAfter #-4 r r4 r2
  
  \break
  e4 -- r8 a8 ~ a r a4 --
  e4 -- r8 a8 -- a8 -- r a4 --
  d,4 -- r8 a'8 ~ a8 r a8 a
  d4 -- r8 a8 f fis d dis
  
  \break
  e4 -- r8 a8 ~ a r a4 --
  e4 -- r8 a8 ~ a r a8 -- a --
  d,4 -- r8 a' r d a [ f ]
  d4 -^ r4 r2
  
  \pageBreak
  c4 -- r8 c' ~ c r c4 --
  c,4 -- r8 c' ~ c g e c
  f4 -- r8 f c'4 -- r
  f, -- r8 f' c c f, g
  
  \break
  a4 -- r8 a e'4 -- e, --
  a -- r8 a e -- e -- a4 --
  bes4 -- bes -- \times 2/3 { bes -^ bes -^ bes -^ }
  a -. -^ r r2
  
  \break
  e4 -- r8 a8 ~ a4 a4 --
  e4 -- r8 a8 -- a4 \bendAfter #-4 a4 --
  d,4 -- r8 d' ~ d a f e
  d4 -- r8 e ( f ) fis g4 --
  
  \break
  e4 -- r8 a8 ~ a r a4 --
  e4 -- r8 a8 ~ a r a4 --
  d,4 -- r8 d'8 a f g e
  d4 -^ r r2
  
  \break
  e4 -- r8 a8 ~ a r a4 --
  e'4 -- r8 a,8 ~ a r a4 --
  d,4 -- r8 a'8 ~ a r a8 -- a --
  d4 -- r8 d a f d dis
  
  \break
  e4 -- r8 a8 ~ a r a4 --
  e4 -- r8 a8 ~ a r a4 --
  d,4 -- r8 d'8 a f g e
  d4 -^ r r2
  
  \break
  c4 -- r8 c8 c'4 -- g --
  c,4 -- r8 c8 c'4 -- c, --
  f4 -- r8 f8 c'4 -- c, --
  f4 -- r8 f8 c'4 -- f,8 -- g --
  
  \break
  a4 -- r8 e'8 ~ e r e4 --
  a,4 -- r8 a8 ~ a r a8 -- a --
  bes4 -- bes -- \times 2/3 { bes -^ bes -^ bes -^ }
  a -. -^ r r2
  
  \pageBreak
  R1*8
  
  \break
  R1*6
  r4  r8 d a f g e
  d4 -^ r r2
  R1
  
  \transpose ees dis \relative c, {
    
    \key ees \minor
    
    \break
    f4 -- r8 bes8 ~ bes r bes8 -- bes --
    f4 -- r8 bes8 -- bes -. bes -. bes -- e, --
    ees?4 -- r8 bes' -- bes -- r bes -- bes --
    ees4 -- r8 ees bes ges ees e
    
    \break
    f4 -- r8 aes8 -- bes -- r bes8 -- bes --
    f4 -- r8 a8 ~ a r bes8 -- bes --
    ees,4 -- r8 ces'8 ~ ces r bes4 --
    ees4 -- r8 ees bes a ees e
    
    \break
    f4 -- r8 bes8 ~ bes r bes8 -- bes --
    f4 -- r8 bes8 ~ bes r bes4 -- \bendAfter #-4
    ees,4 -- r8 bes'8 ~ bes r bes4 -- 
    ees4 -- r8 ees8 bes ges ees e
    
    \break
    f4 -- r8 bes8 ~ bes r bes8 -- bes --
    f'4 -- r8 bes,8 ~ bes r bes8 -- bes --
    ees,4 -- r8 ees'8 bes -- r bes4 -- 
    ees4 -- r8 des8 bes aes ges ees
    
    \break
    f4 -- r8 ces'8 bes r bes8 -- bes -.
    f4 -- r8 ces'8 ~ ces r bes4 -- 
    ees,4 -- r8 bes'8 ~ bes r bes4 --
    ees4 -- r8 ees8 bes bes ees, ( e )
    
    \break
    f4 -- r8 bes8 -- bes -- r bes4 --
    
    \revert Score.Glissando.bound-details.left.padding
    \revert Score.Glissando.style
    f4 -- r8 bes8 -- bes -- r bes8 \glissando f -.
    ees4 -^ r8 ees8 -^ r4 r8 ees -^
    r8 ees -^ ees4 -^ r2
  }
}



\score {
  <<
    \scoreMarkup
    \transpose ees c \scoreChords
    \new Staff \with {midiInstrument = #"violin"} { \transpose d bes, \violin }
    \new Staff \with { midiInstrument = "tenor sax" } { \transpose c ees, \transpose d bes, \altoSaxI }
    \new Staff \with { midiInstrument = "tenor sax" } { \transpose c bes,, \transpose d bes \tenorSaxI }
    \new Staff \with { midiInstrument = "tenor sax" } { \transpose c bes,, \transpose d bes \tenorSaxII }
    \new Staff \with { midiInstrument = "baritone sax" } { \transpose c ees,, \transpose d bes \baritoneSax }
    \new Staff \with { midiInstrument = "pizzicato strings" } { \transpose d bes \bass }
  >>
  \midi {} 
}