\version "2.18.2"

\include "lily-jazz/ignatzek-jazz-chords.ily"
\include "lily-jazz/lily-jazz.ily"





% PLUGINS 1.2
% -----------
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~





% multi-mark-engraver
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
% Aporta la posibilidad de pintar un rehearsal mark al final de una línea y 
% al comienzo de la siguiente.
% http://lists.gnu.org/archive/html/lilypond-user/2011-08/msg00157.html
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
#(define (multi-mark-engraver ctx)
   (let ((texts '())
         (final-texts '())
         (events '()))

     `((start-translation-timestep
        . ,(lambda (trans)
             (set! final-texts '())))

       (listeners
        (mark-event
         . ,(lambda (trans ev)
              (set! events (cons ev events)))))

       (acknowledgers
        (break-alignment-interface
         . ,(lambda (trans grob source)
              (for-each (lambda (mark)
                          (set! (ly:grob-parent mark X) grob))
                        texts))))

       (process-music
        . ,(lambda (trans)
             (for-each
              (lambda (ev)
                (let* ((mark-grob
                        (ly:engraver-make-grob trans 'RehearsalMark ev))
                       (label (ly:event-property ev 'label))
                       (formatter (ly:context-property ctx 'markFormatter)))

                  (if (and (procedure? formatter)
                           (not (markup? label)))
                      (begin
                       (if (not (number? label))
                           (set! label
                                 (ly:context-property ctx 'rehearsalMark)))

                       (if (and (integer? label)
                                (exact? label))
                           (set! (ly:context-property ctx 'rehearsalMark)
                                 (1+ label)))

                       (if (number? label)
                           (set! label (apply formatter (list label ctx)))
                           (ly:warning "rehearsalMark must have
integer value"))))

                  (if (markup? label)
                      (begin
                       (set! (ly:grob-property mark-grob 'text) label)
                       (let ((dir (ly:event-property ev 'direction)))
                         (and (ly:dir? dir)
                              (set! (ly:grob-property mark-grob 'direction)
                                    dir))))
                      (ly:warning "mark label must be a markup object"))

                  (set! texts (cons mark-grob texts))))
              (reverse events))))

       (stop-translation-timestep
        . ,(lambda (trans)
             (if (pair? texts)
                 (let ((staves (ly:context-property ctx 'stavesFound))
                       (priority-index 0))
                   (for-each (lambda (grob)
                               (let ((my-priority (ly:grob-property
grob 'outside-staff-priority 1500)))
                                 (for-each (lambda (stave)

(ly:pointer-group-interface::add-grob grob 'side-support-elements
                                               stave))
                                           staves)
                                 (set! (ly:grob-property grob
'outside-staff-priority) (+ my-priority priority-index))
                                 (set! priority-index (1+ priority-index))
                                 (set! final-texts (cons grob final-texts))))
                             (reverse texts))
                     (set! texts '())
                     (set! events '())))))

        (finalize
         . ,(lambda (trans)
              (and (pair? final-texts)
                   (for-each (lambda (grob)
                               (set! (ly:grob-property grob 'break-visibility)
                                     end-of-line-visible))
                             final-texts)))))))


% Funciones originales
%{
myMark =
#(define-music-function (parser location text) (markup?)
  (make-music 'MarkEvent
              'label text))
			  
toCoda = {
	\tweak #'self-alignment-X #RIGHT
	\tweak #'break-visibility #begin-of-line-invisible
	\myMark \markup { \hspace #1.25 \raise #1.25 \musicglyph #"scripts.coda" }
}
%}

% Función maqueada.
% Se apoya en el código obtenido por consola al ejecutar { \displayMusic { \toCoda }}
eolMark =
#(define-music-function (parser location text) (markup?)
	(make-music 
		'MarkEvent 
		'tweaks (list 
			(cons (quote self-alignment-X) 1)
			(cons (quote break-visibility) #(#t #t #f))
		) 
		'label (markup
			#:line (
				#:hspace 1.25
				#:raise 1.25
				text
			)
		)
	)
)










% rest-score y merge-rests-on-positioning
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
% Permite realizar la fusión de los silencios entre dos voces en un mismo pentagrama.
% https://code.google.com/p/lilypond/issues/detail?id=1228 (fichero merge-rests.ily)
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]

#(define (rest-score r)
  (let ((score 0)
	(yoff (ly:grob-property-data r 'Y-offset))
	(sp (ly:grob-property-data r 'staff-position)))
    (if (number? yoff)
	(set! score (+ score 2))
	(if (eq? yoff 'calculation-in-progress)
	    (set! score (- score 3))))
    (and (number? sp)
	 (<= 0 2 sp)
	 (set! score (+ score 2))
	 (set! score (- score (abs (- 1 sp)))))
    score))

#(define (merge-rests-on-positioning grob)
  (let* ((can-merge #f)
	 (elts (ly:grob-object grob 'elements))
	 (num-elts (and (ly:grob-array? elts)
			(ly:grob-array-length elts)))
	 (two-voice? (= num-elts 2)))
    (if two-voice?
	(let* ((v1-grob (ly:grob-array-ref elts 0))
	       (v2-grob (ly:grob-array-ref elts 1))
	       (v1-rest (ly:grob-object v1-grob 'rest))
	       (v2-rest (ly:grob-object v2-grob 'rest)))
	  (and
	   (ly:grob? v1-rest)
	   (ly:grob? v2-rest)
	   (let* ((v1-duration-log (ly:grob-property v1-rest 'duration-log))
		  (v2-duration-log (ly:grob-property v2-rest 'duration-log))
		  (v1-dot (ly:grob-object v1-rest 'dot))
		  (v2-dot (ly:grob-object v2-rest 'dot))
		  (v1-dot-count (and (ly:grob? v1-dot)
				     (ly:grob-property v1-dot 'dot-count -1)))
		  (v2-dot-count (and (ly:grob? v2-dot)
				     (ly:grob-property v2-dot 'dot-count -1))))
	     (set! can-merge
		   (and
		    (number? v1-duration-log)
		    (number? v2-duration-log)
		    (= v1-duration-log v2-duration-log)
		    (eq? v1-dot-count v2-dot-count)))
	     (if can-merge
		 ;; keep the rest that looks best:
		 (let* ((keep-v1? (>= (rest-score v1-rest)
				      (rest-score v2-rest)))
			(rest-to-keep (if keep-v1? v1-rest v2-rest))
			(dot-to-kill (if keep-v1? v2-dot v1-dot)))
		   ;; uncomment if you're curious of which rest was chosen:
		   ;;(ly:grob-set-property! v1-rest 'color green)
		   ;;(ly:grob-set-property! v2-rest 'color blue)
		   (ly:grob-suicide! (if keep-v1? v2-rest v1-rest))
		   (if (ly:grob? dot-to-kill)
		       (ly:grob-suicide! dot-to-kill))
		   (ly:grob-set-property! rest-to-keep 'direction 0)
		   (ly:rest::y-offset-callback rest-to-keep)))))))
    (if can-merge
	#t
	(ly:rest-collision::calc-positioning-done grob))))










% startParenthesis y endParenthesis
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
% http://comments.gmane.org/gmane.comp.gnu.lilypond.general/64087
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]

% ELIMINADO










% bendBefore
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
% Adaptación
% http://lilypondblog.org/2013/08/adding-ornamentations-to-note-heads-part-1/
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]

%%%
%%% Utilities for defining new grobs, grob properties and music event types
%%% (there should be built-in commands to do that in LilyPond)
%%%
#(define (define-grob-type grob-name grob-entry)
   (let* ((meta-entry   (assoc-get 'meta grob-entry))
          (class        (assoc-get 'class meta-entry))
          (ifaces-entry (assoc-get 'interfaces meta-entry)))
     (set-object-property! grob-name 'translation-type? list?)
     (set-object-property! grob-name 'is-grob? #t)
     (set! ifaces-entry (append (case class
                                  ((Item) '(item-interface))
                                  ((Spanner) '(spanner-interface))
                                  ((Paper_column) '((item-interface
                                                     paper-column-interface)))
                                  ((System) '((system-interface
                                               spanner-interface)))
                                  (else '(unknown-interface)))
                                ifaces-entry))
     (set! ifaces-entry (uniq-list (sort ifaces-entry symbol<?)))
     (set! ifaces-entry (cons 'grob-interface ifaces-entry))
     (set! meta-entry (assoc-set! meta-entry 'name grob-name))
     (set! meta-entry (assoc-set! meta-entry 'interfaces
                                  ifaces-entry))
     (set! grob-entry (assoc-set! grob-entry 'meta meta-entry))
     (set! all-grob-descriptions
           (cons (cons grob-name grob-entry)
                 all-grob-descriptions))))

#(define-public (define-grob-property symbol type? description)
  (set-object-property! symbol 'backend-type? type?)
  (set-object-property! symbol 'backend-doc description)
  symbol)

#(defmacro*-public define-music-type (type-name parents #:rest properties)
   (let ((gproperties (gensym "properties")))
     `(let ((,gproperties (list-copy ',properties)))
        ,@(map (lambda (parent)
                `(define-event-class ',(ly:camel-case->lisp-identifier type-name)
                   ',parent))
               parents)
        (set-object-property! ',type-name
                              'music-description
                              (cdr (assq 'description ,gproperties)))
        (set! ,gproperties (assoc-set! ,gproperties 'name ',type-name))
        (set! ,gproperties (assq-remove! ,gproperties 'description))
        (hashq-set! music-name-to-property-table ',type-name ,gproperties)
        (set! music-descriptions
              (cons (cons ',type-name ,gproperties)
                    music-descriptions)))))

%%%
%%% Grob definition
%%%
#(define (head-ornamentation::print me)
   (let* ((notes (ly:grob-object me 'elements))
          (y-ref (ly:grob-common-refpoint-of-array me notes Y))
          (x-ref (ly:grob-common-refpoint-of-array me notes X))
          (x-ext (ly:relative-group-extent notes x-ref X))
          (y-ext (ly:relative-group-extent notes y-ref Y))
          (y-coord (interval-center y-ext))
          (text (ly:text-interface::print me))
          (width (/ (interval-length (ly:stencil-extent text X)) 2.0))
          (x-coord (- (car x-ext) width)))
     (ly:stencil-translate
      text
      (cons
       (- (- x-coord (ly:grob-relative-coordinate me x-ref X)) .7)
       (- (- y-coord (ly:grob-relative-coordinate me y-ref Y)) 1.5)))))

#(define-grob-type 'HeadOrnamentation
  `((text . ,#{ \markup \fontsize #-4 \musicglyph #"brackettips.up" #})
    (stencil . ,head-ornamentation::print)
    (meta . ((class . Item)
             (interfaces . (font-interface))))))

\layout {
  \context {
    \Global
    \grobdescriptions #all-grob-descriptions
  }
}

%%%
%%% Engraver
%%%
%%% The head-ornamentation engraver, with its note-head acknowledger
%%% (which add HeadOrnamentation grobs to note heads)
#(define head-ornamentation-engraver
   (make-engraver
    (acknowledgers
     ((note-head-interface engraver note-grob source)
      ;; When the note-head event attached to the note-head grob has
      ;; ornamentation events among its articulations, then create a
      ;; HeadOrnamentation grob
      (let* ((note-event (ly:grob-property note-grob 'cause)))
        (for-each
         (lambda (articulation)
           (if (memq 'head-ornamentation-event
                     (ly:event-property articulation 'class))
               (let ((ornament-grob (ly:engraver-make-grob engraver
                                                           'HeadOrnamentation
                                                           note-grob)))
                 (ly:pointer-group-interface::add-grob ornament-grob
                                                       'elements
                                                       note-grob)
                 (set! (ly:grob-parent ornament-grob Y) note-grob)
                 (set! (ly:grob-property ornament-grob 'font-size)
                       (+ (ly:grob-property ornament-grob 'font-size 0.0)
                          (ly:grob-property note-grob 'font-size 0.0))))))
         (ly:event-property note-event 'articulations)))))))

\layout {
  \context {
    \Voice
    \consists #head-ornamentation-engraver
  }
}

%%%
%%% Event type definition
%%%
#(define-music-type HeadOrnamentationEvent (music-event)
   (description . "Print an ornamentation at a note head side")
   (types . (general-music post-event event head-ornamentation-event)))

bendBefore = #(make-music 'HeadOrnamentationEvent)










% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
% Funciones propias
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]





% Índice con borde circular y un cómodo espacio interior (y no el que viene por defecto)
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
#(define-markup-command (boxindex layout props text) (markup?)
	(interpret-markup layout props
		(markup #:huge #:override '(box-padding . 0.5) #:box text)
	)
)

#(define-markup-command (boxed layout props text) (markup?)
	(interpret-markup layout props
		(markup #:huge #:override '(box-padding . 0.9) #:box text)
	)
)

% Ritardando
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
rit =
#(make-music 'CrescendoEvent
             'span-direction START
             'span-type 'text
             'span-text "rit.")

% Negra oblicua
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
rs = {
  \once \override Rest #'stencil = #ly:percent-repeat-item-interface::beat-slash
  \once \override Rest #'thickness = #0.48
  \once \override Rest #'slope = #1.7
  r4
}










% Añadido de funciones al contexto
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
\layout {
  \context {
    \Score		
    
    % multi-mark-engraver
    \remove "Mark_engraver"
    \consists #multi-mark-engraver
    \consists "Tweak_engraver"
    
    % merge-rests-on-positioning
    \override RestCollision #'positioning-done = #merge-rests-on-positioning
    
    % aspecto de los silencios multicompás
    \override MultiMeasureRest.expand-limit = #1
    
    % margen extra de las articulaciones para evitar la colisión con las ligaduras
    \override Script.padding = #0.5
    \override Script.avoid-slur = #'inside
    
    % glissando bonito
    \override Glissando.style = #'trill
    \override Glissando.bound-details.left.padding = #1.7
    
    % dinámicas un poco mas gruesas
    \override Hairpin.thickness = #1.75
    
    % Esconde la cancelacion de accidentales en el cambio de armadura
    % \override KeyCancellation.break-visibility = #'#(#f #f #f)
  }
}












% LAYOUT
% ------
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~






\header {
  title = "Sway"
  subtitle = "(Michael Buble Version)"
  instrument = "Piano"
  composer = "Pablo BELTRÁN"
  arranger = ""
  poet = \markup \tiny "Reducción y edición: Gabriel PALACIOS"
  meter = \markup \tiny "gabriel.ps.ms@gmail.com"
  tagline = \markup {
    \center-column {
      \line { \tiny "Reducción y edición: Gabriel PALACIOS" }
      \line { \tiny "gabriel.ps.ms@gmail.com" }
    }
  }
}

\paper {
  % El definir otro tipo de fuente no permite cambiar el tamaño global del staff. Es un bug conocigo
  #(define fonts (make-pango-font-tree "LilyJAZZText" "Nimbus Sans" "Luxi Mono" (/ 20 20)))
  
  % distancia entre la parte superior de la página y el título
  top-markup-spacing #'basic-distance = 5
  
  % distancia entre el título y el primer sistema
  markup-system-spacing #'basic-distance = 25
  
  % distancia entre el texto y el primer sistema 
  top-system-spacing #'basic-distance = 15
  
  % márgen inferior
  bottom-margin = 20
    
  % número de páginas
  % page-count = 1
  
  ragged-last-bottom = ##f 
  
  oddHeaderMarkup = \markup {
    \on-the-fly #not-first-page \fill-line {
      \fromproperty #'page:page-number-string
      \fromproperty #'header:title
      \fromproperty #'header:instrument
    }
  }
  
  evenHeaderMarkup = \markup {
    \on-the-fly #not-first-page \fill-line {
      \fromproperty #'header:instrument
      \fromproperty #'header:title
      \fromproperty #'page:page-number-string
    }
  }
}










% MUSIC PARTS
% -----------
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~





% scoreMarkup
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
scoreMarkup = \new Staff \with {
   \RemoveEmptyStaves
}
{
  % Oculta el primer pentagrama del markup
  \override Staff.VerticalAxisGroup #'remove-first = ##t
  
  % Indicación de tiempo
  \once \override Score.RehearsalMark.X-offset = #5
  \mark \markup \normalsize "(Count 1, 2, 3, 4 into bar 1)"
  \mark \jazzTempoMarkup "Latin" c4 "127"
  
  \partial 4.
  s4.
  
  s1*8
  
  \bar "||"
  \mark \markup \boxed "V1"
  s1*8
  
  \bar "||"
  \mark \markup \boxed "V2"
  s1*8
  
  \bar "||"
  \mark \markup \boxed "Bridge"
  s1*8
  
  \bar "||"
  \mark \markup \boxed "V3"
  s1*8
  
  \bar "||"
  \mark \markup \boxed "Bass Break"
  s1*8
  
  \bar "||"
  \mark \markup \boxed "Bridge"
  s1*8
  
  \bar "||"
  \mark \markup \boxed "V4 - Violins"
  s1*8
  
  \bar "||"
  \mark \markup \boxed "V5"
  s1*9
  
  \bar "||"
  \mark \markup \boxed "V6 - KEY CH"
  s1*8
  
  \bar "||"
  s1*8
  
  \bar "||"
  s1*8
  
  \bar "|."
}





% scoreChords
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
scoreChords = \chords {
  
  \set minorChordModifier = \markup {"-"}
  
  \partial 4.
  s4.
  
  e2:m7.5- a:7
  e2:m7.5- a:7
  d2:m7+ d:m6
  d2:m7+ d:m6
  
  \break
  e2:m7.5- a:7
  e2:m7.5- a:7
  d2:m7+ d:m6
  d1:m
  
  \break
  e2:m7.5- a:7
  e2:m7.5- a:7
  d2:m7+ d4:m6 e:dim
  d1:m
  
  \break
  e2:m7.5- a:7
  e2:m7.5- a:7
  d2:m7+ d2:m6
  d1:m
  
  \break
  e2:m7.5- a:7
  e2:m7.5- a:7
  d2:m7+ d2:m6
  d1:m
  
  \break
  e2:m7.5- a:7
  e2:m7.5- a:7
  d2:m7+ d2:m6
  d1:m
  
  \pageBreak
  c1:5+
  c1:7
  f:maj7
  f:6
  
  \break
  a1
  a:7
  bes:7
  a:7
  
  \break
  e2:m7.5- a:7
  e2:m7.5- a:7
  d2:m7+ d4:m6 e:dim
  d1:m
  
  \break
  e2:m7.5- a:7
  e2:m7.5- a:7
  d2:m7+ d2:m6
  s1
  
  \break
  e2:m7.5- a:7
  e2:m7.5- a:7
  d2:m7+ d4:m6 e:dim
  d1:m
  
  \break
  e2:m7.5- a:7
  e2:m7.5- a:7
  s1
  s1
  
  \break
  c1:5+
  c1:7
  f:maj7
  f:6
  
  \break
  a1
  a:7
  s1*2
  
  \break
  s1*7
  s4 d2.:6
  
  \break
  e2:m7.5- a:7
  e2:m7.5- a:7
  d2:m7+ d4:m6 e:dim
  d1:m
  
  \break
  e2:m7.5- a:7
  e2:m7.5- a:7
  d2:m7+ d2:m6
  s1
  s1
  
  \break
  f2:m7.5- bes:7
  f2:m7.5- bes:7
  ees2:m7+ ees4:m6 f:dim
  ees1:m
  
  \break
  f2:m7.5- bes:7
  f2:m7.5- bes:7
  ees2:m7+ ees2:m6
  ees1:m
  
  \break
  f2:m7.5- bes:7
  f2:m7.5- bes:7
  ees2:m7+ ees4:m6 f:dim
  ees1:m
  
  \break
  f2:m7.5- bes:7
  f2:m7.5- bes:7
  ees2:m7+ ees2:m6
  ees1:m
  
  \break
  f2:m7.5- bes:7
  f2:m7.5- bes:7
  ees2:m7+ ees4:m6 f:dim
  ees1:m
  
  \break
  f2:m7.5- bes:7
  f2:m7.5- bes:7
  ees1:m6
  s1
}





% music
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
rightHand = \relative c'' {

  % Para pintar todos los números de compás debajo del pentagrama
  % \override Score.BarNumber #'break-visibility = #end-of-line-invisible
  % \override Score.BarNumber #'direction = #DOWN
  % \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0
  
  \key d \minor
  
  \partial 4.
  r8 r r
  
  <d d'>4 <g a>8 <cis, cis'> ~ <cis cis'> <g' a>4 <d d'>8 ~
  <d d'>4 <g a>8 <cis, cis'> <cis cis'>4 <g' a>8 <cis, cis'> ~
  \break
  <cis cis'>4 <f a>8 <b, b'> ~ <b b'> <c c'>4 <cis cis'>8 ~
  <cis cis'> <f a> <d d'> <ees ees'> <e e'> <f f'> <e e'> <ees ees'>
  
  \break
  <d d'>4 <g a>8 <cis, cis'> ~ <cis cis'> <g' a>4 <d d'>8 ~
  <d d'>4 <g a>8 <cis, cis'> <cis cis'>4 <g' a>8 <cis, cis'>
  \break
  <d d'>2 \times 2/3 { <d d'>4 <e e'> <f f'> }
  <d d'>2 r2
  
  \break
  <g, bes d>2 <g a cis>
  <g bes d e> <g a cis>
  <f a d>2. <g bes cis e>4 ->
  <a d f>2. -> <d f a>4 ->
  
  \break
  <bes d g>2 -> <a cis e>
  <bes d g>4. -> <g a cis e>8 -> ~ <g a cis e> <g cis> <g cis>4
  <f a d>2 \times 2/3 { <f' a d>4 -> <g bes cis e> -> <a d f> -> }
  <f a d>4 -^ r r2
  
  \break
  <bes, d e g>4. <g a cis e>8 ~ <g a cis e>2
  <bes d e g>2 <g a cis e>4 <g a cis e>
  <a d f>2 ~ <a d f>8 <d f a b>4. ->
  <f a d>4. -> cis'8 <f, a b>4 <f a d> ->
  
  \break
  <g, bes d>2 <g a cis>
  <g bes d>4. <g a cis>8 ~ <g a cis>2
  \times 2/3 { r4 <d' d'> -> <e e'> -> } \times 2/3 { <f f'> -> <g g'> -> <a a'> -> }
  <d f a d>4 -^ r4 r2
  
  \break
  <gis,, c e>2 <e' e'>4 -> <f f'> ->
  <g g'>1 ->
  <f a c e>2 -> ~ \times 2/3 { <f a c e>4 <a e' a> -> <e' a> -> }
  <a, d a'>1 ->
  
  \break
  \appoggiatura { e16 d } cis2. <g a cis>4
  <g a cis>2 <g a cis>2
  <aes c d>2 \times 2/3 { <d f aes bes>4 -> <d f aes bes>4 -> <d f aes bes>4 -> }
  <cis e g a>4 -^ r r2
  
  \break
  <g bes d>2 <g a cis>
  <g bes d e>4. <g a cis>8 ~ <g a cis>2
  <f a d>2. <g bes cis e>4 ->
  <a d f>1 ->
  
  \break
  <g bes d>4. <g a cis>8 ~ <g a cis>4 g8 bes
  <bes d g bes>4. <cis e g a>8 ~ <cis e g a> g bes cis
  <d f a>4. <d' f a>8 -> <d f bes>8. -> <cis e a>16 -> ~ <cis e a>8 <cis e a>
  <b d f a>4 -^ r r2
  
  \break
  <g bes d g>4. <a cis e>8 ~ <a cis e>2
  <g bes d g>4. a'8 <a, cis e a>4 -> <a a'>
  <f a b d> -. -^ <f a b d>8 -> <f a b d>8 -. -^ r <g bes cis e> -> ~ <g bes cis e> [ <f a d> ]-> ~
  <f a d>4 <d f a b> <f a d> -> d8 d
  
  \break
  <bes' bes'>4 -> <a a'>8 -> <g g'> -> ~ <g g'>2
  <bes bes'>4 -> <c a'>8 -> <bes g'> -> <cis, e g a>4  <cis e g a>
  <d f a b> <f a cis>8 <f a b> -. <d f a d>4. -> <d f a b>8 ->
  <d f a b>4 -^ r4 r2
  
  \break
  <c e gis>2 \ottava #1 \times 2/3 { <c' c'>4 <d d'> <ees ees'> }
  <e? bes' c e?>2. -> <c c'>8 <d d'>
  <e e'>4 <a, a'>8 <c c'> <e e'>8. <g g'>16 ~ <g g'>8 <f f'>
  <e a c e>4. a8 e c e <dis dis'>
  
  \break
  <e e'>4 g8 cis ~ cis4 <e, g a d>4
  <e g a cis>2 r8 e, a cis
  \times 2/3 { <d d'>4 -> <f f'>4 -> <d d'>4 -> } \times 2/3 { <f f'>4 -> <bes, bes'>4 -> <d d'>4 ->}
  <cis cis'>4 -^ \ottava #0 r r2
  
  \break
  R1*7
  r4 ^"gliss." \change Staff = "leftHand" \makeClusters { a,, d' \change Staff = "rightHand" a' }
  
  \break
  \ottava #1 <d d'>4 <g bes>8 <cis, cis'> ~ <cis cis'> <g' a> <d d'> <dis dis'>
  <e e'>4 <g bes>8 <d d'> <cis cis'>4 <g' a>
  <cis, cis'> <g' a>8 <b, b'> r <c c'> r <cis cis'> ~
  <cis cis'> <a f'> r <cis cis'> <d d'> <e e'> <f f'> <a a'>
  
  \break
  <g g'>4 <bes d>8 <a a'> r <gis cis e gis>4 <g g'>8 ~
  <g g'> <e e'> <cis cis'> <f f'> <e e'>4 g8 bes
  <d, d'>8 <f a> a d, <f, f'> <a d> \times 2/3 { <e e'>8 -> <a a'> -> <cis cis'> -> }
  <d d'>4 -^ \ottava #0 r4 r2
  R1
  
  \transpose ees dis \relative c'' {
    
    \key ees \minor
    
    \break
    <ees ees'>4 <aes bes>8 <d, d'> ~ <d d'> <aes' bes>4 <ees ees'>8 ~
    <ees ees'> <aes bes> <ces, ces'> <ees ees'> <d d'>4 <aes' bes>
    <ees ges bes c>4 r8 <bes ees ges>8 r4 <bes ees ges>4
    <bes ees ges>2 ~ <bes ees ges>8 <bes bes'> <aes aes'> <ges ges'>
    
    \break
    <ces ees aes>4 ces8 d ~ d4 <aes bes d>8 ees'
    <ces ees aes>4 ces8 ees d4 <d, f aes bes>8 <d f aes bes>
    <ees ees'>4 bes'8 bes <ges ges'>4 <ees ges bes c>8 <ees ges bes c>
    <ees ges bes c>2 ~ <ees ges bes c>8 <bes' bes'> <aes aes'> <ges ges'>
    
    \break
    <ees' ees'>4 ces'8 <d, d'>8 ~ <d d'>4 <aes' bes>
    <ces, ees aes>4 <ces ces'>8 <ees ees'> <d d'>4 <bes d fis bes>
    <d d'>8 ges <ges bes> <c, c'> ~ <c c'>4. <d d'>8 ~ 
    <d d'>4 <ges bes>8 <d d'> <c c'>4 <ges' bes>
    
    \break
    <ees ees'>4 ces'8 <d, d'>8 ~ <d d'> bes aes <ees ees'>
    <ces ees aes>4 <ces ces'>8 <ees ees'> <d d'>4 <bes d f bes>
    <ges' bes ees>4 -. r <des' ees ges> <des ees ges> -.
    <bes des ees ges> r8 <ees, ges bes> -^ <ees ees'> <bes' bes'> <aes aes'> <bes bes'>
    
    \break
    <ees ees'>4 ces'8 <d, d'>8 ~ <d d'>4 <aes bes>
    <ees' ees'>4 ces'8 <d, d'>8 ~ <d d'>4 <aes bes>
    <ees' ges bes c>2 <ees ges bes ees>4 <ees ges bes c> 
    <ees ges bes c> r <bes bes'>8 <aes aes'> <ges ges'> <ees ees'>
    
    \break
    <ees' ees'>4 ces'8 <d, d'>8 ~ <d d'>8 <aes' bes> <aes, aes'> <ees' ees'>
    r8 <ees ees'> <ces ces'> <ees ees'> <d d'>4 <aes' bes>
    <ges, bes c ees>4 -^ r8 <ges bes c ees> -^ r4 r8 <ges bes c ees> -> ~
    <ges bes c ees> <ges bes c ees> -> <ges bes c ees>4 -^ r2
    
  }
}

leftHand = \relative c' {
  
  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0	
  
  \clef bass
  \key d \minor
  
  \partial 4.
  r8 r r
  
  \ottava #1 <bes bes'>4 <d g>8 <a a'> ~ <a a'> <cis e>4 <bes bes'>8 ~
  <bes bes'>4 <d g>8 <a a'> <a a'>4 <cis e>8 <cis cis'>8 ~  
  <cis cis'>4 <f a>8 <b, b'> ~ <b b'> <c c'>4 <cis cis'>8 ~
  <cis cis'> <f a> <d d'> <ees ees'> <e e'> <f f'> <e e'> <ees ees'>
  
  <bes bes'>4 <d g>8 <a a'> ~ <a a'> <cis e>4 <bes bes'>8 ~
  <bes bes'>4 <d g>8 <a a'> <a a'>4 <cis e>4
  <e f a>2 \times 2/3 { <e f a>4 <e f a> <e f a> }
  <e f a>2 \ottava #0 r2
  
  d,2 cis
  d cis
  d2. e4 -> 
  f2. a4 ->
  
  e2 -> a,
  e'4. -> a,8 ~ a a a4
  d,2 \appoggiatura { c'16 cis } \times 2/3 { d4 -> e -> f -> }
  d4 -^ r4 r2
  
  e4. a,8 ~ a2
  e'2 a,4 a 
  d2 ~ d8 <d b'>4.
  <d d'>2 <d d'>4 <d d'>
  
  e2 a,
  e'4. a,8 ~ a2
  d2 d4 a
  <d, d'>4 -^ r r2
  
  c'8 c4 c8 ~ c4 c
  c2. c4
  f1 ~ 
  f
  
  a,2. a4 
  a2 a4. a8
  bes2 \times 2/3 { bes4 -> bes -> bes -> }
  a4 -^ r4 r2
  
  d2 cis
  d4. cis8 ~ cis2
  d2. e4 ->
  f1 ->
  
  e4. a,8 ~ a2
  e'4. a,8 -> ~ a a a4
  d,4. <d'' f a>8 -> <d f bes>8. -> <cis e a>16 -> ~ <cis e a>8 <cis e a>
  <b d f a>4 -^ r r2
  
  e,4. a8 ~ a2
  e4. r8 <a, g'>2
  d4 -. d8 -> d -. r a' -> ~ a [ d, ] -> ~
  d2. d4
  
  <g bes d>4 r8 <g a cis> ~ <g a cis>2
  <g bes d>2 a,4 a
  d4 e8 d -. f4 -> d8 a
  <d, d'>4 -^ r r2
  
  c'2 \times 2/3 { c'4 d ees }
  <bes c e?>2 c,
  f2 e'8. g16 ~ g8 f
  e4. a8 e c e dis
  
  e g, r <g, g'> r <g g'> r <g g'>
  <g g'>4 r8 <a a'> r2
  <bes bes'> \times 2/3 { <aes' bes d>4 <aes bes d> <aes bes d>4 }
  <g a cis>4 r r2
  
  R1*7
  r4 \makeClusters { a,, d' \change Staff = "rightHand" a' }
  
  \change Staff = "leftHand" 
  <d, d'>4 <g bes>8 <cis, cis'> ~ <cis cis'> <g' a> <d d'> <dis dis'>
  <e e'>4 <g bes>8 <d d'> <cis cis'>4 <g' a>
  <cis, cis'> <g' a>8 <b, b'> r <c c'> r <cis cis'> ~
  <cis cis'> <a f'> r <cis cis'> <d d'> <e e'> <f f'> <a a'>
  
  <g g'>4 <bes d>8 <a a'> r <gis cis e gis>4 <g g'>8 ~
  <g g'> e' cis <f, f'> <e e'>4 g8 bes
  <d, d'>8 <f a> <d d'>8 <f a> r4 \times 2/3 { e8 -> a -> cis -> }
  <d, d'>4 -^ r4 r2
  R1
  
  \transpose ees dis \relative {
    
    \key ees \minor
    
    <ees ees'>4 <aes bes>8 <d, d'> ~ <d d'> <aes' bes>4 <ees ees'>8 ~
    <ees ees'> <aes bes> ces ees d4 r
    <bes ees ges>4 <bes ees ges>8 r r <bes ees ges> r4
    <bes ees ges>2 ~ <bes ees ges>8 bes aes ges
    
    \break
    <f, f'>4 ces'8 d ~ d4 bes8 ees
    <f f'>4 ces8 ees d4 bes8 bes
    ees4 bes'8 bes ges4 <ees, ees'>8 <ees ees'>
    <ees ees'>2 ~ <ees ees'>8 bes'' aes ges
    
    \break
    <ees ees'>4 ces'8 <d, d'>8 ~ <d d'>4 <aes' bes>
    <f f'>4 <ces ces'>8 <ees ees'> <d d'>4 <bes bes'>
    <d d'>8 ges <ges bes> <c, c'> ~ <c c'>4. <d d'>8 ~ 
    <d d'>4 <ges bes>8 <d d'> <c c'>4 <ges' bes>
    
    \break
    <ees ees'>4 ces'8 <d, d'>8 ~ <d d'> bes' aes ees
    f4 ces8 ees d4 bes
    ees4 -. r8 ees r ees ees ees 
    ees4 r8 ees8 -^ r bes' aes bes
    
    \break
    <ees, ees'>4 ces'8 <d, d'>8 ~ <d d'>4 bes
    <ees ees'>4 ces'8 <d, d'>8 ~ <d d'>4 bes
    ees2. ees4
    ees r bes'8 aes ges ees
    
    \break
    <ees ees'>4 ces'8 <d, d'>8 ~ <d d'>8 <aes' bes> aes <ees ees'>
    r8 <ees ees'> ces' <ees, ees'> <d d'>4 bes
    ees4 -^ r8 ees -^ r4 r8 ees -> ~ 
    ees ees -> ees4 -^ r2
  }
}



% score
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
\score {
  <<
    \scoreMarkup
    \new PianoStaff <<
      \new Staff = "rightHand" << \transpose d bes, \rightHand >>
      \transpose d bes \scoreChords
      \new Staff = "leftHand" << \transpose d bes, \leftHand >>
    >>
  >>
}


