\version "2.18.2"

\include "lily-jazz/ignatzek-jazz-chords.ily"
\include "lily-jazz/lily-jazz.ily"





% PLUGINS 1.2
% -----------
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~





% multi-mark-engraver
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
% Aporta la posibilidad de pintar un rehearsal mark al final de una línea y 
% al comienzo de la siguiente.
% http://lists.gnu.org/archive/html/lilypond-user/2011-08/msg00157.html
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
#(define (multi-mark-engraver ctx)
   (let ((texts '())
         (final-texts '())
         (events '()))

     `((start-translation-timestep
        . ,(lambda (trans)
             (set! final-texts '())))

       (listeners
        (mark-event
         . ,(lambda (trans ev)
              (set! events (cons ev events)))))

       (acknowledgers
        (break-alignment-interface
         . ,(lambda (trans grob source)
              (for-each (lambda (mark)
                          (set! (ly:grob-parent mark X) grob))
                        texts))))

       (process-music
        . ,(lambda (trans)
             (for-each
              (lambda (ev)
                (let* ((mark-grob
                        (ly:engraver-make-grob trans 'RehearsalMark ev))
                       (label (ly:event-property ev 'label))
                       (formatter (ly:context-property ctx 'markFormatter)))

                  (if (and (procedure? formatter)
                           (not (markup? label)))
                      (begin
                       (if (not (number? label))
                           (set! label
                                 (ly:context-property ctx 'rehearsalMark)))

                       (if (and (integer? label)
                                (exact? label))
                           (set! (ly:context-property ctx 'rehearsalMark)
                                 (1+ label)))

                       (if (number? label)
                           (set! label (apply formatter (list label ctx)))
                           (ly:warning "rehearsalMark must have
integer value"))))

                  (if (markup? label)
                      (begin
                       (set! (ly:grob-property mark-grob 'text) label)
                       (let ((dir (ly:event-property ev 'direction)))
                         (and (ly:dir? dir)
                              (set! (ly:grob-property mark-grob 'direction)
                                    dir))))
                      (ly:warning "mark label must be a markup object"))

                  (set! texts (cons mark-grob texts))))
              (reverse events))))

       (stop-translation-timestep
        . ,(lambda (trans)
             (if (pair? texts)
                 (let ((staves (ly:context-property ctx 'stavesFound))
                       (priority-index 0))
                   (for-each (lambda (grob)
                               (let ((my-priority (ly:grob-property
grob 'outside-staff-priority 1500)))
                                 (for-each (lambda (stave)

(ly:pointer-group-interface::add-grob grob 'side-support-elements
                                               stave))
                                           staves)
                                 (set! (ly:grob-property grob
'outside-staff-priority) (+ my-priority priority-index))
                                 (set! priority-index (1+ priority-index))
                                 (set! final-texts (cons grob final-texts))))
                             (reverse texts))
                     (set! texts '())
                     (set! events '())))))

        (finalize
         . ,(lambda (trans)
              (and (pair? final-texts)
                   (for-each (lambda (grob)
                               (set! (ly:grob-property grob 'break-visibility)
                                     end-of-line-visible))
                             final-texts)))))))


% Funciones originales
%{
myMark =
#(define-music-function (parser location text) (markup?)
  (make-music 'MarkEvent
              'label text))
			  
toCoda = {
	\tweak #'self-alignment-X #RIGHT
	\tweak #'break-visibility #begin-of-line-invisible
	\myMark \markup { \hspace #1.25 \raise #1.25 \musicglyph #"scripts.coda" }
}
%}

% Función maqueada.
% Se apoya en el código obtenido por consola al ejecutar { \displayMusic { \toCoda }}
eolMark =
#(define-music-function (parser location text) (markup?)
	(make-music 
		'MarkEvent 
		'tweaks (list 
			(cons (quote self-alignment-X) 1)
			(cons (quote break-visibility) #(#t #t #f))
		) 
		'label (markup
			#:line (
				#:hspace 1.25
				#:raise 1.25
				text
			)
		)
	)
)










% rest-score y merge-rests-on-positioning
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
% Permite realizar la fusión de los silencios entre dos voces en un mismo pentagrama.
% https://code.google.com/p/lilypond/issues/detail?id=1228 (fichero merge-rests.ily)
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]

#(define (rest-score r)
  (let ((score 0)
	(yoff (ly:grob-property-data r 'Y-offset))
	(sp (ly:grob-property-data r 'staff-position)))
    (if (number? yoff)
	(set! score (+ score 2))
	(if (eq? yoff 'calculation-in-progress)
	    (set! score (- score 3))))
    (and (number? sp)
	 (<= 0 2 sp)
	 (set! score (+ score 2))
	 (set! score (- score (abs (- 1 sp)))))
    score))

#(define (merge-rests-on-positioning grob)
  (let* ((can-merge #f)
	 (elts (ly:grob-object grob 'elements))
	 (num-elts (and (ly:grob-array? elts)
			(ly:grob-array-length elts)))
	 (two-voice? (= num-elts 2)))
    (if two-voice?
	(let* ((v1-grob (ly:grob-array-ref elts 0))
	       (v2-grob (ly:grob-array-ref elts 1))
	       (v1-rest (ly:grob-object v1-grob 'rest))
	       (v2-rest (ly:grob-object v2-grob 'rest)))
	  (and
	   (ly:grob? v1-rest)
	   (ly:grob? v2-rest)
	   (let* ((v1-duration-log (ly:grob-property v1-rest 'duration-log))
		  (v2-duration-log (ly:grob-property v2-rest 'duration-log))
		  (v1-dot (ly:grob-object v1-rest 'dot))
		  (v2-dot (ly:grob-object v2-rest 'dot))
		  (v1-dot-count (and (ly:grob? v1-dot)
				     (ly:grob-property v1-dot 'dot-count -1)))
		  (v2-dot-count (and (ly:grob? v2-dot)
				     (ly:grob-property v2-dot 'dot-count -1))))
	     (set! can-merge
		   (and
		    (number? v1-duration-log)
		    (number? v2-duration-log)
		    (= v1-duration-log v2-duration-log)
		    (eq? v1-dot-count v2-dot-count)))
	     (if can-merge
		 ;; keep the rest that looks best:
		 (let* ((keep-v1? (>= (rest-score v1-rest)
				      (rest-score v2-rest)))
			(rest-to-keep (if keep-v1? v1-rest v2-rest))
			(dot-to-kill (if keep-v1? v2-dot v1-dot)))
		   ;; uncomment if you're curious of which rest was chosen:
		   ;;(ly:grob-set-property! v1-rest 'color green)
		   ;;(ly:grob-set-property! v2-rest 'color blue)
		   (ly:grob-suicide! (if keep-v1? v2-rest v1-rest))
		   (if (ly:grob? dot-to-kill)
		       (ly:grob-suicide! dot-to-kill))
		   (ly:grob-set-property! rest-to-keep 'direction 0)
		   (ly:rest::y-offset-callback rest-to-keep)))))))
    (if can-merge
	#t
	(ly:rest-collision::calc-positioning-done grob))))










% startParenthesis y endParenthesis
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
% http://comments.gmane.org/gmane.comp.gnu.lilypond.general/64087
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]

% ELIMINADO










% bendBefore
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
% Adaptación
% http://lilypondblog.org/2013/08/adding-ornamentations-to-note-heads-part-1/
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]

%%%
%%% Utilities for defining new grobs, grob properties and music event types
%%% (there should be built-in commands to do that in LilyPond)
%%%
#(define (define-grob-type grob-name grob-entry)
   (let* ((meta-entry   (assoc-get 'meta grob-entry))
          (class        (assoc-get 'class meta-entry))
          (ifaces-entry (assoc-get 'interfaces meta-entry)))
     (set-object-property! grob-name 'translation-type? list?)
     (set-object-property! grob-name 'is-grob? #t)
     (set! ifaces-entry (append (case class
                                  ((Item) '(item-interface))
                                  ((Spanner) '(spanner-interface))
                                  ((Paper_column) '((item-interface
                                                     paper-column-interface)))
                                  ((System) '((system-interface
                                               spanner-interface)))
                                  (else '(unknown-interface)))
                                ifaces-entry))
     (set! ifaces-entry (uniq-list (sort ifaces-entry symbol<?)))
     (set! ifaces-entry (cons 'grob-interface ifaces-entry))
     (set! meta-entry (assoc-set! meta-entry 'name grob-name))
     (set! meta-entry (assoc-set! meta-entry 'interfaces
                                  ifaces-entry))
     (set! grob-entry (assoc-set! grob-entry 'meta meta-entry))
     (set! all-grob-descriptions
           (cons (cons grob-name grob-entry)
                 all-grob-descriptions))))

#(define-public (define-grob-property symbol type? description)
  (set-object-property! symbol 'backend-type? type?)
  (set-object-property! symbol 'backend-doc description)
  symbol)

#(defmacro*-public define-music-type (type-name parents #:rest properties)
   (let ((gproperties (gensym "properties")))
     `(let ((,gproperties (list-copy ',properties)))
        ,@(map (lambda (parent)
                `(define-event-class ',(ly:camel-case->lisp-identifier type-name)
                   ',parent))
               parents)
        (set-object-property! ',type-name
                              'music-description
                              (cdr (assq 'description ,gproperties)))
        (set! ,gproperties (assoc-set! ,gproperties 'name ',type-name))
        (set! ,gproperties (assq-remove! ,gproperties 'description))
        (hashq-set! music-name-to-property-table ',type-name ,gproperties)
        (set! music-descriptions
              (cons (cons ',type-name ,gproperties)
                    music-descriptions)))))

%%%
%%% Grob definition
%%%
#(define (head-ornamentation::print me)
   (let* ((notes (ly:grob-object me 'elements))
          (y-ref (ly:grob-common-refpoint-of-array me notes Y))
          (x-ref (ly:grob-common-refpoint-of-array me notes X))
          (x-ext (ly:relative-group-extent notes x-ref X))
          (y-ext (ly:relative-group-extent notes y-ref Y))
          (y-coord (interval-center y-ext))
          (text (ly:text-interface::print me))
          (width (/ (interval-length (ly:stencil-extent text X)) 2.0))
          (x-coord (- (car x-ext) width)))
     (ly:stencil-translate
      text
      (cons
       (- (- x-coord (ly:grob-relative-coordinate me x-ref X)) .7)
       (- (- y-coord (ly:grob-relative-coordinate me y-ref Y)) 1.5)))))

#(define-grob-type 'HeadOrnamentation
  `((text . ,#{ \markup \fontsize #-4 \musicglyph #"brackettips.up" #})
    (stencil . ,head-ornamentation::print)
    (meta . ((class . Item)
             (interfaces . (font-interface))))))

\layout {
  \context {
    \Global
    \grobdescriptions #all-grob-descriptions
  }
}

%%%
%%% Engraver
%%%
%%% The head-ornamentation engraver, with its note-head acknowledger
%%% (which add HeadOrnamentation grobs to note heads)
#(define head-ornamentation-engraver
   (make-engraver
    (acknowledgers
     ((note-head-interface engraver note-grob source)
      ;; When the note-head event attached to the note-head grob has
      ;; ornamentation events among its articulations, then create a
      ;; HeadOrnamentation grob
      (let* ((note-event (ly:grob-property note-grob 'cause)))
        (for-each
         (lambda (articulation)
           (if (memq 'head-ornamentation-event
                     (ly:event-property articulation 'class))
               (let ((ornament-grob (ly:engraver-make-grob engraver
                                                           'HeadOrnamentation
                                                           note-grob)))
                 (ly:pointer-group-interface::add-grob ornament-grob
                                                       'elements
                                                       note-grob)
                 (set! (ly:grob-parent ornament-grob Y) note-grob)
                 (set! (ly:grob-property ornament-grob 'font-size)
                       (+ (ly:grob-property ornament-grob 'font-size 0.0)
                          (ly:grob-property note-grob 'font-size 0.0))))))
         (ly:event-property note-event 'articulations)))))))

\layout {
  \context {
    \Voice
    \consists #head-ornamentation-engraver
  }
}

%%%
%%% Event type definition
%%%
#(define-music-type HeadOrnamentationEvent (music-event)
   (description . "Print an ornamentation at a note head side")
   (types . (general-music post-event event head-ornamentation-event)))

bendBefore = #(make-music 'HeadOrnamentationEvent)










% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
% Funciones propias
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]





% Índice con borde circular y un cómodo espacio interior (y no el que viene por defecto)
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
#(define-markup-command (boxindex layout props text) (markup?)
	(interpret-markup layout props
		(markup #:huge #:override '(box-padding . 0.5) #:box text)
	)
)

#(define-markup-command (boxed layout props text) (markup?)
	(interpret-markup layout props
		(markup #:huge #:override '(box-padding . 0.9) #:box text)
	)
)

% Ritardando
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
rit =
#(make-music 'CrescendoEvent
             'span-direction START
             'span-type 'text
             'span-text "rit.")

% Barras de improvisacion
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
improOn = {
  \override Rest.stencil = #ly:text-interface::print
  \override Rest.text = \markup \jazzglyph #"noteheads.s2slashjazz"
  \override NoteHead.stencil = #jazz-slashednotehead
}

improOff = {
  \revert Rest.stencil
  \revert Rest.text
  \revert NoteHead.stencil
}










% Añadido de funciones al contexto
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
\layout {
  \context {
    \Score		
    
    % multi-mark-engraver
    \remove "Mark_engraver"
    \consists #multi-mark-engraver
    \consists "Tweak_engraver"
    
    % merge-rests-on-positioning
    \override RestCollision #'positioning-done = #merge-rests-on-positioning
    
    % aspecto de los silencios multicompás
    \override MultiMeasureRest.expand-limit = #1
    
    % margen extra de las articulaciones para evitar la colisión con las ligaduras
    \override Script.padding = #0.5
    \override Script.avoid-slur = #'inside
    
    % glissando bonito
    \override Glissando.style = #'trill
    \override Glissando.bound-details.left.padding = #1.7
    
    % dinámicas un poco mas gruesas
    \override Hairpin.thickness = #1.75
    
    % Esconde la cancelacion de accidentales en el cambio de armadura
    % \override KeyCancellation.break-visibility = #'#(#f #f #f)
  }
}












% LAYOUT
% ------
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~






\header {
  title = "Jealousie Tango"
  instrument = "Piano"
  composer = "Jacob Gade"
  arranger = ""
  poet = \markup \tiny "Reducción y edición: Gabriel PALACIOS"
  meter = \markup \tiny "gabriel.ps.ms@gmail.com"
  tagline = \markup {
    \center-column {
      \line { \tiny "Reducción y edición: Gabriel PALACIOS" }
      \line { \tiny "gabriel.ps.ms@gmail.com" }
    }
  }
}

\paper {
  % El definir otro tipo de fuente no permite cambiar el tamaño global del staff. Es un bug conocigo
  #(define fonts (make-pango-font-tree "LilyJAZZText" "Nimbus Sans" "Luxi Mono" (/ 20 20)))
  
  % distancia entre la parte superior de la página y el título
  top-markup-spacing #'basic-distance = 5
  
  % distancia entre el título y el primer sistema
  markup-system-spacing #'basic-distance = 25
  
  % distancia entre el texto y el primer sistema 
  top-system-spacing #'basic-distance = 15
  
  % márgen inferior
  bottom-margin = 20
    
  % número de páginas
  % page-count = 1
  
  ragged-last-bottom = ##f 
  
  oddHeaderMarkup = \markup {
    \on-the-fly #not-first-page \fill-line {
      \fromproperty #'page:page-number-string
      \fromproperty #'header:title
      \fromproperty #'header:instrument
    }
  }
  
  evenHeaderMarkup = \markup {
    \on-the-fly #not-first-page \fill-line {
      \fromproperty #'header:instrument
      \fromproperty #'header:title
      \fromproperty #'page:page-number-string
    }
  }
}










% MUSIC PARTS
% -----------
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~





% scoreMarkup
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
scoreMarkup = \new Staff \with {
   \RemoveEmptyStaves
}
{
  % Oculta el primer pentagrama del markup
  \override Staff.VerticalAxisGroup #'remove-first = ##t
  
  % Indicación de tiempo
  \once \override Score.RehearsalMark.X-offset = #5
  \mark \markup \normalsize "(Count 1, 2, 3, 4 into bar 1)"
  \mark \jazzTempoMarkup "Tango" c4 "80"
}





% scoreChords
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
scoreChords = \chords {
  
  \set minorChordModifier = \markup {"-"}
  
  s1*32
  
  \break
  d2:6
  d2:6/fis
  a:7/e
  a:7
  
  \break
  a:7/e
  a4:7 a4:7/g
  fis2:m 
  e4:m7 a:7
  
  \break
  d2:6
  d2:7
  g:6
  g:m6
  
  \break
  d2/a
  a4:7/e a:7
  d2:6
  d4:6 a:7
  
  \break
  d2:6
  d2:6/fis
  a:7/e
  a:7
  
  \break
  a:7/e
  a4:7 a4:7/g
  fis2:m 
  e4:m7 a:7
  
  \break
  d2:6
  d2:7
  g:6
  g:m6
  
  \break
  d2/fis
  e4:m7 a
}





% music
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
rightHand = \relative c' {

  % Para pintar todos los números de compás debajo del pentagrama
  % \override Score.BarNumber #'break-visibility = #end-of-line-invisible
  % \override Score.BarNumber #'direction = #DOWN
  % \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0
  
  \key d \minor
  
  r2
  r2
  r2 
  r8. <f a d>16 ( <f a d>8 -^ ) <g bes cis e> -^ 
  
  \break
  <a d f>4 -^  r4 
  r2
  r2
  r8. <g bes d g>16 ( <g bes d g>8 -^ ) <a d fis a> -^ 
  
  \break
  <bes d g bes>4 -^  r4
  R1*7*1/2
  
  \break
  r8. <a, d f>16 <d f a>4 ~ 
  <d f a>4 <f a d>4
  r8. <a, d f a>16 <a d f a>4 ~ 
  <a d f a>8 <d f a> <cis f a> <c f a>
  
  \break
  <bes d g bes>8. bes16 <d e>8 bes 
  <d e>4 <a e' a>8 a
  <a d f>8. <f a d>16 <a d f>8 <f a d> 
  << { r16 a cis [ <cis e> ] r8 <cis e> } \\ { r16 <g a>8 <g a>16 r16 a ~ a8  } >>
  
  \break
  <a d f>8. <a d f>16 <d f a>4 ~ 
  <d f a>8 a <d f> a
  r8. <a d f>16 <d f a>8 -. r 
  r8 <d f> <d g> <d gis>
  
  \break
  << { a'2 } \\ { r8. <c, f>16 <c e>8 <c e> } >>
  r8 <f, a d> r <gis d'>
  a4 r4
  r2
  
  \break
  <cis e a>8. \p <cis e a>16 <cis e a>8 -. <cis e a> -.
  <a' cis e>8. <a cis e>16 <a cis e>8 -. <a cis e> -.
  <a d f a>8. <a d f a>16 <a d f a>8 -. <a d f a> -.
  <a d f a>8. <a d f a>16 <a d f a>8 -. <a d f a> -.
  
  \break
  <d fis a d>8. <d fis a d>16 <d fis a d>8 -. <d fis a d> -.
  <d fis a d>8. <d fis a d>16 <d fis a d>8 -. <d fis a d> -.
  <d g bes d>8. <d g bes d>16 <d g bes d>8 -. <d g bes d> -.
  <d g bes d>8. \< <d g bes d>16 <d g bes d>8 -. <d g bes d> -. \!
  
  \break
  r8. \f <g,, bes d e>16 <g bes d e>8 -> <g bes d f> ->
  << 
    { <g bes d g>8. -> <g' bes d e>16 <g bes d e>8 -> <g bes d f> ->
      <g bes d g>4 -> r } \\
    { s8. <bes, d>16 <bes d>8 <bes d>8
      <bes d>4 -> r }
  >>
  r8. <g' bes d e>16 <g bes d e>8 -> <g bes d g> ->
  
  \break
  <f a d f>4 -> r8 <f a d> ->
  <e g bes d>4 -> r4
  <cis e a cis>8 -> r \appoggiatura { a16 b cis d } e4
  \appoggiatura { e16 f g a } bes a r8 r4
  
  \break
  r8. <d, fis>16 <d fis>8 <d fis>
  r8. <d fis>16 <d cis'>8 <d fis cis'>
  r8. <e cis'>16 <g e'>8 <g cis e>
  r8. <a cis>16 <a cis e>8 <a cis a'>
  
  \break
  r8. <cis, a'>16 <e a cis>8 <e a cis>
  r8. <a, e'>16 <cis e a>8 <e a cis>
  <e a e'>8. <d d'>16 <b b'>8 <d d'>
  <cis cis'>16 <b b'> <a a'>8 ~ <a a'> <cis e g cis>
  
  \break
  r8. <d a'>16 <fis a d>8 <fis a d>
  r8. <d f b d>16 <d f b d>8 <d f b d>
  << 
    { a'2 
      dis,4 e8 cis
    
      b'4 a8 g
      \times 2/3 { b g b } a g
    } \\
    
    { a8. <g, cis>16 <a cis e>8 <a cis e> 
      dis4 cis
    
      g'8. cis,16 cis4
      \times 2/3 { r8 <e g>4 } <cis e>4
    }
  >>
  r16 <a d fis>8 <f a d>16 <a d fis>8 <fis a d fis>
  <f a cis f>8. <f a cis f>16 <f a cis f>8 <f a cis f>
  
  \improOn \repeat unfold 16 { bes'16 bes8 bes16 bes8 bes8 }
  \repeat unfold 11 { bes16 bes8 bes16 bes8 bes8 }
  bes4 \rit bes
  b2 
  b4 b \! \improOff
  
  d,16 ^"A tempo" e fis g a b c cis 
  <d, d'> <e e'> <fis fis'> <g g'> <a a'> <b b'> <c c'> <cis cis'>
  <d d'>4 -^ <a cis e g a> -^
  <d fis a d>4 -^ r
}

leftHand = \relative c, {
  
  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0	
  
  \clef bass
  \key d \minor
  \time 2/4
  
  r2
  r2 
  r2
  r8. <d a' d>16 ( <d a' d>8 -^ ) <d a' d> -^ 
  
  <d a' d>4 -^  r4 
  r2
  r2 
  r8. <g d' g>16 ( <g d' g>8 -^ ) <d a' d> -^ 
  
  <g, d' g>4 -^  r4
  R1*7*1/2
  
  <d' a'>4 -. r8 a' -. 
  <d, d'>4. a'8 -.
  d,4 -. r8 a' -. 
  d,4 -. r
  
  << 
    { g8. d'16 g8 d 
      g, [ d' ] <a e'> <a e'> 
      f8. d'16 f8 d 
      e,4 -> a8. a,16 } \\
    { g'2 
      g4 a 
      f2 
      s2}
  >>
  
  <d, d'>4 -. r8 a'' -. 
  d,8. a'16 d8 a
  d,8. a'16 d8 a 
  d,4 r
  
  a'4. e8
  <b b'>4 e
  a8. e'16 f8 -. e -.
  a,8. ( e'16 a8 e )
  
  a,8. e'16 f8 -. e -.
  << 
    { <g>8. g16 a8 e } \\
    { <a,>2 }
  >>
  e'4 ~ e16 [ d ] \times 2/3 { f e d }
  d2
  
  <d, a' d>4 \arpeggio a''8 bes 
  \appoggiatura { d,16 } c'4 d8 a
  \appoggiatura { g,16 } a'4 ~ a16 [ g ] \times 2/3 { bes a g }
  g2
  
  \break
  \appoggiatura { g,16 a } bes2
  \appoggiatura { f 16 } bes2
  <bes, bes'>4. ~ <bes bes'>16 <bes' d g>16
  <bes d g>4 <bes d g>8 <bes d g> 
  
  <f d' f>4 r8 <f f'>
  <e e'>4 r
  \appoggiatura { e16 } <a, a'>4 r4
  r2
  
  << 
    { d8. fis'16 b8 d,
      fis,8. d'16 a'8 d,
      e,8. e'16 a8 a
      a,8. e'16 a8 a 
    
      e,8. e'16 a8 a
      a,8. e'16 a8 e 
      d,8. a'16 fis'8 a,
      e8. e'16 a8 a,
      
      fis8. d'16 fis8 d
      f,?8. f'16 f8 f
      e,8. e'16 a8 e
      a,8. e'16 a8 e 
      
      e,8. e'16 a8 e
      a,8. e'16 a8 e 
      d,8. a'16 d8 a } \\
    
    { d,2      
      <fis fis'>2
      <e e'>
      a 
      
      <e e'>
      a
      d,2
      e2
      
      <fis fis'>2
      <f? f'?>2
      <e e'>2
      a
      
      <e e'>2
      a 
      d,}
  >>
  <a a'>16 <a a'>8 <a a'>16 <a a'>8 <a a'>
  
  \improOn \repeat unfold 15 { d'8. d16 d8 d8 } d8. \< d16 d8 d8
  d8. \ff d16 d8 d8 \repeat unfold 10 { d8. d16 d8 d8 } d4 d
  d2 
  d4 d \improOff
  
  d16 e fis g a b c cis 
  <d,, d'> <e e'> <fis fis'> <g g'> <a a'> <b b'> <c c'> <cis cis'>
  <d d'>4 -^ <a e' a> -^
  <d, d'>4 -^ r
  
  \bar "|."
}



% score
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
\score {
  <<
    \scoreMarkup
    \new PianoStaff <<
      \new Staff = "rightHand" << \rightHand >>
      \scoreChords
      \new Staff = "leftHand" << \leftHand >>
    >>
  >>
}


