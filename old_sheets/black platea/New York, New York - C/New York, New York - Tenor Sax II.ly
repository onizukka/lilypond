\version "2.18.2"


% Descripción de funcionalidades
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% PLUGINS
% - \eolMark:					mark al final de línea
% - \bendBefore: 				solución al bendBefore (i.e. dis8 \bendBefore)
% - \rit: 						crea una porción de ritardando que debe terminar como las dinámicas, con un \!
% - \improOn, \improOff:	negra de improvisación
% - \emptyStaff:				Imprime un pentagrama vacío
% - \emptyBar:					Imprime un compás invisible (útil para indentados)



% LAYOUT
% - El aspecto de los silencios multicompás es como los de las partituras de jazz
% - Margen extra de las articulaciones para evitar la colisión con las ligaduras
% - Glissandos bonitos



% PAPER
% - Se imprime el título, instrumento y número de página en todas las páginas
% - Solo se debe tocar el page-count (si eso). Sale bonito tal cual.

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Descripción de funcionalidades
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% 03-09-14: Layout
% Ahora las articulaciones van por dentro de las ligaduras. Parece que es la mejor solución



% 04-09-14: Layout
% Los creciendos y decreciendos tienen la línea un poco más gruesa
%
% Markup
% Añadidas guías para correcta posición y markup de la indicación de tempo
%
% Plugins
% Añadida función \emptyStave para imprimir un pentagrama vacío.
% En algunas partituras hay un pentagrama vacío antes del coda (lo cual mola bastante, por cierto)



% 05-09-14: Plugins
% Añadido a la función \emptyStave el esconder la armadura de la clave, que no estaba.



% 08-09-14: Layout
% Ahora no se muestra la cancelación de accidentales al haber un cambio de armadura



% 09-09-14: Chords
% Añadida sentencia para que los acordes menores salgan con un guión en vez de con una M minúscula



% 10-09-14: Plugins
% Mejorada la función \bendAfter.
% Ahora es genérica, y se puede definir cualquier texto en un lateral que haga las funciones de ornamento



% 15-09-14: Layout
% Añadido un poco más de margen a los reguladores



% 16-09-14: Plugins
% Mejorada la función para imprimir barras de improvisación.
% Antes se llamaba al comando \rs para pintar una barra que ocupaba una negra
% Se reemplaza \rs por \restImproOn y \restImproOff, siendo esta opción mucho más flexible.
% Cuando se activa \restImproOn, todo silencio se convierte en barra de improvisación, respetando su duración.



% 20-09-14: Plugins
% Mejorada la función para imprimir barras de improvisación.
% Se reemplaza \restImproOn y \restImproOff por \improOn y \improOff
% Ahora, también las notas del pentagrama salen como barra de improvisación.
% (los comandos \improvisationOn e \improvisationOff de lilypond no se llevan bien con LilyJAZZ)
% Se ha añadido un grob correspondiente a los slash de la fuente LilyJAZZ en el archivo lily-jazz.ly



% 20-09-14: Layout
% Comentada la línea que oculta la cancelación de accidentales al haber cambio de armadura.
% Realmente, la cancelación es muy bienvenida.



% 01-10-14: Plugins
% Renombrada la función \emptyStave a \emptyStaff (stave es el plural de staff) y 
% corregida (ya no muestra el número de compás).
% Recordar que después de usar \emptyStaff se necesita redefinir el 
% número de compás con \set Score.currentBarNumber = #xx
%
% Añadida la función \emptyBar que oculta un compás entero. Se puede usar para crear un 
% efecto de indentación en un pentagrama dado.
% Recordar que después de usar \emptyBar se necesita:
% - redefinir el número de compás con \set Score.currentBarNumber = #xx
% - volver a pintar la clave con \clef
% - volver a pintar la armadura con \key

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Dependencias
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% \includeOnce
% Incluye una dependencia solo una vez. Así se pueden incluir los archivos en otros 
% (para montar un score por ejemplo) sin liarla parda con dependencias cíclicas.

includeOnce = #(
	define-void-function (parser location filename) (string?) (
		if (not (defined? (string->symbol filename))) (
			begin (
				ly:parser-include-string parser (
					string-concatenate (list "\\include \"" filename "\"")
				)
			)
			(primitive-eval (list 'define (string->symbol filename) #t))
		)
	)
)



% LilyJAZZ
\includeOnce "includes/lily-jazz.ily"
\includeOnce "includes/ignatzek-jazz-chords.ily"



% Plugins
\includeOnce "includes/plugins-1.2.ily"

% Language
\language "english"

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~











% Disposición
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Cabecera
\header {
	title = "New York, New York"
	instrument = "Tenor Sax II"
	composer = "John KANDER & Fred EBB"
	arranger = "Arr: Philippe MARILLIA"
	poet = \markup \tiny "Reducción y edición: Gabriel PALACIOS"
	meter = \markup \tiny "gabriel.ps.ms@gmail.com"
	tagline = \markup {
  	\center-column {
    	\line { \tiny "Reducción y edición: Gabriel PALACIOS" }
    	\line { \tiny "gabriel.ps.ms@gmail.com" }
  	}
	}
}



% Márgenes, fuentes y distancias del papel
\paper {
	% El definir otro tipo de fuente no permite cambiar el tamaño global del staff. Es un bug conocigo
	#(define fonts (make-pango-font-tree "LilyJAZZText" "Nimbus Sans" "Luxi Mono" (/ 20 20)))

	% distancia entre la parte superior de la página y el título
	top-markup-spacing #'basic-distance = 5

	% distancia entre el título y el primer sistema
	markup-system-spacing #'basic-distance = 25

	% distancia entre el texto y el primer sistema 
	top-system-spacing #'basic-distance = 15

	% márgen inferior
	bottom-margin = 20
	
	% intentado del primer sistema
	indent = 0
	 
	% número de páginas
	% page-count = 1

	ragged-last-bottom = ##f 

	oddHeaderMarkup = \markup {
	 \on-the-fly #not-first-page \fill-line {
		\fromproperty #'page:page-number-string
		\fromproperty #'header:title
		\fromproperty #'header:instrument
	 }
	}

	evenHeaderMarkup = \markup {
	 \on-the-fly #not-first-page \fill-line {
		\fromproperty #'header:instrument
		\fromproperty #'header:title
		\fromproperty #'page:page-number-string
	 }
	}
}

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~







% Elementos comunes de la partitura
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
	% Oculta el primer pentagrama del markup
	\override Staff.VerticalAxisGroup #'remove-first = ##t

	% Indicación de tiempo
	\once \override Score.RehearsalMark.X-offset = #5
	\mark \jazzTempoMarkup "Swing" c4 "112"
	s1*8
	
	\bar "||"
	\mark \markup \boxed "9"
	s1*24
	
	\bar "||"
	\mark \markup \boxed "33"
	s1*18
	
	\bar "||"
	\mark \markup \boxed "51"
	s1*13
	
	\bar "||"
	\once \override Score.RehearsalMark.X-offset = #5
	\mark \markup \boxed "64"
	\mark \jazzTempoMarkup "Swing" c4 "80"
	s1*11
	
	\once \override Score.RehearsalMark.X-offset = #0
	\mark \jazzTempoMarkup "Swing" c4 "112"
	s1
	
	\bar "||"
	\mark \markup \boxed "76"
	s1*7
	
	\bar "|."
}



% Función musical global
global = {
	% Para pintar todos los números de compás debajo del pentagrama
	% \override Score.BarNumber #'break-visibility = #end-of-line-invisible
	% \override Score.BarNumber #'direction = #DOWN
	% \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

	\jazzOn
	\compressFullBarRests
	
	\override Score.KeyCancellation.break-visibility = #all-invisible
	
	\set Timing.beamExceptions = #'()
	\set Timing.baseMoment = #(ly:make-moment 1/4)
	\set Timing.beatStructure = #'(1 1 1 1) 
	\set Score.explicitKeySignatureVisibility = #begin-of-line-visible
}

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Elementos específicos del instrumento
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Cifrado
scoreChords = \chords {    
	\set minorChordModifier = \markup { "-" }
	
	% c1:m f2:m g2:m7
}

% Música
tenorSaxII = \relative c'' {
  
	\global
	
	% 00: Alto I, Bone I, II, III, Bari
	\relative c'' {
		
		\key d \major
		
		a4 -^ \f a -^ cs8 ( d ) b4 -.
		a4 -^ a -^ cs8 ( d ) b4 -.
		g4 -^ g -^ cs8 ( d ) b4 -.
		g4 -^ g -^ cs8 ( d ) b4 -.
		
		\break
		d4 -^ \p d -^ cs8 ( d ) b4 -.
		d4 -^ d -^ cs8 ( d ) b4 -.
		d4 -^ d -^ cs8 ( d ) b4 -.
		g8 -^ r r4 r2
	}
	
	% 09: Saxs
	\relative c'' {
		
		\key d \major
		
		\break
		R1*6
		
		r8 \mf d -> r b cs d -> b cs 
		d g4. -> g,4 ( -- \> g -- ) \!
		
		\break
		a4 -. \p a8 e fs a -. r fs (
		a fs a b ~ b2 )
		r8 a -> r e ( a b -> a e )
		a g4. -> g4 -. g -.
		
		\break
		fs4 -. r8 b ~ b d b fs ~
		fs4 r fs2 ( -- \< 
		fs2. -- ) \mf d8 fs ~
		fs4 fs ( e d
		
		d2 ) \> r2 \!
		R1*3
		
		\break
		fs2 r8 fs -> r cs'
		a a4. a2
		r8 g -> r g g g g fs 
		g a ~ a2. \>
	}
	
	% 33: Trumpet I, II, Bone I, II, IV
	\relative c'' {
		
		\key d \major
		
		\break
		r4 \mp \! d8 ( cs ~ cs4 a4 -. )
		cs8 ( b4. as8 b -> ) r4
		r4 d8 ( cs ~ cs e c b ~ 
		b4 )
	}
	
	% 36: Trumpet I, II, Alto I, II, Tenor II
	\transpose bf ef' \relative c' {
		
		\key a \major
		
		e4 ( \p fs gs 
		
		\break
		a4 gs8 a ~ a gs a4 -. )
	}
	
	% 38: Trumpet I, II, Bone I, II, IV
	% 41: Alto I, II, Bones I, II, Bari
	\relative c'' {
		
		\key d \major
		
		r4 a ( \< a g \!
		g4. ) g8 -. c c c4 -.
		c -^ r4 r2
		
		\break
		r4 b -^ b2 (
		as ) as4 ( as -. )
		r4 cs -^ b ~ b8 c ~
		c4 c -. r2
		
		\break
		R1*2
	}
	
	% 47: Trumpets, Bari
	% 50: Trumpets, Bone IV
	\transpose bf ef' \relative c'' {
		
		\key a \major
		
		r4 cs -. b8 ( cs ) a4 -.
		cs -. cs -. b8 ( cs ) a4 -.
		b -. b -. b8 ( b a b )
		c4 -. c8 ( \< c c c ) ef4 -. \!
		
		\set Score.explicitKeySignatureVisibility = #all-visible
		\once \override Score.KeyCancellation.break-visibility = #end-of-line-visible
		\key bf \major
		
		\break
		f4 -. \mf e8 ( f ~ f e ) f4 -.
		f8 g ~ g2.
		r8 f -> r ef d4 ( a )
		af4 -^ r4 r2
	}
	
	% 55: Alto I, Tenor I, Bones I, II, IV
	\relative c' {
		
		\set Score.explicitKeySignatureVisibility = #begin-of-line-visible
		\key ef \major
		
		\break
		r4 r8 ef -> r2
		r4 r8 ef -> r2
	}
	
	% 57: Alto I, II, Trumpet I, II, IV
	\relative c'' {
		
		\key ef \major
		
		r4 ef8 g ~ g ef d4 -.
		g4 -^
	}
	
	% 58: Saxs
	\relative c'' {
		
		\key ef \major
		
		\break
		g ( af bf
		
		\break
		bf1 )
	}
	
	% 60: Trumpet I, II, Alto II, Tenor II, Bari
	\relative c'' {
		
		\key bf \major
		
		g4 -^ r r8 g8 ( -- g4 -. )
		gf4 -^ r4 gf2 (
		f ) \times 2/3 { r4 f -> f -> }
		f4 -^ r4 r2
	}
	
	% 64: Saxs
	\relative c' {
		
		\key ef \major
		
		\break
		r4 bf8 ( bf ~ bf bf bf bf
		c c ~ c2. )
		r4 bf8 ( bf bf bf bf4 -. )
		c4 -^ r r2
	}
	
	% 68: Trumpet I, II, Tenor I, II, Bari
	\relative c'' {
		
		\key ef \major
		
		\break
		bf2 ( c
		d ef
		f1 )
		r4 bf ( af g
		
		\break
		ef1
		f2. ) r4
		g1
		e4 -^ \improOn r4 r r \improOff
		
		\break
		r4 f, -^ r g -^
		r af -^ r af -^
		bf -. bf -. bf8 ( bf ) bf4 -.
		bf -. bf -. bf8 ( bf ) bf4 -.
		\break
		f -. f -. f8 ( f ) f4 -.
		b,1 \fermata
		ef4 -^ r r2
	}
}

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Impresión de la partitura
% (Sólo se imprime el primer book que encuentra Lilypond!!)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

\book {
	\score {
		<<
			\scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\new Staff { \tenorSaxII }
			>>
		>>
	}
}


