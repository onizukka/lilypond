\version "2.18.2"


% Descripción de funcionalidades
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% PLUGINS
% - \eolMark:					mark al final de línea
% - \bendBefore: 				solución al bendBefore (i.e. dis8 \bendBefore)
% - \rit: 						crea una porción de ritardando que debe terminar como las dinámicas, con un \!
% - \improOn, \improOff:	negra de improvisación
% - \emptyStaff:				Imprime un pentagrama vacío
% - \emptyBar:					Imprime un compás invisible (útil para indentados)



% LAYOUT
% - El aspecto de los silencios multicompás es como los de las partituras de jazz
% - Margen extra de las articulaciones para evitar la colisión con las ligaduras
% - Glissandos bonitos



% PAPER
% - Se imprime el título, instrumento y número de página en todas las páginas
% - Solo se debe tocar el page-count (si eso). Sale bonito tal cual.

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Descripción de funcionalidades
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% 03-09-14: Layout
% Ahora las articulaciones van por dentro de las ligaduras. Parece que es la mejor solución



% 04-09-14: Layout
% Los creciendos y decreciendos tienen la línea un poco más gruesa
%
% Markup
% Añadidas guías para correcta posición y markup de la indicación de tempo
%
% Plugins
% Añadida función \emptyStave para imprimir un pentagrama vacío.
% En algunas partituras hay un pentagrama vacío antes del coda (lo cual mola bastante, por cierto)



% 05-09-14: Plugins
% Añadido a la función \emptyStave el esconder la armadura de la clave, que no estaba.



% 08-09-14: Layout
% Ahora no se muestra la cancelación de accidentales al haber un cambio de armadura



% 09-09-14: Chords
% Añadida sentencia para que los acordes menores salgan con un guión en vez de con una M minúscula



% 10-09-14: Plugins
% Mejorada la función \bendAfter.
% Ahora es genérica, y se puede definir cualquier texto en un lateral que haga las funciones de ornamento



% 15-09-14: Layout
% Añadido un poco más de margen a los reguladores



% 16-09-14: Plugins
% Mejorada la función para imprimir barras de improvisación.
% Antes se llamaba al comando \rs para pintar una barra que ocupaba una negra
% Se reemplaza \rs por \restImproOn y \restImproOff, siendo esta opción mucho más flexible.
% Cuando se activa \restImproOn, todo silencio se convierte en barra de improvisación, respetando su duración.



% 20-09-14: Plugins
% Mejorada la función para imprimir barras de improvisación.
% Se reemplaza \restImproOn y \restImproOff por \improOn y \improOff
% Ahora, también las notas del pentagrama salen como barra de improvisación.
% (los comandos \improvisationOn e \improvisationOff de lilypond no se llevan bien con LilyJAZZ)
% Se ha añadido un grob correspondiente a los slash de la fuente LilyJAZZ en el archivo lily-jazz.ly



% 20-09-14: Layout
% Comentada la línea que oculta la cancelación de accidentales al haber cambio de armadura.
% Realmente, la cancelación es muy bienvenida.



% 01-10-14: Plugins
% Renombrada la función \emptyStave a \emptyStaff (stave es el plural de staff) y 
% corregida (ya no muestra el número de compás).
% Recordar que después de usar \emptyStaff se necesita redefinir el 
% número de compás con \set Score.currentBarNumber = #xx
%
% Añadida la función \emptyBar que oculta un compás entero. Se puede usar para crear un 
% efecto de indentación en un pentagrama dado.
% Recordar que después de usar \emptyBar se necesita:
% - redefinir el número de compás con \set Score.currentBarNumber = #xx
% - volver a pintar la clave con \clef
% - volver a pintar la armadura con \key

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Dependencias
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% \includeOnce
% Incluye una dependencia solo una vez. Así se pueden incluir los archivos en otros 
% (para montar un score por ejemplo) sin liarla parda con dependencias cíclicas.

includeOnce = #(
	define-void-function (parser location filename) (string?) (
		if (not (defined? (string->symbol filename))) (
			begin (
				ly:parser-include-string parser (
					string-concatenate (list "\\include \"" filename "\"")
				)
			)
			(primitive-eval (list 'define (string->symbol filename) #t))
		)
	)
)



% LilyJAZZ
\includeOnce "includes/lily-jazz.ily"
\includeOnce "includes/ignatzek-jazz-chords.ily"



% Plugins
\includeOnce "includes/plugins-1.2.ily"

% Language
\language "english"

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~











% Disposición
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Cabecera
\header {
	title = "New York, New York"
	instrument = "Baritone Sax"
	composer = "John KANDER & Fred EBB"
	arranger = "Arr: Philippe MARILLIA"
	poet = \markup \tiny "Reducción y edición: Gabriel PALACIOS"
	meter = \markup \tiny "gabriel.ps.ms@gmail.com"
	tagline = \markup {
  	\center-column {
    	\line { \tiny "Reducción y edición: Gabriel PALACIOS" }
    	\line { \tiny "gabriel.ps.ms@gmail.com" }
  	}
	}
}



% Márgenes, fuentes y distancias del papel
\paper {
	% El definir otro tipo de fuente no permite cambiar el tamaño global del staff. Es un bug conocigo
	#(define fonts (make-pango-font-tree "LilyJAZZText" "Nimbus Sans" "Luxi Mono" (/ 20 20)))

	% distancia entre la parte superior de la página y el título
	top-markup-spacing #'basic-distance = 5

	% distancia entre el título y el primer sistema
	markup-system-spacing #'basic-distance = 25

	% distancia entre el texto y el primer sistema 
	top-system-spacing #'basic-distance = 15

	% márgen inferior
	bottom-margin = 20
	
	% intentado del primer sistema
	indent = 0
	 
	% número de páginas
	% page-count = 1

	ragged-last-bottom = ##f 

	oddHeaderMarkup = \markup {
	 \on-the-fly #not-first-page \fill-line {
		\fromproperty #'page:page-number-string
		\fromproperty #'header:title
		\fromproperty #'header:instrument
	 }
	}

	evenHeaderMarkup = \markup {
	 \on-the-fly #not-first-page \fill-line {
		\fromproperty #'header:instrument
		\fromproperty #'header:title
		\fromproperty #'page:page-number-string
	 }
	}
}

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~







% Elementos comunes de la partitura
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
	% Oculta el primer pentagrama del markup
	\override Staff.VerticalAxisGroup #'remove-first = ##t

	% Indicación de tiempo
	\once \override Score.RehearsalMark.X-offset = #5
	\mark \jazzTempoMarkup "Swing" c4 "112"
	s1*8
	
	\bar "||"
	\mark \markup \boxed "9"
	s1*24
	
	\bar "||"
	\mark \markup \boxed "33"
	s1*18
	
	\bar "||"
	\mark \markup \boxed "51"
	s1*13
	
	\bar "||"
	\once \override Score.RehearsalMark.X-offset = #5
	\mark \markup \boxed "64"
	\mark \jazzTempoMarkup "Swing" c4 "80"
	s1*11
	
	\once \override Score.RehearsalMark.X-offset = #0
	\mark \jazzTempoMarkup "Swing" c4 "112"
	s1
	
	\bar "||"
	\mark \markup \boxed "76"
	s1*7
	
	\bar "|."
}



% Función musical global
global = {
	% Para pintar todos los números de compás debajo del pentagrama
	% \override Score.BarNumber #'break-visibility = #end-of-line-invisible
	% \override Score.BarNumber #'direction = #DOWN
	% \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

	\jazzOn
	\compressFullBarRests
	
	\override Score.KeyCancellation.break-visibility = #all-invisible
	
	\set Timing.beamExceptions = #'()
	\set Timing.baseMoment = #(ly:make-moment 1/4)
	\set Timing.beatStructure = #'(1 1 1 1) 
	\set Score.explicitKeySignatureVisibility = #begin-of-line-visible
}

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Elementos específicos del instrumento
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Cifrado
scoreChords = \chords {    
	\set minorChordModifier = \markup { "-" }
	
	% c1:m f2:m g2:m7
}

% Música
baritoneSax = \relative c'' {
  
	\global
	
	% 00: Alto I, Bone I, II, III, Bari
	\relative c' {
		
		\key a \major
		
		a4 -^ \f r e' r
		a,4 -^ r e' r
		b4 -^ r e r
		b4 -^ r e r
		
		\break
		a,4 -^ \p r e' r
		a,4 -^ r e' r
		b4 -^ r e r
		b8 -^ r r4 r2
	}
	
	% 09: Saxs
	\relative c'' {
		
		\key a \major
		
		\break
		R1*6
		
		R1
		r2 b4 ( \mf c )
		
		\break
		cs?4 -. \p cs8 a b cs -. r b (
		cs b b cs ~ cs2 )
		r8 d -> r a ( d e -> d a )
		d b4. -> b4 -. e, -.
		
		\break
		b'4 -. r8 e ~ e fs e a, ~
		a4 r e2 ( \< 
		a,2. ) \mf e'8 a ~
		a4 a, ( b cs
		
		d2 ) \> r2 \!
		R1*3
		
		\break
		cs2 r8 cs -> r gs'
		fs fs4. fs2
		r8 b -> r b b b b as 
		b e, ~ e2. \>
	}
	
	% 33: Trumpet I, II, Bone I, II, IV
	\relative c'' {
		
		\key a \major
		
		\break
		r4 \mp \! e8 ( cs ~ cs4 e,4 -. )
		cs'8 ( cs4. c8 cs -> ) r4
		r4 d8 ( d ~ d d as a ~ 
		a4 )
	}
	
	% 36: Trumpet I, II, Alto I, II, Tenor II
	\transpose ef bf \relative c' {
		
		\key d \major
		
		e4 ( \p e g 
		
		\break
		a4 gs8 a ~ a gs a4 -. )
	}
	
	% 38: Trumpet I, II, Bone I, II, IV
	\relative c'' {
		
		\key a \major
		
		r4 gs ( \< fs f \!
		e4. ) e8 -. e e e4 -.
		a -^ r4 r2
	}
	
	% 41: Alto I, II, Bones I, II, Bari
	\relative c' {
		
		\key a \major
		
		\break
		r4 d -^ d2 ( --
		d -- ) d4 ( d -. )
		r4 a' -^ a ~ a8 fs ~
		fs4 fs -. r2
		
		\break
		R1*2
	}
	
	% 47: Trumpets, Bari
	\relative c' {
		
		\key a \major
		
		a4 -^ \f r e' r
		a,4 -^ r e' r
		b4 -^ r e r
		c4 -^
	}
	
	% 50: Trumpets, Bone IV
	\relative c'' {
		
		\key a \major
		
		c8 ( \< c c c ) ef4 -. \!
		
		\set Score.explicitKeySignatureVisibility = #all-visible
		\once \override Score.KeyCancellation.break-visibility = #end-of-line-visible
		\key bf \major
		
		\break
		f4 -. \mf e8 ( f ~ f e ) f4 -.
		f8 g ~ g2.
		r8 f -> r ef d4 ( a )
		af4 -^ r4 r2
	}
	
	% 55: Alto I, Tenor I, Bones I, II, IV
	\relative c' {
		
		\set Score.explicitKeySignatureVisibility = #begin-of-line-visible
		\key bf \major
		
		\break
		r4 r8 ef -> r2
		r4 r8 ef -> r2
	}
	
	% 57: Alto I, II, Trumpet I, II, IV
	\relative c'' {
		
		\key bf \major
		
		r4 bf8 bf ~ bf bf bf4 -.
		bf4 -^
	}
	
	% 58: Saxs
	\relative c'' {
		
		\key bf \major
		
		\break
		bf ( c cs
		
		\break
		d1 )
	}
	
	% 60: Trumpet I, II, Alto II, Tenor II, Bari
	\relative c'' {
		
		\key bf \major
		
		g4 -^ r r8 g8 ( -- g4 -. )
		fs4 -^ r4 fs2 (
		f? ) \times 2/3 { r4 f -> f -> }
		f4 -^ r4 r2
	}
	
	% 64: Saxs
	\relative c' {
		
		\key bf \major
		
		\break
		r4 bf8 ( bf ~ bf bf bf bf
		c c ~ c2. )
		r4 bf8 ( bf bf bf bf4 -. )
		c4 -^ r r2
	}
	
	% 68: Trumpet I, II, Tenor I, II, Bari
	\relative c' {
		
		\key bf \major
		
		\break
		bf4 -^ r r r8 bf8 ( -- 
		bf4 -^ ) r4 r2
		r4 f'8 ( g af f ~ f4 )
		bf,4 -^ r4 r2
		
		\break
		r4 ef -^ r8 ef ( ef4
		ef4 -^ ) r4 r2
		r4 bf -^ r8 bf4.
		g'4 -^ \improOn r4 r r \improOff
		
		\break
		c,2 -> d ->
		ef -> f ->
		bf, -> f' ->
		bf, -> f' ->
		\break
		c' -> f, ->
		b,1 \fermata
		bf?4 -^ r r2
	}
}

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Impresión de la partitura
% (Sólo se imprime el primer book que encuentra Lilypond!!)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

\include "New York, New York - Violin.ly"
\include "New York, New York - Alto Sax I.ly"
\include "New York, New York - Tenor Sax I.ly"
\include "New York, New York - Tenor Sax II.ly"
\include "New York, New York - Baritone Sax.ly"

\book {
	\score {
		<<
			\scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\new Staff \with {midiInstrument = #"violin"} { \violin }
				\new Staff \with {midiInstrument = #"alto sax"} { \transpose c ef, \altoSaxI }
				\new Staff \with {midiInstrument = #"tenor sax"} { \transpose c bf,, \tenorSaxI }
				\new Staff \with {midiInstrument = #"tenor sax"} { \transpose c bf,, \tenorSaxII }
				\new Staff \with {midiInstrument = #"baritone sax"} { \transpose c ef,, \baritoneSax }
			>>
		>>
		\midi {
			\tempo 4 = 112
		}
		\layout {}
	}
}


