\version "2.18.2"

\include "lily-jazz/ignatzek-jazz-chords.ily"
\include "lily-jazz/lily-jazz.ily"

% PLUGINS
% - \eolMark: 		mark al final de línea
% - \bendBefore: 	solución al bendBefore (i.e. dis8 \bendBefore)
% - \rit: 			crea una porción de ritardando que debe terminar como las dinámicas, con un \!
% - \rs: 			negra de improvisación
% - \emptyStave:	Imprime un pentagrama vacío

% LAYOUT
% - El aspecto de los silencios multicompás es como los de las partituras de jazz
% - Margen extra de las articulaciones para evitar la colisión con las ligaduras
% - Glissandos bonitos

% PAPER
% - Se imprime el título, instrumento y número de página en todas las páginas
% - Solo se debe tocar el page-count (si eso). Sale bonito tal cual.

% CAMBIOS
% 03-09-14: Layout
% Ahora las articulaciones van por dentro de las ligaduras. Parece que es la mejor solución
%
% 04-09-14: Layout
% Los creciendos y decreciendos tienen la línea un poco más gruesa
%
% Markup
% Añadidas guías para correcta posición y markup de la indicación de tempo
%
% Plugins
% Añadida función \emptyStave para imprimir un pentagrama vacío.
% En algunas partituras hay un pentagrama vacío antes del coda (lo cual mola bastante, por cierto)
%
% 05-09-14: Plugins
% Añadido a la función \emptyStave el esconder la armadura de la clave, que no estaba.
%
% 08-09-14: Layout
% Ahora no se muestra la cancelación de accidentales al haber un cambio de armadura
%
% 09-09-14: Chords
% Añadida sentencia para que los acordes menores salgan con un guión en vez de con una M minúscula
%
% 10-09-14: Plugins
% Mejorada la función \bendAfter.
% Ahora es genérica, y se puede definir cualquier texto en un lateral que haga las funciones de ornamento
%
% 15-09-14: Layout
% Añadido un poco más de margen a los reguladores
%
% 16-09-14: Plugins
% Mejorada la función para imprimir barras de improvisación.
% Antes se llamaba al comando \rs para pintar una barra que ocupaba una negra
% Se reemplaza \rs por \restImproOn y \restImproOff, siendo esta opción mucho más flexible.
% Cuando se activa \restImproOn, todo silencio se convierte en barra de improvisación, respetando su duración.
%
% 20-09-14: Plugins
% Mejorada la función para imprimir barras de improvisación.
% Se reemplaza \restImproOn y \restImproOff por \improOn y \improOff
% Ahora, también las notas del pentagrama salen como barra de improvisación.
% (los comandos \improvisationOn e \improvisationOff de lilypond no se llevan bien con LilyJAZZ)
% Se ha añadido un grob correspondiente a los slash de la fuente LilyJAZZ en el archivo lily-jazz.ly
%
% 20-09-14: Layout
% Comentada la línea que oculta la cancelación de accidentales al haber cambio de armadura.
% Realmente, la cancelación es muy bienvenida.


% PLUGINS 1.2
% -----------
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~





% multi-mark-engraver
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
% Aporta la posibilidad de pintar un rehearsal mark al final de una línea y 
% al comienzo de la siguiente.
% http://lists.gnu.org/archive/html/lilypond-user/2011-08/msg00157.html
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
#(define (multi-mark-engraver ctx)
   (let ((texts '())
         (final-texts '())
         (events '()))

     `((start-translation-timestep
        . ,(lambda (trans)
             (set! final-texts '())))

       (listeners
        (mark-event
         . ,(lambda (trans ev)
              (set! events (cons ev events)))))

       (acknowledgers
        (break-alignment-interface
         . ,(lambda (trans grob source)
              (for-each (lambda (mark)
                          (set! (ly:grob-parent mark X) grob))
                        texts))))

       (process-music
        . ,(lambda (trans)
             (for-each
              (lambda (ev)
                (let* ((mark-grob
                        (ly:engraver-make-grob trans 'RehearsalMark ev))
                       (label (ly:event-property ev 'label))
                       (formatter (ly:context-property ctx 'markFormatter)))

                  (if (and (procedure? formatter)
                           (not (markup? label)))
                      (begin
                       (if (not (number? label))
                           (set! label
                                 (ly:context-property ctx 'rehearsalMark)))

                       (if (and (integer? label)
                                (exact? label))
                           (set! (ly:context-property ctx 'rehearsalMark)
                                 (1+ label)))

                       (if (number? label)
                           (set! label (apply formatter (list label ctx)))
                           (ly:warning "rehearsalMark must have
integer value"))))

                  (if (markup? label)
                      (begin
                       (set! (ly:grob-property mark-grob 'text) label)
                       (let ((dir (ly:event-property ev 'direction)))
                         (and (ly:dir? dir)
                              (set! (ly:grob-property mark-grob 'direction)
                                    dir))))
                      (ly:warning "mark label must be a markup object"))

                  (set! texts (cons mark-grob texts))))
              (reverse events))))

       (stop-translation-timestep
        . ,(lambda (trans)
             (if (pair? texts)
                 (let ((staves (ly:context-property ctx 'stavesFound))
                       (priority-index 0))
                   (for-each (lambda (grob)
                               (let ((my-priority (ly:grob-property
grob 'outside-staff-priority 1500)))
                                 (for-each (lambda (stave)

(ly:pointer-group-interface::add-grob grob 'side-support-elements
                                               stave))
                                           staves)
                                 (set! (ly:grob-property grob
'outside-staff-priority) (+ my-priority priority-index))
                                 (set! priority-index (1+ priority-index))
                                 (set! final-texts (cons grob final-texts))))
                             (reverse texts))
                     (set! texts '())
                     (set! events '())))))

        (finalize
         . ,(lambda (trans)
              (and (pair? final-texts)
                   (for-each (lambda (grob)
                               (set! (ly:grob-property grob 'break-visibility)
                                     end-of-line-visible))
                             final-texts)))))))


% Funciones originales
%{
myMark =
#(define-music-function (parser location text) (markup?)
  (make-music 'MarkEvent
              'label text))
			  
toCoda = {
	\tweak #'self-alignment-X #RIGHT
	\tweak #'break-visibility #begin-of-line-invisible
	\myMark \markup { \hspace #1.25 \raise #1.25 \musicglyph #"scripts.coda" }
}
%}

% Función maqueada.
% Se apoya en el código obtenido por consola al ejecutar { \displayMusic { \toCoda }}
eolMark =
#(define-music-function (parser location text) (markup?)
	(make-music 
		'MarkEvent 
		'tweaks (list 
			(cons (quote self-alignment-X) 1)
			(cons (quote break-visibility) #(#t #t #f))
		) 
		'label (markup
			#:line (
				#:hspace 1.25
				#:raise 1.25
				text
			)
		)
	)
)










% rest-score y merge-rests-on-positioning
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
% Permite realizar la fusión de los silencios entre dos voces en un mismo pentagrama.
% https://code.google.com/p/lilypond/issues/detail?id=1228 (fichero merge-rests.ily)
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]

#(define (rest-score r)
  (let ((score 0)
	(yoff (ly:grob-property-data r 'Y-offset))
	(sp (ly:grob-property-data r 'staff-position)))
    (if (number? yoff)
	(set! score (+ score 2))
	(if (eq? yoff 'calculation-in-progress)
	    (set! score (- score 3))))
    (and (number? sp)
	 (<= 0 2 sp)
	 (set! score (+ score 2))
	 (set! score (- score (abs (- 1 sp)))))
    score))

#(define (merge-rests-on-positioning grob)
  (let* ((can-merge #f)
	 (elts (ly:grob-object grob 'elements))
	 (num-elts (and (ly:grob-array? elts)
			(ly:grob-array-length elts)))
	 (two-voice? (= num-elts 2)))
    (if two-voice?
	(let* ((v1-grob (ly:grob-array-ref elts 0))
	       (v2-grob (ly:grob-array-ref elts 1))
	       (v1-rest (ly:grob-object v1-grob 'rest))
	       (v2-rest (ly:grob-object v2-grob 'rest)))
	  (and
	   (ly:grob? v1-rest)
	   (ly:grob? v2-rest)
	   (let* ((v1-duration-log (ly:grob-property v1-rest 'duration-log))
		  (v2-duration-log (ly:grob-property v2-rest 'duration-log))
		  (v1-dot (ly:grob-object v1-rest 'dot))
		  (v2-dot (ly:grob-object v2-rest 'dot))
		  (v1-dot-count (and (ly:grob? v1-dot)
				     (ly:grob-property v1-dot 'dot-count -1)))
		  (v2-dot-count (and (ly:grob? v2-dot)
				     (ly:grob-property v2-dot 'dot-count -1))))
	     (set! can-merge
		   (and
		    (number? v1-duration-log)
		    (number? v2-duration-log)
		    (= v1-duration-log v2-duration-log)
		    (eq? v1-dot-count v2-dot-count)))
	     (if can-merge
		 ;; keep the rest that looks best:
		 (let* ((keep-v1? (>= (rest-score v1-rest)
				      (rest-score v2-rest)))
			(rest-to-keep (if keep-v1? v1-rest v2-rest))
			(dot-to-kill (if keep-v1? v2-dot v1-dot)))
		   ;; uncomment if you're curious of which rest was chosen:
		   ;;(ly:grob-set-property! v1-rest 'color green)
		   ;;(ly:grob-set-property! v2-rest 'color blue)
		   (ly:grob-suicide! (if keep-v1? v2-rest v1-rest))
		   (if (ly:grob? dot-to-kill)
		       (ly:grob-suicide! dot-to-kill))
		   (ly:grob-set-property! rest-to-keep 'direction 0)
		   (ly:rest::y-offset-callback rest-to-keep)))))))
    (if can-merge
	#t
	(ly:rest-collision::calc-positioning-done grob))))










% startParenthesis y endParenthesis
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
% http://comments.gmane.org/gmane.comp.gnu.lilypond.general/64087
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]

% ELIMINADO










% bendBefore
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
% Adaptación
% http://lilypondblog.org/2013/08/adding-ornamentations-to-note-heads-part-1/
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]

%%%
%%% Utilities for defining new grobs, grob properties and music event types
%%% (there should be built-in commands to do that in LilyPond)
%%%
#(define (define-grob-type grob-name grob-entry)
   "Define a new grob and add it to `all-grob-definitions', after
scm/define-grobs.scm fashion.
After grob definitions are added, use:

\\layout {
  \\context {
    \\Global
    \\grobdescriptions #all-grob-descriptions
  }
}

to register them."
   (let* ((meta-entry   (assoc-get 'meta grob-entry))
          (class        (assoc-get 'class meta-entry))
          (ifaces-entry (assoc-get 'interfaces meta-entry)))
     (set-object-property! grob-name 'translation-type? list?)
     (set-object-property! grob-name 'is-grob? #t)
     (set! ifaces-entry (append (case class
                                  ((Item) '(item-interface))
                                  ((Spanner) '(spanner-interface))
                                  ((Paper_column) '((item-interface
                                                     paper-column-interface)))
                                  ((System) '((system-interface
                                               spanner-interface)))
                                  (else '(unknown-interface)))
                                ifaces-entry))
     (set! ifaces-entry (uniq-list (sort ifaces-entry symbol<?)))
     (set! ifaces-entry (cons 'grob-interface ifaces-entry))
     (set! meta-entry (assoc-set! meta-entry 'name grob-name))
     (set! meta-entry (assoc-set! meta-entry 'interfaces
                                  ifaces-entry))
     (set! grob-entry (assoc-set! grob-entry 'meta meta-entry))
     (set! all-grob-descriptions
           (cons (cons grob-name grob-entry)
                 all-grob-descriptions))))

#(define-public (define-grob-property symbol type? description)
   "Define a new grob property.
`symbol': the property name
`type?': the type predicate for this property
`description': the type documentation"
  (set-object-property! symbol 'backend-type? type?)
  (set-object-property! symbol 'backend-doc description)
  symbol)

#(defmacro*-public define-music-type (type-name parents #:rest properties)
     "Add a new music type description to `music-descriptions'
and `music-name-to-property-table'.

`type-name': the music type name (a symbol)
`parents': the parent event classes (a list of symbols)
`properties': a (key . value) property set, which shall contain at least
   description and type properties.
"
   (let ((gproperties (gensym "properties")))
     `(let ((,gproperties (list-copy ',properties)))
        ,@(map (lambda (parent)
                `(define-event-class ',(ly:camel-case->lisp-identifier type-name)
                   ',parent))
               parents)
        (set-object-property! ',type-name
                              'music-description
                              (cdr (assq 'description ,gproperties)))
        (set! ,gproperties (assoc-set! ,gproperties 'name ',type-name))
        (set! ,gproperties (assq-remove! ,gproperties 'description))
        (hashq-set! music-name-to-property-table ',type-name ,gproperties)
        (set! music-descriptions
              (cons (cons ',type-name ,gproperties)
                    music-descriptions)))))

%%%
%%% Grob definition
%%%
#(define (head-ornamentation::print me)
   "Prints a HeadOrnamentation grob, on the left or right side of the
note head, depending on the grob direction."
   (let* ((notes (ly:grob-object me 'elements))
          (y-ref (ly:grob-common-refpoint-of-array me notes Y))
          (x-ref (ly:grob-common-refpoint-of-array me notes X))
          (x-ext (ly:relative-group-extent notes x-ref X))
          (y-ext (ly:relative-group-extent notes y-ref Y))
          (staff-position (ly:grob-staff-position (ly:grob-array-ref notes 0)))
          (y-coord (+ (interval-center y-ext)
                      (if (and (eq? (ly:grob-property me 'shift-when-on-line) #t)
                               (memq staff-position '(-4 -2 0 2 4)))
                          0.5
                          0)))
          (text (ly:text-interface::print me))
          (width (/ (interval-length (ly:stencil-extent text X)) 2.0))
          (x-coord (if (= (ly:grob-property me 'direction) LEFT)
                       (- (car x-ext) width)
                       (+ (cdr x-ext) width))))
     (ly:stencil-translate
      text
      (cons
       (- (- x-coord (ly:grob-relative-coordinate me x-ref X)) 
          (ly:grob-property me 'x-position))
       (- (- y-coord (ly:grob-relative-coordinate me y-ref Y)) 
          (ly:grob-property me 'y-position))
       ))))

%% a new grob property (used to shift an ornamentation when the
%% note head is on a staff line)
#(define-grob-property 'shift-when-on-line boolean?
   "If true, then the ornamentation is vertically shifted when
the note head is on a staff line.")

#(define-grob-property 'x-position number?
   "X position")

#(define-grob-property 'y-position number?
   "Y position")

#(define-grob-type 'HeadOrnamentation
  `((font-size . 0)
    (shift-when-on-line . #f)
    (x-position . 1)
    (y-position . 1)
    (stencil . ,head-ornamentation::print)
    (meta . ((class . Item)
             (interfaces . (font-interface))))))

\layout {
  \context {
    \Global
    \grobdescriptions #all-grob-descriptions
  }
}

%%%
%%% Engraver
%%%
%% The head-ornamentation engraver, with its note-head acknowledger
%% (which add HeadOrnamentation grobs to note heads)
#(define head-ornamentation-engraver
   (make-engraver
    (acknowledgers
     ((note-head-interface engraver note-grob source)
      ;; helper function to create HeadOrnamentation grobs
      (define (make-ornament-grob text direction shift-when-on-line x-position y-position)
        (let ((ornament-grob (ly:engraver-make-grob engraver
                                                    'HeadOrnamentation
                                                    note-grob)))
          ;; use the ornament event text as the grob text property
          (set! (ly:grob-property ornament-grob 'text) text)
          ;; set the grob direction (either LEFT or RIGHT)
          (set! (ly:grob-property ornament-grob 'direction) direction)
          ;; set the shift-when-on-line property using the given value
          (set! (ly:grob-property ornament-grob 'shift-when-on-line)
                shift-when-on-line)
          ;; set the grob x-pos
          (set! (ly:grob-property ornament-grob 'x-position) x-position)
          ;; set the grob y-pos
          (set! (ly:grob-property ornament-grob 'y-position) y-position)
          (ly:pointer-group-interface::add-grob ornament-grob
                                                'elements
                                                note-grob)
          
          ;; the ornamentation is vertically aligned with the note head
          (set! (ly:grob-parent ornament-grob Y) note-grob)
          ;; compute its font size
          (set! (ly:grob-property ornament-grob 'font-size)
                (+ (ly:grob-property ornament-grob 'font-size 0.0)
                   (ly:grob-property note-grob 'font-size 0.0)))
          ;; move accidentals and dots to avoid collision
          (let* ((ornament-stencil (ly:text-interface::print ornament-grob))
                 (ornament-width (interval-length
                                  (ly:stencil-extent ornament-stencil X)))
                 (note-column (ly:grob-object note-grob 'axis-group-parent-X))
                 ;; accidentals attached to the note:
                 (accidentals (and (ly:grob? note-column)
                                   (ly:note-column-accidentals note-column)))
                 ;; dots attached to the note:
                 (dot-column (and (ly:grob? note-column)
                                  (ly:note-column-dot-column note-column))))
            (cond ((and (= direction LEFT) (ly:grob? accidentals))
                   ;; if the ornament is on the left side, and there are
                   ;; accidentals, then increase padding between note
                   ;; and accidentals to make room for the ornament
                   (set! (ly:grob-property accidentals 'padding)
                         ornament-width))
                  ((and (= direction RIGHT) (ly:grob? dot-column))
                   ;; if the ornament is on the right side, and there
                   ;; are dots, then translate the dots to make room for
                   ;; the ornament
                   (set! (ly:grob-property dot-column 'positioning-done)
                         (lambda (grob)
                           (ly:dot-column::calc-positioning-done grob)
                           (ly:grob-translate-axis! grob ornament-width X))))))
          ornament-grob))
      ;; When the note-head event attached to the note-head grob has
      ;; ornamentation events among its articulations, then create a
      ;; HeadOrnamentation grob
      (for-each
       (lambda (articulation)
         (if (memq 'head-ornamentation-event
                   (ly:event-property articulation 'class))
             ;; this articulation is an ornamentation => make the grob
             ;; (either on LEFT or RIGHT direction)
             (begin
               (if (markup? (ly:event-property articulation 'left-text))
                   (make-ornament-grob
                    (ly:event-property articulation 'left-text)
                    LEFT
                    (ly:event-property articulation 'shift-when-on-line)
                    (ly:event-property articulation 'x-position)
                    (ly:event-property articulation 'y-position)))
               (if (markup? (ly:event-property articulation 'right-text))
                   (make-ornament-grob
                   (ly:event-property articulation 'right-text)
                   RIGHT
                   (ly:event-property articulation 'shift-when-on-line)
                   (ly:event-property articulation 'x-position)
                   (ly:event-property articulation 'y-position))))))
         (ly:event-property (ly:grob-property note-grob 'cause) 'articulations))))))

\layout {
  \context {
    \Score
    \consists #head-ornamentation-engraver
  }
}

%%%
%%% Event type definition
%%%
#(define-music-type HeadOrnamentationEvent (music-event)
   (description . "Print an ornamentation at a note head side")
   (types . (general-music post-event event head-ornamentation-event)))

bendBefore =
#(make-music 'HeadOrnamentationEvent
  'x-position 1
  'y-position 2
  'shift-when-on-line #f
  'left-text #{ \markup { \fontsize #0 \rotate #5 \musicglyph #"brackettips.up"} #})










% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
% Funciones propias
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]





% Índice con borde circular y un cómodo espacio interior (y no el que viene por defecto)
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
#(define-markup-command (boxindex layout props text) (markup?)
	(interpret-markup layout props
		(markup #:huge #:override '(box-padding . 0.5) #:box text)
	)
)

#(define-markup-command (boxed layout props text) (markup?)
	(interpret-markup layout props
		(markup #:huge #:override '(box-padding . 0.9) #:box text)
	)
)

% Ritardando
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
rit =
#(make-music 'CrescendoEvent
             'span-direction START
             'span-type 'text
             'span-text "rit.")

% Barras de improvisacion
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
improOn = {
  \override Rest.stencil = #ly:text-interface::print
  \override Rest.text = \markup \jazzglyph #"noteheads.s2slashjazz"
  \override NoteHead.stencil = #jazz-slashednotehead
}

improOff = {
  \revert Rest.stencil
  \revert Rest.text
  \revert NoteHead.stencil
}

% Pentagrama vacío
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
emptyStave = {
  \once \override Staff.KeySignature.stencil = ##f
  \once \override Staff.Clef.stencil = ##f
  \break
  s1
  \once \override Staff.BarLine.stencil = ##f
  \break
}










% Añadido de funciones al contexto
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
\layout {
  \context {
    \Score		
    
    % multi-mark-engraver
    \remove "Mark_engraver"
    \consists #multi-mark-engraver
    \consists "Tweak_engraver"
    
    % merge-rests-on-positioning
    \override RestCollision #'positioning-done = #merge-rests-on-positioning
    
    % aspecto de los silencios multicompás
    \override MultiMeasureRest.expand-limit = #1
    
    % margen extra de las articulaciones para evitar la colisión con las ligaduras
    \override Script.padding = #0.5
    \override Script.avoid-slur = #'inside
    
    % glissando bonito
    \override Glissando.style = #'trill
    \override Glissando.bound-details.left.padding = #1.7
    
    % dinámicas un poco mas gruesas
    \override Hairpin.thickness = #1.75
    \override Hairpin.bound-padding = #2
    
    % Esconde la cancelacion de accidentales en el cambio de armadura
    % \override KeyCancellation.break-visibility = #'#(#f #f #f)
  }
}












% LAYOUT
% ------
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~






\header {
  title = "Cabaret"
  instrument = "Baritone Sax"
  composer = "John KANDER & Fred EBB"
  arranger = ""
  poet = \markup \tiny "Reducción y edición: Gabriel PALACIOS"
  meter = \markup \tiny "gabriel.ps.ms@gmail.com"
  tagline = \markup {
    \center-column {
      \line { \tiny "Reducción y edición: Gabriel PALACIOS" }
      \line { \tiny "gabriel.ps.ms@gmail.com" }
    }
  }
}

\paper {
  % El definir otro tipo de fuente no permite cambiar el tamaño global del staff. Es un bug conocigo
  #(define fonts (make-pango-font-tree "LilyJAZZText" "Nimbus Sans" "Luxi Mono" (/ 20 20)))
  
  % distancia entre la parte superior de la página y el título
  top-markup-spacing #'basic-distance = 5
  
  % distancia entre el título y el primer sistema
  markup-system-spacing #'basic-distance = 25
  
  % distancia entre el texto y el primer sistema 
  top-system-spacing #'basic-distance = 15
  
  % márgen inferior
  bottom-margin = 20
    
  % número de páginas
  % page-count = 2
  
  ragged-last-bottom = ##f 
  
  oddHeaderMarkup = \markup {
    \on-the-fly #not-first-page \fill-line {
      \fromproperty #'page:page-number-string
      \fromproperty #'header:title
      \fromproperty #'header:instrument
    }
  }
  
  evenHeaderMarkup = \markup {
    \on-the-fly #not-first-page \fill-line {
      \fromproperty #'header:instrument
      \fromproperty #'header:title
      \fromproperty #'page:page-number-string
    }
  }
}










% MUSIC PARTS
% -----------
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~





% scoreMarkup
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
scoreMarkup = \new Staff \with {
   \RemoveEmptyStaves
}
{
  % Oculta el primer pentagrama del markup
  \override Staff.VerticalAxisGroup #'remove-first = ##t
  
  % Indicación de tiempo
  \override Score.RehearsalMark.X-offset = #5
  \mark \jazzTempoMarkup "Moderate Swing" c2 "85"
  s1*4
  
  \bar "||"
  \mark \markup \boxed "A"
  s1*8
  
  \bar "||"
  s1*8
  
  \bar "||"
  \mark \markup \boxed "B"
  s1*16
  
  \bar "||"
  \mark \markup {
    \column { \boxed "C" }
    \column { "(Intro to vocal)" }
  }
  s1*4
  
  \bar "||"
  \mark \markup {
    \column { \boxed "D" }
    \column { "(Vocal)" }
  }
  s1*16
  
  \bar "||"
  \mark \markup \boxed "E"
  s1*16
  
  \bar ".|:-||"
  \mark \markup {
    \column { \boxed "F" }
    \column { "(Annotations apply to 2nd time only)" }
  }
  s1*8
  
  \mark \markup \boxed "G"
  s1*16
  
  \bar "||"
  \mark \markup "(Recitativo)"
  s1
  s1
  
  \mark \markup "(A tempo)"
  s1*22
  
  \bar "|."
}





% scoreChords
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
scoreChords = \chords {
    
  \set minorChordModifier = \markup { "-" }
  
  % c1:m f2:m g2:m7
}





% music
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
baritoneSax = \relative c'' {

  % Para pintar todos los números de compás debajo del pentagrama
  % \override Score.BarNumber #'break-visibility = #end-of-line-invisible
  % \override Score.BarNumber #'direction = #DOWN
  % \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0
  \set Score.explicitKeySignatureVisibility = #begin-of-line-visible
  
  \set Score.tempoHideNote = ##t
  \tempo 2 = 85
  
  % 00: Trumpet I, Trumpet II, Bones I, II & III
  \transpose ees c \relative c'' {
    
    \key f \major
    
    g4 -. \f r4 g2
    gis2. gis4
    a2 aes4 -. -^ aes -. -^ 
    g4 -- g8 -- ges -> r ges -- ges4 -.
  }
  
  % 05: Alto I, Alto II, Bones I, III & IV
  \transpose ees c \relative c'' {
   
    \key f \major
   
    \break
    R1*3
    r4 \mf bes8 -- bes -> r bes -- bes4 -.
    
    \break
    c4 -^ r r2
    R1
    r8 g -> r g -- c4 -. -^ c -. -^
    c1 -> --
  }
  
  % 12: Trumpet I, Trumpet II, Trumpet III, saxs?
  \relative c'' {
    
    \key d \major
    
    \break
    g2 ~ g8 d' ( -> b g
    gis2 ~ gis8 ) d' ( -> b gis
    a1 )
  }
  
  % 16: Trumpet I, Trumpet II, Alto I, II, mix(tenor I, II) -> 1/4B Bones I,II,III
  \transpose ees bes, \relative c'' {
    
    \key g \major
    
    r4 a8 ( gis -> ) r gis ( g4 -. )
    
    \break
    c4 -. e2 -> c4 -.
    c2 c
  }
  
  \relative c' {
    
    \key d \major
    
    r8 fis, -> r fis b4 -. -^ b -. -^
    e,4 -- e8 ( a ~ a )
  }
  
  % B0: Alto I, Alto II, Bones I, III & IV
  \transpose ees c \relative c'' {
    
    \key f \major
    c, -- c4 -.
    
    \break
    f'4 -. r r2
    R1*2
    r8 \mf e4. -> gis4 -. -^ e -. -^
    
    \break
    e8 ( f -> ) r4 r2
    R1
    r4 c'8 -- bes -> r b -- bes4 -.
    g2. r4
  }
  
  % 28: Trumpet I, Trumpet II, Trumpet III, saxs?
  \relative c' {
    
    \key d \major
    
    \break
    g4. e'8 d ( -> b g gis ~
    gis2 ~ gis8 ) b ( -> gis a ~
    a2 e' )
  }
  
  % 32: Trumpet I, Trumpet II, Alto I, II, Tenor I
  \transpose ees bes, \relative c''' {
    
    \key g \major
    
    r8 a4. ( -> fis8 gis fis c ~
    
    \break
    c2 ) c4 -. -^ c -. -^
    r8 c4. -> r8 c4. ->
    r8 d ( b [ g ] b g b d ~
    d1 )
  }
  
  % C0: Trumpet I, Trumpet II, Trumpet III/Alto I, Alto II, Tenor I
  \transpose ees bes, \relative c'' {
    
    \key g \major
    
    \break
    g4. \f g8 ~ g2
    b2 r8 e -> ( b [ g ] ) ~
    g2. g4 -. \>
    fis1 -> \!
  }
  
  % D0: Trumpet I, Trumpet II, Trumpet III/Alto I, Alto II, Tenor I
  \transpose ees bes, \relative c'' {
    
    \set Score.explicitKeySignatureVisibility = #all-visible
    \key f \major
    
    \break
    R1*8
    
    \break
    bes4 -. r bes2 (
    b ) r8 b4. ( ->
    c4 -. ) r a ( fis -> ~
    fis1 )
    
    \break
    bes?1
    bes2 bes4. a8 ~
    a2. a4 -.
    bes1
    
    \break
    R1*8
    
    \break
    bes2. bes4 -.
    b4. d8 ~ d2
    e2 -> \glissando c
  }
  
  % 68: Saxs
  \relative c'' {
    
    \set Score.explicitKeySignatureVisibility = #begin-of-line-visible
    \key c \major
      
    r4 g8 ( \< a \! bes [ g ] \> e ) \! r
      
    \break
    d,1
    g
    r4 c8 ( gis a c d e -> )
    R1
    
    \repeat volta 2 {
      
      \break
      r8 \mf c4. -> b8 ( c ) r aes' ~
      aes1
      r4 c,8 ( b c e f g ~
      g1 )
      
      \break
      a2 ( ^\markup  "(rall. & conducted)" aes
      \override BreathingSign.text = \markup { \musicglyph #"scripts.caesura.straight" }
      g2 d ) \breathe
      b'4 -. -^ \fermata r r2
      R1
      
      \break
      r4 r8 g, -> ~ g2
      r4 e8 ( g -> ) r2
      r8 a8 ( c4 -. ) e2
      r4 g8 ( ees ) r g ( ees4 -. )
      
      \break
      r8 e4. -> e4 -. -^ c -. -^
      r8 c4. -> e4 -. -^ r
      r8 ^\markup "(A tempo)" bes'8 ( d [ bes ] g fis f d ~
      d2. ) r4
      
      \break
      f,4 -. \f r f2 (
      fis2. ) fis4 -.
    }
    
    \alternative {
      { g2. a4 -^ ~
        a2 g'8 ( e cis d ~        
        
        \break
        d1 )
        r8 g,4. r8 g4.
        r4 c8 -- c -- c -- c -- c -- e ->
        R1
        
        \break
        s1
        s1 _ \markup \tiny "(Modulation) \"I think of Elsie to this very day [...]\""
        R1*4 _ \markup \tiny "\"Life is a Cabaret, old Chum\""
        
        \break
        R1*2
        r4 c8 ( gis a c d e -> )
        R1
      }
      
      { \break 
        g,2. a4 -^ ~
        a8 a4. -> b4 -. -^ cis4 -. -^ }
    }
    
    d4 -. -^ r d2
    ees2. ees4 -.
    e?2. g4 -^ ~
    \break
    g2 cis, \ff
    
    d2 d \glissando
    d,1 ->
    r2 g ->
    r g ->
    
    \break
    r8 g4. -> a4 -. -^ c -. -^
    e ( g2 ) cis,8 -- d ->
    r8 a' -- d,4 -. d8 ( dis ) r e -> ~
    e2 \fermata c4 -^ c -^
  }
}





% score
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
\score {
  <<
    \scoreMarkup
    \scoreChords
    \new StaffGroup <<
      \new Staff { \transpose f des' \baritoneSax }
    >>
  >>
}

tenorSaxII = \relative c'' {

  % Para pintar todos los números de compás debajo del pentagrama
  % \override Score.BarNumber #'break-visibility = #end-of-line-invisible
  % \override Score.BarNumber #'direction = #DOWN
  % \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0
  \set Score.explicitKeySignatureVisibility = #begin-of-line-visible
  
  % 00: Trumpet I, Trumpet II, Bones I, II & III
  \transpose bes c \relative c'' {
    
    \key f \major
    
    g4 -. \f r4 g2
    gis2. gis4
    a2 b4 -. -^ b -. -^ 
    bes?4 -- bes8 -- bes -> r bes -- bes4 -.
  }
  
  % 05: Alto I, Alto II, Bones I, III & IV
  \transpose bes c \relative c'' {
   
    \key f \major
   
    \break
    R1*3
    r4 \mf gis8 -- e -> r gis -- e4 -.
    
    \break
    f4 -^ r r2
    R1
    r8 ees -> r ees -- f4 -. -^ f -. -^
    g1 -> --
  }
  
  % 12: Trumpet I, Trumpet II, Trumpet III, saxs?
  \transpose bes ees \relative c'' {
    
    \key d \major
    
    \break
    g2 ~ g8 d' ( -> b g
    gis2 ~ gis8 ) d' ( -> b gis
    a1 )
  }
  
  % 16: Trumpet I, Trumpet II, Alto I, II, mix(tenor I, II) -> 1/4B Bones I,II,III
  \transpose bes ees \relative c'' {
    
    \key d \major
    
    r4 cis8 ( a -> ) r cis ( a4 -. )
    
    \break
    b4 -. d2 -> b4 -.
    cis2 cis
    r8 bes -> r bes a4 -. -^ a -. -^
    gis4 -- gis8 ( g ~ g )
  }
  
  % B0: Alto I, Alto II, Bones I, III & IV
  \transpose bes c \relative c'' {
    
    \key f \major
    c -- d4 -.
    
    \break
    a4 -. r r2
    R1*2
    r8 \mf aes4. -> bes4 -. -^ aes -. -^
    
    \break
    aes8 ( a -> ) r4 r2
    R1
    r4 bes8 -- g -> r bes -- g4 -.
    c2. r4
  }
  
  % 28: Trumpet I, Trumpet II, Trumpet III, saxs?
  \transpose bes ees' \relative c' {
    
    \key d \major
    
    \break
    g4. e'8 d ( -> b g gis ~
    gis2 ~ gis8 ) b ( -> gis a ~
    a2 e' )
  }
  
  % 32: Trumpet I, Trumpet II, Alto I, II, Tenor I
  \transpose bes ees \relative c''' {
    
    \key d \major
    
    r8 a4. ( -> fis8 a fis d ~
    
    \break
    d2 ) b4 -. -^ b -. -^
    r8 cis4. -> r8 cis4. ->
    r8 d ( b [ a ] b a b d ~
    d1 )
  }
  
  % C0: Trumpet I, Trumpet II, Trumpet III/Alto I, Alto II, Tenor I
  \transpose bes ees, \relative c'' {
    
    \key d \major
    
    \break
    f4. \f a8 ~ a2
    b2 r8 e -> ( b [ a ] ) ~
    a2. a4 -. \>
    f1 -> \!
  }
  
  % D0: Trumpet I, Trumpet II, Trumpet III/Alto I, Alto II, Tenor I
  \transpose bes ees \relative c'' {
    
    \set Score.explicitKeySignatureVisibility = #all-visible
    \key c \major
    
    \break
    R1*8
    
    \break
    a4 -. r a2 (
    c ) r8 c4. ( ->
    b4 -. ) r b ( g -> ~
    g1 )
    
    \break
    a1
    b2 b4. a8 ~
    a2. g4 -.
    b1
    
    \break
    R1*8
    
    \break
    a2. a4 -.
    c4. ees8 ~ ees2
    e2 -> \glissando b
  }
  
  % 68: Saxs
  \relative c' {
    
    \set Score.explicitKeySignatureVisibility = #begin-of-line-visible
    \key f \major
      
    r4 fis8 ( \< gis \! a [ fis ] \> e ) \! r
      
    \break
    f,1
    d'
    r4 f8 ( cis d f g a -> )
    R1
    
    \repeat volta 2 {
      
      \break
      r8 \mf des,4. -> c8 ( des ) r bes' ~
      bes1
      r4 d,?8 ( cis d f g a ~
      a1 )
      
      \break
      d,2 ( ^\markup  "(rall. & conducted)" g
      \override BreathingSign.text = \markup { \musicglyph #"scripts.caesura.straight" }
      f2 d ) \breathe
      c4 -. -^ \fermata r r2
      R1
      
      \break
      r4 r8 c -> ~ c2
      r4 d8 ( bes -> ) r2
      r8 c8 ( d4 -. ) d2
      r4 gis8 ( d ) r gis ( cis,4 -. )
      
      \break
      r8 d4. -> d4 -. -^ c -. -^
      r8 c4. -> d4 -. -^ r
      r8 ^\markup "(A tempo)" bes'8 ( c [ bes ] g f ees c ~
      c2. ) r4
      
      \break
      d4 -. \f f2 -> d4 -. 
      f4. d8 ~ d2
    }
    
    \alternative {
      { e4 -. g2 c,4 -^ ~ 
        c2 fis8 ( ees fis f ~        
        
        \break
        f1 )
        r8 c4. r8 c4.
        r4 c8 -- c -- c -- c -- c -- d ->
        R1
        
        \break
        s1
        s1 _ \markup \tiny "(Modulation) \"I think of Elsie to this very day [...]\""
        R1*4 _ \markup \tiny "\"Life is a Cabaret, old Chum\""
        
        \break
        R1*2
        r4 f8 ( cis d f g a -> )
        R1
      }
      
      { \break 
        e4 -. c2 -> a4 -^ ~
        a1 }
    }
    
    g'4 -. bes2 -> g4 -.
    b4. gis8 ~ gis2
    a4 -. c2 -> fis,4 -^ ~ 
    \break
    fis2 cis \ff
    
    d2 f \glissando
    bes,1 ->
    r2 c ->
    r c ->
    
    \break
    r8 c4. -> d4 -. -^ f -. -^
    a ( c2 ) cis8 -- d ->
    r8 f -- d4 -. f8 ( fis ) r g -> ~
    g2 \fermata f,4 -^ f -^
  }
}

tenorSaxI = \relative c'' {

  % Para pintar todos los números de compás debajo del pentagrama
  % \override Score.BarNumber #'break-visibility = #end-of-line-invisible
  % \override Score.BarNumber #'direction = #DOWN
  % \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0
  \set Score.explicitKeySignatureVisibility = #begin-of-line-visible
  
  % 00: Trumpet I, Trumpet II, Bones I, II & III
  \transpose bes c \relative c'' {
    
    \key f \major
    
    g4 -. \f r4 g2
    gis2. gis4
    a2 ees'4 -. -^ ees -. -^ 
    d4 -- d8 -- des -> r ees -- des4 -.
  }
  
  % 05: Alto I, Alto II, Bones I, III & IV
  \transpose bes c \relative c'' {
   
    \key f \major
   
    \break
    R1*3
    r4 \mf e8 -- c -> r e -- c4 -.
    
    \break
    d4 -^ r r2
    R1
    r8 c -> r c -- d4 -. -^ d -. -^
    ees4 -> --
  }
  
  % 12: Trumpet I, Trumpet II, Trumpet III, saxs?
  \relative c' {
    
    \key g \major
    b8 -- d -> r \< f -- f4 -. \!
    
    \break
    g4 -. \f a2 -> g4 -.
    cis4. ais8 ~ ais2
    % \set Score.tieWaitForNote = ##t
    b4 -. b2 -> fis4 -^ ~
  }
  
  % 16: Trumpet I, Trumpet II, Alto I, II, mix(tenor I, II) -> 1/4B Bones I,II,III
  \transpose bes ees' \relative c' {
    
    \key d \major
    
    cis4 fis8 ( cis -> ) r fis ( cis4 -. )
    
    \break
    d4 -. fis2 -> d4 -.
    fis2 f
    r8 d -> r d cis4 -. -^ cis -. -^
    c4 -- c8 ( b ~ b )
  }
  
  % B0: Alto I, Alto II, Bones I, III & IV
  \transpose bes c \relative c'' {
    
    \key f \major
    e -- e4 -.
    
    \break
    c4 -. r r2
    R1*2
    r8 \mf bes4. -> c4 -. -^ bes -. -^
    
    \break
    b8 ( c -> ) r4 r2
    R1
    r4 ees8 -- c -> r ees -- c4 -.
    ees4 -> --
  }
  
  % 28: Trumpet I, Trumpet II, Trumpet III, saxs?
  \relative c' {
    
    
    \key g \major
    b8 -- d -> r \< f -- f4 -. \!
    
    \break
    g4 -. \f a2 -> g4 -. 
    r8 bes ( -> ~ \times 2/3 { bes a aes } g2 )
    r8 b4. -> a8 b a fis ~ 
    fis8
  }
  
  % 32: Trumpet I, Trumpet II, Alto I, II, Tenor I
  \transpose bes ees \relative c'' {
    
    \key d \major
    
    cis4. ( -> a8 cis a fis ~
    
    \break
    fis2 ) fis4 -. -^ d -. -^
    r8 fis4. -> r8 f4. ->
    r8 fis ( d [ b ] d b d fis ~
    fis1 )
  }
  
  % C0: Trumpet I, Trumpet II, Trumpet III/Alto I, Alto II, Tenor I
  \transpose bes ees \relative c'' {
    
    \key d \major
    
    \break
    c4. \f f8 ~ f2
    e2 r8 g -> ( e [ c ] ) ~
    c2. c4 -. \>
    b1 -> \!
  }
  
  % D0: Trumpet I, Trumpet II, Trumpet III/Alto I, Alto II, Tenor I
  \relative c'' {
    
    \set Score.explicitKeySignatureVisibility = #all-visible
    \key f \major
    
    \break
    R1*3
    r4 \mf c8 -- gis -> r c -- gis4 -.
    
    \break
    a4 -. -^ r r2
    R1
    r8 c -> r c a4 -. -^ a -. -^
    g4. g8 -- ~ g ees g4 -.
  }
  
  \transpose bes ees \relative c'' {
    
    \set Score.explicitKeySignatureVisibility = #begin-of-line-visible
    \key c \major
    
    \break
    d4 -. r d2 (
    dis ) r8 dis4. ( ->
    e4 -. ) r d ( b -> ~
    b1 )
    
    \break
    e1
    e2 ees4. d8 ~
    d2. c4 -.
    e1
  }
  
  \relative c'' {
    
    \key f \major
    
    \break
    R1*3
    r4 a8 -- a -- \< a -- a -- a4 -. \!
    
    \break
    R1*2
    r8 a4. -> a4 -. -^ r8 g -> ~
    g4 a -. -^ r8 cis d4 -.
  }
  
  \transpose bes ees \relative c'' {
    
    \key c \major
    
    \break
    c2. d4 -.
    ees4. fis8 ~ fis2
    g2 -> \glissando e
  }
  
  % 68: Saxs
  \relative c' {
    
    \key f \major
      
    r4 e8 ( \< f \! fis [ e ] \> c ) \! r
      
    \break
    bes1
    bes
    r4 f'8 ( cis d f g a -> )
    R1
    
    \repeat volta 2 {
      
      \break
      r8 \mf f4. -> e8 ( f ) r des' ~
      des1
      r4 f,8 ( e f a bes c ~
      c1 )
      
      \break
      f,1 ^\markup  "(rall. & conducted)"
      \override BreathingSign.text = \markup { \musicglyph #"scripts.caesura.straight" }
      d2 ( b ) \breathe
      e4 -. -^ \fermata r r2
      R1
      
      \break
      r4 r8 f, -> ~ f2
      r4 bes8 ( e -> ) r2
      r8 f,8 ( a4 -. ) c2
      r4 e8 ( bes ) r e ( bes4 -. )
      
      \break
      r8 c4. -> c4 -. -^ a -. -^
      r8 a4. -> c4 -. -^ r
      r8 ^\markup "(A tempo)" g'8 ( bes [ g ] ees d c a ~
      a2. ) r4
      
      \break
      bes4 -. \f d2 -> bes4 -. 
      d4. b8 ~ b2
    }
    
    \alternative {
      { c4 -. a2 d,4 -^ ~ 
        d2 ees'8 ( c ees d ~        
        
        \break
        d1 )
        r8 c4. r8 c4.
        r4 a8 -- a -- a -- a -- a -- c ->
        R1
        
        \break
        s1
        s1 _ \markup \tiny "(Modulation) \"I think of Elsie to this very day [...]\""
        R1*4 _ \markup \tiny "\"Life is a Cabaret, old Chum\""
        
        \break
        R1*2
        r4 f8 ( cis d f g a -> )
        R1
      }
      
      { \break 
        c,4 -. a2 -> fis4 -^ ~
        fis1}
    }
    
    f'4 -. g2 -> f4 -.
    aes4. f8 ~ f2
    g4 -. a2 -> e4 -^ ~ 
    \break
    e2 fis, \ff
    
    g2 d' \glissando
    g,1 ->
    r2 c ->
    r c ->
    
    \break
    r8 c4. -> d4 -. -^ f -. -^
    a ( c2 ) a8 -- bes ->
    r8 d -- bes4 -. d8 ( dis ) r e -> ~
    e2 \fermata f,4 -^ f -^
  }
}

altoSaxI = \relative c'' {

  % Para pintar todos los números de compás debajo del pentagrama
  % \override Score.BarNumber #'break-visibility = #end-of-line-invisible
  % \override Score.BarNumber #'direction = #DOWN
  % \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0
  \set Score.explicitKeySignatureVisibility = #begin-of-line-visible
  
  % 00: Trumpet I, Trumpet II, Bones I, II & III
  \transpose ees bes \relative c'' {
    
    \key g \major
    
    c4 -. e2 -> c4 -. 
    e4. cis8 ~ cis2
    r8 d -> r d cis4 -. -^ cis -^
    c?4 -- c8 -- bes -> \> ~ bes2
  }
  
  % 05: Alto I, Alto II, Bones I, III & IV
  \relative c'' {
   
    \key d \major
   
    \break
    a4 ( \mf b2 ) d4 -.
    fis4 ( a2 ) ais4 -.
    b4 ( -> a? ) fis -. a -> ~
    a1
    
    \break
    r8 b4. -> a4 -. -^ fis -. -^
    a2 b4. fis8 ~ 
    fis1
  }
  
  % 12: Trumpet I, Trumpet II, Trumpet III, saxs?
  % 16: Trumpet I, Trumpet II, Alto I, II, mix(tenor I, II) -> 1/4B Bones I,II,III
  \transpose ees bes \relative c' {
    
    \key g \major
    
    r4 \mf f8 -- g -> r \< b -- b4 -. \!
    
    \break
    a4 -. \f c2 -> a4 -.
    bes4. g8 ~ g2
    a4 -. a2 -> a4 ( -^ ~ 
    a4. gis8 ~ gis2 )
    
    \break
    g?4 -. b2 -> g4 -.
    fis2 fis4. dis8 ( ~ 
    dis2 d )
    R1
  }
  
  % B0: Alto I, Alto II, Bones I, III & IV
  \relative c'' {
    
    \key d \major
    
    \break
    r8 \mf a4. -> b4 -. -^ d -. -^
    fis8 ( a ~ a2 ) ais8 ( b -> ~ 
    b4 a?8 fis b a fis a -> ~ 
    a1 )
    
    \break
    r8 b4. -> a4 -. -^ fis -. -^
    a2 \times 2/3 { a4 ( ais b  }
    c1 )
  }
  
  % 28: Trumpet I, Trumpet II, Trumpet III, saxs?
  % 32: Trumpet I, Trumpet II, saxs?
  % D0: Trumpet I, Trumpet II, Trumpet III/Alto I, Alto II, Tenor I
  \transpose ees bes \relative c' {
    
    \key g \major
    
    r4 \mf f8 -- g -> r \< b -- b4 -. \!
    
    \break
    a4 -. \f c2 -> a4 -.
    r8 e' ( -> ~ \times 2/3 { e ees d } cis2 )
    r8 a4. -> fis8 a fis a ( ~
    a2 gis )
    
    \break
    r8 g?4. -> b4 -. -^ g4 -. -^
    fis2 fis4. b8 ~ 
    b1
    r4 d,8 ( e -> ) r g ( b4 -. )
    
    \break
    bes4 -. \f d2 -> bes4 -.
    c4 -- a8 e ~ e2
    r4 c'8 ( -> a ) r c ( a4 -. )
    c4 -. -^  c8 c ( -> a -. ) c ( -> a4 -. )
  }
  
  \transpose ees bes \relative c'' {
    
    \set Score.explicitKeySignatureVisibility = #all-visible
    \key f \major
    
    \break
    R1*3
    r4 \mf gis8 -- e -> r gis -- e4 -.
    
    \break
    f4 -. -^ r r2
    R1
    r8 ees' -> r ees d4 -. -^ d -. -^
    des4. c8 -- ~ c a c4 -.
    
    \break
    bes4 -. -^ r r2
    R1*2
    r8 c -> r a b ( c ) c4 -.
    
    \break
    R1*2
    r4 c2 -> \bendBefore a4 -.
    c4 -- bes8 -- b -> r c -- cis4 -.
    
    \break
    d4 -. -^ r r2
    R1*2
    r4 e,8 -- e -- \< e -- e -- e4 -. \!
    
    \break
    R1*2
    r8 ees'4. -> d4 -. -^ r8 cis -> ~
    cis4 c -. -^ r8 cis d4 -.
    
    \break
    d4 -. -^ r r2
    R1*2
  }
  
  % 71: Saxs
  \relative c'' {
    
    \set Score.explicitKeySignatureVisibility = #begin-of-line-visible
    \key c \major
    
    r4 e8 ( \< f \! g [ e ] \> cis ) \! r
      
    \break
    a1
    b1
    r4 c8 ( gis a c d e -> )
    R1
    
    \repeat volta 2 {
      
      \break
      r8 \mf aes,4. -> g8 ( aes ) r f' ~
      f1
      r4 a,?8 ( gis a c d e ~
      e1 )
      
      \break
      e1 ^\markup  "(rall. & conducted)"
      \override BreathingSign.text = \markup { \musicglyph #"scripts.caesura.straight" }
      e2 ( c ) \breathe
      g'4 -. -^ \fermata r r2
      R1
      
      \break
      r4 r8 e, -> ~ e2
      r4 b'8 ( e -> ) r2
      r8 e,8 ( g4 -. ) c2
      r4 f8 ( b, ) r f' ( b,4 -. )
      
      \break
      r8 c4. -> c4 -. -^ a -. -^
      r8 a4. -> c4 -. -^ r
      r8 ^\markup "(A tempo)" g'8 ( bes [ g ] f ees d bes ~
      bes2. ) r4
      
      \break
      c4 -. \f d2 -> c4 -. 
      ees4. c8 ~ c2
    }
    
    \alternative {
      { d4 -. b2 e,4 -^ ~ 
        e2 e'8 ( cis e d ~        
        
        \break
        d1 )
        r8 g,4. r8 g4.
        r4 a8 -- a -- a -- a -- a -- c ->
        R1
        
        \break
        s1
        s1 _ \markup \tiny "(Modulation) \"I think of Elsie to this very day [...]\""
        R1*4 _ \markup \tiny "\"Life is a Cabaret, old Chum\""
        
        \break
        R1*2
        r4 c8 ( gis a c d e -> )
        R1
      }
      
      { \break 
        d4 -. b2 -> g4 -^ ~
        g1}
    }
    
    f'4 -. a2 -> f4 -.
    a4. fis8 ~ fis2
    g4 -. b2 -> e,4 -^ ~ 
    \break
    e2 b \ff
    
    c2 d \glissando
    a1 ->
    r2 g ->
    r g ->
    
    \break
    r8 g'4. -> a4 -. -^ c -. -^
    e ( g2 ) b,8 -- c ->
    r8 d -- c4 -. d8 ( dis ) r e -> ~
    e2 \fermata c4 -^ c -^
  }
}

violin = \relative c'' {

  % Para pintar todos los números de compás debajo del pentagrama
  % \override Score.BarNumber #'break-visibility = #end-of-line-invisible
  % \override Score.BarNumber #'direction = #DOWN
  % \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0
  \set Score.explicitKeySignatureVisibility = #begin-of-line-visible
  
  % 00: Trumpet I, Alto I, Bones I, II & III
  \transpose c bes, \relative c'' {
    
    \key g \major
    e4 -. g2 -> e4 -. 
    g4. e8 ~ e2
    r8 fis -> r fis f4 -. -^ f -^
    e4 -- e8 -- ees -> \> ~ ees2
  }
  
  % 05: Alto I, Alto II, Bones I, III & IV
  \transpose c ees, \relative c'' {
   
    \key d \major
   
    \break
    a4 ( \mf b2 ) d4 -.
    fis4 ( a2 ) ais4 -.
    b4 ( -> a? ) fis -. a -> ~
    a1
    
    \break
    r8 b4. -> a4 -. -^ fis -. -^
    a2 b4. fis8 ~ 
    fis1
  }
  
  % 12: Trumpet I, Trumpet II, Trumpet III, saxs?
  % 16: Trumpet I, Trumpet II, Alto I, II, mix(tenor I, II) -> 1/4B Bones I,II,III
  \transpose c bes, \relative c'' {
    
    \key g \major
    
    r4 \mf a8 -- b -> r \< d -- dis4 -. \!
    
    \break
    e4 -. \f g2 -> e4 -.
    g4. e8 ~ e2
    fis4 -. d2 -> b4 -^ ~ 
    b1
    
    \break
    b4 -. d2 -> b4 -.
    d2 e4. b8 ~ 
    b1
    R1
  }
  
  % B0: Alto I, Alto II, Bones I, III & IV
  \transpose c ees, \relative c'' {
    
    \key d \major
    
    \break
    r8 \mf a4. -> b4 -. -^ d -. -^
    fis8 ( a ~ a2 ) ais8 ( b -> ~ 
    b4 a?8 fis b a fis a -> ~ 
    a1 )
    
    \break
    r8 b4. -> a4 -. -^ fis -. -^
    a2 \times 2/3 { a4 ( ais b  }
    c1 )
  }
  
  % 28: Trumpet I, Trumpet II, Trumpet III, saxs?
  % 32: Trumpet I, Trumpet II, saxs?
  % D0: Trumpet I, Trumpet II, Trumpet III/Alto I, Alto II, Tenor I
  \transpose c bes, \relative c'' {
    
    \key g \major
    
    r4 \mf a8 -- b -> r \< d -- dis4 -. \!
    
    \break
    e4 -. \f g2 -> e4 -.
    r8 g ( -> ~ \times 2/3 { g fis f } e2 )
    r8 fis4. -> d8 fis d b ~
    b1
    
    \break
    r8 b4. -> d4 -. -^ b4 -. -^
    d2 e4. g8 ~ 
    g1
    r4 d,8 ( e -> ) r g ( b4 -. )
    
    \break
    d4 -. \f f2 -> d4 -.
    e4 -- c8 a ~ a2
    r4 c8 ( -> a ) r c ( a4 -. )
    c4 -. -^  c8 c ( -> a -. ) c ( -> a4 -. )
  }
  
  \transpose ees dis \transpose c bes, \relative c'' {
    
    \set Score.explicitKeySignatureVisibility = #all-visible
    \key f \major
    
    \break
    R1*3
    r4 \mf e8 -- c -> r e -- c4 -.
    
    \break
    d4 -. -^ r r2
    R1
    r8 g -> r g f4 -. -^ f -. -^
    e4. ees8 -- ~ ees c ees4 -.
    
    \break
    d4 -. -^ r r2
    R1*2
    r8 e -> r a, dis ( e ) e4 -.
    
    \break
    R1*2
    r4 c2 -> \bendBefore a4 -.
    c4 -- cis8 -- d -> r dis -- e4 -.
    
    \break
    f4 -. -^ r r2
    R1*2
    r4 c8 -- c -- \< c -- c -- c4 -. \!
    
    \break
    R1*2
    r8 g'4. -> f4 -. -^ r8 e -> ~
    e4 ees -. -^ r8 cis d4 -.
    
    \break
    f4 -. -^ r r2
    R1*2
  }
  
  % 68: Saxs
  \transpose ees dis \transpose c ees, \relative c''' {
    
    \set Score.explicitKeySignatureVisibility = #begin-of-line-visible
    \key c \major
    
    r4 g8 ( \< a \! bes [ g ] \> e ) \! r
      
    \break
    e1
    e2 ( ees )
    r4 c8 ( gis a c d e -> )
    R1
    
    \repeat volta 2 {
      
      \break
      r8 \mf c4. -> b8 ( c ) r aes' ~
      aes1
      r4 c,8 ( b c e f g ~
      g1 )
      
      \break
      a2 ^\markup  "(rall. & conducted)" ( aes
      \override BreathingSign.text = \markup { \musicglyph #"scripts.caesura.straight" }
      g e ) \breathe
      b'4 -. -^ \fermata r r2
      R1
      
      \break
      r4 r8 a, -> ~ a2
      r4 e'8 ( g -> ) r2
      r8 a,8 ( c4 -. ) e2
      r4 g8 ( ees ) r g ( ees4 -. )
      
      \break
      r8 e?4. -> e4 -. -^ c -. -^
      r8 c4. -> e4 -. -^ r
      r8 ^\markup "(A tempo)" bes'8 ( d [ bes ] g fis f d ~
      d2. ) r4
      
      \break
      d4 -. \f f2 -> d4 -. 
      fis4. dis8 ~ dis2
    }
    
    \alternative {
      { 
        e4 -. g2 cis,4 -^ ~ 
        cis2 g'8 ( e g f ~        
        
        \break
        f1 )
        r8 g4. r8 g4.
        r4 c,8 -- c -- c -- c -- c -- e ->
        R1
        
        \break
        s1
        s1 _ \markup \tiny "(Modulation) \"I think of Elsie to this very day [...]\""
        R1*4 _ \markup \tiny "\"Life is a Cabaret, old Chum\""
        
        \break
        R1*2
        r4 c8 ( gis a c d e -> )
        R1
      }
      
      { \break 
        e4 -. d2 -> b4 -^ ~
        b1}
    }
    
    a'4 -. c2 -> a4 -.
    c4. a8 ~ a2
    b4 -. d2 -> g,4 -^ ~ 
    \break
    g2 e \ff
    
    f2 f \glissando
    c1 ->
    r2 g' ->
    r g ->
    
    \break
    r8 g4. -> a4 -. -^ c -. -^
    e ( g2 ) cis,8 -- d ->
    r8 f -- d4 -. f8 ( fis ) r g -> ~
    g2 \fermata c,4 -^ c -^
  }
}

bass = \relative c' {

  % Para pintar todos los números de compás debajo del pentagrama
  % \override Score.BarNumber #'break-visibility = #end-of-line-invisible
  % \override Score.BarNumber #'direction = #DOWN
  % \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0	
  
  \clef bass
  \key f \major
  
  g4 \f r g r
  aes r aes aes
  a? r aes aes
  g r \> fis fis
  
  \break
  f? \! r c r
  f r c r
  f r f r
  c c d e
  
  f r f r
  \break
  c r c r
  f r f r
  c' f, g a
  
  bes \f r bes, r
  b r b r
  c r c r
  \break
  d r a' d,
  
  g r g r 
  c, r c' bes
  a r d, r
  g r c, c
  
  \break
  f \mf r f r
  c r c r
  f r f r
  c r c e
  
  f r c r
  \break
  f r f r
  c r c r
  f c' a f
  
  bes \f r bes, r
  b r b r
  c r a' r
  \break
  d, r a' d,
  
  g r g r
  c r c r
  f f e d
  c bes a g
  
  \break
  f \f c' aes f
  bes bes bes bes
  f r c f \>
  e \! r e e
    
  \transpose ees dis \relative c {
    
    \key ees \major
    
    \break
    ees? \mf r ees r
    bes r bes r
    ees r ees r
    bes r bes d
    
    ees r bes r
    \break
    ees r bes r
    ees r ees r
    ees r ees g
    
    aes r aes r
    a r a r
    bes r g r
    \break
    c g e c
    
    f r f r 
    bes, r bes r
    ees ees d c
    bes f' d bes
    
    \break
    ees r ees r
    bes r bes r
    ees r ees r
    bes r bes d
    
    ees r ees r
    \break
    bes r bes r
    r8 bes4. -> c4 -> r8 d -> ~
    d4 ees -> r ees
    
    aes, r aes r
    a r a r
    bes r g' r 
    \break
    c, c d e
    
    f r f r
    bes, r bes r
    ees c' bes g
    ees r r2
    
    \repeat volta 2 {
    
      \break
      aes4 r ees r
      aes r aes r
      ees r bes' r
      ees, r ees d
      
      \break
      c ^\markup  "(rall. & conducted)" r g' r
      \override BreathingSign.text = \markup { \musicglyph #"scripts.caesura.straight" }
      c, r f r \breathe
      bes, \fermata r r2
      r4 bes ^\markup "(tacet 2nd time)" c d
      
      \break
      ees ^\markup "(lento & accel.)" r ees r
      bes r bes r
      ees r ees r
      bes r bes r
      
      \break
      ees r bes r
      ees r ees r
      bes ^\markup "(A tempo)" r bes' r
      ees, r ees g
      
      \break
      aes r aes, r
      a r a r
    }
    
    \alternative {
      { bes r g' r
        c, g' e c
        \break
        
        f r f r
        bes, r bes r 
        ees c' bes g 
        ees r4 r2
        
        \break
        s1
        s1 _ \markup \tiny "(Modulation) \"I think of Elsie to this very day [...]\""
        R1*4 _ \markup \tiny "\"Life is a Cabaret, old Chum\""
        
        \break
        f4 r f r
        bes, r bes r
        ees c' bes g
        ees r r2 }
      
      { \break bes4 r g' r
        c, r d e }
    }
    
    f r f r
    fis r fis r
    
    g r g r
    \break
    c, r e r
    f \ff g aes bes
    c aes f c
    
    bes r bes -> r
    r2 bes4 -> r
    \break
    ees r bes r
    
    ees bes ees e
    f f bes, bes
    ees2 \fermata ees4 -^ ees -^
  }
}



\score {
  <<
    \new StaffGroup <<
      \new Staff \with {midiInstrument = #"violin"} { \transpose f des \violin }
      \new Staff \with {midiInstrument = #"soprano sax"} { \transpose c ees, \transpose f des \altoSaxI }
      \new Staff \with {midiInstrument = #"tenor sax"} { \transpose c bes,, \transpose f des' \tenorSaxI }
      \new Staff \with {midiInstrument = #"tenor sax"} { \transpose c bes,, \transpose f des' \tenorSaxII }
      \new Staff \with {midiInstrument = #"baritone sax"} { \transpose c ees,, \transpose f des' \baritoneSax }
      \new Staff \with {midiInstrument = #"pizzicato strings"} { \transpose f des \bass }
    >>
  >>
  \midi {}
}