\version "2.18.2"

\include "lily-jazz/ignatzek-jazz-chords.ily"
\include "lily-jazz/lily-jazz.ily"

% PLUGINS
% - \eolMark: 		mark al final de línea
% - \bendBefore: 	solución al bendBefore (i.e. dis8 \bendBefore)
% - \rit: 			crea una porción de ritardando que debe terminar como las dinámicas, con un \!
% - \rs: 			negra de improvisación
% - \emptyStave:	Imprime un pentagrama vacío

% LAYOUT
% - El aspecto de los silencios multicompás es como los de las partituras de jazz
% - Margen extra de las articulaciones para evitar la colisión con las ligaduras
% - Glissandos bonitos

% PAPER
% - Se imprime el título, instrumento y número de página en todas las páginas
% - Solo se debe tocar el page-count (si eso). Sale bonito tal cual.

% CAMBIOS
% 03-09-14: Layout
% Ahora las articulaciones van por dentro de las ligaduras. Parece que es la mejor solución
%
% 04-09-14: Layout
% Los creciendos y decreciendos tienen la línea un poco más gruesa
%
% Markup
% Añadidas guías para correcta posición y markup de la indicación de tempo
%
% Plugins
% Añadida función \emptyStave para imprimir un pentagrama vacío.
% En algunas partituras hay un pentagrama vacío antes del coda (lo cual mola bastante, por cierto)
%
% 05-09-14: Plugins
% Añadido a la función \emptyStave el esconder la armadura de la clave, que no estaba.
%
% 08-09-14: Layout
% Ahora no se muestra la cancelación de accidentales al haber un cambio de armadura
%
% 09-09-14: Chords
% Añadida sentencia para que los acordes menores salgan con un guión en vez de con una M minúscula
%
% 10-09-14: Plugins
% Mejorada la función \bendAfter.
% Ahora es genérica, y se puede definir cualquier texto en un lateral que haga las funciones de ornamento


% PLUGINS 1.2
% -----------
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~





% multi-mark-engraver
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
% Aporta la posibilidad de pintar un rehearsal mark al final de una línea y 
% al comienzo de la siguiente.
% http://lists.gnu.org/archive/html/lilypond-user/2011-08/msg00157.html
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
#(define (multi-mark-engraver ctx)
   (let ((texts '())
         (final-texts '())
         (events '()))

     `((start-translation-timestep
        . ,(lambda (trans)
             (set! final-texts '())))

       (listeners
        (mark-event
         . ,(lambda (trans ev)
              (set! events (cons ev events)))))

       (acknowledgers
        (break-alignment-interface
         . ,(lambda (trans grob source)
              (for-each (lambda (mark)
                          (set! (ly:grob-parent mark X) grob))
                        texts))))

       (process-music
        . ,(lambda (trans)
             (for-each
              (lambda (ev)
                (let* ((mark-grob
                        (ly:engraver-make-grob trans 'RehearsalMark ev))
                       (label (ly:event-property ev 'label))
                       (formatter (ly:context-property ctx 'markFormatter)))

                  (if (and (procedure? formatter)
                           (not (markup? label)))
                      (begin
                       (if (not (number? label))
                           (set! label
                                 (ly:context-property ctx 'rehearsalMark)))

                       (if (and (integer? label)
                                (exact? label))
                           (set! (ly:context-property ctx 'rehearsalMark)
                                 (1+ label)))

                       (if (number? label)
                           (set! label (apply formatter (list label ctx)))
                           (ly:warning "rehearsalMark must have
integer value"))))

                  (if (markup? label)
                      (begin
                       (set! (ly:grob-property mark-grob 'text) label)
                       (let ((dir (ly:event-property ev 'direction)))
                         (and (ly:dir? dir)
                              (set! (ly:grob-property mark-grob 'direction)
                                    dir))))
                      (ly:warning "mark label must be a markup object"))

                  (set! texts (cons mark-grob texts))))
              (reverse events))))

       (stop-translation-timestep
        . ,(lambda (trans)
             (if (pair? texts)
                 (let ((staves (ly:context-property ctx 'stavesFound))
                       (priority-index 0))
                   (for-each (lambda (grob)
                               (let ((my-priority (ly:grob-property
grob 'outside-staff-priority 1500)))
                                 (for-each (lambda (stave)

(ly:pointer-group-interface::add-grob grob 'side-support-elements
                                               stave))
                                           staves)
                                 (set! (ly:grob-property grob
'outside-staff-priority) (+ my-priority priority-index))
                                 (set! priority-index (1+ priority-index))
                                 (set! final-texts (cons grob final-texts))))
                             (reverse texts))
                     (set! texts '())
                     (set! events '())))))

        (finalize
         . ,(lambda (trans)
              (and (pair? final-texts)
                   (for-each (lambda (grob)
                               (set! (ly:grob-property grob 'break-visibility)
                                     end-of-line-visible))
                             final-texts)))))))


% Funciones originales
%{
myMark =
#(define-music-function (parser location text) (markup?)
  (make-music 'MarkEvent
              'label text))
			  
toCoda = {
	\tweak #'self-alignment-X #RIGHT
	\tweak #'break-visibility #begin-of-line-invisible
	\myMark \markup { \hspace #1.25 \raise #1.25 \musicglyph #"scripts.coda" }
}
%}

% Función maqueada.
% Se apoya en el código obtenido por consola al ejecutar { \displayMusic { \toCoda }}
eolMark =
#(define-music-function (parser location text) (markup?)
	(make-music 
		'MarkEvent 
		'tweaks (list 
			(cons (quote self-alignment-X) 1)
			(cons (quote break-visibility) #(#t #t #f))
		) 
		'label (markup
			#:line (
				#:hspace 1.25
				#:raise 1.25
				text
			)
		)
	)
)










% rest-score y merge-rests-on-positioning
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
% Permite realizar la fusión de los silencios entre dos voces en un mismo pentagrama.
% https://code.google.com/p/lilypond/issues/detail?id=1228 (fichero merge-rests.ily)
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]

#(define (rest-score r)
  (let ((score 0)
	(yoff (ly:grob-property-data r 'Y-offset))
	(sp (ly:grob-property-data r 'staff-position)))
    (if (number? yoff)
	(set! score (+ score 2))
	(if (eq? yoff 'calculation-in-progress)
	    (set! score (- score 3))))
    (and (number? sp)
	 (<= 0 2 sp)
	 (set! score (+ score 2))
	 (set! score (- score (abs (- 1 sp)))))
    score))

#(define (merge-rests-on-positioning grob)
  (let* ((can-merge #f)
	 (elts (ly:grob-object grob 'elements))
	 (num-elts (and (ly:grob-array? elts)
			(ly:grob-array-length elts)))
	 (two-voice? (= num-elts 2)))
    (if two-voice?
	(let* ((v1-grob (ly:grob-array-ref elts 0))
	       (v2-grob (ly:grob-array-ref elts 1))
	       (v1-rest (ly:grob-object v1-grob 'rest))
	       (v2-rest (ly:grob-object v2-grob 'rest)))
	  (and
	   (ly:grob? v1-rest)
	   (ly:grob? v2-rest)
	   (let* ((v1-duration-log (ly:grob-property v1-rest 'duration-log))
		  (v2-duration-log (ly:grob-property v2-rest 'duration-log))
		  (v1-dot (ly:grob-object v1-rest 'dot))
		  (v2-dot (ly:grob-object v2-rest 'dot))
		  (v1-dot-count (and (ly:grob? v1-dot)
				     (ly:grob-property v1-dot 'dot-count -1)))
		  (v2-dot-count (and (ly:grob? v2-dot)
				     (ly:grob-property v2-dot 'dot-count -1))))
	     (set! can-merge
		   (and
		    (number? v1-duration-log)
		    (number? v2-duration-log)
		    (= v1-duration-log v2-duration-log)
		    (eq? v1-dot-count v2-dot-count)))
	     (if can-merge
		 ;; keep the rest that looks best:
		 (let* ((keep-v1? (>= (rest-score v1-rest)
				      (rest-score v2-rest)))
			(rest-to-keep (if keep-v1? v1-rest v2-rest))
			(dot-to-kill (if keep-v1? v2-dot v1-dot)))
		   ;; uncomment if you're curious of which rest was chosen:
		   ;;(ly:grob-set-property! v1-rest 'color green)
		   ;;(ly:grob-set-property! v2-rest 'color blue)
		   (ly:grob-suicide! (if keep-v1? v2-rest v1-rest))
		   (if (ly:grob? dot-to-kill)
		       (ly:grob-suicide! dot-to-kill))
		   (ly:grob-set-property! rest-to-keep 'direction 0)
		   (ly:rest::y-offset-callback rest-to-keep)))))))
    (if can-merge
	#t
	(ly:rest-collision::calc-positioning-done grob))))










% startParenthesis y endParenthesis
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
% http://comments.gmane.org/gmane.comp.gnu.lilypond.general/64087
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]

% ELIMINADO










% bendBefore
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
% Adaptación
% http://lilypondblog.org/2013/08/adding-ornamentations-to-note-heads-part-1/
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]

%%%
%%% Utilities for defining new grobs, grob properties and music event types
%%% (there should be built-in commands to do that in LilyPond)
%%%
#(define (define-grob-type grob-name grob-entry)
   "Define a new grob and add it to `all-grob-definitions', after
scm/define-grobs.scm fashion.
After grob definitions are added, use:

\\layout {
  \\context {
    \\Global
    \\grobdescriptions #all-grob-descriptions
  }
}

to register them."
   (let* ((meta-entry   (assoc-get 'meta grob-entry))
          (class        (assoc-get 'class meta-entry))
          (ifaces-entry (assoc-get 'interfaces meta-entry)))
     (set-object-property! grob-name 'translation-type? list?)
     (set-object-property! grob-name 'is-grob? #t)
     (set! ifaces-entry (append (case class
                                  ((Item) '(item-interface))
                                  ((Spanner) '(spanner-interface))
                                  ((Paper_column) '((item-interface
                                                     paper-column-interface)))
                                  ((System) '((system-interface
                                               spanner-interface)))
                                  (else '(unknown-interface)))
                                ifaces-entry))
     (set! ifaces-entry (uniq-list (sort ifaces-entry symbol<?)))
     (set! ifaces-entry (cons 'grob-interface ifaces-entry))
     (set! meta-entry (assoc-set! meta-entry 'name grob-name))
     (set! meta-entry (assoc-set! meta-entry 'interfaces
                                  ifaces-entry))
     (set! grob-entry (assoc-set! grob-entry 'meta meta-entry))
     (set! all-grob-descriptions
           (cons (cons grob-name grob-entry)
                 all-grob-descriptions))))

#(define-public (define-grob-property symbol type? description)
   "Define a new grob property.
`symbol': the property name
`type?': the type predicate for this property
`description': the type documentation"
  (set-object-property! symbol 'backend-type? type?)
  (set-object-property! symbol 'backend-doc description)
  symbol)

#(defmacro*-public define-music-type (type-name parents #:rest properties)
     "Add a new music type description to `music-descriptions'
and `music-name-to-property-table'.

`type-name': the music type name (a symbol)
`parents': the parent event classes (a list of symbols)
`properties': a (key . value) property set, which shall contain at least
   description and type properties.
"
   (let ((gproperties (gensym "properties")))
     `(let ((,gproperties (list-copy ',properties)))
        ,@(map (lambda (parent)
                `(define-event-class ',(ly:camel-case->lisp-identifier type-name)
                   ',parent))
               parents)
        (set-object-property! ',type-name
                              'music-description
                              (cdr (assq 'description ,gproperties)))
        (set! ,gproperties (assoc-set! ,gproperties 'name ',type-name))
        (set! ,gproperties (assq-remove! ,gproperties 'description))
        (hashq-set! music-name-to-property-table ',type-name ,gproperties)
        (set! music-descriptions
              (cons (cons ',type-name ,gproperties)
                    music-descriptions)))))

%%%
%%% Grob definition
%%%
#(define (head-ornamentation::print me)
   "Prints a HeadOrnamentation grob, on the left or right side of the
note head, depending on the grob direction."
   (let* ((notes (ly:grob-object me 'elements))
          (y-ref (ly:grob-common-refpoint-of-array me notes Y))
          (x-ref (ly:grob-common-refpoint-of-array me notes X))
          (x-ext (ly:relative-group-extent notes x-ref X))
          (y-ext (ly:relative-group-extent notes y-ref Y))
          (staff-position (ly:grob-staff-position (ly:grob-array-ref notes 0)))
          (y-coord (+ (interval-center y-ext)
                      (if (and (eq? (ly:grob-property me 'shift-when-on-line) #t)
                               (memq staff-position '(-4 -2 0 2 4)))
                          0.5
                          0)))
          (text (ly:text-interface::print me))
          (width (/ (interval-length (ly:stencil-extent text X)) 2.0))
          (x-coord (if (= (ly:grob-property me 'direction) LEFT)
                       (- (car x-ext) width)
                       (+ (cdr x-ext) width))))
     (ly:stencil-translate
      text
      (cons
       (- (- x-coord (ly:grob-relative-coordinate me x-ref X)) 
          (ly:grob-property me 'x-position))
       (- (- y-coord (ly:grob-relative-coordinate me y-ref Y)) 
          (ly:grob-property me 'y-position))
       ))))

%% a new grob property (used to shift an ornamentation when the
%% note head is on a staff line)
#(define-grob-property 'shift-when-on-line boolean?
   "If true, then the ornamentation is vertically shifted when
the note head is on a staff line.")

#(define-grob-property 'x-position number?
   "X position")

#(define-grob-property 'y-position number?
   "Y position")

#(define-grob-type 'HeadOrnamentation
  `((font-size . 0)
    (shift-when-on-line . #f)
    (x-position . 1)
    (y-position . 1)
    (stencil . ,head-ornamentation::print)
    (meta . ((class . Item)
             (interfaces . (font-interface))))))

\layout {
  \context {
    \Global
    \grobdescriptions #all-grob-descriptions
  }
}

%%%
%%% Engraver
%%%
%% The head-ornamentation engraver, with its note-head acknowledger
%% (which add HeadOrnamentation grobs to note heads)
#(define head-ornamentation-engraver
   (make-engraver
    (acknowledgers
     ((note-head-interface engraver note-grob source)
      ;; helper function to create HeadOrnamentation grobs
      (define (make-ornament-grob text direction shift-when-on-line x-position y-position)
        (let ((ornament-grob (ly:engraver-make-grob engraver
                                                    'HeadOrnamentation
                                                    note-grob)))
          ;; use the ornament event text as the grob text property
          (set! (ly:grob-property ornament-grob 'text) text)
          ;; set the grob direction (either LEFT or RIGHT)
          (set! (ly:grob-property ornament-grob 'direction) direction)
          ;; set the shift-when-on-line property using the given value
          (set! (ly:grob-property ornament-grob 'shift-when-on-line)
                shift-when-on-line)
          ;; set the grob x-pos
          (set! (ly:grob-property ornament-grob 'x-position) x-position)
          ;; set the grob y-pos
          (set! (ly:grob-property ornament-grob 'y-position) y-position)
          (ly:pointer-group-interface::add-grob ornament-grob
                                                'elements
                                                note-grob)
          
          ;; the ornamentation is vertically aligned with the note head
          (set! (ly:grob-parent ornament-grob Y) note-grob)
          ;; compute its font size
          (set! (ly:grob-property ornament-grob 'font-size)
                (+ (ly:grob-property ornament-grob 'font-size 0.0)
                   (ly:grob-property note-grob 'font-size 0.0)))
          ;; move accidentals and dots to avoid collision
          (let* ((ornament-stencil (ly:text-interface::print ornament-grob))
                 (ornament-width (interval-length
                                  (ly:stencil-extent ornament-stencil X)))
                 (note-column (ly:grob-object note-grob 'axis-group-parent-X))
                 ;; accidentals attached to the note:
                 (accidentals (and (ly:grob? note-column)
                                   (ly:note-column-accidentals note-column)))
                 ;; dots attached to the note:
                 (dot-column (and (ly:grob? note-column)
                                  (ly:note-column-dot-column note-column))))
            (cond ((and (= direction LEFT) (ly:grob? accidentals))
                   ;; if the ornament is on the left side, and there are
                   ;; accidentals, then increase padding between note
                   ;; and accidentals to make room for the ornament
                   (set! (ly:grob-property accidentals 'padding)
                         ornament-width))
                  ((and (= direction RIGHT) (ly:grob? dot-column))
                   ;; if the ornament is on the right side, and there
                   ;; are dots, then translate the dots to make room for
                   ;; the ornament
                   (set! (ly:grob-property dot-column 'positioning-done)
                         (lambda (grob)
                           (ly:dot-column::calc-positioning-done grob)
                           (ly:grob-translate-axis! grob ornament-width X))))))
          ornament-grob))
      ;; When the note-head event attached to the note-head grob has
      ;; ornamentation events among its articulations, then create a
      ;; HeadOrnamentation grob
      (for-each
       (lambda (articulation)
         (if (memq 'head-ornamentation-event
                   (ly:event-property articulation 'class))
             ;; this articulation is an ornamentation => make the grob
             ;; (either on LEFT or RIGHT direction)
             (begin
               (if (markup? (ly:event-property articulation 'left-text))
                   (make-ornament-grob
                    (ly:event-property articulation 'left-text)
                    LEFT
                    (ly:event-property articulation 'shift-when-on-line)
                    (ly:event-property articulation 'x-position)
                    (ly:event-property articulation 'y-position)))
               (if (markup? (ly:event-property articulation 'right-text))
                   (make-ornament-grob
                   (ly:event-property articulation 'right-text)
                   RIGHT
                   (ly:event-property articulation 'shift-when-on-line)
                   (ly:event-property articulation 'x-position)
                   (ly:event-property articulation 'y-position))))))
         (ly:event-property (ly:grob-property note-grob 'cause) 'articulations))))))

\layout {
  \context {
    \Score
    \consists #head-ornamentation-engraver
  }
}

%%%
%%% Event type definition
%%%
#(define-music-type HeadOrnamentationEvent (music-event)
   (description . "Print an ornamentation at a note head side")
   (types . (general-music post-event event head-ornamentation-event)))

bendBefore =
#(make-music 'HeadOrnamentationEvent
  'x-position 1
  'y-position 2
  'shift-when-on-line #f
  'left-text #{ \markup { \fontsize #0 \rotate #5 \musicglyph #"brackettips.up"} #})

parenth =
#(make-music 'HeadOrnamentationEvent
  'x-position -.1
  'y-position -.2
  'shift-when-on-line #f
  'left-text #{ \markup { \fontsize #2 \rotate #270 \musicglyph #"ties.lyric.short"} #}
  'right-text #{ \markup { \fontsize #2 \rotate #90 \musicglyph #"ties.lyric.short"} #})










% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
% Funciones propias
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]





% Índice con borde circular y un cómodo espacio interior (y no el que viene por defecto)
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
#(define-markup-command (boxindex layout props text) (markup?)
	(interpret-markup layout props
		(markup #:huge #:override '(box-padding . 0.5) #:box text)
	)
)

#(define-markup-command (boxed layout props text) (markup?)
	(interpret-markup layout props
		(markup #:huge #:override '(box-padding . 0.9) #:box text)
	)
)

% Ritardando
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
rit =
#(make-music 'CrescendoEvent
             'span-direction START
             'span-type 'text
             'span-text "rit.")

% Negra oblicua
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
rs = {
  \once \override Rest #'stencil = #ly:percent-repeat-item-interface::beat-slash
  \once \override Rest #'thickness = #0.48
  \once \override Rest #'slope = #1.7
  r4
}

% Pentagrama vacío
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
emptyStave = {
  \once \override Staff.KeySignature.stencil = ##f
  \once \override Staff.Clef.stencil = ##f
  \once \override Score.BarNumber.break-visibility = #all-invisible
  \break
  s1
  \once \override Staff.BarLine.stencil = ##f
  \once \override Score.BarNumber.break-visibility = #begin-of-line-visible
  \break
}










% Añadido de funciones al contexto
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
\layout {
  \context {
    \Score		
    
    % multi-mark-engraver
    \remove "Mark_engraver"
    \consists #multi-mark-engraver
    \consists "Tweak_engraver"
    
    % merge-rests-on-positioning
    \override RestCollision #'positioning-done = #merge-rests-on-positioning
    
    % aspecto de los silencios multicompás
    \override MultiMeasureRest.expand-limit = #1
    
    % margen extra de las articulaciones para evitar la colisión con las ligaduras
    \override Script.padding = #0.5
    \override Script.avoid-slur = #'inside
    
    % glissando bonito
    \override Glissando.style = #'trill
    \override Glissando.bound-details.left.padding = #1.7
    
    % dinámicas un poco mas gruesas
    \override Hairpin.thickness = #1.75
    \override Hairpin.bound-padding = #2
    
    % Esconde la cancelacion de accidentales en el cambio de armadura
    \override KeyCancellation.break-visibility = #'#(#f #f #f)
  }
}












% LAYOUT
% ------
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~






\header {
  title = "Gril from Ipanema"
  instrument = "Baritone Sax"
  composer = "Antonio Carlos JOBIM & Vinicius De MORAES"
  arranger = "Arr: Roger HOLMES"
  poet = ""
  tagline = ""
}

\paper {
  % El definir otro tipo de fuente no permite cambiar el tamaño global del staff. Es un bug conocigo
  #(define fonts (make-pango-font-tree "LilyJAZZText" "Nimbus Sans" "Luxi Mono" (/ 20 20)))
  
  % distancia entre la parte superior de la página y el título
  top-markup-spacing #'basic-distance = 5
  
  % distancia entre el título y el primer sistema
  markup-system-spacing #'basic-distance = 25
  
  % distancia entre el texto y el primer sistema 
  top-system-spacing #'basic-distance = 15
  
  % márgen inferior
  bottom-margin = 20
    
  % número de páginas
  % page-count = 1
  
  ragged-last-bottom = ##f 
  
  oddHeaderMarkup = \markup {
    \on-the-fly #not-first-page \fill-line {
      \fromproperty #'page:page-number-string
      \fromproperty #'header:title
      \fromproperty #'header:instrument
    }
  }
  
  evenHeaderMarkup = \markup {
    \on-the-fly #not-first-page \fill-line {
      \fromproperty #'header:instrument
      \fromproperty #'header:title
      \fromproperty #'page:page-number-string
    }
  }
}










% MUSIC PARTS
% -----------
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~





% scoreMarkup
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
scoreMarkup = \new Staff \with {
   \RemoveEmptyStaves
}
{
  % Oculta el primer pentagrama del markup
  \override Staff.VerticalAxisGroup #'remove-first = ##t
  
  % Indicación de tiempo
  \once \override Score.RehearsalMark.X-offset = #5
  \mark \markup \normalsize "(Moderate Latin Feel)"
  s1*8
  
  \mark \markup \boxed "9"
  s1*8
  
  \mark \markup \boxed "17"
  s1*8
  
  \mark \markup {
    \column { \boxed "25" }
    \hspace #2
    \column { \vspace #-.3 \jazzglyph #'"scripts.varsegnojazz" }
  }
  s1*8
  
  \mark \markup \boxed "33"
  s1*8
  
  \mark \markup \boxed "41"
  s1*6
  
  \once \override Score.RehearsalMark.self-alignment-X = #RIGHT
  \mark \markup { 
    \column { \normalsize "To Coda" }
    \hspace #2
    \column { \vspace #-.3 \normalsize \jazzglyph #'"scripts.varcodajazz" }
  }
  s1*2
  
  \bar "||"
  \mark \markup \boxed "49"
  s1*8
  
  \mark \markup \boxed "57"
  s1*8
  
  \bar "||"
  \eolMark \markup "(D.S. al Coda)"
  s1*1
  
  \once \override Score.RehearsalMark.self-alignment-X = #LEFT
  \mark \markup { 
    \column { \vspace #-.3 \normalsize \jazzglyph #'"scripts.varcodajazz" }
    \hspace #1
    \column { \normalsize "CODA" }
  }
  s1*6
  
  \bar "|."
}





% scoreChords
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
scoreChords = \chords {
    
  \set minorChordModifier = \markup { "-" }
  
  % c1:m f2:m g2:m7
}





% music
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
baritoneSax = \relative c'' {

  % Para pintar todos los números de compás debajo del pentagrama
  % \override Score.BarNumber #'break-visibility = #end-of-line-invisible
  % \override Score.BarNumber #'direction = #DOWN
  % \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0	
  \set Score.explicitKeySignatureVisibility = #begin-of-line-visible
  
  \set Score.tempoHideNote = ##t
  \tempo 4 = 130
  
  % 00: bones
  \transpose ees c \relative c' {
    
    \key f \major 
    
    f4 -- \mf f -. r8 f4 -. aes8 -> ~
    aes4. aes8 -- ~ aes aes -- aes4 -.
    g4 -- g -. r8 g4 -. ges8 -> ~
    ges1
    
    \break
    f4 -- f -. r8 f4 -. aes8 -> ~
    aes4. aes8 -- ~ aes aes -- aes4 -.
    g4 -- g -. r8 g4 -. ges8 ->
    r4 ges2. ( ->
    
    \break
    f1 ~ \>
    f2. ) r4 \!
    R1*4
    f'4 -- \mp f -. r8 f4 -. e8 -> ~
    << e1 { s2 \< s2 \> } >>
    
    \break
    g4 -- \mp g -. r8 e4 -. g8 -.
    R1
    f4 -- f -. r8 d4 -. f8 -.
    R1
    
    \break
    r4 r8 g -. r4 r8 e -.
    r4 r8 ges -. r2
    f2 ~ f8 f4 -. \mf f8 -> ~
    f1
  }
  
  % 25: saxs
  \relative c'' {
    
    \key d \major
    
    \break
    r4 bes8 ( d ees4 bes8 ees, ~
    ees1
    aes1 )
    r2 r8 gis4 -> dis8 -> ~
    
    \break
    dis1
  }
  
  % 30: bones
  \transpose ees c \relative c'' {
    
    \key f \major
    r2 e \glissando
    fis1 ~
    fis2 r8 d4 -> f?8 -> ~
    
    \break
    f1
  }
  
  % 34: saxs
  \relative c' {
    
    \key d \major
    
    \times 2/3 { r4 f e } \times 2/3 { ees d cis }
    c2 
  }
  
  % 35 1/2: bones
  \transpose ees c \relative c'' {
    
    \key f \major
    
    r8 e?4 -> g8 ( -> ~
    g ees g2. )
    
    \break
    r4 a, -. r8 a -. r4
    r8 d4. -- -> d4 -. r
    r4 g4 -. r8 g,8 -. r4
    e'8 -. r e2. -> ~
    
    \break
    e2 r8 e4 -> \mp f8 ->
    R1
    f4 -- f -. r8 d4 -. f8 -.
    R1
    
    \break
    r8 f -. r4 d4 -. r
    r4 e4 -. r2
    R1
  }
  
  % 48: saxs
  \relative c'' {
    
    \key d \major
    
    r8 \mf g ( a [ bes ] ~ bes c d e )
    
    \key f \major
    
    \break
    g4 ( e8 e ~ e d e d
    g4 \glissando e8 e ~ e2 )
    r8 f4. ( -> \glissando b,8 ais b f' -> ~
    f4 \glissando b,8 b ~ b2 )
    
    \break
    r8 f' ( -> d [ cis ] d f -> d e -> ~
    e4 c8 e -> ~ e c bes4 -. )
    r4 f2. ( ->
    fis2. ) r4
    
    \break
    f?8 ( a e' c ~ c e16 c bes8 g
    f2. ) r4 
    r8 b'4. ( -> \glissando e,8 f cis e ~ 
    e d g,2 -> ) r4
    
    \break
    r4 g8 -- bes -. r8 d4 -. a'8 (
    e4 c8 fis, ~ fis2
    f?2. ) r4
    f4 -- f8 -- bes, -. r bes4. -- ->
    
    \set Staff.explicitKeySignatureVisibility = #end-of-line-visible
    \key g \major
    
    \stopStaff
    \once \override Staff.KeySignature.break-visibility = #all-invisible
    \once \override Staff.Clef.break-visibility = #all-invisible
    \once \override Score.BarNumber.break-visibility = #all-invisible
    \break
    s1
    
    \once \override Score.BarNumber.break-visibility = #all-visible
    \once \override Score.BarLine.break-visibility = #all-invisible
    \set Staff.explicitKeySignatureVisibility = #all-visible
    
    \startStaff
    \clef treble
    \set Score.currentBarNumber = #65
    
    \key g \major
    
    d2. r8 cis8 -> ~ 
    cis2. r4
    d2. r8 cis8 -> ~ 
    \break
    cis2. r4
    
    d2 r8 d4 -> d8 -> ~
    d4. d8 -> ~ d2 \fermata
  }
}




% score
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
\score {
  <<
    \scoreMarkup
    \scoreChords
    \new StaffGroup <<
      \new Staff { \baritoneSax }
    >>
  >>
}


tenorSaxII = \relative c'' {

  % Para pintar todos los números de compás debajo del pentagrama
  % \override Score.BarNumber #'break-visibility = #end-of-line-invisible
  % \override Score.BarNumber #'direction = #DOWN
  % \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0	
  \set Score.explicitKeySignatureVisibility = #begin-of-line-visible
  
  % 00: bones
  \transpose bes c' \relative c'' {
    
    \key f \major 
    
    a4 -- \mf a -. r8 a4 -. ges8 -> ~
    ges4. ges8 -- ~ ges ges -- ges4 -.
    f4 -- f -. r8 f4 -. e8 -> ~
    e1
    
    \break
    a4 -- a -. r8 a4 -. ges8 -> ~
    ges4. ges8 -- ~ ges ges -- ges4 -.
    f4 -- f -. r8 f4 -. e8 ->
    r4 e2. ~ ->
    
    \break
    e1 ~ \>
    e2.  r4 \!
    R1*4
    a4 -- \mp a -. r8 a4 -. aes8 -> ~
    << aes1 { s2 \< s2 \> } >>
    
    \break
    a?4 -- \mp a -. r8 g4 -. a8 -.
    R1
    a4 -- a -. r8 f4 -. a8 -.
    R1
    
    \break
    r4 r8 bes -. r4 r8 aes -.
    r4 r8 bes -. r2
    a?2 ~ a8 a4 -. \mf a8 -> ~
    a1
  }
  
  % 25: saxs
  \relative c'' {
    
    \key g \major
    
    \break
    r4 g8 ( bes c4 g8 bes ~
    bes1
    aes1 )
    r2 r8 dis4 -> ais8 -> ~
    
    \break
    ais1
  }
  
  % 30: bones
  \transpose bes c' \relative c' {
    
    \key f \major
    r2 fis \glissando
    a1 ~
    a2 r8 fis4 -> a8 -> ~
    
    \break
    a1
  }
  
  % 34: saxs
  \relative c'' {
    
    \key g \major
    
    \times 2/3 { r4 aes g } \times 2/3 { fis f e }
    a2 
  }
  
  % 35 1/2: bones
  \transpose bes c' \relative c'' {
    
    \key f \major
    
    r8 g4 -> bes8 ( -> ~
    bes g bes2. )
    
    \break
    r4 g -. r8 g -. r4
    r8 fis4. -- -> fis4 -. r
    r4 bes4 -. r8 f?8 -. r4
    ges8 -. r ges2. ( ->
    
    \break
    g?2 ) r8 g4 -> \mp a8 ->
    R1
    a4 -- a -. r8 f4 -. a8 -.
    R1
    
    \break
    r8 a -. r4 f4 -. r
    r4 ges4 -. r2
    R1
  }
  
  % 48: saxs
  \relative c'' {
    
    \key g \major
    
    r8 \mf c ( d [ ees ] ~ ees f g a )
    
    \key bes \major
    
    \break
    d,4 ( c8 c ~ c d c d
    d4 \glissando c8 c ~ c2 )
    r8 c4. ( -> \glissando bes8 a bes c -> ~
    c4 \glissando bes8 bes ~ bes2 )
    
    \break
    r8 c ( -> bes [ a ] bes c -> bes cis -> ~
    cis4 a8 cis -> ~ cis a f4 -. )
    r4 a2. ( ->
    a2. ) r4
    
    \break
    d,8 ( f c' a ~ a c16 a g8 ees
    a2. ) r4 
    r8 e'4. ( -> \glissando a,8 bes fis a ~ 
    a g c2 -> ) r4
    
    \break
    r4 ees,8 -- g -. r8 bes4 -. ees8 (
    des4 a8 a ~ a2 ~
    a2. ) r4
    aes4 -- aes8 -- g -. r des'4. -- ->
    
    \set Staff.explicitKeySignatureVisibility = #end-of-line-visible
    \key g \major
    
    \stopStaff
    \once \override Staff.KeySignature.break-visibility = #all-invisible
    \once \override Staff.Clef.break-visibility = #all-invisible
    \once \override Score.BarNumber.break-visibility = #all-invisible
    \break
    s1
    
    \once \override Score.BarNumber.break-visibility = #all-visible
    \once \override Score.BarLine.break-visibility = #all-invisible
    \set Staff.explicitKeySignatureVisibility = #all-visible
    
    \startStaff
    \clef treble
    \set Score.currentBarNumber = #65
    
    \key g \major
    
    b2. r8 ais8 -> ~ 
    ais2. r4
    b2. r8 ais8 -> ~ 
    \break
    ais2. r4
    
    b2 r8 b4 -> b8 -> ~
    b4. fis8 -> ~ fis2 \fermata
  }
}

tenorSaxI = \relative c'' {

  % Para pintar todos los números de compás debajo del pentagrama
  % \override Score.BarNumber #'break-visibility = #end-of-line-invisible
  % \override Score.BarNumber #'direction = #DOWN
  % \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0	
  \set Score.explicitKeySignatureVisibility = #begin-of-line-visible
  
  % 00: bones
  \transpose bes c' \relative c'' {
    
    \key f \major 
    
    c4 -- \mf c -. r8 c4 -. c8 -> ~
    c4. c8 -- ~ c c -- c4 -.
    bes4 -- bes -. r8 bes4 -. bes8 -> ~
    bes1
    
    \break
    c4 -- c -. r8 c4 -. c8 -> ~
    c4. c8 -- ~ c c -- c4 -.
    bes4 -- bes -. r8 bes4 -. bes8 ->
    r4 bes2. ( ->
    
    \break
    a1 ~ \>
    a2. ) r4 \!
    R1*4
    c4 -- \mp c -. r8 c4 -. c8 -> ~
    << c1 { s2 \< s2 \> } >>
    
    \break
    c4 -- \mp c -. r8 a4 -. c8 -.
    R1
    b4 -- b -. r8 g4 -. b8 -.
    R1
    
    \break
    r4 r8 d -. r4 r8 bes? -.
    r4 r8 c -. r2
    c2 ~ c8 c4 -. \mf c8 -> ~
    c1
  }
  
  % 25: saxs
  \relative c'' {
    
    \key g \major
    
    \break
    r4 bes8 ( c ees4 c8 g ~
    g1
    f1 )
    r2 r8 b?4 -> b8 -> ~
    
    \break
    b1
  }
  
  % 30: bones
  \transpose bes c' \relative c'' {
    
    \key f \major
    r2 a \glissando
    c1 ~
    c2 r8 a4 -> bes8 -> ~
    
    \break
    bes1
  }
  
  % 34: saxs
  \relative c'' {
    
    \key g \major
    
    \times 2/3 { r4 des c } \times 2/3 { c b bes }
    c2 
  }
  
  % 35 1/2: bones
  \transpose bes c' \relative c'' {
    
    \key f \major
    
    r8 bes4 -> des8 ( -> ~
    des bes des2. )
    
    \break
    r4 c -. r8 a -. r4
    r8 c4. -- -> c4 -. r
    r4 d4 -. r8 bes8 -. r4
    bes8 -. r bes2. ( ->
    
    \break
    a2 ) r8 a4 -> \mp c8 ->
    R1
    b4 -- b -. r8 g4 -. b8 -.
    R1
    
    \break
    r8 bes? -. r4 g4 -. r
    r4 bes4 -. r2
    R1
  }
  
  % 48: saxs
  \relative c'' {
    
    \key g \major
    
    r8 \mf c ( d [ ees ] ~ ees f g a )
    
    \key bes \major
    
    \break
    f4 ( d8 d ~ d bes d bes
    f'4 \glissando d8 d ~ d2 )
    r8 e4. ( -> \glissando c8 b c e -> ~
    e4 \glissando c8 c ~ c2 )
    
    \break
    r8 ees ( -> c [ b ] c ees -> c ees -> ~
    ees4 des8 ees -> ~ ees des a4 -. )
    r4 c2. ( ->
    des2. ) r4
    
    \break
    f,8 ( a d c ~ c d16 c bes8 g
    d'2. ) r4 
    r8 e4. ( -> \glissando a,8 bes fis a ~ 
    a g c2 -> ) r4
    
    \break
    r4 g8 -- bes -. r8 c4 -. g'8 (
    ees4 des8 ees ~ ees2
    d2. ) r4
    des4 -- des8 -- des -. r ees4. -- ->
    
    \set Staff.explicitKeySignatureVisibility = #end-of-line-visible
    \key d \major
    
    \stopStaff
    \once \override Staff.KeySignature.break-visibility = #all-invisible
    \once \override Staff.Clef.break-visibility = #all-invisible
    \once \override Score.BarNumber.break-visibility = #all-invisible
    \break
    s1
    
    \once \override Score.BarNumber.break-visibility = #all-visible
    \once \override Score.BarLine.break-visibility = #all-invisible
    \set Staff.explicitKeySignatureVisibility = #all-visible
    
    \startStaff
    \clef treble
    \set Score.currentBarNumber = #65
    
    \key d \major
    
    d2. r8 d8 -> ~ 
    d2. r4
    d2. r8 d8 -> ~ 
    \break
    d2. r4
    
    d2 r8 fis4 -> d8 -> ~
    d4. b8 -> ~ b2 \fermata
  }
}

altoSaxI = \relative c'' {

  % Para pintar todos los números de compás debajo del pentagrama
  % \override Score.BarNumber #'break-visibility = #end-of-line-invisible
  % \override Score.BarNumber #'direction = #DOWN
  % \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0	
  \set Score.explicitKeySignatureVisibility = #begin-of-line-visible
  
  % 00: bones
  \transpose ees c \relative c'' {
    
    \key f \major 
    
    e4 -- \mf e -. r8 e4 -. d8 -> ~
    d4. d8 -- ~ d d -- d4 -.
    d4 -- d -. r8 d4 -. c8 -> ~
    c1
    
    \break
    e4 -- e -. r8 e4 -. d8 -> ~
    d4. d8 -- ~ d d -- d4 -.
    d4 -- d -. r8 d4 -. c8 ->
    r4 c2. -> ~
    
    \break
    c1 ~ \>
    c2. r4 \!
    R1*4
    e4 -- \mp e -. r8 e4 -. d8 -> ~
    << d1 { s2 \< s2 \> } >>
    
    \break
    e4 -- \mp e -. r8 c4 -. e8 -.
    R1
    e4 -- e -. r8 b4 -. e8 -.
    R1
    
    \break
    r4 r8 f -. r4 r8 c -.
    r4 r8 e -. r2
    e2 ~ e8 e4 -. \mf d8 -> ~
    d1
  }
  
  % 25: saxs
  \relative c'' {
    
    \key d \major
    
    \break
    r4 bes8 ( d g4 f8 bes, ~
    bes1 ~
    bes1 )
    r2 r8 dis4 -> cis8 -> ~
    
    \break
    cis1
  }
  
  % 30: bones
  \transpose ees c \relative c'' {
    
    \key f \major
    r2 cis \glissando
    e1 ~
    e2 r8 c4 -> d8 -> ~
    
    \break
    d1
  }
  
  % 34: saxs
  \relative c''' {
    
    \key d \major
    
    \times 2/3 { r4 g fis } \times 2/3 { e fis e }
    d2 
  }
  
  % 35 1/2: bones
  \transpose ees c \relative c'' {
    
    \key f \major
    
    r8 des4 -> f8 ( -> ~
    f des f2. )
    
    \break
    r4 e -. r8 c -. r4
    r8 e4. -- -> ees4 -. r
    r4 f4 -. r8 d8 -. r4
    c8 -. r c2. -> ~
    
    \break
    c2 r8 c4 -> \mp e8 ->
    R1
    e4 -- e -. r8 b4 -. e8 -.
    R1
    
    \break
    r8 d -. r4 bes4 -. r
    r4 c4 -. r2
    R1
  }
  
  % 48: saxs
  \relative c'' {
    
    \key d \major
    
    r8 \mf g ( a [ bes ] ~ bes c d e )
    
    \key f \major
    
    \break
    g4 ( e8 e ~ e d e d
    g4 \glissando e8 e ~ e2 )
    r8 g4. ( -> \glissando e8 dis e g -> ~
    g4 \glissando e8 e ~ e2 )
    
    \break
    r8 f ( -> d [ cis ] d f -> d e -> ~
    e4 c8 e -> ~ e c bes4 -. )
    r4 c2. -> ~
    c2. r4
    
    \break
    a8 ( c g' e ~ e g16 e d8 c
    e2. ) r4 
    r8 b'4. ( -> \glissando e,8 f cis e ~ 
    e d g,2 -> ) r4
    
    \break
    r4 g8 -- bes -. r8 d4 -. a'8 (
    e4 c8 a' ~ a2
    g2. ) r4
    f4 -- f8 -- f -. r fis4. -- ->
    
    \set Staff.explicitKeySignatureVisibility = #end-of-line-visible
    \key d \major
    
    \stopStaff
    \once \override Staff.KeySignature.break-visibility = #all-invisible
    \once \override Staff.Clef.break-visibility = #all-invisible
    \once \override Score.BarNumber.break-visibility = #all-invisible
    \break
    s1
    
    \once \override Score.BarNumber.break-visibility = #all-visible
    \once \override Score.BarLine.break-visibility = #all-invisible
    \set Staff.explicitKeySignatureVisibility = #all-visible
    
    \startStaff
    \clef treble
    \set Score.currentBarNumber = #65
    
    \key d \major
    
    e2. r8 e8 -> ~ 
    e2. r4
    e2. r8 e8 -> ~ 
    \break
    e2. r4
    
    e2 r8 a4 -> e8 -> ~
    e4. gis8 -> ~ gis2 \fermata
  }
}

violin = \relative c'' {

  % Para pintar todos los números de compás debajo del pentagrama
  % \override Score.BarNumber #'break-visibility = #end-of-line-invisible
  % \override Score.BarNumber #'direction = #DOWN
  % \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0	
  \set Score.explicitKeySignatureVisibility = #begin-of-line-visible
  
  % 00: alto I
  \transpose c ees, \relative c' {
    
    \key d \major
    
    r4 \mf fis8 ( a ~ a e' cis d ~
    d2 ~ d8 ) b ( d fis ~
    fis4 \glissando b,8 d ~ d b e f ~
    f1 )
    
    \break
    r4 fis,8 ( a ~ a e' cis d ~
    d1 )
    r8 b ( d [ e ] fis e d f ) ->
    r4 f2. ( ->
    
    \break
    e1 ) \>
    r2 \! r4 \mp fis,8 ( a 
    gis1 ~ 
    gis2 ) e8 ( fis gis b ~ 
    
    \break
    b2 \appoggiatura { c16 cis } d4. cis8 ~
    cis2 \appoggiatura { d16 e } f2
    e1 ~ 
    e )
    
    \break
    R1
    r2 \! r4 \mp fis,8 ( a 
    gis1 ~ 
    gis2 ) b8 ( fis' e b ~ 
    
    \break
    b2 ~ b4. a8 ~
    a1 )
  }
  
  % 23: trumpet I
  \transpose c bes, \relative c'' {
     
     \key g \major
     
     R1
     r8 \mf a ( d [ b ] fis g a g ~
     
     \break
     g1 \> )
  }
  
  % 26: bone I
  \relative c' {
     
     \key f \major
     
     R1*1 \!
     r2 r4 \mf dis8 cis
     dis4 -- cis8 b ~ b4. cis8 -> ~
     
     \break
     cis4
  }
  
  % 29 1/4: trumpet I
  \transpose c bes, \relative c' {
     
     \key g \major
     
     dis8 ( ais' ~ ais4 gis8 dis ~
     dis2. ) r4     
  }
  
  % 31: alto I
  \transpose c ees, \relative c'' {
    
    \key d \major
    
    r2 r4 a8 ( cis
    eis4 \glissando cis8 fis ~ fis2 )
    
    \break
    R1
    \times 2/3 { r4 g fis } \times 2/3 { e fis e }
    d2 r
    r4 g,8 ( bes d2
    
    \break
    cis?1 )
    r2 c (
    b1 )
    R1
    
    \break
    R1
    r4 \mp a8 ( fis e fis a gis ~
    gis1 ~
    gis2 ) r4 r8 b8 ( ~ ->
    
    \break
    b1 
    dis2 ) \breathe f (
    e4 )
  }
  
  % 47: trumpet I
  \transpose c bes, \relative c'' {
     
     \key g \major
     
     a8 ( \mf b ~ b d b d ~
     d2 c )
     
     \once \set Score.explicitKeySignatureVisibility = #all-visible
     \key bes \major
     
     \break
     R1
     r2 r8 f4 -. e8 -.
     R1
     r2 r8 c4 -. ees?8 -.
     
     \break
     R1*2
     r2 \mf d,8 ( f g f 
     aes f aes f ~ f2 )
     
     \break
     R1
     r4 d'8 -- f -. r d4 -. bes8
     c1 ~ -> \fp \<
     c2 \! r8 g'4 -> f8 ~ ->
     
     \break
     f4 f8 -- f -. r2
     r8 f -^ r4 r2
     r4 f,8 ( g a bes c d )
     f4 -- f8 -- f -. r g4. -- ->
     
     \set Staff.explicitKeySignatureVisibility = #end-of-line-visible
     \key g \major
  }
  
  % 65: trumpet I
  \transpose c bes, \relative c'' {
    
    \stopStaff
    \once \override Staff.KeySignature.break-visibility = #all-invisible
    \once \override Staff.Clef.break-visibility = #all-invisible
    \once \override Score.BarNumber.break-visibility = #all-invisible
    s1
    
    \once \override Score.BarNumber.break-visibility = #all-visible
    \once \override Score.BarLine.break-visibility = #all-invisible
    \set Staff.explicitKeySignatureVisibility = #all-visible
    
    \startStaff
    \clef treble
    \set Score.currentBarNumber = #65
    
    \key g \major
    
    a4 a8 ( b ~ b d b d ~
    d2. ) r4
    r a8 ( b ~ b d b d ~
    \break
    d2. ) r4
    
    r4 a8 ( b ~ b2 )
    r8 e ( d [ cis ] ~ cis2 ) \fermata
  }
}



\score {
  <<
    \new StaffGroup <<
      \new Staff \with {midiInstrument = #"violin"} { \violin }
      \new Staff \with {midiInstrument = #"alto sax"} { \transpose c ees, \altoSaxI }
      \new Staff \with {midiInstrument = #"tenor sax"} { \transpose c bes,, \tenorSaxI }
      \new Staff \with {midiInstrument = #"tenor sax"} { \transpose c bes,, \tenorSaxII }
      \new Staff \with {midiInstrument = #"baritone sax"} { \transpose c ees,, \baritoneSax }
    >>
  >>
  \midi {}
}