\version "2.18.2"

\include "lily-jazz/ignatzek-jazz-chords.ily"
\include "lily-jazz/lily-jazz.ily"

\language "english"

% PLUGINS
% - \eolMark: 		mark al final de línea
% - \bendBefore: 	solución al bendBefore (i.e. dis8 \bendBefore)
% - \rit: 			crea una porción de ritardando que debe terminar como las dinámicas, con un \!
% - \rs: 			negra de improvisación
% - \emptyStave:	Imprime un pentagrama vacío

% LAYOUT
% - El aspecto de los silencios multicompás es como los de las partituras de jazz
% - Margen extra de las articulaciones para evitar la colisión con las ligaduras
% - Glissandos bonitos

% PAPER
% - Se imprime el título, instrumento y número de página en todas las páginas
% - Solo se debe tocar el page-count (si eso). Sale bonito tal cual.

% CAMBIOS
% 03-09-14: Layout
% Ahora las articulaciones van por dentro de las ligaduras. Parece que es la mejor solución
%
% 04-09-14: Layout
% Los creciendos y decreciendos tienen la línea un poco más gruesa
%
% Markup
% Añadidas guías para correcta posición y markup de la indicación de tempo
%
% Plugins
% Añadida función \emptyStave para imprimir un pentagrama vacío.
% En algunas partituras hay un pentagrama vacío antes del coda (lo cual mola bastante, por cierto)
%
% 05-09-14: Plugins
% Añadido a la función \emptyStave el esconder la armadura de la clave, que no estaba.
%
% 08-09-14: Layout
% Ahora no se muestra la cancelación de accidentales al haber un cambio de armadura
%
% 09-09-14: Chords
% Añadida sentencia para que los acordes menores salgan con un guión en vez de con una M minúscula
%
% 10-09-14: Plugins
% Mejorada la función \bendAfter.
% Ahora es genérica, y se puede definir cualquier texto en un lateral que haga las funciones de ornamento
%
% 15-09-14: Layout
% Añadido un poco más de margen a los reguladores
%
% 16-09-14: Plugins
% Mejorada la función para imprimir barras de improvisación.
% Antes se llamaba al comando \rs para pintar una barra que ocupaba una negra
% Se reemplaza \rs por \restImproOn y \restImproOff, siendo esta opción mucho más flexible.
% Cuando se activa \restImproOn, todo silencio se convierte en barra de improvisación, respetando su duración.
%
% 20-09-14: Plugins
% Mejorada la función para imprimir barras de improvisación.
% Se reemplaza \restImproOn y \restImproOff por \improOn y \improOff
% Ahora, también las notas del pentagrama salen como barra de improvisación.
% (los comandos \improvisationOn e \improvisationOff de lilypond no se llevan bien con LilyJAZZ)
% Se ha añadido un grob correspondiente a los slash de la fuente LilyJAZZ en el archivo lily-jazz.ly
%
% 20-09-14: Layout
% Comentada la línea que oculta la cancelación de accidentales al haber cambio de armadura.
% Realmente, la cancelación es muy bienvenida.
%
% 01-10-14: Plugins
% Renombrada la función \emptyStave a \emptyStaff (stave es el plural de staff) y 
% corregida (ya no muestra el número de compás).
% Recordar que después de usar \emptyStaff se necesita redefinir el 
% número de compás con \set Score.currentBarNumber = #xx
%
% Añadida la función \emptyBar que oculta un compás entero. Se puede usar para crear un 
% efecto de indentación en un pentagrama dado.
% Recordar que después de usar \emptyBar se necesita:
% - redefinir el número de compás con \set Score.currentBarNumber = #xx
% - volver a pintar la clave con \clef
% - volver a pintar la armadura con \key


% PLUGINS 1.2
% -----------
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~





% multi-mark-engraver
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
% Aporta la posibilidad de pintar un rehearsal mark al final de una línea y 
% al comienzo de la siguiente.
% http://lists.gnu.org/archive/html/lilypond-user/2011-08/msg00157.html
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
#(define (multi-mark-engraver ctx)
   (let ((texts '())
         (final-texts '())
         (events '()))

     `((start-translation-timestep
        . ,(lambda (trans)
             (set! final-texts '())))

       (listeners
        (mark-event
         . ,(lambda (trans ev)
              (set! events (cons ev events)))))

       (acknowledgers
        (break-alignment-interface
         . ,(lambda (trans grob source)
              (for-each (lambda (mark)
                          (set! (ly:grob-parent mark X) grob))
                        texts))))

       (process-music
        . ,(lambda (trans)
             (for-each
              (lambda (ev)
                (let* ((mark-grob
                        (ly:engraver-make-grob trans 'RehearsalMark ev))
                       (label (ly:event-property ev 'label))
                       (formatter (ly:context-property ctx 'markFormatter)))

                  (if (and (procedure? formatter)
                           (not (markup? label)))
                      (begin
                       (if (not (number? label))
                           (set! label
                                 (ly:context-property ctx 'rehearsalMark)))

                       (if (and (integer? label)
                                (exact? label))
                           (set! (ly:context-property ctx 'rehearsalMark)
                                 (1+ label)))

                       (if (number? label)
                           (set! label (apply formatter (list label ctx)))
                           (ly:warning "rehearsalMark must have
integer value"))))

                  (if (markup? label)
                      (begin
                       (set! (ly:grob-property mark-grob 'text) label)
                       (let ((dir (ly:event-property ev 'direction)))
                         (and (ly:dir? dir)
                              (set! (ly:grob-property mark-grob 'direction)
                                    dir))))
                      (ly:warning "mark label must be a markup object"))

                  (set! texts (cons mark-grob texts))))
              (reverse events))))

       (stop-translation-timestep
        . ,(lambda (trans)
             (if (pair? texts)
                 (let ((staves (ly:context-property ctx 'stavesFound))
                       (priority-index 0))
                   (for-each (lambda (grob)
                               (let ((my-priority (ly:grob-property
grob 'outside-staff-priority 1500)))
                                 (for-each (lambda (stave)

(ly:pointer-group-interface::add-grob grob 'side-support-elements
                                               stave))
                                           staves)
                                 (set! (ly:grob-property grob
'outside-staff-priority) (+ my-priority priority-index))
                                 (set! priority-index (1+ priority-index))
                                 (set! final-texts (cons grob final-texts))))
                             (reverse texts))
                     (set! texts '())
                     (set! events '())))))

        (finalize
         . ,(lambda (trans)
              (and (pair? final-texts)
                   (for-each (lambda (grob)
                               (set! (ly:grob-property grob 'break-visibility)
                                     end-of-line-visible))
                             final-texts)))))))


% Funciones originales
%{
myMark =
#(define-music-function (parser location text) (markup?)
  (make-music 'MarkEvent
              'label text))
			  
toCoda = {
	\tweak #'self-alignment-X #RIGHT
	\tweak #'break-visibility #begin-of-line-invisible
	\myMark \markup { \hspace #1.25 \raise #1.25 \musicglyph #"scripts.coda" }
}
%}

% Función maqueada.
% Se apoya en el código obtenido por consola al ejecutar { \displayMusic { \toCoda }}
eolMark =
#(define-music-function (parser location text) (markup?)
	(make-music 
		'MarkEvent 
		'tweaks (list 
			(cons (quote self-alignment-X) 1)
			(cons (quote break-visibility) #(#t #t #f))
		) 
		'label (markup
			#:line (
				#:hspace 1.25
				#:raise 1.25
				text
			)
		)
	)
)










% rest-score y merge-rests-on-positioning
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
% Permite realizar la fusión de los silencios entre dos voces en un mismo pentagrama.
% https://code.google.com/p/lilypond/issues/detail?id=1228 (fichero merge-rests.ily)
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]

#(define (rest-score r)
  (let ((score 0)
	(yoff (ly:grob-property-data r 'Y-offset))
	(sp (ly:grob-property-data r 'staff-position)))
    (if (number? yoff)
	(set! score (+ score 2))
	(if (eq? yoff 'calculation-in-progress)
	    (set! score (- score 3))))
    (and (number? sp)
	 (<= 0 2 sp)
	 (set! score (+ score 2))
	 (set! score (- score (abs (- 1 sp)))))
    score))

#(define (merge-rests-on-positioning grob)
  (let* ((can-merge #f)
	 (elts (ly:grob-object grob 'elements))
	 (num-elts (and (ly:grob-array? elts)
			(ly:grob-array-length elts)))
	 (two-voice? (= num-elts 2)))
    (if two-voice?
	(let* ((v1-grob (ly:grob-array-ref elts 0))
	       (v2-grob (ly:grob-array-ref elts 1))
	       (v1-rest (ly:grob-object v1-grob 'rest))
	       (v2-rest (ly:grob-object v2-grob 'rest)))
	  (and
	   (ly:grob? v1-rest)
	   (ly:grob? v2-rest)
	   (let* ((v1-duration-log (ly:grob-property v1-rest 'duration-log))
		  (v2-duration-log (ly:grob-property v2-rest 'duration-log))
		  (v1-dot (ly:grob-object v1-rest 'dot))
		  (v2-dot (ly:grob-object v2-rest 'dot))
		  (v1-dot-count (and (ly:grob? v1-dot)
				     (ly:grob-property v1-dot 'dot-count -1)))
		  (v2-dot-count (and (ly:grob? v2-dot)
				     (ly:grob-property v2-dot 'dot-count -1))))
	     (set! can-merge
		   (and
		    (number? v1-duration-log)
		    (number? v2-duration-log)
		    (= v1-duration-log v2-duration-log)
		    (eq? v1-dot-count v2-dot-count)))
	     (if can-merge
		 ;; keep the rest that looks best:
		 (let* ((keep-v1? (>= (rest-score v1-rest)
				      (rest-score v2-rest)))
			(rest-to-keep (if keep-v1? v1-rest v2-rest))
			(dot-to-kill (if keep-v1? v2-dot v1-dot)))
		   ;; uncomment if you're curious of which rest was chosen:
		   ;;(ly:grob-set-property! v1-rest 'color green)
		   ;;(ly:grob-set-property! v2-rest 'color blue)
		   (ly:grob-suicide! (if keep-v1? v2-rest v1-rest))
		   (if (ly:grob? dot-to-kill)
		       (ly:grob-suicide! dot-to-kill))
		   (ly:grob-set-property! rest-to-keep 'direction 0)
		   (ly:rest::y-offset-callback rest-to-keep)))))))
    (if can-merge
	#t
	(ly:rest-collision::calc-positioning-done grob))))










% startParenthesis y endParenthesis
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
% http://comments.gmane.org/gmane.comp.gnu.lilypond.general/64087
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]

% ELIMINADO










% bendBefore
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
% Adaptación
% http://lilypondblog.org/2013/08/adding-ornamentations-to-note-heads-part-1/
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]

%%%
%%% Utilities for defining new grobs, grob properties and music event types
%%% (there should be built-in commands to do that in LilyPond)
%%%
#(define (define-grob-type grob-name grob-entry)
   "Define a new grob and add it to `all-grob-definitions', after
scm/define-grobs.scm fashion.
After grob definitions are added, use:

\\layout {
  \\context {
    \\Global
    \\grobdescriptions #all-grob-descriptions
  }
}

to register them."
   (let* ((meta-entry   (assoc-get 'meta grob-entry))
          (class        (assoc-get 'class meta-entry))
          (ifaces-entry (assoc-get 'interfaces meta-entry)))
     (set-object-property! grob-name 'translation-type? list?)
     (set-object-property! grob-name 'is-grob? #t)
     (set! ifaces-entry (append (case class
                                  ((Item) '(item-interface))
                                  ((Spanner) '(spanner-interface))
                                  ((Paper_column) '((item-interface
                                                     paper-column-interface)))
                                  ((System) '((system-interface
                                               spanner-interface)))
                                  (else '(unknown-interface)))
                                ifaces-entry))
     (set! ifaces-entry (uniq-list (sort ifaces-entry symbol<?)))
     (set! ifaces-entry (cons 'grob-interface ifaces-entry))
     (set! meta-entry (assoc-set! meta-entry 'name grob-name))
     (set! meta-entry (assoc-set! meta-entry 'interfaces
                                  ifaces-entry))
     (set! grob-entry (assoc-set! grob-entry 'meta meta-entry))
     (set! all-grob-descriptions
           (cons (cons grob-name grob-entry)
                 all-grob-descriptions))))

#(define-public (define-grob-property symbol type? description)
   "Define a new grob property.
`symbol': the property name
`type?': the type predicate for this property
`description': the type documentation"
  (set-object-property! symbol 'backend-type? type?)
  (set-object-property! symbol 'backend-doc description)
  symbol)

#(defmacro*-public define-music-type (type-name parents #:rest properties)
     "Add a new music type description to `music-descriptions'
and `music-name-to-property-table'.

`type-name': the music type name (a symbol)
`parents': the parent event classes (a list of symbols)
`properties': a (key . value) property set, which shall contain at least
   description and type properties.
"
   (let ((gproperties (gensym "properties")))
     `(let ((,gproperties (list-copy ',properties)))
        ,@(map (lambda (parent)
                `(define-event-class ',(ly:camel-case->lisp-identifier type-name)
                   ',parent))
               parents)
        (set-object-property! ',type-name
                              'music-description
                              (cdr (assq 'description ,gproperties)))
        (set! ,gproperties (assoc-set! ,gproperties 'name ',type-name))
        (set! ,gproperties (assq-remove! ,gproperties 'description))
        (hashq-set! music-name-to-property-table ',type-name ,gproperties)
        (set! music-descriptions
              (cons (cons ',type-name ,gproperties)
                    music-descriptions)))))

%%%
%%% Grob definition
%%%
#(define (head-ornamentation::print me)
   "Prints a HeadOrnamentation grob, on the left or right side of the
note head, depending on the grob direction."
   (let* ((notes (ly:grob-object me 'elements))
          (y-ref (ly:grob-common-refpoint-of-array me notes Y))
          (x-ref (ly:grob-common-refpoint-of-array me notes X))
          (x-ext (ly:relative-group-extent notes x-ref X))
          (y-ext (ly:relative-group-extent notes y-ref Y))
          (staff-position (ly:grob-staff-position (ly:grob-array-ref notes 0)))
          (y-coord (+ (interval-center y-ext)
                      (if (and (eq? (ly:grob-property me 'shift-when-on-line) #t)
                               (memq staff-position '(-4 -2 0 2 4)))
                          0.5
                          0)))
          (text (ly:text-interface::print me))
          (width (/ (interval-length (ly:stencil-extent text X)) 2.0))
          (x-coord (if (= (ly:grob-property me 'direction) LEFT)
                       (- (car x-ext) width)
                       (+ (cdr x-ext) width))))
     (ly:stencil-translate
      text
      (cons
       (- (- x-coord (ly:grob-relative-coordinate me x-ref X)) 
          (ly:grob-property me 'x-position))
       (- (- y-coord (ly:grob-relative-coordinate me y-ref Y)) 
          (ly:grob-property me 'y-position))
       ))))

%% a new grob property (used to shift an ornamentation when the
%% note head is on a staff line)
#(define-grob-property 'shift-when-on-line boolean?
   "If true, then the ornamentation is vertically shifted when
the note head is on a staff line.")

#(define-grob-property 'x-position number?
   "X position")

#(define-grob-property 'y-position number?
   "Y position")

#(define-grob-type 'HeadOrnamentation
  `((font-size . 0)
    (shift-when-on-line . #f)
    (x-position . 1)
    (y-position . 1)
    (stencil . ,head-ornamentation::print)
    (meta . ((class . Item)
             (interfaces . (font-interface))))))

\layout {
  \context {
    \Global
    \grobdescriptions #all-grob-descriptions
  }
}

%%%
%%% Engraver
%%%
%% The head-ornamentation engraver, with its note-head acknowledger
%% (which add HeadOrnamentation grobs to note heads)
#(define head-ornamentation-engraver
   (make-engraver
    (acknowledgers
     ((note-head-interface engraver note-grob source)
      ;; helper function to create HeadOrnamentation grobs
      (define (make-ornament-grob text direction shift-when-on-line x-position y-position)
        (let ((ornament-grob (ly:engraver-make-grob engraver
                                                    'HeadOrnamentation
                                                    note-grob)))
          ;; use the ornament event text as the grob text property
          (set! (ly:grob-property ornament-grob 'text) text)
          ;; set the grob direction (either LEFT or RIGHT)
          (set! (ly:grob-property ornament-grob 'direction) direction)
          ;; set the shift-when-on-line property using the given value
          (set! (ly:grob-property ornament-grob 'shift-when-on-line)
                shift-when-on-line)
          ;; set the grob x-pos
          (set! (ly:grob-property ornament-grob 'x-position) x-position)
          ;; set the grob y-pos
          (set! (ly:grob-property ornament-grob 'y-position) y-position)
          (ly:pointer-group-interface::add-grob ornament-grob
                                                'elements
                                                note-grob)
          
          ;; the ornamentation is vertically aligned with the note head
          (set! (ly:grob-parent ornament-grob Y) note-grob)
          ;; compute its font size
          (set! (ly:grob-property ornament-grob 'font-size)
                (+ (ly:grob-property ornament-grob 'font-size 0.0)
                   (ly:grob-property note-grob 'font-size 0.0)))
          ;; move accidentals and dots to avoid collision
          (let* ((ornament-stencil (ly:text-interface::print ornament-grob))
                 (ornament-width (interval-length
                                  (ly:stencil-extent ornament-stencil X)))
                 (note-column (ly:grob-object note-grob 'axis-group-parent-X))
                 ;; accidentals attached to the note:
                 (accidentals (and (ly:grob? note-column)
                                   (ly:note-column-accidentals note-column)))
                 ;; dots attached to the note:
                 (dot-column (and (ly:grob? note-column)
                                  (ly:note-column-dot-column note-column))))
            (cond ((and (= direction LEFT) (ly:grob? accidentals))
                   ;; if the ornament is on the left side, and there are
                   ;; accidentals, then increase padding between note
                   ;; and accidentals to make room for the ornament
                   (set! (ly:grob-property accidentals 'padding)
                         ornament-width))
                  ((and (= direction RIGHT) (ly:grob? dot-column))
                   ;; if the ornament is on the right side, and there
                   ;; are dots, then translate the dots to make room for
                   ;; the ornament
                   (set! (ly:grob-property dot-column 'positioning-done)
                         (lambda (grob)
                           (ly:dot-column::calc-positioning-done grob)
                           (ly:grob-translate-axis! grob ornament-width X))))))
          ornament-grob))
      ;; When the note-head event attached to the note-head grob has
      ;; ornamentation events among its articulations, then create a
      ;; HeadOrnamentation grob
      (for-each
       (lambda (articulation)
         (if (memq 'head-ornamentation-event
                   (ly:event-property articulation 'class))
             ;; this articulation is an ornamentation => make the grob
             ;; (either on LEFT or RIGHT direction)
             (begin
               (if (markup? (ly:event-property articulation 'left-text))
                   (make-ornament-grob
                    (ly:event-property articulation 'left-text)
                    LEFT
                    (ly:event-property articulation 'shift-when-on-line)
                    (ly:event-property articulation 'x-position)
                    (ly:event-property articulation 'y-position)))
               (if (markup? (ly:event-property articulation 'right-text))
                   (make-ornament-grob
                   (ly:event-property articulation 'right-text)
                   RIGHT
                   (ly:event-property articulation 'shift-when-on-line)
                   (ly:event-property articulation 'x-position)
                   (ly:event-property articulation 'y-position))))))
         (ly:event-property (ly:grob-property note-grob 'cause) 'articulations))))))

\layout {
  \context {
    \Score
    \consists #head-ornamentation-engraver
  }
}

%%%
%%% Event type definition
%%%
#(define-music-type HeadOrnamentationEvent (music-event)
   (description . "Print an ornamentation at a note head side")
   (types . (general-music post-event event head-ornamentation-event)))

bendBefore =
#(make-music 'HeadOrnamentationEvent
  'x-position 1
  'y-position 2
  'shift-when-on-line #f
  'left-text #{ \markup { \fontsize #0 \rotate #5 \musicglyph #"brackettips.up"} #})










% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
% Funciones propias
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]





% Índice con borde circular y un cómodo espacio interior (y no el que viene por defecto)
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
#(define-markup-command (boxed layout props text) (markup?)
	(interpret-markup layout props
		(markup #:huge #:override '(box-padding . 0.9) #:override '(thickness . 4) #:box text)
	)
)

% Ritardando
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
rit =
#(make-music 'CrescendoEvent
             'span-direction START
             'span-type 'text
             'span-text "rit.")

% Barras de improvisacion
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
improOn = {
  \override Rest.stencil = #ly:text-interface::print
  \override Rest.text = \markup \jazzglyph #"noteheads.s2slashjazz"
  \override NoteHead.stencil = #jazz-slashednotehead
}

improOff = {
  \revert Rest.stencil
  \revert Rest.text
  \revert NoteHead.stencil
}

% Pentagrama vacío
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
emptyStaff = {
  \once \override Staff.KeySignature.break-visibility = #all-invisible
  \once \override Staff.Clef.break-visibility = #all-invisible
  \once \override Score.BarNumber.break-visibility = #all-invisible
  \break
  s1
  \once \override Score.BarLine.break-visibility = #all-invisible
  \break
}

emptyBar = {
  \stopStaff
  \once \override Staff.KeySignature.break-visibility = #all-invisible
  \once \override Staff.Clef.break-visibility = #all-invisible
  \once \override Score.BarNumber.break-visibility = #all-invisible
  s1
  
  \once \override Score.BarNumber.break-visibility = #all-visible
  \once \override Score.BarLine.break-visibility = #all-invisible
  \set Staff.explicitKeySignatureVisibility = #all-visible
  \startStaff
}










% Añadido de funciones al contexto
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
\layout {
  \context {
    \Score		
    
    % multi-mark-engraver
    \remove "Mark_engraver"
    \consists #multi-mark-engraver
    \consists "Tweak_engraver"
    
    % merge-rests-on-positioning
    \override RestCollision #'positioning-done = #merge-rests-on-positioning
    
    % aspecto de los silencios multicompás
    \override MultiMeasureRest.expand-limit = #1
    
    % margen extra de las articulaciones para evitar la colisión con las ligaduras
    \override Script.padding = #0.5
    \override Script.avoid-slur = #'inside
    
    % glissando bonito
    \override Glissando.style = #'trill
    \override Glissando.bound-details.left.padding = #1.7
    
    % dinámicas un poco mas gruesas
    \override Hairpin.thickness = #1.75
    \override Hairpin.bound-padding = #2
    
    % Esconde la cancelacion de accidentales en el cambio de armadura
    \override KeyCancellation.break-visibility = #all-invisible
  }
}












% LAYOUT
% ------
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~






\header {
  title = "I've got you under my skin"
  subtitle = "(As recorded by Frank SINATRA)"
  instrument = "Baritone Sax"
  composer = ""
  arranger = "Arr: Matt AMY"
  poet = \markup \tiny "Reducción y edición: Gabriel PALACIOS"
  meter = \markup \tiny "gabriel.ps.ms@gmail.com"
  tagline = \markup {
    \center-column {
      \line { \tiny "Reducción y edición: Gabriel PALACIOS" }
      \line { \tiny "gabriel.ps.ms@gmail.com" }
    }
  }
}

\paper {
  % El definir otro tipo de fuente no permite cambiar el tamaño global del staff. Es un bug conocigo
  #(define fonts (make-pango-font-tree "LilyJAZZText" "Nimbus Sans" "Luxi Mono" (/ 20 20)))
  
  % distancia entre la parte superior de la página y el título
  top-markup-spacing #'basic-distance = 5
  
  % distancia entre el título y el primer sistema
  markup-system-spacing #'basic-distance = 25
  
  % distancia entre el texto y el primer sistema 
  top-system-spacing #'basic-distance = 15
  
  % márgen inferior
  bottom-margin = 20
    
  % número de páginas
  % page-count = 1
  
  ragged-last-bottom = ##f 
  
  oddHeaderMarkup = \markup {
    \on-the-fly #not-first-page \fill-line {
      \fromproperty #'page:page-number-string
      \fromproperty #'header:title
      \fromproperty #'header:instrument
    }
  }
  
  evenHeaderMarkup = \markup {
    \on-the-fly #not-first-page \fill-line {
      \fromproperty #'header:instrument
      \fromproperty #'header:title
      \fromproperty #'page:page-number-string
    }
  }
}










% MUSIC PARTS
% -----------
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~





% scoreMarkup
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
scoreMarkup = \new Staff \with {
   \RemoveEmptyStaves
}
{
  % Oculta el primer pentagrama del markup
  \override Staff.VerticalAxisGroup #'remove-first = ##t
  
  % Indicación de tiempo
  \once \override Score.RehearsalMark.X-offset = #5
  \mark \jazzTempoMarkup "Easy Swing" c4 "120"
  s1*6
  
  \bar "||"
  \mark \markup \boxed "7"
  s1*8
  
  \bar "||"
  \mark \markup \boxed "15"
  s1*8
  
  \bar "||"
  \mark \markup \boxed "23"
  s1*8
  
  \bar "||"
  \mark \markup \boxed "31"
  s1*8
  
  \bar "||"
  \mark \markup \boxed "39"
  s1*8
  
  \bar "||"
  \mark \markup \boxed "47"
  s1*8
  
  \mark \markup \boxed "55"
  s1*6
  
  \bar "||"
  \once \override Score.RehearsalMark.X-offset = #5
  \mark \markup {
    \column { \boxed "61" }
    \hspace #1
    \column { "(Band Break)" }
  }
  s1*12
  
  \bar "||"
  \once \override Score.RehearsalMark.X-offset = #5
  \mark \markup {
    \column { \boxed "73" }
    \hspace #1
    \column { "(Bone Solo)" }
  }
  s1*8
  
  \bar "||"
  \once \override Score.RehearsalMark.X-offset = #5
  \mark \markup {
    \column { \boxed "81" }
    \hspace #1
    \column { "(Shout)" }
  }
  s1*8
  
  \bar "||"
  \once \override Score.RehearsalMark.X-offset = #5
  \mark \markup {
    \column { \boxed "89" }
    \hspace #1
    \column { "(Vox)" }
  }
  s1*8
  
  \bar "||"
  \mark \markup \boxed "97"
  s1*8
  
  \bar "||"
  \mark \markup \boxed "105"
  s1*12
  
  \bar "|."
}





% scoreChords
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
scoreChords = \chords {
    
  \set minorChordModifier = \markup { "-" }
  
  % c1:m f2:m g2:m7
}





% global
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
global = {  

  % Para pintar todos los números de compás debajo del pentagrama
  % \override Score.BarNumber #'break-visibility = #end-of-line-invisible
  % \override Score.BarNumber #'direction = #DOWN
  % \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0
  \set Score.explicitKeySignatureVisibility = #begin-of-line-visible
  
  \set Timing.beamExceptions = #'()
  \set Timing.baseMoment = #(ly:make-moment 1/4)
  \set Timing.beatStructure = #'(1 1 1 1)
  
  \set Score.tempoHideNote = ##t
  \tempo 4 = 120
}





% music
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
baritoneSax = \relative c'' {

  \global
  
  % 00: Tacet, Bones I, II, Tacet, Bari Sax
  \relative c' {
    
    \key bf \major
    bf8 \mf f' g4 -^ c8 f, g4 -^
    c8 f, g f -^ r2
    bf,8 f' g4 -^ c8 f, g4 -^
    \break
    c8 f, g f -^ r2
    bf,8 f' g4 -^ c8 f, g4 -^
    c8 f, g f -^ r2
    
    \break
    bf,8 f' g4 -^ c8 f, g4 -^
    c8 f, g f -^ r2
    bf,8 f' g4 -^ c8 f, g4 -^
    c8 f, g f -^ r2
    
    \break
    bf,8 f' g4 -^ c8 f, g4 -^
    c8 f, g f -^ r2
    bf,8 f' g4 -^ c8 f, g4 -^
    c8 f, g f -^ r2
  }
  
  % 15: Alto I, Trumpet I, Alto II, Tenor I, Bari
  \relative c'' {
    
    \key bf \major
    
    \break
    c1 \mp 
    b2 c
    bf?1 
    bf1
    
    \break
    c1
    c2 b
    bf,?8 f' g4 -^ c8 f, g4 -^
    c8 f, g f -^ r2
  }
  
  % 23: Alto I, Bones I, II(gf-f-f), Alto II, Bari
  \relative c'' {
    
    \key bf \major
    
    \break
    bf,8 \mf f' g4 -^ c8 f, g4 -^
    c8 f, g f -^ r2
    bf,8 f' g4 -^ c8 f, g4 -^
    c8 f, g f -^ r2
    
    \break
    bf,8 f'? fs4 -^ c'8 f, g4 -^
    c8 f,? fs f -^ r2
    bf,8 e g4 -^ c8 f, g4 -^
    c8 f, g f -^ r2
  }
  
  % 31: Saxs
  \relative c'' {
    
    \key bf \major
    
    \break
    R1*2
    r2 r8 d -> \mf d4 -^
    g4 -^ g -^ r2
    
    bf,8 \mf f g4 -^ c8 f, g4 -^
    \break
    c8 f, g c -^ r2
    bf8 f g4 -^ c8 f, g4 -^
    c8 f, g c -^ r2
    
    \break
    c4 -^ \mp c -^ c -- r
    r r8 c -> ~ c c c c 
    bf4 -^  bf -^ bf -- r
    r r8 bf -> ~ bf bf bf bf
    
    \break
    c4 -^ c -^ c -- r
    r r8 c -> ~ c c c c
    d,4 -^ d -^ df4. c8 ->
    r c r c f4 -^ f -^
  }
  
  % 47: Alto I, Bones I, II, III, Bari
  \relative c'' {
    
    \key bf \major
    
    \break
    d1 ~
    d2 cs
    c? bf
    c4 -^ c8 bf ~ bf2
    
    \break
    r8 bf4. -> a2
    bf4. -> a8 ~ a2
    r8 a -> r a g4 -^ r
    f4 -^ f8 bf, -> ~ bf2
  }
  
  % 55: Trumpet I, Trumpet II, Bones I, II, IV
  \transpose ef c \relative c'' {
    
    \key df \major
    
    \break
    r8 ef -^ r4 ef4 -^ r
    r8 gf -^ r4 gf4 -^ r
    r8 af4. \f \bendAfter #-4 r2
    r8 gf -> \> r gf f4 -^ f -^ \!
    r2 ef,4 -^ \mp r8 af8 -> ~
    af2 r2
    
    \break
    \repeat percent 2 {
      df,4 -^ \mf af' -^ r df, -^
      af' -^ r8 df, r ef r af
    }
    
    \break
    \repeat percent 4 {
      df,4 -^ \mf af' -^ r df, -^
      af' -^ r8 df, r ef r af
    }
  }
  
  % 73: Alto I, Alto II, Bone I, Tenor I, Bari
  \relative c' {
    
    \key bf \major
    
    \break
    r2 f2 ~ \mf
    f1
    bf,1 ~ 
    bf1
    
    bf1 ~
    bf1
    bf1 ~
    bf2. bf4 -^ \ff
  }
  
  % 81: Alto I, Bones I, II, III, Bari
  \relative c'' {
    
    \key bf \major
    
    \break
    r8 a d -> ef d a d -> d
    d a d -> d a d d -> d
    e d e e -> ~ e d e e
    e d e e e4 -^ r
    
    \break
    r8 c g' -> g g c, g' -> g
    g c, g' g g c, f4 -^
    bf,,8 f' g4 -^ c8 f, g4 -^
    c8 \> f, g f -> ~ f4 \! r
  }
  
  % 89: Saxs
  \relative c'' {
    
    \key bf \major
    
    \break
    c4 -^ \mp c -^ c -- r
    r r8 c -> ~ c c c c 
    bf4 -^  bf -^ bf -- r
    r r8 bf -> ~ bf bf bf bf
    
    \break
    c4 -^ c -^ c -- r
    r r8 c -> ~ c c c c
    d,4 -^ d -^ df4. c8 ->
    r c r c f4 -^ f -^
  }
  
  % 97: Alto I, Bones I, II, III, Bari
  \relative c'' {
    
    \key bf \major
    
    \break
    d1 ~
    d2 cs
    c? bf
    c4 -^ c8 bf ~ bf2
    
    \break
    r8 bf4. -> a2
    bf4. -> a8 ~ a2
    r8 a -> r a g4 -^ r
    f4 -^ f8 bf, -> ~ bf2
  }
  
  % 105: Tacet, Bones
  \transpose ef c \relative c'' {
    
    \key df \major
    
    \break
    r8 ef -^ \mf r df8 -> ef4. r8
    r8 gf -^ \mf r ff8 -> gf4. r8
    r8 af -> \fp \< ~ af2 f8 f -^ \!
    R1
  }
  
  % 109: Saxs
  \relative c'' {
    
    \key bf \major
    
    \break
    bf8 \mf f g4 -^ c8 f, g4 -^
    c8 f, fs f -^ r2
    bf8 f g4 -^ c8 f, g4 -^
    c8 f, g f -^ r2
    
    \break
    R1*3
    r4 bf, -^ \sf r2
  }
}





% score
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
\score {
  <<
    \scoreMarkup
    \scoreChords
    \new StaffGroup <<
      \new Staff { \baritoneSax }
    >>
  >>
}

tenorSaxII = \relative c'' {

  \global
  
  % 00: Tacet, Bones I, II, Tacet, Bari Sax
  \relative c'' {
    
    \key ef \major
    
    R1*6
    R1*8
  }
  
  % 15: Saxs
  \relative c'' {
    
    \key ef \major
    
    \break
    af1 \mp 
    af
    g1 
    bf1
    
    \break
    c1
    af2 af
    g1 ~ 
    g2. \> r4 \!
  }
  
  % 23: Alto I, Bones I, II(gf-f-f), Alto II, Bari
  \transpose bf ef' \relative c'' {
    
    \key bf \major
    
    \break
    bf1 ~ \p 
    bf2. r4
    a1 ~
    a2 ~ a8 cs c -^ b
    
    \break
    bf?1 ~ 
    bf2. r4
    a2 bf ~ 
    bf2. r4
  }
  
  % 31: Saxs
  \relative c'' {
    
    \key ef \major
    
    \break
    R1*2
    r2 r8 a -> \mf a4 -^
    e'4 -^ e -^ r2
    
    R1*2
    r2 g,4 -^ \mf r
    g -^ r8 g -> r2
    
    \break
    af4 -^ \mp af -^ af -- r
    r r8 af -> ~ af af af af 
    bf4 -^  bf -^ bf -- r
    r r8 bf -> ~ bf bf bf bf
    
    \break
    c4 -^ c -^ c -- r
    r r8 b -> ~ b b b b
    d,4 -^ d -^ df4. c8 ->
    r c r c f4 -^ f -^
  }
  
  % 47: Alto I, Bones I, II, III, Bari
  \transpose bf c' \relative c'' {
    
    \key df \major
    
    \break
    r8 bf -> \mf r4 r8 bf -> r4
    r8 bf -> r4 r8 bf -> r4
    r8 af -> r4 r8 af -> r4
    r8 af -> r4 r8 g -> r4
    
    \break
    r8 gf -> r4 r8 gf -> r4
    r8 gf -> r4 r8 gf -> r4
    r8 f -> r4 r8 f -> r4
    bf4 -^ bf8 a -> ~ a2
  }
  
  % 55: Trumpet I, Trumpet II, Bones I, II, IV
  \transpose bf c' \relative c'' {
    
    \key df \major
    
    \break
    r8 bf -^ r4 bf4 -^ r
    r8 df -^ r4 df4 -^ r
    r8 df4. \f \bendAfter #-4 r2
    r8 cf -> \> r cf bf4 -^ bf -^
    r2 gf4 -^ \mp r8 gf8 -> ~
    gf2 r2
    
    \break
    R1*4
    r8 ^\markup "(Soli)" af \mf af bf af4 -^ r
    af4 -^ bf ~ bf8 bf ef -^ af,
    df4 -^ r8 bf af4 -^ r
    af4 -^ bf ~ bf8 bf ef -^ af,
    
    \break
    df4 -^ af8 af bf bf af4 -^
    r8 af bf af bf bf ef ef
    df af af af bf bf af4 -^
    r8 af bf bf af4 -^ r
  }
  
  % 73: Alto I, Alto II, Bone I, Tenor I, Bari
  \relative c'' {
    
    \key ef \major
    
    \break
    c1 ~ \mf
    c1
    c1 ~ 
    c1
    
    \break
    b1 ~
    b2 c
    b4 c2. ~
    c2. c4 -^ \ff
  }
  
  % 81: Alto I, Bones I, II, III, Bari
  \transpose bf c' \relative c'' {
    
    \key df \major
    
    \break
    c4 -^ c8 c c c c c -> ~
    c4 c -^ c8 bf a4 -^
    g4 -^ g8 g -> ~ g2 ~
    g2 r4 g4 -^
    
    \break
    bf4 -^ bf8 bf bf bf bf bf -> ~
    bf4 gf? -^ gf8 f ef4 -^
    ef1 ~
    ef2. \> r4 \!
  }
  
  % 89: Saxs
  \relative c'' {
    
    \key ef \major
    
    \break
    af4 -^ \mp af -^ af -- r
    r r8 af -> ~ af af af af 
    bf4 -^  bf -^ bf -- r
    r r8 bf -> ~ bf bf bf bf
    
    \break
    c4 -^ c -^ c -- r
    r r8 b -> ~ b b b b
    d,4 -^ d -^ df4. c8 ->
    r c r c f4 -^ f -^
  }
  
  % 97: Alto I, Bones I, II, III, Bari
  \transpose bf c' \relative c'' {
    
    \key df \major
    
    \break
    r8 bf -> \mf r4 r8 bf -> r4
    r8 bf -> r4 r8 bf -> r4
    r8 af -> r af -> af4 -^ af -^ 
    r8 af -> r4 r8 g -> r4
    
    \break
    r8 gf? -> r gf -> gf4 -^ gf -^ 
    r8 gf -> r4 r8 gf -> r4
    r8 f -> r f -> f4 -^ f -^ 
    bf4 -^ bf8 a -> ~ a2
  }
  
  % 105: Tacet, Bones
  \transpose bf c' \relative c'' {
    
    \key df \major
    
    \break
    r8 gf -^ \mf r ef8 -> gf4. r8
    r8 a -^ \mf r gf8 -> a4. r8
    r8 bf -> \fp \< ~ bf2 a8 af -^ \!
    R1
  }
  
  % 109: Saxs
  \relative c' {
    
    \key ef \major
    
    \break
    R1*7
    r4 ef -^ \sf r2
  }
}

tenorSaxI = \relative c'' {

  \global
  
  % 00: Tacet, Bones I, II, Tacet, Bari Sax
  \transpose bf c' \relative c'' {
    
    \key df \major
    
    c1 \mf ~
    c2 r8 e -> ef -^ d
    df?1 ~
    df2 r8 ef? -> d -^ df
    c?1 ~
    c2. r4
    
    \break
    df1 ~ \mp
    df
    c1 ~
    c2 r8 e -> ef -^ d
    df1 ~
    df
    c1 ~
    c2. r4
  }
  
  % 15: Alto I, Trumpet I, Alto II, Tenor I, Bari
  \relative c'' {
    
    \key ef \major
    
    \break
    c1 \mp 
    c2 d
    d1 
    ef1
    
    \break
    ef1
    c2 b
    bf?1 ~ 
    bf2. \> r4 \!
  }
  
  % 23: Alto I, Bones I, II(gf-f-f), Alto II, Bari
  \transpose bf c' \relative c''' {
    
    \key df \major
    
    \break
    r2 gf4 -^ \f r
    gf4 -^ r4 r2
    r2 f4 -^ \f r
    f4 -^ r8 f8 -> \bendAfter #-3 r2
    
    \break
    R1*2
    r2 f4 -^ r
    f4 -^ r8 f -> \bendAfter #-3 r2
  }
  
  % 31: Saxs
  \relative c'' {
    
    \key ef \major
    
    \break
    R1*2
    r2 r8 c -> \mf c4 -^
    g'4 -^ g -^ r2
    
    R1*2
    r2 bf,4 -^ \mf r
    bf -^ r8 bf -> r2
    
    \break
    c4 -^ \mp c -^ c -- r
    r r8 c -> ~ c c c c 
    c4 -^  c -^ c -- r
    r r8 df -> ~ df df df df
    
    \break
    ef4 -^ ef -^ ef -- r
    r r8 ef -> ~ ef ef ef ef
    c4 -^ c -^ bf4. bf8 ->
    r bf r bf af4 -^ af -^
  }
  
  % 47: Alto I, Bones I, II, III, Bari
  \transpose bf c' \relative c'' {
    
    \key df \major
    
    \break
    r8 df -> \mf r4 r8 df -> r4
    r8 df -> r4 r8 c -> r4
    r8 c -> r4 r8 bf -> r4
    r8 c -> r4 r8 bf -> r4
    
    \break
    r8 bf -> r4 r8 af -> r4
    r8 bf -> r4 r8 a -> r4
    r8 af? -> r4 r8 af -> r4
    cf4 -^ cf8 cf -> ~ cf2
  }
  
  % 55: Trumpet I, Trumpet II, Bones I, II, IV
  \transpose bf c' \relative c'' {
    
    \key df \major
    
    \break
    r8 df -^ r4 df4 -^ r
    r8 ef -^ r4 ef4 -^ r
    r8 f4. \f \bendAfter #-4 r2
    \break
    r8 ef -> \> r ef d4 -^ d -^ \!
    r2 df4 -^ \mp r8 df8 -> ~
    df2 r2
    
    \break
    R1*4
    r8 ^\markup "(Soli)" af \mf af bf af4 -^ r
    af4 -^ bf ~ bf8 bf ef -^ af,
    df4 -^ r8 bf af4 -^ r
    af4 -^ bf ~ bf8 bf ef -^ af,
    
    \break
    df4 -^ af8 af bf bf af4 -^
    r8 af bf af bf bf ef ef
    df af af af bf bf af4 -^
    r8 af bf bf af4 -^ r
  }
  
  % 73: Alto I, Alto II, Bone I, Tenor I, Bari
  \transpose bf c' \relative c'' {
    
    \key df \major
    
    \break
    \improOn \repeat unfold 16 { r4 } \improOff
    
    \break
    \improOn \repeat unfold 15 { r4 } \improOff df4 -^ \ff
  }
  
  % 81: Alto I, Bones I, II, III, Bari
  \transpose bf c' \relative c'' {
    
    \key df \major
    
    \break
    ef4 -^ ef8 ef ef ef ef ef -> ~
    ef4 d -^ d8 c c4 -^
    bf4 -^ bf8 bf -> ~ bf2 ~
    bf2 r4 bf4 -^
    
    \break
    df4 -^ df8 df df df df df -> ~
    df4 bf -^ bf8 af gf4 -^
    f1 ~
    f2. \> r4 \!
  }
  
  % 89: Saxs
  \relative c'' {
    
    \key ef \major
    
    \break
    c4 -^ \mp c -^ c -- r
    r r8 c -> ~ c c c c 
    c4 -^  c -^ c -- r
    r r8 df -> ~ df df df df
    
    \break
    ef4 -^ ef -^ ef -- r
    r r8 ef -> ~ ef ef ef ef
    c4 -^ c -^ bf4. bf8 ->
    r bf r bf af4 -^ af -^
  }
  
  % 97: Alto I, Bones I, II, III, Bari
  \transpose bf c' \relative c'' {
    
    \key df \major
    
    \break
    r8 df -> \mf r4 r8 df -> r4
    r8 df -> r4 r8 c -> r4
    r8 c -> r c -> bf4 -^ bf -^ 
    r8 c -> r4 r8 bf -> r4
    
    \break
    r8 bf -> r bf -> af4 -^ af -^ 
    r8 bf -> r4 r8 a -> r4
    r8 af? -> r af -> af4 -^ af -^ 
    cf4 -^ cf8 cf -> ~ cf2
  }
  
  % 105: Tacet, Bones
  \transpose bf c' \relative c'' {
    
    \key df \major
    
    \break
    r8 bf -^ \mf r gf8 -> bf4. r8
    r8 df -^ \mf r a8 -> df4. r8
    r8 df -> \fp \< ~ df2 cf8 bf -^ \!
    R1
  }
  
  % 109: Saxs
  \relative c' {
    
    \key ef \major
    
    \break
    R1*7
    r4 ef -^ \sf r2
  }
}

altoSaxI = \relative c'' {

  \global
  
  % 00: Tacet, Bones I, II, Tacet, Bari Sax
  \transpose ef c \relative c'' {
    
    \key df \major
    
    ef1 \mf ~
    ef2 r8 gf -> f -^ e
    ef?1 ~
    ef2 r8 gf -> f -^ e
    ef?1 ~
    ef2. r4
    
    \break
    ef1 ~ \mp
    ef
    ef1 ~
    ef2 r8 gf -> f -^ e
    ef1 ~
    ef
    ef1 ~
    ef2. r4
  }
  
  % 15: Saxs
  \relative c'' {
    
    \key bf \major
    
    \break
    g1 \mp 
    a2 g
    c d
    d1
    
    \break
    ef1
    bf2 a
    a1 ~ 
    a2. \> r4 \!
  }
  
  % 23: Alto I, Bones I, II(gf-f-f), Alto II, Bari
  \transpose ef c \relative c''' {
    
    \key df \major
    
    \break
    r2 af4 -^ \f r
    af4 -^ r4 r2
    r2 af4 -^ \f r
    af4 -^ r8 af8 -> \bendAfter #-3 r2
    
    \break
    R1*2
    r2 af4 -^ r
    af4 -^ r8 af -> \bendAfter #-3 r2
  }
  
  % 31: Saxs
  \relative c'' {
    
    \key bf \major
    
    \break
    R1*2
    r2 r8 b -> \mf b4 -^
    fs'4 -^ fs -^ r2
    
    R1*2
    r2 g,4 -^ \mf r
    g -^ r8 g -> r2
    
    \break
    bf4 -^ \mp bf -^ bf -- r
    r r8 bf -> ~ bf bf bf bf 
    bf4 -^  bf -^ bf -- r
    r r8 b -> ~ b b b b
    
    \break
    c4 -^ c -^ c -- r
    r r8 c -> ~ c c c c
    c4 -^ c -^ b4. bf8 ->
    r bf r bf a4 -^ a -^
  }
  
  % 47: Alto I, Bones I, II, III, Bari
  \transpose ef c \relative c'' {
    
    \key df \major
    
    \break
    r8 f -> \mf r4 r8 f -> r4
    r8 f -> r4 r8 e -> r4
    r8 ef -> r4 r8 df -> r4
    r8 ef -> r4 r8 df -> r4
    
    \break
    r8 df -> r4 r8 c -> r4
    r8 df -> r4 r8 c -> r4
    r8 c -> r4 r8 bf -> r4
    ef4 -^ ef8 d -> ~ d2
  }
  
  % 55: Trumpet I, Trumpet II, Bones I, II, IV
  \transpose ef bf \relative c'' {
    
    \key ef \major
    
    \break
    r8 c -^ r4 c4 -^ r
    r8 ef -^ r8 b -> ef4 -^ r
    r8 ef4. \f \bendAfter #-4 r2
    R1*3
    
    \break
    R1*11
    r2 \times 2/3 { d8 \f ef e } \times 2/3 { f? fs g }
    
    \break
    af8 af -> r4
  }
  
  % 73: Alto I, Alto II, Bone I, Tenor I, Bari
  \relative c'' {
    
    \key bf \major
    
    \break
    c2 ~ \mf
    c1
    bf1 ~ 
    bf1
    
    \break
    bf1 ~
    bf2 a
    b4 c2. ~
    c2. c4 -^ \ff
  }
  
  % 81: Alto I, Bones I, II, III, Bari
  \transpose ef c \relative c''' {
    
    \key df \major
    
    \break
    g4 -^ g8 g g g g g? -> ~
    g4 f -^ f8 ef d4 -^
    df?4 -^ d8 d -> ~ d2 ~
    d2 r4 d4 -^
    
    \break
    ef4 -^ ef8 ef ef ef ef ef -> ~
    ef4 df -^ df8 c bf4 -^
    af1 ~
    af2. \> r4 \!
  }
  
  % 89: Saxs
  \relative c'' {
    
    \key bf \major
   
    \break
    bf4 -^ \mp bf -^ bf -- r
    r r8 bf -> ~ bf bf bf bf 
    bf4 -^  bf -^ bf -- r
    r r8 b -> ~ b b b b
    
    \break
    c4 -^ c -^ c -- r
    r r8 c -> ~ c c c c
    c4 -^ c -^ b4. bf8 ->
    r bf r bf a4 -^ a -^
  }
  
  % 97: Alto I, Bones I, II, III, Bari
  \transpose ef c \relative c'' {
    
    \key df \major
    
    \break
    r8 f -> \mf r4 r8 f -> r4
    r8 f -> r4 r8 e -> r4
    r8 ef? -> r ef -> df4 -^ df -^ 
    r8 ef -> r4 r8 df -> r4
    
    \break
    r8 df -> r df -> c4 -^ c -^ 
    r8 df -> r4 r8 c -> r4
    r8 c -> r c -> bf4 -^ bf -^ 
    ef4 -^ ef8 d -> ~ d2
  }
  
  % 105: Tacet, Bones
  \transpose ef c \relative c'' {
    
    \key df \major
    
    \break
    r8 df? -^ r4 df4. -> r8
    r8 ef -^ r4 ef4. -> r8
    r8 f -> \fp \< ~ f2 ef8 d -^ \!
    R1
  }
  
  % 109: Saxs
  \relative c'' {
    
    \key bf \major
    
    \break
    R1*7
    r4 bf? -^ \sf r2
  }
}

violin = \relative c'' {

  \global
  
  % 00: Tacet, Bones I, II, Tacet, Bari Sax
  \transpose c ef, \relative c'' {
    
    \key bf \major
    
    R1*6 
    R1*8
  }
  
  % 15: Saxs
  \transpose c ef, \relative c'' {
    
    \key bf \major
    
    \break
    bf1 \mp 
    b2 c
    e f
    g a
    
    \break
    bf2. g4
    bf,2 b
    c1 ~ 
    c2. \> r4 \!
  }
  
  % 23: Alto I, Bones I, II(gf-f-f), Alto II, Bari
  \transpose c ef, \relative c'' {
    
    \key bf \major
    
    \break
    c1 ~ \p 
    c2. r4
    c1 ~
    c2 ~ c8 ef d -^ cs
    
    \break
    c?1 ~ 
    c2. r4
    cs2 d ~ 
    d2. r4
  }
  
  % 31: Saxs
  \transpose c ef, \relative c'' {
    
    \key bf \major
    
    \break
    R1*2
    r2 r8 d -> \mf d4 -^
    g4 -^ g -^ r2
    
    R1*2
    r2 c,4 -^ \mf r
    c -^ r8 c -> r2
    
    \break
    c4 -^ \mp c -^ c -- r
    r r8 c -> ~ c c c c 
    d4 -^  d -^ d -- r
    r r8 d -> ~ d d d d
    
    \break
    ef4 -^ ef -^ ef -- r
    r r8 ef -> ~ ef ef ef ef 
    f4 -^ f -^ f4. f8 ->
    r f r f f4 -^ f -^
  }
  
  % 47: Alto I, Bones I, II, III, Bari
  \transpose c ef, \relative c'' {
    
    \key bf \major
    
    \break
    d1 ~
    d2 cs
    c? bf
    c4 -^ c8 bf ~ bf2
    
    \break
    r8 bf4. -> a2
    bf4. -> a8 ~ a2
    r8 a -> r a g4 -^ r
    c4 -^ c8 b -> ~ b2
  }
  
  % 55: Trumpet I, Trumpet II, Bones I, II, IV
  \transpose c bf, \relative c'' {
    
    \key ef \major
    
    \break
    r8 ef -^ r4 ef4 -^ r
    r8 f -^ r8 ef -> f4 -^ r
    r8 g4. \f \bendAfter #-4 r2
    R1*3
    
    \break
    R1*11
    r2 \times 2/3 { fs8 \f g af } \times 2/3 { a bf b }
    
    \break
    c8 c -> r4
  }
  
  % 73: Alto I, Alto II, Bone I, Tenor I, Bari
  \transpose c ef, \relative c'' {
    
    \key bf \major
    
    \break
    ef2 ~ \mf
    ef2. c4
    d1 ~ 
    d1
    
    \break
    ef1 ~
    ef2 f
    e4 f2. ~
    f2. f4 -^ \ff
  }
  
  % 81: Alto I, Bones I, II, III, Bari
  \transpose c ef, \relative c'' {
    
    \key bf \major
    
    \break
    r8 a d -> ef d a d -> d
    d a d -> d a d d -> d
    e d e e -> ~ e d e e
    e d e e e4 -^ r
    
    \break
    r8 c g' -> g g c, g' -> g
    g c, g' g g c, f4 -^
    r8 f, g4 -^ c8 f, g4 -^
    c8 \> f, g f -> ~ f4 \! r
  }
  
  % 89: Saxs
  \transpose c ef, \relative c'' {
    
    \key bf \major
   
    \break
    c4 -^ \mp c -^ c -- r
    r r8 c -> ~ c c c c 
    d4 -^ d -^ d -- r
    r r8 d -> ~ d d d d
    
    \break
    ef4 -^ ef -^ ef -- r
    r r8 ef -> ~ ef ef ef ef 
    f4 -^ f -^ f4. f8 ->
    r f r f f4 -^ f -^
  }
  % 97: Alto I, Bones I, II, III, Bari
  \transpose c ef, \relative c'' {
    
    \key bf \major
    
    \break
    d1 ~
    d2 cs
    c? bf
    c4 -^ c8 bf ~ bf2
    
    \break
    r8 bf4. -> a2
    bf4. -> a8 ~ a2
    r8 a -> r a g4 -^ r
    c4 -^ c8 b -> ~ b2
  }
  
  % 105: Tacet, Bones
  \transpose c ef, \relative c'' {
    
    \key bf \major
    
    \break
    R1*4
    
  }
  
  % 109: Saxs
  \transpose c ef, \relative c'' {
    
    \key bf \major
    
    R1*7
    r4 bf -^ \sf r2
  }
}


\score {
  \unfoldRepeats
  <<
    \new StaffGroup <<
      \new Staff \with {midiInstrument = #"violin"} { \violin }
      \new Staff \with {midiInstrument = #"alto sax"} { \transpose c ef, \altoSaxI }
      \new Staff \with {midiInstrument = #"tenor sax"} { \transpose c bf,, \tenorSaxI }
      \new Staff \with {midiInstrument = #"tenor sax"} { \transpose c bf,, \tenorSaxII }
      \new Staff \with {midiInstrument = #"baritone sax"} { \transpose c ef,, \baritoneSax }
    >>
  >>
  \midi {}
}

% 15: quitar trompeta
% 23: alteracion alto II
% 32: ??
% 46: ??