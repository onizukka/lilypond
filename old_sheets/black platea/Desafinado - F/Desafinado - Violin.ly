\version "2.18.2"

\include "lily-jazz/ignatzek-jazz-chords.ily"
\include "lily-jazz/lily-jazz.ily"

% PLUGINS
% - \eolMark: 		mark al final de línea
% - \bendBefore: 	solución al bendBefore (i.e. dis8 \bendBefore)
% - \rit: 			crea una porción de ritardando que debe terminar como las dinámicas, con un \!
% - \rs: 			negra de improvisación
% - \emptyStave:	Imprime un pentagrama vacío

% LAYOUT
% - El aspecto de los silencios multicompás es como los de las partituras de jazz
% - Margen extra de las articulaciones para evitar la colisión con las ligaduras
% - Glissandos bonitos

% PAPER
% - Se imprime el título, instrumento y número de página en todas las páginas
% - Solo se debe tocar el page-count (si eso). Sale bonito tal cual.

% CAMBIOS
% 03-09-14: Layout
% Ahora las articulaciones van por dentro de las ligaduras. Parece que es la mejor solución
%
% 04-09-14: Layout
% Los creciendos y decreciendos tienen la línea un poco más gruesa
%
% Markup
% Añadidas guías para correcta posición y markup de la indicación de tempo
%
% Plugins
% Añadida función \emptyStave para imprimir un pentagrama vacío.
% En algunas partituras hay un pentagrama vacío antes del coda (lo cual mola bastante, por cierto)
%
% 05-09-14: Plugins
% Añadido a la función \emptyStave el esconder la armadura de la clave, que no estaba.
%
% 08-09-14: Layout
% Ahora no se muestra la cancelación de accidentales al haber un cambio de armadura
%
% 09-09-14: Chords
% Añadida sentencia para que los acordes menores salgan con un guión en vez de con una M minúscula


% PLUGINS 1.2
% -----------
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~





% multi-mark-engraver
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
% Aporta la posibilidad de pintar un rehearsal mark al final de una línea y 
% al comienzo de la siguiente.
% http://lists.gnu.org/archive/html/lilypond-user/2011-08/msg00157.html
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
#(define (multi-mark-engraver ctx)
   (let ((texts '())
         (final-texts '())
         (events '()))

     `((start-translation-timestep
        . ,(lambda (trans)
             (set! final-texts '())))

       (listeners
        (mark-event
         . ,(lambda (trans ev)
              (set! events (cons ev events)))))

       (acknowledgers
        (break-alignment-interface
         . ,(lambda (trans grob source)
              (for-each (lambda (mark)
                          (set! (ly:grob-parent mark X) grob))
                        texts))))

       (process-music
        . ,(lambda (trans)
             (for-each
              (lambda (ev)
                (let* ((mark-grob
                        (ly:engraver-make-grob trans 'RehearsalMark ev))
                       (label (ly:event-property ev 'label))
                       (formatter (ly:context-property ctx 'markFormatter)))

                  (if (and (procedure? formatter)
                           (not (markup? label)))
                      (begin
                       (if (not (number? label))
                           (set! label
                                 (ly:context-property ctx 'rehearsalMark)))

                       (if (and (integer? label)
                                (exact? label))
                           (set! (ly:context-property ctx 'rehearsalMark)
                                 (1+ label)))

                       (if (number? label)
                           (set! label (apply formatter (list label ctx)))
                           (ly:warning "rehearsalMark must have
integer value"))))

                  (if (markup? label)
                      (begin
                       (set! (ly:grob-property mark-grob 'text) label)
                       (let ((dir (ly:event-property ev 'direction)))
                         (and (ly:dir? dir)
                              (set! (ly:grob-property mark-grob 'direction)
                                    dir))))
                      (ly:warning "mark label must be a markup object"))

                  (set! texts (cons mark-grob texts))))
              (reverse events))))

       (stop-translation-timestep
        . ,(lambda (trans)
             (if (pair? texts)
                 (let ((staves (ly:context-property ctx 'stavesFound))
                       (priority-index 0))
                   (for-each (lambda (grob)
                               (let ((my-priority (ly:grob-property
grob 'outside-staff-priority 1500)))
                                 (for-each (lambda (stave)

(ly:pointer-group-interface::add-grob grob 'side-support-elements
                                               stave))
                                           staves)
                                 (set! (ly:grob-property grob
'outside-staff-priority) (+ my-priority priority-index))
                                 (set! priority-index (1+ priority-index))
                                 (set! final-texts (cons grob final-texts))))
                             (reverse texts))
                     (set! texts '())
                     (set! events '())))))

        (finalize
         . ,(lambda (trans)
              (and (pair? final-texts)
                   (for-each (lambda (grob)
                               (set! (ly:grob-property grob 'break-visibility)
                                     end-of-line-visible))
                             final-texts)))))))


% Funciones originales
%{
myMark =
#(define-music-function (parser location text) (markup?)
  (make-music 'MarkEvent
              'label text))
			  
toCoda = {
	\tweak #'self-alignment-X #RIGHT
	\tweak #'break-visibility #begin-of-line-invisible
	\myMark \markup { \hspace #1.25 \raise #1.25 \musicglyph #"scripts.coda" }
}
%}

% Función maqueada.
% Se apoya en el código obtenido por consola al ejecutar { \displayMusic { \toCoda }}
eolMark =
#(define-music-function (parser location text) (markup?)
	(make-music 
		'MarkEvent 
		'tweaks (list 
			(cons (quote self-alignment-X) 1)
			(cons (quote break-visibility) #(#t #t #f))
		) 
		'label (markup
			#:line (
				#:hspace 1.25
				#:raise 1.25
				text
			)
		)
	)
)










% rest-score y merge-rests-on-positioning
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
% Permite realizar la fusión de los silencios entre dos voces en un mismo pentagrama.
% https://code.google.com/p/lilypond/issues/detail?id=1228 (fichero merge-rests.ily)
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]

#(define (rest-score r)
  (let ((score 0)
	(yoff (ly:grob-property-data r 'Y-offset))
	(sp (ly:grob-property-data r 'staff-position)))
    (if (number? yoff)
	(set! score (+ score 2))
	(if (eq? yoff 'calculation-in-progress)
	    (set! score (- score 3))))
    (and (number? sp)
	 (<= 0 2 sp)
	 (set! score (+ score 2))
	 (set! score (- score (abs (- 1 sp)))))
    score))

#(define (merge-rests-on-positioning grob)
  (let* ((can-merge #f)
	 (elts (ly:grob-object grob 'elements))
	 (num-elts (and (ly:grob-array? elts)
			(ly:grob-array-length elts)))
	 (two-voice? (= num-elts 2)))
    (if two-voice?
	(let* ((v1-grob (ly:grob-array-ref elts 0))
	       (v2-grob (ly:grob-array-ref elts 1))
	       (v1-rest (ly:grob-object v1-grob 'rest))
	       (v2-rest (ly:grob-object v2-grob 'rest)))
	  (and
	   (ly:grob? v1-rest)
	   (ly:grob? v2-rest)
	   (let* ((v1-duration-log (ly:grob-property v1-rest 'duration-log))
		  (v2-duration-log (ly:grob-property v2-rest 'duration-log))
		  (v1-dot (ly:grob-object v1-rest 'dot))
		  (v2-dot (ly:grob-object v2-rest 'dot))
		  (v1-dot-count (and (ly:grob? v1-dot)
				     (ly:grob-property v1-dot 'dot-count -1)))
		  (v2-dot-count (and (ly:grob? v2-dot)
				     (ly:grob-property v2-dot 'dot-count -1))))
	     (set! can-merge
		   (and
		    (number? v1-duration-log)
		    (number? v2-duration-log)
		    (= v1-duration-log v2-duration-log)
		    (eq? v1-dot-count v2-dot-count)))
	     (if can-merge
		 ;; keep the rest that looks best:
		 (let* ((keep-v1? (>= (rest-score v1-rest)
				      (rest-score v2-rest)))
			(rest-to-keep (if keep-v1? v1-rest v2-rest))
			(dot-to-kill (if keep-v1? v2-dot v1-dot)))
		   ;; uncomment if you're curious of which rest was chosen:
		   ;;(ly:grob-set-property! v1-rest 'color green)
		   ;;(ly:grob-set-property! v2-rest 'color blue)
		   (ly:grob-suicide! (if keep-v1? v2-rest v1-rest))
		   (if (ly:grob? dot-to-kill)
		       (ly:grob-suicide! dot-to-kill))
		   (ly:grob-set-property! rest-to-keep 'direction 0)
		   (ly:rest::y-offset-callback rest-to-keep)))))))
    (if can-merge
	#t
	(ly:rest-collision::calc-positioning-done grob))))










% startParenthesis y endParenthesis
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
% http://comments.gmane.org/gmane.comp.gnu.lilypond.general/64087
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]

% ELIMINADO










% bendBefore
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
% Adaptación
% http://lilypondblog.org/2013/08/adding-ornamentations-to-note-heads-part-1/
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]

%%%
%%% Utilities for defining new grobs, grob properties and music event types
%%% (there should be built-in commands to do that in LilyPond)
%%%
#(define (define-grob-type grob-name grob-entry)
   (let* ((meta-entry   (assoc-get 'meta grob-entry))
          (class        (assoc-get 'class meta-entry))
          (ifaces-entry (assoc-get 'interfaces meta-entry)))
     (set-object-property! grob-name 'translation-type? list?)
     (set-object-property! grob-name 'is-grob? #t)
     (set! ifaces-entry (append (case class
                                  ((Item) '(item-interface))
                                  ((Spanner) '(spanner-interface))
                                  ((Paper_column) '((item-interface
                                                     paper-column-interface)))
                                  ((System) '((system-interface
                                               spanner-interface)))
                                  (else '(unknown-interface)))
                                ifaces-entry))
     (set! ifaces-entry (uniq-list (sort ifaces-entry symbol<?)))
     (set! ifaces-entry (cons 'grob-interface ifaces-entry))
     (set! meta-entry (assoc-set! meta-entry 'name grob-name))
     (set! meta-entry (assoc-set! meta-entry 'interfaces
                                  ifaces-entry))
     (set! grob-entry (assoc-set! grob-entry 'meta meta-entry))
     (set! all-grob-descriptions
           (cons (cons grob-name grob-entry)
                 all-grob-descriptions))))

#(define-public (define-grob-property symbol type? description)
  (set-object-property! symbol 'backend-type? type?)
  (set-object-property! symbol 'backend-doc description)
  symbol)

#(defmacro*-public define-music-type (type-name parents #:rest properties)
   (let ((gproperties (gensym "properties")))
     `(let ((,gproperties (list-copy ',properties)))
        ,@(map (lambda (parent)
                `(define-event-class ',(ly:camel-case->lisp-identifier type-name)
                   ',parent))
               parents)
        (set-object-property! ',type-name
                              'music-description
                              (cdr (assq 'description ,gproperties)))
        (set! ,gproperties (assoc-set! ,gproperties 'name ',type-name))
        (set! ,gproperties (assq-remove! ,gproperties 'description))
        (hashq-set! music-name-to-property-table ',type-name ,gproperties)
        (set! music-descriptions
              (cons (cons ',type-name ,gproperties)
                    music-descriptions)))))

%%%
%%% Grob definition
%%%
#(define (head-ornamentation::print me)
   (let* ((notes (ly:grob-object me 'elements))
          (y-ref (ly:grob-common-refpoint-of-array me notes Y))
          (x-ref (ly:grob-common-refpoint-of-array me notes X))
          (x-ext (ly:relative-group-extent notes x-ref X))
          (y-ext (ly:relative-group-extent notes y-ref Y))
          (y-coord (interval-center y-ext))
          (text (ly:text-interface::print me))
          (width (/ (interval-length (ly:stencil-extent text X)) 2.0))
          (x-coord (- (car x-ext) width)))
     (ly:stencil-translate
      text
      (cons
       (- (- x-coord (ly:grob-relative-coordinate me x-ref X)) .7)
       (- (- y-coord (ly:grob-relative-coordinate me y-ref Y)) 1.5)))))

#(define-grob-type 'HeadOrnamentation
  `((text . ,#{ \markup \fontsize #-4 \musicglyph #"brackettips.up" #})
    (stencil . ,head-ornamentation::print)
    (meta . ((class . Item)
             (interfaces . (font-interface))))))

\layout {
  \context {
    \Global
    \grobdescriptions #all-grob-descriptions
  }
}

%%%
%%% Engraver
%%%
%%% The head-ornamentation engraver, with its note-head acknowledger
%%% (which add HeadOrnamentation grobs to note heads)
#(define head-ornamentation-engraver
   (make-engraver
    (acknowledgers
     ((note-head-interface engraver note-grob source)
      ;; When the note-head event attached to the note-head grob has
      ;; ornamentation events among its articulations, then create a
      ;; HeadOrnamentation grob
      (let* ((note-event (ly:grob-property note-grob 'cause)))
        (for-each
         (lambda (articulation)
           (if (memq 'head-ornamentation-event
                     (ly:event-property articulation 'class))
               (let ((ornament-grob (ly:engraver-make-grob engraver
                                                           'HeadOrnamentation
                                                           note-grob)))
                 (ly:pointer-group-interface::add-grob ornament-grob
                                                       'elements
                                                       note-grob)
                 (set! (ly:grob-parent ornament-grob Y) note-grob)
                 (set! (ly:grob-property ornament-grob 'font-size)
                       (+ (ly:grob-property ornament-grob 'font-size 0.0)
                          (ly:grob-property note-grob 'font-size 0.0))))))
         (ly:event-property note-event 'articulations)))))))

\layout {
  \context {
    \Voice
    \consists #head-ornamentation-engraver
  }
}

%%%
%%% Event type definition
%%%
#(define-music-type HeadOrnamentationEvent (music-event)
   (description . "Print an ornamentation at a note head side")
   (types . (general-music post-event event head-ornamentation-event)))

bendBefore = #(make-music 'HeadOrnamentationEvent)










% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
% Funciones propias
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]





% Índice con borde circular y un cómodo espacio interior (y no el que viene por defecto)
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
#(define-markup-command (boxindex layout props text) (markup?)
	(interpret-markup layout props
		(markup #:huge #:override '(box-padding . 0.5) #:box text)
	)
)

#(define-markup-command (boxed layout props text) (markup?)
	(interpret-markup layout props
		(markup #:huge #:override '(box-padding . 0.9) #:box text)
	)
)

% Ritardando
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
rit =
#(make-music 'CrescendoEvent
             'span-direction START
             'span-type 'text
             'span-text "rit.")

% Negra oblicua
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
rs = {
  \once \override Rest #'stencil = #ly:percent-repeat-item-interface::beat-slash
  \once \override Rest #'thickness = #0.48
  \once \override Rest #'slope = #1.7
  r4
}

% Pentagrama vacío
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
emptyStave = {
  \once \override Staff.KeySignature.stencil = ##f
  \once \override Staff.Clef.stencil = ##f
  \break
  s1
  \once \override Staff.BarLine.stencil = ##f
  \break
}










% Añadido de funciones al contexto
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
\layout {
  \context {
    \Score		
    
    % multi-mark-engraver
    \remove "Mark_engraver"
    \consists #multi-mark-engraver
    \consists "Tweak_engraver"
    
    % merge-rests-on-positioning
    \override RestCollision #'positioning-done = #merge-rests-on-positioning
    
    % aspecto de los silencios multicompás
    \override MultiMeasureRest.expand-limit = #1
    
    % margen extra de las articulaciones para evitar la colisión con las ligaduras
    \override Script.padding = #0.5
    \override Script.avoid-slur = #'inside
    
    % glissando bonito
    \override Glissando.style = #'trill
    \override Glissando.bound-details.left.padding = #1.7
    
    % dinámicas un poco mas gruesas
    \override Hairpin.thickness = #1.75
    
    % Esconde la cancelacion de accidentales en el cambio de armadura
    \override KeyCancellation.break-visibility = #'#(#f #f #f)
  }
}












% LAYOUT
% ------
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~






\header {
  title = "Desafinado"
  instrument = "Violin"
  composer = "Antonio Carlos JOBIM"
  arranger = "Arr: Frank MANTOOTH"
  poet = ""
  tagline = ""
}

\paper {
  % El definir otro tipo de fuente no permite cambiar el tamaño global del staff. Es un bug conocigo
  #(define fonts (make-pango-font-tree "LilyJAZZText" "Nimbus Sans" "Luxi Mono" (/ 20 20)))
  
  % distancia entre la parte superior de la página y el título
  top-markup-spacing #'basic-distance = 5
  
  % distancia entre el título y el primer sistema
  markup-system-spacing #'basic-distance = 25
  
  % distancia entre el texto y el primer sistema 
  top-system-spacing #'basic-distance = 15
  
  % márgen inferior
  bottom-margin = 20
    
  % número de páginas
  page-count = 4
  
  ragged-last-bottom = ##f 
  
  oddHeaderMarkup = \markup {
    \on-the-fly #not-first-page \fill-line {
      \fromproperty #'page:page-number-string
      \fromproperty #'header:title
      \fromproperty #'header:instrument
    }
  }
  
  evenHeaderMarkup = \markup {
    \on-the-fly #not-first-page \fill-line {
      \fromproperty #'header:instrument
      \fromproperty #'header:title
      \fromproperty #'page:page-number-string
    }
  }
}










% MUSIC PARTS
% -----------
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~





% scoreMarkup
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
scoreMarkup = \new Staff \with {
   \RemoveEmptyStaves
}
{
  % Oculta el primer pentagrama del markup
  \override Staff.VerticalAxisGroup #'remove-first = ##t
  
  % Indicación de tiempo
  \once \override Score.RehearsalMark.X-offset = #5
  \mark \markup \small "(Bright Bossa)"
  s1*4
  
  \mark \markup \boxed "5"
  s1*14
  
  \mark \markup \boxed "19"
  s1*12
  
  \bar "||"
  \mark \jazzTempoChange c8 c8
  \mark \markup \boxed "31"
  s1*2*3/4
  
  \bar "||"
  \mark \jazzTempoChange c4. c4
  s1
  
  \bar "||"
  \mark \jazzTempoMarkup "Tempo Primo" c2 "96"
  s1*4
  
  \mark \markup \boxed "38"
  s1*12
  
  \mark \markup \boxed "50"
  s1*12
  
  \mark \markup \boxed "62"
  s1*8
  
  \mark \markup \boxed "70"
  s1*8
  
  \mark \markup \boxed "78"
  s1*10
  
  \mark \markup \boxed "88"
  s1*6
  
  \mark \markup \boxed "94"
  s1*16
  s4
  
  \mark \markup \boxed "110"
  s1*12
  
  \mark \markup \boxed "122"
  s1*12
  
  \mark \markup \boxed "134"
  s1*8
  
  \mark \markup \boxed "142"
  s1*8
  
  \bar "||"
  \mark \jazzTempoChange c8 c8
  \mark \markup \boxed "150"
  s1*2*3/4
  
  \bar "||"
  \mark \jazzTempoChange c4. c4
  s1*2
  
  \once \override Score.RehearsalMark.self-alignment-X = #LEFT
  \mark \markup "(Conducted)"
  s1
  
  \bar "||"
  \once \override Score.RehearsalMark.self-alignment-X = #LEFT
  \mark \jazzTempoMarkup "Tempo Primo - Subito Samba" c2 "96"
  \mark \markup \boxed "155"
  s1*6
  
  \bar "||"
  \once \override Score.RehearsalMark.self-alignment-X = #LEFT
  \mark \markup {
    \column { \boxed "161" }
    \hspace #1.5
    \column { "(Funk Out)" }
  }
  s1*8
  
  \once \override Score.RehearsalMark.self-alignment-X = #LEFT
  \mark \markup {
    \column { \boxed "169" }
    \hspace #1.5
    \column { "(Get Rougher)" }
  }
  s1*8
  
  \once \override Score.RehearsalMark.self-alignment-X = #LEFT
  \mark \markup {
    \column { \boxed "177" }
    \hspace #1.5
    \column { "(Goin' Home)" }
  }
  s1*10
  
  \once \override Score.RehearsalMark.self-alignment-X = #LEFT
  \mark \markup "(Molto Rit.)"
  s1*2
  
  \once \override Score.RehearsalMark.self-alignment-X = #LEFT
  \mark \markup "(A Tempo)"
  s1*2
  
  \bar "|."
  
}





% scoreChords
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
scoreChords = \chords {
    
  \set minorChordModifier = \markup { "-" }
  
  % c1:m f2:m g2:m7
}





% music
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
violin = \relative c'' {

  % Para pintar todos los números de compás debajo del pentagrama
  \override Score.BarNumber #'break-visibility = #end-of-line-invisible
  \override Score.BarNumber #'direction = #DOWN
  \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0	
  \set Staff.explicitKeySignatureVisibility = #begin-of-line-visible
  
  % trumpet I
  \transpose c bes \relative c' {
    
    \key g \major
    
    R1*4
    
    \once \override Score.RehearsalMark.self-alignment-X = #LEFT
    \mark \markup \normal-text "(Lightly)"
    d4 -- \mp g8 ( d f c -. ) r d
    r g r d f4 -. c8 ( f
    d4 -- ) g8 ( d f c -. ) r d -.
    \break
    r2 r4 r8 d8 -> ~ 
    
    d4 g8 ( d f c -. ) r d
    r g r d f4 -. c8 ( f
    d4 -- ) g8 ( d f c -. ) r d -.
    \break
    r2 r4 r8 d8 -> ~ 
    
    d4 g8 ( d f c -. ) r d
    r g r d f4 -. c8 ( f
    d4 -- ) g8 ( d f c -. ) r d -.
    R1*3
  }
  
  % saxo I 19
  \transpose c ees, \relative c'' {
   
    \key d \major
    
    \break
    R1*12
    
    \time 3/4
    R1*2*3/4
    
    \time 4/4
    R1
    
    \break
    r2 a8 ais -. r ais
    r b r b ~ b2
    \times 2/3 { r4 d a } a8 -- ais -. r ais
    r b r b ~ b2 \>
  }
  
  % trombone I 38
  \relative c' {
     
     \key f \major
     
     R1*7 \!
     r2 r8 a ( \p c [ b ]
     
     bes?1 ~ 
     bes2. ) c8 a ~ 
     a1
     r8 d ( \mp cis [ d ] g4 -. ) f8 e ~
     
     \break
     e1 \>
     R1 \!
  }
  
  % saxo I 52
  \transpose c ees, \relative c'' {
    
    \key d \major
    
    r2 r4 r8 g \mp
    
    gis cis g fis eis2 ~
    eis2 ~ eis8 fis -. r g ~ 
    g2. r8 fis ( ~ 
    fis2 ~ fis16 \< gis a b cis \> b a gis
    
    fis2 ) \!
    
  }
  
  % trumpet I 57'5
  \transpose c bes, \relative c'' {
    
    \key g \major
    
    r2
    R1*3
    fis2 -> \f g ->
    
    \break
    a4. b8 a b -. r a ~
    a4. g8 fis g -. r a ~
    a4. e8 -. r b' -. r a -> ~
    a2 \breathe b -- \ff
    
    \break
    c4. b8 -. r c r b (
    d4 c b c8 a -> ~
    a1 ~
    a1 ) \>
  }
  
  % alto I 70 (flute)
  \relative c'' {
   
    \key f \major
    
    \break
    r8 \! c4 -. \mp d8 ( e f -. ) r e ( ~
    e4. d8 cis d -. ) r f ~
    f4 cis8 cis ~ cis2 ~
    cis2. r4
    
    \break
    d4 -. r8 e ( f g -. ) r f ~ 
    f4 e -- dis8 e -. r d' ~ 
    d4 des8 c ~ c4. b8
    bes? d -> ~ d2.
    
    \break
    R1*3
    r2 d,4 ( \mp e
    
    f8 -- ) f4 -. f8 r f4 -. f8 -.
    r f4 -. f8 f4 -- \bendBefore f -.
    \break
    f8 -- f4 -. f8 r f4 -. f8 -.
    r f4 -. f8 f4 -- \bendBefore f -.
    
    f8 -- f4 -. f8 r f4 -. f8 -> ~
    f2 \< ~ f8 \! r r4
    \break
    R1*2
    
    c'4 -- \mp f8 ( c ees bes -. ) r c
    r f -. r c ees4 -. bes8 ( ees
    c4 -- ) f8 ( c ees bes -. ) r c -> ~
    c1 \>
    
    \break
    R1*15 \!
  }
  
  % alto I 109
  \transpose c ees, \relative c'' {
    
    \key d \major
    \cadenzaOn \hideNotes f4 \glissando \unHideNotes \cadenzaOff
    b,2 ( \p bes
    
    a1 ~
    a2 ) r8 a4 -. ais8 ~
    ais1 ~
    \break
    ais2 r8 ais4 -. b8 ~
    
    b1 ~
    b4. cis8 -. r d4 -. e8 --
    c1 ~
    c
    
    \break
    r2 r8 fis4 -. a8 (
    g1 )
    r2 r8 e4 -. g8 (
    fis e -. ) r d r cis r b --
    
    \break
    ais1 \> ~
    ais \!
    b1 ~
    b
    
    cis1 ~
    cis
    \break
    cis2. r8 d ( \mp
    e d cis4 -. ) r2
  }
  
  % trumpet II 130
  \transpose c bes, \relative c' {
   
    \key g \major
    
    r2 r4 r8 fis ( ~
    fis2 ~ fis8 e dis e
    \break
    fis1 )
    fis2 ( \mf g
    
    a1 ~
    a ~
    a ~
    a2 )
  }
  
  % alto I 137'5
  \transpose c ees, \relative c'' {
    
    \key d \major
    
    r8 fis \mp a [ g -> \fp ] ~
    
    \break
    g2. ~ g8 g -> \fp ~
    g1
    fis2 ( \mf \glissando cis'
    b4. cis8 -> ~ cis2 \< )
  }
  
  % trumpet I 142
  \transpose c bes, \relative c'' {
     
     \key g \major
     
     \break
     r8 \! d4 -. \f e8 fis -- g -. r fis ~ 
     fis4. e8 ( dis e -. ) r g -> ~
     g4 dis8 dis ~ dis2 ~
     dis2
  }
  
  % tacet 145'5
  r2
  
  \break
  R1*4
  
  \time 3/4
  R1*2*3/4
  
  % trumpet I 152
  \transpose c bes, \relative c'' {
    
    \time 4/4
    \key g \major
    
    R1*2
    r4 e4 ( \mf fis2 ) \fermata
    
    \once \override BreathingSign.text = \markup { \musicglyph #"scripts.caesura.straight" }
    \breathe
  }
  
  % alto I 155
  \transpose c ees, \relative c''' {
    
    \key d \major
    
    \break
    a8 -- \mp d -. r gis, r d' r fis,
    r d' r fis, gis4 -- d' -.
    f,8 -- d' -. r fis, r d' r f,
    r d' r f, e4 -- d' -.
    
    \break
    a8 -- d -. r gis, r d' r b -> ~
    b2 \< ~ b8 \! r r4
  }
  
  % trumpet II 161
  \transpose c bes, \relative c'' {
   
    \key g \major
    
    R1*3
    r2 r8 fis ( \mf eis [ fis ]
    
    \break
    a -> aes g2. ~
    g1 ~
    g1 )
    r2 r8 fis ( eis [ fis ]
    
    a -> aes g2. ~
    g1 ~
    \break
    g1 )
    r2 r8 fis ( eis [ fis ]
    
    a -> aes g2. ~
    g1 ~
    g1 )
    r2 r8 fis ( \f eis [ fis ]
    
    \break
    a -> aes g2. ~
    g1 ~
    g1 )
    r2 r8 fis ( \ff eis [ fis ]
    
    a -> aes g2. ~
    g1 ~
    g1 )
  }
  
  % trumpet III & IV 184
  \transpose c bes, \relative c'' {
    
    \key g \major
    
    \break
    R1*3
    
    R1*2
    r2 r4 <g d'>8 ( \ff <fis cis'> 
    <f c'>1 ) \fermata
  }
}





% score
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
\score {
  <<
    \scoreMarkup
    \scoreChords
    \new StaffGroup <<
      \new Staff { \violin }
    >>
  >>
}


