\version "2.18.2"

\include "lily-jazz/ignatzek-jazz-chords.ily"
\include "lily-jazz/lily-jazz.ily"





% PLUGINS 1.2
% -----------
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~





% multi-mark-engraver
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
% Aporta la posibilidad de pintar un rehearsal mark al final de una línea y 
% al comienzo de la siguiente.
% http://lists.gnu.org/archive/html/lilypond-user/2011-08/msg00157.html
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
#(define (multi-mark-engraver ctx)
   (let ((texts '())
         (final-texts '())
         (events '()))

     `((start-translation-timestep
        . ,(lambda (trans)
             (set! final-texts '())))

       (listeners
        (mark-event
         . ,(lambda (trans ev)
              (set! events (cons ev events)))))

       (acknowledgers
        (break-alignment-interface
         . ,(lambda (trans grob source)
              (for-each (lambda (mark)
                          (set! (ly:grob-parent mark X) grob))
                        texts))))

       (process-music
        . ,(lambda (trans)
             (for-each
              (lambda (ev)
                (let* ((mark-grob
                        (ly:engraver-make-grob trans 'RehearsalMark ev))
                       (label (ly:event-property ev 'label))
                       (formatter (ly:context-property ctx 'markFormatter)))

                  (if (and (procedure? formatter)
                           (not (markup? label)))
                      (begin
                       (if (not (number? label))
                           (set! label
                                 (ly:context-property ctx 'rehearsalMark)))

                       (if (and (integer? label)
                                (exact? label))
                           (set! (ly:context-property ctx 'rehearsalMark)
                                 (1+ label)))

                       (if (number? label)
                           (set! label (apply formatter (list label ctx)))
                           (ly:warning "rehearsalMark must have
integer value"))))

                  (if (markup? label)
                      (begin
                       (set! (ly:grob-property mark-grob 'text) label)
                       (let ((dir (ly:event-property ev 'direction)))
                         (and (ly:dir? dir)
                              (set! (ly:grob-property mark-grob 'direction)
                                    dir))))
                      (ly:warning "mark label must be a markup object"))

                  (set! texts (cons mark-grob texts))))
              (reverse events))))

       (stop-translation-timestep
        . ,(lambda (trans)
             (if (pair? texts)
                 (let ((staves (ly:context-property ctx 'stavesFound))
                       (priority-index 0))
                   (for-each (lambda (grob)
                               (let ((my-priority (ly:grob-property
grob 'outside-staff-priority 1500)))
                                 (for-each (lambda (stave)

(ly:pointer-group-interface::add-grob grob 'side-support-elements
                                               stave))
                                           staves)
                                 (set! (ly:grob-property grob
'outside-staff-priority) (+ my-priority priority-index))
                                 (set! priority-index (1+ priority-index))
                                 (set! final-texts (cons grob final-texts))))
                             (reverse texts))
                     (set! texts '())
                     (set! events '())))))

        (finalize
         . ,(lambda (trans)
              (and (pair? final-texts)
                   (for-each (lambda (grob)
                               (set! (ly:grob-property grob 'break-visibility)
                                     end-of-line-visible))
                             final-texts)))))))


% Funciones originales
%{
myMark =
#(define-music-function (parser location text) (markup?)
  (make-music 'MarkEvent
              'label text))
			  
toCoda = {
	\tweak #'self-alignment-X #RIGHT
	\tweak #'break-visibility #begin-of-line-invisible
	\myMark \markup { \hspace #1.25 \raise #1.25 \musicglyph #"scripts.coda" }
}
%}

% Función maqueada.
% Se apoya en el código obtenido por consola al ejecutar { \displayMusic { \toCoda }}
eolMarkup =
#(define-music-function (parser location text) (markup?)
	(make-music 
		'MarkEvent 
		'tweaks (list 
			(cons (quote self-alignment-X) 1)
			(cons (quote break-visibility) #(#t #t #f))
		) 
		'label (markup
			#:line (
				#:hspace 1.25
				#:raise 1.25
				text
			)
		)
	)
)










% rest-score y merge-rests-on-positioning
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
% Permite realizar la fusión de los silencios entre dos voces en un mismo pentagrama.
% https://code.google.com/p/lilypond/issues/detail?id=1228 (fichero merge-rests.ily)
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]

#(define (rest-score r)
  (let ((score 0)
	(yoff (ly:grob-property-data r 'Y-offset))
	(sp (ly:grob-property-data r 'staff-position)))
    (if (number? yoff)
	(set! score (+ score 2))
	(if (eq? yoff 'calculation-in-progress)
	    (set! score (- score 3))))
    (and (number? sp)
	 (<= 0 2 sp)
	 (set! score (+ score 2))
	 (set! score (- score (abs (- 1 sp)))))
    score))

#(define (merge-rests-on-positioning grob)
  (let* ((can-merge #f)
	 (elts (ly:grob-object grob 'elements))
	 (num-elts (and (ly:grob-array? elts)
			(ly:grob-array-length elts)))
	 (two-voice? (= num-elts 2)))
    (if two-voice?
	(let* ((v1-grob (ly:grob-array-ref elts 0))
	       (v2-grob (ly:grob-array-ref elts 1))
	       (v1-rest (ly:grob-object v1-grob 'rest))
	       (v2-rest (ly:grob-object v2-grob 'rest)))
	  (and
	   (ly:grob? v1-rest)
	   (ly:grob? v2-rest)
	   (let* ((v1-duration-log (ly:grob-property v1-rest 'duration-log))
		  (v2-duration-log (ly:grob-property v2-rest 'duration-log))
		  (v1-dot (ly:grob-object v1-rest 'dot))
		  (v2-dot (ly:grob-object v2-rest 'dot))
		  (v1-dot-count (and (ly:grob? v1-dot)
				     (ly:grob-property v1-dot 'dot-count -1)))
		  (v2-dot-count (and (ly:grob? v2-dot)
				     (ly:grob-property v2-dot 'dot-count -1))))
	     (set! can-merge
		   (and
		    (number? v1-duration-log)
		    (number? v2-duration-log)
		    (= v1-duration-log v2-duration-log)
		    (eq? v1-dot-count v2-dot-count)))
	     (if can-merge
		 ;; keep the rest that looks best:
		 (let* ((keep-v1? (>= (rest-score v1-rest)
				      (rest-score v2-rest)))
			(rest-to-keep (if keep-v1? v1-rest v2-rest))
			(dot-to-kill (if keep-v1? v2-dot v1-dot)))
		   ;; uncomment if you're curious of which rest was chosen:
		   ;;(ly:grob-set-property! v1-rest 'color green)
		   ;;(ly:grob-set-property! v2-rest 'color blue)
		   (ly:grob-suicide! (if keep-v1? v2-rest v1-rest))
		   (if (ly:grob? dot-to-kill)
		       (ly:grob-suicide! dot-to-kill))
		   (ly:grob-set-property! rest-to-keep 'direction 0)
		   (ly:rest::y-offset-callback rest-to-keep)))))))
    (if can-merge
	#t
	(ly:rest-collision::calc-positioning-done grob))))










% startParenthesis y endParenthesis
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
% http://comments.gmane.org/gmane.comp.gnu.lilypond.general/64087
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]

% ELIMINADO










% bendBefore
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
% Adaptación
% http://lilypondblog.org/2013/08/adding-ornamentations-to-note-heads-part-1/
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]

%%%
%%% Utilities for defining new grobs, grob properties and music event types
%%% (there should be built-in commands to do that in LilyPond)
%%%
#(define (define-grob-type grob-name grob-entry)
   (let* ((meta-entry   (assoc-get 'meta grob-entry))
          (class        (assoc-get 'class meta-entry))
          (ifaces-entry (assoc-get 'interfaces meta-entry)))
     (set-object-property! grob-name 'translation-type? list?)
     (set-object-property! grob-name 'is-grob? #t)
     (set! ifaces-entry (append (case class
                                  ((Item) '(item-interface))
                                  ((Spanner) '(spanner-interface))
                                  ((Paper_column) '((item-interface
                                                     paper-column-interface)))
                                  ((System) '((system-interface
                                               spanner-interface)))
                                  (else '(unknown-interface)))
                                ifaces-entry))
     (set! ifaces-entry (uniq-list (sort ifaces-entry symbol<?)))
     (set! ifaces-entry (cons 'grob-interface ifaces-entry))
     (set! meta-entry (assoc-set! meta-entry 'name grob-name))
     (set! meta-entry (assoc-set! meta-entry 'interfaces
                                  ifaces-entry))
     (set! grob-entry (assoc-set! grob-entry 'meta meta-entry))
     (set! all-grob-descriptions
           (cons (cons grob-name grob-entry)
                 all-grob-descriptions))))

#(define-public (define-grob-property symbol type? description)
  (set-object-property! symbol 'backend-type? type?)
  (set-object-property! symbol 'backend-doc description)
  symbol)

#(defmacro*-public define-music-type (type-name parents #:rest properties)
   (let ((gproperties (gensym "properties")))
     `(let ((,gproperties (list-copy ',properties)))
        ,@(map (lambda (parent)
                `(define-event-class ',(ly:camel-case->lisp-identifier type-name)
                   ',parent))
               parents)
        (set-object-property! ',type-name
                              'music-description
                              (cdr (assq 'description ,gproperties)))
        (set! ,gproperties (assoc-set! ,gproperties 'name ',type-name))
        (set! ,gproperties (assq-remove! ,gproperties 'description))
        (hashq-set! music-name-to-property-table ',type-name ,gproperties)
        (set! music-descriptions
              (cons (cons ',type-name ,gproperties)
                    music-descriptions)))))

%%%
%%% Grob definition
%%%
#(define (head-ornamentation::print me)
   (let* ((notes (ly:grob-object me 'elements))
          (y-ref (ly:grob-common-refpoint-of-array me notes Y))
          (x-ref (ly:grob-common-refpoint-of-array me notes X))
          (x-ext (ly:relative-group-extent notes x-ref X))
          (y-ext (ly:relative-group-extent notes y-ref Y))
          (y-coord (interval-center y-ext))
          (text (ly:text-interface::print me))
          (width (/ (interval-length (ly:stencil-extent text X)) 2.0))
          (x-coord (- (car x-ext) width)))
     (ly:stencil-translate
      text
      (cons
       (- (- x-coord (ly:grob-relative-coordinate me x-ref X)) .7)
       (- (- y-coord (ly:grob-relative-coordinate me y-ref Y)) 1.5)))))

#(define-grob-type 'HeadOrnamentation
  `((text . ,#{ \markup \fontsize #-4 \musicglyph #"brackettips.up" #})
    (stencil . ,head-ornamentation::print)
    (meta . ((class . Item)
             (interfaces . (font-interface))))))

\layout {
  \context {
    \Global
    \grobdescriptions #all-grob-descriptions
  }
}

%%%
%%% Engraver
%%%
%%% The head-ornamentation engraver, with its note-head acknowledger
%%% (which add HeadOrnamentation grobs to note heads)
#(define head-ornamentation-engraver
   (make-engraver
    (acknowledgers
     ((note-head-interface engraver note-grob source)
      ;; When the note-head event attached to the note-head grob has
      ;; ornamentation events among its articulations, then create a
      ;; HeadOrnamentation grob
      (let* ((note-event (ly:grob-property note-grob 'cause)))
        (for-each
         (lambda (articulation)
           (if (memq 'head-ornamentation-event
                     (ly:event-property articulation 'class))
               (let ((ornament-grob (ly:engraver-make-grob engraver
                                                           'HeadOrnamentation
                                                           note-grob)))
                 (ly:pointer-group-interface::add-grob ornament-grob
                                                       'elements
                                                       note-grob)
                 (set! (ly:grob-parent ornament-grob Y) note-grob)
                 (set! (ly:grob-property ornament-grob 'font-size)
                       (+ (ly:grob-property ornament-grob 'font-size 0.0)
                          (ly:grob-property note-grob 'font-size 0.0))))))
         (ly:event-property note-event 'articulations)))))))

\layout {
  \context {
    \Voice
    \consists #head-ornamentation-engraver
  }
}

%%%
%%% Event type definition
%%%
#(define-music-type HeadOrnamentationEvent (music-event)
   (description . "Print an ornamentation at a note head side")
   (types . (general-music post-event event head-ornamentation-event)))

bendBefore = #(make-music 'HeadOrnamentationEvent)










% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
% Funciones propias
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]





% Índice con borde circular y un cómodo espacio interior (y no el que viene por defecto)
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
#(define-markup-command (boxindex layout props text) (markup?)
	(interpret-markup layout props
		(markup #:huge #:override '(box-padding . 0.5) #:box text)
	)
)

#(define-markup-command (boxed layout props text) (markup?)
	(interpret-markup layout props
		(markup #:huge #:override '(box-padding . 0.9) #:box text)
	)
)

% Ritardando
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
rit =
#(make-music 'CrescendoEvent
             'span-direction START
             'span-type 'text
             'span-text "rit.")

% Negra oblicua
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
rs = {
  \once \override Rest #'stencil = #ly:percent-repeat-item-interface::beat-slash
  \once \override Rest #'thickness = #0.48
  \once \override Rest #'slope = #1.7
  r4
}










% Añadido de funciones al contexto
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
\layout {
  \context {
    \Score		
    
    % multi-mark-engraver
    \remove "Mark_engraver"
    \consists #multi-mark-engraver
    \consists "Tweak_engraver"
    
    % merge-rests-on-positioning
    \override RestCollision #'positioning-done = #merge-rests-on-positioning
    
    % aspecto de los silencios multicompás
    \override MultiMeasureRest.expand-limit = #1
    
    % margen extra de las articulaciones para evitar la colisión con las ligaduras
    \override Script.padding = #1.5
    \override Script.avoid-slur = #'outside
    
    % glissando en zigzag
    \override Glissando #'style = #'zigzag
  }
}












% LAYOUT
% ------
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~






\header {
  title = "A day in the life of a fool"
  instrument = "Soprano Sax"
  composer = ""
  arranger = "Arr: Jerry NOWAK"
  poet = \markup {
    \center-column {
      \line { \tiny "Reducción y edición: Gabriel PALACIOS" }
      \line { \tiny "gabriel.ps.ms@gmail.com" }
    }
  }
  tagline = \markup {
    \center-column {
      \line { \tiny "Reducción y edición: Gabriel PALACIOS" }
      \line { \tiny "gabriel.ps.ms@gmail.com" }
    }
  }
}

\paper {
  % El definir otro tipo de fuente no permite cambiar el tamaño global del staff. Es un bug conocigo
  #(define fonts (make-pango-font-tree "LilyJAZZText" "Nimbus Sans" "Luxi Mono" (/ 20 20)))
  
  % distancia entre la parte superior de la página y el título
  top-markup-spacing #'basic-distance = 5
  
  % distancia entre el título y el primer sistema
  markup-system-spacing #'basic-distance = 25
  
  % distancia entre el texto y el primer sistema 
  top-system-spacing #'basic-distance = 15
  
  % márgen inferior
  bottom-margin = 20
    
  % número de páginas
  % page-count = 3
  
  ragged-last-bottom = ##f 
  
  oddHeaderMarkup = \markup {
    \on-the-fly #not-first-page \fill-line {
      \fromproperty #'page:page-number-string
      \fromproperty #'header:title
      \fromproperty #'header:instrument
    }
  }
  
  evenHeaderMarkup = \markup {
    \on-the-fly #not-first-page \fill-line {
      \fromproperty #'header:instrument
      \fromproperty #'header:title
      \fromproperty #'page:page-number-string
    }
  }
}










% MUSIC PARTS
% -----------
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~





% scoreMarkup
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
scoreMarkup = \new Staff \with {
   \RemoveEmptyStaves
}
{
  % Oculta el primer pentagrama del markup
  \override Staff.VerticalAxisGroup #'remove-first = ##t
  
  s1*20
  
  \mark \markup \large \jazzglyph #"scripts.varsegnojazz"
  s1*19
  
  \mark \markup {\normalsize "To Coda  " \tiny \jazzglyph #"scripts.varcodajazz"}
  s1*2
  s1*16
  
  \eolMarkup \markup \normalsize "D.S. al Coda"
  \bar "||"
  \mark \markup {\large \jazzglyph #"scripts.varcodajazz" " Coda"}
  s1*8
  
  \bar "|."
}





% scoreChords
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
scoreChords = \chords {
  % c1:m f2:m g2:m7
}





% music
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
violin = \relative c' {

  % Para pintar todos los números de compás debajo del pentagrama
  % \override Score.BarNumber #'break-visibility = #end-of-line-invisible
  % \override Score.BarNumber #'direction = #DOWN
  % \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0	
  
  \key g \minor
  
  R1*4
  
  r8 c(-> \mp d[) g]-> ~ g4 d(
  ees) g8-- a-. r8 fis4.
  r8 g4.-> \mf ~ g4. a8-.
  r8 ees-. r ees-> ~ ees4 r
  
  \break
  r8 c(-> \mp d[) g]-> ~ g4 d
  ees8 g8 ees f ~ f2
  d1 \mf 
  g4. d8 ees8-. r r4
  
  \break
  ees1( \mp
  d)
  d2 g4( fis)
  f1(
  
  \break
  ees)
  d
  R1*2
  
  \break
  r8 c( \mp d[) f] ~ f2
  r8 d( c[) ees] ~ ees2
  R1*2
  
  \break
  r4 cis4 ~ cis8 d4.
  ees4. d8 ~ d2
  r4 g4-> \mf ~ g8 g4.->
  f4.-> ees8-> ~ ees4 f4--
  
  \break
  ees4-> r r2
  R1*7
  
  \break
  ees2( \mp f 
  g f)
  ees( d)
  d1 \<
  g4( ees fis2) \f
  
  \break
  R1*16
  
  \break
  \times 2/3 { r4 d \mf d } f( g)
  e( d) f-. r
  r g2.(
  f2.) r4
  
  \break
  r2 r4 r8 e-- \mf \<
  ees?4( g) g( bes)
  d-> \f r8 d,-> \mp ~ d2 ~ 
  d1 \fermata
}





% score
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
\score {
  <<
    \scoreMarkup
    \scoreChords
    \new StaffGroup <<
      \new Staff { \transpose bes c' \violin }
    >>
  >>
}

%{
altoSaxI = \relative c'' {

  % Para pintar todos los números de compás debajo del pentagrama
  % \override Score.BarNumber #'break-visibility = #end-of-line-invisible
  % \override Score.BarNumber #'direction = #DOWN
  % \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0	
  
  \key a \minor
  
  e4. -> \f e8 c8 [ e8 ] -. r8 d8 -> 
  r8 b8 ( d8 ) [ f8 ] r8 g4. -> 
  r8 e8 \mp \< -- e4 -. d8 -- [
  d8 -. ] r8 f8 -. r e8 -> \f e8 [ e8 -> ] r2
  
  R1*2
  \break
  r4 r8 e8 -- b8 -- [ c8 -. ] r8 d8 -. 
  r8 f8 -. r8 e8 ~ -> e4 r 
  
  R1*2
  \times 2/3 { r4 b4 \mf ( d4 ) } e4 ( d4 )
  g4. c,8 f8 -. r8 r4
  \break
  r4 f,4 \mp ~ f8[ a8 d8 ( c8 ) ] 
  
  b8 ( [ a8 b8 ) g8 ] a2
  r4 e4 ~ e8 [ a8 ( c8 ) e8 ] g8 ( [
  f8 e8 ) c8 ] ~ c2
  
  \break
  r4 d4 ~ d8 [ a8 ( b8 ) d8 ] 
  f8 ( [ e8 d8 ) b8 ] ~ b2 
  r4 r8 b8 \mf c8 -> [ e8 -. ] r f8 ~ -> 
  f8 d4 -> e8 -> r2

  \break
  R1*2
  r4 r8 c8 -. \mf r8 e8 -. r8 g8 -- 
  f8 ( [ e8 d8 ) e8 ] -. r2
  
  R1*2
  \break
  r4 g4 ~ -> \mf g8 f4. -> 
  ees4. -> bes8 ~ -> bes4 c4 -- 
  d4 -> r4 r2 
  
  r4 r8 f8 \mp f8 -- [ f8 -. ] r8 e?8 -.
  R1
  \break
  r8 d8 -- d4 -. f8 -- [ f8 -. ] r4 
  
  r4 c8 ( [ b8 ) ] e4. d8 
  f8 [ d8 ] e4 -. r2 
  r4 r8 e8 -- f8 -- [ f8 -. ] r8 e8 -. 
  r e8 -. r f8 -- e4 -. r4
  
  \pageBreak
  
  R1*3
  r8 a,, ( \< b [ ) c ] \times 2/3 { e4 c a }
  e' ( d f2 ) \f
  
  \break
  \transpose bes ees \relative c'' {
    r8 e -- e4 -. d8 -- d -. r b (
    c d e ) a, -. r c4. ->
    r8 b ( -> \mp \< ais [ b ] ) d ( -> cis d ) b
    \break
    c ( -> d e ) dis ~ -> \f dis2
    
    r8 d -- d4 -. e8 -- e -. r b
    a ( a c ) c ~ -> c4. c8
    b -- b -. r a -. r b -. r b ~ ->
    \break
    b4 d -. d4. -> c8 -> ~
    
    c4. d8 -- e -- e -. r e -> ~
    e4 d -. c2 -> 
    r8 b -- \mp \< b [ -- b ] -. r cis4. ->
    r8 d -- c? [ -- d ] -. r e4. ->
    
    \break
    fis4. -> \f e8 -- d -- c -. r a -.
    r cis -. r c -> ~ c2
    r4 r8 b b [ -> d ] -. r e -> ~
    e f4 -> g8 -> r2
    
    \break
    \times 2/3 { r4 b, \mf b } d ( e )
    cis ( b ) d -. r
    r e2. \mp (
    d2. ) r4
    
    \break
    r4 r8 d -- \mp \< d -- b -. r cis --
    c?4 ( e ) e ( g )
    b -> \f r8 b, -> \mp ~ b2 ~ b1 \fermata
  }
  
}

tenorSaxI = \relative c'' {

  % Para pintar todos los números de compás debajo del pentagrama
  % \override Score.BarNumber #'break-visibility = #end-of-line-invisible
  % \override Score.BarNumber #'direction = #DOWN
  % \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0	
  
  \key a \minor
  
  c4. -> \f c8 a8 [ c8 ] -. r8 b8 -> 
  r8 a8 ( b8 ) [ d8 ] r8 d4. -> 
  r8 c8 \mp \< -- c4 -. b8 -- [
  b8 -. ] r8 d8 -. r c8 -> \f c8 [ c8 -> ] r2
  
  R1*2
  \break
  r4 r8 e8 -- b8 -- [ c8 -. ] r8 b8 -. 
  r8 b8 -. r8 c8 ~ -> c4 r 
  
  R1*2
  \times 2/3 { r4 g4 \mf ( b4 ) } b4 -- b4 --
  e4. a,8 c8 -. r8 r4
  \break
  r4 f,4 \mp ~ f8[ a8 d8 ( c8 ) ] 
  
  b8 ( [ a8 b8 ) g8 ] a2
  r4 e4 ~ e8 [ a8 ( c8 ) e8 ] g8 ( [
  f8 e8 ) c8 ] ~ c2
  
  \break
  r4 d4 ~ d8 [ a8 ( b8 ) d8 ] 
  f8 ( [ e8 d8 ) b8 ] ~ b2 
  r4 r8 g8 \mf a8 -> [ c8 -. ] r d8 ~ -> 
  d8 b4 -> c8 -> r2

  \break
  R1*2
  r4 r8 a8 -. \mf r8 c8 -. r8 e8 -- 
  d8 ( [ b8 b8 ) c8 ] -. r2
  
  R1*2
  \break
  r4 e4 ~ -> \mf e8 d4. -> 
  c4. -> g8 ~ -> g4 bes4 -- 
  a4 -> r4 r2 
  
  r4 r8 d8 \mp d8 -- [ d8 -. ] r8 d8 -.
  R1
  \break
  r8 b8 -- b4 -. d8 -- [ d8 -. ] r4 
  
  r4 c8 ( [ b8 ) ] b4. b8 
  c8 [ c8 ] b4 -. r2 
  r4 r8 c8 -- d8 -- [ d8 -. ] r8 c8 -. 
  r c8 -. r d8 -- c4 -. r4
  
  \pageBreak
  
  R1*3
  r8 a ( \< b [ ) c ] \times 2/3 { e4 c a }
  b ( b d2 ) \f
  
  \break
  r8 e -- e4 -. c8 -- c -. r a (
  b cis d ) gis, -. r b4. ->
  r8 a ( -> \mp \< gis [ a ] ) c? ( -> b c ) a
  \break
  b ( -> cis d ) d ~ -> \f d2
  
  r8 c -- c4 -. e8 -- e -. r c
  a ( a c ) b ~ -> b4. b8
  b -- a -. r b -. r b -. r a ~ ->
  \break
  a4 c -. des4. -> c8 -> ~
  
  c4. c8 -- d? -- e -. r d -> ~
  d4 d -. b2 -> 
  r8 b -- \mp \< a [ -- b ] -. r c4. ->
  r8 c -- b [ -- c ] -. r e4. ->
  
  \break
  d4. -> \f d8 -- cis -- b -. r b -.
  r a -. r gis -> ~ gis2
  r4 r8 b a [ -> c ] -. r d -> ~
  d d4 -> d8 -> r2
  
  \break
  \times 2/3 { r4 a \mf b } c ( e )
  c ( a ) des -. r
  r c2. \mp (
  b2. ) r4
  
  \break
  r4 r8 c -- \mp \< c -- a -. r c --
  c4 ( ees ) d -- d --
  c -> \f r8 b -> \mp ~ b2 ~ b1 \fermata
}
tenorSaxII = \relative c'' {

  % Para pintar todos los números de compás debajo del pentagrama
  % \override Score.BarNumber #'break-visibility = #end-of-line-invisible
  % \override Score.BarNumber #'direction = #DOWN
  % \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0	
  
  \key a \minor
  
  a4. -> \f a8 fis8 [ a8 ] -. r8 a8 -> 
  r8 f?8 ( a8 ) [ b8 ] r8 gis4. -> 
  r8 a8 \mp \< -- a4 -. a8 -- [ a8 -. ] r8 b8 -. 
  r g?8 -> \f g8 [ g8 -> ] r2
  
  R1*2
  \break
  r4 r8 e'8 -- b8 -- [ c8 -. ] r8 a8 -. 
  r8 a8 -. r8 gis8 ~ -> gis4 r 
  
  R1*2
  \times 2/3 { r4 e4 \mf ( g4 ) } g4 -- g4 --
  c4. g8 g8 -. r8 r4
  \break
  r4 f4 \mp ~ f8[ a8 d8 ( c8 ) ] 
  
  b8 ( [ a8 b8 ) g8 ] a2
  r4 e4 ~ e8 [ a8 ( c8 ) e8 ] g8 ( [
  f8 e8 ) c8 ] ~ c2
  
  \break
  r4 d4 ~ d8 [ a8 ( b8 ) d8 ] 
  f8 ( [ e8 d8 ) b8 ] ~ b2 
  r4 r8 e,8 \mf g8 -> [ a8 -. ] r b8 ~ -> 
  b8 a4 -> g8 -> r2

  \break
  R1*2
  r4 r8 g8 -. \mf r8 a8 -. r8 c8 -- 
  b8 ( [ a8 a8 ) gis8 ] -. r2
  
  R1*2
  \break
  r4 c4 ~ -> \mf c8 c4. -> 
  bes4. -> f8 ~ -> f4 g4 -- 
  c4 -> r4 r2 
  
  r4 r8 b?8 \mp b8 -- [ b8 -. ] r8 b8 -.
  R1
  \break
  r8 a8 -- a4 -. b8 -- [ b8 -. ] r4 
  
  r4 c8 ( [ b8 ) ] gis4. gis8 
  a8 [ a8 ] gis4 -. r2 
  r4 r8 a8 -- c8 -- [ c8 -. ] r8 a8 -. 
  r a8 -. r c8 -- a4 -. r4
  
  \pageBreak
  
  R1*3
  r8 a ( \< b [ ) c ] \times 2/3 { e4 c a }
  a ( a b2 ) \f
  
  \break
  r8 c -- c4 -. a8 -- a -. r g (
  a bes b ) f -. r gis4. ->
  r8 g ( -> \mp \< fis [ g ] ) a ( -> gis a ) g
  \break
  a ( -> bes b ) c ~ -> \f c2
  
  r8 a -- a4 -. c8 -- c -. r a
  f ( f a ) a ~ -> a4. aes8
  g -- g -. r g -. r g -. r g ~ ->
  \break
  g4 a -. bes4. -> a8 -> ~
  
  a4. a8 -- c -- c -. r b? -> ~
  b4 b -. a2 -> 
  r8 g -- \mp \< g [ -- g ] -. r bes4. ->
  r8 a -- a [ -- a ] -. r c4. ->
  
  \break
  b?4. -> \f b8 -- bes -- a -. r gis -.
  r c -. r b?  -> ~ b2
  r4 r8 g g [ -> a ] -. r b -> ~
  b a4 -> gis8 -> r2
  
  \break
  \times 2/3 { r4 g \mf g } a ( c )
  a ( g ) bes -. r
  r d2. \mp (
  e2. ) r4
  
  \break
  r4 r8 a, -- \mp \< a -- g -. r a --
  a4 ( c ) b -- c --
  a -> \f r4 r2
  R1 ^\markup \jazzglyph #"scripts.ufermatajazz"
}

baritoneSax = \relative c' {

  % Para pintar todos los números de compás debajo del pentagrama
  % \override Score.BarNumber #'break-visibility = #end-of-line-invisible
  % \override Score.BarNumber #'direction = #DOWN
  % \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0	
  
  \key a \minor
  
  fis4. -> \f fis8 e8 [ fis8 ] -. r8 f8 -> 
  r8 d8 ( f8 ) [ a8 ] r8 c4. -> 
  r8 fis,8 \mp \< -- fis4 -. f8 -- [ f8 -. ] r8 a8 -. 
  r c8 -> \f c8 [ c8 -> ] r2
  
  R1*2
  \break
  r4 r8 e8 -- b8 -- [ c8 -. ] r8 f,8 -. 
  r8 f8 -. r8 f8 ~ -> f4 r 
  
  R1*2
  \times 2/3 { r4 d4 \mf ( e4 ) } e4 -- e4 --
  a4. e8 f8 -. r8 r4
  \break
  r4 f4 \mp ~ f8[ a8 d8 ( c8 ) ] 
  
  b8 ( [ a8 b8 ) g8 ] a2
  r4 e4 ~ e8 [ a8 ( c8 ) e8 ] g8 ( [
  f8 e8 ) c8 ] ~ c2
  
  \break
  r4 d4 ~ d8 [ a8 ( b8 ) d8 ] 
  f8 ( [ e8 d8 ) b8 ] ~ b2 
  r4 r8 d,8 \mf e8 -> [ g8 -. ] r a8 ~ -> 
  a8 f4 -> c'8 -> r2

  \break
  R1*2
  r4 r8 e,8 -. \mf r8 g8 -. r8 a8 -- 
  a8 ( [ g8 f8 ) e8 ] -. r2
  
  R1*2
  \break
  r4 a4 ~ -> \mf a8 a4. -> 
  g4. -> des8 ~ -> des4 f4 -- 
  f4 -> r4 r2 
  
  r4 r8 a8 \mp a8 -- [ a8 -. ] r8 gis8 -.
  R1
  \break
  r8 f8 -- f4 -. a8 -- [ a8 -. ] r4 
  
  r4 c8 ( [ b8 ) ] e,4. e8 
  g8 [ g8 ] fis4 -. r2 
  r4 r8 g8 -- a8 -- [ a8 -. ] r8 g8 -. 
  r g8 -. r a8 -- g4 -. r4
  
  \pageBreak
  
  R1*3
  r8 a ( \< b [ ) c ] \times 2/3 { e4 c a }
  e ( f gis2 ) \f
  
  \break
  \transpose bes ees \relative c' {
    r8 e' -- e4 -. d8 -- d -. r b (
    c d e ) a, -. r c4. ->
    r8 b ( -> \mp \< ais [ b ] ) d ( -> cis d ) b
    \break
    c? ( -> d e ) dis ~ -> \f dis2
    
    r8 d? -- d4 -. e8 -- e -. r d
    a ( a c ) c ~ -> c4. c8
    b -- b -. r c -. r b -. r b ~ ->
    \break
    b4 d -. d4. -> c8 -> ~
    
    c4. d8 -- e -- e -. r e -> ~
    e4 d -. c2 -> 
    r8 b -- \mp \< b [ -- b ] -. r cis4. ->
    r8 d -- c? [ -- d ] -. r e4. ->
    
    \break
    e4. -> \f e8 -- d -- c -. r b -.
    r cis -. r b  -> ~ b2
    r4 r8 b b [ -> d ] -. r e -> ~
    e c4 -> b8 -> r2
    
    \break
    e,2 d
    cis4 b bes r
    r a'2. \mp (
    b2. ) r4
    
    \break
    r4 e, \mp d \< cis
    c2 b
    e4 \f r r2
    R1 ^\markup \jazzglyph #"scripts.ufermatajazz"
  }
}

%}