\version "2.18.2"


% Descripción de funcionalidades
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% PLUGINS
% - \eolMark:					mark al final de línea
% - \bendBefore: 				solución al bendBefore (i.e. dis8 \bendBefore)
% - \rit: 						crea una porción de ritardando que debe terminar como las dinámicas, con un \!
% - \improOn, \improOff:	negra de improvisación
% - \emptyStaff:				Imprime un pentagrama vacío
% - \emptyBar:					Imprime un compás invisible (útil para indentados)



% LAYOUT
% - El aspecto de los silencios multicompás es como los de las partituras de jazz
% - Margen extra de las articulaciones para evitar la colisión con las ligaduras
% - Glissandos bonitos



% PAPER
% - Se imprime el título, instrumento y número de página en todas las páginas
% - Solo se debe tocar el page-count (si eso). Sale bonito tal cual.

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Descripción de funcionalidades
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% 03-09-14: Layout
% Ahora las articulaciones van por dentro de las ligaduras. Parece que es la mejor solución



% 04-09-14: Layout
% Los creciendos y decreciendos tienen la línea un poco más gruesa
%
% Markup
% Añadidas guías para correcta posición y markup de la indicación de tempo
%
% Plugins
% Añadida función \emptyStave para imprimir un pentagrama vacío.
% En algunas partituras hay un pentagrama vacío antes del coda (lo cual mola bastante, por cierto)



% 05-09-14: Plugins
% Añadido a la función \emptyStave el esconder la armadura de la clave, que no estaba.



% 08-09-14: Layout
% Ahora no se muestra la cancelación de accidentales al haber un cambio de armadura



% 09-09-14: Chords
% Añadida sentencia para que los acordes menores salgan con un guión en vez de con una M minúscula



% 10-09-14: Plugins
% Mejorada la función \bendAfter.
% Ahora es genérica, y se puede definir cualquier texto en un lateral que haga las funciones de ornamento



% 15-09-14: Layout
% Añadido un poco más de margen a los reguladores



% 16-09-14: Plugins
% Mejorada la función para imprimir barras de improvisación.
% Antes se llamaba al comando \rs para pintar una barra que ocupaba una negra
% Se reemplaza \rs por \restImproOn y \restImproOff, siendo esta opción mucho más flexible.
% Cuando se activa \restImproOn, todo silencio se convierte en barra de improvisación, respetando su duración.



% 20-09-14: Plugins
% Mejorada la función para imprimir barras de improvisación.
% Se reemplaza \restImproOn y \restImproOff por \improOn y \improOff
% Ahora, también las notas del pentagrama salen como barra de improvisación.
% (los comandos \improvisationOn e \improvisationOff de lilypond no se llevan bien con LilyJAZZ)
% Se ha añadido un grob correspondiente a los slash de la fuente LilyJAZZ en el archivo lily-jazz.ly



% 20-09-14: Layout
% Comentada la línea que oculta la cancelación de accidentales al haber cambio de armadura.
% Realmente, la cancelación es muy bienvenida.



% 01-10-14: Plugins
% Renombrada la función \emptyStave a \emptyStaff (stave es el plural de staff) y 
% corregida (ya no muestra el número de compás).
% Recordar que después de usar \emptyStaff se necesita redefinir el 
% número de compás con \set Score.currentBarNumber = #xx
%
% Añadida la función \emptyBar que oculta un compás entero. Se puede usar para crear un 
% efecto de indentación en un pentagrama dado.
% Recordar que después de usar \emptyBar se necesita:
% - redefinir el número de compás con \set Score.currentBarNumber = #xx
% - volver a pintar la clave con \clef
% - volver a pintar la armadura con \key



% 15-10-14: Language
% Cambiado el lenguaje de entrada de notas del finlandes al inglés
% La diferencia es que ahora los sostenidos se escriben "-s" en vez de "-is"
% y los bemoles "-f" en vez de "-es".
%
% Estructura
% Para poder autoincluir los archivos de instrumento en otros 
% (para montar un midi, o un score), se han movido los plugins a un archivo separado
% y ahora se utiliza la función \includeOnce, evitando así el problema de las
% dependencias circulares.
%
% Global
% Todas las instrucciones que se colocaban justo antes de empezar la música están ahora
% recogidas en una función \global.
% A \global se le ha añadido instrucciones para que agrupe las corcheas de dos en dos
% en vez de cuatro en cuatro (el comportamiento por defecto).
%
% Plugins
% Se le ha aumentado el grosor de los bordes de la función \boxed



% 26-10-14: Chords
% Añadido el include "chord-exception.ily" que recoge una mejor notación de los acordes.
% Actualmente, recoge los acordes maj7, 7(b5), 7(#5), 7(b9), 7(#9), 7(add13), maj9(add13), 9(#11)
% Se irá añadiendo acordes a menudo.
 

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Dependencias
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% \includeOnce
% Incluye una dependencia solo una vez. Así se pueden incluir los archivos en otros 
% (para montar un score por ejemplo) sin liarla parda con dependencias cíclicas.

includeOnce = #(
	define-void-function (parser location filename) (string?) (
		if (not (defined? (string->symbol filename))) (
			begin (
				ly:parser-include-string parser (
					string-concatenate (list "\\include \"" filename "\"")
				)
			)
			(primitive-eval (list 'define (string->symbol filename) #t))
		)
	)
)



% LilyJAZZ
\includeOnce "includes/ignatzek-jazz-chords.ily"
\includeOnce "includes/lily-jazz.ily"



% Language
\language "english"



% Plugins
\includeOnce "includes/plugins-1.2.ily"



% Chord Exceptions
\includeOnce "includes/chord-exceptions.ily"

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~











% Disposición
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Cabecera
\header {
	title = "Pick up the pieces"
	instrument = "Violin"
	composer = ""
	arranger = ""
	poet = ""
	tagline = ""
}



% Márgenes, fuentes y distancias del papel
\paper {
	% El definir otro tipo de fuente no permite cambiar el tamaño global del staff. Es un bug conocigo
	#(define fonts (make-pango-font-tree "LilyJAZZText" "Nimbus Sans" "Luxi Mono" (/ 20 20)))

	% distancia entre la parte superior de la página y el título
	top-markup-spacing #'basic-distance = 5

	% distancia entre el título y el primer sistema
	markup-system-spacing #'basic-distance = 25

	% distancia entre el texto y el primer sistema 
	top-system-spacing #'basic-distance = 15

	% márgen inferior
	bottom-margin = 20
	
	% intentado del primer sistema
	indent = 0
	 
	% número de páginas
	% page-count = 5

	ragged-last-bottom = ##f 

	oddHeaderMarkup = \markup {
	 \on-the-fly #not-first-page \fill-line {
		\fromproperty #'page:page-number-string
		\fromproperty #'header:title
		\fromproperty #'header:instrument
	 }
	}

	evenHeaderMarkup = \markup {
	 \on-the-fly #not-first-page \fill-line {
		\fromproperty #'header:instrument
		\fromproperty #'header:title
		\fromproperty #'page:page-number-string
	 }
	}
}

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~







% Elementos comunes de la partitura
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
	% Oculta el primer pentagrama del markup
	\override Staff.VerticalAxisGroup #'remove-first = ##t

	% Indicación de tiempo
	% \once \override Score.RehearsalMark.X-offset = #5
	% \mark \jazzTempoMarkup "Easy Swing" c4 "112"

	s1*16
	\mark \markup \boxed "A"
	
	s1*32
	\mark \markup {
		\column { \boxed "B" }
		\hspace #2
		\column { \vspace #-.3 \normalsize \jazzglyph #"scripts.segnojazz" }
	}
	s1*16
	
	\bar ".|:-||"
	\eolMark \markup \jazzglyph #"scripts.varcodajazz"
	\mark \markup "Repeat 3 x's"
	s1*12
	
	\bar "||"
	\mark \markup \boxed "C"
	s1*16
	
	\bar "||"
	\mark \markup \boxed "C50"
	s1*16
	
	\bar "||"
	\mark \markup \boxed "C58"
	s1*16
	
	\bar "||"
	\mark \markup \boxed "C66"
	s1*16
	
	\bar ".|:-||"
	\once \override Score.RehearsalMark.X-offset = #5
	\mark \markup {
		\column { \boxed "C74" }
		\column { "Repeat 3 x's" }
	}
	s1*8
	
	\bar ".|:-||"
	\once \override Score.RehearsalMark.X-offset = #5
	\mark \markup {
		\column { \boxed "C82" }
		\column { "Repeat 3 x's" }
	}
	s1*16
	
	\eolMark \markup "D.S. to Coda"
	\mark \markup {
		\column { \vspace #-.3 \jazzglyph #"scripts.codajazz" }
		\hspace #1
		\column { "Coda" }
	}
	s1*24
	
	\bar "||"
	\mark \markup \boxed "D"
	s1*16
	
	\bar "||"
	\mark \markup \boxed "D126"
	s1*16
	
	\bar "||"
	\mark \markup \boxed "D134"
	s1*16
	
	\bar "||"
	\mark \markup \boxed "D142"
	s1*16
	
	\bar "||"
	\mark \markup \boxed "D150"
	s1*16
	
	\bar "||"
	\mark \markup \boxed "D158"
	s1*16
	
	\bar "||"
	s1*16
	
	\bar "||"
	s1*16
	
	\bar ".|:-||"
	\mark \markup \boxed "D182"
	s1*12
	
	\mark \markup \boxed "D188"
	s1*8
	
	\bar "||"
	\mark \markup \boxed "E"
	s1*16
	
	\bar "||"
	\mark \markup \boxed "E200"
	s1*16
	
	\bar "||"
	\mark \markup \boxed "E208"
	s1*16
	
	\bar ".|:-||"
	\mark \markup \boxed "Repeat 4 x's"
	s1*4
	
	\mark \markup \boxed "E227"
	s1*4*3/4
	s1*8
	
	\mark \markup \boxed "F"
	s1*32
	
	\mark \markup \boxed "F264"
	s1*8
	
	\mark \markup \boxed "G"
	s1*32
	
	\mark \markup \boxed "G296"
	s1*16
	
	\bar "||"
	\mark \markup \boxed "H"
	s1*24
	
	\bar "|."
}



% Función musical global
global = {
	% Para pintar todos los números de compás debajo del pentagrama
	% \override Score.BarNumber #'break-visibility = #end-of-line-invisible
	% \override Score.BarNumber #'direction = #DOWN
	% \override Score.BarNumber #'extra-offset = #'(1.5 . 0)
	\override Score.BarNumber.break-visibility = #all-invisible
	\set Score.explicitKeySignatureVisibility = #begin-of-line-visible

	\jazzOn
	\compressFullBarRests

	% Arregla el problema de tener el scoreMarkup añadido
	\revert MultiMeasureRest #'staff-position
	\override Voice.Rest #'staff-position = #0	

	\set Timing.beamExceptions = #'()
	\set Timing.baseMoment = #(ly:make-moment 1/4)
	\set Timing.beatStructure = #'(1 1 1 1) 
}

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Elementos específicos del instrumento
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

% Cifrado
scoreChords = \chords {    
	\set minorChordModifier = \markup { "-" }
	\set chordNameExceptions = #chordNameExceptions
	\override ChordName.font-size = #2.5

	% c1:m f2:m g2:m7
}

% Música
violin = {
  
	\global

	% 00: saxs
	\transpose c ef, \relative c' {
		
		\key f \major
		
		R1*16
		
		\repeat volta 2 {
			f8 \mf a e' d r4 c8 g
			b a r4 g'8 c, d f ~
			f4 d -. c8 d r f ~
			\break
			f4 d -. c8 d r4
			
			R1*4
		}
		
		f8 \mf a, e' d r4 f4 -.
		g8 f r4 g8 c, d f ~
		\break
		f4 d -. c8 d r f ~
		f4 d -. c8 d r4
		
		R1*8
		
		\repeat volta 2 {
			g8 f g f d4 -. g8 f
			\break
			g f c4 -. r2
		}
		
		g'8 f g f d4 -. g8 f
		g f d4 -.  r2
		g8 f g f d4 -. g8 f
		\break
		g f c4 -.  f2 ~ -> \f \>
		f1 ~ \<
		f1 \! \f
		
		R1*4
	}
	
	% B0: Tp1, saxs
	\transpose c bf, \relative c'' {
		
		\key bf \major
		
		\break
		\repeat volta 2 {
			bf8 \f d a' g r4 f8 c
			e d r4 c'8 f, g bf ~
			bf4 g -. f8 g r bf ~
			bf4 g -. f8 g r4
			
			\break
			R1*4
		}
		
		bf8 \mf d, a' g r4 bf4 -.
		c8 bf r4 c8 f, g bf ~
		\break
		bf4 g -. f8 g r bf ~
		bf4 g -. f8 g r4
		
		R1*4
		
		\repeat volta 2 {
			\break
			c8 bf c bf g4 -. c8 bf
			c bf f4 -. r2
		}
		
		c'8 bf c bf g4 -. c8 bf
		c bf d4 -.  r2
		\break
		c8 bf c bf g4 -. c8 bf
		c bf f4 -.  d'2 -> \ff \bendAfter #5
		
		R1*5
		
		r4 bf2 -- \f bf4 -.
		
		c2 \bendAfter #5 r2
		\break
		R1*15
	}
	
	% C0: Alto 1, saxs
	\transpose c ef \relative c'' {
		
		\key f \major
		
		d8 d r4 r2
		R1*15
		
		R1*16
		
		R1*16
		
		\break
		\repeat volta 2 {
			a8 a r4 r4 a8 a
			R1
			a8 a r4 r4 a8 a
			r8 a4 f8 a4 -- b -.
		}
		
		\break
		a8 a r4 r4 a8 a
		R1
		a8 a r4 r4 a8 a
		r8 a4 f8 a4 -- b8 a
		
		\break
		\repeat volta 2 {
			R1*16
		}
	}
	
	% Cd: Tp1, Tp4, saxs
	\transpose c bf \relative c'' {
		
		\key bf \major
		
		
		\break
		c8 bf c bf g4 -. c8 bf
		c bf f4 -. r2
		c'8 bf c bf g4 -. c8 bf
		c bf f4 -. r2
		
		\break
		c'8 bf c bf g4 -. c8 bf
		c bf f4 -. r2
		c'8 bf c bf g4 -. c8 bf
		c bf f4 -. r2
		
		\break
		c'8 bf c bf g4 -. c8 bf
		c bf f4 -. r2
		c'8 bf c bf g4 -. c8 bf
		c bf f4 -. r2
		
		\break
		c'8 bf c bf g4 -. c8 bf
		c bf d4 -. r2
		c8 bf c bf g4 -. c8 bf
		
		\break
		c bf f4 -. d'2 ~ -> 
		d1 ~
		d ~
		d ~
		d ~
		d2 r2
		
		\break
		R1*3
	}
	
	% D0: saxs
	\transpose c ef, \relative c'' {
		
		\key f \major
		
		R1*16
		
		R1*16
		
		R1*16
		
		\break
		R1*16
		
		f8 f r4 r4 f8 f
		R1
		f8 f r4 r4 f8 f
		r8 f4 d8 f4 -- g -.
		
		\break
		f8 f r4 r4 f8 f
		R1
		f8 f r4 r4 f8 f
		r8 f4 d8 f4 -- g -.
		
		f8 f r4 r4 f8 f
		\break
		R1
		f8 f r4 r4 f8 f
		r8 f4 d8 f4 -- g -.
		
		f8 f r4 r4 f8 f
		R1
		\break
		f8 f r4 r4 f8 f
		r8 f4 d8 f4 -- g8 f
		
		R1*16
		
		R1*16
		
		R1*15
		
		\break
		r4 d' -. r8 c4 -. r8
	}
	
	% D182: Tp1, Tp4, saxs
	\transpose c bf, \relative c''' {
		
		\key bf \major
		\repeat volta 2 {
			
			bf8 d, a' g r4 bf -.
			c8 bf r4 c8 f, g bf ~
			\break
			bf4 g -. f8 g r8 bf ~
			bf4 g -. f8 g r4
			
			bf8 d, a' g r4 bf -.
			\break
			c8 bf r4 c8 f, g bf ~
			bf4 g -. f8 g r8 bf ~
			bf4 g -. f8 g r4
			\break
			
			d1 ~ 
			d \bendAfter #5
			R1*2
		}
		
		R1*8
	}
	
	% E0: saxs
	\transpose c ef, \relative c' {
		
		\key f \major
		
		\break
		
		d'8 a ef' f c a f a
		R1
		d8 bf f' e d c a f ~
		f4 f' g8 f4 e8 ~
		
		e4 d4 g8 f4 e8 ~
		\break
		e4 f r8 d g, c
		a d g gs ~ gs2 \glissando
		f2. r4
		
		f8 c d c a c r4
		\break
		r8 a c d f d f g ~
		g4 a8 f ~ f e r4
		r8 f, bf b e f b g ~
		
		g2. cs,4 ~ ->
		cs2 a ~ ->
		\break
		a4 g8 a r2
		g8 a r4 r2
		
		d'2 -- c8 d r4
	}
	
	% E201: Tp1, Tp4, saxs
	\transpose c bf \relative c' {
		
		\key bf \major
		
		\break
		r2 r8 d e4 -.
		f -. g -. bf -. c -.
		d -. bf -. g8 f bf g ~
		
		\break
		g2 f8 g r4
		R1
		r8 d f g a bf c cs
		d c? d c bf g bf g ~
		
		\break
		g2 r
		r8 d e f g a c a ~
		a2 r8 f g bf
		c bf c bf df bf g bf ~
		
		\break
		bf2 g8 f r4
		c'8 bf g bf r2
		r4 d -. d8 d r4
		d4 -. d8 d r2
		
		\break
		f2 \bendAfter #-5 r2
		bf,8 d, a' g ~ g4 f8 c
		e c r4 r2
		df'8 bf df bf ~ bf gs a fs
		
		\break
		g? e fs ds e cs d b
		c a bf?4 r2
		r4 d' c8 a4 d8 ~
		d4 r4 r2
		
		r2 f,8 g bf c ~
		\break
		c4 bf8 g bf c r4
		r2 f,8 g bf c ~
		c4 bf8 g bf c r4
		
		\break
		r4 d4 g2 ~
		g1 ~
		g1 ~
		g2. r4
		
		\break
		\repeat volta 2 {
			ef,8 g d' c r4 b8 fs
			as gs r4 e'8 a, b d ~
			d4 b a8 b r8 d ~
			\break
			d4 b a8 b r4
		}
		
		\repeat volta 2 {
			
			\time 3/4
			
			e2.
			e2.
			e2.
			e2.
		}
		
		\time 4/4
		\break
		f1 ~
		f ~
		f2 r2
		R1*5
		
		\break
		\repeat volta 2 {
			R1*16
		}
		
		\repeat volta 2 {
			R1*16
		}
	}
	
	% F264: Tp1, Tp2, saxs
	\transpose c bf, \relative c'' {
		
		\key bf \major
		
		\break
		\repeat volta 2 {
			d8 d r4 r4 d8 d
			R1
			d8 d r4 r4 d8 d
			r8 d4 bf8 d4 -- e -.
		}
		
		\break
		d8 d r4 r4 d8 d
		R1
		d8 d r4 r4 d8 d
		r8 d4 bf8 d4 -- e8 d
		
		\break
		\repeat volta 2 {
			R1*16
		}
		
		R1*16
		
		\break
		\repeat volta 2 {
			bf'8 d, a' g r4 bf4 -.
			c8 bf r4 c8 f, g bf ~
			bf4 g -. f8 g r bf ~
			bf4 g f8 g r4
		}
		
		\break
		R1*4
		
		R1*8
	}
	
	% H0: Tp1, saxs
	\transpose c bf, \relative c'' {
		
		\key bf \major
		
		bf8 \f d a' g r4 f8 c
		e d r4 c'8 f, g bf ~
		\break
		bf4 g -. f8 g r bf ~
		bf4 g -. f8 -- g -. r4
		
		R1*16
		
		bf,8 \f d a' g r4 f8 c
		\break
		e d r4 c'8 f, g bf ~
		bf4 g -. f8 g r bf ~
		bf8 r8 r4 f'8 -- g -. r4
	}
}

% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~








% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~

\book {
	\score {
		<<
			\scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\new Staff { \violin }
			>>
		>>
	}
}


