\version "2.18.2"

\include "lily-jazz/ignatzek-jazz-chords.ily"
\include "lily-jazz/lily-jazz.ily"





% PLUGINS 1.2
% -----------
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~





% multi-mark-engraver
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
% Aporta la posibilidad de pintar un rehearsal mark al final de una línea y 
% al comienzo de la siguiente.
% http://lists.gnu.org/archive/html/lilypond-user/2011-08/msg00157.html
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
#(define (multi-mark-engraver ctx)
   (let ((texts '())
         (final-texts '())
         (events '()))

     `((start-translation-timestep
        . ,(lambda (trans)
             (set! final-texts '())))

       (listeners
        (mark-event
         . ,(lambda (trans ev)
              (set! events (cons ev events)))))

       (acknowledgers
        (break-alignment-interface
         . ,(lambda (trans grob source)
              (for-each (lambda (mark)
                          (set! (ly:grob-parent mark X) grob))
                        texts))))

       (process-music
        . ,(lambda (trans)
             (for-each
              (lambda (ev)
                (let* ((mark-grob
                        (ly:engraver-make-grob trans 'RehearsalMark ev))
                       (label (ly:event-property ev 'label))
                       (formatter (ly:context-property ctx 'markFormatter)))

                  (if (and (procedure? formatter)
                           (not (markup? label)))
                      (begin
                       (if (not (number? label))
                           (set! label
                                 (ly:context-property ctx 'rehearsalMark)))

                       (if (and (integer? label)
                                (exact? label))
                           (set! (ly:context-property ctx 'rehearsalMark)
                                 (1+ label)))

                       (if (number? label)
                           (set! label (apply formatter (list label ctx)))
                           (ly:warning "rehearsalMark must have
integer value"))))

                  (if (markup? label)
                      (begin
                       (set! (ly:grob-property mark-grob 'text) label)
                       (let ((dir (ly:event-property ev 'direction)))
                         (and (ly:dir? dir)
                              (set! (ly:grob-property mark-grob 'direction)
                                    dir))))
                      (ly:warning "mark label must be a markup object"))

                  (set! texts (cons mark-grob texts))))
              (reverse events))))

       (stop-translation-timestep
        . ,(lambda (trans)
             (if (pair? texts)
                 (let ((staves (ly:context-property ctx 'stavesFound))
                       (priority-index 0))
                   (for-each (lambda (grob)
                               (let ((my-priority (ly:grob-property
grob 'outside-staff-priority 1500)))
                                 (for-each (lambda (stave)

(ly:pointer-group-interface::add-grob grob 'side-support-elements
                                               stave))
                                           staves)
                                 (set! (ly:grob-property grob
'outside-staff-priority) (+ my-priority priority-index))
                                 (set! priority-index (1+ priority-index))
                                 (set! final-texts (cons grob final-texts))))
                             (reverse texts))
                     (set! texts '())
                     (set! events '())))))

        (finalize
         . ,(lambda (trans)
              (and (pair? final-texts)
                   (for-each (lambda (grob)
                               (set! (ly:grob-property grob 'break-visibility)
                                     end-of-line-visible))
                             final-texts)))))))


% Funciones originales
%{
myMark =
#(define-music-function (parser location text) (markup?)
  (make-music 'MarkEvent
              'label text))
			  
toCoda = {
	\tweak #'self-alignment-X #RIGHT
	\tweak #'break-visibility #begin-of-line-invisible
	\myMark \markup { \hspace #1.25 \raise #1.25 \musicglyph #"scripts.coda" }
}
%}

% Función maqueada.
% Se apoya en el código obtenido por consola al ejecutar { \displayMusic { \toCoda }}
eolMark =
#(define-music-function (parser location text) (markup?)
	(make-music 
		'MarkEvent 
		'tweaks (list 
			(cons (quote self-alignment-X) 1)
			(cons (quote break-visibility) #(#t #t #f))
		) 
		'label (markup
			#:line (
				#:hspace 1.25
				#:raise 1.25
				text
			)
		)
	)
)










% rest-score y merge-rests-on-positioning
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
% Permite realizar la fusión de los silencios entre dos voces en un mismo pentagrama.
% https://code.google.com/p/lilypond/issues/detail?id=1228 (fichero merge-rests.ily)
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]

#(define (rest-score r)
  (let ((score 0)
	(yoff (ly:grob-property-data r 'Y-offset))
	(sp (ly:grob-property-data r 'staff-position)))
    (if (number? yoff)
	(set! score (+ score 2))
	(if (eq? yoff 'calculation-in-progress)
	    (set! score (- score 3))))
    (and (number? sp)
	 (<= 0 2 sp)
	 (set! score (+ score 2))
	 (set! score (- score (abs (- 1 sp)))))
    score))

#(define (merge-rests-on-positioning grob)
  (let* ((can-merge #f)
	 (elts (ly:grob-object grob 'elements))
	 (num-elts (and (ly:grob-array? elts)
			(ly:grob-array-length elts)))
	 (two-voice? (= num-elts 2)))
    (if two-voice?
	(let* ((v1-grob (ly:grob-array-ref elts 0))
	       (v2-grob (ly:grob-array-ref elts 1))
	       (v1-rest (ly:grob-object v1-grob 'rest))
	       (v2-rest (ly:grob-object v2-grob 'rest)))
	  (and
	   (ly:grob? v1-rest)
	   (ly:grob? v2-rest)
	   (let* ((v1-duration-log (ly:grob-property v1-rest 'duration-log))
		  (v2-duration-log (ly:grob-property v2-rest 'duration-log))
		  (v1-dot (ly:grob-object v1-rest 'dot))
		  (v2-dot (ly:grob-object v2-rest 'dot))
		  (v1-dot-count (and (ly:grob? v1-dot)
				     (ly:grob-property v1-dot 'dot-count -1)))
		  (v2-dot-count (and (ly:grob? v2-dot)
				     (ly:grob-property v2-dot 'dot-count -1))))
	     (set! can-merge
		   (and
		    (number? v1-duration-log)
		    (number? v2-duration-log)
		    (= v1-duration-log v2-duration-log)
		    (eq? v1-dot-count v2-dot-count)))
	     (if can-merge
		 ;; keep the rest that looks best:
		 (let* ((keep-v1? (>= (rest-score v1-rest)
				      (rest-score v2-rest)))
			(rest-to-keep (if keep-v1? v1-rest v2-rest))
			(dot-to-kill (if keep-v1? v2-dot v1-dot)))
		   ;; uncomment if you're curious of which rest was chosen:
		   ;;(ly:grob-set-property! v1-rest 'color green)
		   ;;(ly:grob-set-property! v2-rest 'color blue)
		   (ly:grob-suicide! (if keep-v1? v2-rest v1-rest))
		   (if (ly:grob? dot-to-kill)
		       (ly:grob-suicide! dot-to-kill))
		   (ly:grob-set-property! rest-to-keep 'direction 0)
		   (ly:rest::y-offset-callback rest-to-keep)))))))
    (if can-merge
	#t
	(ly:rest-collision::calc-positioning-done grob))))










% startParenthesis y endParenthesis
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
% http://comments.gmane.org/gmane.comp.gnu.lilypond.general/64087
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]

% ELIMINADO










% bendBefore
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
% Adaptación
% http://lilypondblog.org/2013/08/adding-ornamentations-to-note-heads-part-1/
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]

%%%
%%% Utilities for defining new grobs, grob properties and music event types
%%% (there should be built-in commands to do that in LilyPond)
%%%
#(define (define-grob-type grob-name grob-entry)
   (let* ((meta-entry   (assoc-get 'meta grob-entry))
          (class        (assoc-get 'class meta-entry))
          (ifaces-entry (assoc-get 'interfaces meta-entry)))
     (set-object-property! grob-name 'translation-type? list?)
     (set-object-property! grob-name 'is-grob? #t)
     (set! ifaces-entry (append (case class
                                  ((Item) '(item-interface))
                                  ((Spanner) '(spanner-interface))
                                  ((Paper_column) '((item-interface
                                                     paper-column-interface)))
                                  ((System) '((system-interface
                                               spanner-interface)))
                                  (else '(unknown-interface)))
                                ifaces-entry))
     (set! ifaces-entry (uniq-list (sort ifaces-entry symbol<?)))
     (set! ifaces-entry (cons 'grob-interface ifaces-entry))
     (set! meta-entry (assoc-set! meta-entry 'name grob-name))
     (set! meta-entry (assoc-set! meta-entry 'interfaces
                                  ifaces-entry))
     (set! grob-entry (assoc-set! grob-entry 'meta meta-entry))
     (set! all-grob-descriptions
           (cons (cons grob-name grob-entry)
                 all-grob-descriptions))))

#(define-public (define-grob-property symbol type? description)
  (set-object-property! symbol 'backend-type? type?)
  (set-object-property! symbol 'backend-doc description)
  symbol)

#(defmacro*-public define-music-type (type-name parents #:rest properties)
   (let ((gproperties (gensym "properties")))
     `(let ((,gproperties (list-copy ',properties)))
        ,@(map (lambda (parent)
                `(define-event-class ',(ly:camel-case->lisp-identifier type-name)
                   ',parent))
               parents)
        (set-object-property! ',type-name
                              'music-description
                              (cdr (assq 'description ,gproperties)))
        (set! ,gproperties (assoc-set! ,gproperties 'name ',type-name))
        (set! ,gproperties (assq-remove! ,gproperties 'description))
        (hashq-set! music-name-to-property-table ',type-name ,gproperties)
        (set! music-descriptions
              (cons (cons ',type-name ,gproperties)
                    music-descriptions)))))

%%%
%%% Grob definition
%%%
#(define (head-ornamentation::print me)
   (let* ((notes (ly:grob-object me 'elements))
          (y-ref (ly:grob-common-refpoint-of-array me notes Y))
          (x-ref (ly:grob-common-refpoint-of-array me notes X))
          (x-ext (ly:relative-group-extent notes x-ref X))
          (y-ext (ly:relative-group-extent notes y-ref Y))
          (y-coord (interval-center y-ext))
          (text (ly:text-interface::print me))
          (width (/ (interval-length (ly:stencil-extent text X)) 2.0))
          (x-coord (- (car x-ext) width)))
     (ly:stencil-translate
      text
      (cons
       (- (- x-coord (ly:grob-relative-coordinate me x-ref X)) .7)
       (- (- y-coord (ly:grob-relative-coordinate me y-ref Y)) 1.5)))))

#(define-grob-type 'HeadOrnamentation
  `((text . ,#{ \markup \fontsize #-4 \musicglyph #"brackettips.up" #})
    (stencil . ,head-ornamentation::print)
    (meta . ((class . Item)
             (interfaces . (font-interface))))))

\layout {
  \context {
    \Global
    \grobdescriptions #all-grob-descriptions
  }
}

%%%
%%% Engraver
%%%
%%% The head-ornamentation engraver, with its note-head acknowledger
%%% (which add HeadOrnamentation grobs to note heads)
#(define head-ornamentation-engraver
   (make-engraver
    (acknowledgers
     ((note-head-interface engraver note-grob source)
      ;; When the note-head event attached to the note-head grob has
      ;; ornamentation events among its articulations, then create a
      ;; HeadOrnamentation grob
      (let* ((note-event (ly:grob-property note-grob 'cause)))
        (for-each
         (lambda (articulation)
           (if (memq 'head-ornamentation-event
                     (ly:event-property articulation 'class))
               (let ((ornament-grob (ly:engraver-make-grob engraver
                                                           'HeadOrnamentation
                                                           note-grob)))
                 (ly:pointer-group-interface::add-grob ornament-grob
                                                       'elements
                                                       note-grob)
                 (set! (ly:grob-parent ornament-grob Y) note-grob)
                 (set! (ly:grob-property ornament-grob 'font-size)
                       (+ (ly:grob-property ornament-grob 'font-size 0.0)
                          (ly:grob-property note-grob 'font-size 0.0))))))
         (ly:event-property note-event 'articulations)))))))

\layout {
  \context {
    \Voice
    \consists #head-ornamentation-engraver
  }
}

%%%
%%% Event type definition
%%%
#(define-music-type HeadOrnamentationEvent (music-event)
   (description . "Print an ornamentation at a note head side")
   (types . (general-music post-event event head-ornamentation-event)))

bendBefore = #(make-music 'HeadOrnamentationEvent)










% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
% Funciones propias
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]





% Índice con borde circular y un cómodo espacio interior (y no el que viene por defecto)
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
#(define-markup-command (boxindex layout props text) (markup?)
	(interpret-markup layout props
		(markup #:huge #:override '(box-padding . 0.5) #:box text)
	)
)

#(define-markup-command (boxed layout props text) (markup?)
	(interpret-markup layout props
		(markup #:huge 
		  #:override '(box-padding . 0.75) 
		  #:override '(thickness . 2.5) 
		  #:box text)
	)
)

% Ritardando
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
rit =
#(make-music 'CrescendoEvent
             'span-direction START
             'span-type 'text
             'span-text "rit.")

% Negra oblicua
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
rs = {
  \once \override Rest #'stencil = #ly:percent-repeat-item-interface::beat-slash
  \once \override Rest #'thickness = #0.48
  \once \override Rest #'slope = #1.7
  r4
}


% Pentagrama vacío
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
emptyStave = {
  \once \override Staff.Clef.stencil = ##f
  \break
  s1
  \once \override Staff.BarLine.stencil = ##f
  \break
}

sffzp = #(make-dynamic-script "sffzp")










% Añadido de funciones al contexto
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
\layout {
  \context {
    \Score		
    
    % multi-mark-engraver
    \remove "Mark_engraver"
    \consists #multi-mark-engraver
    \consists "Tweak_engraver"
    
    % merge-rests-on-positioning
    \override RestCollision #'positioning-done = #merge-rests-on-positioning
    
    % aspecto de los silencios multicompás
    \override MultiMeasureRest.expand-limit = #1
    
    % margen extra de las articulaciones para evitar la colisión con las ligaduras
    \override Script.padding = #0.5
    \override Script.avoid-slur = #'inside
    
    % glissando bonito
    \override Glissando.style = #'trill
    \override Glissando.bound-details.left.padding = #1.7
    
    % dinámicas un poco mas gruesas
    \override Hairpin.thickness = #1.75
    
    % Esconde la cancelacion de accidentales en el cambio de armadura
    \override KeyCancellation.break-visibility = #'#(#f #f #f)
  }
}












% LAYOUT
% ------
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~






\header {
  title = "Caravan"
  instrument = "Baritone Sax"
  composer = "Duke ELLINGTON, Irving MILLS & Juan TIZOL"
  arranger = "Arr: Dave WOLPE"
  poet = ""
  tagline = ""
}

\paper {
  % El definir otro tipo de fuente no permite cambiar el tamaño global del staff. Es un bug conocigo
  #(define fonts (make-pango-font-tree "LilyJAZZText" "Nimbus Sans" "Luxi Mono" (/ 20 20)))
  
  % distancia entre la parte superior de la página y el título
  top-markup-spacing #'basic-distance = 5
  
  % distancia entre el título y el primer sistema
  markup-system-spacing #'basic-distance = 25
  
  % distancia entre el texto y el primer sistema 
  top-system-spacing #'basic-distance = 15
  
  % márgen inferior
  bottom-margin = 20
    
  page-count = 2
  
  ragged-last-bottom = ##f 
  
  oddHeaderMarkup = \markup {
    \on-the-fly #not-first-page \fill-line {
      \fromproperty #'page:page-number-string
      \fromproperty #'header:title
      \fromproperty #'header:instrument
    }
  }
  
  evenHeaderMarkup = \markup {
    \on-the-fly #not-first-page \fill-line {
      \fromproperty #'header:instrument
      \fromproperty #'header:title
      \fromproperty #'page:page-number-string
    }
  }
}










% MUSIC PARTS
% -----------
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~




% scoreMarkup
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
scoreMarkup = \new Staff \with {
   \RemoveEmptyStaves
}
{
  % Oculta el primer pentagrama del markup
  \override Staff.VerticalAxisGroup #'remove-first = ##t
  
  \once \override Score.RehearsalMark.X-offset = #5
  \mark \jazzTempoMarkup "Bright Meringue" c2 "120"
  
  s1*16
  
  \once \override Score.RehearsalMark.X-offset = #5
  \mark \markup {
    \column { \boxed "A" }
    \hspace #2
    \column { \normalsize "(2nd X Only)" }
  }
  s1*20
  
  \mark \markup \boxed "B"
  s1*16
  s32
  
  \bar "||"
  \mark \markup \boxed "C"
  s1*16
  
  \bar "||"
  \once \override Score.RehearsalMark.self-alignment-X = #LEFT
  \mark \markup {
    \column { \boxed "D" }
    \column { \normalsize "(Swing)" }
  }
  s1*16
  
  \bar "||"
  \once \override Score.RehearsalMark.X-offset = #5
  \mark \markup {
    \column { \boxed "E" }
    \column { \normalsize "(Latin)" }
  }
  s1*16
  s32
  
  \bar "||"
  \mark \markup \boxed "F"
  s1*14
  
  \bar "|."
  
    
  %{ 
  \once \override Score.RehearsalMark.self-alignment-X = #RIGHT
  \mark \markup {
    \column { \vspace #0.3 "To Coda" }
    \hspace #1.5
    \column { \jazzglyph #'"scripts.varcodajazz" }
  } %}
}





% scoreChords
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
scoreChords = \chords {
  
}





% music
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
baritoneSax = \relative c' {

  % Para pintar todos los números de compás debajo del pentagrama
  % \override Score.BarNumber #'break-visibility = #end-of-line-invisible
  % \override Score.BarNumber #'direction = #DOWN
  % \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0
  \set Staff.explicitKeySignatureVisibility = #begin-of-line-visible
    
  % saxo
  \relative c' {
    
    \key d \minor
    
    R1*4
    
  \set countPercentRepeats = ##t
  \set repeatCountVisibility = #(every-nth-repeat-count-visible 4)
    <<
      \repeat percent 6 { a2. -> \f e'4 -. }
      {s1*4 s1 \< s1}
    >>
    
    \break
    a,1 \! -> ~
    a
    
    r4 r8 a' ( b4 -. ) a -.
    b8 a4 -. b8 -> ~ b2 ~
    b r
    R1
  }
    
  \bar ".|:-||"
  \repeat volta 2 {
    
    \transpose ees c \relative c'' {
      
      \key f \minor
    
      \break
      r4 c2. -> \fp \< ~
      c1 ~
      c8 \! r r2 r4
      R1 
      
      \break
      R1*4
      
      c,1 ->
      c1 ->
      c4 -^ r r2
      R1
    }
  }
  
  
  \alternative {
    { 
      \transpose ees c' \relative c' {
        
        \key f \minor
      
        \break
        r4 r8 c ( d4 -. ) c -.
        d8 -- c4 -. d8 -> ~ d2
        f,1 -> ~ 
        f
      }
    }
    
    { 
      \transpose ees c \relative c' {
        
        \key f \minor
      
        \break
        r2 f -> ~
        f1
        f4 -. r8 f -> ~ f
      }
      
      % saxo A19
      \relative c'' {
       
        \key d \minor
        
        a ( b [ cis ]
        e cis d e f fis g gis )
      }
    }
  }
  
  \relative c'' {
    
    \key d \minor
   
    \break
    fis1 (
    c
    d,2 )
  }
      
  % trombone B3
  \transpose ees c \relative c'' {
    
    \key f \minor
    
    ees8 [ -- ees -. ] r ees-.
    r ees4 -. ees8 -- ees4 -. ees4 -.
    \break
    bes1 ~ 
    bes1
    
    r4 f'2 -> \turn d4 -.
    aes'2. \turn aes4 -.
    \break
    \cadenzaOn g2 -> \glissando \hideNotes bes,32 \unHideNotes
  }
    
  % saxo 9'5
  \relative c''' { 
    
    \key d \minor
    
    bes2 ( \cadenzaOff \bar "|"
    e, e
    g,1 \glissando 
    c, )
    
    \pageBreak
    r4 f2 -> f4 -. 
    bes,1 ->
    r2 a'4 ( \< b8 bis
    cis4 ees8 e f4 f8 fis
    
    \break
    g1 -> \! ) ~ 
    g ~
    g4 ( aes g d
    f g b d,
    
    \break
    cis1 ) ~ 
    cis
    r4 g' ( aes g
    aes g fis b,
    
    \break
    cis1 ) ~
    cis2. r4
    r g' ( fis f 
    e ees cis a
    
    \break
    b2. ) ~ b8 a -> ~
    a2. ~ a8 d, -> ~
    d1 \< ~ 
    d
    
    \break
    R1*16 \!
  }
  
  % trombone E
  \transpose ees c \relative c' {
    
    \key f \minor
    
    R1*2
    c1 -> \fp ~
    \override Glissando.style = #'line
    c2 \< \glissando des \> \glissando
    \override Glissando.style = #'trill
    
    c1 \! ~ 
    c1
  }
    
  % saxo E7
  \relative c''' {
    
    \key d \minor
   
    \break
    r4 g ( aes g
    aes g fis b,
    cis1 \< ) ~
    cis ~
    
    \break
    cis8 \! g'4. -> \ff fis4 -. f -.
    e -. ees -. cis8 -- a4 -. d,8 -> ~ 
    \cadenzaOn d1 -> \glissando \hideNotes a32 \unHideNotes \cadenzaOff \bar "|"
    R1*3
    
    \break
    <<
      \repeat percent 6 { a2. -> \f e'4 -. }
      {s1*4 s1 \< s1}
    >>
    
    \break
    \repeat percent 4 { a,2. -> \f e'4 -. }
    
    \break
    a,4 -^ a'8 -> a -> a -> a -> r a ->
    r a4 -> a8 -> a4 -> a ->
    d,4 -^ r r d8 -> d ->
    d4 -> r r2
  }
}





% score
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
\score {
  <<
    \scoreMarkup
    \scoreChords
    \new StaffGroup <<
      \new Staff { \baritoneSax }
    >>
  >>
}

violin = \relative c' {

  % Para pintar todos los números de compás debajo del pentagrama
  % \override Score.BarNumber #'break-visibility = #end-of-line-invisible
  % \override Score.BarNumber #'direction = #DOWN
  % \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0
  \set Staff.explicitKeySignatureVisibility = #begin-of-line-visible
    
  % alto 1
  \transpose c ees, \relative c''' {
    
    \key d \minor
    
    R1*8
    
    a1 -> \fp \< ~ 
    a1 ~ 
    a1 \! ~ 
    a1
    
    \break
    r4 r8 a, ( b4 -. ) a -.
    b8 -- a4 -. b8 -> ~ b2 ~ 
    b r
    R1
    
    \break
    \repeat volta 2 {
      a'1 \f ~ 
      a1 ~ 
      a4 ( bes a e
      g a cis e,
      
      g1 ) ~ 
      g
      \break
      r4 a ( bes a
      bes a gis cis, 
      
      g'?1 ) ~ 
      g
      r4 a ( aes g 
      fis f ees cis
    }
    
    \alternative {
      { \break 
        d1 ) ~ 
        d
        R1*2 }
      
      { \break
        d1 ~ 
        d2 b'
        e4 -^ r8 e -> ~ e2 ~ 
        e2 ~ e8 r r4 }
    }
  }
  
  % trompeta 1 2,5 antes de B
  \transpose c bes, \relative c''' {
    
    \key g \minor
    
    \break
    g1 -> ~ 
    g4 e2 ( -> d4 -.)
    a1 -> ~ 
    a
    
    r2 d ( \glissando
    a' g 
    \break
    d1 )
    r4 a'2 -> \turn g4 -. 
    
    \cadenzaOn c1 -> \glissando \hideNotes e,32 \unHideNotes \cadenzaOff \bar "|"
    R1*2
    r4 f2 -> g4 -.
    
    \pageBreak
    bes1 ->
    r4 bes8 -- c -. ~ c bes -- c4 -.
    d1 -> \sffzp \< ~ 
    d
    
    \break
    d1 \! -> ~ 
    \cadenzaOn d \glissando \hideNotes e,32 \unHideNotes \cadenzaOff \bar "|"
    R1*2
    
    r2 d' -> \bendAfter #-4
    r d -> \bendAfter #-4
    \break
    R1*2
    
    r4 gis,8 -> gis -> gis -> gis -> r gis -> 
    r gis4 ->  gis8 -> gis4 -> gis ->
    b4 -^ r r2
    R1
    
    \break
    R1*3
    d4 -^ \ff c8 ( bes a2 )
    
    \break
    R1*15
    r4 ees'8 \> ( bes g ges f ees
    
    d1 ) \mf ~ 
    d ~ 
    d4 ( ees d a
    \break
    c \< d fis \> a,
    
    c1 \! ) ~ 
    c ~ 
    c8 r r4 r2
    R1*3
    
    \break
    r8 d'4. -> \ff des4 -. c -.
    b -. bes-. aes8 -- fis4 -. g8 -> ~ 
    \cadenzaOn g1 \glissando \hideNotes g,32 \unHideNotes \cadenzaOff \bar "|"
    R1*3
  }
  
  % alto 1 en F
  \transpose c ees, \relative c''' {
    
    \key d \minor
    
    \break
    R1*4
    
    a1 -> \fp \< ~ 
    a ~
    \break
    a8 \! r r4 r2
    r2 a, \glissando
    
    \once \override Tie.direction = #DOWN
    \pitchedTrill
    a'1 ~ \startTrillSpan bes 
    a ~
    \break
    a8 \stopTrillSpan r a -> a -> a -> a -> r a ->
    r a4 -> a8 -> a4 -> a ->
    a4 -^ r r2
    R1
  } 
}

altoSaxI = \relative c' {

  % Para pintar todos los números de compás debajo del pentagrama
  % \override Score.BarNumber #'break-visibility = #end-of-line-invisible
  % \override Score.BarNumber #'direction = #DOWN
  % \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0
  \set Staff.explicitKeySignatureVisibility = #begin-of-line-visible
    
  % trumpet
  \transpose ees bes, \relative c''' {
    
    \key g \minor
    
    R1*10
    
    r4 gis8 -> \ff gis -> gis -> gis -> r gis ->
    r gis4 -> gis8 -> gis4 -> gis4 ->
    
    \break
    \cadenzaOn a1 -> \glissando \hideNotes a,32 \unHideNotes \cadenzaOff \bar "|"
    R1
    r2 a'8 -- bes4 -. a'8 ->
    r4 fis8 ( f e2 )
  }
    
  \bar ".|:-||"
  \repeat volta 2 {
    
    \transpose ees c \relative c'' {
      
      \key f \minor
    
      \break
      r4 des2. -> \fp \< ~
      des1 ~
      des8 \! r r2 r4
      R1 
    }
    
    \transpose ees bes \relative c'' {
    
      \key g \minor
      
      r4 d,2 -> ees4 -.
      \break
      c'4 ( b8 a aes2 ) ~
      aes8 r r4 r2
      R1
    }
    
    % trombone A9
    \transpose ees c \relative c'' {
      
      \key f \minor
      
      r2 bes ->
      r2 bes ->
      des4 -^ r r2
      R1
    }
  }
  
  % trumpet A15
  \alternative {
    { 
      \transpose ees bes \relative c'' {
      
        \key g \minor
      
        \break
        R1*2
        r4 r8 d ( e4 -. ) d -.
        e8 -- d4 -. e8 -> ~ e2
      }
    }
    
    { 
      \transpose ees bes \relative c'' {
      
        \key g \minor
      
        \break
        R1
        r2 e ->
        a4 -^ r8 a -> ~ a
      }
      
      % saxo A19
      \relative c'' {
       
        \key d \minor
        
        a ( b [ cis ]
        e cis d e f fis g gis )
      }
    }
  }
  
  \relative c''' {
    
    \key d \minor
   
    \break
    a1 (
    e
    b2 )
  }
      
  % trombone B3
  \transpose ees c \relative c'' {
    
    \key f \minor
    
    d8 [ -- d -. ] r des-.
    r des4 -.des8 -- des4 -. des4 -.
    \break
    c1 ~ 
    c1
    
    r4 d2 -> \turn c4 -.
    g'2. \turn f4 -.
    \break
    \cadenzaOn bes2 -> \glissando \hideNotes bes,32 \unHideNotes
  }
    
  % saxo 9'5
  \relative c''' { 
    
    \key d \minor
    
    c2 ( \cadenzaOff \bar "|"
    a g
    d1 ) ~ 
    d
    
    \break
    r4 d2 -> cis4 -. 
    c1 ->
    r2 a4 ( \< b8 bis
    cis4 ees8 e f4 g8 gis
    
    \break
    a1 -> \! ) ~ 
    a ~
    a4 ( bes a e
    g a cis e,
    
    \break
    g1 ) ~ 
    g
    r4 a ( bes a
    bes a gis cis,
    
    \break
    g'1 ) ~
    g2. r4
    r a ( aes g 
    fis f ees cis
    
    \break
    d2. ) ~ d8 e -> ~
    e2
  }
  
  % trumpet C14
  \transpose ees bes \relative c'' {
    
    \key g \minor
    
    a8 -- bes4 -. a'8 ->
    r4 fis8 ( f e2 ) \< ~ 
    e1
    
    \break
    R1*16 \!
  }
  
  % trombone E
  \transpose ees c' \relative c' {
    
    \key f \minor
    
    R1*2
    ees1 -> \fp ~
    \override Glissando.style = #'line
    ees2 \< \glissando e \> \glissando
    \override Glissando.style = #'trill
    
    ees1 \! ~ 
    ees1
  }
    
  % saxo E7
  \relative c''' {
    
    \key d \minor
   
    \break
    r4 a ( bes a
    bes a gis cis,
    g'1 \< ) ~
    g ~
    
    \break
    g8 \! a4. -> \ff aes4 -. g -.
    fis -. f -. ees8 -- cis4 -. e8 -> ~ 
    \cadenzaOn e1 -> \glissando \hideNotes g,32 \unHideNotes \cadenzaOff \bar "|"
    R1*3
  }
  
  % trumpet F
  \transpose ees bes, \relative c''' {
    
    \key g \minor
    
    R1*6
    
    \break
    \repeat percent 2 { r4 gis8 -> gis -> gis2 -> }
    \repeat percent 2 { r4 b8 -> b -> b2 -> }
    
    \break
    r4 d8 -> d -> d -> d -> r d ->
    r d4 -> d8 -> d4 -> d ->
    g4 -^ r4
  }
  
  % trombone 2END
  \transpose ees c \relative c' {
    
    \key f \minor
    
    r4 f8 -> f -> 
    f4 -> r r2
  }
}

tenorSaxI = \relative c' {

  % Para pintar todos los números de compás debajo del pentagrama
  % \override Score.BarNumber #'break-visibility = #end-of-line-invisible
  % \override Score.BarNumber #'direction = #DOWN
  % \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0
  \set Staff.explicitKeySignatureVisibility = #begin-of-line-visible
    
  % trumpet
  \relative c'' {
    
    \key g \minor
    
    R1*10
    
    r4 fis8 -> \ff fis -> fis -> fis -> r fis ->
    r fis4 -> fis8 -> fis4 -> fis4 ->
    
    \break
    \cadenzaOn e1 -> \glissando \hideNotes e,32 \unHideNotes \cadenzaOff \bar "|"
    R1
    r2 a8 -- bes4 -. a'8 ->
    r4 fis8 ( f e2 )
  }
    
  \bar ".|:-||"
  \repeat volta 2 {
    
    \transpose bes c'' \relative c' {
      
      \key f \minor
      
      \break
      r4 bes2. -> \fp \< ~
      bes1 ~
      bes8 \! r r2 r4
      R1
    }
    
    \relative c'' {
    
      \key g \minor
      
      r4 d2 -> ees?4 -.
      \break
      c'4 ( b8 a aes2 ) ~
      aes8 r r4 r2
      R1
    }
    
    % trombone A9
    \transpose bes c' \relative c' {
      
      \key f \minor
      
      r2 e ->
      r2 e ->
      bes'4 -^ r r2
      R1
    }
  }
  
  % trumpet A15
  \alternative {
    { 
      \relative c'' {
      
        \key g \minor
      
        \break
        R1*2
        r4 r8 d ( e4 -. ) d -.
        e8 -- d4 -. e8 -> ~ e2
      }
    }
    
    { 
      \relative c'' {
      
        \key g \minor
      
        \break
        R1
        a2 e' ->
        e4 -^ r8 e -> ~ e
      }
      
      % saxo A19
      \relative c' {
       
        \key g \minor
        
        d ( e [ fis ]
        a fis g a bes b c cis
      }
    }
  }
  
  \relative c'' {
    
    \key g \minor
   
    \break
    f1
    c?1
    f,2 )
  }
      
  % trombone B3
  \transpose bes c' \relative c'' {
    
    \key f \minor
    
    a8 [ -- a -. ] r a-.
    r a4 -. a8 -- a4 -. a4 -.
    \break
    aes?1 ~ 
    aes1
    
    r4 c2 -> \turn aes4 -.
    d2. \turn d4 -.
    \break
    \cadenzaOn f2 -> \glissando \hideNotes g,32 \unHideNotes
  }
    
  % saxo 9'5
  \relative c''' { 
    
    \key g \minor
    
    a2 ( \cadenzaOff \bar "|"
    ees ees
    bes1
    a )
    
    \break
    r4 aes2 -> aes4 -. 
    g1 ->
    r2 d'4 ( \< e8 eis
    fis4 aes8 a bes4 e,8 fis
    
    \break
    fis1 -> \! ) ~ 
    fis ~
    fis4 ( g fis cis
    e fis ais cis,
    
    \break
    dis1 ) ~ 
    dis
    r4 fis ( g fis
    g fis eis ais,
    
    \break
    dis1 ) ~
    dis2. r4
    r fis ( f e 
    ees? d c c
    
    \break
    bes2 )
  }
  
  
  % trumpet C14
  \relative c'' {
    
    \key g \minor
    
    a8 -- bes4 -. a'8 -> ~
    a1 ~
    a1 \< ~
    a1
    
    \break
    R1*16 \!
  }
  
  % trombone E7
  \transpose bes c' \relative c'' {
    
    \key f \minor
    
    R1*2
    bes1 -> \fp ~
    \override Glissando.style = #'line
    bes2 \< \glissando ces \> \glissando
    \override Glissando.style = #'trill
    
    bes1 \! ~ 
    bes1
  }
    
  % saxo E
  \relative c'' {
    
    \key g \minor
   
    \break
    r4 fis ( g fis
    g fis eis ais,
    dis1 \< ) ~
    dis ~
    
    \break
    dis8 \! fis4. -> \ff f4 -. e -.
    ees? -. d -. c8 -- c4 -. d8 -> ~ 
    \cadenzaOn d1 -> \glissando \hideNotes e,32 \unHideNotes \cadenzaOff \bar "|"
    R1*3
  }
  
  % trumpet F
  \relative c'' {
    
    \key g \minor
    
    R1*6
    
    \break
    \repeat percent 2 { r4 ees8 -> ees -> ees2 -> }
    \repeat percent 2 { r4 fis8 -> fis -> fis2 -> }
    
    \break
    r4 b8 -> b -> b -> b -> r b ->
    r b4 -> b8 -> b4 -> b ->
    bes?4 -^ r4
  }
  
  % trombone 2END
  \transpose bes c' \relative c' {
    
    \key f \minor
    
    r4 f8 -> f -> 
    f4 -> r r2
  }
}

tenorSaxII = \relative c' {

  % Para pintar todos los números de compás debajo del pentagrama
  % \override Score.BarNumber #'break-visibility = #end-of-line-invisible
  % \override Score.BarNumber #'direction = #DOWN
  % \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0
  \set Staff.explicitKeySignatureVisibility = #begin-of-line-visible
    
  % trumpet
  \relative c'' {
    
    \key g \minor
    
    R1*10
    
    r4 ees8 -> \ff ees -> ees -> ees -> r ees ->
    r ees4 -> ees8 -> ees4 -> ees4 ->
    
    \break
    \cadenzaOn d1 -> \glissando \hideNotes d,32 \unHideNotes \cadenzaOff \bar "|"
    R1*3
  }
    
  \bar ".|:-||"
  \repeat volta 2 {
    
    % trombone A
    \transpose bes c'' \relative c {
      
      \key f \minor
      
      \break
      r4 e2. -> \fp \< ~
      e1 ~
      e8 \! r r2 r4
      R1
      
      \break
      R1*4
      
      r4 g2 -> g4 -.
      r4 g2 -> g4 -.
      e4 -^ r r2
      R1
    }
  }
  
  \alternative {
    { 
      \transpose bes c'' \relative c' {
        
        \key f \minor
      
        \break
        r4 r8 c ( d4 -. ) c -.
        d8 -- c4 -. d8 -> ~ d2
        c1 -> ~ 
        c
      }
    }
    
    { 
      % trumpet 4B
      \relative c' {
      
        \key g \minor
      
        \break
        r2 e ->
        a1 ->
        d4 -^ r8 d -> ~ d
      }
      
      % saxo A19
      \relative c' {
       
        \key g \minor
        
        d ( e [ fis ]
        a fis g a bes b c cis
      }
    }
  }
  
  \relative c'' {
    
    \key g \minor
   
    \break
    d1
    a1
    a2 )
  }
      
  % trombone B3
  \transpose bes c' \relative c'' {
    
    \key f \minor
    
    g8 [ -- g -. ] r g-.
    r g4 -. g8 -- g4 -. g4 -.
    \break
    d1 ~ 
    d1
    
    r4 aes'2 -> \turn f4 -.
    c'2. \turn c4 -.
    \break
    \cadenzaOn des2 -> \glissando \hideNotes g,32 \unHideNotes
  }
    
  % saxo 9'5
  \relative c''' { 
    
    \key g \minor
    
    g2 ( \cadenzaOff \bar "|"
    d c
    g1 \glissando
    c, )
    
    \break
    r4 c'2 -> c4 -. 
    bes1 ->
    r2 d,4 ( \< e8 eis
    fis4 aes8 a bes4 cis8 d
    
    \break
    ees1 -> \! ) ~ 
    ees ~
    ees4 ( e ees bes
    des ees g bes,
    
    \break
    a1 ) ~ 
    a
    r4 ees' ( e ees
    e ees d g,
    
    \break
    a1 ) ~
    a2. r4
    r ees' ( d des 
    c b a? fis
    
    \break
    a2 )
  }
  
  
  % trumpet C14
  \relative c'' {
    
    \key g \minor
    
    a8 -- bes4 -. e8 -> ~
    e1 ~
    e1 \< ~
    e
    
    \break
    R1*16 \!
  }
  
  % trombone E7
  \transpose bes c' \relative c' {
    
    \key f \minor
    
    R1*2
    e1 -> \fp ~
    \override Glissando.style = #'line
    e2 \< \glissando f \> \glissando
    \override Glissando.style = #'trill
    
    e1 \! ~ 
    e1
  }
    
  % saxo E
  \relative c'' {
    
    \key g \minor
   
    \break
    r4 ees ( e ees
    e ees d g,
    a1 \< ) ~
    a ~
    
    \break
    a8 \! ees'4. -> \ff d4 -. des -.
    c -. b -. a?8 -- a4 -. bes8 -> ~ 
    \cadenzaOn bes1 -> \glissando \hideNotes e,32 \unHideNotes \cadenzaOff \bar "|"
    R1*3
  }
  
  % trumpet F
  \relative c'' {
    
    \key g \minor
    
    R1*6
    
    \break
    \repeat percent 2 { r4 c8 -> c -> c2 -> }
    \repeat percent 2 { r4 ees8 -> ees -> ees2 -> }
    
    \break
    r4 fis8 -> fis -> fis -> fis -> r fis ->
    r fis4 -> fis8 -> fis4 -> fis ->
    g4 -^ r4
  }
  
  % trombone 2END
  \transpose bes c' \relative c' {
    
    \key f \minor
    
    r4 f8 -> f -> 
    f4 -> r r2
  }
}

\score {
  <<
    \new Staff { \set Staff.midiInstrument = #"violin" \unfoldRepeats \transpose c c \violin }
    \new Staff { \set Staff.midiInstrument = #"alto sax" \unfoldRepeats \transpose c ees, \altoSaxI }
    \new Staff { \set Staff.midiInstrument = #"tenor sax" \unfoldRepeats \transpose c bes,, \tenorSaxI }
    \new Staff { \set Staff.midiInstrument = #"tenor sax" \unfoldRepeats \transpose c bes,, \tenorSaxII }
    \new Staff { \set Staff.midiInstrument = #"baritone sax" \unfoldRepeats { \transpose c ees,, \baritoneSax } }
  >>
  
  \midi {
    
    \context {
      \Score
      tempoWholesPerMinute = #(ly:make-moment 120 2)
    }
  }
}