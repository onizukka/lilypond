\version "2.18.2"

\include "lily-jazz/ignatzek-jazz-chords.ily"
\include "lily-jazz/lily-jazz.ily"

% PLUGINS
% - \eolMark: 		mark al final de línea
% - \bendBefore: 	solución al bendBefore (i.e. dis8 \bendBefore)
% - \rit: 			crea una porción de ritardando que debe terminar como las dinámicas, con un \!
% - \rs: 			negra de improvisación
% - \emptyStave:	Imprime un pentagrama vacío

% LAYOUT
% - El aspecto de los silencios multicompás es como los de las partituras de jazz
% - Margen extra de las articulaciones para evitar la colisión con las ligaduras
% - Glissandos bonitos

% PAPER
% - Se imprime el título, instrumento y número de página en todas las páginas
% - Solo se debe tocar el page-count (si eso). Sale bonito tal cual.

% CAMBIOS
% 03-09-14: Layout
% Ahora las articulaciones van por dentro de las ligaduras. Parece que es la mejor solución
%
% 04-09-14: Layout
% Los creciendos y decreciendos tienen la línea un poco más gruesa
%
% Markup
% Añadidas guías para correcta posición y markup de la indicación de tempo
%
% Plugins
% Añadida función \emptyStave para imprimir un pentagrama vacío.
% En algunas partituras hay un pentagrama vacío antes del coda (lo cual mola bastante, por cierto)
%
% 05-09-14: Plugins
% Añadido a la función \emptyStave el esconder la armadura de la clave, que no estaba.
%
% 08-09-14: Layout
% Ahora no se muestra la cancelación de accidentales al haber un cambio de armadura
%
% 09-09-14: Chords
% Añadida sentencia para que los acordes menores salgan con un guión en vez de con una M minúscula
%
% 10-09-14: Plugins
% Mejorada la función \bendAfter.
% Ahora es genérica, y se puede definir cualquier texto en un lateral que haga las funciones de ornamento
%
% 15-09-14: Layout
% Añadido un poco más de margen a los reguladores
%
% 16-09-14: Plugins
% Mejorada la función para imprimir barras de improvisación.
% Antes se llamaba al comando \rs para pintar una barra que ocupaba una negra
% Se reemplaza \rs por \restImproOn y \restImproOff, siendo esta opción mucho más flexible.
% Cuando se activa \restImproOn, todo silencio se convierte en barra de improvisación, respetando su duración.
%
% 20-09-14: Plugins
% Mejorada la función para imprimir barras de improvisación.
% Se reemplaza \restImproOn y \restImproOff por \improOn y \improOff
% Ahora, también las notas del pentagrama salen como barra de improvisación.
% (los comandos \improvisationOn e \improvisationOff de lilypond no se llevan bien con LilyJAZZ)
% Se ha añadido un grob correspondiente a los slash de la fuente LilyJAZZ en el archivo lily-jazz.ly
%
% 20-09-14: Layout
% Comentada la línea que oculta la cancelación de accidentales al haber cambio de armadura.
% Realmente, la cancelación es muy bienvenida.
%
% 01-10-14: Plugins
% Renombrada la función \emptyStave a \emptyStaff (stave es el plural de staff) y 
% corregida (ya no muestra el número de compás).
% Recordar que después de usar \emptyStaff se necesita redefinir el 
% número de compás con \set Score.currentBarNumber = #xx
%
% Añadida la función \emptyBar que oculta un compás entero. Se puede usar para crear un 
% efecto de indentación en un pentagrama dado.
% Recordar que después de usar \emptyBar se necesita:
% - redefinir el número de compás con \set Score.currentBarNumber = #xx
% - volver a pintar la clave con \clef
% - volver a pintar la armadura con \key


% PLUGINS 1.2
% -----------
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~





% multi-mark-engraver
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
% Aporta la posibilidad de pintar un rehearsal mark al final de una línea y 
% al comienzo de la siguiente.
% http://lists.gnu.org/archive/html/lilypond-user/2011-08/msg00157.html
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
#(define (multi-mark-engraver ctx)
   (let ((texts '())
         (final-texts '())
         (events '()))

     `((start-translation-timestep
        . ,(lambda (trans)
             (set! final-texts '())))

       (listeners
        (mark-event
         . ,(lambda (trans ev)
              (set! events (cons ev events)))))

       (acknowledgers
        (break-alignment-interface
         . ,(lambda (trans grob source)
              (for-each (lambda (mark)
                          (set! (ly:grob-parent mark X) grob))
                        texts))))

       (process-music
        . ,(lambda (trans)
             (for-each
              (lambda (ev)
                (let* ((mark-grob
                        (ly:engraver-make-grob trans 'RehearsalMark ev))
                       (label (ly:event-property ev 'label))
                       (formatter (ly:context-property ctx 'markFormatter)))

                  (if (and (procedure? formatter)
                           (not (markup? label)))
                      (begin
                       (if (not (number? label))
                           (set! label
                                 (ly:context-property ctx 'rehearsalMark)))

                       (if (and (integer? label)
                                (exact? label))
                           (set! (ly:context-property ctx 'rehearsalMark)
                                 (1+ label)))

                       (if (number? label)
                           (set! label (apply formatter (list label ctx)))
                           (ly:warning "rehearsalMark must have
integer value"))))

                  (if (markup? label)
                      (begin
                       (set! (ly:grob-property mark-grob 'text) label)
                       (let ((dir (ly:event-property ev 'direction)))
                         (and (ly:dir? dir)
                              (set! (ly:grob-property mark-grob 'direction)
                                    dir))))
                      (ly:warning "mark label must be a markup object"))

                  (set! texts (cons mark-grob texts))))
              (reverse events))))

       (stop-translation-timestep
        . ,(lambda (trans)
             (if (pair? texts)
                 (let ((staves (ly:context-property ctx 'stavesFound))
                       (priority-index 0))
                   (for-each (lambda (grob)
                               (let ((my-priority (ly:grob-property
grob 'outside-staff-priority 1500)))
                                 (for-each (lambda (stave)

(ly:pointer-group-interface::add-grob grob 'side-support-elements
                                               stave))
                                           staves)
                                 (set! (ly:grob-property grob
'outside-staff-priority) (+ my-priority priority-index))
                                 (set! priority-index (1+ priority-index))
                                 (set! final-texts (cons grob final-texts))))
                             (reverse texts))
                     (set! texts '())
                     (set! events '())))))

        (finalize
         . ,(lambda (trans)
              (and (pair? final-texts)
                   (for-each (lambda (grob)
                               (set! (ly:grob-property grob 'break-visibility)
                                     end-of-line-visible))
                             final-texts)))))))


% Funciones originales
%{
myMark =
#(define-music-function (parser location text) (markup?)
  (make-music 'MarkEvent
              'label text))
			  
toCoda = {
	\tweak #'self-alignment-X #RIGHT
	\tweak #'break-visibility #begin-of-line-invisible
	\myMark \markup { \hspace #1.25 \raise #1.25 \musicglyph #"scripts.coda" }
}
%}

% Función maqueada.
% Se apoya en el código obtenido por consola al ejecutar { \displayMusic { \toCoda }}
eolMark =
#(define-music-function (parser location text) (markup?)
	(make-music 
		'MarkEvent 
		'tweaks (list 
			(cons (quote self-alignment-X) 1)
			(cons (quote break-visibility) #(#t #t #f))
		) 
		'label (markup
			#:line (
				#:hspace 1.25
				#:raise 1.25
				text
			)
		)
	)
)










% rest-score y merge-rests-on-positioning
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
% Permite realizar la fusión de los silencios entre dos voces en un mismo pentagrama.
% https://code.google.com/p/lilypond/issues/detail?id=1228 (fichero merge-rests.ily)
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]

#(define (rest-score r)
  (let ((score 0)
	(yoff (ly:grob-property-data r 'Y-offset))
	(sp (ly:grob-property-data r 'staff-position)))
    (if (number? yoff)
	(set! score (+ score 2))
	(if (eq? yoff 'calculation-in-progress)
	    (set! score (- score 3))))
    (and (number? sp)
	 (<= 0 2 sp)
	 (set! score (+ score 2))
	 (set! score (- score (abs (- 1 sp)))))
    score))

#(define (merge-rests-on-positioning grob)
  (let* ((can-merge #f)
	 (elts (ly:grob-object grob 'elements))
	 (num-elts (and (ly:grob-array? elts)
			(ly:grob-array-length elts)))
	 (two-voice? (= num-elts 2)))
    (if two-voice?
	(let* ((v1-grob (ly:grob-array-ref elts 0))
	       (v2-grob (ly:grob-array-ref elts 1))
	       (v1-rest (ly:grob-object v1-grob 'rest))
	       (v2-rest (ly:grob-object v2-grob 'rest)))
	  (and
	   (ly:grob? v1-rest)
	   (ly:grob? v2-rest)
	   (let* ((v1-duration-log (ly:grob-property v1-rest 'duration-log))
		  (v2-duration-log (ly:grob-property v2-rest 'duration-log))
		  (v1-dot (ly:grob-object v1-rest 'dot))
		  (v2-dot (ly:grob-object v2-rest 'dot))
		  (v1-dot-count (and (ly:grob? v1-dot)
				     (ly:grob-property v1-dot 'dot-count -1)))
		  (v2-dot-count (and (ly:grob? v2-dot)
				     (ly:grob-property v2-dot 'dot-count -1))))
	     (set! can-merge
		   (and
		    (number? v1-duration-log)
		    (number? v2-duration-log)
		    (= v1-duration-log v2-duration-log)
		    (eq? v1-dot-count v2-dot-count)))
	     (if can-merge
		 ;; keep the rest that looks best:
		 (let* ((keep-v1? (>= (rest-score v1-rest)
				      (rest-score v2-rest)))
			(rest-to-keep (if keep-v1? v1-rest v2-rest))
			(dot-to-kill (if keep-v1? v2-dot v1-dot)))
		   ;; uncomment if you're curious of which rest was chosen:
		   ;;(ly:grob-set-property! v1-rest 'color green)
		   ;;(ly:grob-set-property! v2-rest 'color blue)
		   (ly:grob-suicide! (if keep-v1? v2-rest v1-rest))
		   (if (ly:grob? dot-to-kill)
		       (ly:grob-suicide! dot-to-kill))
		   (ly:grob-set-property! rest-to-keep 'direction 0)
		   (ly:rest::y-offset-callback rest-to-keep)))))))
    (if can-merge
	#t
	(ly:rest-collision::calc-positioning-done grob))))










% startParenthesis y endParenthesis
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
% http://comments.gmane.org/gmane.comp.gnu.lilypond.general/64087
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]

% ELIMINADO










% bendBefore
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
% Adaptación
% http://lilypondblog.org/2013/08/adding-ornamentations-to-note-heads-part-1/
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]

%%%
%%% Utilities for defining new grobs, grob properties and music event types
%%% (there should be built-in commands to do that in LilyPond)
%%%
#(define (define-grob-type grob-name grob-entry)
   "Define a new grob and add it to `all-grob-definitions', after
scm/define-grobs.scm fashion.
After grob definitions are added, use:

\\layout {
  \\context {
    \\Global
    \\grobdescriptions #all-grob-descriptions
  }
}

to register them."
   (let* ((meta-entry   (assoc-get 'meta grob-entry))
          (class        (assoc-get 'class meta-entry))
          (ifaces-entry (assoc-get 'interfaces meta-entry)))
     (set-object-property! grob-name 'translation-type? list?)
     (set-object-property! grob-name 'is-grob? #t)
     (set! ifaces-entry (append (case class
                                  ((Item) '(item-interface))
                                  ((Spanner) '(spanner-interface))
                                  ((Paper_column) '((item-interface
                                                     paper-column-interface)))
                                  ((System) '((system-interface
                                               spanner-interface)))
                                  (else '(unknown-interface)))
                                ifaces-entry))
     (set! ifaces-entry (uniq-list (sort ifaces-entry symbol<?)))
     (set! ifaces-entry (cons 'grob-interface ifaces-entry))
     (set! meta-entry (assoc-set! meta-entry 'name grob-name))
     (set! meta-entry (assoc-set! meta-entry 'interfaces
                                  ifaces-entry))
     (set! grob-entry (assoc-set! grob-entry 'meta meta-entry))
     (set! all-grob-descriptions
           (cons (cons grob-name grob-entry)
                 all-grob-descriptions))))

#(define-public (define-grob-property symbol type? description)
   "Define a new grob property.
`symbol': the property name
`type?': the type predicate for this property
`description': the type documentation"
  (set-object-property! symbol 'backend-type? type?)
  (set-object-property! symbol 'backend-doc description)
  symbol)

#(defmacro*-public define-music-type (type-name parents #:rest properties)
     "Add a new music type description to `music-descriptions'
and `music-name-to-property-table'.

`type-name': the music type name (a symbol)
`parents': the parent event classes (a list of symbols)
`properties': a (key . value) property set, which shall contain at least
   description and type properties.
"
   (let ((gproperties (gensym "properties")))
     `(let ((,gproperties (list-copy ',properties)))
        ,@(map (lambda (parent)
                `(define-event-class ',(ly:camel-case->lisp-identifier type-name)
                   ',parent))
               parents)
        (set-object-property! ',type-name
                              'music-description
                              (cdr (assq 'description ,gproperties)))
        (set! ,gproperties (assoc-set! ,gproperties 'name ',type-name))
        (set! ,gproperties (assq-remove! ,gproperties 'description))
        (hashq-set! music-name-to-property-table ',type-name ,gproperties)
        (set! music-descriptions
              (cons (cons ',type-name ,gproperties)
                    music-descriptions)))))

%%%
%%% Grob definition
%%%
#(define (head-ornamentation::print me)
   "Prints a HeadOrnamentation grob, on the left or right side of the
note head, depending on the grob direction."
   (let* ((notes (ly:grob-object me 'elements))
          (y-ref (ly:grob-common-refpoint-of-array me notes Y))
          (x-ref (ly:grob-common-refpoint-of-array me notes X))
          (x-ext (ly:relative-group-extent notes x-ref X))
          (y-ext (ly:relative-group-extent notes y-ref Y))
          (staff-position (ly:grob-staff-position (ly:grob-array-ref notes 0)))
          (y-coord (+ (interval-center y-ext)
                      (if (and (eq? (ly:grob-property me 'shift-when-on-line) #t)
                               (memq staff-position '(-4 -2 0 2 4)))
                          0.5
                          0)))
          (text (ly:text-interface::print me))
          (width (/ (interval-length (ly:stencil-extent text X)) 2.0))
          (x-coord (if (= (ly:grob-property me 'direction) LEFT)
                       (- (car x-ext) width)
                       (+ (cdr x-ext) width))))
     (ly:stencil-translate
      text
      (cons
       (- (- x-coord (ly:grob-relative-coordinate me x-ref X)) 
          (ly:grob-property me 'x-position))
       (- (- y-coord (ly:grob-relative-coordinate me y-ref Y)) 
          (ly:grob-property me 'y-position))
       ))))

%% a new grob property (used to shift an ornamentation when the
%% note head is on a staff line)
#(define-grob-property 'shift-when-on-line boolean?
   "If true, then the ornamentation is vertically shifted when
the note head is on a staff line.")

#(define-grob-property 'x-position number?
   "X position")

#(define-grob-property 'y-position number?
   "Y position")

#(define-grob-type 'HeadOrnamentation
  `((font-size . 0)
    (shift-when-on-line . #f)
    (x-position . 1)
    (y-position . 1)
    (stencil . ,head-ornamentation::print)
    (meta . ((class . Item)
             (interfaces . (font-interface))))))

\layout {
  \context {
    \Global
    \grobdescriptions #all-grob-descriptions
  }
}

%%%
%%% Engraver
%%%
%% The head-ornamentation engraver, with its note-head acknowledger
%% (which add HeadOrnamentation grobs to note heads)
#(define head-ornamentation-engraver
   (make-engraver
    (acknowledgers
     ((note-head-interface engraver note-grob source)
      ;; helper function to create HeadOrnamentation grobs
      (define (make-ornament-grob text direction shift-when-on-line x-position y-position)
        (let ((ornament-grob (ly:engraver-make-grob engraver
                                                    'HeadOrnamentation
                                                    note-grob)))
          ;; use the ornament event text as the grob text property
          (set! (ly:grob-property ornament-grob 'text) text)
          ;; set the grob direction (either LEFT or RIGHT)
          (set! (ly:grob-property ornament-grob 'direction) direction)
          ;; set the shift-when-on-line property using the given value
          (set! (ly:grob-property ornament-grob 'shift-when-on-line)
                shift-when-on-line)
          ;; set the grob x-pos
          (set! (ly:grob-property ornament-grob 'x-position) x-position)
          ;; set the grob y-pos
          (set! (ly:grob-property ornament-grob 'y-position) y-position)
          (ly:pointer-group-interface::add-grob ornament-grob
                                                'elements
                                                note-grob)
          
          ;; the ornamentation is vertically aligned with the note head
          (set! (ly:grob-parent ornament-grob Y) note-grob)
          ;; compute its font size
          (set! (ly:grob-property ornament-grob 'font-size)
                (+ (ly:grob-property ornament-grob 'font-size 0.0)
                   (ly:grob-property note-grob 'font-size 0.0)))
          ;; move accidentals and dots to avoid collision
          (let* ((ornament-stencil (ly:text-interface::print ornament-grob))
                 (ornament-width (interval-length
                                  (ly:stencil-extent ornament-stencil X)))
                 (note-column (ly:grob-object note-grob 'axis-group-parent-X))
                 ;; accidentals attached to the note:
                 (accidentals (and (ly:grob? note-column)
                                   (ly:note-column-accidentals note-column)))
                 ;; dots attached to the note:
                 (dot-column (and (ly:grob? note-column)
                                  (ly:note-column-dot-column note-column))))
            (cond ((and (= direction LEFT) (ly:grob? accidentals))
                   ;; if the ornament is on the left side, and there are
                   ;; accidentals, then increase padding between note
                   ;; and accidentals to make room for the ornament
                   (set! (ly:grob-property accidentals 'padding)
                         ornament-width))
                  ((and (= direction RIGHT) (ly:grob? dot-column))
                   ;; if the ornament is on the right side, and there
                   ;; are dots, then translate the dots to make room for
                   ;; the ornament
                   (set! (ly:grob-property dot-column 'positioning-done)
                         (lambda (grob)
                           (ly:dot-column::calc-positioning-done grob)
                           (ly:grob-translate-axis! grob ornament-width X))))))
          ornament-grob))
      ;; When the note-head event attached to the note-head grob has
      ;; ornamentation events among its articulations, then create a
      ;; HeadOrnamentation grob
      (for-each
       (lambda (articulation)
         (if (memq 'head-ornamentation-event
                   (ly:event-property articulation 'class))
             ;; this articulation is an ornamentation => make the grob
             ;; (either on LEFT or RIGHT direction)
             (begin
               (if (markup? (ly:event-property articulation 'left-text))
                   (make-ornament-grob
                    (ly:event-property articulation 'left-text)
                    LEFT
                    (ly:event-property articulation 'shift-when-on-line)
                    (ly:event-property articulation 'x-position)
                    (ly:event-property articulation 'y-position)))
               (if (markup? (ly:event-property articulation 'right-text))
                   (make-ornament-grob
                   (ly:event-property articulation 'right-text)
                   RIGHT
                   (ly:event-property articulation 'shift-when-on-line)
                   (ly:event-property articulation 'x-position)
                   (ly:event-property articulation 'y-position))))))
         (ly:event-property (ly:grob-property note-grob 'cause) 'articulations))))))

\layout {
  \context {
    \Score
    \consists #head-ornamentation-engraver
  }
}

%%%
%%% Event type definition
%%%
#(define-music-type HeadOrnamentationEvent (music-event)
   (description . "Print an ornamentation at a note head side")
   (types . (general-music post-event event head-ornamentation-event)))

bendBefore =
#(make-music 'HeadOrnamentationEvent
  'x-position 1
  'y-position 2
  'shift-when-on-line #f
  'left-text #{ \markup { \fontsize #0 \rotate #5 \musicglyph #"brackettips.up"} #})










% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
% Funciones propias
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]





% Índice con borde circular y un cómodo espacio interior (y no el que viene por defecto)
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
#(define-markup-command (boxindex layout props text) (markup?)
	(interpret-markup layout props
		(markup #:huge #:override '(box-padding . 0.5) #:box text)
	)
)

#(define-markup-command (boxed layout props text) (markup?)
	(interpret-markup layout props
		(markup #:huge #:override '(box-padding . 0.9) #:box text)
	)
)

% Ritardando
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
rit =
#(make-music 'CrescendoEvent
             'span-direction START
             'span-type 'text
             'span-text "rit.")

% Barras de improvisacion
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
improOn = {
  \override Rest.stencil = #ly:text-interface::print
  \override Rest.text = \markup \jazzglyph #"noteheads.s2slashjazz"
  \override NoteHead.stencil = #jazz-slashednotehead
}

improOff = {
  \revert Rest.stencil
  \revert Rest.text
  \revert NoteHead.stencil
}

% Pentagrama vacío
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
emptyStaff = {
  \once \override Staff.KeySignature.break-visibility = #all-invisible
  \once \override Staff.Clef.break-visibility = #all-invisible
  \once \override Score.BarNumber.break-visibility = #all-invisible
  \break
  s1
  \once \override Score.BarLine.break-visibility = #all-invisible
  \break
}

emptyBar = {
  \stopStaff
  \once \override Staff.KeySignature.break-visibility = #all-invisible
  \once \override Staff.Clef.break-visibility = #all-invisible
  \once \override Score.BarNumber.break-visibility = #all-invisible
  s1
  
  \once \override Score.BarNumber.break-visibility = #all-visible
  \once \override Score.BarLine.break-visibility = #all-invisible
  \set Staff.explicitKeySignatureVisibility = #all-visible
  \startStaff
}










% Añadido de funciones al contexto
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
\layout {
  \context {
    \Score		
    
    % multi-mark-engraver
    \remove "Mark_engraver"
    \consists #multi-mark-engraver
    \consists "Tweak_engraver"
    
    % merge-rests-on-positioning
    \override RestCollision #'positioning-done = #merge-rests-on-positioning
    
    % aspecto de los silencios multicompás
    \override MultiMeasureRest.expand-limit = #1
    
    % margen extra de las articulaciones para evitar la colisión con las ligaduras
    \override Script.padding = #0.5
    \override Script.avoid-slur = #'inside
    
    % glissando bonito
    \override Glissando.style = #'trill
    \override Glissando.bound-details.left.padding = #1.7
    
    % dinámicas un poco mas gruesas
    \override Hairpin.thickness = #1.75
    \override Hairpin.bound-padding = #2
    
    % Esconde la cancelacion de accidentales en el cambio de armadura
    % \override KeyCancellation.break-visibility = #'#(#f #f #f)
  }
}












% LAYOUT
% ------
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~






\header {
  title = "Perfidia"
  instrument = "Baritone Sax"
  composer = "Linda RONSTADT"
  arranger = "Arr: Ray SANTOS"
  poet = \markup \tiny "Reducción y edición: Gabriel PALACIOS"
  meter = \markup \tiny "gabriel.ps.ms@gmail.com"
  tagline = \markup {
    \center-column {
      \line { \tiny "Reducción y edición: Gabriel PALACIOS" }
      \line { \tiny "gabriel.ps.ms@gmail.com" }
    }
  }
}

\paper {
  % El definir otro tipo de fuente no permite cambiar el tamaño global del staff. Es un bug conocigo
  #(define fonts (make-pango-font-tree "LilyJAZZText" "Nimbus Sans" "Luxi Mono" (/ 20 20)))
  
  % distancia entre la parte superior de la página y el título
  top-markup-spacing #'basic-distance = 5
  
  % distancia entre el título y el primer sistema
  markup-system-spacing #'basic-distance = 25
  
  % distancia entre el texto y el primer sistema 
  top-system-spacing #'basic-distance = 15
  
  % márgen inferior
  bottom-margin = 20
    
  % número de páginas
  page-count = 2
  
  ragged-last-bottom = ##f 
  
  oddHeaderMarkup = \markup {
    \on-the-fly #not-first-page \fill-line {
      \fromproperty #'page:page-number-string
      \fromproperty #'header:title
      \fromproperty #'header:instrument
    }
  }
  
  evenHeaderMarkup = \markup {
    \on-the-fly #not-first-page \fill-line {
      \fromproperty #'header:instrument
      \fromproperty #'header:title
      \fromproperty #'page:page-number-string
    }
  }
}










% MUSIC PARTS
% -----------
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~





% scoreMarkup
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
scoreMarkup = \new Staff \with {
   \RemoveEmptyStaves
}
{
  % Oculta el primer pentagrama del markup
  \override Staff.VerticalAxisGroup #'remove-first = ##t
  
  % Indicación de tiempo
  \once \override Score.RehearsalMark.X-offset = #5
  \mark \jazzTempoMarkup "Easy Swing" c4 "112"
  s1*2
  
  \bar "||"
  s1*8
  
  \bar ".|:-||"
  \mark \markup \boxed "A"
  s1*10
  
  \bar "||"
  \mark \markup {
    \column { \boxed "B" }
    \hspace #3
    \column { \vspace #-.4 \jazzglyph #"scripts.segnojazz" }
  }
  s1*8
  
  \bar "||"
  \mark \markup \boxed "C"
  s1*6
  
  \eolMark \markup {
    \column { "To Coda" }
    \hspace #1
    \column { \vspace #-.3 \small \jazzglyph #"scripts.varcodajazz" }
  }
  s1*3
  
  \bar ".|:-||"
  \mark \markup \boxed "D"
  s1*10
  
  \eolMark \markup { 
    \column { "D.S. al" }
    \hspace #1
    \column { \vspace #-.3 \small \jazzglyph #"scripts.varcodajazz" }
  }
  \bar "||"
  s1
  
  \mark \markup \jazzglyph #"scripts.codajazz"
  s1*10
  
  \bar "|."
}





% scoreChords
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
scoreChords = \chords {
    
  \set minorChordModifier = \markup { "-" }
  
  % c1:m f2:m g2:m7
}





% music
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
baritoneSax = \relative c'' {

  % Para pintar todos los números de compás debajo del pentagrama
  % \override Score.BarNumber #'break-visibility = #end-of-line-invisible
  % \override Score.BarNumber #'direction = #DOWN
  % \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0	
  
  \set Score.tempoHideNote = ##t
  \tempo 4 = 110
  
  \key e \minor
  
  R1*2
  
  R1*4
  
  R1*3
  R1 \fermataMarkup
  
  \key e \major
  
  \repeat volta 2 {
    
    \break
    e8 ^"(2x only)" b c cis -> ~ cis4. e8
    fis cis d? dis -> ~ dis2
    e8 b c cis -> ~ cis4. e8
    \break
    fis cis d? dis -> ~ dis2
    
    e,2 ^"(Both x)" ( \p g )
    fis ( b, )
  }
  
  \alternative {
    { \break
      gis'2 ~ gis8 dis -^ fis -^ gis -^
      fis2 -> ( b, -> ) }
    
    { gis'1 ~ 
      gis2. r4 }
  }
  
  \break
  r8 fis cis' [ fis, ] r eis cis' [ eis, ]
  r8 e cis' [ e, ] r dis cis' [ dis, ]
  r8 dis' ( bis [ gis ] ) gis2
  b4 a gis2
  
  \break
  fis4. \pp fis8 b,4. b8
  ais2 ( a )
  r4 r8 \f dis8 -^ dis4 -^ gis -^
  r8 fis8 -^ fis4 -^ b, -^ r
  
  \break
  e'8 \mf b c cis -> ~ cis4. e8
  fis cis d? dis -> ~ dis2
  e8 \mf b c cis -> ~ cis4. e8
  \break
  fis cis d? dis -> ~ dis2
  
  e,2 ( \p g )
  fis ( b, )
  
  \break
  e8 -^ gis -^ fis -^ a -^ gis -^ b -^ a -^ cis -^
  b4 -^ r8 fis8 -> ~ fis4 b8 -- b -.
  r2 b,2 ->
  
  \repeat volta 2 {
    
    \break
    e4. -> cis8 -> ~ cis4 fis8 -- fis -.
    r4 r8 fis b, -> fis' b fis
    e4. -> cis8 -> ~ cis4 fis8 -- fis -.
    \break
    r4 r8 fis b, -> fis' b fis
    
    e4. d8 -^ ~ d8 g4 -^ r8
    c,2 b8 -- b -. r gis'? -> ~
  }
  
  \alternative {
    { \break
      gis1
      fis4 -. r b,2 -> }
    
    { \break
      gis'4 r8 fis -^ fis [ -^ fis ] -^ gis4 -^
      r8 fis -^ fis [ -^ fis ] -^ gis4 -^ r }
  }
  
  \break
  \emptyBar
  \clef treble
  \key e \major
  
  \repeat percent 2 {
    e'8 b c cis -> ~ cis4. e8
    fis cis d? dis -> ~ dis2
  }
  
  \break
  g,4. e8 ~ e2
  a4. d,8 ~ d a' d a
  gis4. g8 ~ g4. g8
  \break
  fis4. b,8 r b'4 -^ e,8 -> ~
  
  e2. r8 gis -^
  e'16 ( -> fis e dis ) e16 ( -> fis e dis ) e8 r r4
}





% score
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
\score {
  <<
    \scoreMarkup
    \scoreChords
    \new StaffGroup <<
      \new Staff { \baritoneSax }
    >>
  >>
}

tenorSaxII = \relative c''' {

  % Para pintar todos los números de compás debajo del pentagrama
  % \override Score.BarNumber #'break-visibility = #end-of-line-invisible
  % \override Score.BarNumber #'direction = #DOWN
  % \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0	
  
  \key a \minor
  
  R1*2
  
  R1*4
  
  R1*3
  R1 \fermataMarkup
  
  \key a \major
  
  \repeat volta 2 {
    
    \break
    a8 e eis fis -> ~ fis4. a8
    b fis g? gis -> ~ gis2
    a8 e eis fis -> ~ fis4. a8
    \break
    b fis g? gis -> ~ gis2
    
    cis,2 ( \p bes )
    a ( gis )
  }
  
  \alternative {
    { \break
      b?2 ~ b8 r r4
      a2 ( -> gis -> ) }
    
    { b?1 ~ 
      b2. r4 }
  }
  
  \break
  d1 ( ~
  d2 b )
  r8 gis' ( eis [ cis ] ) b2
  b4 a b2
  
  \break
  a2 ( \pp gis )
  fis ( fis -- )
  r4 r8 \f b8 -^ b4 -^ b -^
  r8 a8 -^ a4 -^ gis -^ r
  
  \break
  a'8 \mf e eis fis -> ~ fis4. a8
  b fis g? gis -> ~ gis2
  a8 \mf e f fis -> ~ fis4. a8
  \break
  b fis g? gis -> ~ gis2
  
  cis,2 ( \p bes )
  a ( gis )
  
  \break
  a8 -^ cis -^ b -^ d -^ cis -^ e -^ d -^ fis -^
  e4 -^ r8 a,8 -> ~ a4 b8 -- b -.
  r2 gis2 ->
  
  \repeat volta 2 {
    
    \break
    e4. -> a8 -> ~ a4 a8 -- a -.
    r2 a2 ->
    e4. -> a8 -> ~ a4 a8 -- a -.
    \break
    r2 a2 ->
    
    e4. bes'8 -^ ~ bes8 bes4 -^ r8
    a2 a8 -- a -. r4
  }
  
  \alternative {
    { \break
      r8 cis -- d [ -- e ] -^ r8 e -- d [ -- cis ] --
      a4 -^ r gis2 -> }
    
    { r4 r8 fis -^ fis [ -^ fis ] -^ gis4 -^
      r8 fis -^ fis [ -^ fis ] -^ gis4 -^ r }
  }
  
  \break
  \emptyBar
  \clef treble
  \key a \major
  
  \repeat percent 2 {
    a'8 e eis fis -> ~ fis4. a8
    b fis g? gis -> ~ gis2
  }
  
  \break
  a,4. c8 ~ c2
  c4. b8 ~ b4. c8
  b4. bes8 ~ bes4. bes8
  \break
  a4. gis8 r d'4 -^ cis8 -> ~
  
  cis2. r8 gis -^
  a16 ( -> b a gis ) a16 ( -> b a gis ) a8 r r4
}

tenorSaxI = \relative c'' {

  % Para pintar todos los números de compás debajo del pentagrama
  % \override Score.BarNumber #'break-visibility = #end-of-line-invisible
  % \override Score.BarNumber #'direction = #DOWN
  % \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0	
  
  \key e \minor
  
  R1*2
  
  R1*4
  
  R1*3
  R1 \fermataMarkup
  
  \key e \major
  
  \repeat volta 2 {
    
    \break
    e8 b c cis -> ~ cis4. e8
    fis cis d? dis -> ~ dis2
    e8 b c cis -> ~ cis4. e8
    \break
    fis cis d? dis -> ~ dis2
    
    dis2 ( \p b )
    a ( fis )
  }
  
  \alternative {
    { \break
      bis2 ~ bis8 r r4
      a2 ( -> fis -> ) }
    
    { bis1 ~ 
      bis2. r4 }
  }
  
  \break
  cis1 ( ~ 
  cis2 a )
  r8 dis ( bis [ gis ] ) bis2
  dis4 cis bis2
  
  \break
  a2 ( \pp fis )
  gis ( g )
  r4 r8 \f cis8 -^ cis4 -^ bis -^
  r8 a8 -^ a4 -^ a -^ r
  
  \break
  e'8 \mf b c cis -> ~ cis4. e8
  fis cis d? dis -> ~ dis2
  e8 \mf b c cis -> ~ cis4. e8
  \break
  fis cis d? dis -> ~ dis2
  
  dis2 ( \p b )
  a ( fis )
  
  \break
  e8 -^ gis -^ fis -^ a -^ gis -^ b -^ a -^ cis -^
  b4 -^ r8 a8 -> ~ a4 a8 -- a -.
  r2 a2 ->
  
  \repeat volta 2 {
    
    \break
    gis4. -> gis8 -> ~ gis4 a8 -- a -.
    r2 a2 ->
    gis4. -> gis8 -> ~ gis4 a8 -- a -.
    \break
    r2 a2 ->
    
    gis4. a8 -^ ~ a8 a4 -^ r8
    g2 a8 -- a -. r4
  }
  
  \alternative {
    { \break
      r8 bis -- cis [ -- dis ] -^ r8 dis -- cis [ -- bis ] --
      a4 -^ r a2 -> }
    
    { r4 r8 a -^ a [ -^ a ] -^ bis4 -^
      r8 a -^ a [ -^ a ] -^ bis4 -^ r }
  }
  
  \break
  \emptyBar
  \clef treble
  \key e \major
  
  \repeat percent 2 {
    e8 b c cis -> ~ cis4. e8
    fis cis d? dis -> ~ dis2
  }
  
  \break
  b4. b8 ~ b2
  c4. a8 ~ a4. a8
  b4. bes8 ~ bes4. bes8
  \break
  a4. a8 r dis4 -^ dis8 -> ~
  
  dis2. r8 dis -^
  e16 ( -> fis e dis ) e16 ( -> fis e dis ) e8 r r4
}

altoSaxI = \relative c'' {

  % Para pintar todos los números de compás debajo del pentagrama
  % \override Score.BarNumber #'break-visibility = #end-of-line-invisible
  % \override Score.BarNumber #'direction = #DOWN
  % \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0	
  
  \key a \minor
  
  R1*2
  
  R1*4
  
  R1*3
  R1 \fermataMarkup
  
  \key a \major
  \repeat volta 2 {
    
    \break
    R1*4
    
    cis2 ( \p d )
    cis ( ais )
  }
  
  \alternative {
    { \break
      r4 cis16 ( d e d cis2 )
      r4 d16 ( e fis d e2 ) }
    
    { r2 r4 eis16 ( fis gis fis 
      eis1 ) }
  }
  
  \break
  R1*4
  
  \break
  b2 ( \pp cis )
  cis ( b )
  r4 r8 \f fis'8 -^ fis4 -^ eis -^
  r8 a8 -^ a4 -^ fis -^ r
  
  \break
  R1*4
  cis2 ( \p d )
  cis ( b )
  
  \break
  R1
  R1
  r4 r8 \f e -^ fis16 ( f e8 -^ ) fis16 ( f e8 -^ )
  
  \repeat volta 2 {
    
    \break
    e4. -> e8 -> ~ e4 e8 -- e -.
    r4 r8 gis, ( a b a gis )
    e'4. -> e8 -> ~ e4 e8 -- e -.
    \break
    r4 r8 gis, ( a b a gis )
    
    a8 gis fis a ~ a4. fis8
    e4 d8 e fis d -^ r cis -> ~
  }
  
  \alternative {
    { \break
      cis1 
      r8 b' -> d [ -> e ] -> fis16 ( f e8 -^ ) fis16 ( f e8 -^ ) }
    
    { cis4 r8 fis -^ fis [ -^ fis ] -^ gis4 -^
      r8 fis -^ fis [ -^ fis ] -^ gis4 -^ r }
  }
  
  \break
  \emptyBar
  \clef treble
  \key a \major
  R1*4
  
  \break
  c,8 ( g gis a ~ a4. ) c8 (
  d a ais b -^ ~ b c4 -^ ) d8 (
  cis? gis a bes ~ bes4. ) g'8 (
  \break
  fis8 d a d -^ ~ d gis4 -^ gis8 -> ) ~
  
  gis1 ~
  gis2 ~ gis8 r r4
}

violin = \relative c'' {

  % Para pintar todos los números de compás debajo del pentagrama
  % \override Score.BarNumber #'break-visibility = #end-of-line-invisible
  % \override Score.BarNumber #'direction = #DOWN
  % \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0	
  
  \key a \minor
  
  R1*2
  
  R1*4
  
  R1*3
  R1 \fermataMarkup
  
  \key a \major
  \repeat volta 2 {
    
    \break
    R1*4
    
    e2 ( \p fis )
    e ( cis )
  }
  
  \alternative {
    { \break
      r4 eis16 ( fis gis fis eis2 )
      r4 fis16 ( gis a fis gis2 ) }
    
    { r4 cis,16 ( d e d cis2 ) ~
      cis4 gis'16 ( a b a gis2 ) }
  }
  
  \break
  R1*4
  
  \break
  d2 ( \pp e )
  eis ( e )
  r4 r8 \f ais8 -^ ais4 -^ gis -^
  r8 cis8 -^ cis4 -^ b -^ r
  
  \break
  R1*4
  e,2 ( \p fis )
  e ( d )
  
  \break
  R1
  R1
  r4 r8 \f e -^ fis16 ( f e8 -^ ) fis16 ( f e8 -^ )
  
  \repeat volta 2 {
    
    \break
    a4. -> a8 -> ~ a4 a8 -- a -.
    r4 r8 gis ( a b a gis )
    a4. -> a8 -> ~ a4 a8 -- a -.
    \break
    r4 r8 gis ( a b a gis )
    
    a8 gis fis a ~ a4. fis8
    e4 d8 e fis d' -^ r cis -> ~
  }
  
  \alternative {
    { \break
      cis1 
      r8 b, -> d [ -> e ] -> fis16 ( f e8 -^ ) fis16 ( f e8 -^ ) }
    
    { cis'4 r8 b -^ b [ -^ b ] -^ cis4 -^
      r8 b -^ b [ -^ b ] -^ cis4 -^ r }
  }
  
  \break
  \emptyBar
  \clef treble
  \key a \major
  R1*4
  
  \break
  c8 ( g gis a ~ a4. ) c8 (
  d a ais b -^ ~ b c4 -^ ) d8 (
  cis? gis a bes ~ bes4. ) bes8 (
  \break
  a8 fis d f -^ ~ f c'4 -^ b8 -> ) ~
  
  b1 ~
  b2 ~ b8 r r4
}


\score {
  \unfoldRepeats
  <<
    \new StaffGroup <<
      \new Staff \with {midiInstrument = #"violin"} { \transpose c bes, \violin }
      \new Staff \with {midiInstrument = #"alto sax"} { \transpose c ees, \transpose ees bes \altoSaxI }
      \new Staff \with {midiInstrument = #"tenor sax"} { \transpose c bes,, \transpose bes ees' \tenorSaxI }
      \new Staff \with {midiInstrument = #"tenor sax"} { \transpose c bes,, \tenorSaxII }
      \new Staff \with {midiInstrument = #"baritone sax"} { \transpose c ees,, \baritoneSax }
    >>
  >>
  \midi {}
}


