\version "2.18.2"

\include "lily-jazz/ignatzek-jazz-chords.ily"
\include "lily-jazz/lily-jazz.ily"

% PLUGINS
% - \eolMark: 		mark al final de línea
% - \bendBefore: 	solución al bendBefore (i.e. dis8 \bendBefore)
% - \rit: 			crea una porción de ritardando que debe terminar como las dinámicas, con un \!
% - \rs: 			negra de improvisación
% - \emptyStave:	Imprime un pentagrama vacío

% LAYOUT
% - El aspecto de los silencios multicompás es como los de las partituras de jazz
% - Margen extra de las articulaciones para evitar la colisión con las ligaduras
% - Glissandos bonitos

% PAPER
% - Se imprime el título, instrumento y número de página en todas las páginas
% - Solo se debe tocar el page-count (si eso). Sale bonito tal cual.

% CAMBIOS
% 03-09-14: Layout
% Ahora las articulaciones van por dentro de las ligaduras. Parece que es la mejor solución
%
% 04-09-14: Layout
% Los creciendos y decreciendos tienen la línea un poco más gruesa
%
% Markup
% Añadidas guías para correcta posición y markup de la indicación de tempo
%
% Plugins
% Añadida función \emptyStave para imprimir un pentagrama vacío.
% En algunas partituras hay un pentagrama vacío antes del coda (lo cual mola bastante, por cierto)
%
% 05-09-14: Plugins
% Añadido a la función \emptyStave el esconder la armadura de la clave, que no estaba.
%
% 08-09-14: Layout
% Ahora no se muestra la cancelación de accidentales al haber un cambio de armadura
%
% 09-09-14: Chords
% Añadida sentencia para que los acordes menores salgan con un guión en vez de con una M minúscula
%
% 10-09-14: Plugins
% Mejorada la función \bendAfter.
% Ahora es genérica, y se puede definir cualquier texto en un lateral que haga las funciones de ornamento
%
% 15-09-14: Layout
% Añadido un poco más de margen a los reguladores


% PLUGINS 1.2
% -----------
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~





% multi-mark-engraver
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
% Aporta la posibilidad de pintar un rehearsal mark al final de una línea y 
% al comienzo de la siguiente.
% http://lists.gnu.org/archive/html/lilypond-user/2011-08/msg00157.html
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
#(define (multi-mark-engraver ctx)
   (let ((texts '())
         (final-texts '())
         (events '()))

     `((start-translation-timestep
        . ,(lambda (trans)
             (set! final-texts '())))

       (listeners
        (mark-event
         . ,(lambda (trans ev)
              (set! events (cons ev events)))))

       (acknowledgers
        (break-alignment-interface
         . ,(lambda (trans grob source)
              (for-each (lambda (mark)
                          (set! (ly:grob-parent mark X) grob))
                        texts))))

       (process-music
        . ,(lambda (trans)
             (for-each
              (lambda (ev)
                (let* ((mark-grob
                        (ly:engraver-make-grob trans 'RehearsalMark ev))
                       (label (ly:event-property ev 'label))
                       (formatter (ly:context-property ctx 'markFormatter)))

                  (if (and (procedure? formatter)
                           (not (markup? label)))
                      (begin
                       (if (not (number? label))
                           (set! label
                                 (ly:context-property ctx 'rehearsalMark)))

                       (if (and (integer? label)
                                (exact? label))
                           (set! (ly:context-property ctx 'rehearsalMark)
                                 (1+ label)))

                       (if (number? label)
                           (set! label (apply formatter (list label ctx)))
                           (ly:warning "rehearsalMark must have
integer value"))))

                  (if (markup? label)
                      (begin
                       (set! (ly:grob-property mark-grob 'text) label)
                       (let ((dir (ly:event-property ev 'direction)))
                         (and (ly:dir? dir)
                              (set! (ly:grob-property mark-grob 'direction)
                                    dir))))
                      (ly:warning "mark label must be a markup object"))

                  (set! texts (cons mark-grob texts))))
              (reverse events))))

       (stop-translation-timestep
        . ,(lambda (trans)
             (if (pair? texts)
                 (let ((staves (ly:context-property ctx 'stavesFound))
                       (priority-index 0))
                   (for-each (lambda (grob)
                               (let ((my-priority (ly:grob-property
grob 'outside-staff-priority 1500)))
                                 (for-each (lambda (stave)

(ly:pointer-group-interface::add-grob grob 'side-support-elements
                                               stave))
                                           staves)
                                 (set! (ly:grob-property grob
'outside-staff-priority) (+ my-priority priority-index))
                                 (set! priority-index (1+ priority-index))
                                 (set! final-texts (cons grob final-texts))))
                             (reverse texts))
                     (set! texts '())
                     (set! events '())))))

        (finalize
         . ,(lambda (trans)
              (and (pair? final-texts)
                   (for-each (lambda (grob)
                               (set! (ly:grob-property grob 'break-visibility)
                                     end-of-line-visible))
                             final-texts)))))))


% Funciones originales
%{
myMark =
#(define-music-function (parser location text) (markup?)
  (make-music 'MarkEvent
              'label text))
			  
toCoda = {
	\tweak #'self-alignment-X #RIGHT
	\tweak #'break-visibility #begin-of-line-invisible
	\myMark \markup { \hspace #1.25 \raise #1.25 \musicglyph #"scripts.coda" }
}
%}

% Función maqueada.
% Se apoya en el código obtenido por consola al ejecutar { \displayMusic { \toCoda }}
eolMark =
#(define-music-function (parser location text) (markup?)
	(make-music 
		'MarkEvent 
		'tweaks (list 
			(cons (quote self-alignment-X) 1)
			(cons (quote break-visibility) #(#t #t #f))
		) 
		'label (markup
			#:line (
				#:hspace 1.25
				#:raise 1.25
				text
			)
		)
	)
)










% rest-score y merge-rests-on-positioning
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
% Permite realizar la fusión de los silencios entre dos voces en un mismo pentagrama.
% https://code.google.com/p/lilypond/issues/detail?id=1228 (fichero merge-rests.ily)
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]

#(define (rest-score r)
  (let ((score 0)
	(yoff (ly:grob-property-data r 'Y-offset))
	(sp (ly:grob-property-data r 'staff-position)))
    (if (number? yoff)
	(set! score (+ score 2))
	(if (eq? yoff 'calculation-in-progress)
	    (set! score (- score 3))))
    (and (number? sp)
	 (<= 0 2 sp)
	 (set! score (+ score 2))
	 (set! score (- score (abs (- 1 sp)))))
    score))

#(define (merge-rests-on-positioning grob)
  (let* ((can-merge #f)
	 (elts (ly:grob-object grob 'elements))
	 (num-elts (and (ly:grob-array? elts)
			(ly:grob-array-length elts)))
	 (two-voice? (= num-elts 2)))
    (if two-voice?
	(let* ((v1-grob (ly:grob-array-ref elts 0))
	       (v2-grob (ly:grob-array-ref elts 1))
	       (v1-rest (ly:grob-object v1-grob 'rest))
	       (v2-rest (ly:grob-object v2-grob 'rest)))
	  (and
	   (ly:grob? v1-rest)
	   (ly:grob? v2-rest)
	   (let* ((v1-duration-log (ly:grob-property v1-rest 'duration-log))
		  (v2-duration-log (ly:grob-property v2-rest 'duration-log))
		  (v1-dot (ly:grob-object v1-rest 'dot))
		  (v2-dot (ly:grob-object v2-rest 'dot))
		  (v1-dot-count (and (ly:grob? v1-dot)
				     (ly:grob-property v1-dot 'dot-count -1)))
		  (v2-dot-count (and (ly:grob? v2-dot)
				     (ly:grob-property v2-dot 'dot-count -1))))
	     (set! can-merge
		   (and
		    (number? v1-duration-log)
		    (number? v2-duration-log)
		    (= v1-duration-log v2-duration-log)
		    (eq? v1-dot-count v2-dot-count)))
	     (if can-merge
		 ;; keep the rest that looks best:
		 (let* ((keep-v1? (>= (rest-score v1-rest)
				      (rest-score v2-rest)))
			(rest-to-keep (if keep-v1? v1-rest v2-rest))
			(dot-to-kill (if keep-v1? v2-dot v1-dot)))
		   ;; uncomment if you're curious of which rest was chosen:
		   ;;(ly:grob-set-property! v1-rest 'color green)
		   ;;(ly:grob-set-property! v2-rest 'color blue)
		   (ly:grob-suicide! (if keep-v1? v2-rest v1-rest))
		   (if (ly:grob? dot-to-kill)
		       (ly:grob-suicide! dot-to-kill))
		   (ly:grob-set-property! rest-to-keep 'direction 0)
		   (ly:rest::y-offset-callback rest-to-keep)))))))
    (if can-merge
	#t
	(ly:rest-collision::calc-positioning-done grob))))










% startParenthesis y endParenthesis
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
% http://comments.gmane.org/gmane.comp.gnu.lilypond.general/64087
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]

% ELIMINADO










% bendBefore
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
% Adaptación
% http://lilypondblog.org/2013/08/adding-ornamentations-to-note-heads-part-1/
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]

%%%
%%% Utilities for defining new grobs, grob properties and music event types
%%% (there should be built-in commands to do that in LilyPond)
%%%
#(define (define-grob-type grob-name grob-entry)
   "Define a new grob and add it to `all-grob-definitions', after
scm/define-grobs.scm fashion.
After grob definitions are added, use:

\\layout {
  \\context {
    \\Global
    \\grobdescriptions #all-grob-descriptions
  }
}

to register them."
   (let* ((meta-entry   (assoc-get 'meta grob-entry))
          (class        (assoc-get 'class meta-entry))
          (ifaces-entry (assoc-get 'interfaces meta-entry)))
     (set-object-property! grob-name 'translation-type? list?)
     (set-object-property! grob-name 'is-grob? #t)
     (set! ifaces-entry (append (case class
                                  ((Item) '(item-interface))
                                  ((Spanner) '(spanner-interface))
                                  ((Paper_column) '((item-interface
                                                     paper-column-interface)))
                                  ((System) '((system-interface
                                               spanner-interface)))
                                  (else '(unknown-interface)))
                                ifaces-entry))
     (set! ifaces-entry (uniq-list (sort ifaces-entry symbol<?)))
     (set! ifaces-entry (cons 'grob-interface ifaces-entry))
     (set! meta-entry (assoc-set! meta-entry 'name grob-name))
     (set! meta-entry (assoc-set! meta-entry 'interfaces
                                  ifaces-entry))
     (set! grob-entry (assoc-set! grob-entry 'meta meta-entry))
     (set! all-grob-descriptions
           (cons (cons grob-name grob-entry)
                 all-grob-descriptions))))

#(define-public (define-grob-property symbol type? description)
   "Define a new grob property.
`symbol': the property name
`type?': the type predicate for this property
`description': the type documentation"
  (set-object-property! symbol 'backend-type? type?)
  (set-object-property! symbol 'backend-doc description)
  symbol)

#(defmacro*-public define-music-type (type-name parents #:rest properties)
     "Add a new music type description to `music-descriptions'
and `music-name-to-property-table'.

`type-name': the music type name (a symbol)
`parents': the parent event classes (a list of symbols)
`properties': a (key . value) property set, which shall contain at least
   description and type properties.
"
   (let ((gproperties (gensym "properties")))
     `(let ((,gproperties (list-copy ',properties)))
        ,@(map (lambda (parent)
                `(define-event-class ',(ly:camel-case->lisp-identifier type-name)
                   ',parent))
               parents)
        (set-object-property! ',type-name
                              'music-description
                              (cdr (assq 'description ,gproperties)))
        (set! ,gproperties (assoc-set! ,gproperties 'name ',type-name))
        (set! ,gproperties (assq-remove! ,gproperties 'description))
        (hashq-set! music-name-to-property-table ',type-name ,gproperties)
        (set! music-descriptions
              (cons (cons ',type-name ,gproperties)
                    music-descriptions)))))

%%%
%%% Grob definition
%%%
#(define (head-ornamentation::print me)
   "Prints a HeadOrnamentation grob, on the left or right side of the
note head, depending on the grob direction."
   (let* ((notes (ly:grob-object me 'elements))
          (y-ref (ly:grob-common-refpoint-of-array me notes Y))
          (x-ref (ly:grob-common-refpoint-of-array me notes X))
          (x-ext (ly:relative-group-extent notes x-ref X))
          (y-ext (ly:relative-group-extent notes y-ref Y))
          (staff-position (ly:grob-staff-position (ly:grob-array-ref notes 0)))
          (y-coord (+ (interval-center y-ext)
                      (if (and (eq? (ly:grob-property me 'shift-when-on-line) #t)
                               (memq staff-position '(-4 -2 0 2 4)))
                          0.5
                          0)))
          (text (ly:text-interface::print me))
          (width (/ (interval-length (ly:stencil-extent text X)) 2.0))
          (x-coord (if (= (ly:grob-property me 'direction) LEFT)
                       (- (car x-ext) width)
                       (+ (cdr x-ext) width))))
     (ly:stencil-translate
      text
      (cons
       (- (- x-coord (ly:grob-relative-coordinate me x-ref X)) 
          (ly:grob-property me 'x-position))
       (- (- y-coord (ly:grob-relative-coordinate me y-ref Y)) 
          (ly:grob-property me 'y-position))
       ))))

%% a new grob property (used to shift an ornamentation when the
%% note head is on a staff line)
#(define-grob-property 'shift-when-on-line boolean?
   "If true, then the ornamentation is vertically shifted when
the note head is on a staff line.")

#(define-grob-property 'x-position number?
   "X position")

#(define-grob-property 'y-position number?
   "Y position")

#(define-grob-type 'HeadOrnamentation
  `((font-size . 0)
    (shift-when-on-line . #f)
    (x-position . 1)
    (y-position . 1)
    (stencil . ,head-ornamentation::print)
    (meta . ((class . Item)
             (interfaces . (font-interface))))))

\layout {
  \context {
    \Global
    \grobdescriptions #all-grob-descriptions
  }
}

%%%
%%% Engraver
%%%
%% The head-ornamentation engraver, with its note-head acknowledger
%% (which add HeadOrnamentation grobs to note heads)
#(define head-ornamentation-engraver
   (make-engraver
    (acknowledgers
     ((note-head-interface engraver note-grob source)
      ;; helper function to create HeadOrnamentation grobs
      (define (make-ornament-grob text direction shift-when-on-line x-position y-position)
        (let ((ornament-grob (ly:engraver-make-grob engraver
                                                    'HeadOrnamentation
                                                    note-grob)))
          ;; use the ornament event text as the grob text property
          (set! (ly:grob-property ornament-grob 'text) text)
          ;; set the grob direction (either LEFT or RIGHT)
          (set! (ly:grob-property ornament-grob 'direction) direction)
          ;; set the shift-when-on-line property using the given value
          (set! (ly:grob-property ornament-grob 'shift-when-on-line)
                shift-when-on-line)
          ;; set the grob x-pos
          (set! (ly:grob-property ornament-grob 'x-position) x-position)
          ;; set the grob y-pos
          (set! (ly:grob-property ornament-grob 'y-position) y-position)
          (ly:pointer-group-interface::add-grob ornament-grob
                                                'elements
                                                note-grob)
          
          ;; the ornamentation is vertically aligned with the note head
          (set! (ly:grob-parent ornament-grob Y) note-grob)
          ;; compute its font size
          (set! (ly:grob-property ornament-grob 'font-size)
                (+ (ly:grob-property ornament-grob 'font-size 0.0)
                   (ly:grob-property note-grob 'font-size 0.0)))
          ;; move accidentals and dots to avoid collision
          (let* ((ornament-stencil (ly:text-interface::print ornament-grob))
                 (ornament-width (interval-length
                                  (ly:stencil-extent ornament-stencil X)))
                 (note-column (ly:grob-object note-grob 'axis-group-parent-X))
                 ;; accidentals attached to the note:
                 (accidentals (and (ly:grob? note-column)
                                   (ly:note-column-accidentals note-column)))
                 ;; dots attached to the note:
                 (dot-column (and (ly:grob? note-column)
                                  (ly:note-column-dot-column note-column))))
            (cond ((and (= direction LEFT) (ly:grob? accidentals))
                   ;; if the ornament is on the left side, and there are
                   ;; accidentals, then increase padding between note
                   ;; and accidentals to make room for the ornament
                   (set! (ly:grob-property accidentals 'padding)
                         ornament-width))
                  ((and (= direction RIGHT) (ly:grob? dot-column))
                   ;; if the ornament is on the right side, and there
                   ;; are dots, then translate the dots to make room for
                   ;; the ornament
                   (set! (ly:grob-property dot-column 'positioning-done)
                         (lambda (grob)
                           (ly:dot-column::calc-positioning-done grob)
                           (ly:grob-translate-axis! grob ornament-width X))))))
          ornament-grob))
      ;; When the note-head event attached to the note-head grob has
      ;; ornamentation events among its articulations, then create a
      ;; HeadOrnamentation grob
      (for-each
       (lambda (articulation)
         (if (memq 'head-ornamentation-event
                   (ly:event-property articulation 'class))
             ;; this articulation is an ornamentation => make the grob
             ;; (either on LEFT or RIGHT direction)
             (begin
               (if (markup? (ly:event-property articulation 'left-text))
                   (make-ornament-grob
                    (ly:event-property articulation 'left-text)
                    LEFT
                    (ly:event-property articulation 'shift-when-on-line)
                    (ly:event-property articulation 'x-position)
                    (ly:event-property articulation 'y-position)))
               (if (markup? (ly:event-property articulation 'right-text))
                   (make-ornament-grob
                   (ly:event-property articulation 'right-text)
                   RIGHT
                   (ly:event-property articulation 'shift-when-on-line)
                   (ly:event-property articulation 'x-position)
                   (ly:event-property articulation 'y-position))))))
         (ly:event-property (ly:grob-property note-grob 'cause) 'articulations))))))

\layout {
  \context {
    \Score
    \consists #head-ornamentation-engraver
  }
}

%%%
%%% Event type definition
%%%
#(define-music-type HeadOrnamentationEvent (music-event)
   (description . "Print an ornamentation at a note head side")
   (types . (general-music post-event event head-ornamentation-event)))

bendBefore =
#(make-music 'HeadOrnamentationEvent
  'x-position 1
  'y-position 2
  'shift-when-on-line #f
  'left-text #{ \markup { \fontsize #0 \rotate #5 \musicglyph #"brackettips.up"} #})










% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
% Funciones propias
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]





% Índice con borde circular y un cómodo espacio interior (y no el que viene por defecto)
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
#(define-markup-command (boxindex layout props text) (markup?)
	(interpret-markup layout props
		(markup #:huge #:override '(box-padding . 0.5) #:box text)
	)
)

#(define-markup-command (boxed layout props text) (markup?)
	(interpret-markup layout props
		(markup #:huge #:override '(box-padding . 0.9) #:box text)
	)
)

% Ritardando
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
rit =
#(make-music 'CrescendoEvent
             'span-direction START
             'span-type 'text
             'span-text "rit.")

% Negra oblicua
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
restImproOn = {
  \override Rest.stencil = #ly:percent-repeat-item-interface::beat-slash
  \override Rest.thickness = #0.25
  \override Rest.slope = #1.5
}

restImproOff = {
  \revert Rest.stencil
  \revert Rest.thickness
  \revert Rest.slope
}


% Pentagrama vacío
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
emptyStave = {
  \once \override Staff.KeySignature.stencil = ##f
  \once \override Staff.Clef.stencil = ##f
  \break
  s1
  \once \override Staff.BarLine.stencil = ##f
  \break
}










% Añadido de funciones al contexto
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
\layout {
  \context {
    \Score		
    
    % multi-mark-engraver
    \remove "Mark_engraver"
    \consists #multi-mark-engraver
    \consists "Tweak_engraver"
    
    % merge-rests-on-positioning
    \override RestCollision #'positioning-done = #merge-rests-on-positioning
    
    % aspecto de los silencios multicompás
    \override MultiMeasureRest.expand-limit = #1
    
    % margen extra de las articulaciones para evitar la colisión con las ligaduras
    \override Script.padding = #0.5
    \override Script.avoid-slur = #'inside
    
    % glissando bonito
    \override Glissando.style = #'trill
    \override Glissando.bound-details.left.padding = #1.7
    
    % dinámicas un poco mas gruesas
    \override Hairpin.thickness = #1.75
    \override Hairpin.bound-padding = #2
    
    % Esconde la cancelacion de accidentales en el cambio de armadura
    \override KeyCancellation.break-visibility = #'#(#f #f #f)
  }
}












% LAYOUT
% ------
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~






\header {
  title = "L-O-V-E"
  instrument = "Baritone Sax"
  composer = "Bert KAEMPFERT & Milt GABLER"
  arranger = ""
  poet = ""
  tagline = ""
}

\paper {
  % El definir otro tipo de fuente no permite cambiar el tamaño global del staff. Es un bug conocigo
  #(define fonts (make-pango-font-tree "LilyJAZZText" "Nimbus Sans" "Luxi Mono" (/ 20 20)))
  
  % distancia entre la parte superior de la página y el título
  top-markup-spacing #'basic-distance = 5
  
  % distancia entre el título y el primer sistema
  markup-system-spacing #'basic-distance = 25
  
  % distancia entre el texto y el primer sistema 
  top-system-spacing #'basic-distance = 15
  
  % márgen inferior
  bottom-margin = 20
    
  % número de páginas
  page-count = 3
  
  ragged-last-bottom = ##f 
  
  oddHeaderMarkup = \markup {
    \on-the-fly #not-first-page \fill-line {
      \fromproperty #'page:page-number-string
      \fromproperty #'header:title
      \fromproperty #'header:instrument
    }
  }
  
  evenHeaderMarkup = \markup {
    \on-the-fly #not-first-page \fill-line {
      \fromproperty #'header:instrument
      \fromproperty #'header:title
      \fromproperty #'page:page-number-string
    }
  }
}










% MUSIC PARTS
% -----------
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~





% scoreMarkup
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
scoreMarkup = \new Staff \with {
   \RemoveEmptyStaves
}
{
  % Oculta el primer pentagrama del markup
  \override Staff.VerticalAxisGroup #'remove-first = ##t
  
  % Indicación de tiempo
  \once \override Score.RehearsalMark.X-offset = #5
  \mark \markup \normalsize "(Medium Swing)"
  s1*4
  
  \bar "||"
  \once \override Score.RehearsalMark.X-offset = #5
  \mark \markup { 
    \column { \boxed "1" } 
    \column { \small "(Vocal)" } 
  }
  s1*16
  
  \mark \markup \boxed "17"
  s1*16
  
  \bar "||"
  \once \override Score.RehearsalMark.X-offset = #5
  \mark \markup { 
    \column { \boxed "33" } 
    \column { \small "(Ensemble)" } 
  }
  s1*16
  
  \bar "||"
  \once \override Score.RehearsalMark.X-offset = #5
  \mark \markup { 
    \column { \boxed "49" } 
    \column { \small "(Violin Solo)" } 
  }
  s1*16
  
  \bar "||"
  \once \override Score.RehearsalMark.X-offset = #5
  \mark \markup { 
    \column { \boxed "65" } 
    \column { \small "(Vocal)" } 
  }
  s1*16
  
  \mark \markup \boxed "81"
  s1*4
  \break
  s1*12
  
  \mark \markup \boxed "97"
  s1*11 
  
  \bar "|."
}





% scoreChords
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
scoreChords = \chords {
    
  \set minorChordModifier = \markup { "-" }
  
  % c1:m f2:m g2:m7
}





% music
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
baritoneSax = \relative c'' {

  % Para pintar todos los números de compás debajo del pentagrama
  % \override Score.BarNumber #'break-visibility = #end-of-line-invisible
  % \override Score.BarNumber #'direction = #DOWN
  % \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0	
  \set Score.explicitKeySignatureVisibility = #begin-of-line-visible
  
  \set Score.tempoHideNote = ##t
  \tempo 4 = 130
  
  % 00: bones
  \transpose ees c' \relative c'' {
    
    \key g \major
    
    R1*4 _\markup \small "(A - D)"
    
    \set Score.barNumberVisibility = #all-bar-numbers-visible
    \set Score.currentBarNumber = #1
    \break
    R1*16
    
    \break
    R1*14
    r4 \mf ais8 ( b -> ~ b4 ) a8 ( bes -> ~
    bes4 ) r r2
  }
  
  % 33: trumpets
  \transpose ees bes \relative c'' {
    
    \key a \major
    
    \break
    r4 cis2 -> \f a4 -.
    a -. r8 e dis ( e ) r b' -> ~
    b2. d8 ( b -^ )
    R1
    
    \break
    r4 ais4 -. b -^ g -.
    gis -^ eis8 ( fis ~ fis ) g ( gis4 -. )
    a2 -> e4 -. dis8 ( e -> ~
    e2 )
  }
  
  % 40 1/2: saxs
  \relative c'' {
    
    \key e \major
    \times 2/3 { r8 b ( \f cis } \times 2/3 { e fis g }
    
    \break
    gis2. ) e4 \glissando
    cis'4. b8 ( gis [ e ] ) r4
    gis8 ( b a g gis e eis fis ~
    fis2 ) r2
    
    \break
    r4 ais2 -> fis4 \glissando
    cis'4. ais8 ( gis fis ) r4
    R1
    r2 b8 ( a f dis
    
    \break
    e4 ) -^ r4 r2
    r4 gis,4 -. \mp g8 ( gis ) r4
    R1
    r4 a8. ( fis16 gis8 a ) r4
    
    \break
    R1
    r8 a8 ( a8. fis16 gis8 a ) r4
    R1*2
    
    \break
    g8 ( gis ) r4 r g8 ( gis )
    R1
    gis8 ( a ) r4 r4 a8 cis -> \sfp ~
    cis1
    
    \break
    R1*2
    r2 cis8 ( \< c b c ) -> \ff ~
    c4. c8 g -> c,4 -> r8
    
    \once \set Score.explicitKeySignatureVisibility = #all-visible
    \key f \major
    
    \break
    R1
    c'8 \mf r r c r2
    r2 d8 r r bes16 ( b
    c8 ) r r4 r8 c r4
    
    \break
    r2 r8 c r4
    c8 r r c r2
    r2 r4 r8 bes16 ( b
    c8 ) r 
  }
  
  % 72 1/4: bones
  \transpose ees c' \relative c'' {
    
    \key aes \major
    b4 -. \mf c8 ees r ees, ~
    
    \break
    ees1 _"dim..." ~
    ees
    f ~
    f2. des8 r
    
    \break
    d r r4 r2
    r2 r4 f4 -- \f
    ees4 -. r r2
    R1
  }
  
  % 81: trumpet I&II, bones I&II
  \transpose ees c' \relative c' {
    
    \key aes \major
    
    \break
    \repeat percent 3 {
      ees2 ( \mf f
      g4. f8 ~ f2 )
    }
    
    aes2 ( \< bes
    b4. c8 ~ c4 )
  }
  
  % 89ana: trumpets
  \transpose ees bes \relative c'' {
    
    \key bes \major
    bes4 -. \f
    
    \break
    f1 -> \sfp ~
    f2.. r8
    ees2. -> \sfp e4 -> \sfp ~
    e1
    
    \break
    r4 cis' -. d8 f, r4
    r2 r4 r8 f -> \f ~ 
    f4 g8. ( f16 ) bes4 -^ r8 c -> \sfp ~
    c2. r4
    
    \break
    r4 cis -. \mf d8 g, r4
    r2 r4 r8 f' -> \f ~ 
    f4 g8. ( f16 ) d4 -^ r8 aes -> \f ~
    aes4 bes8. ( aes16 ) b4 -^ r
  }
  
  % 101: bones
  \transpose ees c' \relative c'' {
    
    \key aes \major
    
    \break
    r4 b -. \mf c8 f, r4
    r2 r4 r8 ees -> \f  ~ 
    ees4 f8. ( ees16 ) aes4 -^ r8 aes -> ~
    aes4 bes8. ( aes16 ) ees4 -^
  }
  
  % 104ana: trumpets
  \transpose ees bes \relative c'' {
    
    \key bes \major
    r8 c -> \ff ~
    
    \break
    c4 ees8. c16 f,4 -. ees -.
    r4 r8 ees r4 g -> ~
    g1 \fermata
  }
}





% score
% [][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]
\score {
  <<
    \scoreMarkup
    \scoreChords
    \new StaffGroup <<
      \new Staff { \baritoneSax }
    >>
  >>
}


tenorSaxII = \relative c'' {

  % Para pintar todos los números de compás debajo del pentagrama
  % \override Score.BarNumber #'break-visibility = #end-of-line-invisible
  % \override Score.BarNumber #'direction = #DOWN
  % \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0	
  \set Score.explicitKeySignatureVisibility = #begin-of-line-visible
  
  % 00: bones
  \transpose bes c' \relative c'' {
    
    \key g \major
    
    R1*4 _\markup \small "(A - D)"
    
    \set Score.barNumberVisibility = #all-bar-numbers-visible
    \set Score.currentBarNumber = #1
    \break
    R1*16
    
    \break
    R1*14
    r4 \mf cis8 ( d -> ~ d4 ) cis8 ( d -> ~
    d4 ) r r2
  }
  
  % 33: trumpets
  \relative c'' {
    
    \key a \major
    
    \break
    r4 e2 -> \f cis4 -.
    cis -. r8 a gis ( a ) r d -> ~
    d2. fis8 ( d -^ )
    R1
    
    \break
    r4 cis4 -. d -^ ais -.
    b -^ g8 ( gis ~ gis ) ais ( b4 -. )
    cis2 -> a4 -. eis8 ( fis -> ~
    fis2 )
  }
  
  % 40 1/2: saxs
  \relative c'' {
    
    \key a \major
    \times 2/3 { r8 e ( \f fis } \times 2/3 { a fis eis }
    
    \break
    e2. ) cis4 \glissando
    a'4. fis8 ( e [ cis ] ) r4
    d8 ( fis fis cis d b cis d ~
    d2 ) r2
    
    \break
    r4 fis2 -> dis4 \glissando
    a'4. fis8 ( e dis ) r4
    R1
    r8 g ( f [ d ] e d bes gis?
    
    \break
    a4 ) -^ r4 r2
    r4 e4 -. \mp dis8 ( e ) r4
    R1
    r4 fis8. ( d16 eis8 fis ) r4
    
    \break
    R1
    r8 e8 ( e8. d16 eis8 fis ) r4
    R1*2
    
    \break
    dis8 ( e ) r4 r dis8 ( e )
    R1
    eis8 ( fis ) r4 r4 fis8 a -> \sfp ~
    a1
    
    \break
    R1*2
    r2 a8 ( \< b cis c ) -> \ff ~
    c4. c8 c -> c4 -> r8
    
    \once \set Score.explicitKeySignatureVisibility = #all-visible
    \key bes \major
    
    \break
    R1
    g8 \mf r r g r2
    r2 bes8 r r g16 ( gis
    a8 ) r r4 r8 a r4
    
    \break
    r2 r8 a r4
    a8 r r a r2
    r2 r4 r8 f16 ( fis
    g8 ) r 
  }
  
  % 72 1/4: bones
  \transpose bes c' \relative c'' {
    
    \key aes \major
    b4 -. \mf c8 ees r f, ~
    
    \break
    f1 _"dim..." ~
    f
    aes ~
    aes2. f8 r
    
    \break
    f r r4 r2
    r2 r4 aes4 -- \f
    g4 -. r r2
    R1
  }
  
  % 81: trumpet I&II, bones I&II
  \transpose bes c' \relative c' {
    
    \key aes \major
    
    \break
    \repeat percent 3 {
      ees2 ( \mf f
      g4. f8 ~ f2 )
    }
    
    c'2 ( \< des
    d4. ees8 ~ ees4 )
  }
  
  % 89ana: trumpets
  \relative c'' {
    
    \key bes \major
    bes4 -. \f
    
    \break
    g1 -> \sfp ~
    g2.. r8
    g2. -> \sfp g4 -> \sfp ~
    g1
    
    \break
    r4 cis -. d8 f, r4
    r2 r4 r8 f -> \f ~ 
    f4 g8. ( f16 ) bes4 -^ r8 d -> \sfp ~
    d2. r4
    
    \break
    r4 cis -. \mf d8 g, r4
    r2 r4 r8 f' -> \f ~ 
    f4 g8. ( f16 ) f4 -^ r8 aes -> \f ~
    aes4 bes8. ( aes16 ) f4 -^ r
  }
  
  % 101: bones
  \transpose bes c' \relative c'' {
    
    \key aes \major
    
    \break
    r4 b -. \mf c8 f, r4
    r2 r4 r8 ees -> \f  ~ 
    ees4 f8. ( ees16 ) aes4 -^ r8 aes -> ~
    aes4 bes8. ( aes16 ) f4 -^
  }
  
  % 104ana: trumpets
  \relative c'' {
    
    \key bes \major
    r8 c -> \ff ~
    
    \break
    c4 ees8. c16 bes4 -. a -.
    r4 r8 a r4 c -> ~
    c1 \fermata
  }
}

tenorSaxI = \relative c'' {

  % Para pintar todos los números de compás debajo del pentagrama
  % \override Score.BarNumber #'break-visibility = #end-of-line-invisible
  % \override Score.BarNumber #'direction = #DOWN
  % \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0	
  \set Score.explicitKeySignatureVisibility = #begin-of-line-visible
  
  % 00: bones
  \transpose bes c' \relative c'' {
    
    \key g \major
    
    R1*4 _\markup \small "(A - D)"
    
    \set Score.barNumberVisibility = #all-bar-numbers-visible
    \set Score.currentBarNumber = #1
    \break
    R1*16
    
    \break
    b1 \pp ~ 
    b
    c ~
    c2.. r8
    
    \break
    c1 ~
    c
    b ~
    b2.. r8
    
    \break
    f'1 ~
    f
    e ~
    e4 r r2
    
    \break
    b1 (
    c2 c )
    r4 \mf dis8 ( e -> ~ e4 ) d8 ( ees -> ~
    ees4 ) r r2
  }
  
  % 33: trumpets
  \relative c'' {
    
    \key a \major
    
    \break
    r4 fis2 -> \f e4 -.
    e -. r8 cis bis ( cis ) r fis -> ~
    fis2. gis8 ( e -^ )
    R1
    
    \break
    r4 dis4 -. e -^ cis -.
    d -^ ais8 ( b ~ b ) cis ( d4 -. )
    e2 -> cis4 -. gis8 ( a -> ~
    a2 )
  }
  
  % 40 1/2: saxs
  \relative c'' {
    
    \key a \major
    \times 2/3 { r8 e ( \f fis } \times 2/3 { a fis eis }
    
    \break
    fis2. ) e4 \glissando
    cis'4. a8 ( fis [ e ] ) r4
    fis8 ( a a eis fis d eis fis ~
    fis2 ) r2
    
    \break
    r4 a2 -> fis4 \glissando
    b4. a8 ( fis fis ) r4
    R1
    r8 g ( f [ d ] e d bes gis?
    
    \break
    a4 ) -^ r4 r2
    r4 fis4 -. \mp eis8 ( fis ) r4
    R1
    r4 gis8. ( fis16 g8 gis ) r4
    
    \break
    R1
    r8 gis8 ( gis8. fis16 g8 gis ) r4
    R1*2
    
    \break
    fis8 ( g ) r4 r fis8 ( g )
    R1
    ais8 ( b ) r4 r4 b8 c -> \sfp ~
    c1
    
    \break
    R1*2
    r2 fis8 ( \< f e ees ) -> \ff ~
    ees4. ees8 ees -> ees4 -> r8
    
    \once \set Score.explicitKeySignatureVisibility = #all-visible
    \key bes \major
    
    \break
    R1
    bes8 \mf r r bes r2
    r2 c8 r r bes16 ( b
    c8 ) r r4 r8 c r4
    
    \break
    r2 r8 c r4
    c8 r r c r2
    r2 r4 r8 gis16 ( a
    bes8 ) r 
  }
  
  % 72 1/4: bones
  \transpose bes c' \relative c'' {
    
    \key aes \major
    b4 -. \mf c8 ees r aes, ~
    
    \break
    aes1 _"dim..." ~
    aes
    bes ~
    bes2. aes8 r
    
    \break
    aes r r4 r2
    r2 r4 bes4 -- \f
    bes4 -. r r2
    R1
  }
  
  % 81: trumpet I&II, bones I&II
  \relative c' {
    
    \key bes \major
    
    \break
    r4 r8 f ( \mf g [ g ] ) r4
    bes4 -. g8 ( g ~ g4 ) r
    r4 r8 bes ( c [ c ] ) r4
    d4 -. c8 ( c ~ c4 ) r
    
    \break
    r4 r8 a ( c [ c ] ) r4
    r4 cis8 ( \< d ~ d4 ) \! r4
    r4 r8 bes ( \< c [ c ] ) r4
    cis4 -. e8 ( d ~ d4 )
  }
  
  % 89ana: trumpets
  \relative c'' {
    
    \key bes \major
    bes4 -. \f
    
    \break
    bes1 -> \sfp ~
    bes2.. r8
    bes2. -> \sfp bes4 -> \sfp ~
    bes1
    
    \break
    r4 cis -. d8 f, r4
    r2 r4 r8 f' -> \f ~ 
    f4 g8. ( f16 ) bes4 -^ r8 f -> \sfp ~
    f2. r4
    
    \break
    r4 cis -. \mf d8 g, r4
    r2 r4 r8 f' -> \f ~ 
    f4 g8. ( f16 ) bes4 -^ r8 aes -> \f ~
    aes4 bes8. ( aes16 ) g4 -^ r
  }
  
  % 101: bones
  \transpose bes c' \relative c'' {
    
    \key aes \major
    
    \break
    r4 b -. \mf c8 f, r4
    r2 r4 r8 ees -> \f  ~ 
    ees4 f8. ( ees16 ) aes4 -^ r8 aes -> ~
    aes4 bes8. ( aes16 ) a4 -^
  }
  
  % 104ana: trumpets
  \relative c'' {
    
    \key bes \major
    r8 c -> \ff ~
    
    \break
    c4 ees8. c16 c4 -. d -.
    r4 r8 d r4 f -> ~
    f1 \fermata
  }
}

altoSaxI = \relative c'' {

  % Para pintar todos los números de compás debajo del pentagrama
  % \override Score.BarNumber #'break-visibility = #end-of-line-invisible
  % \override Score.BarNumber #'direction = #DOWN
  % \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0	
  \set Score.explicitKeySignatureVisibility = #begin-of-line-visible
  
  % 00: bones
  \transpose ees c \relative c'' {
    
    \key g \major
    
    R1*4 _\markup \small "(A - D)"
    
    \set Score.barNumberVisibility = #all-bar-numbers-visible
    \set Score.currentBarNumber = #1
    \break
    R1*16
    
    \break
    b1 \pp ~ 
    b
    c ~
    c2.. r8
    
    \break
    c1 ~
    c
    b ~
    b2.. r8
    
    \break
    f'1 ~
    f
    e ~
    e4 r r2
    
    \break
    b1 (
    c2 c )
    r4 \mf fis8 ( g -> ~ g4 ) fis8 ( g -> ~
    g4 ) r r2
  }
  
  % 33: trumpets
  \transpose ees bes, \relative c''' {
    
    \key a \major
    
    \break
    r4 a2 -> \f gis4 -.
    gis -. r8 fis eis ( fis ) r a -> ~
    a2. b8 ( gis -^ )
    R1
    
    \break
    r4 g4 -. gis -^ eis -.
    fis -^ cis8 ( d ~ d ) eis ( fis4 -. )
    gis2 -> fis4 -. bis,8 ( cis -> ~
    cis2 )
  }
  
  % 40 1/2: saxs
  \relative c'' {
    
    \key e \major
    \times 2/3 { r8 b ( \f cis } \times 2/3 { e fis g }
    
    \break
    gis?2. ) e4 \glissando
    cis'4. b8 ( gis [ e ] ) r4
    gis8 ( b a g gis e eis fis ~
    fis2 ) r2
    
    \break
    r4 ais2 -> fis4 \glissando
    cis'4. ais8 ( gis fis ) r4
    r8 ais ( b [ gis ] a b a fis
    g4 -- g8 e fis4 -- eis8 fis
    
    \break
    e4 ) -^ r4 r2
    r4 gis,4 -. \mp g8 ( gis ) r4
    R1
    r4 a8. ( fis16 gis8 a ) r4
    
    \break
    R1
    r8 a8 ( a8. fis16 gis8 a ) r4
    R1*2
    
    \break
    g8 ( gis ) r4 r g8 ( gis )
    R1
    bis8 ( cis ) r4 r4 cis8 dis -> \sfp ~
    dis1
    
    \break
    R1*2
    \times 2/3 { r8 cis ( c } b cis e \< fis gis a ) -> \ff ~
    a4. g8 a -> g4 -> r8
    
    \once \set Score.explicitKeySignatureVisibility = #all-visible
    \key f \major
    
    \break
    R1
    d8 \mf r r d r2
    r2 d8 r r c16 ( cis
    d8 ) r r4 r8 d r4
    
    \break
    r2 r8 d r4
    d8 r r d r2
    r2 r4 r8 c16 ( cis
    d8 ) r 
  }
  
  % 72 1/4: bones
  \transpose ees c \relative c'' {
    
    \key aes \major
    b4 -. \mf c8 ees r c ~
    
    \break
    c1 _"dim..." ~
    c
    des ~
    des2. ces8 r
    
    \break
    bes r r4 r2
    r2 r4 d4 -- \f
    des4 -. r r2
    R1
  }
  
  % 81: trumpet I&II, bones I&II
  \transpose ees bes, \relative c'' {
    
    \key bes \major
    
    \break
    r4 r8 bes ( \mf c [ c ] ) r4
    d4 -. c8 ( c ~ c4 ) r
    r4 r8 c ( ees [ ees ] ) r4
    f4 -. ees8 ( ees ~ ees4 ) r
    
    \break
    r4 r8 c ( ees [ ees ] ) r4
    r4 e8 ( \< f ~ f4 ) \! r4
    r4 r8 d ( \< ees [ ees ] ) r4
    e4 -. g8 ( f ~ f4 )
  }
  
  % 89ana: trumpets
  \transpose ees bes, \relative c'' {
    
    \key bes \major
    bes4 -. \f
    
    \break
    d1 -> \sfp ~
    d2.. r8
    d2. -> \sfp cis4 -> \sfp ~
    cis1
    
    \break
    r4 cis -. d8 f, r4
    r2 r4 r8 f' -> \f ~ 
    f4 g8. ( f16 ) bes4 -^ r8 aes -> \sfp ~
    aes2. r4
    
    \break
    r4 cis, -. \mf d8 g, r4
    r2 r4 r8 f' -> \f ~ 
    f4 g8. ( f16 ) bes4 -^ r8 aes -> \f ~
    aes4 bes8. ( aes16 ) des4 -^ r
  }
  
  % 101: bones
  \transpose ees c \relative c'' {
    
    \key aes \major
    
    \break
    r4 b -. \mf c8 f, r4
    r2 r4 r8 ees -> \f  ~ 
    ees4 f8. ( ees16 ) aes4 -^ r8 aes -> ~
    aes4 bes8. ( aes16 ) des4 -^
  }
  
  % 104ana: trumpets
  \transpose ees bes, \relative c'' {
    
    \key bes \major
    r8 c -> \ff ~
    
    \break
    c4 ees8. c16 ees4 -. f -.
    r4 r8 f r4 bes -> ~
    bes1 \fermata
  }
}

violin = \relative c'' {

  % Para pintar todos los números de compás debajo del pentagrama
  % \override Score.BarNumber #'break-visibility = #end-of-line-invisible
  % \override Score.BarNumber #'direction = #DOWN
  % \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

  \jazzOn
  \compressFullBarRests
  
  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest #'staff-position
  \override Voice.Rest #'staff-position = #0	
  \set Score.explicitKeySignatureVisibility = #begin-of-line-visible
  
  % 00: violin
  \relative c' {
    
    \key g \major
    
    \repeat percent 2 {
      d2 \mp ^\markup \small "(A)" ( e
      fis e )
    }
    
    \break
    \set Score.barNumberVisibility = #all-bar-numbers-visible
    \set Score.currentBarNumber = #1
    \repeat percent 4 {
      d2 ( e
      fis e )
    }
    
    \break
    \repeat percent 2 {
      g2 ( a
      b a )
    }
    
    \break
    g2 ( a
    b a )
    fis4 -^ r4 r2
    R1
    
    \break
    \repeat percent 4 {
      d2 ( e
      fis e )
    }
    
    \break
    g2 ( a
    b a )
    g2 ( a
    bes g )
    
    \break
    d2 ( e
    fis e )
  }
  
  % 3: saxo I
  \transpose c ees, \relative c'' {
    
    \key e \major
    R1
    r8 \mf b ( \times 2/3 { c e g ) } b8 ( a f dis
    
    \break
    e1 ) ~
    e ~
    e2. cis8 ( dis -^ )
    r4 g8 ( gis b bes a fis ~
    
    \break
    fis1 ) ~
    fis2 ~ fis8 bis, ( cis4 -. )
    b1 ~ 
    b4 r r2
  }
  
  % 41: trumpet I
  \transpose c bes, \relative c'' {
     
     \key a \major
     
     \mark \markup \tiny "(saxos)"
     \break
     R1*3 
     r4 \times 2/3 { r8 fis ( a } f4 -- ) e8 ( ees -> )
     
     \break
     R1
     r2 r4 \times 2/3 { b8 -. \< cis -. dis -. }
     e4 -^ \! r4 r2
     R1
     
     \break
     r4 \restImproOn r4 r4 r4
     \repeat unfold 12 { r4 }
     
     \break
     \repeat unfold 16 { r4 }
     
     \break
     \repeat unfold 16 { r4 }
     
     \break
     \repeat unfold 8 { r4 }
     \restImproOff
     \times 2/3 { r8 fis, ( \< f } e fis a b cis d -> \ff ) ~
     d4. c8 d -> c4 -> r8
     
     \once \set Score.explicitKeySignatureVisibility = #all-visible
     \key bes \major
     
     \break
     r4 \mf cis4 -. d8 f, r4
     R1
     r4 d' -. ees8 c r4
     r4 d -. ees8 c r4
     
     \break
     r4 d -. ees8 f, r4
     R1
     r4 cis'4 -. d8 f, r4
     r4 cis'4 -. d8 f r4
  }
  
  % 73: saxo I
  \transpose c ees, \relative c'' {
    
    \key f \major
    
    \break
    r4 d4 -. c8 ( -> f, ~ f4 ) ~
    f4 d' -- c -- f, --
    r4 c'8 ( -> f, ~ f2 ) ~
    f2. r4
    
    \break
    << 
      { r4 cis'4 -. d8 ( f ~ f4 ) 
        d2. g4 -- } \\
      
      { \hideNotes 
        s2. f4 \glissando 
        d4 d2 \glissando g4 
        \unHideNotes } 
    >>
    c4 -. r r2
    R1
    
    \break
    R1*7
    r2 r4 f,,4 -.
    
    \break
    a4 ( \mf bes c d8. c16
    f2.. ) r8 
    f2. -> \sfp r4
    \times 2/3 { b,8 ( c cis } \times 2/3 { d dis e } \times 2/3 { f fis g } gis8 d ~
    
    \break
    d2. ) c8 ( d ~ 
    d2. d8 d ~ 
    d2. ) r8 c -> \f ~
    c4 d8. ( c16 ) f8 -> ees4. ->
    
    \break
    d2. \mf c4 -.
    d2. r8 c -> ~
    c4 d8. ( c16 ) f4 -^ r8 c -> \f ~
    c4 d8. ( c16 ) fis4 -^ r
    
    \break
    r4 \mf d2 c8 ( d ~ 
    d4 ) c8 ( d ~ d4. ) r8
    r2 r4 r8 \f f -> ~
    f4 g8. ( f16 ) bes4 -^ r8 g -> \ff ~
    
    \break
    g4 bes8. ( g16 ) bes4 -. c -.
    r4 r8 c r4 a -> ~
    a1 \fermata
    
  }
}



\score {
  \unfoldRepeats
  <<
    \new StaffGroup <<
      \new Staff \with {midiInstrument = #"violin"} { \violin }
      \new Staff \with {midiInstrument = #"alto sax"} { \transpose c ees, \altoSaxI }
      \new Staff \with {midiInstrument = #"tenor sax"} { \transpose c bes,, \tenorSaxI }
      \new Staff \with {midiInstrument = #"tenor sax"} { \transpose c bes,, \tenorSaxII }
      \new Staff \with {midiInstrument = #"baritone sax"} { \transpose c ees,, \baritoneSax }
    >>
  >>
  \midi {}
}