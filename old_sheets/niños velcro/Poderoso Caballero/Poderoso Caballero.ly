\version "2.12.3"
\relative c'' {
	<<
		\new ChordNames
		{
			\set chordChanges = ##t
			\chordmode 
			{	
				R1*4
				R1*8
				
				e2:maj7 e4:maj7 e8:maj7 e16:maj7 a16:maj7 
				a1:maj7 
				r8 e4 e16 b16 b2
				gis8:m gis16:m a16 a2.
				
				cis2:maj7 cis4:maj7 cis8:maj7 cis16:maj7 fis16:maj7
				fis1:maj7 
				r8 cis4:m cis16:m b16 b2
				fis2 a8. b16 r4
				
				\set chordChanges = ##f
				e2 b a a
				e2 b a a
				\set chordChanges = ##t
				
				R1*4
				
				fis1:m7 gis1:m7
				fis1:m7 a2 b2
				
				\set chordChanges = ##f
				e2 b a a
				e2 b a a
				\set chordChanges = ##t
			}
		}
		\new Staff 
		{ 
			\compressFullBarRests
			\clef treble 
			
			r4 r8 r16 <gis, cis e>16 ~ <gis cis e>4 ~ <gis cis e>8. cis16
			<a cis fis>4. r8 a'8 ais16 b r4
			
			R1*2
			
			R1*8
	
			e2. b8-. r16 a16 ~ a1 
			r8 e'8 ~ e16 e8-. b16 ~ b2
			gis8-. r16 a16 ~ a2.
			cis2. ~ cis8. fis,16 ~
			fis1
			r8 cis'8 ~ cis16 cis8-. b16 ~ b2
			fis2 a8.^"hamm" b16 r4 
			
			\bar "|:"
			r1^"reggae x4" r1
			\bar ":|"
			
			\mark \markup { \musicglyph #"scripts.coda" }
			
			\bar "|:"
			r1^"reggae x2" r1
			\bar ":|"
			
			R1*4
			
			r1 r1 r1 r1
			
			
			\bar "|:"
			e2.^"reggae + impro x4(8?)" cis8 b 
			g16. fis32 e16. cis32 e16. e32 r8 r2
			r1 r1
			\bar ":|"
		}
	>>
}
