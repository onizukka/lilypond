\version "2.12.3"
\relative c'' {
	<<
		\new ChordNames
		{
			\chordmode 
			{
				d1:m7 a:aug d:11 a:aug
				d:m7 a:aug d:11 a:aug
				d:m7 a:aug d:11 a:aug
				d:m7 a:aug d:11 a:aug
				
				d:m7 a:aug d:11 a:aug
				d:m7 a:aug d:11 a:aug
				d:m7 a:aug d:11 a:aug
				
				d:m a:m d:m a:m
				
				R1*8
				
				d1:m7 a:m7 bes:7 a:aug
				d:m7 a:m7 bes:7 a:aug
				
				R1*4
				
				d1:m7 a:aug d:11 a:aug
				
				d:m a:m d:m a:m
			}
		}
		\new Staff 
		{ 
			\compressFullBarRests
			\clef treble 
			r1 r r 
			r2 f16 c g gis a c d c 
			
			\appoggiatura ees32 e16 g8 c,16 ~ c2.
			\chordmode { a8:aug } r8 r4 r2
			r2 <g b>16 f d c d8 f
			
			r1 r1
			r4 r8 r16 d <aes' d> g r8 r4 r1
			
			r1 r1
			r2 g'16 aes g f d c r8 
			\appoggiatura aes16 g8 f aes d, r2
			<a cis f>8. <a cis f>16 r4 r2
			
			<a c e g>8. <a c e g>16 r4 r8 <a c e g>8 ~ <a c e g>16 <a c e g> r8
			<a cis f>8. <a cis f>16 r4 r2
			r1 r1
			
			r1 r4 r8 r16 d aes' d, g d f d c8
			f8. g16 r4 r2 r1 
			
			\textLengthOn 
			r1^"notas largas" r1 r1 r1^"impro salida"			
			
			\bar "|:"
			r1^"reggae" r1 r1 r1
			\bar ":|"
			
			R1*8
			
			r1 r1 r1 r1
			r4 <aes' d>16 g f d g f d c d c a c ~ c2 r2
			r1 r2 <aes' d>16 g f d g f d c
			
			R1*4
			
			<a, c e g>8. <a c e g>16 r4 r8 <a c e g>8 ~ <a c e g>16 <a c e g> r8
			<a cis f>8. <a cis f>16 r8 r16 d aes' d, g d f d c8
			f8. g16 r4 r2 r1 
			
			\bar "|:"
			r1^"reggae" r1 r1 r1
			\bar ":|"
			
			R1*6
			
			c1\pp\< ~ c2. r4\ff\!
			
			R1*7
			
			\chordmode 
			{ 
				r2 r8 f8:8^7 ~ f16:8^7 g:8^7 ~ g8:8^7
			}
			
			\set countPercentRepeats = ##t
			\repeat "volta" 4
			{
				r4 <a c d f> r <a c d f>
			}
			\alternative
			{
				{ 
					r4 <a c d f> r8 <f a c f> ~ <f a c f>16 <g b d g> ~ <g b d g>8
				}
				{
					r4 <a c d f> r8 <f a c f> ~ <f a c f>16 <g b d g> ~ <g b d g>8
					d8 r8 r4 r2
				}	
			}
		}
	>>
}
