\version "2.12.3"
\relative c'' {
	<<
		\new ChordNames
		{
			\chordmode 
			{
				R1*8
				R1*4
				R1*4
				R1*4
				R2*4
				R1*4
				R1*4
				R1*4
				d4 d c g 
				d4 d c g 
				
			}
		}
		\new Staff 
		{ 
			\compressFullBarRests
			{
				R1*8
				R1*4^"Rapido"
				R1*4^"Cuesta"
				R1*4^"Rodando"
				R1*4
				R1*4^"Rapido"
				R1*4^"Cuesta"
				R1*4^"Rodando"
				\bar "|:" 
					r1^"reggae x 12 (hay que parar)"
				\bar "|:"
				\bar "|:" 
					r1^"reggae x 4"
				\bar "|:"
			}
		}
	>>
}
