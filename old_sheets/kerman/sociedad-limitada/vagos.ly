\version "2.18.2"
\language "english"

% Se debe incluir antes que lilyjazz. Si no, lilyjazz no funciona.
#(set-global-staff-size 20)

% includes
\include "includes/lilyjazz.ily"
\include "includes/chords.ily"
\include "includes/plugins-1.2.ily"

% Cabecera
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\header {
	title = "Vagos y maleantes"
	instrument = ""
	composer = "Kerman"
	arranger = ""
	poet = ""
	tagline = ""
}

% Márgenes, fuentes y distancias del papel
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\paper {
	
	% distancia entre la parte superior de la página y el título
	top-markup-spacing #'basic-distance = 5

	% distancia entre el título y el primer sistema
	markup-system-spacing #'basic-distance = 25

	% distancia entre el texto y el primer sistema 
	top-system-spacing #'basic-distance = 15

	% márgen inferior
	bottom-margin = 20
	
	% intentado del primer sistema
	indent = 0
	 
	% número de páginas
	% page-count = 1

	ragged-last-bottom = ##f 

	oddHeaderMarkup = \markup {
	 \on-the-fly #not-first-page \fill-line {
		\fromproperty #'page:page-number-string
		\fromproperty #'header:title
		\fromproperty #'header:instrument
	 }
	}

	evenHeaderMarkup = \markup {
	 \on-the-fly #not-first-page \fill-line {
		\fromproperty #'header:instrument
		\fromproperty #'header:title
		\fromproperty #'page:page-number-string
	 }
	}
}

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
	% Oculta el primer pentagrama del markup
	\override Staff.VerticalAxisGroup #'remove-first = ##t

	% Indicación de tiempo
	% \once \override Score.RehearsalMark.self-alignment-X = #LEFT
	% \once \override Score.RehearsalMark.extra-offset = #'(0 . 3) 
	% \mark \jazzTempoMarkup "Easy Swing" c4 "112"

	% s1*2
	% \once \override Score.RehearsalMark.X-offset = #5
	% \mark \markup \boxed "A"
}



% Función musical global
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
global = {
	% Para pintar todos los números de compás debajo del pentagrama
	% \override Score.BarNumber #'break-visibility = #end-of-line-invisible
	% \override Score.BarNumber #'direction = #DOWN
	% \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

	\compressFullBarRests

	% Arregla el problema de tener el scoreMarkup añadido
	\revert MultiMeasureRest #'staff-position
	\override Voice.Rest #'staff-position = #0	

	\set Timing.beamExceptions = #'()
	\set Timing.baseMoment = #(ly:make-moment 1/4)
	\set Timing.beatStructure = #'(1 1 1 1) 
	
	\override ParenthesesItem.font-size = #0
}

% Cifrado
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
scoreChords = \chords {
	
	% c1:m f2:m g2:m7
}

% Música
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
trombone = \relative c'' {
  
	\global
	\key fs \minor

	R1*8^"Intro libre"

	\break
	r4 \times 2/3 {cs8-- cs-- cs--} cs a fs a
	r2 \times 2/3 {e4( fs gs}
	fs4) a16 a a8-. gs8 a b a~->
	a4 b16 b b8-. a8 b c a~->

	\break
	a4 \times 2/3 {cs?8-- cs-- cs--} cs a fs a
	r2 \times 2/3 {e4( fs gs}
	fs4) a16 a a8-. gs8 a b a~->
	a4 b16 b b8-. b8 cs e, fs->	\bendAfter #-3

	\break
	R1*8

	R1*7
	r2 r4 r8 fs

	\break
	a8 cs e ds cs ds4-. cs8-.
	r8 b4-. gs8-. r a4-. fs8
	a8 cs e ds cs ds4-. cs8-.
	r8 cs4-. a8~-> a4 r8 fs8

	\break
	a8 cs e ds cs ds4-. cs8-.
	r8 b4-. gs8-. r a4-. e8
	a8 b e ds cs ds4-. cs8-.
	r8 b a fs e4 gs 

	\break
	fs2-> r2
	R1
	r4 gs(\bendBefore fs e 
	fs) gs(\bendBefore fs e

	\break
	fs2)-> r2
	R1
	r4 gs(\bendBefore fs e 
	fs) a(\bendBefore gs a

	\break
	fs2) r
	R1
	r2 gs(\bendBefore
	fs e

	\break
	fs2) r2
	R1*2
	r4 e( fs gs

	\break
	fs4) \times 2/3 {cs'8-- cs-- cs--} cs a fs a
	R1*3
}

% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\book {
	\paper { 
		% Descomentar para score
		% #(set-paper-size "a4" 'landscape)
		% indent = 20
		% systems-per-page = 2
	}
	
	\score {
		% Descomentar para midi
		% \midi { \tempo 4 = 110 }
		
		% NO USAR layout-set-staff-size O SE ROMPE lilyjazz!!!
		\layout { }
		
		<<
			\scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\new Staff \with { instrumentName = "" midiInstrument = #"" } { \trombone }
			>>
		>>
	}
}



%{
\book {
	\header { instrument = "" }
   \bookOutputName ""
	\score {
		<<
			\scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\new Staff { \transpose bf c' \instrumentA }
			>>
		>>
	}
}

\book {
	\header { instrument = "" }
   \bookOutputName ""
	\score {
		<<
			\scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\new Staff { \transpose ef c' \instrumentB }
			>>
		>>
	}
}
%}

