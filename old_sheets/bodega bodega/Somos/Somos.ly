\version "2.12.3"

\header {
	title = "Somos"
	composer = "Bodega Bodega"
}

#(set-global-staff-size 14)


\new StaffGroup 
<<
	\new Staff 
	<< 
		\set Staff.instrumentName = #"Golpes Tpt/SxA"
		\relative c''
		{
			b8 (b16) b (b8) b b8 (b16) b (b8) b 
			a8 (a16) bes (bes8) b c8 (c16) cis (cis8) dis
			e r8 r4 r2
		}
		\\ 
		\relative c'
		{
			fis8 (fis16) fis (fis8) fis fis8 (fis16) fis (fis8) fis 
			fis8 (fis16) f (f8) e ees8 (ees16) d (d8) cis
			b1
		}
	>>
	\new Staff 
	{ 
		\set Staff.instrumentName = #"Golpes SxT"
		\relative c''
		{
			b8 (b16) b (b8) b b8 (b16) b (b8) b 
			b8 (b16) bes (bes8) a aes8 (aes16) g (g8) fis 
			e1
		}
	}
>>


\new StaffGroup 
<<
	\new Staff 
	<< 
		\set Staff.instrumentName = #"Intro Tpt/SxA"
		\relative c''
		{
			a8 (a16) a (a8) a (a16) a a8 c d
			a8 (a16) a (a8) a (a16) a a8 e' d
			a8 (a16) a (a8) a (a16) a a8 c d
			a8 (a16) a (a8) a e'16 e8 d16 d8 c16 c
			r1
		}
		\\ 
		\relative c''
		{
			a8 (a16) e (e8) a, (a16) a' a8 c d
			a8 (a16) a, (a8) a (a16) a' a8 c b
			a8 (a16) a (a8) a, (a16) e' a8 g bes
			a8 (a16) a (a8) a c16 c8 g16 g8 fis16 e
			r1
		}
	>>
	\new Staff 
	{ 
		\set Staff.instrumentName = #"Intro SxT"
		\relative c''
		{
			a8 (a16) a (a8) a, (a16) a' a8 c d
			a8 (a16) a, (a8) a (a16) a' a8 e' d
			a8 (a16) a (a8) a, (a16) e' a8 c d
			a8 (a16) a (a8) a e'16 e8 d16 d8 c16 c
			r1
		}
	}
>>

\new StaffGroup 
<<
	\new Staff 
	<< 
		\set Staff.instrumentName = #"Obg A Tpt/SxA"
		\relative c''
		{
			r2 r4 a16 c d a 
			c d a c d a c d e e8 e16 (e) e (e8)
			e16 c8 a16 (a16) e'8 d16 (d16) a8 d16 e8 d16 e
			e8 r8 r4 r2
		}
		\\ 
		\relative c'
		{
			r4 r8 a16 c d a c d r4
			r8 e16 g a e g a b c8 c16 (c) b (b8)
			c16 a8 e16 (e) c'8 a16 (a) fis8 b16 b8 g16 fis
			r1
		}
	>>
	\new Staff 
	{ 
		\set Staff.instrumentName = #"Obg A SxT"
		\transpose bes c
		{
		\relative c''
			{
				a16 c d a c d r8 r2
				r4 r16 a' c d e g8 fis16 (fis) f (f8)
				a16 e8 a,16 (a16) g'8 fis16 (fis16) d8 fis16 fis8 e16 a, 
				a8 r8 r4 r2
			}
		}
	}
>>

\new StaffGroup 
<<
	\new Staff 
	<< 
		\set Staff.instrumentName = #"Obg A Tpt/SxA"
		\relative c''
		{r1
			e8 r8 r4 r2
		}
		\\ 
		\relative c'
		{r1
			r1
		}
	>>
	\new Staff 
	{ 
		\set Staff.instrumentName = #"Obg A SxT"
		\transpose bes c
		{
			\relative c'''
				{
					d16 cis c8 
					r16 a r aes 
					r a f' d 
					fis g e gis
					e a r8
					d,16 cis c8 
					r16 g8.
					d'16 c a r
					a a r8
					r1
				}
			}
	}
>>

\markup 
{
	\fill-line 
	{
		\center-column 
		{
			\hspace #4
			\huge \smallCaps \bold "Golpes"
			\hspace #1
			\huge \smallCaps \bold "Intro"
			\hspace #1
			\huge \smallCaps "4x"
			\hspace #1
			\huge \smallCaps \bold "Obg A"
			\hspace #1
			\huge \smallCaps "8x"
			\hspace #1
			\huge \smallCaps \bold "3x Pide pide + Golpes"
			\hspace #1
			\huge \smallCaps \bold "Obg B"
			\hspace #1
			\huge \smallCaps \bold "4x Somos"
			\hspace #1
			\huge \smallCaps \bold "4x Mini impros"
			\hspace #1
			\huge \smallCaps \bold "4x Lento Impro"
			\hspace #1
			\huge \smallCaps \bold "4x Lento Impro"
			\hspace #1
			\huge \smallCaps \bold "Obg C"
			\hspace #1
			\huge \smallCaps "8x"
			\hspace #1
			\huge \smallCaps \bold "3x Pide pide + Golpes"
			\hspace #1
			\huge \smallCaps \bold "4x Somos"
			\hspace #1
			\huge \smallCaps \bold "4x Mini impros"
			\hspace #1
			\huge \smallCaps \bold "Intro"
			\hspace #1
			\huge \smallCaps \bold "4x Ska"
			\hspace #1
			\huge \smallCaps \bold "4x Ska FUERTE"
			\hspace #1
			\huge \smallCaps \bold "Final"
			
		}
	}
}