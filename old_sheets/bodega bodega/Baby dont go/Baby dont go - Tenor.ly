tenor = \new Staff
{
	\set Staff.instrumentName = "Sax. Tenor"
	\set Score.skipBars = ##t
	\override Glissando #'style = #'zigzag
	
	\key a \major
	
	\relative c''
	{
		R1*8
		a4--\f a4-- a8.-- g16( a g a8-.) 
		c8.-> a16--( a4) r2
		a4-- a4-- a8.-- g16( a g a8-.)
		ees4->^\markup { \bold growl } ~ 
		\once \override Glissando #'(bound-details right Y) = #-4
		ees \glissando r2
		a4-- a4-- a8.-- g16( a g a8-.) 
		d8.->\< a16--( a8) a-. ees'8.-> a,16--( a8) a-.
		\once \override Glissando #'(bound-details right Y) = #-4
		e'8.-> a,16--( a8) a-. a'4->\sf\glissando r4
		e16( d8-.) c16-- ~ c d16( c8-.)
		ees16 ees8-. d16-- ~ d c-- ~ c8
		\once \override Glissando #'(bound-details right Y) = #-2
		a8->\glissando r8 r4 r2
		R1*3
		R1*3
		r2 r4 c16( d c8-.)
		\once \override Glissando #'(bound-details right Y) = #-3
		e4-> \glissando r4 r2
	}
	
	\relative c''
	{
		R1*7
		r1
		r2 a,4--\mf a--
		a8-> r8 r4 r2
		r2 a4--\f\< a--
		a8->\sf r8 r4 r2
		r1
		a'1\mp\< ~ a
	}
	
	\relative c''
	{
		\repeat "percent" 2
		{
			r8\f a-.\< c16( d c8-.) d->\! r8 r4
			r1
		}
		r8 a-.\< c16( d c8-.) d->\! r8 r4
		r8 gis,-.\> a4-- e--\! r4
		r8 a-.\< c16( d c8-.) d->\! r8 r4
		r8 gis,-.\> a4-- e--\< ~ e \glissando
		\once \override Glissando #'(bound-details right Y) = #-3
		a'4\sf\> \glissando r4\! r2
		R1*7
	}
	
	\relative c''
	{
		R1*2
		\times 2/3 { a,8-. r a-. r a-. r} r2
		r1
		R1*2
		a'1\mp\< ~ a^\markup { \bold growl }
	}
	
	\relative c''
	{
		\repeat "percent" 3
		{
			r8 a-.\< c16( d c8-.) d->\! r8 r4
			r8 gis,-.\> a4-- e--\! r4
		}
		r8 a-.\< c16( d c8-.) d->\! r8 r4
		r8 gis,-. a4-- e4.\< r8\! 
		R1*4^\markup { \italic "ad lib." }
		s1*4^"solo"
		a'4.-> \glissando a4.-> \glissando b4->\glissando (  b8\glissando ) c4.->\glissando  d2->\glissando 
	}
	
	\relative c'''
	{
		e4->\sf\glissando r4 r2
		r2 \times 2/3 { c,4-- d-- d-- }
		\repeat "percent" 4
		{
			a,4-_ c-_ d-_ e8-. g8-. 
			gis4-_ a-_ \times 2/3 { c4-- d-- d-- }
		}
		a,4-_ c-_ d-_ e8-. g8-. 
		gis4-_ a-_ \times 2/3 { a,8-. r a-. r a-. r}
		a4-_ c-_ d-_ e8-. g8-. 
		gis4-_ a-_ \times 2/3 { c4-- d-- d-- }
		a,4-_ c-_ d-_ e8-. g8-. 
		c2.^\markup { \bold growl }_\markup { \dynamic p \italic sub. }\< r4\ff
	}
	
	\relative c''
	{
		r8 a-. a16 a16( a8) d16 d8-. d16--( d8) c-- 
		a4-> r4 r2
		r8 a-. a16 a16( a8) d4.->^\markup { \bold growl } c8 
		\once \override Glissando #'(bound-details right Y) = #-4
		a4->\glissando r4 r2
		r8 a-. a16 a16( a8) d16 d8-. d16--( d8) c-- 
		a4-> r4 r2
		r8 d-. d16 d16--( d8) g16 g8-- g16--( g8) f-- 
		d2-- \glissando a'4-> \glissando r4
		d,4->^\markup { \italic lento } ~  d\>\glissando r2\!\fermata
		e8-> r8 r4 r2
	}
	
	\bar "||"
}

