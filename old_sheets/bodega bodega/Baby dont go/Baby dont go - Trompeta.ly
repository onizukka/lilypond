trompeta = \new Staff
{
	\set Staff.instrumentName = "Trompeta"
	\set Score.skipBars = ##t
	\override Glissando #'style = #'zigzag
	
	\key a \major
	
	\relative c'''
	{
		R1*8
		a4--\f a4-- a8.-- g16( a g a8-.) 
		c8.-> a16--( a4) r2
		a4-- a4-- a8.-- g16( a g a8-.)
		ees4->^\markup { \bold growl } ~ 
		\once \override Glissando #'(bound-details right Y) = #-2
		ees \glissando r2
		a4-- a4-- a8.-- g16( a g a8-.)
		d8.->\< a16--( a8) a-. ees'8.-> a,16--( a8) a-.
		e'8.-> a,16--( a8) a-. g'4->\sf\glissando r4
		e16( d8-.) c16-- ~ c d16-- ~ d8
		ees16 ees8-. d16-- ~ d c16-- ~ c8
		a8->\glissando r8 r4 r2
		R1*3
		R1*3
		r4 a-- ees-- r4
	}
	
	\relative c'''
	{
		R1*8
		r1
		r2 g4--\mf gis-- 
		a8-> r8 r4 r2
		r2 g'4--\f\< gis--
		a8->\glissando\sf r8 r4 r2
		r1
		c,1\mp\< ~ c
	}
	
	\relative c'''
	{
		\repeat "percent" 2
		{
			r8\!\f a-.\< c16( d c8-.) d->\! r8 r4
			r1
		}
		\repeat "percent" 2
		{
			r8 a-.\< c16( d c8-.) d->\! r8 r4
			r8 gis,-.\> a4-- e--\! r4
		}
		R1*8
	}
	
	\relative c'''
	{
		R1*2
		\times 2/3 { g'8-. r g-. r g-. r} r2
		r1
		R1*2
		r2\mp\< r8 gis,-- a4--
		ees1--^\markup { \bold growl }
	}
	
	\relative c'''
	{
		\repeat "percent" 3
		{
			r8\f a-.\< c16( d c8-.) d->\! r8 r4
			r8 gis,-.\> a4-- e--\! r4
		}
		r8 a-.\< c16( d c8-.) d->\! r8 r4
		r8 d-. cis4-- c4.--\< r8\!
		R1*4^\markup { \italic "ad lib." }
		R1*4^\markup { \italic "ad lib." }
		R1*2
	}
	
	\relative c'''
	{
		R1*8
		R1*3
		r2 \times 2/3 { e8-. r e-. r e-. r}
		a,( g f e d e f g 
		a b c d e8-.) r8 r4
		a,8( g f e f g a b 
		c2.)^\markup { \bold growl }_\markup { \dynamic p \italic sub. }\< r4\ff
	}
	
	\relative c'''
	{
		r8 a-. a16 a16( a8) d16 d8-. d16--( d8) c-- 
		a4-> r4 r2
		r8 a-. a16 a16( a8) d4.->^\markup { \bold growl } c8-. 
		a4-> \glissando r4 r2
		r8 c-. c16 c16( c8) f16 f8-. f16--( f8) e-- 
		c4-> r4 r2
		r8 d-.\< d16 d16--( d8) g16 g8-- g16--( g8) f-- 
		a2.--\ff r4
		d,4->^\markup { \italic lento } ~ d\>\glissando r2\!\fermata
		a'8->\sf r8 r4 r2
	}
	
	\bar "||"
}

