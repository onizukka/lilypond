\version "2.12.3"

\include "Baby dont go - Trompeta.ly"
\include "Baby dont go - Alto.ly"
\include "Baby dont go - Tenor.ly"

\header {
	title = "Baby don't go"
	composer = "Bodega Bodega"
}

\score {
	{
		<<
			\trompeta
			\alto
			\tenor
		>>
	}

}