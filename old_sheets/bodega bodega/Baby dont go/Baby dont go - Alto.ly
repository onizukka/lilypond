alto = \new Staff
{
	\set Staff.instrumentName = "Sax. Alto"
	\set Score.skipBars = ##t
	\override Glissando #'style = #'zigzag
	
	\key a \major
	
	\relative c''
	{
		R1*8
		e4-- e4-- e8.-- d16( e d e8-.)
		a8.-> fis16--( fis4) r2
		e4-- e4-- e8.-- d16( e d e8-.) 
		c4->^\markup { \bold growl } ~ 
		\once \override Glissando #'(bound-details right Y) = #-3
		c \glissando r2
		e4-- e4-- e8.-- d16( e d e8-.) 
		a8.->\< e16--( e8) e-. b'8.-> e,16--( e8) e-.
		cis'8.-> e,16--( e8) e-. e'4\sf\glissando r4
		cis16( b8-.) a16-- ~ a b( a8-.)
		b16( c8-.) b16-- ~ b16 a16-- ~ a8
		e8->\glissando r8 r4 r2
		R1*3
		R1*3
		r4 e-- a--, r4 
		\once \override Glissando #'(bound-details right Y) = #-3
		a'4->\glissando r4 r2
	}
	
	\relative c''
	{
		R1*7
		r1
		r2 d4-- dis-- 
		e8--> r8 r4 r2
		r2 d'4--\f\< dis--
		e8->\sf\glissando r8 r4 r2
		r1
		\once \override Glissando #'style = #'line
		g,1\sp\< \glissando fis
	}
	
	\relative c''
	{
		\repeat "percent" 2
		{
			r8\f e-.\< g16( a g8-.) a->\! r8 r4
			r1
		}
		\repeat "percent" 2
		{
			r8 e-.\< g16( a g8-.) a->\! r8 r4
			r8 e-.\> dis4-- d--\! r4
		}
		R1*8
	}
	
	\relative c''
	{
		R1*2
		\times 2/3 { d'8-. r dis-. r e-. r} r2
		r1
		R1*2
		\once \override Glissando #'style = #'line
		cis,1\mp\<\glissando c^\markup { \bold growl }
	}
	
	\relative c''
	{
		\repeat "percent" 3
		{
			r8\f e-.\< g16( a g8-.) a->\! r8 r4
			r8 e-.\> dis4-- d--\! r4
		}
		r8 e-.\< g16( a g8-.) a->\! r8 r4
		r8 bes-. a4-- g4.--\< r8\!
		R1*4^\markup { \italic "ad lib." }
		R1*4^\markup { \italic "ad lib." }
		R1*4
		R1*3
		r2 \times 2/3 { g4-- a-- a-- }
		e,4-_ g-_ a-_ c8-. d-. dis4-_ e-_ \times 2/3 { g4-- a-- c-- }
		dis( e \times 2/3 { fis g gis } a e \times 2/3 { c a e) }
		e,-_ g-_ a-_ c8-. d-. dis4-_ e-_ \times 2/3 { e'8-. e-. e-. }
		e,,4-_ g-_ a-_ c8-. d-. dis4-_ e-_ \times 2/3 { g4-- a-- a-- }
		e, g a c8 d 
		gis2. r4
	}
	
	\relative c''
	{
		r8 e e16 e16( e8) a16 a8 a16( a8) g 
		fis4 r4 r2
		r8 e e16 e16( e8) a4. g8 
		fis4 r4 r2
		r8 a a16 a16( a8) d16 d8 d16( d8) c 
		a4 r4 r2
		r8 a a16 a16( a8) b16 b8 b16( b8) a 
		c2. r4 
		a2 \glissando r2\fermata
		e'8 r8 r4 r2
	}
	
	\bar "||"
}

