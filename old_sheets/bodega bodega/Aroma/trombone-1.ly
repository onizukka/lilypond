firstTrombone= \new Staff
{
	\set Staff.midiInstrument = #"trombone"
	% \set Staff.instrumentName = "1st Trombone"
	\set Score.skipBars = ##t
	
	\relative c''
	{
		\time 4/4
	
		r4 cis8 d r4 cis8 d 	| r4 cis8 d r4 d				|
		r4 b8 g r4 g8 e			| r d fis[ e] gis4 ais 			|
		r4 cis8 d r4 cis8 d 	| r4 cis8 d r4 e				|
		r4 b8 g r4 g8 e			| r e fis[ ais] b8 a ais4 		|
		
		\bar "||"
		
		b2 b8[ b] fis r 	| r4 b b8 e, fis4 	| g2 g8[ a] e r 	| fis2. gis8 ais 	| 
		b2 b8[ b] fis r 	| r4 b b8 e, fis4 	| g2 g8[ a] e r 	| fis2. r4 			| 
		
		fis4. e8 d fis e d 	| r d fis[ e] gis4 ais 		|
		b4 d,8 e e d d e	| r d fis[ e] gis4 ais 		|
		fis4. e8 d fis e d 	| r d fis[ e] gis4 ais 		|
		b4 d,8 e e d d e	| r d fis[ e] gis4 ais 		|
		
		b1	
		
		\bar "||"
		
		R1*16
		
		\bar "||"
		
		r2. d8-> r		| r2. cis8-> r		| 
		r2. b8-> r		| r2. b8 d->		|	
		r2. d8-> r		| r2. cis8-> r		| 
		r2. b8-> r		| r2 cis4 b8 d->	|
		
		r2. d8-> r		| r2. cis8-> r			|
		r2. b8-> r		| r4 cis-> fis, b8 d->	|
		r2. d8-> r		| r2. cis8-> r			| 
		r2. b8-> r		| ais4-> fis b d8 b->	| b1	| R1*3	|
		
		R1*3 | r2 \times 2/3 {ais4 gis fis}
		
		\bar "||"
		
		b2. d4 					| cis4. d8 cis b ais fis 			|
		g d' e4 r8 ais,8 b gis 	| ais2 \times 2/3 {ais4 cis fis }	|
		d2. e4 					| fis4. d8 r8 e8 r8 fis 			|
		
		g4. e8 \times 2/3 {d4 cis b}						|
		\times 2/3 {ais4 r b} \times 2/3 {cis fis d}	|
		
		b4 r2.
	}
}

