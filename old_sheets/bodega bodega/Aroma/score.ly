\version "2.12.3"

\include "trombone-1.ly"
\include "sax-tenor-1.ly"

layoutScore =
{
	\new StaffGroup 
	<<
		\firstTenor
		\firstTrombone
	>>
}