firstTenor = \new Staff
{
	\set Staff.midiInstrument = #"tenor sax"
	% \set Staff.instrumentName = "1st Sax. Tenor"
	\set Score.skipBars = ##t
	
	\transpose bes c
	\relative c'''
	{
		\time 4/4
		
		r4 e8 fis r4 e8 fis 	| r4 e8 fis r4 fis				|
		r4 e8 g r4 b,8 g 		| r fis b[ fis] d'4 cis 		|
		r4 e8 fis r4 e8 fis 	| r4 e8 fis r4 g				|
		r4 e8 g r4 b,8 g 		| r e fis[ ais] b8 a cis4 		|
		
		\bar "||"
		
		d2 d8[ cis] b r 		| r4 d d8 cis b4 		| d2 d8[ cis] b r 	| cis2. b8 cis 	| 
		d2 d8[ cis] b r 		| r4 d d8 cis b4 		| d2 d8[ cis] b r 	| cis2. r4 		| 
		
		b4. d8 fis b, d fis 	| r fis, b[ fis] d'4 cis 		|
		g4 d'8 g, g d' g, d'	| r fis, b[ fis] d'4 cis 		|
		b4. d8 fis b, d fis 	| r fis, b[ fis] d'4 cis 		|
		g4 d'8 g, g d' g, d'	| r fis, b[ fis] d'4cis 		|
		
		d1					
		
		\bar "||"
		
		R1*16
		
		\bar "||"
		
		r2. fis8-> r	| r2. fis8-> r		| 
		r2. g8-> r		| r2. d8 fis->		|	
		r2. fis8-> r	| r2. fis8-> r		| 
		r2. g8-> r		| r2 e4 d8 fis->	|
		
		r2. fis8-> r	| r2. e8-> r			|
		r2. fis8-> r	| r4 e-> b d8 fis->		|
		r2. fis8-> r	| r2. e8-> r			| 
		r2. fis8-> r	| e4-> b d fis8 b->	~ 	| b1	| R1*3	|
		
		R1*3 | r2 \times 2/3 {cis,4 d e}
		
		\bar "||"
		
		fis2. b4 				| b4. fis8 e fis e d 			|
		a'4 g r8 cis,8 d e 		| fis2 \times 2/3 {fis4 gis ais }	|
		b2. cis4 				| d4. b8 r8 cis8 r8 d 			|
		
		e4. fis8 \times 2/3 {fis4 e d}						|
		\times 2/3 {fis,4 r gis} \times 2/3 {ais cis fis}	|
		
		b4 r2.
		
		
		
		
	}
	}
}

