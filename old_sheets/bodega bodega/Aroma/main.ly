\version "2.12.3"

\include "score.ly"

\header
{
	title = "Aroma"
}

\paper {
	#(set-default-paper-size "a4" 'landscape)
}

\score
{
	\layoutScore
	
	\layout {}
	\midi {
	  \context {
		 \Score
		 tempoWholesPerMinute = #(ly:make-moment 130 4)
	  }
	}
}