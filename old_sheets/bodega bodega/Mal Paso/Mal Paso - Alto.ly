alto = \new Staff
{
   \set Staff.midiInstrument = #"alto sax"
	\set Staff.instrumentName = "Sax. Alto"
	\set Score.skipBars = ##t
	\key g \minor
	
	\relative c'
	{
		R1*8
		d1\< ~ d1 ~ d1 ~ d1 ~ d1 ~ d1 ~ d1 ~ d1
		
		\time 6/4 
		r2\! r r8 bes'\ff( a aes 
		
		\time 4/4
		\repeat volta 4
		{
			\once \override Script #'extra-offset = #' (0 . 1)
			g1->)
			r8 g( fis g) ais8-- ais4-- fis8--
			\once \override Script #'extra-offset = #' (0 . 1.5)
			g2.-> r4
			
		}
		
		\alternative
		{
			{ 
				bes8( a g fis g) bes( a aes)
			}
			{ 
				bes8\>( a g fis g\<) a( bes c 
				\once \override Script #'extra-offset = #' (0 . 0.5)
				d8->\fff) r8 r4 r2
			}
		}
	}
	
	\relative c''
	{
		R1*8
		R1*4
		d1\< ~ d1 ~ d1 ~ d1 ~
		d8-^\! r8 r4 r2
		R1*7
	}
	
	\relative c'''
	{
		\repeat volta 3
		{
			bes1->\ff
			a1->
		}
		
		\alternative
		{
			{ 
				bes8-^ r8 r4 r2
				r1
			}
			{ 
				bes8-^ r8 r4 r2
				r1
			}
		}
		bes1-> 
		a1->
	}
	
	\relative c'''
	{
		\repeat "percent" 2 
		{ 
			bes8^\markup { \italic staccato } g d g fis g16[( a8) a16]( fis8)
			g-> r r4 r2
		}
		
		bes8 g d g f f16[( a8) a16]( f8)
		bes8 g d g f g16[( a8) a16]( f8)
		bes8 g d g f f16[( a8) a16]( f8)
		bes8 g d g fis g16[( a8) a16]( fis8)
		d'->\!_\markup { \bold \italic sf } r r4 r2
		
		R1*7
	}
	
	\relative c'''
	{
		\repeat volta 3
		{
			bes1->\ff
			a1->
		}
		
		\alternative
		{
			{ r1 r1 }
			{ s1^"solo" s1 }
			{ r1 r1 }
		}
	}
	
	\relative c'''
	{
		bes1->
		a1-> ~ 
		\time 12/8
		a4.\mf^"lento"-\bendAfter #-8 r4. r2.
		r4. r4 g,8 a4.( a8) g fis
		\repeat "percent" 2
		{
			g8-_ r4 d8-_ r g\( bes4.( bes8) a aes \)
			g8-_ r4 d8-_ r g\( a4.( a8) g fis\)
		}
		g8-_ r4 d8-_ r g\( bes4.( bes8) a aes \)
		g8-_ r4 d8-_ r g( c2.)\fermata
	}
	
	\relative c'''
	{
		\time 4/4
		bes1->^\markup { \italic tempo 1 }\ff
		a->
		r1 r
		bes->
		a->
		s^"solo" s
		bes->
		a8-^ r8 r4 r2
		r1 r
		bes-> a->\<
		bes->\! a->
		bes->\<
		c2->^\markup { \italic rall. } c2->
		d2.->\fermata -\bendAfter #-8 \ff r4
	}
	
	\bar "||"
}

