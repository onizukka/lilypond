\version "2.12.3"

\include "Mal Paso - Trompeta.ly"
\include "Mal Paso - Trombon.ly"
\include "Mal Paso - Alto.ly"
\include "Mal Paso - Tenor.ly"
\include "Mal Paso - Bajo.ly"

\header {
	title = "Mal Paso"
	composer = "Bodega Bodega"
}

\score {
	<<
		\new ChoirStaff
		<<
         \tempo 4 = 150
			\trompeta
			\trombon
			\alto
			\tenor
		>>
		\relative
		<<
			\bajo
		>>
	>>
   \midi {}
}