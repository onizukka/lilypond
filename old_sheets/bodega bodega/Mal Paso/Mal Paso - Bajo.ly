introUnoBajo = \relative c
{		
	\repeat "unfold" 4
	{
		g4 
		\repeat "tremolo" 6 g8
		g ees' \repeat "tremolo" 4 ees d bes
		\repeat "tremolo" 8 g8
		\times 2/3 {ees'4 ees ees} \times 2/3 {ees ees d}
	}
}

introDosBajo = \relative c
{	
	\repeat volta 4
	{
		g4. d'8 fis4. g8
		fis4. ees8 d bes a bes
		g4 d' fis g
	}
	\alternative
	{
		{ 
			fis g \times 2/3 { a a a }
		}
		{ 
			fis4 g r8 d d d
		}
	}
	g,8-> r r4 r2
}

introTresBajo = \relative c
{
	g4. d'8 fis4. g8
	fis4. ees8 d ees d bes
	g4. d'8 fis4 g
	fis g \times 2/3 { a a a }
	
	g,4. d'8 fis4. g8
	fis4. ees8 d bes a bes
	g4. d'8 fis4 g
	fis g \times 2/3 { a a a }
}

versoBajo = \relative c
{	
	g4 d' fis g
	fis ees d bes8 a
	g4 d' fis g
	fis g a a
	
	g,4 d' fis g
	fis ees d8 bes a bes
	g4 d' fis g
	fis g \times 2/3 { a a a }
}

estribilloUnoBajo = \relative c
{
	\repeat volta 3
	{
		ees4. ees8 g4 ees
		c4. c8 g'4 c,
	}
	\alternative
	{
		{
			g2. ~ g8 g8
			\times 2/3 { bes4 bes bes } \times 2/3 {d bes d}
		}
		{
			g,2. ~ g8 g8
			\times 2/3 { bes4 bes bes } \times 2/3 {d bes d}
		}
	}
	ees4. ees8 g4 ees
	c2 g'4 c,
}

estribilloDosBajo = \relative c
{
	\repeat volta 3
	{
		ees4. ees8 g4 ees
		c4. c8 g'4 c,
	}
	\alternative
	{
		{
			g2. ~ g8 g8
			\times 2/3 { bes4 bes bes } \times 2/3 {d bes d}
		}
		{
			g,2. ~ g8 g8
			\times 2/3 { bes4 bes bes } \times 2/3 {d bes d}
		}
		{
			g,2. ~ g8 g8
			\times 2/3 { bes4 bes bes } \times 2/3 {d bes d}
		}
	}
	ees4. ees8 g4 ees
	c2 g'4 c,
}

estribilloTresBajo = \relative c
{
	\repeat volta 3
	{
		ees4 g ees g8 ees
		c4 g' c, g'8 c,
	}
	\alternative
	{
		{
			g4 d' g, d'8 g,
			\times 2/3 { bes4 bes bes } \times 2/3 {d bes d}
		}
		{
			g,4 d' g, d'8 g,
			\times 2/3 { bes4 bes bes } \times 2/3 {d bes d}
		}
		{
			g,4 d' g, d'8 g,
			\times 2/3 { bes4 bes bes } \times 2/3 {d bes d}
		}
	}
	ees4. ees8 g4 ees
	c2 g'4 c,
	ees4. ees8 g4 ees
	c2 g'4 c,
	ees4. ees8 g4 ees8 d
	c4 r4 c4 r4
	g1\fermata
}

puenteUnoBajo = \relative c
{
	g8-> r8 r4 r2
	r2 r4 r8 f
	g8-> r8 r4 r2
	r8 ees' d ees d bes a bes
	g8-> r8 r4 r2
	r2 r4 r8 f
	g8-> r8 r4 r2
	r8 ees' d ees d bes a bes
}

puenteDosBajo = \relative c
{
	\time 12/8
	\repeat unfold 7
	{
		g8 d' d g, d' d ees bes' bes d, a' a
	}
	g,8 d' d g, d' d 
	ees2.\fermata
}

bajo = \new Staff
{
   \set Staff.midiInstrument = #"electric bass (finger)"
	\set Staff.instrumentName = "Bajo"
	\set Score.skipBars = ##t
	\clef bass
	\key g \minor
	
	\relative c
	{
		\introUnoBajo	
		
		\time 6/4 
		g8-> r8 r4 r2 r2
		\time 4/4
		
		\introDosBajo
		
		\versoBajo
		
		\introTresBajo
		
		\versoBajo
		
		\estribilloUnoBajo
		
		\puenteUnoBajo
		
		\versoBajo
		
		\estribilloDosBajo
		
		\puenteDosBajo
		
		\estribilloTresBajo
	}
	
	\bar "||"
}

