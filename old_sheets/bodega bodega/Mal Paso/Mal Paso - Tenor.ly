tenor = \new Staff
{
   \set Staff.midiInstrument = #"tenor sax"
	\set Staff.instrumentName = "Sax. Tenor"
	\set Score.skipBars = ##t
	\key g \minor
	
	\relative c'
	{
		R1*7 r2 r4 r8 g\mf(
		c2 f4 d ~ d2.) r8 g,(
		c2 f4 c ~ c2.) r8 f,(
		g2 a4 f ~ f1) ~
		f1\< ~ f
		
		\time 6/4 
		r2\! r r8 d\ff( cis c 
		
		\time 4/4
		\repeat volta 4
		{
			g'1->)
			r8 g( fis g) cis8-- cis4-- a8--
			g2.-> r4
			
		}
		
		\alternative
		{
			{ 
				bes8( a g gis a) d,( cis c)
			}
			{ 
				bes'8\>( a g fis g\<) d( cis c 
				g'8->\fff) r8 r4 r2
			}
		}
	}
	
	\relative c''
	{
		R1*8
		R1*4 
		g1\< ~ g1 ~ g1 ~ g1 ~
		g8-^\! s8^"solo" s4 s2 s1 s1 s1
		s1 s1 s1 s1
	}
	
	\relative c'''
	{
		\repeat volta 3
		{
			g1->\ff
			f->
		}
		
		\alternative
		{
			{ 
				g8-^ r8 r4 r2
				r1
			}
			{ 
				g8-^ r8 r4 r2
				r1
			}
		}
		d->\ff
		c->
	}
	
	\relative c''
	{
		\repeat "percent" 2 
		{ 
			d8^\markup { \italic staccato } bes g bes a bes16[( c8) c16]( a8)
			bes-> r r4 r2
		}
		
		d8 bes g bes a a16[( c8) c16]( a8)
		d8 bes g bes a bes16[( c8) c16]( a8)
		d8 bes g bes a a16[( c8) c16]( a8)
		d8 bes g bes a bes16[( c8) c16]( a8)
		g'->\!_\markup { \bold \italic sf } r r4 r2
		
		R1*7
	}
	
	\relative c'''
	{
		\repeat volta 3
		{
			g1->\ff
			f1->
		}
		
		\alternative
		{
			{ r1 r1 }
			{ r1 r1 }
			{ s1^"solo" s1 }
		}
	}
	
	\relative c'''
	{
		bes1->
		g1-> ~ 
		\time 12/8 
		f4.\mf^"lento"-\bendAfter #-8 bes,,8-_ r d( ees2.)
		d8-_ r4 bes8-_ r cis( d2.)
		\repeat "percent" 2
		{
			d8-_ r4 bes8-_ r d( ees2.)
			d8-_ r4 bes8-_ r cis( d2.)
		}
		d8-_ r4 bes8-_ r d( ees2.)
		d8-_ r4 bes8-_ r d( g2.)\fermata
	}
	
	\relative c'''
	{
		\time 4/4
		g1->^\markup { \italic tempo 1 }\ff
		f->
		r r
		g->
		f->
		r r
		g->
		f8-^ r8 r4 r2
		s1^"solo" s1
		d->
		c->\<
		g'->\!
		bes->
		bes->\<
		c2->^\markup { \italic rall. }
		r_"coletilla"
		g2.->\fermata -\bendAfter #-8 \ff r4
	}
	
	\bar "||"
}

