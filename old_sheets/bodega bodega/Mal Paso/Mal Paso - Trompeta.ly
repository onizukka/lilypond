trompeta = \new Staff
{
   \set Staff.midiInstrument = #"brass section"
	\set Staff.instrumentName = "Trompeta"
	\set Score.skipBars = ##t
	\key g \minor
	
	\relative c''
	{
		R1*7 r2 r4 r8 a\mf(
		\repeat "percent" 3 { bes2 d4 bes ~ bes2.) r8 a }
		bes1\< ~ bes
		
		\time 6/4 
		r2\! r r8 d\ff( cis d 
		
		\time 4/4
		\repeat volta 4
		{
			\once \override Script #'extra-offset = #' (0 . 1)
			bes1->)
			r8 bes( a g) cis8-- cis4-- a8--
			\once \override Script #'extra-offset = #' (0 . 1.5)
			bes2.-> r4
		}
		
		\alternative
		{
			{ 
				d8( c bes b c) d( cis d)
			}
			{ 
				d8\>( c bes b c\<) d( cis d 
				\once \override Script #'extra-offset = #' (0 . 0.5)
				bes->\fff) r8 r4 r2
			}
		}
	}
	
	\relative c'''
	{
		R1*8
		R1*3 r2 r4 r8 a\mf(
		bes2 d4 bes ~ bes2.) r8 a(
		bes1)\< ~ bes1 ~
		bes8-^\! r8 r4 r2
		R1*7
	}
	
	\relative c'''
	{
		\repeat volta 3
		{
			d1->\ff
			c1->
		}
		
		\alternative
		{
			{ 
				d8-^ r8 r4 r2
				r1
			}
			{ 
				d8-^ r8 c,\mf\<( d ees f g) r
				d\<( e fis g a bes c a 
			}
		}
		d1->\ff)
		c1->
	}
	
	\relative c'''
	{
		\repeat "percent" 2 
		{ 
			d8^\markup { \italic staccato } bes g bes a bes16[( c8) c16]( a8)
			bes-> r r4 r2
		}
		
		d8 bes g bes a a16[( c8) c16]( a8)
		d8 bes g bes a bes16[( c8) c16]( a8)
		d8 bes g bes a a16[( c8) c16]( a8)
		d8 bes g bes a bes16[( c8) c16]( a8)
		g'->\!_\markup { \bold \italic sf } r r4 r2
		
		R1*7
	}
	
	\relative c'''
	{
		\repeat volta 3
		{
			d1->\ff
			c1->
		}
		
		\alternative
		{
			{ s1^"solo" s1 }
			{ r1 r1 }
			{ r1 r1 }
		}
	}
	
	\relative c'''
	{
		d1->
		c1-> ~ 
		\time 12/8
		c4.\mf^"lento"-\bendAfter #-8 r4. r2.
		r1.
		r2. r4. r8 c_\markup { \italic express. }( a bes4. g4. ees2.\>)
		R1.*3\!
		r2. r2.\fermata
	}
	
	\relative c'''
	{
		\time 4/4
		d1->^\markup { \italic tempo 1 }\ff
		c->
		s^"solo" s1
		g'->
		c,->
		r r
		d->
		c8-^ r8 r4 r2
		r1 r
		d-> c->\<
		r\! c->
		d->\<
		e2->^\markup { \italic rall. }
		fis->
		g2.->\fermata -\bendAfter #-8 \ff r4
	}
	
	\bar "||"
}

