trombon = \new Staff
{
   \set Staff.midiInstrument = #"trombone"
	\set Staff.instrumentName = "Trombon"
	\set Score.skipBars = ##t
	\key g \minor
	
	\relative c'
	{
		R1*8
		d1\< ~ d1 ~ d1 ~ d2. r8 a8(
		bes2 d4 bes ~ bes1) ~
		bes1\! ~ bes
		
		\time 6/4 
		r2\! r r8 d\ff( cis d 
		
		\time 4/4
		\repeat volta 4
		{
			\once \override Script #'extra-offset = #' (0 . 1)
			bes1->)
			r8 bes( a g) f'8-- f4-- cis8--
			\once \override Script #'extra-offset = #' (0 . 1.5)
			d2.-> r4
		}
		
		\alternative
		{
			{ 
				d8( c bes b c) d( cis d)
			}
			{ 
				d8\>( c bes b c\<) d( cis d 
				\once \override Script #'extra-offset = #' (0 . 0.5)
				g->\fff) r8 r4 r2
			}
		}
	}
	
	\relative c''
	{
		R1*8
		R1*4
		d1\< ~ d1 ~ d1 ~ d1 ~
		d8-^\! r8 r4 r2
		R1*7
	}
	
	\relative c''
	{
		\repeat volta 3
		{
			d1->\ff
			c1->
		}
		
		\alternative
		{
			{ 
				d8-^ r8 r4 r2
				r1
			}
			{ 
				d8-^ r8 r4 r2
				r1
			}
		}
		d1->\ff	
		c1->
	}
	
	\relative c''
	{
		\repeat "percent" 2 
		{ 
			d8^\markup { \italic staccato } bes g bes a bes16[( c8) c16]( a8)
			bes-> r r4 r2
		}
		
		d8 bes g bes a a16[( c8) c16]( a8)
		d8 bes g bes a bes16[( c8) c16]( a8)
		d8 bes g bes a a16[( c8) c16]( a8)
		d8 bes g bes a bes16[( c8) c16]( a8)
		g->\!_\markup { \bold \italic sf } r r4 r2
		
		R1*7
	}
	
	\relative c''
	{
		\repeat volta 3
		{
			d1->\ff
			c1->
		}
		
		\alternative
		{
			{ s1^"solo" s1}
			{ r1 r1 }
			{ r1 r1 }
		}
	}
	
	\relative c''
	{
		d1->
		c1-> ~ 
		\time 12/8
		c4.\mf^"lento"-\bendAfter #-8 r4. r2.
		r1.
		r2. r4. r8 ees_\markup { \italic express. }( c d4. bes4. c2.\>)
		R1.*3\!
		r2. r2.\fermata
	}
	
	\relative c''
	{
		\time 4/4
		d1->^\markup { \italic tempo 1 }\ff
		c->
		s^"solo" s1
		d->
		c->
		r r
		d->
		c8-^ r8 r4 r2
		r1 r
		d-> c->\<
		r1\! e->
		d->\<
		c2->^\markup { \italic rall. }
		c->
		g2.->\fermata -\bendAfter #-8 \ff r4
	}
	
	\bar "||"
}

