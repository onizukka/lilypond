trompeta = \new Staff
{
	\set Staff.instrumentName = "Trompeta"
	\set Score.skipBars = ##t
	\override Glissando #'style = #'zigzag
	
	\key g \minor
	
	\relative c'''
	{
		R1*7 r2 r4 r8 a\mf(
		\repeat "percent" 3 { bes2 d4 bes ~ bes2.) r8 a }
		bes1\< ~ bes
		
		\time 6/4 
		r2\! r r8 d\ff( cis d 
		
		\time 4/4
		\repeat volta 4
		{
			\once \override Script #'extra-offset = #' (0 . 1)
			bes1->)
			r8 bes( a g) cis8.-- cis16-- ~ cis8 a8--
			\once \override Script #'extra-offset = #' (0 . 1.5)
			bes2.-> r4
			
		}
		
		\alternative
		{
			{ 
				d8( c bes b c) d( cis d)
			}
			{ 
				d8\>( c bes b c\<) b( c cis 
				\once \override Script #'extra-offset = #' (0 . 0.5)
				d8->\sf) r8 r4 r2
			}
		}
	}
	
	\relative c'''
	{
		R1*8
		R1*3 r2 r4 r8 a\mf(
		bes2 d4 bes ~ bes2.) r8 a(
		bes1) ~ bes2 r
		R1*8
	}
	
	\relative c'''
	{
		\repeat volta 3
		{
			d1->\ff
			c1->
		}
		
		\alternative
		{
			{ 
				d8-^ r8 r4 r2
			}
			{ 
				d8-^ r8 c,\mf\<( d ees f g) r
				d\<( e fis g a bes c a 
				d1->\ff)
				c1->
			}
		}
	}
	
	\relative c'''
	{
		\repeat "percent" 2 
		{ 
			d8^\markup { \italic staccato } bes g bes a[ bes16 c] ~ c[ c16 a8]
			bes-> r r4 r2
		}
		
		\repeat "percent" 3 
		{ d8 bes g bes a[ a16 c] ~ c[ c a8]
		}
		
		d bes g bes a\<[ a16 c] ~ c[ c d8-^]
		g->\!_\sf r r4 r2
		
		R1*8
	}
	
	\relative c'''
	{
		\repeat volta 3
		{
			d1->\ff
			c1->
		}
		
		\alternative
		{
			{ s1^"solo" }
			{ r1 }
			{ r1 }
		}
	}
	
	\relative c'''
	{
		d1->
		c1-> ~ 
		\time 12/8
		\once \override Glissando #'(bound-details right Y) = #-2
		c4.\mf^"lento" \glissando r4. r2.
		r1.
		r2. r4. r8 c_\markup { \italic express. }( a bes4. g4. d2.\>)
		R1.*3\!
		r2. r2.\fermata
	}
	
	\relative c'''
	{
		\time 4/4
		d1->^\markup { \italic tempo 1 }\ff
		c->
		s^"solo"
		g'->
		c,->
		r
		d->
		c4-- r4 r2
		r1 
		\repeat "percent" 2 { d->\< c->\!}
		d->\<
		e2->^\markup { \italic rall. }
		fis->
		g2.->\fermata -\bendAfter #-8 \ff r4
	}
	
	\bar "||"
}

