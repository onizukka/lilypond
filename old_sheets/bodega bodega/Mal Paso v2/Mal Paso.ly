\version "2.12.3"

\include "Mal Paso - Trompeta.ly"
\include "Mal Paso - Alto.ly"
\include "Mal Paso - Tenor.ly"

\header {
	title = "Mal Paso"
	composer = "Bodega Bodega"
}

\score {
	{
		<<
			\trompeta
			\alto
			\tenor
		>>
	}
}