alto = \new Staff
{
	\set Staff.instrumentName = "Sax. Alto"
	\set Score.skipBars = ##t
	\override Glissando #'style = #'zigzag
	
	\key g \minor
	
	\relative c''
	{
		R1*8
		d1\< ~ d1 ~ d1 ~ d1 ~ d1 ~ d1 ~ d1 ~ d1
		
		\time 6/4 
		r2\! r r8 bes'\ff( a aes 
		
		\time 4/4
		\repeat volta 4
		{
			\once \override Script #'extra-offset = #' (0 . 1)
			g1->)
			r8 g( fis g) bes8.-- bes16-- ~ bes8 fis8--
			\once \override Script #'extra-offset = #' (0 . 1.5)
			g2.-> r4
			
		}
		
		\alternative
		{
			{ 
				bes8( a g fis g) bes( a aes)
			}
			{ 
				bes8\>( a g fis g\<) fis( g a 
				\once \override Script #'extra-offset = #' (0 . 0.5)
				bes8->\sf) r8 r4 r2
			}
		}
	}
	
	\relative c''
	{
		R1*8
		R1*4
		d1 ~ d1 ~ d1 ~ d1
		R1*6
		r2 r4 r16 a'( d bes,
		a bes a bes a bes) bes'8-^ f4.-- d16( bes)
	}
	
	\relative c'''
	{
		\repeat volta 3
		{
			bes1->\ff
			a1->
		}
		
		\alternative
		{
			{ 
				bes8-^ r8 r4 r2
			}
			{ 
				bes8-^ r8 r4 r2
				r1
				bes1-> 
				a1->
			}
		}
	}
	
	\relative c'''
	{
		\repeat "percent" 2 
		{ 
			bes8^\markup { \italic staccato } g d g fis[ g16 a] ~ a[ a fis8]
			g-> r r4 r2
		}
		
		\repeat "percent" 3 
		{ 
			bes8 g d g f[ f16 a] ~ a[ a f8]
		}
		
		bes g d g fis\<[ g16 a] ~ a[ a fis8-^]
		d'->\!\sf r r4 r2
		
		R1*8
	}
	
	\relative c'''
	{
		\repeat volta 3
		{
			bes1->\ff
			a1->
		}
		
		\alternative
		{
			{ r1 }
			{ s1^"solo" }
			{ r1 }
		}
	}
	
	\relative c'''
	{
		bes1->
		a1-> ~ 
		\time 12/8
		\once \override Glissando #'(bound-details right Y) = #-2
		a4.\mf^"lento" \glissando r4. r2.
		r4. r4 g,8 a4.( a8) g fis
		\repeat "percent" 2
		{
			g8-_ r4 d8-_ r g\( bes4.( bes8) a aes \)
			g8-_ r4 d8-_ r g\( a4.( a8) g fis\)
		}
		g8-_ r4 d8-_ r g\( bes4.( bes8) a aes \)
		g8-_ r4 d8-_ r g( bes2.)\fermata
	}
	
	\relative c'''
	{
		\time 4/4 
		bes1->^\markup { \italic tempo 1 }\ff
		a->
		r1
		bes->
		a->
		s^"solo"
		bes->
		a4-- r4 r2
		r1 
		\repeat "percent" 2 { bes->\< a->\!}
		bes->\<
		c2->^\markup { \italic rall. } c2->
		d2.->\fermata -\bendAfter #-8 \ff r4
	}
	
	\bar "||"
}

