tenor = \new Staff
{
	\set Staff.instrumentName = "Sax. Tenor"
	\set Score.skipBars = ##t
	\override Glissando #'style = #'zigzag
	
	\key g \minor
	
	\relative c''
	{
		R1*13 r2 r4 g' \glissando 
		bes,,1\< ~ bes
		
		\time 6/4 
		r2\! r r8 d\ff( cis c 
		
		\time 4/4
		\repeat volta 4
		{
			g'1->)
			r8 g( fis f) cis'8.-- cis16-- ~ cis8 a8--
			g2.-> r4
			
		}
		
		\alternative
		{
			{ 
				bes8( a g gis a) d,( cis c)
			}
			{ 
				bes'8\>( a g gis a\<) d,( cis c 
				g'8->\sf) r8 r4 r2
			}
		}
	}
	
	\relative c''
	{
		R1*8
		R1*4 
		g1 ~ g1 
		bes,1 ~ bes2 r2
		s1^"solo"
		s1
		s1
		s1
		s1
		s1
		s2 s4 s16 f''( a d,
		cis d cis d cis d) d'8-^ a4.-- f16( d)
	}
	
	\relative c'''
	{
		\repeat volta 3
		{
			g1->\ff
			f->
		}
		
		
		\alternative
		{
			{ 
				g8-^ r8 r4 r2
			}
			{ 
				g8-^ r8 r4 r2
				r1
				g->
				f->
			}
		}
	}
	
	\relative c''
	{
		\repeat "percent" 2 
		{ 
			d8^\markup { \italic staccato } bes g bes a[ bes16 c] ~ c[ c a8]
			bes-> r r4 r2
		}
		
		\repeat "percent" 3 
		{ d8 bes g bes a[ a16 c] ~ c[ c a8]
		}
		
		d bes g bes a\<[ a16 c] ~ c[ c d8-^]
		d->\!_\sf r r4 r2
		
		R1*8
	}
	
	\relative c'''
	{
		\repeat volta 3
		{
			g1->\ff
			f1->
		}
		
		\alternative
		{
			{ r1 }
			{ r1 }
			{ s1^"solo" }
		}
	}
	
	\relative c'''
	{
		g1->
		f1-> ~ 
		\time 12/8 
		f4.\mf^"lento" \glissando bes,,8-_ r d( ees2.)
		d8-_ r4 bes8-_ r cis( d2.)
		\repeat "percent" 2
		{
			d8-_ r4 bes8-_ r d( ees2.)
			d8-_ r4 bes8-_ r cis( d2.)
		}
		d8-_ r4 bes8-_ r d( ees2.)
		d8-_ r4 bes8-_ r d( g2.)\fermata
	}
	
	\relative c'''
	{
		\time 4/4
		g1->^\markup { \italic tempo 1 }\ff
		f->
		r
		g->
		f->
		r
		g->
		f4-- r4 r2
		s1^"solo"
		d->\<
		c->\>
		f->\<
		d->\>
		g->\<
		g2->^\markup { \italic rall. }
		g-> \glissando
		d2.->\fermata -\bendAfter #-8 \ff r4
	}
	
	\bar "||"
}

