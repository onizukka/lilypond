
\header {
	title = "Magnifique"
}

\score {
	{
		<<
		
			\new Staff
			{
				\set Score.skipBars = ##t
			
				\relative c''
				{
					c8 d ees c16 d ~ d ees ~ ees c d8 ees^"    ..." R1*3^"4 veces"
					R1^"las bajadas"
				}
			}
		>>
	}

}