
\header {
	title = "Somos"
}

\score {
	{
		<<
		
			\new Staff
			{
				\set Score.skipBars = ##t
			
				\relative c''
				{
					R1^"bateria ad lib."
					e8 ~ e16 e16 ~ e8 e8 ~ e16 e16 e8 g a
					e8 ~ e16 e16 ~ e8 e8 ~ e16 e16 e8 c' b
					e,8 ~ e16 e16 ~ e8 e8 ~ e16 e16 e8 g a
					e8 ~ e16 e16 ~ e8 e8 ~ b'16 b8 a16 a8 g
					e4 r4 r2
					R1*4
					R1*4^"solo"
				}
			}
		>>
	}

}
