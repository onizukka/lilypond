\version "2.18.2"
\language "english"

% Se debe incluir antes que lilyjazz. Si no, lilyjazz no funciona.
#(set-global-staff-size 19)

\include "main.ily"

\paper {
	indent = 10
}

\header {
	title = "September"
	instrument = "Trumpet, Tenor Sax & Trombone"
	composer = "Earth, Wind & Fire"
	arranger = "Transc: gabriel@sauros.es"
	poet = ""
	tagline = "Edition: gabriel@sauros.es"
}

% Función musical global
global = {
	% Para pintar todos los números de compás debajo del pentagrama
	% \override Score.BarNumber #'break-visibility = #end-of-line-invisible
	% \override Score.BarNumber #'direction = #DOWN
	% \override Score.BarNumber #'extra-offset = #'(1.5 . 0)
	\compressFullBarRests

	\set Timing.beamExceptions = #'()
	\set Timing.baseMoment = #(ly:make-moment 1/4)
	\set Timing.beatStructure = #'(1 1 1 1) 

  \key a \major
}

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
	% Oculta el primer pentagrama del markup
	\override Staff.VerticalAxisGroup #'remove-first = ##t

	% Indicación de tiempo
	% \once \override Score.RehearsalMark.self-alignment-X = #LEFT
	% \once \override Score.RehearsalMark.extra-offset = #'(0 . 3) 
	% \mark \jazzTempoMarkup "Easy Swing" c4 "112"

  s1*32
  \mkup "(Emphazise bending)"

  s1*10
  \mkup "(Tacet 1st x)"
  s1*6
  \mkup "(Play both xs)"
}

% Cifrados
scoreChords = \chords {
	% c1:m f2:m g2:m7
}

% Música
trumpet = \relative c'' {
	\global

  R1*6
  r4 \f c16 c b a b-> a ~ a8 ~ a4 ~
  a2. r8 a-> ~
  a1 ~
  a4 \> r \! a->  r

  % Estrofa
  \break
  R1*5
  r2 r4 r8 \f a-> ~
  a1 ~
  a2 a4-^ a-> \bendAfter #-3

  % Estrofa
  R1*8

  % Estribillo
  \break
  \repeat percent 3 {
    r8 \mf a---> r4 r8 gs---> r4
    R1
  }
  r8 \f a-^ g \bendBefore a-. r8 a-^ g \bendBefore a-. 
  r8 a-^ g \bendBefore a-. a-^ a-^ a-^ r

  % Puente
  R1*6
  a16 \f a c cs e e fs e a-> a fs cs e e fs e 
  a-> a fs e a-> a fs e a-> a fs e a-> a fs e

  % Estrofa
  \pageBreak
  \bar "[|:"
  \repeat volta 2 {
    \repeat percent 2 {
      r8 \mf a,-^ r4 r2
      R1
    }
    r8 a-^ r4 r2
    b4. \f cs8-> ~ cs4 r4
    a16 a c cs e e fs e a-> a fs cs e e fs e 
    a-> a fs e a-> a fs e a-> a fs e a-> a fs e
  }
  \bar ":|]"

  \break
  \repeat percent 3 {
    r8 \mf a,---> r4 r8 gs---> r4
    r1
  }
  r8 \f a-^ g \bendBefore a-. r8 a-^ g \bendBefore a-. 
  r8 a-^ g \bendBefore a-. r8 a-^ g \bendBefore a-. 

  \repeat percent 3 {
    r8 a---> r4 r8 gs---> r4
    a4-> fs8( e-.) a4-> fs8( e-.) 
  }
  a8-. a4---> a8-. a4---> a8-. a--->  ~
  a a8-. a4---> a8-^ a8-^ a4-> \bendAfter #-5

  \repeat percent 2 {
    r8 \mf a---> r4 r8 gs---> r4
    a4-> fs8( e-.) a4-> fs8( e-.) 
  }
  r8 a'---> r4 r8 gs---> r4
  a4-> fs8( e-.) a4-> fs8( e-.) 
  a8-. \f a4---> a8-. a4---> a8-. a--->  ~
  a a8-. a4---> a8-^ a8-^ a4-> \bendAfter #-5


  % Estribillo
  \break
  \bar "[|:"
  \repeat volta 2 {
    \repeat percent 3 {
      r8 a---> r4 r8 gs---> r4
      a4-> fs8( e-.) a4-> fs8( e-.) 
    }
    a8-. a4---> a8-. a4---> a8-. a--->  ~
    a a8-. a4---> a8-^ a8-^ a4->
  }
  \bar ":|][|:"
  \repeat volta 2 {
    \repeat percent 3 {
      cs,8-. cs4---> b8-. b4---> b8-. b---> ~
      b b8-. b4---> a8-^ a8-^ a4-^
    }
    a8-. a4---> a8-. a4---> a8-. a--->  ~
    a a8-. a4---> a8-^ a8-^ a4->
  }
  \bar ":|]"
}

tenorSax = \relative c'' {
	\global

  R1*5
  r2 r4 \f c16 c b a 
  b-> a ~ a8 ~ a2. ~
  a2. r8 fs'-> ~
  fs1 ~
  fs4 \> r \! fs->  r

  % Estrofa
  R1*6
  r4 r8 \f d16 e fs2-> ~
  fs2 fs4-^ fs-> \bendAfter #-3

  % Estrofa
  R1*8

  % Estribillo
  \repeat percent 3 {
    r8 \mf fs---> r4 r8 e---> r4
    a4-> fs8( e-.) a4-> fs8( e-.) 
  }
  r8 \f a-^ g \bendBefore a-. r8 a-^ g \bendBefore a-. 
  r8 a-^ g \bendBefore a-. a-^ a-^ a-^ r

  % Puente
  \repeat percent 2 {
    a,8 \mf a a gs-> r fs-> r e-> ~
    e4. fs8-> ~ fs4 r
  }
  a8 a a gs-> r fs-> r es-> ~
  es4. fs8-> ~ fs4 r
  a16 a c cs e e fs e a-> a fs cs e e fs e 
  a-> a fs e a-> a fs e a-> a fs e a-> a fs e


  % Estrofa
  \repeat volta 2 {
    % Estrofa
    \repeat percent 2 {
      r8 \mf fs-^ r4 cs8( e-.) fs8( e-.)
      a4-> fs8( e-.) a4-> fs8( e-.)
    }
    r8 fs-^ r4 r2
    gs4. \f a8-> ~ a4 r4
    a,16 a c cs e e fs e a-> a fs cs e e fs e 
    a-> a fs e a-> a fs e a-> a fs e a-> a fs e
  }

  \repeat percent 3 {
    r8 \mf fs---> r4 r8 e---> r4
    a4-> fs8( e-.) a4-> fs8( e-.) 
  }
  r8 \f a-^ g \bendBefore a-. r8 a-^ g \bendBefore a-. 
  r8 a-^ g \bendBefore a-. r8 a-^ g \bendBefore a-. 

  \repeat percent 3 {
    r8 fs---> r4 r8 e---> r4
    a4-> fs8( e-.) a4-> fs8( e-.) 
  }
  fs8-. fs4---> fs8-. fs4---> fs8-. fs---> ~
  fs fs8-. fs4---> fs8-^ fs8-^ fs4-> \bendAfter #-5

  \repeat percent 3 {
    r8 \mf fs---> r4 r8 e---> r4
    a4-> fs8( e-.) a4-> fs8( e-.) 
  }
  fs8-. \f fs4---> fs8-. fs4---> fs8-. fs---> ~
  fs fs8-. fs4---> fs8-^ fs8-^ fs4-> \bendAfter #-5


  % Estribillo
  \bar "[|:"
  \repeat volta 2 {
    \repeat percent 3 {
      r8 fs---> r4 r8 e---> r4
      a4-> fs8( e-.) a4-> fs8( e-.) 
    }
    fs8-. fs4---> fs8-. fs4---> fs8-. fs---> ~
    fs fs8-. fs4---> fs8-^ fs8-^ fs4->
  }
  \bar ":|][|:"
  \repeat volta 2 {
    \repeat percent 3 {
      a8-. a4---> gs8-. gs4---> gs8-. gs---> ~
      gs gs8-. gs4---> fs8-^ fs8-^ fs4-^
    }
    fs8-. fs4---> fs8-. fs4---> fs8-. fs---> ~
    fs fs8-. fs4---> fs8-^ fs8-^ fs4->
  }
  \bar ":|]"
}

trombone = \relative c' {
	\global
  \clef bass

  R1*5
  r2 r4 \f c16 c b a 
  b-> a ~ a8 ~ a2 c16 c b a 
  b16 a ~ a8 ~ a2 r8 d-> ~
  d1 ~
  d4 \> r \! d->  r

  % Estrofa
  R1*5
  r2 r4 r8 b16 cs 
  d1 ~
  d2 d4-^ d-> \bendAfter #-3

  % Estrofa
  R1*8

  % Estribillo
  \repeat percent 3 {
    r8 \mf cs---> r4 r8 b---> r4
    a4-> fs8( e-.) a4-> fs8( e-.) 
  }
  r8 \f a-^ g \bendBefore a-. r8 a-^ g \bendBefore a-. 
  r8 a-^ g \bendBefore a-. a-^ a-^ a-^ r

  % Puente
  \repeat percent 2 {
    cs8 \mf cs cs b-> r a-> r b-> ~
    b4. a8-> ~ a4 r
  }
  cs8 \mf cs cs b-> r a-> r b-> ~
  b4. a8-> ~ a4 r
  a16 \f a c cs e e fs e a-> a fs cs e e fs e 
  a-> a fs e a-> a fs e a-> a fs e a-> a fs e

  % Estrofa
  \repeat volta 2 {
    \repeat percent 2 {
      r8 \mf cs-^ r4 cs8( e-.) fs8( e-.)
      a4-> fs8( e-.) a4-> fs8( e-.)
    }
    r8 cs-^ r4 r2
    es4. \f fs8-> ~ fs4 r4
    a,16 \f a c cs e e fs e a-> a fs cs e e fs e 
    a-> a fs e a-> a fs e a-> a fs e a-> a fs e
  }

  \repeat percent 3 {
    r8 \mf cs---> r4 r8 b---> r4
    a4-> fs8( e-.) a4-> fs8( e-.) 
  }
  r8 \f a-^ g \bendBefore a-. r8 a-^ g \bendBefore a-. 
  r8 a-^ g \bendBefore a-. r8 a-^ g \bendBefore a-. 

  \repeat percent 3 {
    r8 cs---> r4 r8 b---> r4
    a4-> fs8( e-.) a4-> fs8( e-.) 
  }
  d'8-. d4---> d8-. d4---> d8-. d---> ~
  d d8-. d4---> d8-^ d8-^ d4-> \bendAfter #-5

  \repeat percent 3 {
    cs4( \mf d8-.) cs-> ~ cs4 r8 cs(-> ~
    cs d-.) cs( b-.) a4(-> b-^)
  }
  d8-. \f d4---> d8-. d4---> d8-. d---> ~
  d d8-. d4---> d8-^ d8-^ d4-> \bendAfter #-5

  % Estribillo
  \bar "[|:"
  \repeat volta 2 {
    \repeat percent 3 {
      r8 cs---> r4 r8 b---> r4
      a4-> fs8( e-.) a4-> fs8( e-.) 
    }
    d'8-. \f d4---> d8-. d4---> d8-. d---> ~
    d d8-. d4---> d8-^ d8-^ d4->
  }
  \bar ":|][|:"
  \repeat volta 2 {
    \repeat percent 3 {
      fs8-. fs4---> e8-. e4---> e8-. e---> ~
      e e8-. e4---> cs8-^ cs8-^ cs4-^
    }
    d8-. \f d4---> d8-. d4---> d8-. d---> ~
    d d8-. d4---> d8-^ d8-^ d4->
  }
  \bar ":|]"
}

% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\book {
	\paper { 
		% Descomentar para score
		% #(set-paper-size "a4" 'landscape)
		% indent = 20
		% systems-per-page = 2
	}
	
	\score {
		% Descomentar para midi
		% \midi { \tempo 4 = 110 }
		
		% NO USAR layout-set-staff-size O SE ROMPE lilyjazz!!!
		\layout { }
		
		<<
			\scoreMarkup
			\transpose bf c' \scoreChords
			\new StaffGroup <<
				\new Staff \with 
				{ instrumentName = "Trpt."}
				{ \transpose bf c' \trumpet } 

				\new Staff \with 
				{ instrumentName = "T Sx." \RemoveEmptyStaves }
				{ \transpose bf c' \tenorSax } 

				\new Staff \with 
				{ instrumentName = "Trbn." \RemoveEmptyStaves } 
				{ \trombone }
			>>
		>>
	}
}
