\version "2.18.2"
\language "english"

% Se debe incluir antes que lilyjazz. Si no, lilyjazz no funciona.
#(set-global-staff-size 19)

\include "main.ily"

\paper {
	indent = 10
}

\header {
	title = "Midnight hour"
	instrument = "Trumpet, Tenor Sax & Trombone"
	composer = "Wilson Pickett"
	arranger = "Transc: gabriel@sauros.es"
	poet = ""
	tagline = "Edition: gabriel@sauros.es"
}

% Función musical global
global = {
	% Para pintar todos los números de compás debajo del pentagrama
	% \override Score.BarNumber #'break-visibility = #end-of-line-invisible
	% \override Score.BarNumber #'direction = #DOWN
	% \override Score.BarNumber #'extra-offset = #'(1.5 . 0)
	\compressFullBarRests

	\set Timing.beamExceptions = #'()
	\set Timing.baseMoment = #(ly:make-moment 1/4)
	\set Timing.beatStructure = #'(1 1 1 1) 
}

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
	% Oculta el primer pentagrama del markup
	\override Staff.VerticalAxisGroup #'remove-first = ##t

	% Indicación de tiempo
	% \once \override Score.RehearsalMark.self-alignment-X = #LEFT
	% \once \override Score.RehearsalMark.extra-offset = #'(0 . 3) 
	% \mark \jazzTempoMarkup "Easy Swing" c4 "112"

	s1*8

	\mkup "(Tacet 1st x)"
	\mkup "(Repeat 2xs)"
	s1*8

	\mkup "(Play both xs)"
	s1*2
	\break
	s1*14

	\mkup "(Trumpet solo)"
	s1*2
	\break
	s1*17

	\mkup "(Ad. lib)"
	s1
	\break
}

% Cifrados
scoreChords = \chords {
	% c1:m f2:m g2:m7

	s1*32
	b1:7
	s1

	e1 s s s
	e1 s s s
	b1:7 a:7 a:7 s
	e1 
}

% Música
trumpet = \transpose g e \relative c'' {
	\global
	\key g \major

	f2.~ f8 d~-> | d2. r4 | c2.~ c8 f~->\fp | f1\< | R1*4\!

	\bar "[|:"
	\repeat volta 2 {
		d1->\mf | r2 c8 b g g ~-> | g1 | R | d'-> | r2 c8 b g g ~-> | g1 | R

		d'2. r4 | c2. r4 | r8 c8 ~->\fp c2. ~\< | c1 |
		R1\! | r2 c8 b g g ~-> | g1 | R1*2
	}
	\bar ":|]"

	d'2-> c8-. c4-- b8 ~-> | b2 c8-. c4-- d8 ~-> | d4 c8 a c4 a8 g | a2 c8-. c4-- d8 ~->
	d2 c8-. c4-- b8 ~-> | b2 \times 2/3 { c4-- f,-- f-- } | g1->

	\improOn r2^"(Solo break)" r2 

	\repeat unfold 26 { r2 }
	\improOff
	r2 c8 b g g ~-> | g1 | R1*3

	\bar "||"
	R1
	\bar "||"

	d'2-> c8-. c4-- b8 ~-> | b2 c8-. c4-- d8 ~-> | d4 c8 a c4 a8 g | a2 c8-. c4-- d8 ~->
	d2 c8-. c4-- b8 ~-> | b2 \times 2/3 { c4-- f,-- f-- }

	\break
	g1 ~-> | g2 \times 2/3 { c4-- f,-- f-- } | 
	g1 ~-> | g2 \times 2/3 { c4-- f,-- f-- } | 
	g1 ~-> | g2 \times 2/3 { c4-- f,-- f-- } | 
	g2.\fermata r4

	g'1->\fermata

	\bar "|."
}

tenorSax = \transpose g e \relative c'' {
	\global
	\key g \major

	c'2.~ c8 a~-> | a2. r4 | g2.~ g8 bf~->\fp | bf1\< | R1*4\!

	\repeat volta 2 {
		d1->\mf | r2 g,8 fs? d d ~-> | d1 | R | d' | r2 g,8 fs? d d ~-> | d1 | R

		a'2. r4 | g2. r4 | r8 e8 ~->\fp e2. ~\< | e1 |
		R1\! | r2 g8 fs d d ~-> | d1 | R1*2
	}

	d'2-> c8-. c4-- b8 ~-> | b2 c8-. c4-- d8 ~-> | d4 c8 a c4 a8 g | a2 c8-. c4-- d8 ~->
	d2 c8-. c4-- b8 ~-> | b2 \times 2/3 { c4-- f,-- f-- } | g1->
	R1

	R1*13
	r2 c8 b g g ~-> | g1 | R1*3

	R1

	d'2-> c8-. c4-- b8 ~-> | b2 c8-. c4-- d8 ~-> | d4 c8 a c4 a8 g | a2 c8-. c4-- d8 ~->
	d2 c8-. c4-- b8 ~-> | b2 \times 2/3 { c4-- f,-- f-- }

	\break
	g1 ~-> | g2 \times 2/3 { c4-- f,-- f-- } | 
	g1 ~-> | g2 \times 2/3 { c4-- f,-- f-- } | 
	g1 ~-> | g2 \times 2/3 { c4-- f,-- f-- } | 
	g2.\fermata r4

	d'1->\fermata
}

trombone = \transpose g e \relative c' {
	\global
	\key g \major
	\clef bass

	a'2.~ a8 fs~-> | fs2. r4 | e2.~ e8 d~->\fp | d1\< | R1*4\!

	\repeat volta 2 {
		d1->\mf | r2 c8 b g g ~-> | g1 | R | d' | r2 c8 b g g ~-> | g1 | R

		fs'2. r4 | e2. r4 | r8 b~->\fp b2.\< ~ | b1 |
		R1\! | r2 c8 b g g ~-> | g1 | R1*2
	}

	d'2-> c8-. c4-- b8 ~-> | b2 c8-. c4-- d8 ~-> | d4 c8 a c4 a8 g | a2 c8-. c4-- d8 ~->
	d2 c8-. c4-- b8 ~-> | b2 \times 2/3 { c4-- f,-- f-- } | g1->
	R1

	R1*13
	r2 c8 b g g ~-> | g1 | R1*3

	R1

	d'2-> c8-. c4-- b8 ~-> | b2 c8-. c4-- d8 ~-> | d4 c8 a c4 a8 g | a2 c8-. c4-- d8 ~->
	d2 c8-. c4-- b8 ~-> | b2 \times 2/3 { c4-- f,-- f-- }

	g1 ~-> | g2 \times 2/3 { c4-- f,-- f-- } | 
	g1 ~-> | g2 \times 2/3 { c4-- f,-- f-- } | 
	g1 ~-> | g2 \times 2/3 { c4-- f,-- f-- } | 
	g2.\fermata r4

	g'1->\fermata
}

% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\book {
	\paper { 
		% Descomentar para score
		% #(set-paper-size "a4" 'landscape)
		% indent = 20
		% systems-per-page = 2
	}
	
	\score {
		% Descomentar para midi
		% \midi { \tempo 4 = 110 }
		
		% NO USAR layout-set-staff-size O SE ROMPE lilyjazz!!!
		\layout { }
		
		<<
			\scoreMarkup
			\transpose bf c' \scoreChords
			\new StaffGroup <<
				\new Staff \with 
				{ instrumentName = "Trpt."}
				{ \transpose bf c' \trumpet } 

				\new Staff \with 
				{ instrumentName = "T Sx." }
				{ \transpose bf c' \tenorSax } 

				\new Staff \with 
				{ instrumentName = "Trbn." } 
				{ \trombone }
			>>
		>>
	}
}
