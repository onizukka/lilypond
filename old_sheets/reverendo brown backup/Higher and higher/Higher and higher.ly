\version "2.18.2"
\language "english"

% Se debe incluir antes que lilyjazz. Si no, lilyjazz no funciona.
#(set-global-staff-size 19)

\include "main.ily"

\paper {
	indent = 10
}

\header {
	title = "Higher and higher"
	instrument = "Trumpet, Tenor Sax & Trombone"
	composer = "Jackie Wilson"
	arranger = "Transc: gabriel@sauros.es"
	poet = ""
	tagline = "Edition: gabriel@sauros.es"
}

% Función musical global
global = {
	% Para pintar todos los números de compás debajo del pentagrama
	% \override Score.BarNumber #'break-visibility = #end-of-line-invisible
	% \override Score.BarNumber #'direction = #DOWN
	% \override Score.BarNumber #'extra-offset = #'(1.5 . 0)
	\compressFullBarRests

	\set Timing.beamExceptions = #'()
	\set Timing.baseMoment = #(ly:make-moment 1/4)
	\set Timing.beatStructure = #'(1 1 1 1) 
}

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
	% Oculta el primer pentagrama del markup
	\override Staff.VerticalAxisGroup #'remove-first = ##t

	% Indicación de tiempo
	% \once \override Score.RehearsalMark.self-alignment-X = #LEFT
	% \once \override Score.RehearsalMark.extra-offset = #'(0 . 3) 
	% \mark \jazzTempoMarkup "Easy Swing" c4 "112"

	s1*32

	s1*10

	s1*34

	s1*42

	s1*18
}

% Cifrados
scoreChords = \chords {
	% c1:m f2:m g2:m7
}

% Música
trumpet = \relative c'' {
	\global
	\key d \major

	R1*31
	r4 fs-- a-- fs-- 

	\bar "[|:"
	\repeat volta 2 {
		a1->
		fs2~ fs8 a fs4-. 
		d1
		r4 a8 a b d4-. b8 

		\break
		d1
		r4 a8 a b d4-. b8 
	}
	\alternative {
		{
			d1
			r4 fs-- a-- fs-- 
			\bar ":|]"
		}

		{
			d1
      R1
		}
	}

	\break
	\bar "[|:"
	\repeat volta 2 {
		R1*16
		R1*5
		\break
		R1*3
		\bar "||"
		R1*2
		\break
		R1*4		
	}
	\alternative {
		{
			\break
			R1*2
			\bar ":|]"
		}

		{
			r4 d2.->\fp\<
			r4\f fs-- a-- fs-- 
		}
	}


	\break
	\repeat volta 2 {
		a1->
		fs2~ fs8 a fs4-. 
		d1
		r4 a8 a b d4-. b8 

		\break
		d1
		r4 a8 a b d4-. b8 
	}
	\alternative {
		{
			d1
			r4 fs-- a-- fs-- 
			\bar ":|]"
		}

		{
			d1
      R1
		}
	}

	R1*16
	\bar "||"
	\pageBreak
	R1*16

	\bar "[|:"
	\repeat volta 2 {
		R1*4
		\break
		R1*4
		\bar "||"
		R1*4
		\break
		R1*2
	}

	\alternative {
		{
			R1*2
			\bar ":|]"
		}

		{
			\break
			r4 d2.->\fp\<
			r4\f fs-- a-- fs-- 
		}
	}

	\bar "[|:"
	\repeat volta 2 {
		a1->
		fs2~ fs8 a fs4-. 
		\break
		d1
		r4 a8 a b d4-. b8 

		d1
		r4 a8 a b d4-. b8 
	}
	\alternative {
		{
		\break
			d1
			r4 fs-- a-- fs-- 
			\bar ":|]"
		}

		{	
			d4-^ r4 r2\fermata
			e1\fermata
			\bar "||"
		}
	}
}

tenorSax = \relative c'' {
	\global
	\key d \major

	R1*31
	r4 a'-- d-- a-- 

	\bar "[|:"
	\repeat volta 2 {
		d1->
		a2~ a8 d a4-. 
		b1
		r4 fs8 fs g a4-. fs8 

		g1
		r4 fs8 fs g b4-. g8 
	}
	\alternative {
		{
			a1
			r4 a-- d-- a-- 
			\bar ":|]"
		}

		{
			a1
      R1
		}
	}

	\repeat volta 2 {
		R1*16

		r4 d,-. r8 d-. r4
		d4-. r8 d-. r4 d-.
		r4 d-. r8 d-. r4
		d4-. r8 d-. r4 d-.

		r4 e-. r8 e-. r4
		e4-. r8 e-. r4 e-.
		r4 d-. r8 d-. r4
		d4-. r8 d-. r4 d-.

		r4 d-. r8 d-. r4
		d4-. r8 d-. r4 d-.
		r4 d-. r8 d-. r4
		d4-. r8 d-. r4 d-.

		r4 e-. r8 e-. r4
		e4-. r8 e-. r4 e-.
	}
	\alternative {
		{
			r4 d-. r8 d-. r4
			d4-. r8 d-. r4 d-.
			\bar ":|]"
		}

		{
			r4 a'2.->\fp\<
			r4\f a-- d-- a-- 
		}
	}

	\bar "[|:"
	\repeat volta 2 {
		d1->
		a2~ a8 d a4-. 
		b1
		r4 fs8 fs g a4-. fs8 

		g1
		r4 fs8 fs g b4-. g8 
	}
	\alternative {
		{
			a1
			r4 a-- d-- a-- 
			\bar ":|]"
		}

		{
			a1
      R1
		}
	}

	R1*16
	R1*16

	\repeat volta 2 {
		r4 d,-. r8 d-. r4
		d4-. r8 d-. r4 d-.
		r4 d-. r8 d-. r4
		d4-. r8 d-. r4 d-.

		r4 e-. r8 e-. r4
		e4-. r8 e-. r4 e-.
		r4 d-. r8 d-. r4
		d4-. r8 d-. r4 d-.

		r4 d-. r8 d-. r4
		d4-. r8 d-. r4 d-.
		r4 d-. r8 d-. r4
		d4-. r8 d-. r4 d-.

		r4 e-. r8 e-. r4
		e4-. r8 e-. r4 e-.
	}

	\alternative {
		{
			r4 d-. r8 d-. r4
			d4-. r8 d-. r4 d-.
			\bar ":|]"
		}

		{
			r4 a'2.->\fp\<
			r4\f a-- d-- a-- 
		}
	}

	\bar "[|:"
	\repeat volta 2 {
		d1->
		a2~ a8 d a4-. 
		b1
		r4 fs8 fs g a4-. fs8 

		g1
		r4 fs8 fs g b4-. g8 
	}
	\alternative {
		{
			a1
			r4 a-- d-- a-- 
			\bar ":|]"
		}

		{	
			a4-^ r4 r2\fermata
			c1\fermata
			\bar "||"
		}
	}
}

trombone = \relative c' {
	\global
	\key d \major
	\clef bass

	R1*31
	r4 fs-- a-- fs-- 

	\bar "[|:"
	\repeat volta 2 {
		a1->
		fs2~ fs8 a fs4-. 
		d1
		r4 a8 a b d4-. b8 

		d1
		r4 a8 a b d4-. b8 
	}
	\alternative {
		{
			d1
			r4 fs-- a-- fs-- 
			\bar ":|]"
		}

		{
			d1
      R1
		}
	}

	\repeat volta 2 {
		R1*16

		r4 a-. r8 a-. r4
		a4-. r8 a-. r4 a-.
		r4 b-. r8 b-. r4
		b4-. r8 b-. r4 b-.

		r4 b-. r8 b-. r4
		b4-. r8 b-. r4 b-.
		r4 a-. r8 a-. r4
		a4-. r8 a-. r4 a-.

		r4 a-. r8 a-. r4
		a4-. r8 a-. r4 a-.
		r4 b-. r8 b-. r4
		b4-. r8 b-. r4 b-.

		r4 b-. r8 b-. r4
		b4-. r8 b-. r4 b-.
	}
	\alternative {
		{
			r4 a-. r8 a-. r4
			a4-. r8 a-. r4 a-.
			\bar ":|]"
		}

		{
			r4 d2.->\fp\<
			r4\f fs-- a-- fs-- 
		}
	}

	\bar "[|:"
	\repeat volta 2 {
		a1->
		fs2~ fs8 a fs4-. 
		d1
		r4 a8 a b d4-. b8 

		d1
		r4 a8 a b d4-. b8 
	}
	\alternative {
		{
			d1
			r4 fs-- a-- fs-- 
			\bar ":|]"
		}

		{
			d1
      R1
		}
	}

	R1*16
	R1*16

	\repeat volta 2 {
		r4 a-. r8 a-. r4
		a4-. r8 a-. r4 a-.
		r4 b-. r8 b-. r4
		b4-. r8 b-. r4 b-.

		r4 b-. r8 b-. r4
		b4-. r8 b-. r4 b-.
		r4 a-. r8 a-. r4
		a4-. r8 a-. r4 a-.

		r4 a-. r8 a-. r4
		a4-. r8 a-. r4 a-.
		r4 b-. r8 b-. r4
		b4-. r8 b-. r4 b-.

		r4 b-. r8 b-. r4
		b4-. r8 b-. r4 b-.
	}

	\alternative {
		{
			r4 a-. r8 a-. r4
			a4-. r8 a-. r4 a-.
			\bar ":|]"
		}

		{
			r4 d2.->\fp\<
			r4\f fs-- a-- fs-- 
		}
	}

	\bar "[|:"
	\repeat volta 2 {
		a1->
		fs2~ fs8 a fs4-. 
		d1
		r4 a8 a b d4-. b8 

		d1
		r4 a8 a b d4-. b8 
	}
	\alternative {
		{
			d1
			r4 fs-- a-- fs-- 
			\bar ":|]"
		}

		{	
			d4-^ r4 r2\fermata
			fs1\fermata
			\bar "||"
		}
	}
}

% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\book {
	\paper { 
		% Descomentar para score
		% #(set-paper-size "a4" 'landscape)
		% indent = 20
		% systems-per-page = 2
	}
	
	\score {
		% Descomentar para midi
		% \midi { \tempo 4 = 110 }
		
		% NO USAR layout-set-staff-size O SE ROMPE lilyjazz!!!
		\layout { }
		
		<<
			\scoreMarkup
			\transpose bf c' \scoreChords
			\new StaffGroup <<
				\new Staff \with 
				{ instrumentName = "Trpt."}
				{ \transpose bf c' \trumpet } 

				\new Staff \with 
				{ instrumentName = "T Sx." \RemoveEmptyStaves }
				{ \transpose bf c' \tenorSax } 

				\new Staff \with 
				{ instrumentName = "Trbn." \RemoveEmptyStaves } 
				{ \trombone }
			>>
		>>
	}
}
