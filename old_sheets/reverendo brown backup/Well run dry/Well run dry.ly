\version "2.18.2"
\language "english"

% Se debe incluir antes que lilyjazz. Si no, lilyjazz no funciona.
#(set-global-staff-size 19)

\include "main.ily"

\paper {
	indent = 10
}

\header {
	title = "Well run dry"
	instrument = "Trumpet, Tenor Sax & Trombone"
	composer = "Phat Phunktion"
	arranger = "Transc: gabriel@sauros.es"
	poet = ""
	tagline = "Edition: gabriel@sauros.es"
}

% Función musical global
global = {
	% Para pintar todos los números de compás debajo del pentagrama
	% \override Score.BarNumber #'break-visibility = #end-of-line-invisible
	% \override Score.BarNumber #'direction = #DOWN
	% \override Score.BarNumber #'extra-offset = #'(1.5 . 0)
	\compressFullBarRests

	\set Timing.beamExceptions = #'()
	\set Timing.baseMoment = #(ly:make-moment 1/4)
	\set Timing.beatStructure = #'(1 1 1 1) 
}

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
	% Oculta el primer pentagrama del markup
	\override Staff.VerticalAxisGroup #'remove-first = ##t

	% Indicación de tiempo
	% \once \override Score.RehearsalMark.self-alignment-X = #LEFT
	% \once \override Score.RehearsalMark.extra-offset = #'(0 . 3) 
	% \mark \jazzTempoMarkup "Easy Swing" c4 "112"

	s1*16

	s1*9
	\mkup "(Play only 2nd x)"

	s1*5
	\mkup "(Play both xs)"
}

% Cifrados
scoreChords = \chords {
	% c1:m f2:m g2:m7
}
% Música
trumpet = \relative c'' {
  
	\global
	\key e \minor
	
	R1*8
	
	r2\mf b8.---> cs16~---> cs8 d~--->\fp
	d4\< r8\! b16 cs d cs b8-. cs-. b~->
	\break
	b cs -. r4 r4 cs8-. cs~->\fp
	cs4\< r4\! d16 e8-. d16 g8-- e--
	
	r2 e8.---> fs16~---> fs8 g~--->\fp
	\break
	g4\< r8\! e16 fs g fs e8-. fs-. e~->
	e fs -. r4 r4 r8 d~->\fp
	d1\<
	
	\break
	\bar "[|:"
	\repeat volta 2 {
		R1*8\!
		
		R1
		r2 r4 r8 c,~->\fp\<
		c1
		r8\mf bf( e a g bf, d4--
		
		\break
		e4)-^ r4 r2
		R1
		r2 g8( g a b)-.
		r4 g'2.->\fp\<
		
		\break
		R1*2\!
		r4 r8 \mf b,16 cs d cs b8-. cs-. b~->
		b cs -. r4 d16\f e8-. d16 g8-- e--
		
		\break
		R1*2
		r2 r4 r8\mf a,~->
		a a bf8. bf16 b8( d)-. bf16( a g8-.)
	}

	\alternative {
		{
			\break
			e'4-^ r e8.---> fs16~---> fs8 g~--->\fp
			g4\< r8\! e16 fs g fs e8-. fs-. e~->
			e fs -. r4 r4 r8 d~->\fp
			d1\<

			\bar ":|]"
		}

		{
			\break
			e4-^\f r r2
			r2 r8 d-> r e->
		}
	}
	R1
	r4 b'8( bf a d,4)-^ e8~->
	
	\break
	e8 d e4-^ r2
	r2 r8 d-> r e->
	r4 r8 f-> r4 r8 g->
  r8 g-> r a-^ bf16( a g <e e' \leftParenthese>^"(Optional 8va)" <ef ef'> <d d'> <b b'> <e e' \rightParenthese>)-^
	
	\pageBreak
	R1*8
	
	R1
	r2 r4 r8 c,~->\fp\<
	c1
	r8\! bf( e a g bf, d4--
	
	\break
	e4)-^ r4 r2
	R1
	r2 g8( g a b)-.
	r4 g'2.->\fp\<
	R1\!
	
	\break
	R1*2\!
	r4 r8 \mf b,16 cs d cs b8-. cs-. b~->
	b cs -. r4 d16\f e8-. d16 g8-- e--
	
	\break
	R1*2
	r2 r4 r8 a,~->
	a a bf8. bf16 b8( d)-. bf16( a g8-.)
	
	\break
	R1*2
	r4 r8 \mf b16 cs d cs b8-. cs-. b~->
	b cs -. r4 d16\f e8-. d16 g8-- e--
	
	\break
	R1*2
	r2 r4 r8 a,(~->
	a8 e g a b d e g 
	
	\break
	e4)---> r r2
	R1
	r2 r4 r8 d->
  r8 d-> r8 d~-> d4 <d d' \leftParenthese>16(^"(Optional 8va)" <b b'> <d d'> <e e' \rightParenthese>)-^
	
	R1*2
	\break
	r2 r4 r8 d->
	r8 d-> r8 d~->\fp d4\< r4\!
	
	r8 b,( e a g d)-^ r4
	r8 cs( fs b a e)-^ r4
	\break
	r8 d( g c b f)-^ r d'->
	r8 d-> r d-> r cs-> r d~->
	
	d4 d-^ r8 d-> r d~->
	\break
	d4 d-^ r8 d-> r d~->
	d4 d-^ r8 d-> r a~->
	a a bf8 bf b8( d)-. bf16( a g8-.)
	
	\bar "||"
	
}

tenorSax = \relative c'' {
  
	\global
	\key e \minor

	R1*8
	
	r2\mf g'8.---> a16~---> a8 b~--->\fp
	b4\< r8\! g16 a b16 a g8-. a-. g~->
	g a-. r4 r4 a8-. g~->\fp
	g4\< r4\! d16 e8-. d16 g8-- e--
	
	r2 g8.---> a16~---> a8 b~--->\fp
	b4\< r8\! g16 a b16 a g8-. a-. g~->
	g a-. r4 r4 r8 d,~->\fp
	d1\<
	
	\repeat volta 2 {
		R1*8\!
		
		R1
		r2 r4 r8 g,~->\fp
		g1\<
		r8\mf bf( e a g bf, d4--
		
		e4)-^ r4 r2
		R1
		r2 g8( g a b)-.
		r4 ds2.->\fp\<
		
		R1*2\!
		r4 r8\mf g,16 a b16 a g8-. a-. g~->
		g a-. r4 d,16\f e8-. d16 g8-- e--
		
		R1*2
		r2 r4 r8\mf a,~->
		a a bf8. bf16 b8( d)-. bf16( a g8)-.
		
		
	}
	
	\alternative {
		{
      e'4-^ r g8.---> a16~---> a8 b~--->\fp
      b4\< r8\! g16 a b16 a g8-. a-. g~->
      g a-. r4 r4 r8 d,~->\fp
			d1\<
		}

		{
			e4-^\f r r2
			r2 r8 d-> r e->
		}
	}
	R1
	r4 b'8( bf a d,4-^) e8~->

	e8 d e4-^ r2
	r2 r8 d-> r e->
	r4 r8 f-> r4 r8 g->
	r8 g-> r a-^ bf16( a g e ef d b e)-^
	
	R1*8
	
	R1
	r2 r4 r8 g,~
	g1
	r8 bf( e a g bf, d4--
	
	e4)-^ r4 r2
	R1
	r2 g8( g a b)-.
	r4 ds2.->\fp\<
	R1\!
	
	R1*2
	r4 r8\mf g,16 a b16 a g8-. a-. g~->
	g a-. r4 d,16 e8-. d16 g8-- e--
	
	R1*2
	r2 r4 r8 a,~->
	a a bf8. bf16 b8( d)-. bf16( a g8)-.
	
	R1*2
	r4 r8\mf g'16 a b16 a g8-. a-. g~->
	g a-. r4\f d,16 e8-. d16 g8-- e--
	
	R1*2
	r2 r4 r8 a,(~->
	a8 e g a b d e g 
	
	b4)---> r r2
	R1
	r2 r4 r8 bf->
	r8 bf-> r8 a~-> a4 d,16( b? d e)-^
	
	R1*2
	r2 r4 r8 bf'->
	r8 bf-> r8 a~->\fp a4\< r4\!
	
	r8 b,?( e a g d)-^ r4
	r8 cs( fs b? a e)-^ r4
	r8 d( g c b f)-^ r bf->
	r8 bf-> r a-> r gs-> r a~->
	
	a4 a-^ r8 a-> r a~->
	a4 a-^ r8 a-> r a~->
	a4 a-^ r8 a-> r a,~->
	a a bf8 bf b8( d)-. bf16( a g8-.)
}

trombone = \relative c' {
  
	\global
	\key e \minor
	\clef bass

	R1*8
	
	r2\mf d8.---> e16~---> e8 e~--->\fp
	e4\< r8\! d16 e fs16 e d8-. e-. d~->
	d e -. r4 r4 g8-. e~->\fp
	e4\< r4\! d16 e8-. d16 g8-- e--
	
	r2 d8.---> e16~---> e8 e~--->\fp
	e4\< r8\! d16 e fs16 e d8-. e-. d~->
	d e -. r4 r4 r8 d~->\fp
	d1\<
	
	\repeat volta 2 {
		R1*8\!
		
		R1
		r2 r4 r8 f,~->\fp\<
		f1
		r8\mf bf( e a g bf, d4--
		
		e4)-^ r4 r2
		R1
		r2 g,8( g a b)-.
		r4 a'2.->\fp\<
		
		R1*2\!
		r4 r8\mf d,16 e fs16 e d8-. e-. d~->
		d e-. r4 d16\f e8-. d16 g8-- e--
		
		R1*2
		r2 r4 r8\mf a,~->
		a a bf8. bf16 b8( d)-. bf16( a g8-.)
	}

	\alternative {
		{
      e'4-^ r d8.---> e16~---> e8 e~--->\fp
      e4\< r8\! d16 e fs16 e d8-. e-. d~->
      d e -. r4 r4 r8 d~->\fp
			d1\<
		}

		{
			e,4-^\f r r2
			r2 r8 d-> r e->
		}
	}
	R1
	r4 b'8( bf a d,4)-^ e8~->
	
	e8 d e4-^ r2
	r2 r8 d-> r e->
	r4 r8 f-> r4 r8 g->
	r8 g-> r a-^ bf16( a g e' ef d b e)-^
		
	R1*8
	
	R1
	r2 r4 r8 f,~->\fp\<
	f1
	r8\! bf( e a g bf, d4--
	
	e4)-^ r4 r2
	R1
	r2 g,8( g a b)-.
	r4 a'2.->\fp\<
	R1\!
	
	R1*2\!
	r4 r8\mf d,16 e fs16 e d8-. e-. d~->
	d e-. r4 d16 e8-. d16 g8-- e--
	
	R1*2
	r2 r4 r8 a,~->
	a a bf8. bf16 b8( d)-. bf16( a g8-.)
	
	R1*2\!
	r4 r8\mf d'16 e fs16 e d8-. e-. d~->
	d e-. r4 d16 e8-. d16 g8-- e--
	
	R1*2
	r2 r4 r8 a,(~->
	a8 e g a b d e g 
	
	e4)---> r r2
	R1
	r2 r4 r8 f->
	r8 f-> r8 ef~-> ef4 d16( b d e)-^
	
	R1*2
	r2 r4 r8 f->
	r8 f-> r8 ef~->\fp ef4\< r4\!
	
	r8 b,?( e a g d)-^ r4
	r8 cs( fs b a e)-^ r4
	r8 d( g c b f)-^ r f'->
	r8 f-> r ef-> r d-> r ds~->
	
	ds4 ds-^ r8 ds-> r ds~->
	ds4 ds-^ r8 ds-> r ds~->
	ds4 ds-^ r8 ds-> r a~->
	a a bf8 bf b8( d)-. bf16( a g8-.)
}

% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\book {
	\paper { 
		% Descomentar para score
		% #(set-paper-size "a4" 'landscape)
		% indent = 20
		% systems-per-page = 2
	}
	
	\score {
		% Descomentar para midi
		% \midi { \tempo 4 = 110 }
		
		% NO USAR layout-set-staff-size O SE ROMPE lilyjazz!!!
		\layout { }
		
		<<
			\scoreMarkup
			\transpose bf c' \scoreChords
			\new StaffGroup <<
				\new Staff \with 
				{ instrumentName = "Trpt."}
				{ \transpose bf c' \trumpet } 

				\new Staff \with 
				{ instrumentName = "T Sx." }
				{ \transpose bf c' \tenorSax } 

				\new Staff \with 
				{ instrumentName = "Trbn." } 
				{ \trombone }
			>>
		>>
	}
}
