\version "2.18.2"
\language "english"

% Se debe incluir antes que lilyjazz. Si no, lilyjazz no funciona.
#(set-global-staff-size 19)

\include "main.ily"

\paper {
	indent = 10
}

\header {
	title = "I'm just a gigoló"
	instrument = "Trumpet, Tenor Sax & Trombone"
	composer = "Louis Prima"
	arranger = "Arr & Transc: gabriel@sauros.es"
	poet = ""
	tagline = "Edition: gabriel@sauros.es"
}

% Se añade a todos los pentagramas de instrumento
global = {
	% Para pintar todos los números de compás debajo del pentagrama
	% \override Score.BarNumber #'break-visibility = #end-of-line-invisible
	% \override Score.BarNumber #'direction = #DOWN
	% \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

	% Silencios de multi compás
	\compressFullBarRests 
}


% Elementos comunes de texto (marcas de ensayo, tempo, etc)
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
	% Oculta el primer pentagrama del markup
	\override Staff.VerticalAxisGroup #'remove-first = ##t

	% Agrupa los compases de 4/4 en 4 grupos de corcheas
	% Basta con que esté definido en un pentagrama para que aplique al resto
	\set Timing.beamExceptions = #'()
	\set Timing.baseMoment = #(ly:make-moment 1/4)
	\set Timing.beatStructure = #'(1 1 1 1)

	% Indicación de tiempo
	\once \override Score.RehearsalMark.self-alignment-X = #LEFT
	\once \override Score.RehearsalMark.extra-offset = #'(0 . 3) 
	\mark \markup \fontsize #-1 "Easy Swing"
}

% Cifrado
scoreChords = \chords {
	% c1:m f2:m g2:m7

	s1*2

	s1*12

	\break
	s1*4

	\break
	s1*4

	\pageBreak
	s1*5

	\break
	s1*4

	\break
	s1*4

	\break
	s1*8
	\mkup "(Harp. solo)"
	s1*8

	\pageBreak
	\mkup "(Tenor solo)"
	s1*4

	\break
	s1*4

	\break
	\mkup "(Trombone solo)"
	s1*4

	\break
	s1*4

	\pageBreak
	s1*4

	\break
	s1*4

	\break
	s1*8

	\pageBreak
	s1*5

	\break
	s1*4

	\break
	s1*4

	\break
	\mkup "(Repeat lyrics shouting)"
	s1*16
	s1*4
}

tenorSaxChords = \chords {
	s1*2

	s1*8
	s1*8

	s1*9

	s1*8
	s1*8

	s1*8

	af1:7
	af2:7 g4 gf4
	f1:7
	f1:7

	bf1:7
	bf1:7
	bf1:m7
	ef1:7/bf
}

tromboneChords = \chords {
	s1*2

	s1*8
	s1*8

	s1*9

	s1*8
	s1*8

	s1*8
	s1*8

	af1
	af1
	df1
	df1

	f1/c
	f1/c
}

% Instrumentos
trumpet = \relative c'' {
	\global

	\key df \major

	R1*2

	\repeat volta 2 {
		R1*8

		af1( \p \<
		gf2.) r4 \!
		f1 \mf \> ~ 
		f2 r4\! r8 f( \p

		f-- f-.) r f( e-- e-.) r e(
		ef?-- ef-.) r ef( ef-- ef-.) r ef(
		df-.) r8 r4 r2
		R1
	}

	\repeat volta 2 {
		af'1 \mf ~
		af4-- g-- gf-- f--
		c'2.-> \sfp \< ~ c8^\markup \fontsize #3 "-3" c-^ \f
		R1

		R1*3
	}
	\alternative {
	 { R1 }
	 { r4 r8 af-> ~ af4 f8-^-- r } 
	}

	\repeat percent 4 {
		r4 r8 af-> ~ af4 f8-^-- r
	}

	\repeat percent 2 {
		r4 r8 g-> ~ g4 f8-^-- r
	}
	af8-^ r8 r4 r2
	R1

	af1 ~
	af4-- g-- gf-- f--
	c'2. \sfp \< ~ c8^\markup \fontsize #3 "-3" c-^ \f
	R1

	R1*4

	\bar "||"
	R1*8

	\bar "||"
	R1*7
	r4 r8 af-> ~ af4 f8-^-- r

	\bar "||"
	\repeat percent 4 {
		r4 r8 af-> ~ af4 f8-^-- r
	}

	r4 r8 g-> ~ g4 f8-^-- r
	r4 r8 g-> ~ g4 f8-^-- c'16( c
	c4) \bendAfter -5 r8 c16( c c4) \bendAfter -5 r8 c16( c
	c4) \bendAfter -5 r8 c16( c c2)\<

	\bar "||"
	\repeat percent 2 {
		c8--\! r c8-- r c8 \bendAfter -5 r r c--
		r8 c-- r8 c( c--)[ c(] c-.) r8
	}
	c8-- r c8-- r c8 \bendAfter -5 r r c--
	r8 c-- r8 c( c--)[ c(] c-.) r8
	c,8^^ r r4 r2
	R1

	\bar "[|:"
	\repeat volta 2 {
		\times 2/3 { af'4 af \bendBefore af \bendBefore }
		\times 2/3 { af4 \bendBefore af \bendBefore af \bendBefore }
		af4-- g-- gf-- f--
		c'2.-> \sfp \< ~ c8^\markup \fontsize #3 "-3" c-^ \f
		R1

		R1*3
	}
	\alternative {{ 
		r1 
		\bar ":|]"
	}{ 
		r4 r8 af-> ~ af4 f8-^-- r 
		\bar "||"
	}}

	\repeat percent 4 {
	r4 r8 af-> ~ af4 f8-^-- r
	}

	\repeat percent 2 {
	r4 r8 g-> ~ g4 f8-^-- r
	}

	\repeat percent 2 {
	 r8 af8-> \bendAfter -5 r4 r8 af8-> \bendAfter -5 r4
	}

	\repeat percent 2 {
	 r8 af8-> \bendAfter -5 r4 r8 af8-> \bendAfter -5 r4
	}

	\repeat percent 2 {
	 r8 g8-> \bendAfter -5 r4 r8 g8-> \bendAfter -5 r4
	}
	\bar "||"

	R1*16

	R1*2
	r4 \times 2/3 {af,8( bf c} ef-.) df( af-.) bf(
	c8-.) r b' c-> ~ c2 \fermata

	\bar "|."
}

tenorSax = \relative c'' {
	\global
	\key df \major

	R1*2

	\bar "[|:"
	\repeat volta 2 {
		R1*8

		ef1( \p \<
		ef2.) r4 \!
		c1( \mf \>
		df2) r4\! r8 df( \p

		df-- df-.) r df( cf-- cf-.) r cf(
		c?-- c-.) r c( cf-- cf-.) r cf(
		bf-.) r8 r4 r2
		R1
	}

	\bar ":|][|:"
	\repeat volta 2 {
		gf'1 \mf ~
		gf4-- f-- ff-- ef--
		f2.-> \sfp \< ~ f8 f-^ \f
		R1

		R1*3
	}
	\alternative {{ 
		R1 
		\bar ":|]"
		}{ 
		r4 r8 ef,-> ~ ef4 f8-^-- r 
	}}

	\repeat percent 2 {
		af,4-> \sfz r8 c-> ~ c4 ef8-^-- r
	}
	\repeat percent 2 {
		af,4-> \sfz r8 f'-> ~ f4 af8-^-- r
	}

	\repeat percent 2 {
		c,4-> \sfz r8 f-> ~ f4 a8-^-- r
	}
	c8-^ r8 r4 r2
	R1

	gf'1 \mf ~
	gf4-- f-- ff-- ef--
	f2.-> \sfp \< ~ f8 f-^ \f
	R1

	R1*4

	R1*8
	\impro 32

	\repeat percent 2 {
		af,4-> \sfz r8 c-> ~ c4 ef8-^-- r
	}
	\repeat percent 2 {
		af,4-> \sfz r8 f'-> ~ f4 af8-^-- r
	}

	\repeat percent 2 {
		c,4-> \sfz r8 f-> ~ f4 a8-^-- r
	}
	r8 af'8-> \bendAfter -5 r4 r8 af8-> \bendAfter -5 r4
	r8 af8-> \bendAfter -5 r4 r8 af8~-> af4\<

	af8--\! r af8-- r af8 \bendAfter -5 r r af--
	r8 af-- r8 af( af--)[ af(] af-.) r8

	a8-- r a8-- r a8 \bendAfter -5 r r a--
	r8 a-- r8 g( g--)[ g(] a-.) r8

	f8-- r f8-- r f8 \bendAfter -5 r r f--
	r8 f-- r8 f( fs--)[ fs(] g-.) r8
	af,8^^ r r4 r2
	R1

	\repeat volta 2 {
		\times 2/3 { gf'4 gf \bendBefore gf \bendBefore }
		\times 2/3 { gf4 \bendBefore gf \bendBefore gf \bendBefore }
		gf4-- f-- e-- ef--
		f2.-> \sfp \< ~ f8 f-^ \f
		R1

		R1*3
	}
	\alternative {{ 
		r1
		}{
		r4 r8 ef,-> ~ ef4 f8-^-- r 
	}}

	\repeat percent 2 {
		af,4-> \sfz r8 c-> ~ c4 ef8-^-- r
	}
	\repeat percent 2 {
		af,4-> \sfz r8 f'-> ~ f4 af8-^-- r
	}

	\repeat percent 2 {
		c,4-> \sfz r8 f-> ~ f4 a8-^-- r
	}

	r8 df8-> \bendAfter -5 r4 r8 df8-> \bendAfter -5 r4
	r8 d8-> \bendAfter -5 r4 r8 d8-> \bendAfter -5 r4

	\repeat percent 2 {
	r8 f8-> \bendAfter -5 r4 r8 f8-> \bendAfter -5 r4
	}

	\repeat percent 2 {
	 r8 ef8-> \bendAfter -5 r4 r8 ef8-> \bendAfter -5 r4
	}

	R1*16

	R1*2

	r4 \times 2/3 {af,8( bf c} ef-.) df( af-.) bf(
	c8-.) r d f-> ~ f2 \fermata
}

trombone = \relative c' {
	\global
	\clef bass
	\key df \major

	R1*2

	\repeat volta 2 {
	R1*8

	c1( \p \<
	bf2.) r4 \!
	a1( \mf \>
	bf2) r4\! r8 af( \p

	af-- af-.) r af( af-- af-.) r af(
	g-- g-.) r g( gf-- gf-.) r gf(
	f-.) r8 r4 r2
	R1
	}

	\repeat volta 2 {
	ef'1 \mf ~
	ef4-- d-- df-- c--
	d2.-> \sfp \< ~ d8 d-^ \f
	R1

	R1*3
	}
	\alternative {{
		R1 
		}{ 
		r4 r8 f-> ~ f4 c8-^-- r 
	}}

	\repeat percent 2 {
		r4 r8 f-> ~ f4 c8-^-- r
	}

	\repeat percent 2 {
		r4 r8 df-> ~ df4 df8-^-- r
	}

	\repeat percent 2 {
		r4 r8 ef-> ~ ef4 c8-^-- r
	}
	d8-^ r8 r4 r2
	R1

	ef1 \mf ~
	ef4-- d-- df-- c--
	d2.-> \sfp \< ~ d8 d-^ \f
	R1

	R1*4

	R1*8

	R1*8

	\impro 24

	r8 ef8-> \bendAfter -5 r4 r8 ef8-> \bendAfter -5 r4
	r8 e8-> \bendAfter -5 r4 r8 e8~-> e4\<

	f8--\! r f8-- r f8 \bendAfter -5 r r f--
	r8 f-- r8 f( f--)[ f(] f-.) r8

	ef8-- r ef8-- r ef8 \bendAfter -5 r r ef--
	r8 ef-- r8 ef( ef--)[ ef(] f-.) r8

	df8-- r df8-- r df8 \bendAfter -5 r r df--
	r8 df-- r8 df( d--)[ d(] ef-.) r8
	af,,8^^ r r4 r2
	R1

	\repeat volta 2 {
	\times 2/3 { ef''4 ef \bendBefore ef \bendBefore }
	\times 2/3 { ef4 \bendBefore ef \bendBefore ef \bendBefore }
	ef4-- d-- df-- c--
	d2.-> \sfp \< ~ d8 d-^ \f
	R1

	R1*3
	}
	\alternative {{
		r1 
		}{ 
		r4 r8 f-> ~ f4 c8-^-- r 
	}}

	\repeat percent 2 {
		r4 r8 f-> ~ f4 c8-^-- r
	}

	\repeat percent 2 {
		r4 r8 df-> ~ df4 df8-^-- r
	}

	\repeat percent 2 {
		r4 r8 ef-> ~ ef4 c8-^-- r
	}

	r8 bf8-> \bendAfter -5 r4 r8 bf8-> \bendAfter -5 r4
	r8 b8-> \bendAfter -5 r4 r8 b8-> \bendAfter -5 r4

	\repeat percent 2 {
		r8 c8-> \bendAfter -5 r4 r8 c8-> \bendAfter -5 r4
	}

	r8 d8-> \bendAfter -5 r4 r8 d8-> \bendAfter -5 r4
	r8 d8-> \bendAfter -5 r4 r8 c8-> \bendAfter -5 r4

	R1*16

	R1*2

	r4 \times 2/3 {af8( bf c} ef-.) df( af-.) bf(
	c8-.) r ef, af-> ~ af2 \fermata
}

% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\book {
	\paper { 
		% Descomentar para score
		% #(set-paper-size "a4" 'landscape)
		% indent = 20
		% systems-per-page = 2
	}
	
	\score {
		% Descomentar para midi
		% \midi { \tempo 4 = 110 }
		
		% NO USAR layout-set-staff-size O SE ROMPE lilyjazz!!!
		\layout { }

		<<
			\scoreMarkup
			\transpose bf c' \scoreChords
			\new StaffGroup <<
				\new Staff \with 
				{ instrumentName = "Trpt." }
				{ \transpose bf c' \trumpet }

				\transpose bf c' \tenorSaxChords
				\new Staff \with 
				{ instrumentName = "T Sx." }
				{ \transpose bf c' \tenorSax } 

				\tromboneChords
				\new Staff \with 
				{ instrumentName = "Trbn." } 
				{ \trombone }
			>>
		>>
	}
}
