\version "2.18.2"
\language "english"

% Se debe incluir antes que lilyjazz. Si no, lilyjazz no funciona.
#(set-global-staff-size 19)

\include "main.ily"

\paper {
	indent = 10
}

\header {
	title = "Superstition"
	instrument = "Trumpet, Tenor Sax & Trombone"
	composer = "Stevie Wonder"
	arranger = "Transc: gabriel@sauros.es"
	poet = ""
	tagline = "Edition: gabriel@sauros.es"
}

% Función musical global
global = {
	% Para pintar todos los números de compás debajo del pentagrama
	% \override Score.BarNumber #'break-visibility = #end-of-line-invisible
	% \override Score.BarNumber #'direction = #DOWN
	% \override Score.BarNumber #'extra-offset = #'(1.5 . 0)
	\compressFullBarRests

	\set Timing.beamExceptions = #'()
	\set Timing.baseMoment = #(ly:make-moment 1/4)
	\set Timing.beatStructure = #'(1 1 1 1) 
}

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
	% Oculta el primer pentagrama del markup
	\override Staff.VerticalAxisGroup #'remove-first = ##t

	% Indicación de tiempo
	% \once \override Score.RehearsalMark.self-alignment-X = #LEFT
	% \once \override Score.RehearsalMark.extra-offset = #'(0 . 3) 
	% \mark \jazzTempoMarkup "Easy Swing" c4 "112"
	s1*4
	s1*8

	\break
	\mkup "(Repeat 3xs)"
	s1*8
	s1*2

	\break
	s1*6

	\break
	s1*4

	\break
	s1*4

	\break
	s1*6

	\break
  s1*2
	\mkup "(Repeat 3xs)"
	s1*4

	\break
  \mark \markup "Keep calm & tighten up"
	\mkup "(Repeat 4xs)"
	s1*2
}

% Cifrados
scoreChords = \chords {
	% c1:m f2:m g2:m7
}

% Música
trumpet = \relative c'' {
	\global
	\key e \minor

	R1*4
	R1*8

	\bar "[|:"
	\repeat volta 3 {
		R1*8
		\repeat percent 4 {
			r8\mf e~-. e16 e-. r8 g,16 a b d~ d\turn b e e-.
			r8 e-. d16( e8-.) d16( b'8->) g\prall e8-> r
		}

		fs,4-> d16 cs b8-. fs'8.-> fs16-. r4
		fs4-> d16 cs b8-. e4-> d8-. e~->
		e2.\< r4\!
		\times 2/3 { b'16(\f b b) } \times 2/3 { b16( b b) } b8-> r r2

		e2.-> r4
		d8->\bendAfter -2 r cs8->\bendAfter -2 r b4-- cs8-. e,~->
	}
	\alternative {{
    e1 \>
		R1 \!
		\bar ":|]"
		}{

    e1\repeatTie \>
		R1 \!

		fs4-> d16 cs b8-. fs'8. fs16-. r4
		fs4-> d16 cs b8-. e4 d8-. e~->
		e2.\< r4\!
		R1
		\bar ":|]"
		}{

    e1\repeatTie \>
		R1 \!
	}}

	\bar "[|:"
	\repeat volta 3 {
		e'2.-> r4
		d8->\bendAfter -2 r cs8->\bendAfter -2 r b4-- cs8-. e,~->
    e1 \>
		R1 \!
	}

	\bar ":|][|:"
	\repeat volta 4 {
		r8\f e'~-. e16 e-. r8 g,16 a b d~ d\turn b e e-.
		r8 e-. d16( e8-.) d16( b'8->) g\prall e8---> d16 e-.
	}
	\bar ":|]"
}

tenorSax = \relative c'' {
	\global
	\key e \minor

	R1*4
	R1*8

	\repeat volta 3 {
		R1*8
		\repeat percent 4 {
			r8\mf e~-. e16 e-. r8 g,16 a b d~ d\turn b e e-.
			r8 e-. d16( e8-.) d16( b'8->) g\prall e8-> r
		}

		fs4-> d16 cs b8-. fs'8.-> fs16-. r4
		fs4-> d16 cs b8-. e4-> d8-. e~->
		e2.\< r4\!
		\times 2/3 { g16(\f g g) } \times 2/3 { g16( g g) } g8-> r r2

		e2.-> r4
		b'8->\bendAfter -2 r a8->\bendAfter -2 r g4-- fs8-. e~->
	}
	\alternative {{
    e1 \>
		R1 \!
		}{

    e1\repeatTie \>
		R1 \!

		fs4-> d16 cs b8-. fs'8. fs16-. r4
		fs4-> d16 cs b8-. e4 d8-. e~->
		e2.\< r4\!
		R1 }{

    e1\repeatTie \>
		R1 \!
	}}

	\repeat volta 3 {
		e2.-> r4
		b'8->\bendAfter -2 r a8->\bendAfter -2 r g4-- fs8-. e~->
    e1 \>
		R1 \!
	}

	\repeat volta 4 {
		r8\f e~-. e16 e-. r8 g,16 a b d~ d\turn b e e-.
		r8 e-. d16( e8-.) d16( b'8->) g\prall e8---> d16 e-.
	}
}

trombone = \relative c' {
	\global
	\clef bass
	\key e \minor

	R1*4
	R1*8

	\repeat volta 3 {
		R1*8
		\repeat percent 4 {
			r8\mf e~-. e16 e-. r8 g,16 a b d~ d\turn b e e-.
			r8 e-. d16( e8-.) d16( b'8->) g\prall e8-> r
		}

		fs4 -- -> r fs4 -- -> r
		fs4 -- -> r e4 -- -> d8-. e~->
		e2.\< r4\!
		\times 2/3 { ds16(\f ds ds) } \times 2/3 { ds16( ds ds) } ds8-> r r2

		e2.-> r4
		d8->\bendAfter -2 r cs8->\bendAfter -2 r b4-- cs8-. e,~->
	}
	\alternative {{
    e1 \>
		R1 \!
		}{

    e1\repeatTie \>
		R1 \!

		fs'4 -- -> r g4 -- -> r
		fs4 -- -> r e4 -- -> d8-. e~->
		e2.\< r4\!
		R1 }{

    e1\repeatTie \>
		R1 \!
	}}

	\repeat volta 3 {
		e2.-> r4
		d8->\bendAfter -2 r cs8->\bendAfter -2 r b4-- cs8-. e,~->
    e1 \>
		R1 \!
	}

	\repeat volta 4 {
		r8\f e'~-. e16 e-. r8 g,16 a b d~ d\turn b e e-.
		r8 e-. d16( e8-.) d16( b'8->) g\prall e8---> d16 e-.
	}
}

% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\book {
	\paper { 
		% Descomentar para score
		% #(set-paper-size "a4" 'landscape)
		% indent = 20
		% systems-per-page = 2
	}
	
	\score {
		% Descomentar para midi
		% \midi { \tempo 4 = 110 }
		
		% NO USAR layout-set-staff-size O SE ROMPE lilyjazz!!!
		\layout { }
		
		<<
			\scoreMarkup
			\transpose bf c' \scoreChords
			\new StaffGroup <<
				\new Staff \with 
				{ instrumentName = "Trpt."}
				{ \transpose bf c' \trumpet } 

				\new Staff \with 
				{ instrumentName = "T Sx." \RemoveEmptyStaves }
				{ \transpose bf c' \tenorSax } 

				\new Staff \with 
				{ instrumentName = "Trbn." \RemoveEmptyStaves } 
				{ \trombone }
			>>
		>>
	}
}
