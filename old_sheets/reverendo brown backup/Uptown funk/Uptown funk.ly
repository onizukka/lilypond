\version "2.18.2"
\language "english"

% Se debe incluir antes que lilyjazz. Si no, lilyjazz no funciona.
#(set-global-staff-size 20)

\include "main.ily"

\paper {
	indent = 15
}

\header {
	title = "Uptown funk"
	instrument = "Trumpet, Tenor Sax & Trombone"
	composer = "Bruno Mars"
	arranger = "Transc: gabriel@sauros.es"
	poet = ""
	tagline = "Edition: gabriel@sauros.es"
}

% Función musical global
global = {
	% Para pintar todos los números de compás debajo del pentagrama
	% \override Score.BarNumber #'break-visibility = #end-of-line-invisible
	% \override Score.BarNumber #'direction = #DOWN
	% \override Score.BarNumber #'extra-offset = #'(1.5 . 0)
	\compressFullBarRests

	\set Timing.beamExceptions = #'()
	\set Timing.baseMoment = #(ly:make-moment 1/4)
	\set Timing.beatStructure = #'(1 1 1 1) 
}

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
	% Oculta el primer pentagrama del markup
	\override Staff.VerticalAxisGroup #'remove-first = ##t

	% Indicación de tiempo
	% \once \override Score.RehearsalMark.self-alignment-X = #LEFT
	% \once \override Score.RehearsalMark.extra-offset = #'(0 . 3) 
	% \mark \jazzTempoMarkup "Easy Swing" c4 "112"

	s1*8
	\mkup "(Repeat 3xs)"
	s1*16
	s1*8

	\break
	s1*4

	\break
	s1*4 

	\break
	s1*7

	\break
	s1*4


}

% Cifrados
scoreChords = \chords {
	% c1:m f2:m g2:m7
}

% Música
trumpet = \relative c'' {
	\global
	\key d \minor

	R1*7
	f2. \p \< f16( \f f -.) r8

  \bar "[|:"
	\repeat volta 3 {
		R1*16
		R1*7
		r2 r4 f16(\f f-.) r8

		f16 d c d r8 f16 d c d r8 f16 d c d 
		r8 f-. r4 r4 f16 e d c
		R1
		r2 r8 f16 f f f f f

		f16 d c d r8 f16 d c d r8 f16 d c d
		r8 f-. r4 r4 f16 e d c
		R1
		\repeat percent 4 { r2 r8 f16 f f f f f }
	}
	\alternative {{
		R1 
    \bar ":|]" }{
		\once \override MultiMeasureRestNumber.transparent = ##t
		R1*2^\markup \underline \fontsize #2 "(ad.lib.)"
    \bar ":|]" }{
		d8-> r d8-> r d8-> r d16( d-.) r8
	}}

	r2 r8 d16( d-.) r4
	r2 r4 f16 e d-. c~->
	c8 d-> r4 r8 d16( d-.) r4
	r2 a16 c d-. f~-> f8 \turn d-> 

	r8 d-> r4 r8 d16( d-.) r4
	r2 r4 f16 e d -. c~->\fp
	c2.\< r4\!
	d8->\f c-> b-> a-> f-> g-> af16 f8.->

	r8 d'-> r4 r8 d16( d-.) r4
	r2 r4 f16 e d -. c~->
	c8 d-> r4 r8 d16( d-.) r4
	d8-. d-. d-. d-. a16 c d-. f~-> f8 \turn d-> 

	r8 d-> r4 r8 d16( d-.) r4
	r2 r4 f16 e d -. d,~->
	d4 f8.-> g16~-> g8 a-. c( d-.)
	f16 f f f~-. f f-. r f-- f f-. r8 d4->

	\bar "|."
}

tenorSax = \relative c'' {
	\global
	\key d \minor

	R1*7
	f2. \p \< f16( \f f -.) r8

	\repeat volta 3 {
		R1*16
		R1*7
		r2 r4 f16(\f f-.) r8

		f16 d c d r8 f16 d c d r8 f16 d c d 
		r8 f-. r4 r4 f16 e d c
		R1
		r2 r8 f16 f f f f f

		f16 d c d r8 f16 d c d r8 f16 d c d
		r8 f-. r4 r4 f16 e d c
		R1
		\repeat percent 4 { r2 r8 f16 f f f f f }
	}
	\alternative {{
		R1 }{
		\once \override MultiMeasureRestNumber.transparent = ##t
    R1*2 }{
		d8-> r d8-> r d8-> r d16( d-.) r8
	}}

	r2 r8 d16( d-.) r4
	r2 r4 f16 e d -. c~->
	c8 d-> r4 r8 d16( d-.) r4
	r2 a16 c d -. f~-> f8 \turn d-> 

	r8 d-> r4 r8 d16( d-.) r4
	r2 r4 f16 e d -. c~->\fp
	c2.\< r4\!
	d8->\f c-> b-> a-> f-> g-> af16 f8.->

	r8 d'-> r4 r8 d16( d-.) r4
	r2 r4 f16 e d -. c~->
	c8 d-> r4 r8 d16( d-.) r4
	d8-. d-. d-. d-. a16 c d-. f~-> f8 \turn d-> 

	r8 d-> r4 r8 d16( d-.) r4
	r2 r4 f16 e d -. d,~->
	d4 f8.-> g16~-> g8 a-. c( d-.)
	f16 f f f~-. f f-. r f-- f f-. r8 d4->
}

trombone = \relative c' {
	\global
	\clef bass
	\key d \minor

	R1*7
	c2. \p \< c16( \f  c-.) r8

	\repeat volta 3 {
		R1*16
		R1*7
		r2 r4 c16(\f c-.) r8

		f16 d c d r8 f16 d c d r8 f16 d c d 
		r8 f-. r4 r4 f16 e d c
		R1
		r2 r8 c16 c c c c c

		f16 d c d r8 f16 d c d r8 f16 d c d
		r8 f-. r4 r4 f16 e d c
		R1
		\repeat percent 4 { r2 r8 c16 c c c c c }
	}
	\alternative {{
		R1 }{
		\once \override MultiMeasureRestNumber.transparent = ##t
    R1*2 }{
		d8-> r d8-> r d8-> r d16( d-.) r8
	}}

	r2 r8 d16( d-.) r4
	r2 r4 f16 e d-. c~->
	c8 d-> r4 r8 d16( d-.) r4
	r2 a16 c d-. f~-> f8 \turn d-> 

	r8 d-> r4 r8 d16( d-.) r4
	r2 r4 f16 e d-. c~->\fp
	c2.\< r4\!
	d8->\f c-> b-> a-> f-> g-> af16 f8.->

	r8 d'-> r4 r8 d16( d-.) r4
	r2 r4 f16 e d-. c~->
	c8 d-> r4 r8 d16( d-.) r4
	d8-. d-. d-. d-. a16 c d-. f~-> f8 \turn d-> 

	r8 d-> r4 r8 d16( d-.) r4
	r2 r4 f16 e d-. d,~->
	d4 f8.-> g16~-> g8 a-. c( d-.)
	c16 c c c~-. c c-. r c-- c c-. r8 d4->
}

% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\book {
	\paper { 
		% Descomentar para score
		% #(set-paper-size "a4" 'landscape)
		% indent = 20
		% systems-per-page = 2
	}
	
	\score {
		% Descomentar para midi
		% \midi { \tempo 4 = 110 }
		
		% NO USAR layout-set-staff-size O SE ROMPE lilyjazz!!!
		\layout { }
		
		<<
			\scoreMarkup
			\transpose bf c' \scoreChords
			\new StaffGroup <<
				\new Staff \with 
				{ instrumentName = "Trpt."}
				{ \transpose bf c' \trumpet } 

				\new Staff \with 
				{ instrumentName = "T Sx." \RemoveEmptyStaves }
				{ \transpose bf c' \tenorSax } 

				\new Staff \with 
				{ instrumentName = "Trbn." \RemoveEmptyStaves } 
				{ \trombone }
			>>
		>>
	}
}
