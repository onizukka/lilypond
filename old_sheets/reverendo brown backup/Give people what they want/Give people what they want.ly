\version "2.18.2"
\language "english"

% Se debe incluir antes que lilyjazz. Si no, lilyjazz no funciona.
#(set-global-staff-size 19)

\include "main.ily"

\paper {
	indent = 10
}

\header {
	title = "Give people what they want"
	instrument = "Trumpet, Tenor Sax & Trombone"
	composer = "O'Jays"
	arranger = "Transc: pacosaxo@gmail.com"
	poet = ""
	tagline = "Edition: gabriel@sauros.es"
}

% Función musical global
global = {
	% Para pintar todos los números de compás debajo del pentagrama
	% \override Score.BarNumber #'break-visibility = #end-of-line-invisible
	% \override Score.BarNumber #'direction = #DOWN
	% \override Score.BarNumber #'extra-offset = #'(1.5 . 0)
	\compressFullBarRests

	\set Timing.beamExceptions = #'()
	\set Timing.baseMoment = #(ly:make-moment 1/4)
	\set Timing.beatStructure = #'(1 1 1 1) 

  \key c \minor
}

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
	% Oculta el primer pentagrama del markup
	\override Staff.VerticalAxisGroup #'remove-first = ##t

	% Indicación de tiempo
	% \once \override Score.RehearsalMark.self-alignment-X = #LEFT
	% \once \override Score.RehearsalMark.extra-offset = #'(0 . 3) 
	% \mark \jazzTempoMarkup "Easy Swing" c4 "112"

  \mkup "(Drums)"
  s1*4

  \mkup "(Bass)"
  s1*4
  s1*54

  \mkup "(Repeat 8xs)"
  s1*2
  \mkup "(Repeat 3xs)"
  s1*2
}

% Cifrados
scoreChords = \chords {
	% c1:m f2:m g2:m7
}

% Música
trumpet = \relative c'' {
	\global

  R1*4
  R1*4

  \bar "||"
  R1*2
  r4 r8 c16 (c c ef8.-> c4--->
  c4-^) r r2

  \break
  a2~ a8 a-. a16 bf8.->
  a8 a16( a c8) c16( c ef8 f) ef( c)
  c4-^ r r2
  r2 r8 c-. ef( f-^)

  f2.-> r4
  R1*3

  g2.-> r4
  R1
  g2.-> r4
  g8-. g-. g-. g-. g4-^ r

  \break
  c,4-^ r r2
  r8 c16( c ef8 f-^) r2
  c4-^ r r2
  c4-^ r r2

  a2~ a8 a-. a16 bf8.->
  a8 a16( a c8) c16( c ef8 f) ef( c)
  r8 g'-. f4-> ef8-. c-> ~ c bf16( bf
  c8.-.) bf16-. r bf( c8-.) r8 c-. ef( f-^)

  f2.-> r4
  R1*3

  \pageBreak
  g2.-> r4
  R1
  g2.-> r4
  g8-. g-. g-. g-. g4-^ r

  c,4-^ r r2
  r8 \mf \< c16( c c4-^) r2\!
  r8 g'16( g f8 ef-.) c( bf-.) c4-^
  c8.-. bf16-. r bf( c8-.) r2

  a2~ a8 a-. a16 bf8.->
  a8 a16( a c8) c16( c ef8 f) ef( c)
  r8 g'-. f4-> ef8-. c-> ~ c bf16( bf
  c8.-.) bf16-. r bf( c8-.) r8 c-. ef( f-^)

  \bar "[|:"
  \repeat volta 2 {
    f8( f16 f-.) r f( ef8 ~ ef f-^) ef( f-^)
    g8.( f16-.) r f( ef8 ~ ef8 f-^) ef( f-^)
  }
  \bar ":|]"

  \pageBreak
  g8 ( g16 g-.) r g( g8 ~ g b-^) g4-^
  g16( g g g g8-.) g16( g g8 b-^) g4-^
  g8.( g16-.) r8 g(-> ~ g b-^) g4-^
  c8-^ c8-^ bf8-^ bf8-^ g4-^ r4 

  c,4-^ r r2
  r8 \mf \< ef16( ef ef8-^\!) r8 \mf \< ef16 ( ef-^) r8\! r4
  r8 g16( g f8 ef-.) c( bf-.) c4-^
  c8.-. bf16-. r bf( c8-.) r8 c-. ef( f-^)

  a,2~ a8 a-. a16 bf8.->
  a8 a16( a c8) c16( c ef8 f) ef( c)
  r8 g'-. f4-> ef8-. c-> ~ c bf16( bf
  c8.-.) bf16-. r bf( c8-.) r8 c-. ef( f-^)

  \break
  \bar "[|:"
  \repeat volta 8 {
    bf2-> ~ bf8 bf16 bf bf a8.->
    g2\> r\!
  }

  \bar ":|][|:"
  \repeat volta 3 {
    c,8.-. bf16-. r bf( c8-.) r16 f( g bf-.) ef,8( c-.)
  }
  \bar ":|]"

  ef8-. ef16( d-.) r d( ef8-^) r2
  \bar "|."
}

tenorSax = \relative c'' {
	\global

  R1*4
  R1*4

  \bar "||"
  c8.-. bf16-. r8 c-. r16 fs,( g bf-> ~ bf g ef8-.)
  c8.-. bf16-. r8 c-. r2
  c'8.-. bf16-. r8 c-. r16 fs,( g bf-> ~ bf g ef8-.)
  c8.-. bf16-. r8 c-. r2

  ef'2~ ef8 ef-. ef16 f8.->
  ef2( ef8 f) ef( c)
  c4-^ r r2
  r2 r8 c-. ef( f-^)

  a2.-> r4
  R1*3

  b2.-> r4
  R1
  b2.-> r4
  b8-. b-. b-. b-. b4-^ r

  c4-^ r r2
  r8 c,16( c ef8 f-^) r2
  c4-^ r r2
  c4-^ r r2

  ef2~ ef8 ef-. ef16 f8.->
  ef2( ef8 f) ef( c)
  r8 g'-. f4-> ef8-. c-> ~ c bf16( bf
  c8.-.) bf16-. r bf( c8-.) r8 c-. ef( f-^)

  a2.-> r4
  R1*3

  b2.-> r4
  R1
  b2.-> r4
  b8-. b-. b-. b-. b4-^ r

  c4-^ r r2
  r8 \mf \< c,16( c c4-^) r2\!
  r8 g'16( g f8 ef-.) c( bf-.) c4-^
  c8.-. bf16-. r bf( c8-.) r2

  ef2~ ef8 ef-. ef16 f8.->
  ef2( ef8 f) ef( c)
  r8 g'-. f4-> ef8-. c-> ~ c bf16( bf
  c8.-.) bf16-. r bf( c8-.) r8 c-. ef( f-^)

  \bar "[|:"
  \repeat volta 2 {
    f8( f16 f-.) r f( ef8 ~ ef f-^) ef( f-^)
    g8.( f16-.) r f( ef8 ~ ef8 f-^) ef( f-^)
  }
  \bar ":|]"

  g8 ( g16 g-.) r g( g8 ~ g b-^) g4-^
  g16( g g g g8-.) g16( g g8 b-^) g4-^
  g8.( g16-.) r8 g(-> ~ g b-^) g4-^
  c8-^ c8-^ bf8-^ bf8-^ g4-^ r4 

  c,4-^ r r2
  r8 \mf \< bf'16( bf bf8-^\!) r8 \mf \< bf16 ( bf-^) r8\! r4
  r8 g16( g f8 ef-.) c( bf-.) c4-^
  c8.-. bf16-. r bf( c8-.) r8 c-. ef( f-^)

  ef2~ ef8 ef-. ef16 f8.->
  ef2( ef8 f) ef( c)
  r8 g'-. f4-> ef8-. c-> ~ c bf16( bf
  c8.-.) bf16-. r bf( c8-.) r8 c-. ef( f-^)

  \bar "[|:"
  \repeat volta 8 {
    ef2-> ~ ef8 ef ef16 f8.->
    ef2\> r\!
  }

  \bar ":|][|:"
  \repeat volta 3 {
    c8.-. bf16-. r bf( c8-.) r16 f( g bf-.) ef,8( c-.)
  }
  \bar ":|]"

  c'8-. c16( bf-.) r bf( c8-^) r2
  \bar "|."
}

trombone = \relative c' {
	\global
  \clef bass

  R1*4
  R1*4

  \bar "||"
  c8.-. bf16-. r8 c-. r16 fs,( g bf-> ~ bf g ef8-.)
  c8.-. bf16-. r8 c-. r2
  c'8.-. bf16-. r8 c-. r16 fs,( g bf-> ~ bf g ef8-.)
  c8.-. bf16-. r8 c-. r2

  f,2. r4
  f2. r4
  c'4-^ r r2
  r2 r8 c'-. ef( f-^)

  f2.-> r4
  R1*3

  g2.-> r4
  R1
  g2.-> r4
  g8-. g-. g-. g-. g4-^ r

  c,4-^ r r2
  r8 c16( c ef8 f-^) r2
  c4-^ r r2
  c4-^ r r2

  f,,2. r4
  f2. r4
  r8 g''-. f4-> ef8-. c-> ~ c bf16( bf
  c8.-.) bf16-. r bf( c8-.) r8 c-. ef( f-^)

  f2.-> r4
  R1*3

  g2.-> r4
  R1
  g2.-> r4
  g8-. g-. g-. g-. g4-^ r

  c,4-^ r r2
  r8 \mf \< c16( c c4-^) r2\!
  r8 g'16( g f8 ef-.) c( bf-.) c4-^
  c8.-. bf16-. r bf( c8-.) r2

  f,,2. r4
  f2. r4
  r8 g''-. f4-> ef8-. c-> ~ c bf16( bf
  c8.-.) bf16-. r bf( c8-.) r2

  \bar "[|:"
  \repeat volta 2 {
    f,1-> ~
    f2.-> r4
  }
  \bar ":|]"

  g2.-> r4
  g2.-> r4
  g2.-> r4
  c8-^ c8-^ bf8-^ bf8-^ g4-^ r4 

  c4-^ r r2
  r8 \mf \< e16( e e8-^\!) r8 \mf \< e16 ( e-^) r8\! r4
  r8 g16( g f8 ef-.) c( bf-.) c4-^
  c8.-. bf16-. r bf( c8-.) r2

  f,,2. r4
  f2. r4
  r8 g''-. f4-> ef8-. c-> ~ c bf16( bf
  c8.-.) bf16-. r bf( c8-.) r8 c-. ef( f-^)

  \bar "[|:"
  \repeat volta 8 {
    bf,2-> ~ bf8 bf bf16 a8.->
    bf2\> r\!
  }

  \bar ":|][|:"
  \repeat volta 3 {
    c8.-. bf16-. r bf( c8-.) r16 f,( g bf-.) ef,8( c-.)
  }
  \bar ":|]"

  c'8-. c16( bf-.) r bf( c8-^) r2
  \bar "|."
}

% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\book {
	\paper { 
		% Descomentar para score
		% #(set-paper-size "a4" 'landscape)
		% indent = 20
		% systems-per-page = 2
	}
	
	\score {
		% Descomentar para midi
		% \midi { \tempo 4 = 110 }
		
		% NO USAR layout-set-staff-size O SE ROMPE lilyjazz!!!
		\layout { }
		
		<<
			\scoreMarkup
			\transpose bf c' \scoreChords
			\new StaffGroup <<
				\new Staff \with 
				{ instrumentName = "Trpt."}
				{ \transpose bf c' \trumpet } 

				\new Staff \with 
				{ instrumentName = "T Sx." \RemoveEmptyStaves }
				{ \transpose bf c' \tenorSax } 

				\new Staff \with 
				{ instrumentName = "Trbn." \RemoveEmptyStaves } 
				{ \trombone }
			>>
		>>
	}
}
