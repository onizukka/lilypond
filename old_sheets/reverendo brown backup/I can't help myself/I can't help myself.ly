\version "2.18.2"
\language "english"

% Se debe incluir antes que lilyjazz. Si no, lilyjazz no funciona.
#(set-global-staff-size 19)

\include "main.ily"

\paper {
	indent = 10
}

\header {
	title = "I can't help myself"
	instrument = "Trumpet, Tenor Sax & Trombone"
	composer = "Four Tops"
	arranger = "Transc: pacosaxo@gmail.com"
	poet = ""
	tagline = "Edition: gabriel@sauros.es"
}

% Función musical global
global = {
	% Para pintar todos los números de compás debajo del pentagrama
	% \override Score.BarNumber #'break-visibility = #end-of-line-invisible
	% \override Score.BarNumber #'direction = #DOWN
	% \override Score.BarNumber #'extra-offset = #'(1.5 . 0)
	\compressFullBarRests

	\set Timing.beamExceptions = #'()
	\set Timing.baseMoment = #(ly:make-moment 1/4)
	\set Timing.beatStructure = #'(1 1 1 1) 

  \key c \major
}

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
	% Oculta el primer pentagrama del markup
	\override Staff.VerticalAxisGroup #'remove-first = ##t

	% Indicación de tiempo
	% \once \override Score.RehearsalMark.self-alignment-X = #LEFT
	% \once \override Score.RehearsalMark.extra-offset = #'(0 . 3) 
	% \mark \jazzTempoMarkup "Easy Swing" c4 "112"
}

% Cifrados
scoreChords = \chords {
	% c1:m f2:m g2:m7
}

% Música
trumpet = \relative c'' {
	\global

  R1*2
  R1*2

  c8 g a c~ c g a c
  c8 g a c~ c g a c

  \bar "||"
  R1*8

  \bar "||"
  r2 r4 r8 e( ~
  e8 e d c c4) r
  r2 e8( g e g-> ~ 
  g a-> ~ a4) r2

  r4 f( e d
  a2.) r4
  r4 f'( e d
  g-^) g-^ g8 a-> ~ a4

  c,1 ~ 
  c2 e4( f
  g1 ~
  g2) f4( e

  d1 ~
  d2) r4 e-^
  f8-. c-. d-. f-> ~ f c-. d-. f-.
  g4-^ g-^ g8 a-> ~ a4
}

tenorSax = \relative c'' {
	\global
}

trombone = \relative c' {
	\global
}

% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\book {
	\paper { 
		% Descomentar para score
		% #(set-paper-size "a4" 'landscape)
		% indent = 20
		% systems-per-page = 2
	}
	
	\score {
		% Descomentar para midi
		% \midi { \tempo 4 = 110 }
		
		% NO USAR layout-set-staff-size O SE ROMPE lilyjazz!!!
		\layout { }
		
		<<
			\scoreMarkup
			\transpose bf c' \scoreChords
			\new StaffGroup <<
				\new Staff \with 
				{ instrumentName = "Trpt."}
				{ \transpose bf c' \trumpet } 

				\new Staff \with 
				{ instrumentName = "T Sx." \RemoveEmptyStaves }
				{ \transpose bf c' \tenorSax } 

				\new Staff \with 
				{ instrumentName = "Trbn." \RemoveEmptyStaves } 
				{ \trombone }
			>>
		>>
	}
}
