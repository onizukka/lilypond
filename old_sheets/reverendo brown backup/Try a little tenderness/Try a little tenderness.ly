\version "2.18.2"
\language "english"

% Se debe incluir antes que lilyjazz. Si no, lilyjazz no funciona.
#(set-global-staff-size 19)

\include "main.ily"

\paper {
	indent = 10
	markup-system-spacing.basic-distance = 20
	bottom-margin = 15
}

\header {
	title = "Try a little tenderness"
	instrument = "Trumpet, Tenor Sax & Trombone"
	composer = "Otis Reeding"
	arranger = "Arr & Transc: gabriel@sauros.es"
	poet = ""
	tagline = "Edition: gabriel@sauros.es"
}

% Función musical global
global = {
	% Para pintar todos los números de compás debajo del pentagrama
	% \override Score.BarNumber #'break-visibility = #end-of-line-invisible
	% \override Score.BarNumber #'direction = #DOWN
	% \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

	\compressFullBarRests

	\set Timing.beamExceptions = #'()
	\set Timing.baseMoment = #(ly:make-moment 1/4)
	\set Timing.beatStructure = #'(1 1 1 1) 

}

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
	% Oculta el primer pentagrama del markup
	\override Staff.VerticalAxisGroup #'remove-first = ##t

	% Indicación de tiempo
	% \once \override Score.RehearsalMark.self-alignment-X = #LEFT
	% \once \override Score.RehearsalMark.extra-offset = #'(0 . 3) 
	% \mark \jazzTempoMarkup "Easy Swing" c4 "112"

	\mkup "(Molto legato)"
	s1*4

	\break
	\mkup "(Rubato)"
	s1*16
	\mkup "(A tempo)"
	s1*16
	s1*16

	\break
	s1*12

	\break
	s1*4
	\mkup "(Repeat 5xs)"
	s1*2

	\break
	\mkup "(Tacet 2nd & 4th xs)"
  s1*2
  \mkup "(Play all xs)"
}


% Cifrado
scoreChords = \chords {
	% c1:m f2:m g2:m7
}

tenorChords = \chords {
  s1*4
  s1*12
  g1/b
  g1:m/bf
  a1:m7
  d1:7
}

% Música
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
trumpet = \relative c'' {
	\global
	\key g \major

	d2( b4 e d2 b4 a
	g2 g e\rit)\breathe d\!

	R1*16

	\bar "||"
	R1*16

	R1*16

	d1( ~ 
	d\< 
	e2.)\> r4\!
	R1

	d1(
	b)\breathe
	e1
	R1

	R1*4

	g1\< ~ 
	g1\! 
	gs1\> 
	R1\!

	\bar "[|:"
	\repeat volta 5 {		
		r8 a4---> a8-^ r8 b4---> b8-^
		r8 c4---> c8-^ r8 cs4---> cs8-^
		r8 d4---> d8-^ r8 ds4---> ds8-^
		r8 e4---> e8 f8-- r fs-- r
	}
	\alternative {{
		g1-> 
		f2\> r2\! 
		e1\> 
		R1\!	
		\bar ":|]"
		}{
		g1->\fermata
	}}

	\bar "|."
}

tenorSax = \relative c'' {
	\global
	\key g \major
	d4( c b a
	g a g ds
	e fs e d
	cs2)\rit\breathe d\!

	R1*12

	\impro 16

	R1*16

	R1*16

	b'1( ~ 
	b\< 
	c2.)\> r4\!
	R1

	b1(
	a)\breathe
	b1
	R1

	R1*4

	d1\< ~ 
	d\! 
	b\> 
	R1\!

	\repeat volta 5 {
		r8 a4---> a8-^ r8 b4---> b8-^
		r8 c4---> c8-^ r8 cs4---> cs8-^
		r8 d4---> d8-^ r8 ds4---> ds8-^
		r8 e4---> e8 f8-- r fs-- r
	}
	\alternative {{
		b1-> 
		a2\> r2\! 
		b1\> 
		R1\!
		}{
		b1->\fermata
	}}
}

trombone = \relative c' {
	\global
	\clef bass
	\key g \major
	\override Staff.VerticalAxisGroup #'remove-first = ##t

	R1*4
	R1*16
	R1*16
	R1*16

	g1( ~ 
	g\< 
	g2.)\> r4\!
	R1

	g1(
	f)\breathe
	e1
	R1

	R1*4

	g1\< ~ 
	g1\! 
	e1\> 
	R1\!

	\repeat volta 5 {
		r8 a4---> a8-^ r8 b4---> b8-^
		r8 c4---> c8-^ r8 cs4---> cs8-^
		r8 d4---> d8-^ r8 ds4---> ds8-^
		r8 e4---> e8 f8-- r fs-- r
	}
	\alternative {{
		g1-> 
		f2\> r2\! 
		e1\> 
		R1\!
		}{
		g1->\fermata
	}}
}

% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\book {
	\paper { 
		% Descomentar para score
		% #(set-paper-size "a4" 'landscape)
		% indent = 20
		systems-per-page = 5
	}
	
	\score {
		% Descomentar para midi
		% \midi { \tempo 4 = 110 }
		
		% NO USAR layout-set-staff-size O SE ROMPE lilyjazz!!!
		\layout { }
		
		<<
			\scoreMarkup
			\transpose bf c' \scoreChords
			\new StaffGroup <<
				\new Staff \with 
				{ instrumentName = "Trpt." \RemoveEmptyStaves}
				{ \transpose bf c' \trumpet } 

				\transpose bf c' \tenorChords
				\new Staff \with 
				{ instrumentName = "T Sx." }
				{ \transpose bf c' \tenorSax } 

				\new Staff \with 
				{ instrumentName = "Trbn." \RemoveEmptyStaves } 
				{ \trombone }
			>>
		>>
	}
}
