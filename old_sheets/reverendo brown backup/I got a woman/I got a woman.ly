\version "2.18.2"
\language "english"

% Se debe incluir antes que lilyjazz. Si no, lilyjazz no funciona.
#(set-global-staff-size 19)

\include "main.ily"

\paper {
	indent = 10
	bottom-margin = 3
	markup-system-spacing.basic-distance = 20
	top-system-spacing.basic-distance = 0
}

\header {
	title = "I got a woman"
	instrument = "Trumpet, Tenor Sax & Trombone"
	composer = "Ray Charles"
	arranger = "Arr & Transc: gabriel@sauros.es"
	poet = ""
	tagline = "Edition: gabriel@sauros.es"
}

% Función musical global
global = {
	% Para pintar todos los números de compás debajo del pentagrama
	% \override Score.BarNumber #'break-visibility = #end-of-line-invisible
	% \override Score.BarNumber #'direction = #DOWN
	% \override Score.BarNumber #'extra-offset = #'(1.5 . 0)
	\compressFullBarRests

	\set Timing.beamExceptions = #'()
	\set Timing.baseMoment = #(ly:make-moment 1/4)
	\set Timing.beatStructure = #'(1 1 1 1) 
}

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
	% Oculta el primer pentagrama del markup
	\override Staff.VerticalAxisGroup #'remove-first = ##t

	% Indicación de tiempo
	% \once \override Score.RehearsalMark.self-alignment-X = #LEFT
	% \once \override Score.RehearsalMark.extra-offset = #'(0 . 3) 
	% \mark \jazzTempoMarkup "Easy Swing" c4 "112"

  \mkup "(On segno: don't repeat)"
	s1*6

	\break
	s1*6

	\break
	s1*4

	\break
	s1*8

	\break
	s1*4

	\break
	s1*4
	\eolMark \toCoda

	\break
  \mkup "(2nd x: Trombone solo)"
	\mkup "(1st x: Harmonica solo)"
	s1*4

	\break
	s1*4

	\break
	s1*4

	\break
	s1*4

	\break
  \mkup "(Repeat 8xs)"
	s1*2
  s1*1
	\eolMark \markup \underline "(D.C al Coda)"

	\break
  \mkup "(Repeat 4xs)"
	\mark \coda
	s1*4
  s1*2
}

% Cifrados
scoreChords = \chords {
	% c1:m f2:m g2:m7
}

tromboneChords = \chords {
	s1*32
	a1:6
	s1
	s1
	s1

	a1:6
	s1
	e1:7
	s1

	a1
	a1/g
	d1/fs
	f:6

	a1/e
	b2:m7 e:7
	a1
	s1
}

% Música
trumpet = \relative c'' {
	\global
	\key a \major
	
	\bar "[|:"
	\repeat volta 2 {
		r4 a4-^ r8 a8-^ r4 
		a2-> r2
		r4 gs4-^ r8 gs8-^ r4 
		gs2-> r2

		r4 a4-^ r8 a8-^ r4 
		a2-> r2
		r4 a4-^ r8 a8-^ r4 
		a2-> r2
		
		\repeat "percent" 2 {
			r4 a4 r8 a8 r4 
			a2 r2
		}
		
		\repeat "percent" 2 {
			r4 gs4 r8 gs8 r4 
			gs2 r2
		}
		
		\repeat "percent" 4 {
			r4 a4 r8 a8 r4 
			a2 r2
		}
		
		r4 a4 r8 a8 r4 
		a2 r2
		r4 gs4 r8 gs8 r4 
		gs2 r2

		r4 a4 r8 a8 r4 
		a2 r2
		r4 a4 r8 a8 r4 
		gs2 r2
	}

	\bar ":|][|:"
	R1*16
	
	
	\bar ":|][|:"
	\repeat volta 8 { 
		a4-^ r4 r2 
	}
	\alternative {
		{ r2 r8 a8-> ~ a4 \bar ":|]" }
		{ R1 }
	}
	
	\bar "[|:"
	\repeat volta 4 {
		r4\p a4-. r8 a8-. r4 
		a4-. r4 r2
	}
	\alternative {{
		r4 a4-. r8 a8-. r4 
		a4-. r4 r2
    \bar ":|]"
		}{
		R1
		r2 r4 a4-> \fermata
	}}

	\bar "|."
}

tenorSax = \relative c'' {
	\global
	\key a \major
		
	r4 fs4-^ r8 fs8-^ r4 
	fs2-> r2
	r4 e4-^ r8 ef8-^ r4 
	d2-> r2

	r4 fs4-^ r8 fs8-^ r4 
	fs2-> r2
	r4 fs4-^ r8 fs8-^ r4 
	g2-> r2
	
	\repeat "percent" 2 {
		r4 fs4 r8 fs8 r4 
		fs2 r2
	}
	r4 e4 r8 ef8 r4 
	d2 r2
	r4 d4 r8 d8 r4 
	d2 r2
	
	\repeat "percent" 4 {
		r4 fs4 r8 fs8 r4 
		fs2 r2
	}
	
	r4 fs4 r8 fs8 r4 
	fs2 r2
	r4 e?4 r8 ef8 r4 
	d2 r2

	r4 fs4 r8 fs8 r4 
	fs2 r2
	r4 fs4 r8 fs8 r4 
	e2 r2
	
	R1*16
	
	\repeat volta 8 { 
		cs4-^ r4 r2 
	}
	\alternative {
		{ r2 r8 d8-> ~ d4 }
		{ R1 }
	}

	
	
	\repeat volta 4 {
		r4\p fs4-. r8 fs8-. r4 
		fs4-. r4 r2
	}
	\alternative {{
		r4 fs4-. r8 fs8-. r4 
		fs4-. r4 r2
		}{
		R1
		r2 r4 cs4-> \fermata
	}}
}

trombone = \relative c' {
	\global
	\clef bass
	\key a \major
	
	r4 cs4-^ r8 cs8-^ r4 
	cs2-> r2
	r4 b4-^ r8 b8-^ r4 
	b2-> r2

	r4 cs4-^ r8 cs8-^ r4 
	c2-> r2
	r4 cs?4-^ r8 cs8-^ r4 
	b2-> r2
	
	\repeat "percent" 2 {
		r4 c4 r8 c8 r4 
		c2 r2
	}
	
	r4 b4 r8 b8 r4 
	b2 r2
	r4 e,4 r8 e8 r4 
	e2 r2

	\repeat "percent" 2 {
		r4 cs'?4 r8 cs8 r4 
		cs2 r2
	}
	
	\repeat "percent" 2 {
		r4 c4 r8 c8 r4 
		c2 r2
	}
	
	r4 cs4 r8 cs8 r4 
	cs2 r2
	r4 b r8 b8 r4 
	b2 r2

	r4 cs4 r8 cs8 r4 
	c2 r2
	r4 cs4 r8 c8 r4 
	b2 r2
	
	\impro 64
	
	\repeat volta 8 { 
		a,4-^ r4 r2 
	}
	\alternative {
		{ r2 r8 bf8-> ~ bf4 }
		{ R1 }
	}
	
	\repeat volta 7 {
		r4\p cs'4-. r8 cs8-. r4 
		cs4-. r4 r2
	}
	\alternative {{
		r4 cs4-. r8 cs8-. r4 
		cs4-. r4 r2
		}{
		R1
		r2 r4 g4-> \fermata
	}}
}

% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\book {
	\paper { 
		% Descomentar para score
		% #(set-paper-size "a4" 'landscape)
		% indent = 20
		% systems-per-page = 2
	}
	
	\score {
		% Descomentar para midi
		% \midi { \tempo 4 = 110 }
		
		% NO USAR layout-set-staff-size O SE ROMPE lilyjazz!!!
		\layout { }
		
		<<
			\scoreMarkup
			\transpose bf c' \scoreChords
			\new StaffGroup <<
				\new Staff \with 
				{ instrumentName = "Trpt." \RemoveEmptyStaves }
				{ \transpose bf c' \trumpet } 

				\new Staff \with 
				{ instrumentName = "T Sx." \RemoveEmptyStaves }
				{ \transpose bf c' \tenorSax } 

				\tromboneChords
				\new Staff \with 
				{ instrumentName = "Trbn." } 
				{ \trombone }
			>>
		>>
	}
}
