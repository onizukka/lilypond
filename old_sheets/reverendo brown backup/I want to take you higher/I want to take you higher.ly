\version "2.18.2"
\language "english"

% Se debe incluir antes que lilyjazz. Si no, lilyjazz no funciona.
#(set-global-staff-size 19)

\include "main.ily"

\paper {
	indent = 10
}

\header {
	title = "I want to take you higher"
	instrument = "Trumpet, Tenor Sax & Trombone"
	composer = "Sly and the family stone"
	arranger = "Transc: gabriel@sauros.es"
	poet = ""
	tagline = "Edition: gabriel@sauros.es"
}

% Función musical global
global = {
	% Para pintar todos los números de compás debajo del pentagrama
	% \override Score.BarNumber #'break-visibility = #end-of-line-invisible
	% \override Score.BarNumber #'direction = #DOWN
	% \override Score.BarNumber #'extra-offset = #'(1.5 . 0)
	\compressFullBarRests

	\set Timing.beamExceptions = #'()
	\set Timing.baseMoment = #(ly:make-moment 1/4)
	\set Timing.beatStructure = #'(1 1 1 1) 

  \key a \minor
}

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
	% Oculta el primer pentagrama del markup
	\override Staff.VerticalAxisGroup #'remove-first = ##t

	% Indicación de tiempo
	% \once \override Score.RehearsalMark.self-alignment-X = #LEFT
	% \once \override Score.RehearsalMark.extra-offset = #'(0 . 3) 
	% \mark \jazzTempoMarkup "Easy Swing" c4 "112"

  s1*10
  \mkup "(Repeat 4 xs last D.C.)"
  s1*3
  \eolMark \markup \underline \fontsize #2 "(D.C. ad.lib.)"
}

% Cifrados
scoreChords = \chords {
	% c1:m f2:m g2:m7
}

% Música
trumpet = \transpose a g \relative c'' {
	\global

  a1\p\< ~
  a1 ~
  a2 r4\! a'-^\f
  R1
  \bar "||"
  R1*6

  \bar "[|:"
  \repeat volta 4 {
    a8--->\f r8 r4 r8 g---> r4
    r4 r16 g a8---> g4---^ r
  }
  \bar ":|]"

  R1
}

tenorSax = \transpose a g \relative c'' {
	\global

  a1\p\< ~
  a1 ~
  a2 r4\! a'-^\f
  R1
  R1*6

  \bar "[|:"
  \repeat volta 4 {
    c8--->\f r8 r4 r8 c---> r4
    r4 r16 c d8---> c4---^ r
  }
  \bar ":|]"

  R1
}

trombone = \transpose a g \relative c' {
	\global
  \clef bass

  a'1\p\< ~
  a1 ~
  a2 r4\! a-^\f
  R1
  R1*6

  \bar "[|:"
  \repeat volta 4 {
    a8--->\f r8 r4 r8 g---> r4
    r4 r16 g fs8---> e4---^ r
  }
  \bar ":|]"

  R1
  \bar "|."
}

% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\book {
	\paper { 
		% Descomentar para score
		% #(set-paper-size "a4" 'landscape)
		% indent = 20
		% systems-per-page = 2
	}
	
	\score {
		% Descomentar para midi
		% \midi { \tempo 4 = 110 }
		
		% NO USAR layout-set-staff-size O SE ROMPE lilyjazz!!!
		\layout { }
		
		<<
			\scoreMarkup
			\transpose bf c' \scoreChords
			\new StaffGroup <<
				\new Staff \with 
				{ instrumentName = "Trpt."}
				{ \transpose bf c' \trumpet } 

				\new Staff \with 
				{ instrumentName = "T Sx." \RemoveEmptyStaves }
				{ \transpose bf c' \tenorSax } 

				\new Staff \with 
				{ instrumentName = "Trbn." \RemoveEmptyStaves } 
				{ \trombone }
			>>
		>>
	}
}
