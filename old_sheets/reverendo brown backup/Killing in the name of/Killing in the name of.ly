\version "2.18.2"
\language "english"

% Se debe incluir antes que lilyjazz. Si no, lilyjazz no funciona.
#(set-global-staff-size 19)

\include "main.ily"

\paper {
	indent = 10
}

\header {
	title = "Killing in the name of"
	instrument = "Trumpet, Tenor Sax & Trombone"
	composer = ""
	arranger = "Transc: gabriel@sauros.es"
	poet = ""
	tagline = "Edition: gabriel@sauros.es"
}

% Función musical global
global = {
	% Para pintar todos los números de compás debajo del pentagrama
	% \override Score.BarNumber #'break-visibility = #end-of-line-invisible
	% \override Score.BarNumber #'direction = #DOWN
	% \override Score.BarNumber #'extra-offset = #'(1.5 . 0)
	\compressFullBarRests

	\set Timing.beamExceptions = #'()
	\set Timing.baseMoment = #(ly:make-moment 1/4)
	\set Timing.beatStructure = #'(1 1 1 1) 

  \key d \minor
}

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
	% Oculta el primer pentagrama del markup
	\override Staff.VerticalAxisGroup #'remove-first = ##t

	% Indicación de tiempo
	% \once \override Score.RehearsalMark.self-alignment-X = #LEFT
	% \once \override Score.RehearsalMark.extra-offset = #'(0 . 3) 
	\mark \jazzTempoMarkup "Easy Swing" c4 "112"

  s1*8
  s1*8
	\mark \jazzTempoMarkup "" c4 "90"

  s1*4
  \mark \segno
  s1*3
  \mkup "(Play only on segno)"
  s1*5

  s1*4
  \mkup "(Repeat 8 xs)"
  s1*5
  \mkup "(Repeat 4 xs)"
  s1
  \mkup "(Repeat 4 xs)"
  s1
  \eolMark \toSegno
  s1*8
  \mkup "(2nd x: Tenor Sax)"
  \mkup "(1st x: Keyboard)"
  s1*4
  \mark \markup "(On tenor sax solo, only Trumpet and Bone)"
  \mkup "(Backgrounds)"
  s1*12

  \mkup "(Repeat 4 xs)"
  s1
  \mkup "(Repeat 4 xs)"
  s1
  \mark \markup "(Exxagerate crescendos)"
  \mkup "(Repeat 4 xs)"
}

% Cifrados
scoreChords = \chords {
	% c1:m f2:m g2:m7
  s1*47
  d1:m
}

% Música
trumpet = \relative c'' {
  \global
  
  d,1 \f
  d1
  d1
  d1
  
  R1*4

  \break
  \bar "[|:"
  \repeat volta 2 {
    \repeat percent 3 {
      r2 cs'8( d-.) fs( g-.)
    }
    r2 cs,8( d-.) af'( g-.)
  }
  \bar ":|]"

  \break
  \repeat percent 3 {
    \times 2/3 {d,4---> d---> d--->} \times 2/3 {ef'4---> ef---> ef--->}
  }
  \times 2/3 {d,4---> d---> d--->} ef'---> r

  \break
  \repeat percent 3 {
    r2 r4 \mf d8 \bendBefore d8 \bendBefore 
  }
  R1


  \break
  \bar "||"
  R1*3
  r2 g,16\< gs a c cs d f8--->\!
  R1*4

  \break
  r4 r8 \mf b,---> r4 r8 as--->
  r4 r8 a?---> r8 a---> a[---> a]--->
  r4 r8 b---> r4 r8 bf--->
  r4 r8 a---> d16-.\< d-. d-. d-. d-. d-. d-. d-.\!

  \break
  \bar "[|:"
  \repeat volta 8 {
    d16( \f d-.) r8 d8---> r8 r2
  }
  \bar ":|]"
  \repeat percent 3 {
    d16( d-.) r8 d8---> r8 r8 r16 d-> ~ d4 \bendAfter #-6
  }
  d16( d-.) r8 d8---> r8 d2->\ff^"Shake"

  \break
  \bar "[|:"
  \repeat volta 4 {
    d8-. \mf \stopTrillSpan r d-- ~ d16 d16-. ~ d16 d( f,8-.) g-. c-.
  }
  \bar ":|][|:"
  \repeat volta 4 {
    d8-. r d-- ~ d16 d16-. ~ d16 d( f,8-.) g-. c-.
  }
  \bar ":|]"

  \break
  d8---> \f r c---> r a---> r g---> r 
  d8---> r c---> r f4---> \startTrillSpan r4 \stopTrillSpan
  d'8---> \stopTrillSpan r c---> r a---> r g---> r 
  d8---> r c---> r d4---> \startTrillSpan r4 \stopTrillSpan

  \break
  a'16 c-> ~ c8 ~ c8 \bendAfter #-3 r a16 c-> ~ c8 ~ c8 \bendAfter #-3 r
  g16 a-> ~ a8 ~ a8 \bendAfter #-3 r g16 a-> ~ a8 ~ a8 \bendAfter #-3 r
  d2\sfp \< e\sfp \<
  c2.\sfp \< r4\!

  \pageBreak
  \bar "[|:"
  \repeat volta 2 {
    R1*4
    r2 r4 \mf d8 \bendBefore d8 \bendBefore 
    r2 r4 d8 \bendBefore d8 \bendBefore 
    r2 r4 d8 \bendBefore d8 \bendBefore 
    r1
  }
  \bar ":|]"

  \break
  d,1( \mp
  e) \breathe
  f( 
  g) \breathe
  a( 
  bf) \breathe
  c \breathe
  d \<

  \break
  \bar "[|:"
  \repeat volta 4 {
    d8-.\f r d-- ~ d16 d16-. ~ d16 d( f,8-.) g-. c-.
  }
  \bar ":|][|:"
  \repeat volta 4 {
    d8-. r d-- ~ d16 d16-. ~ d16 d( f,8-.) g-. c-.
  }
  \bar ":|][|:"

  \break
  \repeat volta 8 {
    d8-^ r d16(\p\< d d d-.) \f r8 d16(\p\< d d d-.) \f r8
  }
  \bar ":|]"

  \break
  d,2\< d'\ff
  \slurDown
  \times 2/3 {d,4---> d---> d--->} \times 2/3 {ef'4---> ef---> ef--->}

  \bar "|."
}

tenorSax = \relative c'' {
  \global
  
  d,1\f
  d1
  d1
  d1
  
  R1*4
  
  \break
  \repeat volta 2 {
    \repeat percent 3 {
      \times 2/3 {d4-. d-. d-.} c'8( df-.) f( gf-.)
    }
    \times 2/3 {d,4-. d-. d-.} c'8( df-.) g?( gf-.)
  }
  
  \break
  \repeat percent 3 {
    \times 2/3 {d,4---> d---> d--->} \times 2/3 {ef'4---> ef---> ef--->}
  }
  \times 2/3 {d,4---> d---> d--->} ef'---> r
  
  \break
  \repeat percent 3 {
    r2 r4 \mf a8 \bendBefore a8 \bendBefore 
  }
  R1
  
  \break
  R1*3
  r2 g,16\< gs a c cs d f8--->\!
  R1*4
  
  \break
  r4 r8 \mf g---> r4 r8 fs--->
  r4 r8 f---> r8 d---> df[---> c]--->
  r4 r8 g---> r4 r8 fs--->
  r4 r8 f---> d16-.\< d-. d-. d-. d-. d-. d-. d-.\!
  
  \break
  \bar "[|:"
  \repeat volta 8 {
    d16( \f d-.) r8 d8---> r8 r2
  }
  \bar ":|]"
  \repeat percent 3 {
    d16( d-.) r8 d8---> r8 r8 r16 d-> ~ d4 \bendAfter #-6
  }
  d16( d-.) r8 d8---> r8 d'2->\ff
  
  \break
  \bar "[|:"
  \repeat volta 4 {
    d8-. \mf r d-- ~ d16 d16-. ~ d16 d( f,8-.) g-. c-.
  }
  \bar ":|][|:"
  \repeat volta 4 {
    a'8-. r a-- ~ a16 a16-. ~ a16 a( c,8-.) d-. g-.
  }
  \bar ":|]"
  
  \break
  d'(---> \f d,) c'(---> c,) a'(---> a,) g'(---> g,)
  d'(---> f,) c'(---> ef,) f4---> \startTrillSpan r4 \stopTrillSpan
  d''8(---> d,) c'(---> c,) a'(---> a,) g'(---> g,)
  d'(---> f,) c'(---> ef,) d4---> \startTrillSpan r4 \stopTrillSpan
  
  \break
  c'16 ef-> ~ ef8 ~ ef8 \bendAfter #-3 r c16 ef-> ~ ef8 ~ ef8 \bendAfter #-3 r
  bf16 c-> ~ c8 ~ c8 \bendAfter #-3 r bf16 c ~ c8 ~ c8 \bendAfter #-3 r
  c2\sfp \< d\sfp \<
  gs2.\sfp \< r4\!

  \break
  \bar "[|:"
  \repeat volta 2 {
    \impro 16
    r2 r4 a8 \mf \bendBefore a8 \bendBefore 
    r2 r4 a8 \bendBefore a8 \bendBefore 
    r2 r4 a8 \bendBefore a8 \bendBefore 
    r1
  }
  \bar ":|]"
  
  \break
  d,,1( \mp
  e) \breathe
  f( 
  g) \breathe
  a( 
  bf) \breathe
  c \breathe
  d \<
  
  \break
  \bar "[|:"
  \repeat volta 4 {
    d8-.\f r d-- ~ d16 d16-. ~ d16 d( f,8-.) g-. c-.
  }
  \bar ":|][|:"
  \repeat volta 4 {
    a'8-. r a-- ~ a16 a16-. ~ a16 a( c,8-.) d-. g-.
  }
  \bar ":|]"
  
  \break
  \bar ":|][|:"
  \repeat volta 8 {
    ef8-^ r ef16(\p\< ef ef ef-.) \f r8 ef16(\p\< ef ef ef-.) \f r8
  }
  \bar ":|]"
  
  \break
  d,2\< d'\ff
  \slurDown
  \times 2/3 {d,4---> d---> d--->} \times 2/3 {ef'4---> ef---> ef--->}
  
  \bar "|."
}

trombone = \relative c' {
  \global
  \clef bass
  
  d,1 \f 
  d1
  d1
  d1
  
  R1*4
  
  \break
  \repeat volta 2 {
    \repeat percent 3 {
      \times 2/3 {d4-. d-. d-.} cs'8( d-.) fs( g-.)
    }
    \times 2/3 {d,4-. d-. d-.} cs'8( d-.) af'( g-.)
  }
  
  \break
  \repeat percent 3 {
    \times 2/3 {d,4---> d---> d--->} \times 2/3 {ef'4---> ef---> ef--->}
  }
  \times 2/3 {d,4---> d---> d--->} ef'---> r
  
  \break
  \repeat percent 3 {
    r2 r4 \mf f8 \bendBefore f8 \bendBefore 
  }
  R1
  
  \break
  R1*3
  r2 g,16\< gs a c cs d f8--->\!
  R1*4
  
  \break
  d,8 \mf c16 d ~ d d f8---> d8 c16 d ~ d d e8--->
  d8 c16 d ~ d d ef8---> r16 d f8---> e[---> ef]--->
  d8 c16 d ~ d d f8---> d8 c16 d ~ d d e8--->
  d8 c16 d ~ d d ef8---> c16-.\< c-. c-. c-. c-. c-. c-. c-.\!
  
  \break
  \bar "[|:"
  \repeat volta 8 {
    d16(\f d-.) r8 d8---> r8 r2
  }
  \bar ":|]"
  \repeat percent 3 {
    d16( d-.) r8 d8---> r8 r8 r16 d-> ~ d4 \bendAfter #-6
  }
  d16( d-.) r8 d8---> r8 d'2->^"Shake"
  
  \break
  \bar "[|:"
  \repeat volta 4 {
    d,8-.\mf r d-- ~ d16 d16-. ~ d16 d( f,8-.) g-. c-.
  }
  \bar ":|][|:"
  \repeat volta 4 {
    f'8-. r f-- ~ f16 f16-. ~ f16 f( a,8-.) b-. e-.
  }
  \bar ":|]"
  
  \break
  r8 \f d---> r c---> r a---> r g--->
  r d---> r c f4---> \startTrillSpan r4 \stopTrillSpan
  r8 \stopTrillSpan d'---> r c---> r a---> r g--->
  r d---> r c d4---> \startTrillSpan r4 \stopTrillSpan
  
  \break
  r16 af'-> ~ af8 ~ af8 \bendAfter #-3 r r16 af-> ~ af8 ~ af8 \bendAfter #-3 r
  r16 fs-> ~ fs8 ~ fs8 \bendAfter #-3 r r16 fs ~ fs8 ~ fs8 \bendAfter #-3 r
  af2\sfp \< bf\sfp \<
  d2.\sfp \< r4\!

  \break
  \bar "[|:"
  \repeat volta 2 {
    R1*4
    r2 r4 \mf f8 \bendBefore f8 \bendBefore 
    r2 r4 f8 \bendBefore f8 \bendBefore 
    r2 r4 f8 \bendBefore f8 \bendBefore 
    r1
  }
  \bar ":|]"
  
  \break
  d,1(\mp
  e) \breathe
  f( 
  g) \breathe
  a( 
  bf) \breathe
  c \breathe
  d \<
  
  \break
  \bar "[|:"
  \repeat volta 4 {
    d,8-. \f r d-- ~ d16 d16-. ~ d16 d( f,8-.) g-. c-.
  }
  \bar ":|][|:"
  \repeat volta 4 {
    f'8-. r f-- ~ f16 f16-. ~ f16 f( a,8-.) b-. e-.
  }
  \bar ":|]"
  
  \break
  \bar ":|][|:"
  \repeat volta 8 {
    c8-^ r c16(\p\< c c c-.) \f r8 c16(\p\< c c c-.) \f r8
  }
  \bar ":|]"
  
  \break
  d,2\< d'\ff
  \slurDown
  \times 2/3 {d,4---^ d---^ d---^} \times 2/3 {ef'4---^ ef---^ ef---^}
  
  \bar "|."
}

% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\book {
	\paper { 
		% Descomentar para score
		% #(set-paper-size "a4" 'landscape)
		% indent = 20
		% systems-per-page = 2
	}
	
	\score {
		% Descomentar para midi
		% \midi { \tempo 4 = 110 }
		
		% NO USAR layout-set-staff-size O SE ROMPE lilyjazz!!!
		\layout { }
		
		<<
			\scoreMarkup
			\transpose bf c' \scoreChords
			\new StaffGroup <<
				\new Staff \with 
				{ instrumentName = "Trpt." \RemoveEmptyStaves }
				{ \transpose bf c' \trumpet } 

				\new Staff \with 
				{ instrumentName = "T Sx." \RemoveEmptyStaves }
				{ \transpose bf c' \tenorSax } 

				\new Staff \with 
				{ instrumentName = "Trbn." \RemoveEmptyStaves } 
				{ \trombone }
			>>
		>>
	}
}
