\version "2.18.2"
\language "english"

% Se debe incluir antes que lilyjazz. Si no, lilyjazz no funciona.
#(set-global-staff-size 19)

\include "main.ily"

\paper {
	indent = 10
}

\header {
	title = "Let's stay together"
	instrument = "Trumpet, Tenor Sax & Trombone"
	composer = "Al Green"
	arranger = "Arr & Transc: gabriel@sauros.es"
	poet = ""
	tagline = "Edition: gabriel@sauros.es"
}

% Función musical global
global = {
	% Para pintar todos los números de compás debajo del pentagrama
	% \override Score.BarNumber #'break-visibility = #end-of-line-invisible
	% \override Score.BarNumber #'direction = #DOWN
	% \override Score.BarNumber #'extra-offset = #'(1.5 . 0)
	\compressFullBarRests

	\set Timing.beamExceptions = #'()
	\set Timing.baseMoment = #(ly:make-moment 1/4)
	\set Timing.beatStructure = #'(1 1 1 1) 
}

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
	% Oculta el primer pentagrama del markup
	\override Staff.VerticalAxisGroup #'remove-first = ##t

	% Indicación de tiempo
	% \once \override Score.RehearsalMark.self-alignment-X = #LEFT
	% \once \override Score.RehearsalMark.extra-offset = #'(0 . 3) 
	% \mark \jazzTempoMarkup "Easy Swing" c4 "112"

  \mkup "(Soft)"
	s1*4

	\break
	s1*32
  \eolMark \markup \underline \fontsize #2 "(Sharp!)"

	\break
	s1*8

	\break
  s1*6
  \mkup "(Soft)"
	s1*18

	\break
	s1*2
  s1*4
  s1*2


	\break
	s1*6
  s1*2

	\break
  s1*5
  \eolMark \markup \underline \fontsize #2 "(Emphasize Tenor Sax)"
}

% Cifrados
scoreChords = \chords {
	% c1:m f2:m g2:m7
}

% Música
trumpet = \relative c'' {
	\global
	\key f \major

	r4 \mf a-> r8 g-. g4->
	r4 f-> r8 g-> r4
	r4 a-> r8 g-. g4->
	r4 f-> r8 e-> r4
	\bar "||"

	R1*16
	\bar "||"
	R1*15
	r2 r4 r8 \f cs'(
	\bar "||"

	d4-.) r r2
	r2 r4 r8 gs,(
	a4-.) r r2
	R1

	R1*4

	R1*6
	r8 \mf bf-> r4 r8 a-> r4
	r8 d-> r4 r8 c-> r4

	R1*16
	\bar "||"

	R1
	r2 r4 r8 \f gs(
	a4-.) r r2
	R1

	R1*2
	r8 \mf a-> r4 r8 g-> r4
	r8 f-> r4 r8 e-> r8 \f cs'(
	\bar "||"

	d4-.) r r2
	r2 r4 r8 gs,(
	a4-.) r r2
	R1

	R1*2	
	r8 \mf a-> r4 r8 g-> r4
	r8 f-> r4 r8 e-> r4


	r8 a-> r4 r8 g-> r4
	r8 f-> r4 r8 e-> r4
	r8 a-> r4 r8 g-> r g~->
	g2\fermata r2
	
	e1\fermata

	\bar "|."
}

tenorSax = \relative c'' {
	\global\key f \major

	r4 \mf f-> r8 e-. e4->
	r4 d-> r8 e-> r4
	r4 f-> r8 e-. e4->
	r4 d-> r8 c-> r4

	R1*16
	R1*15
	r2 r4 r8 \f gs'(

	a4-.) r r2
	r2 r4 r8 ds,(
	e4-.) r r2
	R1

	R1*4

	R1*6
	r8 \mf f-> r4 r8 e-> r4
	r8 a-> r4 r8 g-> r4

	R1*16

	R1
	r2 r4 r8 \f ds(
	e4-.) r r2
	R1

	R1*2
	r8 \mf f-> r4 r8 e-> r4
	r8 d-> r4 r8 c-> r8 \f gs'(

	a4-.) r r2
	r2 r4 r8 ds,(
	e4-.) r r2
	R1

	R1*2	
	r8 \mf f-> r4 r8 e-> r4
	r8 d-> r4 r8 c-> r4

	r8 f-> r4 r8 e-> r4
	r8 d-> r4 r8 c-> r4
	r8 f-> r4 r8 e-> r c~->
	c2\fermata r2
	
	g1\fermata
}

trombone = \relative c' {
	\global
	\clef bass
	\key f \major

	r4 \mf d-> r8 c-. c4->
	r4 bf-> r8 c-> r4
	r4 d-> r8 c-. c4->
	r4 bf-> r8 bf-> r4

	R1*16
	R1*15
	r2 r4 r8 \f a(

	bf4-.) r r2
	r2 r4 r8 b(
	c4-.) r r2
	R1

	R1*4

	R1*6
	r8 \mf d-> r4 r8 c-> r4
	r8 f-> r4 r8 e-> r4

	R1*16

	R1
	r2 r4 r8 \f b(
	c4-.) r r2
	R1

	R1*2
	r8 \mf d-> r4 r8 c-> r4
	r8 bf-> r4 r8 bf-> r8 \f a(

	bf4-.) r r2
	r2 r4 r8 b(
	c4-.) r r2
	R1

	R1*2
	r8 \mf d-> r4 r8 c-> r4
	r8 bf-> r4 r8 bf-> r4

	r8 d-> r4 r8 c-> r4
	r8 bf-> r4 r8 bf-> r4
	r8 d-> r4 r8 a-> r bf~->
	bf2\fermata r2
	
	f1\fermata
}

% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\book {
	\paper { 
		% Descomentar para score
		% #(set-paper-size "a4" 'landscape)
		% indent = 20
		% systems-per-page = 2
	}
	
	\score {
		% Descomentar para midi
		% \midi { \tempo 4 = 110 }
		
		% NO USAR layout-set-staff-size O SE ROMPE lilyjazz!!!
		\layout { }
		
		<<
			\scoreMarkup
			\transpose bf c' \scoreChords
			\new StaffGroup <<
				\new Staff \with 
				{ instrumentName = "Trpt."}
				{ \transpose bf c' \trumpet } 

				\new Staff \with 
				{ instrumentName = "T Sx." \RemoveEmptyStaves }
				{ \transpose bf c' \tenorSax } 

				\new Staff \with 
				{ instrumentName = "Trbn." \RemoveEmptyStaves } 
				{ \trombone }
			>>
		>>
	}
}
