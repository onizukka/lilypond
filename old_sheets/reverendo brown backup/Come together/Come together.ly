\version "2.18.2"
\language "english"

% Se debe incluir antes que lilyjazz. Si no, lilyjazz no funciona.
#(set-global-staff-size 19)

\include "main.ily"

\paper {
  indent = 10
}

\header {
  title = "Come together"
  instrument = "Trumpet, Tenor Sax & Trombone"
  composer = "Ike & Tina"
  arranger = "Transc: gabriel@sauros.es"
  poet = ""
  tagline = "Edition: gabriel@sauros.es"
}

% Función musical global
global = {
  % Para pintar todos los números de compás debajo del pentagrama
  % \override Score.BarNumber #'break-visibility = #end-of-line-invisible
  % \override Score.BarNumber #'direction = #DOWN
  % \override Score.BarNumber #'extra-offset = #'(1.5 . 0)
  \compressFullBarRests

  \set Timing.beamExceptions = #'()
  \set Timing.baseMoment = #(ly:make-moment 1/4)
  \set Timing.beatStructure = #'(1 1 1 1) 
}

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
  % Oculta el primer pentagrama del markup
  \override Staff.VerticalAxisGroup #'remove-first = ##t

  % Indicación de tiempo
  % \once \override Score.RehearsalMark.self-alignment-X = #LEFT
  % \once \override Score.RehearsalMark.extra-offset = #'(0 . 3) 
  % \mark \jazzTempoMarkup "Easy Swing" c4 "112"

  \part "Intro"
  s1*4

  \part "Verse"
  s1*8
  \part "Bridge"
  s1*4

  \part "Verse"
  s1*8
  \part "hook"
  s1*2
  \part "Bridge"
  s1*4

  \part "Guitar solo"
  s1*8
  \part "Bridge"
  s1*4

  \part "Verse"
  s1*8
  \part "hook"
  s1*2
  \part "Bridge"
  s1*4

  \part "Refrain"
  s1*8
  s1*2
  s1*2
  \part "Outro"
  s1*4
  s1*2
}

% Cifrados
scoreChords = \chords {
  % c1:m f2:m g2:m7
}

% Música
trumpet = \relative c'' {
  \global
  \key g \minor

  R1*4
  \bar "||"
  R1*8

  \break
  r4 r8 f,~->\fp\< f4 d4-^\!
  r4 r8 f~->\fp\< f4 d4-^\!
  r4 r8 f~->\fp\< f4 d4-^\!
  r4 r8 f'~->\f f4 d4-^\!

  \bar "[|:"
  \repeat volta 2 {
    \break
    R1*8
    R1*2

    \break
    r4 r8 f,~->\fp\< f4 d4-^\!
    r4 r8 f~->\fp\< f4 d4-^\!
    r4 r8 f~->\fp\< f4 d4-^\!
    r4 r8 f'~->\f f4 d4-^\!
  }
  \bar ":|]"

  \break
  R1*8

  \break
  r4 r8 f,~->\fp\< f4 d4-^\!
  r4 r8 f~->\fp\< f4 d4-^\!
  r4 r8 f~->\fp\< f4 d4-^\!
  r4 r8 f'~->\f f4 d4-^\!

  \break
  R1*8
  R1*2

  \break
  r4 r8 f,~->\fp\< f4 d4-^\!
  r4 r8 f~->\fp\< f4 d4-^\!
  r4 r8 f~->\fp\< f4 d4-^\!
  r4 r8 f'~->\f f4 d4-^\!

  \break
  R1*8
  \bar "[|:"
  \repeat volta 2 {
    r4 g,2.~\fp->
    g2.\< r4\!
  }
  \bar ":|][|:"
  \repeat volta 2 {
    r4 g'2.~\fp->
    g2.\< r4\!
  }
  \bar ":|]"

  \break
  R1*4
  r4 r8 f,~->\fp\< f4 d4-^\!
  r4 r8 f~-> f2\!\fermata

  \bar "|."
}

tenorSax = \relative c'' {
  \global
  \key g \minor

  R1*4

  R1*8

  r4 r8 bf~->\fp\< bf4 g4-^\!
  r4 r8 bf~->\fp\< bf4 g4-^\!
  r4 r8 bf~->\fp\< bf4 g4-^\!
  r4 r8 bf'~->\f bf4 g4-^\!

  \repeat volta 2 {
    R1*8
    R1*2

    r4 r8 bf,~->\fp\< bf4 g4-^\!
    r4 r8 bf~->\fp\< bf4 g4-^\!
    r4 r8 bf~->\fp\< bf4 g4-^\!
    r4 r8 bf'~->\f bf4 g4-^\!
  }

  R1*8

  r4 r8 bf,~->\fp\< bf4 g4-^\!
  r4 r8 bf~->\fp\< bf4 g4-^\!
  r4 r8 bf~->\fp\< bf4 g4-^\!
  r4 r8 bf'~->\f bf4 g4-^\!

  R1*8
  R1*2

  r4 r8 bf,~->\fp\< bf4 g4-^\!
  r4 r8 bf~->\fp\< bf4 g4-^\!
  r4 r8 bf~->\fp\< bf4 g4-^\!
  r4 r8 bf'~->\f bf4 g4-^\!

  R1*8

  \repeat volta 2 {
    r4 d2.~\fp->
    d2.\< r4\!
  }

  \repeat volta 2 {
    r4 d'2.~\fp->
    d2.\< r4\!
  }

  R1*4

  r4 r8 bf,~->\fp\< bf4 g4-^\!
  r4 r8 bf~-> bf2\fermata
}

trombone = \relative c' {
  \global
  \clef bass
  \key g \minor

  R1*4

  R1*8

  r4 r8 f,~->\fp\< f4 d4-^\!
  r4 r8 f~->\fp\< f4 d4-^\!
  r4 r8 f~->\fp\< f4 d4-^\!
  r4 r8 f'~->\f f4 d4-^\!

  \repeat volta 2 {
    R1*8
    R1*2

    r4 r8 f,~->\fp\< f4 d4-^\!
    r4 r8 f~->\fp\< f4 d4-^\!
    r4 r8 f~->\fp\< f4 d4-^\!
    r4 r8 f'~->\f f4 d4-^\!
  }

  R1*8

  r4 r8 f,~->\fp\< f4 d4-^\!
  r4 r8 f~->\fp\< f4 d4-^\!
  r4 r8 f~->\fp\< f4 d4-^\!
  r4 r8 f'~->\f f4 d4-^\!

  R1*8
  R1*2

  r4 r8 f,~->\fp\< f4 d4-^\!
  r4 r8 f~->\fp\< f4 d4-^\!
  r4 r8 f~->\fp\< f4 d4-^\!
  r4 r8 f'~->\f f4 d4-^\!

  R1*8

  \repeat volta 2 {
    r4 g,2.~\fp->
    g2.\< r4\!
  }

  \repeat volta 2 {
    r4 g'2.~\fp->
    g2.\< r4\!
  }

  R1*4

  r4 r8 f,~->\fp\< f4 d4-^\!
  r4 r8 f~-> f2\fermata
}

% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\book {
  \paper { 
    % Descomentar para score
    % #(set-paper-size "a4" 'landscape)
    % indent = 20
    % systems-per-page = 2
  }

  \score {
    % Descomentar para midi
    % \midi { \tempo 4 = 110 }

    % NO USAR layout-set-staff-size O SE ROMPE lilyjazz!!!
    \layout { }

    <<
      \scoreMarkup
      \transpose bf c' \scoreChords
      \new StaffGroup <<
        \new Staff \with 
        { instrumentName = "Trpt." }
        { \transpose bf c' \trumpet } 

        \new Staff \with 
        { instrumentName = "T Sx." \RemoveEmptyStaves }
        { \transpose bf c' \tenorSax } 

        \new Staff \with 
        { instrumentName = "Trbn." \RemoveEmptyStaves } 
        { \trombone }
      >>
    >>
  }
}
