\version "2.18.2"
\language "english"

% Se debe incluir antes que lilyjazz. Si no, lilyjazz no funciona.
#(set-global-staff-size 19)

\include "main.ily"

\paper {
	indent = 10
}

\header {
	title = "Fire & water"
	instrument = "Trumpet, Tenor Sax & Trombone"
	composer = "Wilson Pickett"
	arranger = "Transc: gabriel@sauros.es"
	poet = ""
	tagline = "Edition: gabriel@sauros.es"
}

% Función musical global
global = {
	% Para pintar todos los números de compás debajo del pentagrama
	% \override Score.BarNumber #'break-visibility = #end-of-line-invisible
	% \override Score.BarNumber #'direction = #DOWN
	% \override Score.BarNumber #'extra-offset = #'(1.5 . 0)
	\compressFullBarRests

	\set Timing.beamExceptions = #'()
	\set Timing.baseMoment = #(ly:make-moment 1/4)
	\set Timing.beatStructure = #'(1 1 1 1) 
}

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
	% Oculta el primer pentagrama del markup
	\override Staff.VerticalAxisGroup #'remove-first = ##t

	% Indicación de tiempo
	% \once \override Score.RehearsalMark.self-alignment-X = #LEFT
	% \once \override Score.RehearsalMark.extra-offset = #'(0 . 3) 
	% \mark \jazzTempoMarkup "Easy Swing" c4 "112"
	s1*25

	\mark \segno
	s1*18

	\eolMark \toCoda
	s1*3
	% \once \override Score.RehearsalMark.self-alignment-X = #RIGHT
  \mark \markup {
    \column { 
      \null
      \underline \fontsize #2 "(Repeat 4xs)"
      \null
    }
    \column {
      \underline "(1st & 2nd x: Guitar)"
      \underline "(3rd x: Harp)"
      \underline "(4th x: T.Sax)"
    }
  }
	s1*8

	\eolMark \dalSegnoAlCoda
	\mark \coda
}

% Cifrados
scoreChords = \chords {
  s1*46
	c1:m s1 f1:7 s1
	c1:m s1 f1:7 s1
}

% Música
trumpet = \relative c'' {
	\global
	\key c \minor


	R1*3
	r4\f ef16 ef ef ef ef8(-> d)-. c4-^

	R1*4

	r4\mf a-> bf-> a8(-> g)-.
	R1
	\break
	f2-> f8( ef bf c)
	r4\f ef'16 ef ef ef ef8(-> d)-. c4-^	

	r4\mf a-> bf-> a8(-> g)-.
	R1
	f2.-> r4
	f4.-> ef8~-> ef4 r

	\break
	r4 r8\f ef'8~-> ef4 r
	r4 r8 ef8~-> ef4 r
	r4 r8 f8~-> f4 r
	r4 r8 ef8~-> ef4 r

	g,1\mf
	f2.\< r4\!
	\break
	a1->\f
	g2.\> r4\!

	f4.-> ef8~-> ef4 d4-^
  \bar "||"
	R1
	c'2\< \glissando f->\ff

	\pageBreak
	R1*3
	r4\f ef16 ef ef ef ef8(-> d)-. c4-^

	r8 c-^ r bf-^ r g-^ r fs-^
	r8 f-^ r ef-^ r c( bf)-. c-.
	\break
	a'2.-> r4
	f4.-> ef8~-> ef4 r

	r4 r8\f ef'8~-> ef4 r
	r4 r8 ef8~-> ef4 r
	r4 r8 f8~-> f4 r
	r4 r8 ef8~-> ef4 r

	\break
	g,1\mf
	f2.\< r4\!
	a1->\f
	g2.\> r4\!

  \bar "||"
	f4.-> ef8~-> ef4 d4-^
	R1
	c'2\< f->\ff

	\break
	\bar "[|:"
	\repeat volta 4 { 
		R1*8 
	}
	\bar ":|]"

	\break
	R1*4

	g,1\mf
	f2.\< r4\!
	a2.\mf\< r4\!
	c16\f c8-. c16~-> c16 bf c8~ c4 r

	\break
	\bar "[|:"
	\repeat volta 2 {
		r2 r8\f c( ef c)-.
		r2 r8\f c( ef f)-.
		r2 r8\f c( ef f)-.
		c2-> \glissando c'4 r4

		\break
		g,1\mf
		f2.\< r4\!
		a1\f
	}

	\alternative {
		{
			g2.\> r4\!
			\bar ":|]"
		}
		{
			c16\f c8-. c16~-> c16 bf c8~ c4 r
		}
	}

	\bar "[|:"
	\repeat volta 2{
		f,4.-> ef8~-> ef4 d4-^
		R1
		c'2\< f4->\ff r
	}
	\bar ":|]"

	f,4.-> ef8~-> ef4 d4-^
	bf'8\ff c-^ r4 r2 \fermata
	\bar "|."
}

tenorSax = \relative c'' {
	\global
	\key c \minor
	R1*3
	r4\f c'16 c c c c8(-> bf)-. g4-^

	R1*4
	r4\mf f-> g-> f8(-> ef)-.
	R1
	f2-> f8( ef bf c)
	r4\f c'16 c c c c8(-> bf)-. g4-^	

	r4\mf f-> g-> f8(-> ef)-.
	R1
	f2.-> r4
	f4.-> ef8~-> ef4 r

	r4 r8\f c'8~-> c4 r
	r4 r8 bf8~-> bf4 r
	r4 r8 c8~-> c4 r
	r4 r8 g8~-> g4 r

	bf,1\mf
	d2.\< r4\!
	c1->\f
	ef2.\> r4\!

	f4.-> ef8~-> ef4 d4-^
	r2 a'~->
	a2\< \glissando c->\ff

	R1*3
	r4\f ef16 ef ef ef ef8(-> d)-. c4-^

	r8 c-^ r bf-^ r g-^ r fs-^
	r8 f-^ r ef-^ r c( bf)-. c-.
	c2.-> r4
	f4.-> ef8~-> ef4 r

	r4 r8\f c'8~-> c4 r
	r4 r8 bf8~-> bf4 r
	r4 r8 c8~-> c4 r
	r4 r8 g8~-> g4 r

	bf,1\mf
	d2.\< r4\!
	c1->\f
	ef2.\> r4\!

	f4.-> ef8~-> ef4 d4-^
	r2 a'~->
	a2\< \glissando c->\ff

	\repeat volta 4 { 
    \impro 16
    \break
    \impro 16
	}

	R1*4

	bf,1\mf
	d2.\< r4\!
	c2.\mf\< r4\!
	c16\f c8-. c16~-> c16 bf c8~ c4 r

	\repeat volta 2 {
		r2 r8\f c( ef c)-.
		r2 r8\f c( ef f)-.
		r2 r8\f c( ef f)-.
		c2-> \glissando g'4 r4

		ef1\mf
		bf2.\< r4\!
		c1\f
	}

	\alternative {
		{
			ef2.\> r4\!
		}
		{
			c16\f c8-. c16~-> c16 bf c8~ c4 r
		}
	}

	\repeat volta 2{
		f4.-> ef8~-> ef4 d4-^
		r2 a'~->
		a2\< \glissando c4->\ff r
	}

	f,4.-> ef8~-> ef4 d4-^
	f8\ff g-^ r4 r2 \fermata
}

trombone = \relative c' {
	\global
	\clef bass

	\key c \minor
	R1*3
	r4\f g'16 g g g g8(-> f)-. ef4-^

	R1*4
	r4\mf bf-> ef-> d8(-> c)-.
	R1
	f,2-> f8( ef bf c)
	r4\f g''16 g g g g8(-> f)-. ef4-^	

	r4\mf bf-> ef-> d8(-> c)-.
	R1
	f,2.-> r4
	f4.-> ef8~-> ef4 r

	r4 r8\f g'8~-> g4 r
	r4 r8 g8~-> g4 r
	r4 r8 f8~-> f4 r
	r4 r8 c8~-> c4 r

	ef,1\mf
	bf2.\< r4\!
	f'1->\f
	c2.\> r4\!

	f4.-> ef8~-> ef4 d4-^
	f1~->
	f2\< \glissando f'->\ff

	R1*3
	r4\f g16 g g g g8(-> f)-. ef4-^

	r8 c-^ r bf-^ r g-^ r fs-^
	r8 f-^ r ef-^ r c( bf)-. c-.
	f2.-> r4
	f4.-> ef8~-> ef4 r

	r4 r8\f g'8~-> g4 r
	r4 r8 g8~-> g4 r
	r4 r8 f8~-> f4 r
	r4 r8 c8~-> c4 r

	ef,1\mf
	bf2.\< r4\!
	f'1->\f
	c2.\> r4\!

	f4.-> ef8~-> ef4 d4-^
	f1~->
	f2\< \glissando f'->\ff

	\repeat volta 4 { 
		R1*8
	}

	R1*4

	ef,1\mf
	bf2.\< r4\!
	f'2.\mf\< r4\!
	c'16\f c8-. c16~-> c16 bf c8~ c4 r

	\repeat volta 2 {
		r2 r8\f c( ef c)-.
		r2 r8\f c( ef f)-.
		r2 r8\f c( ef f)-.
		c2-> \glissando c,4 r4

		ef1\mf
		bf2.\< r4\!
		f'1\f
	}

	\alternative {
		{
			c2.\> r4\!
		}
		{
			c'16\f c8-. c16~-> c16 bf c8~ c4 r
		}
	}

	\repeat volta 2{
		f,4.-> ef8~-> ef4 d4-^
		f1~->
		f2\< \glissando f'4->\ff r
	}

	f,4.-> ef8~-> ef4 d4-^
	bf'8\ff c-^ r4 r2 \fermata
}

% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\book {
	\paper { 
		% Descomentar para score
		% #(set-paper-size "a4" 'landscape)
		% indent = 20
		% systems-per-page = 2
		top-markup-spacing.basic-distance = 5

		% Distancia entre el título y el primer sistema
		markup-system-spacing.basic-distance = 15

		% Margen inferior
		bottom-margin = 10
	}
	
	\score {
		% Descomentar para midi
		% \midi { \tempo 4 = 110 }
		
		% NO USAR layout-set-staff-size O SE ROMPE lilyjazz!!!
		\layout { }
		
		<<
			\scoreMarkup
			\transpose bf c' \scoreChords
			\new StaffGroup <<
				\new Staff \with 
				{ instrumentName = "Trpt." \RemoveEmptyStaves }
				{ \transpose bf c' \trumpet } 

				\new Staff \with 
				{ instrumentName = "T Sx." \RemoveEmptyStaves }
				{ \transpose bf c' \tenorSax } 

				\new Staff \with 
				{ instrumentName = "Trbn." \RemoveEmptyStaves } 
				{ \trombone }
			>>
		>>
	}
}
