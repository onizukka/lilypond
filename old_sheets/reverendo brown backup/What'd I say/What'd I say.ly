\version "2.18.2"
\language "english"

% Se debe incluir antes que lilyjazz. Si no, lilyjazz no funciona.
#(set-global-staff-size 19)

\include "main.ily"

\paper {
	indent = 10
}

\header {
	title = "What'd I say"
	instrument = "Trumpet, Tenor Sax & Trombone"
	composer = "Ray Charlse"
	arranger = "Transc: pacosaxo@gmail.com"
	poet = ""
	tagline = "Edition: gabriel@sauros.es"
}

% Función musical global
global = {
	% Para pintar todos los números de compás debajo del pentagrama
	% \override Score.BarNumber #'break-visibility = #end-of-line-invisible
	% \override Score.BarNumber #'direction = #DOWN
	% \override Score.BarNumber #'extra-offset = #'(1.5 . 0)
	\compressFullBarRests

	\set Timing.beamExceptions = #'()
	\set Timing.baseMoment = #(ly:make-moment 1/4)
	\set Timing.beatStructure = #'(1 1 1 1) 

  \key e \major
}

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
	% Oculta el primer pentagrama del markup
	\override Staff.VerticalAxisGroup #'remove-first = ##t

	% Indicación de tiempo
	% \once \override Score.RehearsalMark.self-alignment-X = #LEFT
	% \once \override Score.RehearsalMark.extra-offset = #'(0 . 3) 
	% \mark \jazzTempoMarkup "Easy Swing" c4 "112"

  \mkup "(Keys: 6 xs)"
  s1*12
  \mkup "(Vocals: 4 xs)"
  s1*12
  \mkup "(Keys: 2 xs)"
  s1*12
  \mkup "(Vocals: 2 xs)"
  s1*12

  s1*12
	\once \override Score.RehearsalMark.extra-offset = #'(1 . 0) 
  \eolMark \markup \musicglyph #"scripts.ufermata"
  \mkup "(Vocals: \"eeeeo\")"
}

% Cifrados
scoreChords = \chords {
	% c1:m f2:m g2:m7
}

% Música
trumpet = \relative c'' {
	\global

  \bar "[|:"
  R1*12
  \bar ":|][|:"
  R1*12
  \bar ":|][|:"
  R1*12
  \bar ":|][|:"
  R1*12

  \break
  \bar ":|][|:"
  \repeat volta 2 {
    r4 gs-^ gs8( gs-.) r4 | gs4.->( gs8-.) r2
    r4 gs-^ gs8( gs-.) r4 | gs4.->( gs8-.) r2
    r4 g-^ g8( g-.) r4 | g4.->( g8-.) r2
    r4 gs?-^ gs8( gs-.) r4 | gs4.->( gs8-.) r2
    r4 a-^ a8( a-.) r4 | g4.->( g8-.) r2
    r4 gs?-^ gs8( gs4.->) | gs4-^ r4 r2
  }

  \break
  \bar ":|][|:"
  \repeat volta 2 {
    R1*8
    r4 gs-^ gs8( gs-.) r4 | gs4.->( gs8-.) r2
    r4 gs-^ gs8( gs-.) r4 | gs4.->( gs8-.) r2
    r4 g-^ g8( g-.) r4 | g4.->( g8-.) r2
    r4 gs?-^ gs8( gs-.) r4 | gs4.->( gs8-.) r2
    r4 a-^ a8( a-.) r4 | g4.->( g8-.) r2
    r4 gs?-^ gs8( gs4.->) | gs4-^ r4 r2
  }

  \break
  \bar ":|][|:"
  \repeat volta 2 {
    r4 gs-^ gs8( gs-.) r4 | gs4.->( gs8-.) r2
    r4 gs-^ gs8( gs-.) r4 | gs4.->( gs8-.) r2
    r4 g-^ g8( g-.) r4 | g4.->( g8-.) r2
    r4 gs?-^ gs8( gs-.) r4 | gs4.->( gs8-.) r2
    r4 a-^ a8( a-.) r4 | g4.->( g8-.) r2
    r4 gs?-^ gs8( gs4.->) | gs4-^ r4 r2
  }
  \bar ":|]"
}

tenorSax = \relative c'' {
	\global

  \bar "[|:"
  R1*12
  \bar ":|][|:"
  R1*12
  \bar ":|][|:"
  R1*12
  \bar ":|][|:"
  R1*12
  \bar ":|][|:"
  \repeat volta 2 {
    r4 b-^ b8( b-.) r4 | b4.->( b8-.) r2
    r4 b-^ b8( b-.) r4 | b4.->( b8-.) r2
    r4 a-^ a8( a-.) r4 | a4.->( a8-.) r2
    r4 b-^ b8( b-.) r4 | b4.->( b8-.) r2
    r4 b-^ b8( b-.) r4 | a4.->( a8-.) r2
    r4 b-^ b8( b4.->) | b4-^ r4 r2
  }
  \bar ":|][|:"
  \repeat volta 2 {
    R1*8
    r4 b-^ b8( b-.) r4 | b4.->( b8-.) r2
    r4 b-^ b8( b-.) r4 | b4.->( b8-.) r2
    r4 a-^ a8( a-.) r4 | a4.->( a8-.) r2
    r4 b-^ b8( b-.) r4 | b4.->( b8-.) r2
    r4 b-^ b8( b-.) r4 | a4.->( a8-.) r2
    r4 b-^ b8( b4.->) | b4-^ r4 r2
  }
  \bar ":|][|:"
  \repeat volta 2 {
    r4 b-^ b8( b-.) r4 | b4.->( b8-.) r2
    r4 b-^ b8( b-.) r4 | b4.->( b8-.) r2
    r4 a-^ a8( a-.) r4 | a4.->( a8-.) r2
    r4 b-^ b8( b-.) r4 | b4.->( b8-.) r2
    r4 b-^ b8( b-.) r4 | a4.->( a8-.) r2
    r4 b-^ b8( b4.->) | b4-^ r4 r2
  }
  \bar ":|]"
}

trombone = \relative c {
	\global
  \clef bass

  \bar "[|:"
  R1*12
  \bar ":|][|:"
  R1*12
  \bar ":|][|:"
  R1*12
  \bar ":|][|:"
  R1*12
  \bar ":|][|:"
  \repeat volta 2 {
    r4 e-^ e8( e-.) r4 | e4.->( e8-.) r2
    r4 e-^ e8( e-.) r4 | e4.->( e8-.) r2
    r4 e-^ e8( e-.) r4 | e4.->( e8-.) r2
    r4 e-^ e8( e-.) r4 | e4.->( e8-.) r2
    r4 fs-^ fs8( fs-.) r4 | e4.->( e8-.) r2
    r4 e-^ e8( e4.->) | e4-^ r4 r2
  }
  \bar ":|][|:"
  \repeat volta 2 {
    R1*8
    r4 e-^ e8( e-.) r4 | e4.->( e8-.) r2
    r4 e-^ e8( e-.) r4 | e4.->( e8-.) r2
    r4 e-^ e8( e-.) r4 | e4.->( e8-.) r2
    r4 e-^ e8( e-.) r4 | e4.->( e8-.) r2
    r4 fs-^ fs8( fs-.) r4 | e4.->( e8-.) r2
    r4 e-^ e8( e4.->) | e4-^ r4 r2
  }
  \bar ":|][|:"
  \repeat volta 2 {
    r4 e-^ e8( e-.) r4 | e4.->( e8-.) r2
    r4 e-^ e8( e-.) r4 | e4.->( e8-.) r2
    r4 e-^ e8( e-.) r4 | e4.->( e8-.) r2
    r4 e-^ e8( e-.) r4 | e4.->( e8-.) r2
    r4 fs-^ fs8( fs-.) r4 | e4.->( e8-.) r2
    r4 e-^ e8( e4.->) | e4-^ r4 r2
  }
  \bar ":|]"
}

% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\book {
	\paper { 
		% Descomentar para score
		% #(set-paper-size "a4" 'landscape)
		% indent = 20
		% systems-per-page = 2
	}
	
	\score {
		% Descomentar para midi
		% \midi { \tempo 4 = 110 }
		
		% NO USAR layout-set-staff-size O SE ROMPE lilyjazz!!!
		\layout { }
		
		<<
			\scoreMarkup
			\transpose bf c' \scoreChords
			\new StaffGroup <<
				\new Staff \with 
				{ instrumentName = "Trpt."}
				{ \transpose bf c' \trumpet } 

				\new Staff \with 
				{ instrumentName = "T Sx." \RemoveEmptyStaves }
				{ \transpose bf c' \tenorSax } 

				\new Staff \with 
				{ instrumentName = "Trbn." \RemoveEmptyStaves } 
				{ \trombone }
			>>
		>>
	}
}
