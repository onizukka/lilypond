\version "2.18.2"
\language "english"

% Se debe incluir antes que lilyjazz. Si no, lilyjazz no funciona.
#(set-global-staff-size 19)

\include "main.ily"

\paper {
	indent = 10
}

\header {
	title = "I can' turn you loose"
	instrument = "Trumpet, Tenor Sax & Trombone"
	composer = "Otis Reeding"
	arranger = "Transc: gabriel@sauros.es"
	poet = ""
	tagline = "Edition: gabriel@sauros.es"
}

% Función musical global
global = {
	% Para pintar todos los números de compás debajo del pentagrama
	% \override Score.BarNumber #'break-visibility = #end-of-line-invisible
	% \override Score.BarNumber #'direction = #DOWN
	% \override Score.BarNumber #'extra-offset = #'(1.5 . 0)
	\compressFullBarRests

	\set Timing.beamExceptions = #'()
	\set Timing.baseMoment = #(ly:make-moment 1/4)
	\set Timing.beatStructure = #'(1 1 1 1) 
}

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
	% Oculta el primer pentagrama del markup
	\override Staff.VerticalAxisGroup #'remove-first = ##t

	% Indicación de tiempo
	% \once \override Score.RehearsalMark.self-alignment-X = #LEFT
	% \once \override Score.RehearsalMark.extra-offset = #'(0 . 3) 
	% \mark \jazzTempoMarkup "Easy Swing" c4 "112"
	\mkup "(Drums)"
	s1*4
	\mkup "(Bass)"
  s1*4

  \break
  \mkup "(Guitar)"
	s1*4
  \mkup "(Keyboard)"
  s1*8

	\break
	s1*4

	\break
	s1*4

	\break
	s1*20

	\pageBreak
	\mark \segno
	s1*4

	\break
	s1*4
	\eolMark \toCoda

	\break
	s1*2
	\mkup "(Repeat 8xs)"
	s1*2
	\eolMark \dalSegnoAlCoda

	\break
	\mark \coda
	s1*3
}

% Cifrados
scoreChords = \chords {
	% c1:m f2:m g2:m7
}

% Música
trumpet = \relative c'' {
	\global
	\key c \major

	R1*4
	R1*4
	R1*4
	R1*8

	\bar "[|:"	
    e2->\f d8-. c4-> e8->
    r2 g,8( a) c-. d-.
    e2-> d8-. c4-> c8->
    r2 g8( a) c-. d-.

    e2-> d8-. c4-> e8->
    r2 g,8( a) c-. d-.
    e2-> d8-. c4-> d8~->
    d8 e4-> f8~-> f fs4-> g8(

		c,4)-. r4 r2
		R1*3
		R1*4
		R1*4
    R1*4
    R1*4
	\bar ":|]"

	e2->\f d8-. c4-> e8->
	r2 g,8( a) c-. d-.
	e2-> d8-. c4-> c8->
	r2 g8( a) c-. d-.

	e2-> d8-. c4-> e8->
	r2 g,8( a) c-. d-.
	e2-> d8-. c4-> d8~->
	d8 e4-> f8~-> f fs4-> g8->
	\bar "||"

	r8 e4-> f8~-> f fs4-> g8->
	r8 e4-> f8~-> f fs4-> g8(

	\bar "[|:"
	\repeat volta 8 {
		c,4)-> r4 r2
		R1
	}
	\bar ":|]"

	r8 e4-> f8~-> f fs4-> g8->
	r8 e4-> f8~-> f fs4-> g8(
	c,4)-> r4 r2

	\bar "|."
}

tenorSax = \relative c'' {
	\global
	\key c \major

	R1*4
	R1*4
  R1*4
  R1*8

	\bar "[|:"	
    e2->\f d8-. c4-> e8->
    r2 g,8( a) c-. d-.
    e2-> d8-. c4-> c8->
    r2 g8( a) c4-.

    c'2-> a8-. g4-> c8->
    r2 g,8( a) c4-.
    c'2-> a8-. g4-> d8~->
    d8 e4-> f8~-> f fs4-> g8(

    \repeat percent 4 {
      <\parenthesize c, e>4)-.\p r8 e-> r2
      r8 e-. e( c) e-. e-. e( c)
    }

    \repeat percent 2 {
      ef4-. r8 ef-> r2
      r8 ef-. ef( c) ef-. ef-. ef( c)
    }
    \repeat percent 2 {
      e?4-. r8 e-> r2
      r8 e-. e( c) e-. e-. e( c)
    }

    f4-. r8 f-> r2
    r8 f-. f( d) f-. f-. f( d)
    ef4-. r8 ef-> r2
    r8 ef-. ef( c) ef-. ef-. ef4-.
  \bar ":|]"

	e?2->\f d8-. c4-> e8->
	r2 g,8( a) c-. d-.
	e2-> d8-. c4-> c8->
	r2 g8( a) c4-.

	c'2-> a8-. g4-> c8->
	r2 g,8( a) c4-.
	c'2-> a8-. g4-> d8~->
	d8 e4-> f8~-> f fs4-> g8->

	r8 e4-> f8~-> f fs4-> g8->
	r8 g4-> a8~-> a as4-> b8(

	\repeat volta 8 {
		<e, \parenthesize c'>4)-> e->\mf e-> e->
		e8( c) e-. f8~-> f e-. r4
	}

	r8 e4-> f8~-> f fs4-> g8->
	r8 g4-> a8~-> a as4-> b8(
	c4)-> r4 r2
}

trombone = \relative c' {
	\global
	\clef bass
	\key c \major

	R1*4
	R1*4
	R1*4
	R1*8

  \bar "[|:"
    e2->\f d8-. c4-> e8->
    r2 g,8( a) c-. d-.
    e2-> d8-. c4-> c8->
    r2 g8( a) c-. d-.

    g2-> f8-. e4-> g8->
    r2 g,8( a) c-. d-.
    g2-> f8-. e4-> d8~->
    d8 e4-> f8~-> f fs4-> g8(

    \repeat percent 4 {
      c,4)-.\p r8 c-> r2
      r8 c-. c( a) c-. c-. c( a)
    }

    \repeat percent 2 {
      c4-. r8 c-> r2
      r8 c-. c( a) c-. c-. c( a)
    }
    \repeat percent 2 {
      c4-. r8 c-> r2
      r8 c-. c( g) c-. c-. c( g)
    }

    d'4-. r8 d-> r2
    r8 d-. d( b) d-. d-. d( b)
    c4-. r8 c-> r2
    r8 c-. c( a) c-. c-. c4-.
  \bar ":|]"

	e2->\f d8-. c4-> e8->
	r2 g,8( a) c-. d-.
	e2-> d8-. c4-> c8->
	r2 g8( a) c-. d-.

	g2-> f8-. e4-> g8->
	r2 g,8( a) c-. d-.
	g2-> f8-. e4-> d8~->
	d8 e4-> f8~-> f fs4-> g8->

	r8 e4-> f8~-> f fs4-> g8->
	r8 e4-> f8~-> f fs4-> g8(

	\repeat volta 8 {
		<g, \parenthesize c>4)-> g->\mf g-> g->
		g8( e) g-. a8~-> a g-. r4
	}

	r8 e'4-> f8~-> f fs4-> g8->
	r8 e4-> f8~-> f fs4-> g8(
	c4)-> r4 r2
}

% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\book {
	\paper { 
		% Descomentar para score
		% #(set-paper-size "a4" 'landscape)
		% indent = 20
		% systems-per-page = 2
	}
	
	\score {
		% Descomentar para midi
		% \midi { \tempo 4 = 110 }
		
		% NO USAR layout-set-staff-size O SE ROMPE lilyjazz!!!
		\layout { }
		
		<<
			\scoreMarkup
			\transpose bf c' \scoreChords
			\new StaffGroup <<
				\new Staff \with 
				{ instrumentName = "Trpt." }
				{ \transpose bf c' \trumpet } 

				\new Staff \with 
				{ instrumentName = "T Sx." \RemoveEmptyStaves }
				{ \transpose bf c' \tenorSax } 

				\new Staff \with 
				{ instrumentName = "Trbn." \RemoveEmptyStaves } 
				{ \trombone }
			>>
		>>
	}
}
