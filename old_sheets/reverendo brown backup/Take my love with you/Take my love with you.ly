\version "2.18.2"
\language "english"

% Se debe incluir antes que lilyjazz. Si no, lilyjazz no funciona.
#(set-global-staff-size 19)

\include "main.ily"

\paper {
	indent = 10
	markup-system-spacing.basic-distance = 15
}

\header {
	title = "Take my love with you"
	instrument = "Trumpet, Tenor Sax & Trombone"
	composer = ""
	arranger = "Transc: "
	poet = ""
	tagline = "Edition: music@gpalaciosm.com"
}

% Función musical global
global = {
	% Para pintar todos los números de compás debajo del pentagrama
	% \override Score.BarNumber #'break-visibility = #end-of-line-invisible
	% \override Score.BarNumber #'direction = #DOWN
	% \override Score.BarNumber #'extra-offset = #'(1.5 . 0)
	\compressFullBarRests

	\set Timing.beamExceptions = #'()
	\set Timing.baseMoment = #(ly:make-moment 1/4)
	\set Timing.beatStructure = #'(1 1 1 1) 
}

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
	% Oculta el primer pentagrama del markup
	\override Staff.VerticalAxisGroup #'remove-first = ##t

	% Indicación de tiempo
	% \once \override Score.RehearsalMark.self-alignment-X = #LEFT
	% \once \override Score.RehearsalMark.extra-offset = #'(0 . 3) 
	% \mark \jazzTempoMarkup "Easy Swing" c4 "112"

	s1*6

	\break
	s1*12
	\mkup "(Repeat 3xs)"
	s1*4

	\break
	s1*8
	s1*8

	\pageBreak
	s1*12
	s1*2

	\break
	\mkup "(Tenor solo)"
	s1*4

	\break
	s1*4

	\break
	s1*16

	\pageBreak
	s1*12
	\mkup "(Repeat 8xs)"
	s1*3

	\break
	% \mark \markup {
	% 	\column { \fontsize #2 \musicglyph #"scripts.segno" }
	% 	\column {
  %     \line { " " }
	% 		\line { \underline "(Repeat       xs)" }
	% 	}
	% }
  \mkup "(Repeat       xs)"
  \mark \segno
	s1*3
	\set Score.repeatCommands = #'((volta "1, ..."))
	s1
	\set Score.repeatCommands = #'((volta #f) (volta "last") end-repeat)
	s1
	\set Score.repeatCommands = #'((volta #f))


	\break
	\mkup "(Repeat       xs)"
	s1*4
	\eolMark \toCoda

	\break
	\mkup "(Repeat       xs)"
	s1*3
	\set Score.repeatCommands = #'((volta "1, ..."))
	s1
	\set Score.repeatCommands = #'((volta #f) (volta "last") end-repeat)
	s1
	\set Score.repeatCommands = #'((volta #f))
	\eolMark \dalSegnoAlCoda

	\pageBreak
	\mkup "(Repeat       xs)"
	\mark \coda
	s1*3
	\set Score.repeatCommands = #'((volta "1, ..."))
	s1
	\set Score.repeatCommands = #'((volta #f) (volta "last") end-repeat)

	s1
	\set Score.repeatCommands = #'((volta #f))
	s1
	\once \override Score.RehearsalMark.self-alignment-X = #RIGHT
	\mkup "(Go to Shout?)"
	s1
	\eolMark \markup \underline "(Otherwhise END)"

	\break
	\mkup "(SHOUT!: The Isley Brothers)"
	s1*6

	\pageBreak
	\mkup "(Repeat 4xs)"
	s1*3

  \break
	\mkup "(Repeat 4xs)"
	s1*2
	\mkup "(Repeat 4xs)"
	s1*2

	\break
	s1*3
	\mkup "(Half-time feel)"
	s1*16

	\pageBreak
	\mkup "(Repeat ad.lib.)"
  \mark \segno
	s1*3
  \break
  s1*3

  \break
	\mkup "(Collective improvisation)"
	\mkup "(Repeat ad.lib.)"
  s1*2
  s1
  \eolMark \toSegno
}

% Cifrados
scoreChords = \chords {
	% c1:m f2:m g2:m7

	s1*52

	c1:m
	s1
	af1
	s1

	c1:m
	s1
	fs:7
	b1
}

% Música
trumpet = \relative c'' {
	\global
	\key ef \major

	R1\fermataMarkup
	c1\>\fermata

	R1\!\fermataMarkup
	ef,1\>\fermata

	R1\!\fermataMarkup
	g'1\fermata
	\bar "||"

	R1*12
	\bar "[|:"
	\repeat volta 3 {
		r4 ef8( f ef-.) r r4
		r4 c8( bf c8-.) r r4
	}
	\bar ":|]"
	ef1\<
	ef4-.\! r4 r2

  ef2.~-> ef8 c
  ef4-. r4 r2
  ef2.~-> ef8 c
  bf4-. r4 r2	

  ef2.~-> ef8 c
  ef4-. r4 r2
  f2.~-> f8 cs
  d4-. r4 r2

  ef2.~-> ef8 c
  ef4-. r4 r2
  ef2.~-> ef8 c
  bf4-. r4 r2	

  ef2~-> ef8 c ef c
  ef4-. r4 r2
	ef2.~-> ef8 c
	ef4-. r4 r2

	R1*12

	\bar "[|:"
	\repeat volta 4 {
		r4 ef8( f ef-.) r r4
		r4 c8( bf c8-.) r r4
	}
	\bar ":|]"

	R1*8

	\key e \major

  e2.~-> e8 cs
  e4-. r4 r2
  e2.~-> e8 cs
  b4-. r4 r2 	

  e2.~-> e8 cs
  e4-. r4 r2
  fs2.~-> fs8 es
  fs4-. r4 r2

  e2.~-> e8 cs
  e4-. r4 r2
  e2.~-> e8 cs
  b4-. r4 r2 	

  e2~-> e8 cs e cs
  e4-. r4 r2
	e2.~-> e8 cs
	e4-. r4 r2

	R1*12
	\bar "[|:"
	\repeat volta 8 {
		r4 e8( fs e-.) r r4
	}
	\alternative {{
		r4 cs8( b cs-.) r r4 
		\bar ":|]" }{
		r4 cs8( b cs-.) r r4
	}}

	\bar "[|:"
	\repeat volta 4 {
		R1*3
	}
	\alternative {{
		R1
		\bar ":|]" }{
		R1
	}}

	\bar "[|:"
	\repeat volta 2 {
		R1*4
	}

	\bar ":|][|:"
	\repeat volta 2 {
		R1*3
	}
	\alternative {{
		R1
		\bar ":|]"
		} {
		R1
	}}

	\bar "[|:"
	\repeat volta 4 {
		r4 e8( fs e-.) r r4
		r4 cs8( b cs-.) r r4 
		r4 e8( fs e-.) r r4
	}
	\alternative {{
		r4 cs8( b cs-.) r r4 
		\bar ":|]" }{
		r4 cs8( b cs-.) r r4 
	}}
	e1\fermata
	\bar "||"
	e1\fermata

	\bar "[|:"
	\repeat volta 2 {
		e4-> r4 r2
		e4-> r4 r2
	}
	\bar ":|]"
	e4-> r4 r2
	R1*3

	\bar "[|:"
	\repeat volta 4 {
		r4 e,2~-> e8 cs
	}
	\alternative {{
		fs4-> r8 gs r gs r gs
		\bar ":|]" }{
		fs4-> r8 gs r gs r4
	}}

	\bar "[|:"
	\repeat volta 4 {
		e'4-> r4 r2
		e4-> r4 r2
	}

	\bar ":|][|:"
	\repeat volta 4 {
		r4 e,8 ds e e-. r ds(
		e4)-. r8 e r e r e
		r4 e8 ds e e-. r ds(
	}
	\alternative {{
		e4)-. r8 e r e r e 
		\bar ":|]" }{
		e4-. r4 r2
	}}

	R1*16

	\bar "[|:"
	\repeat volta 2 {
		e'4-> r4 r2
		e4-> r4 r2
	}	
	\bar ":|][|:"
	\repeat volta 2 {
    e4-> r4 r2
    r4 e4-> b-> fs'->
  }
	\bar ":|][|:"
	\repeat volta 2 {
    e4-> r4 r2
    r4 g4-> fs-> e8 cs
  }
	\bar ":|][|:"
	\repeat volta 2 {
    e4-> r4 r2
    R1
  }

	e1\fermata
  \bar "||"
	\optOttava e'1 \fermata

	\bar "|."
}

tenorSax = \relative c'' {
	\global
	\key ef \major

	R1\fermataMarkup
	g'1\>\fermata

	R1\!\fermataMarkup
	c,1\>\fermata

	R1\!\fermataMarkup
	g'2\glissando bf2\fermata

	R1*12
	\repeat volta 3 {
		r4 g8( af g-.) r r4
		r4 af8( ef af8-.) r r4
	}
	af1\<
	g4-.\! r4 r2

  g2.~-> g8 fs
  g4-. r4 r2
  af2.~-> af8 fs
  g4-. r4 r2 

  g2.~-> g8 fs
  g4-. r4 r2
  a2.~-> a8 a
  bf4-. r4 r2

  g2.~-> g8 fs
  g4-. r4 r2
  af2.~-> af8 fs
  g4-. r4 r2 

  g2~-> g8 fs g fs
  g4-. r4 r2
	af2.~-> af8 fs
	g4-. r4 r2

	R1*12
	\repeat volta 4 {
		r4 g8( af g-.) r r4
		r4 af8( ef af8-.) r r4
	}

	\impro 32

  \key e \major
  gs2.~-> gs8 fss
  gs4-. r4 r2
  a2.~-> a8 fss
  gs4-. r4 r2

  gs2.~-> gs8 fss
  gs4-. r4 r2
  as2.~-> as8 as
  b4-. r4 r2

  gs2.~-> gs8 fss
  gs4-. r4 r2
  a2.~-> a8 fss
  gs4-. r4 r2

  gs2~-> gs8 fss gs fss
  gs4-. r4 r2
	a2.~-> a8 fss
	gs4-. r4 r2

	R1*12
	\repeat volta 8 {
		r4 gs8( a gs-.) r r4
	}
	\alternative {{
		r4 a8( e a8-.) r r4
		}{
		r4 a8( e a8-.) r r4
	}}

	\repeat volta 4 {
		b,,8-> r r4 r4 r8 bs(
		cs8)-> r r4 r2 
		b8->  r r4 r4 r8 bs(
	}
	\alternative {{
		cs8)-> r r4 r2 
		}{
		cs8-> r r4 r4 r8 as(
	}}

	\repeat volta 2 {
		b8)-> r r4 r4 r8 bs(
		cs8)-> r r4 r4 r8 as(
		b8)-> r r4 r4 r8 bs(
		cs8)-> r r4 r4 r8 as(
	}

	\repeat volta 2 {
		b8)-> r r4 r2
		r2 r4 r8 as(
		b8)-> r r4 r2
	}
	\alternative {{
		r2 r4 r8 as
		} {
		R1
	}}

	\repeat volta 2 {
		b8 r gs''8( a gs-.) r8 r4
		r4 a8( e a8-.) r8 r8 as,,
		b r gs''8( a gs-.) r8 r4
	}
	\alternative {{
		r4 a8( e a8-.) r8 r8 as,, \laissezVibrer
		} {
		r4 a''8( e a8-.) r8 r4
	}}

	g1\fermata
	b1\fermata

	\repeat volta 2 {
		gs4-> r4 r2
		gs4-> r4 r2
	}
	gs4-> r4 r2
	R1*3

	\repeat volta 4 {
		r4 b,2~-> b8 gs
	}
	\alternative {{
		cs4-> r8 e r e r e
		}{
		cs4-> r8 e r e r4
	}}

	\repeat volta 4 {
		gs4-> r4 r2
		gs4-> r4 r2
	}

	\repeat volta 4 {
		r4 b,8 as b b-. r as(
		b4)-. r8 b r b r cs
		r4 cs8 bs cs cs-. r bs(
	}
	\alternative {{
		cs4)-. r8 cs r cs r b }{
		cs4-. r4 r2
	}}

	R1*16

	\bar "[|:"
	\repeat volta 2 {
		gs'4-> r4 r2
		gs4-> r4 r2
	}	
	\bar ":|][|:"
	\repeat volta 2 {
    gs4-> r4 r2
    r4 e4-> b-> a'->
  }
	\bar ":|][|:"
	\repeat volta 2 {
    gs4-> r4 r2
    r4 g4-> fs-> e8 fs
  }
	\bar ":|][|:"
	\repeat volta 2 {
    gs4-> r4 r2
    R1
  }

	g1\fermata
	b1\fermata
}

trombone = \relative c' {
	\global
	\clef bass
	\key ef \major

	R1\fermataMarkup
	ef1\>\fermata

	R1\!\fermataMarkup
	g,1\>\fermata

	R1\!\fermataMarkup
	ef'1\fermata

	R1*12
	\repeat volta 3 {
		r4 bf8( c bf-.) r r4
		r4 ef8( c ef8-.) r r4
	}
	bf1\<
	ef4-.\! r4 r2

  ef2.~-> ef8 d
  ef4-. r4 r2
  c2.~-> c8 d
  ef4-. r4 r2
  
  ef2.~-> ef8 d
  ef4-. r4 r2
  c2.~-> c8 e
  f4-. r4 r2

  ef2.~-> ef8 d
  ef4-. r4 r2
  c2.~-> c8 d
  ef4-. r4 r2
  
  ef2~-> ef8 d ef d
  ef4-. r4 r2
	bf2.~-> bf8 d
	ef4-. r4 r2

	R1*12
	\repeat volta 4 {
		r4 bf8( c bf-.) r r4
		r4 ef8( c ef8-.) r r4
	}

	R1*8

	\key e \major
  e2.~-> e8 ds
  e4-. r4 r2
  cs2.~-> cs8 ds
  e4-. r4 r2

  e2.~-> e8 ds
  e4-. r4 r2
  cs2.~-> cs8 es
  fs4-. r4 r2

  e2.~-> e8 ds
  e4-. r4 r2
  cs2.~-> cs8 ds
  e4-. r4 r2

  e2~-> e8 ds e ds
  e4-. r4 r2
	b2.~-> b8 ds
	e4-. r4 r2

	R1*12
	\bar "[|:"
	\repeat volta 8 {
		r4 b8( cs b-.) r r4
	}
	\alternative {{
		r4 e8( cs e8-.) r r4
		\bar ":|]" }{
		r4 e8( cs e8-.) b( cs b
	}}

	\repeat volta 4 {
		e,1) ^\markup \underline "(Play only 1st x)" \bendAfter -3 
		R1*2
	}
	\alternative {{
		R1
		}{
		r2 r4 r8 ds(
	}}

	\repeat volta 2 {
		e8)-> r r4 r4 r8 ds(
		e8)-> r r4 r4 r8 ds(
		e8)-> r r4 r4 r8 ds(
		e8)-> r r4 r4 r8 ds(
	}

	\repeat volta 2 {
		e8)-> r r4 r2
		r2 r4 r8 ds(
		e8)-> r r4 r2
	}
	\alternative {{
		r2 r4 r8 ds
		} {
		r2 r8 b'( cs b)
	}}

	\repeat volta 3 {
		e,8-> r b'8( cs b-.) r r4
		r4 e8( cs e-.) r r ds,(
		e8)-> r b'8( cs b-.) r r4
	}
	\alternative {{
		r4 e8( cs e-.) r r ds, \laissezVibrer
		}{
		r4 e'8( cs e-.) r r4
	}}

	cs1\fermata
	e1\fermata

	\repeat volta 2 {
		b4-> r4 r2
		cs4-> r4 r2
	}
	b4-> r4 r2
	R1*3

	\repeat volta 4 {
		r4 gs2~-> gs8 e
	}
	\alternative {{
		gs4-> r8 cs r cs r b
		}{
		gs4-> r8 cs r cs r4
	}}

	\repeat volta 4 {
		b4-> r4 r2
		cs4-> r4 r2
	}

	\repeat volta 4 {
		r4 gs8 fss gs gs-. r fss(
		gs4)-. r8 gs r gs r gs
		r4 gs8 fss gs gs-. r fss(
	}
	\alternative {{
		gs4)-. r8 gs r gs r gs }{
		gs4-. r4 r2
	}}

	R1*16

	\bar "[|:"
	\repeat volta 2 {
		b4-> r4 r2
		cs4-> r4 r2
	}	
	\bar ":|][|:"
	\repeat volta 2 {
    b4-> r4 r2
    r4 e4-> b-> cs->
  }
	\bar ":|][|:"
	\repeat volta 2 {
    b4-> r4 r2
    r4 g'4-> fs-> cs8 a
  }
	\bar ":|][|:"
	\repeat volta 2 {
    b4-> r4 r2
    R1
  }

	cs1\fermata
	e1\fermata
}

% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\book {
	\paper { 
		% Descomentar para score
		% #(set-paper-size "a4" 'landscape)
		% indent = 20
		% systems-per-page = 2
	}
	
	\score {
		% Descomentar para midi
		% \midi { \tempo 4 = 110 }
		
		% NO USAR layout-set-staff-size O SE ROMPE lilyjazz!!!
		\layout { }
		
		<<
			\scoreMarkup
			\new StaffGroup <<
				\new Staff \with 
				{ instrumentName = "Trpt." \RemoveEmptyStaves }
				{ \transpose bf c' \trumpet } 

				\transpose bf c' \scoreChords
				\new Staff \with 
				{ instrumentName = "T Sx." \RemoveEmptyStaves }
				{ \transpose bf c' \tenorSax } 

				\new Staff \with 
				{ instrumentName = "Trbn." \RemoveEmptyStaves } 
				{ \trombone }
			>>
		>>
	}
}
