\version "2.18.2"
\language "english"

% Se debe incluir antes que lilyjazz. Si no, lilyjazz no funciona.
#(set-global-staff-size 19)

\include "main.ily"

\paper {
	indent = 10
}

\header {
	title = "Sex machine"
	instrument = "Trumpet, Tenor Sax & Trombone"
	composer = "James Brown"
	arranger = "Transc: gabriel@sauros.es"
	poet = ""
	tagline = "Edition: gabriel@sauros.es"
}

% Función musical global
global = {
	% Para pintar todos los números de compás debajo del pentagrama
	% \override Score.BarNumber #'break-visibility = #end-of-line-invisible
	% \override Score.BarNumber #'direction = #DOWN
	% \override Score.BarNumber #'extra-offset = #'(1.5 . 0)
	\compressFullBarRests

	\set Timing.beamExceptions = #'()
	\set Timing.baseMoment = #(ly:make-moment 1/4)
	\set Timing.beatStructure = #'(1 1 1 1) 
}

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
	% Oculta el primer pentagrama del markup
	\override Staff.VerticalAxisGroup #'remove-first = ##t

	% Indicación de tiempo
	% \once \override Score.RehearsalMark.self-alignment-X = #LEFT
	% \once \override Score.RehearsalMark.extra-offset = #'(0 . 3) 
	% \mark \jazzTempoMarkup "Easy Swing" c4 "112"
	s1*4
  \mkup "(Backgrounds to Keyboard solo)"
  s1*16

	\mark \segno
	s1*2

	\mkup "(Repeat 8xs)"
	s1*2

	\mkup "(Repeat 4xs)"
	s1*2

	\eolMark \toCoda
  s1*2
  \mkup "(Harp)"
  \mkup "(Solos, 16 measures each)"
  s1
  \mkup "(Guitar)"
  s1
  \mkup "(Tenor sax)"
  s1
  \mkup "(Trumpet)"
  s1
  \mkup "(ad.lib.)"
  s1*2

	\eolMark \dalSegnoAlCoda
	\mark \coda
}

% Cifrados
scoreChords = \chords {
  s1*28
  d1:m 
  ef1:m 
  d1:m 
  ef1:m 
}

% Música
trumpet = \relative c'' {
	\global
	\key d \minor
	b4->\mf b-> b-> b-> 
	b4->\< b-> b-> b->

	\bar "[|:"
	\repeat volta 2 {
		\once \override MultiMeasureRestNumber.transparent = ##t
		R1*2\!
		\break

		R1*4
		r2 a4-^\p a-^
		b4. \bendBefore a8-. r2
		r2 a4-^ a-^
		b4. \bendBefore a8-. r2

		\break
		R1*4
		r2 a4-^\p a-^
		b4. \bendBefore a8-. r2
		r2 a4-^ a-^
		b4. \bendBefore a8-. r2
	}
	\bar ":|]"

	\break
	\once \override MultiMeasureRestNumber.transparent = ##t
	R1*2\!

	\bar "[|:"
	\repeat volta 8 {
		e'8( e)-. r4 d8( d)-. r c-.
		r8 cs r d -> r2 
	}
	\bar ":|][|:"

	\repeat volta 4 {
		e8( e)-. r4 d8( d)-. r c-.
		r8 cs r d -> r2 
	}
	\bar ":|]"

	b4->\mf b-> b-> b-> 
	b4->\< b-> b-> b->

	\break
  \improOn
  r \! r r r
  \improOff
  \bar "||"
  \impro 4
  \break
  \bar "||"
  \impro 4
  \bar "||"
  \impro 4
	\bar "||"
	\once \override MultiMeasureRestNumber.transparent = ##t
  R1*2
	\bar "||"

	\break
	b4->\mf b-> b-> b-> 
	b4-> r4 r2

	\bar "|."
}

tenorSax = \relative c'' {
	\global
	\key d \minor
	e4->\mf e-> e-> e-> 
	e4->\< e-> e-> e->

	\repeat volta 2 {
		\once \override MultiMeasureRestNumber.transparent = ##t
		R1*2\!
		\break

		R1*4
		r2 f4-^\p f-^
		g4. \bendBefore f8-. r2
		r2 f4-^ f-^
		g4. \bendBefore f8-. r2

		\break
		R1*4
		r2 f4-^\p f-^
		g4. \bendBefore f8-. r2
		r2 f4-^ f-^
		g4. \bendBefore f8-. r2
	}

	\break
	\once \override MultiMeasureRestNumber.transparent = ##t
	R1*2\!

	\repeat volta 8 {
		a8( a)-. r4 g8( g)-. r f-.
		r8 fs r g -> r2 
	}

	\repeat volta 4 {
		a8( a)-. r4 g8( g)-. r f-.
		r8 fs r g -> r2 
	}

	\break
	e4->\mf e-> e-> e-> 
	e4->\< e-> e-> e->

	\break
  s1*4\!
	\once \override MultiMeasureRestNumber.transparent = ##t
  R1*2

	\break
	e4->\mf e-> e-> e-> 
	e4-> r4 r2
}

trombone = \relative c' {
	\global
	\clef bass
	\key d \minor
	cs4->\mf cs-> cs-> cs-> 
	cs4->\< cs-> cs-> cs->

	\repeat volta 2 {
		\once \override MultiMeasureRestNumber.transparent = ##t
		R1*2\!
		\break

		R1*4
		r2 c4-^\p c-^
		d4. \bendBefore c8-. r2
		r2 c4-^ c-^
		d4. \bendBefore c8-. r2

		\break
		R1*4
		r2 c4-^\p c-^
		d4. \bendBefore c8-. r2
		r2 c4-^ c-^
		d4. \bendBefore c8-. r2
	}

	\break
	\once \override MultiMeasureRestNumber.transparent = ##t
	R1*2\!

	\repeat volta 8 {
		a8( a)-. r4 g8( g)-. r f-.
		r8 fs r g -> r2 
	}

	\repeat volta 4 {
		c8( c)-. r4 b8( b)-. r a-.
		r8 as r b -> r2 
	}

	\break
	cs4->\mf cs-> cs-> cs-> 
	cs4->\< cs-> cs-> cs->

	\break
  s1*4\!
	\once \override MultiMeasureRestNumber.transparent = ##t
  R1*2

	\break
	cs4->\mf cs-> cs-> cs-> 
	cs4-> r4 r2
}

% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\book {
	\paper { 
		% Descomentar para score
		% #(set-paper-size "a4" 'landscape)
		% indent = 20
		% systems-per-page = 2
	}
	
	\score {
		% Descomentar para midi
		% \midi { \tempo 4 = 110 }
		
		% NO USAR layout-set-staff-size O SE ROMPE lilyjazz!!!
		\layout { }
		
		<<
			\scoreMarkup
			\transpose bf c' \scoreChords
			\new StaffGroup <<
				\new Staff \with 
				{ instrumentName = "Trpt."}
				{ \transpose bf c' \trumpet } 

				\new Staff \with 
				{ instrumentName = "T Sx." \RemoveEmptyStaves }
				{ \transpose bf c' \tenorSax } 

				\new Staff \with 
				{ instrumentName = "Trbn." \RemoveEmptyStaves } 
				{ \trombone }
			>>
		>>
	}
}
