\version "2.18.2"
\language "english"

% Se debe incluir antes que lilyjazz. Si no, lilyjazz no funciona.
#(set-global-staff-size 20)

\include "main.ily"

\header {
	title = "Hold on, I'm coming"
	instrument = "Trumpet, Tenor Sax & Trombone"
	composer = "Sam & Dave"
	arranger = "Transc: gabriel@sauros.es"
	poet = ""
	tagline = "Edition: gabriel@sauros.es"
}

\paper {
}



% Función musical global
global = {
	% Para pintar todos los números de compás debajo del pentagrama
	% \override Score.BarNumber #'break-visibility = #end-of-line-invisible
	% \override Score.BarNumber #'direction = #DOWN
	% \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

	\compressFullBarRests
}

% Elementos comunes de texto (marcas de ensayo, tempo, saltos de línea, etc)
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
	% Oculta el primer pentagrama del markup
	\override Staff.VerticalAxisGroup #'remove-first = ##t

	\set Timing.beamExceptions = #'()
	\set Timing.baseMoment = #(ly:make-moment 1/4)
	\set Timing.beatStructure = #'(1 1 1 1) 

	% Indicación de tiempo
	% \once \override Score.RehearsalMark.self-alignment-X = #LEFT
	% \once \override Score.RehearsalMark.extra-offset = #'(0 . 3) 
	% \mark \jazzTempoMarkup "Easy Swing" c4 "112"

	% s1*2
	% \once \override Score.RehearsalMark.X-offset = #5
	% \mark \markup \boxed "A"

	%{ 
	\mark \markup {
		\column { \vspace #-.4 \fontsize #1 \musicglyph #"scripts.coda"  }
	    \hspace #.5
	    \column { "text" }
	} 
	%}

	s1*4

	\break
	s1*8
	\mark \segno
	s1*4

	\break
	s1*2
	s1*2
	\eolMark \toCoda

	\break
	s1*12

	\break
	s1*6

	\break
	s1*2
	\mkup "(Keyboard solo)"
	s1*16
	\mkup "(Guitar solo)"
	s1*12
	\eolMark \dalSegnoAlCoda

	\break
	\mark \markup "3rd x: double feel"
	\mkup "(Repeat 6xs. Keep calm and chill)"
	\mark \coda
	s1*4

	\break
	s1*2

}

% Cifrado de acordes
scoreChords = \chords {
	% c1:m f2:m g2:m7
}

trumpet = \relative c' {
  	\global
  	\key af \major


  	\repeat percent 2 { af'16 gf8-. ef16~-. ef8 ef~-> ef16 ef gf8-. af4-. }
  	\repeat percent 2 { af'16 gf8-. ef16~-. ef8 ef~-> ef16 ef gf8-. af4-. }

  	R1*8
  	\bar "||"

  	\repeat percent 2 { af,16 gf8-. ef16~-. ef8 ef~-> ef16 ef gf8-. af4-. }
  	\repeat percent 2 { af'16 gf8-. ef16~-. ef8 ef~-> ef16 ef gf8-. af4-. }

  	\bar "[|:"
  	\repeat volta 2 { 
  		r4 c,-. c-. c-.
  		r4 c-. c-. c-.
  	}

  	\bar ":|][|:"
  	\repeat volta 2 { 
  		r4 cf-. cf-. cf-.
  		r4 cf-. cf-. cf-.
  	}

	\bar ":|][|:"
  	\repeat volta 2 {
  		\repeat percent 4 { af16 gf8-. ef16~-. ef8 ef~-> ef16 ef gf8-. af4-. }
  	}
  	\bar ":|]"

  	R1*8

  	af1( 
  	gf)\> \breathe
  	cf(\!
  	df2.)\< r4 \!

  	\repeat percent 2 { af16 gf8-. ef16~-. ef8 ef~-> ef16 ef gf8-. af4-. }
  	\repeat percent 2 { af'16 gf8-. ef16~-. ef8 ef~-> ef16 ef gf8-. af4-. }

  	R1*16
  	R1*12

	\bar ".|:-||"
  	\repeat volta 6 {
  		\repeat percent 2 { af,16 gf8-. ef16~-. ef8 ef~-> ef16 ef gf8-. af4-. }
  	}
  	\alternative {{
  		\repeat percent 2 { af16 gf8-. ef16~-. ef8 ef~-> ef16 ef gf8-. af4-. }
  		} {
  		af16 gf8-. ef16~-. ef8 ef~-> ef16 ef gf8-. af8.-. ef'16
  	}}
  	af16 gf8-. ef16~-. ef df ef8~-> ef16 ef gf8-. af4->
  	\bar "|."
}

% Música
tenorSax = \relative c'' {
  	\global
  	\key af \major

  	\repeat percent 2 { af16 gf8-. ef16~-. ef8 ef~-> ef16 ef gf8-. af4-. }
  	\repeat percent 2 { af'16 gf8-. ef16~-. ef8 ef~-> ef16 ef gf8-. af4-. }

  	R1*8

  	\repeat percent 2 { af,16 gf8-. ef16~-. ef8 ef~-> ef16 ef gf8-. af4-. }
  	\repeat percent 2 { af'16 gf8-. ef16~-. ef8 ef~-> ef16 ef gf8-. af4-. }
  	\repeat volta 2 { 
  		r4 ef-. ef-. ef-. 
  		r4 ef-. ef-. ef-. 
  	}
  	\repeat volta 2 { 
  		r4 f-. f-. f-.
  		r4 f-. f-. f-.
  	}
  	
  	\repeat volta 2 {
  		\repeat percent 4 { af,16 gf8-. ef16~-. ef8 ef~-> ef16 ef gf8-. af4-. }
  	}

  	R1*8

  	ef'1( 
  	df)\> \breathe
  	gf(\!
  	af2.)\< r4\!

  	\repeat percent 2 { af,16 gf8-. ef16~-. ef8 ef~-> ef16 ef gf8-. af4-. }
  	\repeat percent 2 { af'16 gf8-. ef16~-. ef8 ef~-> ef16 ef gf8-. af4-. }

  	R1*16
  	R1*12

  	\repeat volta 2 {
  		\repeat percent 2 { af,16 gf8-. ef16~-. ef8 ef~-> ef16 ef gf8-. af4-. }
  	}
  	\alternative {{
  		\repeat percent 2 { af16 gf8-. ef16~-. ef8 ef~-> ef16 ef gf8-. af4-. }
  		} {
  		af16 gf8-. ef16~-. ef8 ef~-> ef16 ef gf8-. af8.-. ef'16
  	}}
  	af16 gf8-. ef16~-. ef df ef8~-> ef16 ef gf8-. af4->
}

% Música
trombone = \relative c {
  	\global
  	\clef bass
  	\key af \major
  	\repeat percent 2 { af'16 gf8-. ef16~-. ef8 ef~-> ef16 ef gf8-. af4-. }
  	\repeat percent 2 { af'16 gf8-. ef16~-. ef8 ef~-> ef16 ef gf8-. af4-. }

  	R1*8

  	\repeat percent 2 { af,16 gf8-. ef16~-. ef8 ef~-> ef16 ef gf8-. af4-. }
  	\repeat percent 2 { af'16 gf8-. ef16~-. ef8 ef~-> ef16 ef gf8-. af4-. }
  	\repeat volta 2 { 
  		r4 af,-. af-. af-. 
  		r4 af-. af-. af-.
  	}
  	\repeat volta 2 { 
  		r4 df-. df-. df-. 
  		r4 df-. df-. df-. 
  	}
  	
  	\repeat volta 2 {
  		\repeat percent 4 { af16 gf8-. ef16~-. ef8 ef~-> ef16 ef gf8-. af4-. }
  	}

  	R1*8

  	c1( 
  	bf)\> \breathe
  	ef(\!
  	df2.)\< r4\!

  	\repeat percent 2 { af16 gf8-. ef16~-. ef8 ef~-> ef16 ef gf8-. af4-. }
  	\repeat percent 2 { af'16 gf8-. ef16~-. ef8 ef~-> ef16 ef gf8-. af4-. }

  	R1*16
  	R1*12

  	\repeat volta 2 {
  		\repeat percent 2 { af,16 gf8-. ef16~-. ef8 ef~-> ef16 ef gf8-. af4-. }
  	}
  	\alternative {{
  		\repeat percent 2 { af16 gf8-. ef16~-. ef8 ef~-> ef16 ef gf8-. af4-. }
  		} {
  		af16 gf8-. ef16~-. ef8 ef~-> ef16 ef gf8-. af8.-. ef'16
  	}}
  	af16 gf8-. ef16~-. ef df ef8~-> ef16 ef gf8-. af4->
}

% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
\book {
	\paper { 
		% Descomentar para score
		% #(set-paper-size "a4" 'landscape)
		% indent = 20
		% systems-per-page = 2
	}
	
	\score {
		% Descomentar para midi
		% \midi { \tempo 4 = 110 }
		
		% ATENCIÓN
		% NO USAR layout-set-staff-size O SE ROMPE lilyjazz!!!
		\layout { }
		
		<<
			\scoreMarkup
			\transpose bf c' \scoreChords
			\new StaffGroup <<
				\new Staff \with 
				{ instrumentName = "Trpt."}
				{ \transpose bf c' \trumpet } 

				\new Staff \with 
				{ instrumentName = "T Sx." }
				{ \transpose bf c' \tenorSax } 

				\new Staff \with 
				{ instrumentName = "Trbn." } 
				{ \trombone }
			>>
		>>
	}
}



%{
\book {
	\header { instrument = "" }
	\bookOutputName ""
	\score {
		<<
			\scoreMarkup
			\scoreChords
			\new Staff { \transpose bf c' \instrumentA }
		>>
	}
}
%}

