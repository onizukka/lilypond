\version "2.18.2"
\language "english"

% Se debe incluir antes que lilyjazz. Si no, lilyjazz no funciona.
#(set-global-staff-size 19)

\include "main.ily"

\paper {
	indent = 10
}

\header {
	title = "I feel good"
	instrument = "Trumpet, Tenor Sax & Trombone"
	composer = "James Brown"
	arranger = "Transc: gabriel@sauros.es"
	poet = ""
	tagline = "Edition: gabriel@sauros.es"
}

% Función musical global
global = {
	% Para pintar todos los números de compás debajo del pentagrama
	% \override Score.BarNumber #'break-visibility = #end-of-line-invisible
	% \override Score.BarNumber #'direction = #DOWN
	% \override Score.BarNumber #'extra-offset = #'(1.5 . 0)
	\compressFullBarRests

	\set Timing.beamExceptions = #'()
	\set Timing.baseMoment = #(ly:make-moment 1/4)
	\set Timing.beatStructure = #'(1 1 1 1) 
}

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
	% Oculta el primer pentagrama del markup
	\override Staff.VerticalAxisGroup #'remove-first = ##t

	% Indicación de tiempo
	% \once \override Score.RehearsalMark.self-alignment-X = #LEFT
	% \once \override Score.RehearsalMark.extra-offset = #'(0 . 3) 
	% \mark \jazzTempoMarkup "Easy Swing" c4 "112"

	s1
	s1
	\mark \markup {
		\column { \fontsize #2 \musicglyph #"scripts.segno" }
    \hspace #1
		\column { 
      \line { \underline \fontsize #2 "(On Segno tacet & repeat 4xs for solos)" }
      \line {
        \column {
          \line { \underline \fontsize #1 "(1st x: Harp)" }
          \line { \underline \fontsize #1 "(2nd x: Trumpet)" }
        }
        \hspace #1
        \column {
          \line { \underline \fontsize #1 "(3rd x: Tenor Sax)" }
          \line { \underline \fontsize #1 "(4th x: Trombone)" }
        }
      }
    }
	}
	s1*2

	\break
	s1*5

	\break
	s1*3
	\mkup "(On Trombone solo: play & continue)"
	s1*2

	\break
	s1*4

	\pageBreak
	s1*8

	\break
	s1*5

	\break
	s1*5


	\break
	s1
	\once \override Score.RehearsalMark.extra-offset = #'(9 . 0) 
	\mark \toSegno
	s1
	\once \override Score.RehearsalMark.extra-offset = #'(1 . 0) 
	s1*2

	\break
	s1*5
}

% Cifrados
scoreChords = \chords {
	% c1:m f2:m g2:m7
	s1*2
	d1:7
	s1
	s1
	s1

	g1:7
	s1
	d1:7
	s1

	a1:7
	g1:7
	d1:7
	s1
}

% Música
trumpet = \relative c'' {
	\global
	\key d \major
	r8 d,-. r fs8-. r8 a-. r c-.
	r8 e8-. r4 r2

	\bar "[|:"
	\repeat volta 2 {
		r4 d,8( cs d cs d cs 
		d4-.) r4 r2
		r4 d8( cs d cs d cs 
		d4-.) r4 r2

		r4 f8( e f e f e 
		f4-.) r4 r2
		r4 d8( cs d cs d cs 
		d4-.) r4 r2

		r4 e-. e-> r4
		r4 d-. d-> r4
		r8 d-. r fs8-. r8 a-. r c-.
		r8 e8-. r4 r2
	}
	\bar ":|]"

	d,4-> r8 d'-. r8 c-. r8 a
	c8 a r f~\bendBefore f2
	d4-> r8 d'-. r8 c-. r8 a
	c8 a r f~\bendBefore f4 r

	f2-> r
	f2-> r
	R1
	r4 g8( f g f d4-.)

	f2-> r
	f2-> r
	R1*2

	r4 d8( cs d cs d cs 
	d4-.) r4 r2
	r4 d8( cs d cs d cs 
	d4-.) r4 r2
	
	r4 f8( e f e f e 
	f4-.) r4 r2
	r4 d8( cs d cs d cs 
	d4-.) r4 r2	

	r4 e-. e-> r4
	r4 d-. d-> r4
	r8 d-. r fs8-. r8 a-. r c-.
	r8 e8-. r4 r2

	\bar "[|:"
	\repeat volta 2 {
		r4 e,-. e-> r4
		r4 d-. d-> r4
		r8 d-. r fs8-. r8 a-. r c-.
	}
	\alternative {{
		r8 e8-. r4 r2 
		\bar ":|]" }{
		r8\rit e8-. r d-. r c-. r a-.
	}}

	r8 g-. r f~-> f2\fermata\!
	e'1\fermata

	\bar "|."
}

tenorSax = \relative c'' {
	\global
	\key d \major
	r8 d,-. r fs8-. r8 a-. r c-.
	r8 e8-. r4 r2

	\repeat volta 2 {
		r4 a,8( gs a gs a gs 
		a4-.) r4 r2
		r4 a8( gs a gs a gs 
		a4-.) r4 r2

		r4 d8( cs d cs d cs 
		d4-.) r4 r2
		r4 a8( gs a gs a gs 
		a4-.) r4 r2

		r4 cs-. cs-> r4
		r4 b-. b-> r4
		r8 d,-. r fs8-. r8 a-. r c-.
		r8 e8-. r4 r2
	}

	d,4-> r8 d'-. r8 c-. r8 a
	c8 a r f~\bendBefore f2
	d4-> r8 d'-. r8 c-. r8 a
	c8 a r f~\bendBefore f4 r

	d'2-> r
	d2-> r
	R1
	r4 g8( f g f d4-.)

	d2-> r
	d2-> r
	R1*2

	r4 a8( gs a gs a gs 
	a4-.) r4 r2
	r4 a8( gs a gs a gs 
	a4-.) r4 r2

	r4 d8( cs d cs d cs 
	d4-.) r4 r2
	r4 a8( gs a gs a gs 
	a4-.) r4 r2

	r4 cs-. cs-> r4
	r4 b-. b-> r4
	r8 d,-. r fs8-. r8 a-. r c-.
	r8 e8-. r4 r2

	\repeat volta 2 {
		r4 cs-. cs-> r4
		r4 b-. b-> r4
		r8 d,-. r fs8-. r8 a-. r c-.
	}
	\alternative {{
		r8 e8-. r4 r2 }{
		r8\rit e8-. r d-. r c-. r a-.
	}}

	r8 g-. r f~-> f2\fermata\!
	c'1\fermata
}

trombone = \relative c' {
	\global
	\clef bass
	\key d \major

	r8 d,-. r fs8-. r8 a-. r c-.
	r8 e8-. r4 r2

	\repeat volta 2 {
		r4 fs,8( es fs es fs es
		fs4-.) r4 r2
		r4 fs8( es fs es fs es 
		fs4-.) r4 r2

		r4 b8( as b as b as 
		a?4-.) r r2 
		r4 fs8( es fs es fs es 
		fs4-.) r4 r2

		r4 g-. g-> r4
		r4 f-. f-> r4
		r8 d-. r fs8-. r8 a-. r c-.
		r8 e8-. r4 r2
	}

	d,4-> r8 d'-. r8 c-. r8 a
	c8 a r f~\bendBefore f2
	d4-> r8 d'-. r8 c-. r8 a
	c8 a r f~\bendBefore f4 r

	g2-> r
	g2-> r
	R1
	r4 g8( f g f d4-.)

	g2-> r
	g2-> r
	R1*2

	r4 fs8( es fs es fs es
	fs4-.) r4 r2
	r4 fs8( es fs es fs es 
	fs4-.) r4 r2

	r4 b8( as b as b as 
	a?4-.) r4 r2
	r4 fs8( es fs es fs es 
	fs4-.) r4 r2

	r4 g-. g-> r4
	r4 f-. f-> r4
	r8 d-. r fs8-. r8 a-. r c-.
	r8 e8-. r4 r2

	\repeat volta 2 {
		r4 g,-. g-> r4
		r4 f-. f-> r4
		r8 d-. r fs8-. r8 a-. r c-.
	}
	\alternative {{
		r8 e8-. r4 r2 }{
		r8\rit e8-. r d-. r c-. r a-.
	}}

	r8 g-. r f~-> f2\fermata\!
	fs'?1\fermata
}

% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\book {
	\paper { 
		% Descomentar para score
		% #(set-paper-size "a4" 'landscape)
		% indent = 20
		% systems-per-page = 2
		page-count = 2
		top-system-spacing.basic-distance = 10
		bottom-margin = 10
	}
	
	\score {
		% Descomentar para midi
		% \midi { \tempo 4 = 110 }
		
		% NO USAR layout-set-staff-size O SE ROMPE lilyjazz!!!
		\layout { }
		
		<<
			\scoreMarkup
			\transpose bf c' \scoreChords
			\new StaffGroup <<
				\new Staff \with 
				{ instrumentName = "Trpt." \RemoveEmptyStaves }
				{ \transpose bf c' \trumpet }
				
				\new Staff \with 
				{ instrumentName = "T Sx." }
				{ \transpose bf c' \tenorSax } 

 				\scoreChords
				\new Staff \with 
				{ instrumentName = "Trbn." \RemoveEmptyStaves } 
				{ \trombone }
			>>
		>>
	}
}
