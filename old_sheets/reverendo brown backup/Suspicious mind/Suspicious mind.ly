\version "2.18.2"
\language "english"

% Se debe incluir antes que lilyjazz. Si no, lilyjazz no funciona.
#(set-global-staff-size 19)

\include "main.ily"

\paper {
	indent = 10
}

\header {
	title = "Suspicious mind"
	instrument = "Trumpet, Tenor Sax & Trombone"
	composer = "Elvis Presley"
	arranger = "Transc: gabriel@sauros.es"
	poet = ""
	tagline = "Edition: gabriel@sauros.es"
}

% Función musical global
global = {
	% Para pintar todos los números de compás debajo del pentagrama
	% \override Score.BarNumber #'break-visibility = #end-of-line-invisible
	% \override Score.BarNumber #'direction = #DOWN
	% \override Score.BarNumber #'extra-offset = #'(1.5 . 0)
	\compressFullBarRests

	\set Timing.beamExceptions = #'()
	\set Timing.baseMoment = #(ly:make-moment 1/4)
	\set Timing.beatStructure = #'(1 1 1 1) 
}

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
	% Oculta el primer pentagrama del markup
	\override Staff.VerticalAxisGroup #'remove-first = ##t

	% Indicación de tiempo
	% \once \override Score.RehearsalMark.self-alignment-X = #LEFT
	% \once \override Score.RehearsalMark.extra-offset = #'(0 . 3) 
	% \mark \jazzTempoMarkup "Easy Swing" c4 "112"

	s1*34

	\break
	s1*8

	\break
	s1*8

	\break
	\mkup "(Slow feel)"
	\mark \tempoChange c4 c8
	s1*11*6/8
	\mkup "(A tempo)"
	\mark \tempoChange c8 c4
	s1
	s1*16

	\break
  \mark \markup \underline "(Tacet after decresc.)"
	\mkup "(Repeat ad.lib.)"
	s1*4

	\break
	s1*2
  \mark \markup \underline "(Stop playing when decresc.)"
  \mkup "(Reprise)"
  s1*2
  \break
	s1*5
}

% Cifrados
scoreChords = \chords {
	% c1:m f2:m g2:m7
}

% Música
trumpet = \relative c'' {
	\global
	\key g \major

	R1*2
	\bar "||"

	R1*24
	\bar "||"

	R1*6
	r4 c8\mf b e d~ d4
	r4 c8 b e d~ d4

	d,8-. d-. d4-. r2
	R1
	e8-. e-. e4-. r2
	R1

	fs1->
	g2.-> r4
	R1*2

	R1*1
	r2 r4 g16 a b c
	d1\mf \breathe
	e2\< fs

	g1\f
	fs2 r
	e1
	g2 fs
	\bar "||"

	\time 6/8
	R1*8*6/8

	R1*3*6/8
	\time 4/4
	d4->\f d-> d-> d->
	\bar "||"

	R1*8
	R1*8

	\bar "[|:"
	\repeat volta 2 {
		d,8-.\f d-. d4~-> d r4
		d8-. d-. d4~-> d r4
		e8-. e-. e4~-> e r4
		e8-. e-. e4~-> e r4
  }
  \alternative {
    {
      fs1->
      g2.-> r4
      r4 c8 b e d~ d4
      r4 c8 b e d~ d4
    }
    {
      fs,1-> \fermata \>
    }
  }

  R1 \fermata \!
  g1-> \fermata \>
  R1 \fermata \!
	g'1-> \fermata
	\bar "|."
}

tenorSax = \relative c'' {
	\global
	\key g \major

	R1*2

	R1*24

	R1*6
	r4 c8\mf b e d~ d4
	r4 c8 b e d~ d4

	b8-. b-. b4-. r2
	R1
	c8-. c-. c4-. r2
	R1

	d1->
	e2.-> r4
	R1*2

	R1*1
	r2 r4 b16 c d e
	fs1\mf \breathe
	g2\< a

	b1\f
	b2 r
	g1
	a1

	\time 6/8
	R1*8*6/8

	R1*3*6/8
	\time 4/4
	d,4->\f d-> d-> d->

	R1*8
	R1*8

	\repeat volta 2 {
		b8-.\f b-. b4~-> b r4
		b8-. b-. b4~-> b r4
		c8-. c-. c4~-> c r4
		c8-. c-. c4~-> c r4
  }
  \alternative {
    {
      d1->
      e2.-> r4
      r4 c8 b e d~ d4
      r4 c8 b e d~ d4
    }
    {
      d1-> \fermata \>
    }
  }

  R1 \fermata \!
  e1-> \fermata \>
  R1 \fermata \!
	b'1-> \fermata
}

trombone = \relative c' {
	\global
	\clef bass
	\key g \major

	R1*2

	R1*24

	R1*6
	r4 c8\mf b e d~ d4
	r4 c8 b e d~ d4

	g,8-. g-. g4-. r2
	R1
	g8-. g-. g4-. r2
	R1

	a->
	g2.-> r4
	R1*2

	R1*2
	b1\mf \breathe
	c2\< d

	e1\f
	b2 r
	c1
	d1

	\time 6/8
	R1*8*6/8

	R1*3*6/8
	\time 4/4
	d4-> d-> d-> d->

	R1*8
	R1*8

	\repeat volta 2 {
		g,8-.\mf g-. g4~-> g r4
		g8-. g-. g4~-> g r4
		g8-. g-. g4~-> g r4
		g8-. g-. g4~-> g r4
  }
  \alternative {
    {
      a1->
      g2.-> r4
      r4 c8 b e d~ d4
      r4 c8 b e d~ d4
    }
    {
      a1-> \fermata \>
    }
  }

  R1 \fermata \!
  g-> \fermata \>
  R1 \fermata \!
	g'1-> \fermata
}

% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\book {
	\paper { 
		% Descomentar para score
		% #(set-paper-size "a4" 'landscape)
		% indent = 20
		% systems-per-page = 2
	}
	
	\score {
		% Descomentar para midi
		% \midi { \tempo 4 = 110 }
		
		% NO USAR layout-set-staff-size O SE ROMPE lilyjazz!!!
		\layout { }
		
		<<
			\scoreMarkup
			\transpose bf c' \scoreChords
			\new StaffGroup <<
				\new Staff \with 
				{ instrumentName = "Trpt."}
				{ \transpose bf c' \trumpet } 

				\new Staff \with 
				{ instrumentName = "T Sx." \RemoveEmptyStaves }
				{ \transpose bf c' \tenorSax } 

				\new Staff \with 
				{ instrumentName = "Trbn." \RemoveEmptyStaves } 
				{ \trombone }
			>>
		>>
	}
}
