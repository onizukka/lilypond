\version "2.18.2"
\language "english"

% Se debe incluir antes que lilyjazz. Si no, lilyjazz no funciona.
#(set-global-staff-size 19)

\include "main.ily"

\paper {
	indent = 10
}

\header {
	title = "River deep, mountain high"
	instrument = "Trumpet, Tenor Sax & Trombone"
	composer = "Ike & Tina Turner"
	arranger = "Transc: pacosaxo@gmail.com"
	poet = ""
	tagline = "Edition: gabriel@sauros.es"
}

% Función musical global
global = {
	% Para pintar todos los números de compás debajo del pentagrama
	% \override Score.BarNumber #'break-visibility = #end-of-line-invisible
	% \override Score.BarNumber #'direction = #DOWN
	% \override Score.BarNumber #'extra-offset = #'(1.5 . 0)
	\compressFullBarRests

	\set Timing.beamExceptions = #'()
	\set Timing.baseMoment = #(ly:make-moment 1/4)
	\set Timing.beatStructure = #'(1 1 1 1) 

  \key f \major
}

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
	% Oculta el primer pentagrama del markup
	\override Staff.VerticalAxisGroup #'remove-first = ##t

	% Indicación de tiempo
	% \once \override Score.RehearsalMark.self-alignment-X = #LEFT
	% \once \override Score.RehearsalMark.extra-offset = #'(0 . 3) 
	% \mark \jazzTempoMarkup "Easy Swing" c4 "112"

  \partial 8
  s8 

  \time 3/4
  s1*3/4

  \time 4/4
  s1*34

  \time 2/4
  s2

  \eolMark \toSegno
  \mkup "(Repeat 4x)"
  s1*2
  \mkup "(Repeat 4x)"
  \mkup "(Vocals)"
}

% Cifrados
scoreChords = \chords {
	% c1:m f2:m g2:m7
}

% Música
trumpet = \relative c'' {
	\global

	\override Staff.VerticalAxisGroup #'remove-first = ##t
  \time 3/4
  \partial 8
  r8

  \mark \segno
  R1*3/4

  \time 4/4
  R1*4

  \break
  \bar "[|:"
  R1*4
  \break
  R1*4
  \bar ":|]"
  R1

  \break
  \bar "[|:"
  \repeat volta 3 {
    c1 ~ 
    c2 r4 r8 d-> ~
    d4( c-.) c->( bf-.)
  }
  \alternative {
    {
      bf4( a-.) a8( c-.) r c-> \laissezVibrer
    } {
      bf4( a-.) a8( af g) ef-> ~
    }
  }

  ef1 ~ 
  ef1
  r4 ef( d ef 
  f ef d ef)

  f1 ~
  f1
  r4 f( e f 
  g f e f)

  ef1 ~ 
  ef1
  r4 ef( d ef 
  f ef d ef)

  f1 ~ f ~ f\> ~ f 

  \time 2/4
  R1*2/4\!

  \pageBreak
  \time 4/4
  \bar "[|:"
  R1*2
  \bar ":|][|:"
  R1*2
  \bar ":|]"
  R1
  \bar "[|:"
  R1*2
  \bar ":|]"
  R1

  R1
  r2 r8 \f c'( ef e)
  f4-^ r8 f( ef e-.) r f-^
  R1


  f,4-> \mf f-> r2
  f4-^ a8( f-.) r f( g-.) gs-.
  a4-> a-> r2
  a4-^ c8( a-.) r a( bf-.) b-.

  c4-> c-> r2
  c4-^ f8( c-.) r f,( g-.) gs-.
  a4-> \< a-> r8 a( bf-.) b-.
  c4-> c-> r8 d( ef-.) e-.

  f8-. \f f-. f-. f-. f-. f-. f-. f-. 
  f8-. f-. f-. f-. f4-> e->

  ef1 -> \> ~
  ef
  r4 \mf ef,( d ef 
  f ef d ef)

  f1 ~
  f1
  r4 f( e f 
  g f e f)

  ef1 ~ 
  ef1
  r4 ef( d ef 
  f ef d ef)

  f1 ~ f ~ f\> ~ f 

  \time 2/4
  R1*2/4\!

  \time 3/4
  R1*3/4

  \time 4/4
  R1*3

  \bar "|."
}

tenorSax = \relative c'' {
	\global

  \time 3/4
  \partial 8
  r8

  f,2. \sfp \< ~

  \time 4/4
  f1 ~
  f1
  R1 \!
  r2 r4 r8 a(

  \bar "[|:"
  \repeat volta 2 {
    bf4-.) r8 a( bf4-.) r8 bf-.
    r8 bf8-. r a( bf4-.) r8 a(
    bf4-.) r8 a( bf4-.) r8 bf-.
    r8 bf8-. r a( bf4-.) r8 e,(

    f4-.) r8 e( f4-.) r8 f-.
    r8 f8-. r e( f4-.) r8 e(
    f4-.) r8 e( f4-.) r8 f-.
  }
  \alternative {
    {
      r8 f8-. r e( f4-.) r8 a \laissezVibrer
      \bar ":|]"
    } {
      r8 f8-. r e( f4-.) r4
    }
  }

  \break
  \bar "[|:"
  \repeat volta 3 {
    c'2 a8( c-.) r8 a ~
    a4 g-. r4 r8 d'-> ~
    d4( c-.) c->( bf-.)
  }
  \alternative {
    {
      bf4( a-.) a8( c-.) r c-> \laissezVibrer
    } {
      bf4( a-.) a8( af g) ef-> ~
    }
  }

  ef1 ~ 
  ef1
  r4 ef'( d ef 
  f ef d ef)

  f1 ~
  f1
  r4 f( e f 
  g f e f)

  ef1 ~ 
  ef1
  r4 ef( d ef 
  f ef d ef)

  f1 ~ f ~ f\> ~ f 

  \time 2/4
  R1*2/4\!

  \break
  \time 4/4
  \repeat volta 2 {
    f,4-^ f4-^ r2
    f8( f-.) r f-^ r c( ef e)
  }
  \repeat volta 4 {
    f4-^ f4-^ r2
  }
  \alternative {
    {
      f8( f-.) r f-^ r c( ef e)
    } {
      f8( f-.) r f-^ r f( af a)
    }
  }
  \repeat volta 2 {
    bf4-^ bf4-^ r2
  }
  \alternative {
    {
      bf8( bf-.) r bf-^ r f( af a)
    } {
      bf8( bf-.) r bf-^ r c,( ef e)
    }
  }

  f4-^ f4-^ r2
  f8( f-.) r f-^ r \f c'( ef e)
  f4-^ r8 f( ef e-.) r f-^
  R1

  \break
  a,4-> \mf a-> r2
  a4-^ c8( a-.) r a( bf-.) b-.
  c4-> c-> r2
  c4-^ f8( c-.) r c( d-.) ef-.

  f4-> f-> r2
  f4-^ a8( f-.) r a,( bf-.) b-.
  c4-> \< c-> r8 c( d-.) ef-.
  f4-> f-> r8 d( ef-.) e-.

  f8-. \f f-. f-. f-. f-. f-. f-. f-. 
  f8-. f-. f-. f-. f4-> e->

  ef1 -> \> ~
  ef
  r4 \mf ef( d ef 
  f ef d ef)

  f1 ~
  f1
  r4 f( e f 
  g f e f)

  ef1 ~ 
  ef1
  r4 ef( d ef 
  f ef d ef)

  f1 ~ f ~ f\> ~ f 

  \time 2/4
  R1*2/4\!

  f,2. \sfp \< ~

  \time 4/4
  f1 ~
  f1
  R1 \!

  \bar "|."
}

trombone = \relative c' {
	\global
  \clef bass

  \time 3/4
  \partial 8
  r8

  f,,2. \sfp \< ~

  \time 4/4
  f1 ~
  f1
  R1 \!
  r2 r4 r8 a(

  \bar "[|:"
  \repeat volta 2 {
    bf4-.) r8 a( bf4-.) r8 bf-.
    r8 bf8-. r a( bf4-.) r8 a(
    bf4-.) r8 a( bf4-.) r8 bf-.
    r8 bf8-. r a( bf4-.) r8 e,(

    f4-.) r8 e( f4-.) r8 f-.
    r8 f8-. r e( f4-.) r8 e(
    f4-.) r8 e( f4-.) r8 f-.
  }
  \alternative {
    {
      r8 f8-. r e( f4-.) r8 a \laissezVibrer
      \bar ":|]"
    } {
      r8 f8-. r e( f4-.) r4
    }
  }

  \break
  \bar "[|:"
  \repeat volta 3 {
    c''1 ~
    c2 r4 r8 d-> ~
    d4( c-.) c->( bf-.)
  }
  \alternative {
    {
      bf4( a-.) a8( c-.) r c-> \laissezVibrer
    } {
      bf4( a-.) a8( af g) ef-> ~
    }
  }

  ef1 ~ 
  ef1
  r4 ef( d ef 
  f ef d ef)

  f1 ~
  f1
  r4 f( e f 
  g f e f)

  ef1 ~ 
  ef1
  r4 ef( d ef 
  f ef d ef)

  f1 ~ f ~ f\> ~ f 

  \time 2/4
  R1*2/4\!

  \break
  \time 4/4
  \repeat volta 2 {
    f4-^ f4-^ r2
    f8( f-.) r f-^ r c( ef e)
  }
  \repeat volta 4 {
    f4-^ f4-^ r2
  }
  \alternative {
    {
      f8( f-.) r f-^ r c( ef e)
    } {
      f8( f-.) r f-^ r f( af a)
    }
  }
  \repeat volta 2 {
    bf4-^ bf4-^ r2
  }
  \alternative {
    {
      bf8( bf-.) r bf-^ r f( af a)
    } {
      bf8( bf-.) r bf-^ r c,( ef e)
    }
  }

  f4-^ f4-^ r2
  f8( f-.) r f-^ r \f c'( ef e)
  f4-^ r8 f( ef e-.) r f-^
  R1

  f,4-> \mf f-> r2
  f4-^ a8( f-.) r f( g-.) gs-.
  a4-> a-> r2
  a4-^ c8( a-.) r a( bf-.) b-.

  c4-> c-> r2
  c4-^ f8( c-.) r f,( g-.) gs-.
  a4-> \< a-> r8 a( bf-.) b-.
  c4-> c-> r8 d( ef-.) e-.

  f8-. \f f-. f-. f-. f-. f-. f-. f-. 
  f8-. f-. f-. f-. f4-> e->

  ef1 -> \> ~
  ef
  r4 \mf ef,( d ef 
  f ef d ef)

  f1 ~
  f1
  r4 f( e f 
  g f e f)

  ef1 ~ 
  ef1
  r4 ef( d ef 
  f ef d ef)

  f1 ~ f ~ f\> ~ f 

  \time 2/4
  R1*2/4\!

  f,2. \sfp \< ~

  \time 4/4
  f1 ~
  f1
  R1 \!

  \bar "|."
}

% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\book {
	\paper { 
		% Descomentar para score
		% #(set-paper-size "a4" 'landscape)
		% indent = 20
		% systems-per-page = 2
	}
	
	\score {
		% Descomentar para midi
		% \midi { \tempo 4 = 110 }
		
		% NO USAR layout-set-staff-size O SE ROMPE lilyjazz!!!
		\layout { }
		
		<<
			\scoreMarkup
			\transpose bf c' \scoreChords
			\new StaffGroup <<
				\new Staff \with 
				{ instrumentName = "Trpt." }
				{ \transpose bf c' \trumpet } 

				\new Staff \with 
				{ instrumentName = "T Sx." \RemoveEmptyStaves }
				{ \transpose bf c' \tenorSax } 

				\new Staff \with 
				{ instrumentName = "Trbn." \RemoveEmptyStaves } 
				{ \trombone }
			>>
		>>
	}
}
