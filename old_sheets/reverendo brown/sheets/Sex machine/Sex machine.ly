\version "2.18.2"

% Se debe incluir antes que lilyjazz. Si no, lilyjazz no funciona.
#(set-global-staff-size 20)

\include "main.ily"
\include "music-sheet-paper.ily"

\paper {
}

\header {
  title = "Sex machine"
  instrument = "Trumpet, Tenor Sax & Trombone"
  composer = "James Brown"
  arranger = ""
  poet = "gabriel@sauros.es"
  tagline = ""
  meter = \jazzTempoMarkup "Medium Funk" c4 "95"
}

% Función musical global
global = {
  % Para pintar todos los números de compás debajo del pentagrama
  % \override Score.BarNumber #'break-visibility = #end-of-line-invisible
  % \override Score.BarNumber #'direction = #DOWN
  % \override Score.BarNumber #'extra-offset = #'(1.5 . 0)
  \compressFullBarRests

  \set Timing.beamExceptions = #'()
  \set Timing.baseMoment = #(ly:make-moment 1/4)
  \set Timing.beatStructure = #'(1 1 1 1) 
}

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
  % Oculta el primer pentagrama del markup
  \override Staff.VerticalAxisGroup #'remove-first = ##t

  % Indicación de tiempo
  % \once \override Score.RehearsalMark.self-alignment-X = #LEFT
  \override Score.RehearsalMark.extra-offset = #'(0 . 2) 

  s1

  \part "Chorus"
  s1*2

  \part "Verse"
  s1*8
  \part "Chorus"
  s1*4

  \note "Keyboard solo"
  s1*8

  \part "Chorus"
  s1*2
  \part "Build"
  s1*4

  \mark \markup \line { "Bridge" \underline "(Repeat 4 xs)" }
  s1
  \note "Repeat 4 xs"
  s1
  \note "Repeat 4 xs"
  s1

  s1
  \note "Harp solo, 8 xs"
  s1
  \note "Guitar solo, 8 xs"
  s1
  \note "Tenor sax solo, 8 xs"
  s1
  \note "Trumpet solo, 8xs"
  s1

  \part "Audience Chorus"
  s1*2
  \part "Chorus"
  s1*2
  \part "Verse"
  s1*8
  \part "Chorus (\"Right on, right on\")"
  s1*4
  \part "Chorus"
  s1*4
  \part "(\"Shake your money-maker!\")"
  s1*8
  \part "Chorus"
  s1*4
  \part "Build"
  s1*4

  \mark \markup \line { "Bridge" \underline "(Repeat 4 xs)" }
  s1
  \note "Repeat 4 xs"
  s1
  \note "Repeat 4 xs"
  s1
}

nextPageRests = \markup \fill-line { 
  \null
  \small \line {
    \score {
      \new Staff \with {
        fontSize = #-2
        \override StaffSymbol #'staff-space = #(magstep -4)
      }
      \relative c'' { 
        \once \override Score.RehearsalMark.extra-offset = #'(10 . -1.5) 
        \mark \markup \scale #'(1 . -1) \rotate #0 \curvedArrow #0 #0 #0
        \compressFullBarRests 
        \once \override MultiMeasureRestNumber.stencil = #ly:text-interface::print
        \once \override MultiMeasureRestNumber.font-name = #"lilyjazz-text"
        \once \override MultiMeasureRestNumber.text = "ad.lib."
        R1*2
        \bar ""
      }
      \layout {
        indent = 0
        \context {
          \Staff
          \remove "Clef_engraver"
          \remove "Time_signature_engraver"
        }
      }
    }
    \hspace #9
  }
}

% Cifrados
scoreChords = \chords {
  s1*33
  d1:m 
  ef1:m 
  d1:m 
  ef1:m 
}

% Música
trumpet = \relative c'' {
  \global
  \key d \minor
  b8-> \mf b-> b-> b-> b->\< b-> b-> b->

  \break
  \bar "[|:"
  \repeat volta 2 {
    \once \override MultiMeasureRestNumber.stencil = #ly:text-interface::print
    \once \override MultiMeasureRestNumber.font-name = #"lilyjazz-text"
    \once \override MultiMeasureRestNumber.text = "ad.lib."
    R1*2 \!

    R1*8
    R1*4

    \break
    R1*2
    r4 a8-^ \p a-^ b8. \bendBefore a16-. r4
    r4 a8-^ a-^ b8. \bendBefore a16-. r4

    \break
    R1*2
    r4 a8-^ a-^ b8. \bendBefore a16-. r4
    r4 a8-^ a-^ b8. \bendBefore a16-. \footnote "" #'(0 . 0) \nextPageRests r4
  }
  \bar ":|]"

  \break
  \once \override MultiMeasureRestNumber.stencil = #ly:text-interface::print
  \once \override MultiMeasureRestNumber.font-name = #"lilyjazz-text"
  \once \override MultiMeasureRestNumber.text = "ad.lib."
  R1*2 \!
  R1*4

  \break
  \bar "[|:"
  \repeat volta 4 {
    e'16( \mf e)-. r8 d16( d)-. r c-.  r cs8-. d16 -> r4 
  }
  \bar ":|][|:"
  \repeat volta 4 {
    e16( e)-. r8 d16( d)-. r c-.  r cs8-. d16 -> r4 
  }
  \bar ":|][|:"
  \repeat volta 4 {
    e16( e)-. r8 d16( d)-. r c-.  r cs8-. d16 -> r4 
  }
  \bar ":|]"

  b8-> \mf b-> b-> b-> b->\< b-> b-> b->

  \break
  \bar "[|:"
  \improOn
  r4 \! r r r
  \improOff
  \bar ":|][|:"
  \impro 4
  \break
  \bar ":|][|:"
  \impro 4
  \bar ":|][|:"
  \impro 4
  \bar ":|]"

  \break
  \once \override MultiMeasureRestNumber.stencil = #ly:text-interface::print
  \once \override MultiMeasureRestNumber.font-name = #"lilyjazz-text"
  \once \override MultiMeasureRestNumber.text = "ad.lib."
  R1*2
  \once \override MultiMeasureRestNumber.stencil = #ly:text-interface::print
  \once \override MultiMeasureRestNumber.font-name = #"lilyjazz-text"
  \once \override MultiMeasureRestNumber.text = "ad.lib."
  R1*2

  R1*8
  \break
  R1*4
  R1*4
  \break
  \once \override MultiMeasureRest.minimum-length = #30
  R1*8
  R1*4
  R1*4

  \break
  \bar "[|:"
  \repeat volta 4 {
    e16( \mf e)-. r8 d16( d)-. r c-.  r cs8-. d16 -> r4 
  }
  \bar ":|][|:"
  \repeat volta 4 {
    e16( e)-. r8 d16( d)-. r c-.  r cs8-. d16 -> r4 
  }
  \bar ":|][|:"
  \repeat volta 4 {
    e16( e)-. r8 d16( d)-. r c-.  r cs8-. d16 -> r4 
  }
  \bar ":|]"

  b8-> \f b-> b-> b-> b4-^ r4

  \bar "|."
}

tenorSax = \relative c'' {
  \global
  \key d \minor
  e8-> \mf e-> e-> e-> e->\< e-> e-> e->

  \break
  \repeat volta 2 {
    R1*2 \!

    R1*8
    R1*4

    \break
    R1*2
    r4 f8-^ \p f-^ g8. \bendBefore a16-. r4
    r4 f8-^ f-^ g8. \bendBefore a16-. r4

    \break
    R1*2
    r4 f8-^ f-^ g8. \bendBefore a16-. r4
    r4 f8-^ f-^ g8. \bendBefore a16-. r4
  }

  \break
  R1*2 \!
  R1*4

  \break
  \repeat volta 4 {
    a16( \mf a)-. r8 a16( a)-. r g-.  r gs8-. a16 -> r4 
  }
  \repeat volta 4 {
    a16( a)-. r8 a16( a)-. r g-.  r gs8-. a16 -> r4 
  }
  \repeat volta 4 {
    a16( a)-. r8 a16( a)-. r g-.  r gs8-. a16 -> r4 
  }

  e8-> \mf e-> e-> e-> e->\< e-> e-> e->

  \break
  R1*4

  \break
  R1*2
  R1*2

  R1*8
  \break
  R1*4
  R1*4
  \break
  R1*8
  R1*4
  R1*4

  \break
  \repeat volta 4 {
    a16( \mf a)-. r8 a16( a)-. r g-.  r gs8-. a16 -> r4 
  }
  \repeat volta 4 {
    a16( a)-. r8 a16( a)-. r g-.  r gs8-. a16 -> r4 
  }
  \repeat volta 4 {
    a16( a)-. r8 a16( a)-. r g-.  r gs8-. a16 -> r4 
  }

  e8-> \f e-> e-> e-> e4-^ r
}

trombone = \relative c' {
  \global
  \key d \minor
  \clef bass

  cs8-> \mf cs-> cs-> cs-> cs->\< cs-> cs-> cs->

  \break
  \repeat volta 2 {
    R1*2 \!

    R1*8
    R1*4

    \break
    R1*2
    r4 c8-^ \p c-^ d8. \bendBefore c16-. r4
    r4 c8-^ c-^ d8. \bendBefore c16-. r4

    \break
    R1*2
    r4 c8-^ c-^ d8. \bendBefore c16-. r4
    r4 c8-^ c-^ d8. \bendBefore c16-. r4
  }

  \break
  R1*2 \!
  R1*4

  \break
  \repeat volta 4 {
    a16( \mf a)-. r8 a16( a)-. r g-.  r gs8-. a16 -> r4 
  }
  \repeat volta 4 {
    a16( a)-. r8 a16( a)-. r g-.  r gs8-. a16 -> r4 
  }
  \repeat volta 4 {
    f'16( f)-. r8 f16( f)-. r ef-.  r e8-. f16 -> r4 
  }

  cs8-> \mf cs-> cs-> cs-> cs->\< cs-> cs-> cs->

  \break
  R1*4

  \break
  R1*2
  R1*2

  R1*8
  \break
  R1*4
  R1*4
  \break
  R1*8
  R1*4
  R1*4

  \break
  \repeat volta 4 {
    a16( \mf a)-. r8 a16( a)-. r g-.  r gs8-. a16 -> r4 
  }
  \repeat volta 4 {
    a16( a)-. r8 a16( a)-. r g-.  r gs8-. a16 -> r4 
  }
  \repeat volta 4 {
    f'16( f)-. r8 f16( f)-. r ef-.  r e8-. f16 -> r4 
  }

  cs8-> \f cs-> cs-> cs-> cs4-^ r
}

% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\book {
  \paper { 
    % Descomentar para score
    % #(set-paper-size "a4" 'landscape)
    % indent = 20
    % systems-per-page = 2
  }

  \score {
    % Descomentar para midi
    % \midi { \tempo 4 = 110 }

    % NO USAR layout-set-staff-size O SE ROMPE lilyjazz!!!
    \layout { }

    <<
      \scoreMarkup
      \transpose bf c' \scoreChords
      \new StaffGroup <<
        \new Staff \with 
        { instrumentName = "Trpt."}
        { \transpose bf c' \trumpet } 

        \new Staff \with 
        { instrumentName = "T Sx." \RemoveEmptyStaves }
        { \transpose bf c' \tenorSax } 

        \new Staff \with 
        { instrumentName = "Trbn." \RemoveEmptyStaves } 
        { \trombone }
      >>
    >>
  }
}
