% Se debe incluir antes que lilyjazz. Si no, lilyjazz no funciona.
#(set-global-staff-size 20)

\include "main.ily"
\include "music-sheet-paper.ily"

\paper {
  first-page-number = 2
}

\header {
  title = "Take my love with you / Shout"
  instrument = "Trumpet, Tenor Sax & Trombone"
  composer = "Eli \"Paperboy\" Reed"
  arranger = "Ilsey Brothers"
  poet = "gabriel@sauros.es"
  tagline = ""
  meter = \jazzTempoMarkup "Medium Swing" c4 "135"
}

% Función musical global
global = {
  % Para pintar todos los números de compás debajo del pentagrama
  % \override Score.BarNumber #'break-visibility = #end-of-line-invisible
  % \override Score.BarNumber #'direction = #DOWN
  % \override Score.BarNumber #'extra-offset = #'(1.5 . 0)
  \compressFullBarRests

  \set Timing.beamExceptions = #'()
  \set Timing.baseMoment = #(ly:make-moment 1/4)
  \set Timing.beatStructure = #'(1 1 1 1) 
}

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
  % Oculta el primer pentagrama del markup
  \override Staff.VerticalAxisGroup #'remove-first = ##t

  % Indicación de tiempo
  % \once \override Score.RehearsalMark.self-alignment-X = #LEFT
  \override Score.RehearsalMark.extra-offset = #'(0 . 2) 
  % \mark \jazzTempoMarkup "Easy Swing" c4 "112"

  s1*6

  \part "Chorus"
  s1*12
  \note "Repeat 3 xs"
  s1*4

  \part "Verse"
  s1*8
  s1*8

  \part "Chorus"
  s1*12
  \note "Repeat 4 xs"
  s1*2

  \note "Tenor solo"
  s1*8

  \part "Verse"
  s1*16

  \part "Chorus"
  s1*12
  \note "Repeat 8 xs"
  s1*3

  \note "Harp solo, Repeat ad.lib."
  s1*4

  \note "Harp solo"
  s1*4

  \once \override Score.RehearsalMark.extra-offset = #'(0 . 0) 
  \mark \markup \line { "Chorus " \underline "(Repeat 4 xs)" }
  s1*5

  \note "Repeat ad.lib."
  s1*4

  \mark \markup \line { "Chorus " \underline "(Repeat 7 xs)" }
  s1*3
  \mark \markup \underline "(Shout! or .."
  s1
  \mark \markup \underline "... Fine?)"
  s1

  \once \override Score.RehearsalMark.extra-offset = #'(0 . 0) 
  \mark \markup \line { "Chorus " \underline "(Shout! The Isley Brothers)" }
  s1*6

  \mark \markup \line { "Verse " \underline "(Repeat 4 xs)" }
  s1*3

  \mark \markup \line { "Chorus " \underline "(Repeat 4 xs)" }
  s1*2
  \mark \markup \line { "Verse" \underline "(Repeat 4 xs)" }
  s1*2

  s1*3
  \once \override Score.RehearsalMark.extra-offset = #'(0 . 0) 
  \note "Half-time feel"
  s1*16

  \mark \markup \general-align #Y #DOWN {
    \column { \musicglyph #"scripts.segno" }
    \column { \underline "(Repeat ad.lib.)" }
  }
  s1*2
  \note "On singers cue"
  s1*4

  \mark \markup \line { "Madhouse Impro" \underline "(Repeat ad.lib.)" }
  s1*2
  s1
  \eolMark \markup \underline \line {
    "(Optional D.S. "
    \fontsize #-4 \raise #1 { \musicglyph #"scripts.varsegno" }
    ")"
  }
}

% Cifrados
scoreChords = \chords {
  % c1:m f2:m g2:m7

  s1*52

  c1:m
  s1
  af1
  s1

  c1:m
  s1
  fs:7
  b1
}

% Música
trumpet = \relative c'' {
  \global
  \key ef \major

  R1
  c1\>\fermata \f

  R1\!
  ef,1\>\fermata

  \break
  R1\!
  g'1\fermata
  \bar "||"

  R1*12

  \break
  \bar "[|:"
  \repeat volta 3 {
    r4 ef8( \mf f ef-.) r r4
    r4 c8( bf c8-.) r r4
  }
  \bar ":|]"
  ef1-> \<
  ef4-.\! r4 r2

  \bar "||"
  \break
  ef2.~-> \mf ef8 c
  ef4-. r4 r2
  ef2.~-> ef8 c
  bf4-. r4 r2	

  \break
  ef2.~-> ef8 c
  ef4-. r4 r2
  f2.~-> f8 cs
  d4-. r4 r2

  \break
  ef2.~-> ef8 c
  ef4-. r4 r2
  ef2.~-> ef8 c
  bf4-. r4 r2	

  \break
  ef2~-> ef8 c ef c
  ef4-. r4 r2
  ef2.~-> ef8 c
  ef4-. r4 r2

  R1*12

  \pageBreak
  \bar "[|:"
  \repeat volta 4 {
    r4 ef8( \mf f ef-.) r r4
    r4 c8( bf c8-.) r r4
  }
  \bar ":|]"

  \break
  R1*4
  \break
  R1*4

  \break
  \bar "||"
  \key e \major
  e2.~-> \mf e8 cs
  e4-. r4 r2
  e2.~-> e8 cs
  b4-. r4 r2 	

  \break
  e2.~-> e8 cs
  e4-. r4 r2
  fs2.~-> fs8 es
  fs4-. r4 r2

  \break
  e2.~-> e8 cs
  e4-. r4 r2
  e2.~-> e8 cs
  b4-. r4 r2 	

  \break
  e2~-> e8 cs e cs
  e4-. r4 r2
  e2.~-> e8 cs
  e4-. r4 r2


  R1*12

  \break
  \bar "[|:"
  \repeat volta 8 {
    r4 e8( \mf fs e-.) r r4
  }
  \alternative {{
    r4 cs8( b cs-.) r r4 
    \bar ":|]" 
  }{
    r4 cs8( b cs-.) r r4
  }}

  \break
  \bar "[|:"
  \repeat volta 4 {
    \showStave R1
    R1*3
  }

  \break
  \bar ":|][|:"
  \repeat volta 2 {
    \showStave R1
    R1*3
  }

  \pageBreak
  \bar ":|][|:"
  \repeat volta 4 {
    \showStave R1*3
  }
  \alternative {{
    R1
    \bar ":|]"
  }{
    R1
  }}

  \break
  \bar ":|][|:"
  \repeat volta 2 {
    \showStave R1
    R1*3
  }

  \break
  \bar ":|][|:"
  \repeat volta 7 {
    r4 e8( \f fs e-.) r r4
  }
  \alternative {{
    r4 cs8( b cs-.) r r4 
    \bar ":|]" 
  }{
    \break
    r4 cs8( b cs-.) r r4 
  }}

  e1\fermata
  \bar ".|"
  e1\fermata

  \pageBreak
  \bar "[|:-|."
  \repeat volta 2 {
    e4-> \f r4 r2
    e4-> r4 r2
  }
  \bar ":|]"
  e4-> r4 r2
  R1*3

  \break
  \bar "[|:"
  \repeat volta 4 {
    r4 e,2~-> \mf e8 cs
  }
  \alternative {{
    e4-> r8 gs r gs r gs
    \bar ":|]" 
  }{
    e4-> r8 gs r gs r4
  }}

  \break
  \bar "[|:"
  \repeat volta 4 {
    e'4-> \f r4 r2
    e4-> r4 r2
  }

  \bar ":|][|:"
  \repeat volta 4 {
    r4 e,8 \mf ds e e-. r ds(
    e4)-. r8 e r e r e
    r4 e8 ds e e-. r ds(
  }
  \alternative {{
    e4)-. r8 e r e r e 
    \bar ":|]" 
  }{
    e4-. r4 r2
  }}

  R1*16

  \pageBreak
  \bar "[|:-||"
  \repeat volta 2 {
    e'4-> \f r4 r2
    e4-> r4 r2
  }	

  \break
  \bar ":|][|:"
  \repeat volta 2 {
    e4-> r4 r2
    r4 e4-> \ff b-> fs'->
  }
  \bar ":|][|:"
  \repeat volta 2 {
    e4-> r4 r2
    r4 g4-> fs-> e8 cs
  }

  \break
  \bar ":|][|:"
  \repeat volta 2 {
    e4-> \mf r4 r2
    R1
  }
  \bar ":|]"

  e1 \f \fermata
  \bar "||"
  \ottava #1 e'1 \fermata

  \bar "|."
}

tenorSax = \relative c'' {
  \global
  \key ef \major

  R1
  g'1\>\fermata \f

  R1\!
  c,1\>\fermata

  R1\!
  g'2\glissando bf2\fermata

  R1*12
  \repeat volta 3 {
    r4 g8( \mf af g-.) r r4
    r4 af8( ef af8-.) r r4
  }
  af1-> \<
  g4-.\! r4 r2

  g2.~-> \mf g8 fs
  g4-. r4 r2
  af2.~-> af8 fs
  g4-. r4 r2 

  g2.~-> g8 fs
  g4-. r4 r2
  a2.~-> a8 a
  bf4-. r4 r2

  g2.~-> g8 fs
  g4-. r4 r2
  af2.~-> af8 fs
  g4-. r4 r2 

  g2~-> g8 fs g fs
  g4-. r4 r2
  af2.~-> af8 fs
  g4-. r4 r2

  R1*12
  \repeat volta 4 {
    r4 g8( \mf af g-.) r r4
    r4 af8( ef af8-.) r r4
  }

  \impro 32

  \key e \major
  gs2.~-> \mf gs8 fss
  gs4-. r4 r2
  a2.~-> a8 fss
  gs4-. r4 r2

  gs2.~-> gs8 fss
  gs4-. r4 r2
  as2.~-> as8 as
  b4-. r4 r2

  gs2.~-> gs8 fss
  gs4-. r4 r2
  a2.~-> a8 fss
  gs4-. r4 r2

  gs2~-> gs8 fss gs fss
  gs4-. r4 r2
  a2.~-> a8 fss
  gs4-. r4 r2

  R1*12
  \repeat volta 8 {
    r4 gs8( \mf a gs-.) r r4
  }
  \alternative {{
    r4 a8( e a8-.) r r4
  }{
    r4 a8( e a8-.) r r4
  }}

  \repeat volta 2 {
    b,,8-> \mf r r4 r4 r8 bs(
    cs8)-> r r4 r2 
    b8->  r r4 r4 r8 bs(
    cs8)-> r r4 r2 
  }

  \repeat volta 2 {
    R1*4
  }

  \repeat volta 4 {
    \showStave R1*3
  } \alternative {{
    R1
  }{
    R1
  }}

  \repeat volta 2 {
    b8-> \mf r r4 r2
    r2 r4 r8 as(
    b8)-> r r4 r2
    r2 r4 r8 as(
  }

  \repeat volta 7 {
    b8) r gs''8( \f a gs-.) r8 r4
  }
  \alternative {{
    r4 a8( e a8-.) r8 r8 as,,
  } {
    r4 a''8( e a8-.) r8 r4
  }}

  g1\fermata
  b1\fermata

  \repeat volta 2 {
    gs4-> \f r4 r2
    gs4-> r4 r2
  }
  gs4-> r4 r2
  R1*3

  \repeat volta 4 {
    r4 b,2~-> \mf b8 gs
  }
  \alternative {{
    cs4-> r8 e r e r e
  }{
    cs4-> r8 e r e r4
  }}

  \repeat volta 4 {
    gs4-> \f r4 r2
    gs4-> r4 r2
  }

  \repeat volta 4 {
    r4 b,8 \mf as b b-. r as(
    b4)-. r8 b r b r cs
    r4 cs8 bs cs cs-. r bs(
  }
  \alternative {{
    cs4)-. r8 cs r cs r b 
  }{
    cs4-. r4 r2
  }}

  R1*16

  \repeat volta 2 {
    gs'4-> \f r4 r2
    gs4-> r4 r2
  }	
  \repeat volta 2 {
    gs4-> r4 r2
    r4 e4-> \ff b-> a'->
  }
  \repeat volta 2 {
    gs4-> r4 r2
    r4 g4-> fs-> e8 fs
  }
  \repeat volta 2 {
    gs4-> \mf r4 r2
    R1
  }

  g1 \f \fermata
  b1\fermata
}

trombone = \relative c' {
  \global
  \clef bass
  \key ef \major

  R1
  ef1\>\fermata \f

  R1\!
  g,1\>\fermata

  R1\!
  ef'1\fermata

  R1*12
  \repeat volta 3 {
    r4 bf8( \mf c bf-.) r r4
    r4 ef8( c ef8-.) r r4
  }
  bf1->\<
  ef4-.\! r4 r2

  ef2.~-> \mf ef8 d
  ef4-. r4 r2
  c2.~-> c8 d
  ef4-. r4 r2

  ef2.~-> ef8 d
  ef4-. r4 r2
  c2.~-> c8 e
  f4-. r4 r2

  ef2.~-> ef8 d
  ef4-. r4 r2
  c2.~-> c8 d
  ef4-. r4 r2

  ef2~-> ef8 d ef d
  ef4-. r4 r2
  bf2.~-> bf8 d
  ef4-. r4 r2

  R1*12
  \repeat volta 4 {
    r4 bf8( \mf c bf-.) r r4
    r4 ef8( c ef8-.) r r4
  }

  R1*8

  \key e \major
  e2.~-> \mf e8 ds
  e4-. r4 r2
  cs2.~-> cs8 ds
  e4-. r4 r2

  e2.~-> e8 ds
  e4-. r4 r2
  cs2.~-> cs8 es
  fs4-. r4 r2

  e2.~-> e8 ds
  e4-. r4 r2
  cs2.~-> cs8 ds
  e4-. r4 r2

  e2~-> e8 ds e ds
  e4-. r4 r2
  b2.~-> b8 ds
  e4-. r4 r2

  R1*12
  \repeat volta 8 {
    r4 b8( \mf cs b-.) r r4
  }
  \alternative {{
    r4 e8( cs e8-.) r r4
  }{
    r4 e8( cs e8-.) b( \ff cs b
  }}

  \repeat volta 4 {
    e,1) ^\markup \underline "(Play only 1st x)" \bendAfter -3 
    R1*3
  }

  \repeat volta 2 {
    R1*4
  }

  \repeat volta 4 {
    R1*3
  } \alternative {{
    R1
  }{
    r2 r8 b'( \ff cs b
  }}

  \repeat volta 2 {
    e,8)-> r r4 r2
    r2 r4 r8 ds( \mf
    e8)-> r r4 r2
    r2 r4 r8 ds
  }

  \repeat volta 7 {
    e8-> r b'8( \f cs b-.) r r4
  }
  \alternative {{
    r4 e8( cs e-.) r r ds,
  }{
    r4 e'8( cs e-.) r r4
  }}

  a,1\fermata
  e'1\fermata

  \repeat volta 2 {
    b4-> \f r4 r2
    cs4-> r4 r2
  }
  b4-> r4 r2
  R1*3

  \repeat volta 4 {
    r4 gs2~-> \mf gs8 e
  }
  \alternative {{
    gs4-> r8 cs r cs r b
  }{
    gs4-> r8 cs r cs r4
  }}

  \repeat volta 4 {
    b4-> \f r4 r2
    cs4-> r4 r2
  }

  \repeat volta 4 {
    r4 gs8 \mf fss gs gs-. r fss(
    gs4)-. r8 gs r gs r gs
    r4 gs8 fss gs gs-. r fss(
  }
  \alternative {{
    gs4)-. r8 gs r gs r gs 
  }{
    gs4-. r4 r2
  }}

  R1*16

  \repeat volta 2 {
    b4-> \f r4 r2
    cs4-> r4 r2
  }	
  \repeat volta 2 {
    b4-> r4 r2
    r4 e4-> \ff b-> cs->
  }
  \repeat volta 2 {
    b4-> r4 r2
    r4 g'4-> fs-> cs8 a
  }
  \repeat volta 2 {
    b4-> \mf r4 r2
    R1
  }

  a1\fermata \f
  e'1\fermata
}

% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\book {
  \paper { 
    % Descomentar para score
    % #(set-paper-size "a4" 'landscape)
    % indent = 20
    % systems-per-page = 2
  }

  \score {
    % Descomentar para midi
    % \midi { \tempo 4 = 110 }

    % NO USAR layout-set-staff-size O SE ROMPE lilyjazz!!!
    \layout { }

    <<
      \scoreMarkup
      \new StaffGroup <<
        \new Staff \with 
        { instrumentName = "Trpt." \RemoveEmptyStaves }
        { \transpose bf c' \trumpet } 

        \transpose bf c' \scoreChords
        \new Staff \with 
        { instrumentName = "T Sx." \RemoveEmptyStaves }
        { \transpose bf c' \tenorSax } 

        \new Staff \with 
        { instrumentName = "Trbn." \RemoveEmptyStaves } 
        { \trombone }
      >>
    >>
  }
}
