\version "2.18.2"

% Se debe incluir antes que lilyjazz. Si no, lilyjazz no funciona.
#(set-global-staff-size 20)

\include "main.ily"
\include "music-sheet-paper.ily"

\paper {
  first-page-number = 2
}

\header {
  title = "Cry to me"
  instrument = "Trumpet, Tenor Sax & Trombone"
  composer = "Solomon Burke"
  arranger = ""
  poet = "gabriel@sauros.es"
  tagline = ""
  meter = \jazzTempoMarkup "Soul" c4 "95"
}

% Función musical global
global = {
  % Para pintar todos los números de compás debajo del pentagrama
  % \override Score.BarNumber #'break-visibility = #end-of-line-invisible
  % \override Score.BarNumber #'direction = #DOWN
  % \override Score.BarNumber #'extra-offset = #'(1.5 . 0)
  \compressFullBarRests

  \set Timing.beamExceptions = #'()
  \set Timing.baseMoment = #(ly:make-moment 1/4)
  \set Timing.beatStructure = #'(1 1 1 1) 
}

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
  % Oculta el primer pentagrama del markup
  \override Staff.VerticalAxisGroup #'remove-first = ##t

  % Indicación de tiempo
  % \once \override Score.RehearsalMark.self-alignment-X = #LEFT
  \override Score.RehearsalMark.extra-offset = #'(0 . 2) 

  s1*8

  \part "Verse"
  s1*8

  \part "Chorus"
  s1*10

  \part "Verse II"
  s1*10

  \note "Harp. solo"
  s1*8

  \part "Verse"
  s1*8

  \mark \markup \line { "Chorus" \underline "(Repeat 3 xs)" }
}

nextPageRests = \markup \fill-line { 
  \null
  \small \line {
    \score {
      \new Staff \with {
        fontSize = #-2
        \override StaffSymbol #'staff-space = #(magstep -4)
      }
      \relative c'' { 
        \once \override Score.RehearsalMark.extra-offset = #'(7 . -1.5) 
        \mark \markup \scale #'(1 . -1) \rotate #0 \curvedArrow #0 #0 #0
        \compressFullBarRests R1*7
        \bar ""
      }
      \layout {
        indent = 0
        \context {
          \Staff
          \remove "Clef_engraver"
          \remove "Time_signature_engraver"
        }
      }
    }
    \hspace #9
  }
}


% Cifrados
scoreChords = \chords {
  % c1:m f2:m g2:m7
}

% Música
trumpet = \relative c'' {
  \global
  \key f \major

  R1*4
  \repeat percent 4 {
    r4 r8 \p f-> r2
  }

  \break
  \bar "[|:"
  \repeat volta 2 {
    r4 r8 \p f-> r2
    r4 r8 f-> r2
    r4 r8 f-> r2
    r4 r8 f-> r2

    r4 r8 f-> r2
    r4 r8 f-> r2
    r4 r8 f-> r2
    r4 r8 f-> r2

    r4 \p e,8 \< e e4-. e4-. \!
    r4 \p e8 \< e e4-. e4-. \!
    r4 \p f8 \< f f4-. f4-. \!
    r4 \p f8 \< f f4-. f4-. \!

    r4 \p e8 \< e e4-. e4-. \!
    r4 \p e8 \< e e4-. e4-. \!

  } \alternative {{
    r4 \p f8 \< f f4-. f4-. \!
    r4 f8 \mf f f4-. r
    \bar ":|]"
  }{
    r4 a8( \mf a a4 bf4
  }}
  c4 \< d ef2

  \bar "[|:"
  \repeat volta 2 {
    f1 ) \f \>
    R1 \!
    f,4-> \p r8 f-> r2
    f4-> r8 f-> r2

    r4 \p e8 \< e e4-. \! r4
    r4 \p e8 \< e e4-. \! r4

  } \alternative {{
    \times 2/3 { r4 a ( \mf bf } \times 2/3 { c c d }
    ef1 ) \<
    \bar ":|]"
  }{
    f,4-> \p r8 f-> r2
  }}
  \footnote "" #'(0 . 0) \nextPageRests f4-> r r2

  R1*8

  \bar "||"
  r4 r8 \p f'-> r2
  r4 r8 f-> r2
  r4 r8 f-> r2
  r4 r8 f-> r2

  r4 r8 f-> r2
  r4 r8 f-> r2
  r4 r8 f-> r2
  r4 r8 f-> r2

  \bar "[|:"
  \repeat volta 3 {
    r2 r4 \p e8 \< e
    e4-. \! r r2
    r2 r4 \p f8 \< f
    f4-. \! r r2

    r2 r4 \p e8 \< e
    e4-. \! r r2

  } \alternative {{
    \set Score.repeatCommands = #'((volta "1, 2"))
    r2 r4 \p f8 \< f
    f4-. \! r r2
    \bar ":|]"
  }{
    r2 r4 f8 \f f
  }}
  f4-. r r2

  \bar "|."
}

tenorSax = \relative c'' {
  \global
  \key f \major

  R1*4
  \repeat percent 4 {
    r4 r8 \p a'-> r2
  }

  \bar "[|:"
  \repeat volta 2 {
    r4 r8 \p a-> r2
    r4 r8 a-> r2
    r4 r8 a-> r2
    r4 r8 a-> r2

    r4 r8 bf-> r2
    r4 r8 bf-> r2
    r4 r8 a-> r2
    r4 r8 a-> r2

    r4 \p bf,8 \< bf bf4-. bf4-. \!
    r4 \p bf8 \< bf bf4-. bf4-. \!
    r4 \p a8 \< a a4-. a4-. \!
    r4 \p a8 \< a a4-. a4-. \!

    r4 \p bf8 \< bf bf4-. bf4-. \!
    r4 \p bf8 \< bf bf4-. bf4-. \!

  } \alternative {{
    r4 \p a8 \< a a4-. a4-. \!
    r4 a8 \mf a a4-. r
    \bar ":|]"
  }{
    r4 f'8( \mf f f4 g4
  }}
  a4 \< bf c2

  \bar "[|:"
  \repeat volta 2 {
    bf1 ) \f \>
    R1 \!
    a,4-> \p r8 a-> r2
    a4-> r8 a-> r2

    r4 \p bf8 \< bf bf4-. \! r4
    r4 \p bf8 \< bf bf4-. \! r4

  } \alternative {{
    \times 2/3 { r4 f' ( \mf g } \times 2/3 { a a bf }
    c1 ) \<
    \bar ":|]"
  }{
    a,4-> \p r8 a-> r2
  }}
  a4-> r r2

  R1*8

  r4 r8 \p a'-> r2
  r4 r8 a-> r2
  r4 r8 a-> r2
  r4 r8 a-> r2

  r4 r8 a-> r2
  r4 r8 a-> r2
  r4 r8 a-> r2
  r4 r8 a-> r2

  \bar "[|:"
  \repeat volta 3 {
    r2 r4 \p bf8 \< bf
    bf4-. \! r r2
    r2 r4 \p a8 \< a
    a4-. \! r r2

    r2 r4 \p bf8 \< bf
    bf4-. \! r r2

  } \alternative {{
    \set Score.repeatCommands = #'((volta "1, 2"))
    r2 r4 \p a8 \< a
    a4-. \! r r2
    \bar ":|]"
  }{
    r2 r4 a8 \f a
  }}
  a4-. r r2

  \bar "|."
}

trombone = \relative c' {
  \global
  \key f \major
  \clef bass

  R1*3
  r2 r4 d,8( \p c

  \repeat percent 4 {
    f,4-.) r8 f''-> r4 d,8( c)
  }

  \bar "[|:"
  \repeat volta 2 {
    f,4-. r8 f''-> r4 d,8( c
    f,4-.) r8 f''-> r4 d,8( c
    f,4-.) r8 f''-> r4 d,8( c
    f,4-.) r8 f''-> r4 g,8( f

    \break
    bf,4-.) r8 f''-> r4 g,8( f
    bf,4-.) r8 f''-> r4 d,8( c
    f,4-.) r8 f''-> r4 d,8( c
    f,4-.) r8 f''-> r2

    \break
    r4 \p c,8 \< c c4-. c4-. \!
    r4 \p c8 \< c c4-. c4-. \!
    r4 \p f8 \< f f4-. f4-. \!
    r4 \p f8 \< f f4-. f4-. \!

    \break
    r4 \p c8 \< c c4-. c4-. \!
    r4 \p c8 \< c c4-. c4-. \!

  } \alternative {{
    r4 \p f8 \< f f4-. f4-. \!
    r4 f8 \mf f f4-. d8 ( \p c )
    \bar ":|]"
  }{
    \break
    r4 a'8( \mf a a4 bf4
  }}
  c4 \< d ef2

  \bar "[|:"
  \repeat volta 2 {
    d1 ) \f \>
    R1 \!
    \break
    f,4-> \p r8 f-> r2
    f,4-> r8 f-> r2

    r4 \p c'8 \< c c4-. \! r4
    r4 \p c8 \< c c4-. \! r4

  } \alternative {{
    \times 2/3 { r4 a' ( \mf bf } \times 2/3 { c c d }
    ef1 ) \<
    \bar ":|]"
  }{
    f,4-> \p r8 f-> r8 c-. d( c
  }}
  f,4->) r r2

  \pageBreak
  \once \override MultiMeasureRest #'minimum-length = #65
  R1*7
  r2 r4 d'8( \p c

  \break
  f,4-.) r8 f''-> r4 d,8( c
  f,4-.) r8 f''-> r4 d,8( c
  f,4-.) r8 f''-> r4 d,8( c
  f,4-.) r8 f''-> r4 g,8( f

  \break
  bf,4-.) r8 f''-> r4 g,8( f
  bf,4-.) r8 f''-> r4 d,8( c
  f,4-.) r8 f''-> r4 d,8( c
  f,4-.) r8 f''-> r2

  \bar "[|:"
  \repeat volta 3 {
    r2 r4 \p c8 \< c
    c4-. \! r r2
    r2 r4 \p f,8 \< f
    f4-. \! r r2

    \break
    r2 r4 \p c'8 \< c
    c4-. \! r r2

  } \alternative {{
    \set Score.repeatCommands = #'((volta "1, 2"))
    r2 r4 \p f,8 \< f
    f4-. \! r r2
    \bar ":|]"
  }{
    r2 r4 f8 \f f
  }}
  f4-. r r2

  \bar "|."
}

% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\book {
  \paper { 
    % Descomentar para score
    % #(set-paper-size "a4" 'landscape)
    % indent = 20
    % systems-per-page = 2
  }

  \score {
    % Descomentar para midi
    % \midi { \tempo 4 = 110 }

    % NO USAR layout-set-staff-size O SE ROMPE lilyjazz!!!
    \layout { }

    <<
      \scoreMarkup
      \transpose bf c' \scoreChords
      \new StaffGroup <<
        \new Staff \with 
        { instrumentName = "Trpt."}
        { \transpose bf c' \trumpet } 

        \new Staff \with 
        { instrumentName = "T Sx." \RemoveEmptyStaves }
        { \transpose bf c' \tenorSax } 

        \new Staff \with 
        { instrumentName = "Trbn." \RemoveEmptyStaves } 
        { \trombone }
      >>
    >>
  }
}
