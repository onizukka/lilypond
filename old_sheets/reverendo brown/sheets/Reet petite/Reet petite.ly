% Se debe incluir antes que lilyjazz. Si no, lilyjazz no funciona.
#(set-global-staff-size 20)

\include "main.ily"
\include "music-sheet-paper.ily"

\paper {
}

\header {
  title = "Reet petite"
  instrument = "Trumpet, Tenor Sax & Trombone"
  composer = "Jackie Wilson"
  arranger = ""
  poet = "gabriel@sauros.es"
  tagline = ""
  meter = \jazzTempoMarkup "Medium Swing" c4 "160"
}

% Función musical global
global = {
  % Para pintar todos los números de compás debajo del pentagrama
  % \override Score.BarNumber #'break-visibility = #end-of-line-invisible
  % \override Score.BarNumber #'direction = #DOWN
  % \override Score.BarNumber #'extra-offset = #'(1.5 . 0)
  \compressFullBarRests

  \set Timing.beamExceptions = #'()
  \set Timing.baseMoment = #(ly:make-moment 1/4)
  \set Timing.beatStructure = #'(1 1 1 1) 
}

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
  % Oculta el primer pentagrama del markup
  \override Staff.VerticalAxisGroup #'remove-first = ##t

  % Indicación de tiempo
  % \once \override Score.RehearsalMark.self-alignment-X = #LEFT
  \override Score.RehearsalMark.extra-offset = #'(0 . 2) 

  s1
  s1*4

  \part "Verse"
  s1*12

  \part "Verse"
  s1*12

  \part "Chorus"
  s1*8

  \part "Verse"
  s1*12

  \part "Chorus"
  s1*8

  s1*4

  \part "Verse"
  s1*12

  \part "Verse"
  s1*12

  \part "Chorus"
  s1*4
}

% Cifrados
scoreChords = \chords {
  % c1:m f2:m g2:m7
}

% Música
trumpet = \relative c'' {
  \global
  \key f \major

  r1

  \bar "||"
  r8 \mf f-^ r4 f-^ r
  f4-^ r r2
  r8 f-^ r4 f-^ r
  f4-^ r8 f-^ r2

  \break
  \bar "||"
  R1*2
  r8 \mf f-^ r4 f-^ r
  r8 f-^ r4 f-^ r

  R1*2
  r8 f-^ r4 f-^ r
  r8 f-^ r4 f-^ r

  r4 g---^ r g---^
  r4 af---^ r af---^
  r8 f-^ r f( \p \< f f f4-^)
  f4-^ \f r r2

  \bar "||"
  R1*12

  \break
  r8 \mf a,-> ~ a4 g-^ f-^
  d4-^ r r2
  r8 f( d4-.) c( d-.)
  f4-^ r r2

  R1*2
  r8 f'-^ r4 f-^ r
  f4-^ r r2

  \break
  \bar "[|:"
  \repeat volta 2 {
    R1*2
    f4-^ \mf r f-^ r
    f4-^ r8 f f4-^ f-^

    R1*6
    r8 f-^ r4 f-^ r
    f4-^ r r2
  }
  \bar ":|]"

  r8 a,-> ~ a4 g-^ f-^
  d4-^ r r2
  r8 f( d4-.) c( d-.)
  f4-^ r r2

  R1*4

  \bar "||"
  r8 \mf f'-^ r4 f-^ r
  f4-^ r r2
  r8 f-^ r4 f-^ r
  f4-^ r8 f-^ r2

  \break
  R1*2
  f4-^ \mf r f-^ r
  f4-^ r8 f f4-^ f-^

  R1*6
  r8 f-^ r4 f-^ r
  f4-^ r8 f-^ r2

  \bar "||"
  R1*2
  r2 r4 f---^ \f
  g4---^ r r2

  R1*6
  r8 \mf f-^ r4 f-^ r
  f4-^ r r2

  \break
  \bar "||"
  r8 a,-> ~ a4 g-^ f-^
  d4-^ r r2
  r8 f( d4-.) c( d-.)
  f4-^ r r2
  
  \break
  \bar "[|:"
  \repeat volta 2 {
    R1*2
    r8 \p f-^ r4 f-^ r
    f4-^ r8 f-^ r2
  }
  \bar ":|]"

  r8 \f f'-^ r4 f-^ r
  f4-^ r8 f-^ r2
  
  \bar "|."
}

tenorSax = \relative c'' {
  \global
  \key f \major

  r1

  r8 \mf f-^ r4 f-^ r
  f4-^ r r2
  r8 f-^ r4 f-^ r
  f4-^ r8 f-^ r2

  R1*2
  r8 \mf d-^ r4 d-^ r
  r8 d-^ r4 d-^ r

  R1*2
  r8 d-^ r4 d-^ r
  r8 d-^ r4 d-^ r

  r4 e---^ r e---^
  r4 f---^ r f---^
  r8 d-^ r d( \p \< d d d4-^)
  d4-^ \f r r2

  R1*12

  r8 \mf a-> ~ a4 g-^ f-^
  d4-^ r r2
  r8 f( d4-.) c( d-.)
  f4-^ r r2

  R1*2
  r8 d'-^ r4 d-^ r
  d4-^ r r2

  \repeat volta 2 {
    R1*2
    c4-^ \mf r d-^ r
    ef4-^ r8 ef ef4-^ ef-^

    R1*6
    r8 d-^ r4 d-^ r
    d4-^ r r2
  }

  r8 a-> ~ a4 g-^ f-^
  d4-^ r r2
  r8 f( d4-.) c( d-.)
  f4-^ r r2

  R1*4

  r8 \mf f'-^ r4 f-^ r
  f4-^ r r2
  r8 f-^ r4 f-^ r
  f4-^ r8 f-^ r2

  R1*2
  c4-^ \mf r d-^ r
  ef4-^ r8 ef ef4-^ ef-^

  R1*6
  r8 d-^ r4 d-^ r
  d4-^ r8 d-^ r2

  R1*2
  r2 r4 d---^ \f
  ef4---^ r r2

  R1*6
  r8 \mf d-^ r4 d-^ r
  d4-^ r r2

  r8 a-> ~ a4 g-^ f-^
  d4-^ r r2
  r8 f( d4-.) c( d-.)
  f4-^ r r2
  
  \repeat volta 2 {
    R1*2
    r8 \p d'-^ r4 d-^ r
    d4-^ r8 d-^ r2
  }

  r8 \f d-^ r4 d-^ r
  d4-^ r8 d-^ r2
}

trombone = \relative c' {
  \global
  \key f \major
  \clef bass

  r8 \f a4.-> g4-^ f-^

  d2.-> r4
  r8 af'4.-> g4-^ f-^
  c1 -> ~
  c2 r2

  R1*2
  r8 \mf c'-^ r4 c-^ r
  r8 c-^ r4 c-^ r

  R1*2
  r8 c-^ r4 c-^ r
  r8 c-^ r4 c-^ r

  r4 c---^ r c---^
  r4 d---^ r d---^
  r8 c-^ r c( \p \< c c c4-^)
  c4-^ \f r r2

  R1*12

  r8 \mf a-> ~ a4 g-^ f-^
  d4-^ r r2
  r8 f( d4-.) c( d-.)
  f4-^ r r2

  R1*2
  r8 c'-^ r4 c-^ r
  c4-^ r r2

  \repeat volta 2 {
    R1*2
    f,4-^ \mf r f-^ r
    f4-^ r8 f f4-^ f-^

    R1*6
    r8 c'-^ r4 c-^ r
    c4-^ r r2
  }

  r8 a-> ~ a4 g-^ f-^
  d4-^ r r2
  r8 f( d4-.) c( d-.)
  f4-^ r r2

  R1*3
  r8 \f a4.-> g4-^ f-^

  d2.-> r4
  r8 af'4.-> g4-^ f-^
  c1 -> ~
  c2 r2

  R1*2
  f4-^ \mf r f-^ r
  f4-^ r8 f f4-^ f-^

  R1*6
  r8 c'-^ r4 c-^ r
  c4-^ r8 c-^ r2

  R1*2
  r2 r4 c,---^ \f
  f4---^ r r2

  R1*6
  r8 \mf c'-^ r4 c-^ r
  c4-^ r r2

  r8 a-> ~ a4 g-^ f-^
  d4-^ r r2
  r8 f( d4-.) c( d-.)
  f4-^ r r2
  
  \repeat volta 2 {
    R1*2
    r8 \p c'-^ r4 c-^ r
    c4-^ r8 c-^ r2
  }

  r8\f c-^ r4 c,-^ r
  c'4-^ r8 f,-^ r2
}

% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\book {
  \paper { 
    % Descomentar para score
    % #(set-paper-size "a4" 'landscape)
    % indent = 20
    % systems-per-page = 2
  }

  \score {
    % Descomentar para midi
    % \midi { \tempo 4 = 110 }

    % NO USAR layout-set-staff-size O SE ROMPE lilyjazz!!!
    \layout { }

    <<
      \scoreMarkup
      \transpose bf c' \scoreChords
      \new StaffGroup <<
        \new Staff \with 
        { instrumentName = "Trpt."}
        { \transpose bf c' \trumpet } 

        \new Staff \with 
        { instrumentName = "T Sx." \RemoveEmptyStaves }
        { \transpose bf c' \tenorSax } 

        \new Staff \with 
        { instrumentName = "Trbn." \RemoveEmptyStaves } 
        { \trombone }
      >>
    >>
  }
}
