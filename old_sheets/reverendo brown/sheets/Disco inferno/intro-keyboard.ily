\version "2.18.2"
\language "english"

% Se debe incluir antes que lilyjazz. Si no, lilyjazz no funciona.
#(set-global-staff-size 20)

\include "main.ily"

\paper {
  indent = 10
  top-system-spacing.basic-distance = 40
  score-system-spacing.basic-distance = 40
  two-sided = ##t
  inner-margin = 20
  outer-margin = 20
}

\header {
  title = ""
  instrument = ""
  composer = ""
  arranger = ""
  poet = ""
  tagline = ""
}

% Función musical global
global = {
  % Para pintar todos los números de compás debajo del pentagrama
  % \override Score.BarNumber #'break-visibility = #end-of-line-invisible
  % \override Score.BarNumber #'direction = #DOWN
  % \override Score.BarNumber #'extra-offset = #'(1.5 . 0)
  \compressFullBarRests

  \set Timing.beamExceptions = #'()
  \set Timing.baseMoment = #(ly:make-moment 1/4)
  \set Timing.beatStructure = #'(1 1 1 1) 
}

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
  % Oculta el primer pentagrama del markup
  \override Staff.VerticalAxisGroup #'remove-first = ##t

  % Indicación de tiempo
  % \once \override Score.RehearsalMark.self-alignment-X = #LEFT
  \override Score.RehearsalMark.extra-offset = #'(0 . 3) 
  s1*2
}


% Cifrados
scoreChords = \chords {
  % c1:m f2:m g2:m7
}

% Música
trumpet = \relative c''' {
  \global
  \key c \minor

  g16-5 ef-3 cf-1 af-2
  cf-5 af-3 f-1 ef-2
  f-5 ef-3 cf-1 bf-2
  cf-5 af-3 f-1 ef-2

  \break
  f-1 af-2 cf-3 ef-5
  cf-1 ef-2 f-3 af-5
  ef-2 f-1 af-2 cf-4
  cf-1 ef-3 g8-5

  \break
  \bar "||"
  \stemUp
  g16^5 ef^3 cf^1 af^2
  \stemDown
  cf_1 af_2 f_4 ef_5
  \stemUp
  f^5 ef^3 cf^1 bf^2
  \stemDown
  cf_1 af_2 f_4 ef_5

  \break
  \stemUp
  f^1 af^2 cf^3 ef^5
  cf^1 ef^2 f^3 af^5
  \stemDown
  ef_5 f_4 af_2 cf_1
  \stemUp
  cf^1 ef^3 g8^5
}

tenorSax = \relative c'' {
  \global
}

trombone = \relative c' {
  \global
}

% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\book {
  \paper { 
    % Descomentar para score
    % #(set-paper-size "a4" 'landscape)
    % indent = 20
    % systems-per-page = 2
  }

  \score {
    % Descomentar para midi
    % \midi { \tempo 4 = 110 }

    % NO USAR layout-set-staff-size O SE ROMPE lilyjazz!!!
    \layout { }

    <<
      \scoreMarkup
      \new StaffGroup <<
        \new Staff \relative c''' {
      \global
      \key c \minor

  \part "Solo mano derecha"
      g16-5 ef-3 cf-1 af-2
      cf-5 af-3 f-1 ef-2
      f-5 ef-3 cf-1 bf-2
      cf-5 af-3 f-1 ef-2

      \break
      f-1 af-2 cf-3 ef-5
      cf-1 ef-2 f-3 af-5
      ef-2 f-1 af-2 cf-4
      cf-1 ef-3 g8-5
} 
      >>
    >>
  }

  \score {
    % Descomentar para midi
    % \midi { \tempo 4 = 110 }

    % NO USAR layout-set-staff-size O SE ROMPE lilyjazz!!!
    \layout { }

    <<
      \scoreMarkup
      \new StaffGroup <<
        \new Staff \relative c''' {
      \global
      \key c \minor

  \part "Alternando mano derecha e izquierda"
  \stemUp
  g16^5 ef^3 cf^1 af^2
  \stemDown
  cf_1 af_2 f_4 ef_5
  \stemUp
  f^5 ef^3 cf^1 bf^2
  \stemDown
  cf_1 af_2 f_4 ef_5

  \break
  \stemUp
  f^1 af^2 cf^3 ef^5
  cf^1 ef^2 f^3 af^5
  \stemDown
  ef_5 f_4 af_2 cf_1
  \stemUp
  cf^1 ef^3 g8^5
} 
      >>
    >>
  }
}
