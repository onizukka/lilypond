% Se debe incluir antes que lilyjazz. Si no, lilyjazz no funciona.
#(set-global-staff-size 20)

% includes
\include "main.ily"
\include "music-sheet-paper.ily"

% Cabecera
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\header {
  title = "Disco Inferno"
  instrument = "Trumpet, Tenor Sax & Trombone"
  composer = "The Trammps"
  arranger = ""
  poet = "gabriel@sauros.es"
  tagline = ""
  meter = \jazzTempoMarkup "Medium Funk" c4 "120"
}

\paper {
}

% Función musical global
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
global = {
  % Para pintar todos los números de compás debajo del pentagrama
  % \override Score.BarNumber #'break-visibility = #end-of-line-invisible
  % \override Score.BarNumber #'direction = #DOWN
  % \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

  \compressFullBarRests

  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest.staff-position
  \override Voice.Rest.staff-position = #0	

  \set Timing.beamExceptions = #'()
  \set Timing.baseMoment = #(ly:make-moment 1/4)
  \set Timing.beatStructure = #'(1 1 1 1) 

  \override ParenthesesItem.font-size = #0
}

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
  % Oculta el primer pentagrama del markup
  \override Staff.VerticalAxisGroup #'remove-first = ##t
  \override Score.RehearsalMark.self-alignment-X = #LEFT

  % Indicación de tiempo
  % \once \override Score.RehearsalMark.self-alignment-X = #LEFT
  \override Score.RehearsalMark.extra-offset = #'(0 . 2) 
  % \mark \jazzTempoMarkup "Easy Swing" c4 "112"

  % s1*2
  % \once \override Score.RehearsalMark.X-offset = #5
  % \mark \markup \boxed "A"

  s1*2
  \part "Verse"
  s1*8	
  s1*4
  s1*4
  \part "Chorus"
  s1*2
  s1*3

  \part "Verse"
  s1*8	
  s1*4
  s1*4
  \part "Chorus"
  s1*2
  s1*3
  \part "Verse"
  s1*8
  s1*4

  s1*4
  \part "Chorus"
  s1*2
  s1*3
  s1*4
  s1*3
  \part "Verse"
  s1*4
  s1*4
  s1*8
  \mark \markup \line { "Chorus" \underline "(Repeat 4 xs)" }
  s1*2
  s1*3
  s1*9

}

% Cifrado
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
scoreChords = \chords {
  % c1:m f2:m g2:m7
}

% Música
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
trumpet = \relative c' {

  \global
  \key c \minor

  f8-.\p f-. f-. f-. f-. f-. f-. f-. 
  f-.\< f-. f-. f-. f-. f-. f-. g--->\f \bendAfter #-5
  \bar "||"

  \break
  R1*6
  r8 \mf c,-. c-. g-. bf-. c---> r4
  r8 c8-- c4-. bf'8-. \f bf-. a-. g---> \bendAfter #-5

  \break
  R1*4

  \pageBreak
  R1*3
  r2 e8-. \mf f-. g-. a~->

  \bar "[|:"
  \repeat volta 2 {
    a4 g4-^ f-^ r8 bf~->
    bf4 f4-^ bf-^ r8 e,(~->
    \break
    e8 e)-. f( e)-. f( e4)-. g8~->

  }
  \alternative {{
    g4 r e8-. f-. g-. a-> \laissezVibrer
    \bar ":|]"
  }{
    g4 \repeatTie r4 bf8-. \f a4-> g8-> \bendAfter #-5 
  }}

  \break
  R1*6
  r8 \mf c,-. c-. g-. bf-. c---> r4
  r8 c8-- c4-. bf'8-. \f bf-. a-. g---> \bendAfter #-5

  \break
  R1*4

  \pageBreak
  R1*3
  r2 e8-. \mf f-. g-. a~->

  \bar "[|:"
  \repeat volta 2 {
    a4 g4-^ f-^ r8 bf~->
    bf4 f4-^ bf-^ r8 e,(~->
    \break
    e8 e)-. f( e)-. f( e4)-. g8~->

  }
  \alternative {{
    g4 r e8-. f-. g-. a-> \laissezVibrer
    \bar ":|]"
  }{
    g4 \repeatTie r4 bf8-. \f a4-> g8-> \bendAfter #-5 
  }}

  \break
  R1*8

  R1*4

  \pageBreak
  R1*3
  r2 e8-. \mf f-. g-. a~->
  \bar "[|:"
  \repeat volta 2 {
    a4 g4-^ f-^ r8 bf~->
    bf4 f4-^ bf-^ r8 e,(~->
    \break
    e8 e)-. f( e)-. f( e4)-. g8~->
  }
  \alternative {{
    g4 r e8-. f-. g-. a-> \laissezVibrer
    \bar ":|]"
  }{
    g4 \repeatTie r4 bf8-. \f a4-> r8
  }}

  \break
  \bar "[|:"
  \repeat volta 2 {
    r4 d,2.-> \mf \<
    a'1 \f
  }
  \alternative {{ 
    g1~
    g2. r4
    \bar ":|]"
  }{ 
    g1
    R1
  }}
  r2 r4 r8 g8-> \bendAfter #-5 \f

  \break
  R1
  r2 bf8-. \mf a-. g-. ef~->
  ef4 r r2
  R1

  \break
  R1*2
  r8 c-. c-. g-. bf-. c---> r4
  r8 c8-- c4-. bf'8-. \f bf-. a-. g---> \bendAfter #-5

  \break
  R1*7
  r2 e8-. \mf f-. g-. a~->

  \bar "[|:"
  \repeat volta 4 {
    a4 g4-^ f-^ r8 bf~->
    bf4 f4-^ bf-^ r8 e,(~->
    \break
    e8 e)-. f( e)-. f( e4)-. g8~->
  }
  \alternative {{
    g4 r e8-. f-. g-. a \laissezVibrer
    \bar ":|]"
  }{
    g4 \repeatTie r4 bf8-. \f a4-> r8
  }}

  \break
  \bar "[|:"
  \repeat volta 2 {
    r4 d,2.-> \mf \<
    a'1 \f
  }
  \alternative {{ 
    g1~
    g2. r4
    \bar ":|]"
  }{ 
    g1
  }}

  R1*4

  \bar "|."
}

% Música
tenorSax = \relative c'' {

  \global
  \key c \minor

  % Intro
  df8-.\p df-. df-. df-. df-. df-. df-. df-. 
  df-.\< df-. df-. df-. df-. df-. df-. ef--->\f \bendAfter #-5

  R1*6
  r8 \mf c-. c-. g-. bf-. c---> r4
  r8 c8-- c4-. f8-. \f f-. f-. ef---> \bendAfter #-5 

  R1*7
  r2 c8-. \mf d-. e-. f~->

  \repeat volta 2 {
    f4 e4-^ d-^ r8 f~->
    f4 c4-^ d-^ r8 c(~->
    c8 c)-. d( c)-. d( c4)-. e8~-> 
  }
  \alternative {{
    e4 r c8-. d-. e-. f-> \laissezVibrer
  } {
    e4 \repeatTie r4 f8-. \f f4-> ef8-> \bendAfter #-5 
  }}

  R1*6
  r8 \mf c-. c-. g-. bf-. c---> r4
  r8 c8-- c4-. f8-. \f f-. f-. ef---> \bendAfter #-5 

  R1*7
  r2 c8-. \mf d-. e-. f~->

  \repeat volta 2 {
    f4 e4-^ d-^ r8 f~->
    f4 c4-^ d-^ r8 c(~->
    c8 c)-. d( c)-. d( c4)-. e8~-> 
  }
  \alternative {{
    e4 r c8-. d-. e-. f-> \laissezVibrer
  } {
    e4 \repeatTie r4 f8-. \f f4-> ef8-> \bendAfter #-5 
  }}

  R1*15
  r2 c8-. \mf d-. e-. f~->

  \repeat volta 2 {
    f4 e4-^ d-^ r8 f~->
    f4 c4-^ d-^ r8 c(~->
    c8 c)-. d( c)-. d( c4)-. e8~->
  }
  \alternative {{
    e4 r c8-. d-. e-. f-> \laissezVibrer
  } {
    e4 \repeatTie r4 f8-. \f f4-> d8~-> \fp
  }}

  % Puente
  \repeat volta 2 {
    d2~-> \< d8 f~-> f4
    f1 \f
  }
  \alternative {{
    e1~
    e2. r4
  }{ 
    e1
    R1
  }}
  r2 r4 r8 ef8-> \bendAfter #-5 \f

  R1
  r2 bf'8-. \mf a-. g-. ef~->
  ef4 r r2
  R1

  R1*2
  r8 c-. c-. g-. bf-. c---> r4
  r8 c8-- c4-. f8-. \f f-. f-. ef---> \bendAfter #-5 

  R1*7
  r2 c8-. \mf d-. e-. f~->

  \repeat volta 4 {
    f4 e4-^ d-^ r8 f~->
    f4 c4-^ d-^ r8 c(~->
    c8 c)-. d( c)-. d( c4)-. e8~->
  }
  \alternative {{
    e4 r c8-. d-. e-. f-> \laissezVibrer
  }{
    e4 \repeatTie r4 f8-. \f f4-> d8~-> \fp
  }}
  % Puente
  \repeat volta 2 {
    d2~-> \< d8 f~-> f4
    f1 \f
  }
  \alternative {{
    e1~
    e2. r4
  }{ 
    e1
  }}

  R1*4
}

% Música
trombone = \relative c {

  \global
  \key c \minor
  \clef bass

  % Intro
  bf'8-.\p bf-. bf-. bf-. bf-. bf-. bf-. bf-.
  bf-.\< bf-. bf-. bf-. bf-. bf-. bf-. bf--->\f \bendAfter #-5

  R1*6
  r8 \mf c,-. c-. g-. bf-. c---> r4
  r8 c8-- c4-. d'8-. \f d-. c-. bf---> \bendAfter #-5

  R1*7
  r2 e,8-. \mf f-. g-. c~->

  \repeat volta 2 {
    c4 g4-^ a-^ r8 d~->
    d4 a4-^ bf-^ r8 g(~->
    g8 g)-. a( g)-. a( g4)-. c8~->
  }
  \alternative {{
    c4 r e,8-. f-. g-. c-> \laissezVibrer
  }{
    c4 \repeatTie r4 d8-. \f c4-> bf8-> \bendAfter #-5 
  }}

  R1*6
  r8 \mf c,-. c-. g-. bf-. c---> r4
  r8 c8-- c4-. d'8-. \f d-. c-. bf---> \bendAfter #-5

  R1*7
  r2 e,8-. \mf f-. g-. c~->

  \repeat volta 2 {
    c4 g4-^ a-^ r8 d~->
    d4 a4-^ bf-^ r8 g(~->
    g8 g)-. a( g)-. a( g4)-. c8~->
  }
  \alternative {{
    c4 r e,8-. f-. g-. c-> \laissezVibrer
  }{
    c4 \repeatTie r4 d8-. \f c4-> bf8-> \bendAfter #-5 
  }}

  R1*15
  r2 e,8-. \mf f-. g-. c~->

  \repeat volta 2 {
    c4 g4-^ a-^ r8 d~->
    d4 a4-^ bf-^ r8 g(~->
    g8 g)-. a( g)-. a( g4)-. c8~->
  }
  \alternative {{
    c4 r e,8-. f-. g-. c-> \laissezVibrer
  }{
    c4 \repeatTie r4 d8-. \f c4-> bf8~-> \fp
  }}

  % Puente
  \repeat volta 2 {
    bf1 \<
    bf1 \f
  }
  \alternative {{ 
    c1~
    c2. r4
  }{
    c1
    R1
  }}
  r2 r4 r8 bf8-> \f \bendAfter #-5 

  R1
  r8 \mf bf-. c-. bf-. r2
  R1*2

  R1*2
  r8 c,-. c-. g-. bf-. c---> r4
  r8 c8-- c4-. d'8-. \f d-. c-. bf---> \bendAfter #-5

  R1*7
  r2 e,8-. \mf f-. g-. c~->

  \repeat volta 4 {
    c4 g4-^ a-^ r8 d~->
    d4 a4-^ bf-^ r8 g(~->
    g8 g)-. a( g)-. a( g4)-. c8~->
  }
  \alternative {{
    c4 r e,8-. f-. g-. c-> \laissezVibrer
  }{
    c4 \repeatTie r4 d8-. \f c4-> bf8~-> \fp
  }}

  % Puente
  \repeat volta 2 {
    bf1 \<
    bf1 \f
  }
  \alternative {{
    c1~
    c2. r4
  }{
    c1
  }}
  R1*4
}

\layout {
  \context {
    \Score
    \override StaffGrouper.staff-staff-spacing.padding = #1
    \override StaffGrouper.staff-staff-spacing.basic-distance = #0
  }
}

% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\book {
  \paper { 
    % Descomentar para score
    % #(set-paper-size "a4" 'landscape)
    % indent = 20
    % systems-per-page = 2
  }

  \score {
    % Descomentar para midi
    % \midi { \tempo 4 = 110 }

    % ATENCIÓN
    % NO USAR layout-set-staff-size O SE ROMPE lilyjazz!!!
    \layout { }

    <<
      \scoreMarkup
      \scoreChords
      \new StaffGroup <<
        \new Staff \with 
        { instrumentName = "Trpt."}
        { \transpose bf c' \trumpet } 

        \new Staff \with 
        { instrumentName = "T Sx." \RemoveEmptyStaves }
        { \transpose bf c' \tenorSax } 

        \new Staff \with 
        { instrumentName = "Trbn." \RemoveEmptyStaves } 
        { \trombone }
      >>
    >>
  }
}



%{
  \book {
    \header { instrument = "" }
    \bookOutputName ""
    \score {
      <<
        \scoreMarkup
        \scoreChords
        \new StaffGroup <<
          \new Staff { \transpose bf c' \instrumentA }
        >>
      >>
    }
  }
  %}

