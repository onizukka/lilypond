% Se debe incluir antes que lilyjazz. Si no, lilyjazz no funciona.
#(set-global-staff-size 20)

\include "main.ily"
\include "music-sheet-paper.ily"

\paper {
  first-page-number = 2
  markup-system-spacing.basic-distance = 20
}

\header {
	title = "What'd I say"
  instrument = "Trumpet, Tenor Sax & Trombone"
  composer = "Ray Charlse"
  arranger = ""
  poet = "gabriel@sauros.es"
  tagline = ""
  meter = \jazzTempoMarkup "R&B" c4 "112"
}

% Función musical global
global = {
  % Para pintar todos los números de compás debajo del pentagrama
  % \override Score.BarNumber #'break-visibility = #end-of-line-invisible
  % \override Score.BarNumber #'direction = #DOWN
  % \override Score.BarNumber #'extra-offset = #'(1.5 . 0)
  \compressFullBarRests

  \set Timing.beamExceptions = #'()
  \set Timing.baseMoment = #(ly:make-moment 1/4)
  \set Timing.beatStructure = #'(1 1 1 1) 

  \key e \major
}

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
  % Oculta el primer pentagrama del markup
  \override Staff.VerticalAxisGroup #'remove-first = ##t

  % Indicación de tiempo
  % \once \override Score.RehearsalMark.self-alignment-X = #LEFT
  \override Score.RehearsalMark.extra-offset = #'(0 . 2) 
  % \mark 

  \mark \markup \fontsize #-0.75 \line { "Instrumental " \underline "(Repeat 6 xs)" }
  s1*12
  \mark \markup \fontsize #-0.75 \line { "Verse " \underline "(Repeat 4 xs)" }
  s1*12

  \part "Instrumental"
  s1*12
  \part "Verse"
  s1*12

  \part "Chorus"
  s1*12

  \mark \markup \line { "Vocal echoes" \underline "(Repeat 3 xs)" }
  s1*8

  \part "Chorus"
  s1*12

  \part "Chorus"
}

% Cifrados
scoreChords = \chords {
  % c1:m f2:m g2:m7
}

% Música
trumpet = \relative c'' {
  \global

  \bar "[|:"
  R1*12
  \bar ":|][|:"
  R1*12

  \break
  \bar ":|][|:"
  R1*12
  \bar ":|][|:"
  R1*12

  \break
  \bar ":|][|:"
  \repeat volta 2 {
    \repeat percent 2 { r4 gs-^ gs8( gs-.) r4 | gs4-. r8 gs8-. r2 \noBreak }
    r4 g-^ g8( g-.) r4 | g4-. r8 g8-. r2
    \break
    r4 gs?-^ gs8( gs-.) r4 | gs4-. r8 gs8-. r2
    r4 a-^ a8( a-.) r4 | g4-. r8 g8-. r2
    r4 gs?-^ gs8 gs8-> ~ gs4 | gs4-^ r4 r2
  }

  \pageBreak
  \bar ":|][|:"
  \repeat volta 2 {
    R1*8

    \bar "||"
    \break
    \repeat percent 2 { r4 gs-^ gs8( gs-.) r4 | gs4-. r8 gs8-. r2 \noBreak }
    r4 g-^ g8( g-.) r4 | g4-. r8 g8-. r2
    \break
    r4 gs?-^ gs8( gs-.) r4 | gs4-. r8 gs8-. r2
    r4 a-^ a8( a-.) r4 \noBreak | g4-. r8 g8-. r2
    r4 gs?-^ gs8 gs8-> ~ gs4 | gs4-^ r4 r2
  }

  \break
  \bar ":|][|:"
  \repeat volta 2 {
    \repeat percent 2 { r4 gs-^ gs8( gs-.) r4 | gs4-. r8 gs8-. r2 \noBreak }
    r4 g-^ g8( g-.) r4 | g4-. r8 g8-. r2
    \break
    r4 gs?-^ gs8( gs-.) r4 | gs4-. r8 gs8-. r2
    r4 a-^ a8( a-.) r4 | g4-. r8 g8-. r2
    r4 gs?-^ gs8 gs8-> ~ gs4 | gs4-^ r4 r2
  }
  \bar ":|]"
}

tenorSax = \relative c'' {
  \global

  \bar "[|:"
  R1*12
  \bar ":|][|:"
  R1*12

  \bar ":|][|:"
  R1*12
  \bar ":|][|:"
  R1*12

  \bar ":|][|:"
  \repeat volta 2 {
    \repeat percent 2 { r4 d-^ d8( d-.) r4 | d4-. r8 d8-. r2 }
    r4 cs-^ cs8( cs-.) r4 | cs4-. r8 cs8-. r2
    r4 d-^ d8( d-.) r4 | d4-. r8 d8-. r2
    r4 ds?-^ ds8( ds-.) r4 | cs4-. r8 cs8-. r2
    r4 d-^ d8 d-> ~ d4 | d4-^ r4 r2
  }

  \bar ":|][|:"
  \repeat volta 2 {
    R1*8
    \repeat percent 2 { r4 d-^ d8( d-.) r4 | d4-. r8 d8-. r2 }
    r4 cs-^ cs8( cs-.) r4 | cs4-. r8 cs8-. r2
    r4 d-^ d8( d-.) r4 | d4-. r8 d8-. r2
    r4 ds?-^ ds8( ds-.) r4 | cs4-. r8 cs8-. r2
    r4 d-^ d8 d-> ~ d4 | d4-^ r4 r2
  }

  \bar ":|][|:"
  \repeat volta 2 {
    \repeat percent 2 { r4 d-^ d8( d-.) r4 | d4-. r8 d8-. r2 }
    r4 cs-^ cs8( cs-.) r4 | cs4-. r8 cs8-. r2
    r4 d-^ d8( d-.) r4 | d4-. r8 d8-. r2
    r4 ds?-^ ds8( ds-.) r4 | cs4-. r8 cs8-. r2
    r4 d-^ d8 d-> ~ d4 | d4-^ r4 r2
  }
  \bar ":|]"
}

trombone = \relative c {
  \global
  \clef bass

  \bar "[|:"
  R1*12
  \bar ":|][|:"
  R1*12

  \bar ":|][|:"
  R1*12
  \bar ":|][|:"
  R1*12

  \bar ":|][|:"
  \repeat volta 2 {
    \repeat percent 2 { e2-> e8( e-.) r4 | e4-. r8 e8-. r2 }
    e2-> e8( e-.) r4 | e4-. r8 e8-. r2
    e2-> e8( e-.) r4 | e4-. r8 e8-. r2
    r4 fs-^ fs8( fs-.) r4 | e4-. r8 e8-. r2
    e2-> e8 e-> ~ e4 | e4-^ r4 r2
  }

  \bar ":|][|:"
  \repeat volta 2 {
    R1*8
    \repeat percent 2 { e2-> e8( e-.) r4 | e4-. r8 e8-. r2 }
    e2-> e8( e-.) r4 | e4-. r8 e8-. r2
    e2-> e8( e-.) r4 | e4-. r8 e8-. r2
    r4 fs-^ fs8( fs-.) r4 | e4-. r8 e8-. r2
    e2-> e8 e-> ~ e4 | e4-^ r4 r2
  }

  \bar ":|][|:"
  \repeat volta 2 {
    \repeat percent 2 { e2-> e8( e-.) r4 | e4-. r8 e8-. r2 }
    e2-> e8( e-.) r4 | e4-. r8 e8-. r2
    e2-> e8( e-.) r4 | e4-. r8 e8-. r2
    r4 fs-^ fs8( fs-.) r4 | e4-. r8 e8-. r2
    e2-> e8 e-> ~ e4 | e4-^ r4 r2
  }
  \bar ":|]"
}

% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\book {
  \paper { 
    % Descomentar para score
    % #(set-paper-size "a4" 'landscape)
    % indent = 20
    % systems-per-page = 2
  }

  \score {
    % Descomentar para midi
    % \midi { \tempo 4 = 110 }

    % NO USAR layout-set-staff-size O SE ROMPE lilyjazz!!!
    \layout { }

    <<
      \scoreMarkup
      \transpose bf c' \scoreChords
      \new StaffGroup <<
        \new Staff \with 
        { instrumentName = "Trpt."}
        { \transpose bf c' \trumpet } 

        \new Staff \with 
        { instrumentName = "T Sx." \RemoveEmptyStaves }
        { \transpose bf c' \tenorSax } 

        \new Staff \with 
        { instrumentName = "Trbn." \RemoveEmptyStaves } 
        { \trombone }
      >>
    >>
  }
}
