% Se debe incluir antes que lilyjazz. Si no, lilyjazz no funciona.
#(set-global-staff-size 20)

\include "main.ily"
\include "music-sheet-paper.ily"

\paper {
  first-page-number = 2
}

\header {
  title = "These arms of mine"
  instrument = "Trumpet, Tenor Sax & Trombone"
  composer = "Otis Reeding"
  arranger = "(Ben l'Oncle Soul)"
  poet = "gabriel@sauros.es"
  tagline = ""
  meter = \jazzTempoMarkup "Slow Soul" c4. "50"
}

% Función musical global
global = {
  % Para pintar todos los números de compás debajo del pentagrama
  % \override Score.BarNumber #'break-visibility = #end-of-line-invisible
  % \override Score.BarNumber #'direction = #DOWN
  % \override Score.BarNumber #'extra-offset = #'(1.5 . 0)
  \compressFullBarRests

  \set Timing.beamExceptions = #'()
  \set Timing.baseMoment = #(ly:make-moment 1/4)
  \set Timing.beatStructure = #'(1 1 1 1) 
}

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
  % Oculta el primer pentagrama del markup
  \override Staff.VerticalAxisGroup #'remove-first = ##t

  % Indicación de tiempo
  % \once \override Score.RehearsalMark.self-alignment-X = #LEFT
  \override Score.RehearsalMark.extra-offset = #'(0 . 2) 
  % \mark 

  \part "Intro"
  s1*2*12/8

  \part "Verse"
  s1*8*12/8

  s1*4*12/8

  \part "Chorus"
  s1*4*12/8
  s1*4*12/8

  \mark \markup \line { "Chorus" \underline "(Repeat ad.lib.)" }
  s1*4*12/8

  \note "Repeat 7 xs"
  s1*2*12/8

  s1*2*12/8
}

% Cifrados
scoreChords = \chords {
  % c1:m f2:m g2:m7
}

% Música
trumpet = \relative c' {
  \global
  \key bf \major
  \time 12/8

  r4. f-> \f r bf->
  r4. c-> r2.

  \bar "[|:"
  R1*8*12/8

  r4. d4-^ \mp c8( bf2.)
  r4. d4-^ c8( bf4 c8 d4.)
  r4. d4-^ bf8( c4.) r4 f,8
  g16( bf) ~ bf4 ~ bf4. ~ bf2. \>
  \bar ":|]"

  R1*4*12/8 \!

  \bar "[|:"
  r4. d4-^ \mp bf8( _"cresc. poco a poco" c4.) r4 f,8
  g16( bf) ~ bf4 ~ bf4. ~ bf2. \>
  r4. \! d4-^ bf8( c4.) r4 f,8
  g16( bf) ~ bf4 ~ bf4. ~ bf2. \>

  \bar ":|][|:"
  R1*4*12/8 \!

  \bar ":|][|:"
  r4. d4-^ \f  bf8( c4.) r4 f8
  g16( bf) ~ bf4 ~ bf4. ~ bf2.
  \bar ":|]"

  r4. d,4-^ \mp bf8( c4.) r4 f,8
  g16( bf) ~ bf4 ~ bf4. ~ bf2. \fermata
  \bar "|."
}

tenorSax = \relative c' {
  \global
  \key bf \major

  r4. f-> \f r bf->
  r4. c-> r2.

  \break
  R1*8*12/8

  \break
  r4. d4-^ \mp c8( bf2.)
  r4. d4-^ c8( bf4 c8 d4.)
  r4. d4-^ bf8( c4.) r4 f,8
  g16( bf) ~ bf4 ~ bf4. ~ bf2. \>

  \break
  R1*4*12/8 \!

  \break
  r4. d4-^ \mp bf8( _"cresc. poco a poco" c4.) r4 f,8
  g16( bf) ~ bf4 ~ bf4. ~ bf2. \>
  r4. \! d4-^ bf8( c4.) r4 f,8
  g16( bf) ~ bf4 ~ bf4. ~ bf2. \>

  \break
  R1*4*12/8 \!

  \break
  r4. d4-^ \f bf8( c4.) r4 f8
  g16( bf) ~ bf4 ~ bf4. ~ bf2.

  \break
  r4. d,4-^ \mp bf8( c4.) r4 f,8
  g16( bf) ~ bf4 ~ bf4. ~ bf2. \fermata
}

trombone = \relative c, {
  \global
  \clef bass
  \key bf \major

  r4. f-> \f r bf->
  r4. c-> r2.

  R1*8*12/8

  r4. d4-^ \mp c8( bf2.)
  r4. d4-^ c8( bf4 c8 d4.)
  r4. d4-^ bf8( c4.) r4 f,8
  g16( bf) ~ bf4 ~ bf4. ~ bf2. \>

  R1*4*12/8 \!

  r4. d4-^ \mp bf8( c4.) r4 f,8
  g16( bf) ~ bf4 ~ bf4. ~ bf2. \>
  r4. \! d4-^ bf8( c4.) r4 f,8
  g16( bf) ~ bf4 ~ bf4. ~ bf2. \>

  R1*4*12/8 \!

  r4. d4-^ \f  bf8( c4.) r4 f8
  g16( bf) ~ bf4 ~ bf4. ~ bf2.

  r4. d,4-^ \mp bf8( c4.) r4 f,8
  g16( bf) ~ bf4 ~ bf4. ~ bf2. \fermata
}

% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\book {
  \paper { 
    % Descomentar para score
    % #(set-paper-size "a4" 'landscape)
    % indent = 20
    % systems-per-page = 2
  }

  \score {
    % Descomentar para midi
    % \midi { \tempo 4 = 110 }

    % NO USAR layout-set-staff-size O SE ROMPE lilyjazz!!!
    \layout { }

    <<
      \scoreMarkup
      \transpose bf c' \scoreChords
      \new StaffGroup <<
        \new Staff \with 
        { instrumentName = "Trpt." }
        { \transpose bf c' \trumpet } 

        \new Staff \with 
        { instrumentName = "T Sx." \RemoveEmptyStaves }
        { \transpose bf c' \tenorSax } 

        \new Staff \with 
        { instrumentName = "Trbn." \RemoveEmptyStaves } 
        { \trombone }
      >>
    >>
  }
}
