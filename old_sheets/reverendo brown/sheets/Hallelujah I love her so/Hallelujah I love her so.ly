% Se debe incluir antes que lilyjazz. Si no, lilyjazz no funciona.
#(set-global-staff-size 20)

\include "main.ily"
\include "music-sheet-paper.ily"

\header {
  title = "Hallelujah I love her so"
  instrument = "Trumpet, Tenor Sax & Trombone"
  composer = "Ray Charles"
  arranger = ""
  poet = "gabriel@sauros.es"
  tagline = ""
  meter =  \jazzTempoMarkup "Medium Swing" c4 "140"
}

\paper {
  first-page-number = 2
}

% Función musical global
global = {
  % Para pintar todos los números de compás debajo del pentagrama
  % \override Score.BarNumber #'break-visibility = #end-of-line-invisible
  % \override Score.BarNumber #'direction = #DOWN
  % \override Score.BarNumber #'extra-offset = #'(1.5 . 0)
  \compressFullBarRests

  \set Timing.beamExceptions = #'()
  \set Timing.baseMoment = #(ly:make-moment 1/4)
  \set Timing.beatStructure = #'(1 1 1 1) 
}

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
  % Oculta el primer pentagrama del markup
  \override Staff.VerticalAxisGroup #'remove-first = ##t
  \override Score.RehearsalMark.extra-offset = #'(0 . 2) 

  \once \override Score.RehearsalMark.extra-offset = #'(0 . 0) 
  \part "Intro (straight 8s)"
  s4.
  s1*4

  \note "Swing"
  s1*4

  \part "Verse"
  s1*8
  s1*5

  \part "Bridge"
  s1*8

  \part "Verse"
  s1*8
  s1*4

  \mkup "(Tenor sax solo)"
  s1*12

  \part "Bridge"
  s1*8

  \part "Verse"
  s1*8

  \part "Chorus"
  s1*2
  \mkup "(Repeat 6 xs)"
  s1*2
}

nextPageRests = \markup \fill-line { 
  \null
  \small \line {
    \score {
      \new Staff \with {
        fontSize = #-2
        \override StaffSymbol #'staff-space = #(magstep -4)
      }
      \relative c'' { 
        \once \override Score.RehearsalMark.extra-offset = #'(32 . -1.5) 
        \mark \markup \scale #'(1 . -1) \rotate #0 \curvedArrow #0 #0 #0
        \compressFullBarRests r4 r8 e-> ~ e4 r4 | r4 r8 f-> ~ f4 r4 
        \bar ""
      }
      \layout {
        indent = 0
        \context {
          \Staff
          \remove "Clef_engraver"
          \remove "Time_signature_engraver"
        }
      }
    }
    \hspace #9
  }
}

nextPageRestsB = \markup \fill-line { 
  \null
  \small \line {
    \score {
      \new Staff \with {
        fontSize = #-2
        \override StaffSymbol #'staff-space = #(magstep -4)
      }
      \relative c'' { 
        \once \override Score.RehearsalMark.extra-offset = #'(24 . -1.5) 
        \mark \markup \scale #'(1 . -1) \rotate #0 \curvedArrow #0 #0 #0
        \compressFullBarRests r2 r8 g a( g) | c4-^ r4
        \bar ""
      }
      \layout {
        indent = 0
        \context {
          \Staff
          \remove "Clef_engraver"
          \remove "Time_signature_engraver"
        }
      }
    }
    \hspace #9
  }
}

% Cifrados
scoreChords = \chords {
  % c1:m f2:m g2:m7
  s4.
  s1*4
  s1*4
  s1*8
  s1*5
  s1*8
  s1*8
  s1*4

  bf2 bf/d
  ef2:7 e:dim
  bf2 bf/d
  ef2:7 e:dim

  bf1
  bf1/d
  ef1
  e1:dim

  bf2 d:7
  g2:m ef:7
  c:7 f:7
  bf
}

tenorChords = \chords {
}

% Música
trumpet = \relative c'' {
  \global
  \key bf \major

  \partial 4.
  r4.
  R1*4

  \bar "||"
  r4 f-^ \f fs8( g4.->)
  cs,4-. d-> a8( c4.->
  bf4-^) r4 r2
  \break
  r4 r8 e-^ r f4.->

  \bar "[|:"
  \repeat volta 2 {
    bf,4-^ r d,4-^ \p r
    ef4-> r8 e8-. r f g( f)
    bf4-^ r d,4-^ r
    \break
    ef4-> r8 e8-. r f g( f)

    bf4-^ r r2
    d,4-^ r r2
    ef4-^ r r2
    e4-^ r r2

    \break
    r4 r8 \mf d'-> ~ d4 r
    r4 r8 ef-> ~ ef4 r
    e,2 ef4. d8-.
  } \alternative {{
    r8 ef8-. r e-. r f g( f)
    \bar ":|]"
  }{
    \times 2/3 {r4 \f bf( c} df2) \prall
  }}

  \pageBreak
  bf4-^ \p r4 r2
  bf4-^ r8 g-. r bf-. r g(
  bf4-^) r4 r2
  \break
  bf4-^ r4 r8 gf->~ gf4

  f4-^ r r2
  ef4-^ r r8 bf'-. r c-.
  R1
  \break
  r2 r8 f, g( f)

  \bar "||"
  bf4-^ r d,4-^ \p r
  ef4-> r8 e8-. r f g( f)
  bf4-^ r d,4-^ r
  \break
  ef4-> r8 e8-. r f g( f)

  bf4-^ r r2
  d,4-^ r r2
  ef4-^ r r2
  \footnote "" #'(0 . 0) \nextPageRests e4-^ r r2

  \break
  r4 r8 \mf d'-> ~ d4 r
  r4 r8 ef-> ~ ef4 r
  e,2 ef4. d8-.
  r8 ef8-. r e-. r f g( f)

  \bar "||"
  R1
  \break
  a8 \pp bf a bf-> ~ bf4 r
  R1
  a8 bf a bf-> ~ bf4 r

  bf4-^ r4 r2
  af4-^ r4 r2
  g4-^ r4 r2
  \slurDown
  \times 2/3 { r4 \mf g( bf } df2) \prall
  \slurNeutral

  \break
  R1*3
  \times 2/3 {r4 \f bf( c} df2) \prall

  \break
  bf4-^ \p r4 r2
  bf4-^ r8 g-. r bf-. r g(
  bf4-^) r4 r2
  \break
  bf4-^ r4 r8 gf->~ gf4

  f4-^ r r2
  ef4-^ r r8 bf'-. r \footnote "" #'(0 . 0) \nextPageRestsB c-.
  R1
  \pageBreak
  r2 r8 f, g( f)

  \bar "||"
  bf4-^ r d,4-^ \p r
  ef4-> r8 e8-. r f g( f)
  bf4-^ r d,4-^ r
  \break
  ef4-> r8 e8-. r f g( f)

  bf4-^ r r2
  d,4-^ r r2
  ef4-^ r r2
  e4-^ r r2

  \break
  r4 r8 \mf d'-> ~ d4 r
  r4 r8 ef-> ~ ef4 r

  \bar "[|:"
  \repeat volta 6 {
    e,2 ef4. d8-.
  } \alternative {{
    \set Score.repeatCommands = #'((volta "1, 2, 3, 4, 5"))
    r2 f2 ->
    \bar ":|]"
  }{
    R1
  }}

  r4 f'-^ \f fs8( g4.->)
  cs,4-. d-> a8( c4.->
  bf4-^) r4 r2

  r8 e,( f bf-^) r2
  \bar "|."
}

tenorSax = \relative c'' {
  \global
  \key bf \major

  \partial 4.
  r4.
  R1*4

  r4 g'-^ \f e8( f4.->)
  d4-. ef?-> c8( ef4.->
  d4-^) r4 r2
  r4 r8 fs-^ r g4.->

  \repeat volta 2 {
    bf,4-^ r bf4-^ \p r
    bf4-> r8 bf8-. r f g( f)
    bf4-^ r bf4-^ r
    bf4-> r8 bf8-. r f g( f)

    bf4-^ r r2
    bf4-^ r r2
    bf4-^ r r2
    bf4-^ r r2

    r4 r8 \mf a'-> ~ a4 r
    r4 r8 bf-> ~ bf4 r
    g,2 a4. bf8-.
  } \alternative {{
    r8 ef,8-. r e-. r f g( f)
  }{
    \times 2/3 {r4 \f bf( c} d2)
  }}

  ef4-^ \p r4 r2
  df4-^ r8 bf-. r df-. r bf(
  d?4-^) r4 r2
  d4-^ r4 r8 d->~ d4

  df4-^ r r2
  cf4-^ r r8 cf-. r a?-.
  R1
  r2 r8 f g( f)

  bf4-^ r bf4-^ \p r
  bf4-> r8 bf8-. r f g( f)
  bf4-^ r bf4-^ r
  bf4-> r8 bf8-. r f g( f)

  bf4-^ r r2
  bf4-^ r r2
  bf4-^ r r2
  bf4-^ r r2

  r4 r8 \mf a'-> ~ a4 r
  r4 r8 bf-> ~ bf4 r
  g,2 a4. bf8-.
  r8 ef,8-. r e-. r f g( f)

  bf4-^ c-^ df8 g,4.->
  r2 r4 r8 g
  bf4-^ c-^ df8 g,4.->
  r2 r8 f'( g4-.)

  bf4-^ af-^ f8 bf,4.->
  f'4 \bendBefore bf8 e, \bendBefore ef df bf g
  ef'4-^ f8 fs g f d ef 
  e8-^ \tiny e,16[ g] bf df e g \normalsize bf2->

  d4-^ f,8 d fs d'4-> c8
  bf8-. g-. f-. e-. ef-. f-. g-. e-.
  f8-. g-. bf4-. r8 f g4-^
  \times 2/3 { bf-^ bf,( ^\markup \fontsize #2 "Play!" c } d2)

  ef4-^ \p r4 r2
  df4-^ r8 bf-. r df-. r bf(
  d?4-^) r4 r2
  d4-^ r4 r8 d->~ d4

  df4-^ r r2
  cf4-^ r r8 cf-. r a?-.
  R1
  r2 r8 f g( f)

  bf4-^ r bf4-^ \p r
  bf4-> r8 bf8-. r f g( f)
  bf4-^ r bf4-^ r
  bf4-> r8 bf8-. r f g( f)

  bf4-^ r r2
  bf4-^ r r2
  bf4-^ r r2
  bf4-^ r r2

  r4 r8 \mf a'-> ~ a4 r
  r4 r8 bf-> ~ bf4 r

  \repeat volta 6 {
    g,2 a4. bf8-.
  } \alternative {{
    r2 d2 ->
  }{
    R1
  }}

  r4 g-^ \f e8( f4.->)
  d4-. ef?-> c8( ef4.->
  d4-^) r4 r2

  r8 e( ef d-^) r2
}

trombone = \relative c' {
  \global
  \clef bass
  \key bf \major

  \partial 4.
  r4.
  R1*4

  r4 d-^ \f bf8( b4.->)
  a4-. bf?-> g8( a4.->
  f4-^) r4 r2
  r4 r8 d'-^ r ef4.->

  \repeat volta 2 {
    bf4-^ r d,4-^ \p r
    ef4-> r8 e8-. r f g( f)
    bf4-^ r d,4-^ r
    ef4-> r8 e8-. r f g( f)

    bf4-^ r r2
    d,4-^ r r2
    ef4-^ r r2
    e4-^ r r2

    r4 r8 \mf c'-> ~ c4 r
    r4 r8 df-> ~ df4 r
    c,2 f,4. bf8-.
  } \alternative {{
    r8 ef8-. r e-. r f g( f)
  }{
    \times 2/3 {r4 \f bf( c} af2)
  }}

  g4-^ \p r4 r2
  e4-^ r8 e-. r g-. r e(
  f4-^) r4 r2
  af4-^ r4 r8 af->~ af4

  g4-^ r r2
  f4-^ r r8 f-. r e-.
  R1
  r2 r8 f g( f)

  bf4-^ r d,4-^ \p r
  ef4-> r8 e8-. r f g( f)
  bf4-^ r d,4-^ r
  ef4-> r8 e8-. r f g( f)

  bf4-^ r r2
  d,4-^ r r2
  ef4-^ r r2
  e4-^ r r2

  r4 r8 \mf c'-> ~ c4 r
  r4 r8 df-> ~ df4 r
  c,2 f,4. bf8-.
  r8 ef8-. r e-. r f g( f)

  R1
  c'8 \pp df c df-> ~ df4 r
  R1
  c8 df c df-> ~ df4 r

  d4-^ r4 r2
  bf4-^ r4 r2
  bf4-^ r4 r2
  \slurDown
  \times 2/3 { r4 \mf bf( df } e2)
  \slurNeutral

  R1*3
  \times 2/3 {r4 \f bf( c} af2)

  g4-^ \p r4 r2
  e4-^ r8 e-. r g-. r e(
  f4-^) r4 r2
  af4-^ r4 r8 af->~ af4

  g4-^ r r2
  f4-^ r r8 f-. r e-.
  R1
  r2 r8 f g( f)

  bf4-^ r d,4-^ \p r
  ef4-> r8 e8-. r f g( f)
  bf4-^ r d,4-^ r
  ef4-> r8 e8-. r f g( f)

  bf4-^ r r2
  d,4-^ r r2
  ef4-^ r r2
  e4-^ r r2

  r4 r8 \mf c'-> ~ c4 r
  r4 r8 df-> ~ df4 r

  \repeat volta 6 {
    c,2 f,4. bf8-.
  } \alternative {{
    r2 g2 ->
  }{
    R1
  }}

  r4 d''-^ \f bf8( b4.->)
  a4-. bf?-> g8( a4.->
  f4-^) r4 r2

  r8 e( f bf,-^) r2
}

% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\book {
  \paper { 
    % Descomentar para score
    % #(set-paper-size "a4" 'landscape)
    % indent = 20
    % systems-per-page = 4
  }

  \score {
    % Descomentar para midi
    % \midi { \tempo 4 = 110 }

    % NO USAR layout-set-staff-size O SE ROMPE lilyjazz!!!
    \layout { }

    <<
      \scoreMarkup
      \transpose bf c' \scoreChords
      \new StaffGroup <<
        \new Staff \with 
        { instrumentName = "Trpt." }
        { \transpose bf c' \trumpet } 

        \transpose bf c' \tenorChords
        \new Staff \with 
        { instrumentName = "T Sx." }
        { \transpose bf c' \tenorSax } 

        \new Staff \with 
        { instrumentName = "Trbn." } 
        { \trombone }
      >>
    >>
  }
}
