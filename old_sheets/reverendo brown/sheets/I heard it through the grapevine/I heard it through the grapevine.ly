\version "2.18.2"
\language "english"

% Se debe incluir antes que lilyjazz. Si no, lilyjazz no funciona.
#(set-global-staff-size 19)

\include "main.ily"

\paper {
	indent = 10
}

\header {
	title = "I heard it through the grapevine"
	instrument = "Trumpet, Tenor Sax & Trombone"
	composer = "Marvin Gaye"
	arranger = "Transc: gabriel@sauros.es"
	poet = ""
	tagline = "Edition: gabriel@sauros.es"
}

% Función musical global
global = {
	% Para pintar todos los números de compás debajo del pentagrama
	% \override Score.BarNumber #'break-visibility = #end-of-line-invisible
	% \override Score.BarNumber #'direction = #DOWN
	% \override Score.BarNumber #'extra-offset = #'(1.5 . 0)
	\compressFullBarRests

	\set Timing.beamExceptions = #'()
	\set Timing.baseMoment = #(ly:make-moment 1/4)
	\set Timing.beatStructure = #'(1 1 1 1) 
  
  \key c \minor
}

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
	% Oculta el primer pentagrama del markup
	\override Staff.VerticalAxisGroup #'remove-first = ##t

	% Indicación de tiempo
	% \once \override Score.RehearsalMark.self-alignment-X = #LEFT
	% \once \override Score.RehearsalMark.extra-offset = #'(0 . 3) 
	% \mark \jazzTempoMarkup "Easy Swing" c4 "112"

  s1*8
  \mkup "(Repeat 4 xs)"
  s1*2
  \mark \segno
  s1*12
  \eolMark \markup \underline {
    \line {
      \fontsize #3 "(to "
      \fontsize #-2 \raise #1.5 { \musicglyph #"scripts.varcoda" }
      \fontsize #3 " on 3rd segno)"
    } 
  }
  s1*8
  \eolMark \markup \underline {
    \line {
      \fontsize #3 "(D.S. "
      \hspace #-1
      \fontsize #-2 \raise #1.5 { \musicglyph #"scripts.varsegno" }
      \fontsize #3 " 3 xs al Coda "
      \hspace #-1
      \fontsize #-2 \raise #1.5 { \musicglyph #"scripts.varcoda" }
      \hspace #-1
      \fontsize #3 ")"

    } 
  }
}

% Cifrados
scoreChords = \chords {
	% c1:m f2:m g2:m7
}

% Música
trumpet = \relative c'' {
	\global
	
	R1*7
	
	r2 r4. c,8\p  ~
	
  \bar "[|:"
	\repeat volta 4 {
		c2\< ef4\!-. c4->\> ~
		c2 ef8\!->( d8-.) r8 c8 ~
	}
  \bar ":|]"
	
  <c \parenthesize c'>2 r2
	R1*7
	
	r4 c'8( c8) c8( c8) r4
	c8( c8) r8 c8-^ r2
	r4 c8( c8) c8( c8) r4
	c8( c8) r8 c8-^ r4 r8 c,8 ~
	
  \break
  \bar "[|:"
	\repeat volta 2 {
		c2\< ef4\!-. c4->\> ~
		c2 r4\! r8 f8 ~
		f2\< g4\!-. f4->\> ~
		f2 r4\! r8 c'8 ~
	}
  \bar ":|]"

  \pageBreak
	c2 ef4\!-. c4-> ~
	c2 ef8\!->( d8-.) r8 c8 ~
	c2 ef4\!-. c4-> ~
	c2 ef8\!->( d8-.) r8 c8 \laissezVibrer
	
	\break
  \bar "[|:"
	\repeat volta 4 {
		c,2\< ef4\!-. c4->\> ~
		c2 r4\! r8 f8 ~
		f2\< g4\!-. f4->\> ~
		f2 r4\! r8 c8 ~
	}
  \break
  \bar ":|][|:" 
	\repeat volta 8 {
		c2\< ef4\!-. c4->\> ~
	}
	\alternative {
		{
			c2 ef8\!->( d8-.) r8 c8 \laissezVibrer
      \bar ":|]"
		}
		{	
			\cadenzaOn \hideNotes c32 ~ \cadenzaOff \unHideNotes
			c2 ef8\!->( d8-.) r4\fermata
		}
	}
	
	c1\p\fermata
  \bar "|."
}

tenorSax = \relative c'' {
	\global

	R1*7
	r2 r4. g8\p  ~
	
	\repeat volta 4 {
		g2\< bf4\!-. g4->\> ~
		g2 bf8\!->( a8-.) r8 g8 ~
	}
	
  <g \parenthesize g'>2 r2
	R1*7
	
  \break
	r4 a'8( a8) a8( a8) r4
	g8( g8) r8 a8-^ r2
	r4 g8( g8) g8( g8) r4
	a8( a8) r8 g8-^ r4 r8 g,8 ~
	
	\repeat volta 2 {
		g2\< bf4\!-. g4->\> ~
		g2 r4\! r8 a8 ~
		a2\< bf4\!-. a4->\> ~
		a2 r4\! r8 g'8 ~
	}
	
	g2 bf4\!-. g4-> ~
	g2 bf8\!->( a8-.) r8 g8 ~
	g2 bf4\!-. g4-> ~
	g2 bf8\!->( a8-.) r8 g8 \laissezVibrer
	
	\break
	
	\repeat volta 4 {
		g,2\< bf4\!-. g4->\> ~
		g2 r4\! r8 a8 ~
		a2\< bf4\!-. a4->\> ~
		a2 r4\! r8 g8 ~
	}
	
	\repeat volta 8 {
		g2\< bf4\!-. g4->\> ~
	}
	\alternative {
		{
			g2 bf8\!->( a8-.) r8 g8 \laissezVibrer
		}
		{	
			\cadenzaOn \hideNotes g32 ~ \cadenzaOff \unHideNotes
			g2 bf8\!->( a8-.) r4 \fermata
		}
	}
	
	g1\p\fermata
}

trombone = \relative c' {
	\global
  \clef bass

	R1*7
	
	r2 r4. c,8\p  ~
	
	\repeat volta 4 {
		c2\< ef4\!-. c4->\> ~
		c2 ef8\!->( d8-.) r8 c8 ~
	}
	
  <c \parenthesize c'>2 r2
	R1*7
	
	r4 e'8( e8) e8( e8) r4
	ef?8( ef8) r8 ef8-^ r2
	r4 e8( e8) e8( e8) r4
	ef?8( ef8) r8 ef8-^ r4 r8 c,8 ~
	
	\repeat volta 2 {
		c2\< ef4\!-. c4->\> ~
		c2 r4\! r8 f8 ~
		f2\< g4\!-. f4->\> ~
		f2 r4\! r8 c'8 ~
	}
	
	c2 ef4\!-. c4-> ~
	c2 ef8\!->( d8-.) r8 c8 ~
	c2 ef4\!-. c4-> ~
	c2 ef8\!->( d8-.) r8 c8 \laissezVibrer
	
	\break
	
	\repeat volta 4 {
		c,2\< ef4\!-. c4->\> ~
		c2 r4\! r8 f8 ~
		f2\< g4\!-. f4->\> ~
		f2 r4\! r8 c8 ~
	}
	
	\repeat volta 8 {
		c2\< ef4\!-. c4->\> ~
	}
	\alternative {
		{
			c2 ef8\!->( d8-.) r8 c8 \laissezVibrer
		}
		{	
			\cadenzaOn \hideNotes c32 ~ \cadenzaOff \unHideNotes
			c2 ef8\!->( d8-.) r4 \fermata
		}
	}
	
	c1\p\fermata
}

% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\book {
	\paper { 
		% Descomentar para score
		% #(set-paper-size "a4" 'landscape)
		% indent = 20
		% systems-per-page = 2
	}
	
	\score {
		% Descomentar para midi
		% \midi { \tempo 4 = 110 }
		
		% NO USAR layout-set-staff-size O SE ROMPE lilyjazz!!!
		\layout { }
		
		<<
			\scoreMarkup
			\transpose bf c' \scoreChords
			\new StaffGroup <<
				\new Staff \with 
				{ instrumentName = "Trpt."}
				{ \transpose bf c' \trumpet } 

				\new Staff \with 
				{ instrumentName = "T Sx." \RemoveEmptyStaves }
				{ \transpose bf c' \tenorSax } 

				\new Staff \with 
				{ instrumentName = "Trbn." \RemoveEmptyStaves } 
				{ \trombone }
			>>
		>>
	}
}
