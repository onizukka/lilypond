% Se debe incluir antes que lilyjazz. Si no, lilyjazz no funciona.
#(set-global-staff-size 20)

\include "main.ily"
\include "music-sheet-paper.ily"

\paper {
  first-page-number = 2
}

\header {
  title = "I feel good"
  instrument = "Trumpet, Tenor Sax & Trombone"
  composer = "James Brown"
  arranger = ""
  poet = "gabriel@sauros.es"
  tagline = ""
  meter = \jazzTempoMarkup "R&B" c4 "145"
}

% Función musical global
global = {
  % Para pintar todos los números de compás debajo del pentagrama
  % \override Score.BarNumber #'break-visibility = #end-of-line-invisible
  % \override Score.BarNumber #'direction = #DOWN
  % \override Score.BarNumber #'extra-offset = #'(1.5 . 0)
  \compressFullBarRests

  \set Timing.beamExceptions = #'()
  \set Timing.baseMoment = #(ly:make-moment 1/4)
  \set Timing.beatStructure = #'(1 1 1 1) 
}

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
  % Oculta el primer pentagrama del markup
  \override Staff.VerticalAxisGroup #'remove-first = ##t

  % Indicación de tiempo
  % \once \override Score.RehearsalMark.self-alignment-X = #LEFT
  \override Score.RehearsalMark.extra-offset = #'(0 . 2) 

  s1*2

  \part "Chorus"
  s1*12
  \part "Verse"
  s1*12

  \part "Chorus"
  s1*12

  \once \override Score.RehearsalMark.extra-offset = #'(8 . -2) 
  \note "Repeat 4 xs: 1st Harp, 2nd Trumpet, 3rd Tenor & 4th Bone"
  s1*14

  \part "Verse"
  s1*12
  \part "Chorus"
  s1*8
  \note "Repeat 3 xs"
}

nextPageRests = \markup \fill-line { 
  \null
  \small \line {
    \score {
      \new Staff \with {
        fontSize = #-2
        \override StaffSymbol #'staff-space = #(magstep -4)
      }
      \relative c'' { 
        \once \override Score.RehearsalMark.extra-offset = #'(8 . -1.5) 
        \mark \markup \scale #'(1 . -1) \rotate #0 \curvedArrow #0 #0 #0
        \compressFullBarRests R1*12
        \bar ""
      }
      \layout {
        indent = 0
        \context {
          \Staff
          \remove "Clef_engraver"
          \remove "Time_signature_engraver"
        }
      }
    }
    \hspace #9
  }
}

% Cifrados
scoreChords = \chords {
  % c1:m f2:m g2:m7
  s1*38

  d1:7
  s1
  s1
  s1

  g1:7
  s1
  d1:7
  s1

  a1:7
  g1:7
  d1:7
  s1
}

% Música
trumpet = \relative c'' {
  \global
  \key d \major
  r8 \f d,-. r fs8-. r8 a-. r c-.
  r8 e8-. r4 r2

  \break
  \bar "[|:-||"
  \repeat volta 2 {
    r4 d,8( \mf cs d cs d cs 
    d4-.) r4 r2
    r4 d8( cs d cs d cs 
    d4-.) r4 r2

    r4 f8( e f e f e 
    f4-.) r4 r2
    r4 d8( cs d cs d cs 
    d4-.) r4 r2

    r4 e-. e-> r4
    r4 d-. d-> r4
    r8 \f d-. r fs8-. r8 a-. r c-.
    r8 e8-. r4 r2
  }
  \bar ":|]"

  \pageBreak
  \repeat percent 2 {
    d,4-> \f r8 d'-. r8 c-. r8 a
    c8 a r f~\bendBefore f2
  }

  \break
  f2-> \mf r
  f2-> r
  R1
  r4 g8( \f f g f d4-.)

  f2-> \mf r
  f2-> r
  R1*2

  \bar "||"
  \repeat percent 2 {
    r4 d8( \mf cs d cs d cs 
    d4-.) r4 r2
  }

  r4 f8( e f e f e 
  f4-.) r4 r2
  r4 d8( cs d cs d cs 
  d4-.) r4 r2	

  r4 e-. e-> r4
  r4 d-. d-> r4
  r8 \f d-. r fs8-. r8 a-. r c-.
  r8 \footnote "" #'(0 . 0) \nextPageRests e8-. r4 r2

  \pageBreak
  \bar "[|:-||"
  \repeat volta 4 {
    \improOn
    \repeat unfold 4 { r2 r2 }
    \break
    \repeat unfold 4 { r2 r2 }
    \break
    \repeat unfold 2 { r2 r2 }
  } \alternative {{
    \set Score.repeatCommands = #'((volta "1, 2, 3"))
    r2 r2
    r2 r2
    \bar ":|]"
  }{
    \break
    \improOff
    r8 \f d,-. r fs8-. r8 a-. r c-.
  }}
  r8 e8-. r4 r2

  \break
  \bar "||"
  \repeat percent 2 {
    d,4-> \f r8 d'-. r8 c-. r8 a
    c8 a r f~\bendBefore f2
  }

  \break
  f2-> \mf r
  f2-> r
  R1
  r4 g8( \f f g f d4-.)

  f2-> \mf r
  f2-> r
  R1*2

  \break
  \bar "||"
  r4 d8( \mf cs d cs d cs 
  d4-.) r4 r2
  r4 d8( cs d cs d cs 
  d4-.) r4 r2

  \break
  r4 f8( e f e f e 
  f4-.) r4 r2
  r4 d8( cs d cs d cs 
  d4-.) r4 r2	

  \break
  \bar "[|:"
  \repeat volta 3 {
    r4 e-. \mf e-> r4
    r4 d-. d-> r4
    r8 \f d-. r fs8-. r8 a-. r c-.

  } \alternative {{
    \set Score.repeatCommands = #'((volta "1, 2"))
    r8 e8-. r4 r2 
    \bar ":|]" 
  }{
    \break
    r8\rit e8-. r d-. r c-. r a-.
  }}

  r8 g-. r f~-> f2\fermata\!
  e'1\fermata

  \bar "|."
}

tenorSax = \relative c'' {
  \global
  \key d \major
  r8 \f d,-. r fs8-. r8 a-. r c-.
  r8 e8-. r4 r2

  \repeat volta 2 {
    r4 a,8( \mf gs a gs a gs 
    a4-.) r4 r2
    r4 a8( gs a gs a gs 
    a4-.) r4 r2

    r4 d8( cs d cs d cs 
    d4-.) r4 r2
    r4 a8( gs a gs a gs 
    a4-.) r4 r2

    r4 cs-. cs-> r4
    r4 b-. b-> r4
    r8 \f d,-. r fs8-. r8 a-. r c-.
    r8 e8-. r4 r2
  }

  \repeat percent 2 {
    d,4-> \f r8 d'-. r8 c-. r8 a
    c8 a r f~\bendBefore f2
  }

  d'2-> \mf r
  d2-> r
  R1
  r4 g8( \f f g f d4-.)

  d2-> \mf r
  d2-> r
  R1*2

  \repeat percent 2 {
    r4 a8( \mf gs a gs a gs 
    a4-.) r4 r2
  }

  r4 d8( cs d cs d cs 
  d4-.) r4 r2
  r4 a8( gs a gs a gs 
  a4-.) r4 r2

  r4 cs-. cs-> r4
  r4 b-. b-> r4
  r8 \f d,-. r fs8-. r8 a-. r c-.
  r8 e8-. r4 r2

  \repeat volta 4 {
    \improOn
    \repeat unfold 10 { r2 r2 }
  } \alternative {{
    r2 r2
    r2 r2
  }{
    \improOff
    r8 d,-. r fs8-. r8 a-. r c-.
  }}
  r8 e8-. r4 r2

  \repeat percent 2 {
    d,4-> \f r8 d'-. r8 c-. r8 a
    c8 a r f~\bendBefore f2
  }

  d'2-> \mf r
  d2-> r
  R1
  r4 g8( \f f g f d4-.)

  d2-> \mf r
  d2-> r
  R1*2

  r4 a8( \mf gs a gs a gs 
  a4-.) r4 r2
  r4 a8( gs a gs a gs 
  a4-.) r4 r2

  r4 d8( cs d cs d cs 
  d4-.) r4 r2
  r4 a8( gs a gs a gs 
  a4-.) r4 r2

  \repeat volta 3 {
    r4 cs-. \mf cs-> r4
    r4 b-. b-> r4
    r8 d,-. \f r fs8-. r8 a-. r c-.

  } \alternative {{
    r8 e8-. r4 r2 
  }{
    r8\rit e8-. r d-. r c-. r a-.
  }}

  r8 g-. r f~-> f2\fermata\!
  c'1\fermata
}

trombone = \relative c' {
  \global
  \clef bass
  \key d \major

  r8 \f d,-. r fs8-. r8 a-. r c-.
  r8 e8-. r4 r2

  \repeat volta 2 {
    r4 fs,8( \mf es fs es fs es
    fs4-.) r4 r2
    r4 fs8( es fs es fs es 
    fs4-.) r4 r2

    \break
    r4 b8( as b as b as 
    a?4-.) r r2 
    r4 fs8( es fs es fs es 
    fs4-.) r4 r2

    \break
    r4 g-. g-> r4
    r4 f-. f-> r4
    r8 \f d-. r fs8-. r8 a-. r c-.
    r8 e8-. r4 r2
  }

  \repeat percent 2 {
    d,4-> \f r8 d'-. r8 c-. r8 a
    c8 a r f~\bendBefore f2
  }

  g2-> \mf r
  g2-> r
  R1
  r4 g8( \f f g f d4-.)

  g2-> \mf r
  g2-> r
  R1*2

  \break
  \repeat percent 2 {
    r4 fs8( \mf es fs es fs es
    fs4-.) r4 r2
  }

  r4 b8( as b as b as 
  a?4-.) r4 r2
  r4 fs8( es fs es fs es 
  fs4-.) r4 r2

  r4 g-. g-> r4
  r4 f-. f-> r4
  r8 \f d-. r fs8-. r8 a-. r c-.
  r8 e8-. r4 r2

  \pageBreak
  \repeat volta 4 {
    \improOn
    \repeat unfold 10 { r2 r2 }
  } \alternative {{
    r2 r2
    r2 r2
  }{
    \improOff
    r8 \f d,-. r fs8-. r8 a-. r c-.
  }}
  r8 e8-. r4 r2

  \repeat percent 2 {
    d,4-> \f r8 d'-. r8 c-. r8 a
    c8 a r f~\bendBefore f2
  }

  g2-> \mf r
  g2-> r
  R1
  r4 g8( \f f g f d4-.)

  g2-> \mf r
  g2-> r
  R1*2

  \break
  r4 fs8( \mf es fs es fs es
  fs4-.) r4 r2
  r4 fs8( es fs es fs es 
  fs4-.) r4 r2

  r4 b8( as b as b as 
  a?4-.) r4 r2
  r4 fs8( es fs es fs es 
  fs4-.) r4 r2

  \repeat volta 3 {
    r4 g-. \mf g-> r4
    r4 f-. f-> r4
    r8 \f d-. r fs8-. r8 a-. r c-.

  } \alternative {{
    r8 e8-. r4 r2 
  }{
    r8\rit e8-. r d-. r c-. r a-.
  }}

  r8 g-. r f~-> f2\fermata\!
  fs'?1\fermata
}

% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\book {
  \paper { 
    % Descomentar para score
    % #(set-paper-size "a4" 'landscape)
    % indent = 20
    % systems-per-page = 2
  }

  \score {
    % Descomentar para midi
    % \midi { \tempo 4 = 110 }

    % NO USAR layout-set-staff-size O SE ROMPE lilyjazz!!!
    \layout { }

    <<
      \scoreMarkup
      \transpose bf c' \scoreChords
      \new StaffGroup <<
        \new Staff \with 
        { instrumentName = "Trpt." \RemoveEmptyStaves }
        { \transpose bf c' \trumpet }

        \new Staff \with 
        { instrumentName = "T Sx." }
        { \transpose bf c' \tenorSax } 

        \scoreChords
        \new Staff \with 
        { instrumentName = "Trbn." \RemoveEmptyStaves } 
        { \trombone }
      >>
    >>
  }
}
