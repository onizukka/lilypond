% Se debe incluir antes que lilyjazz. Si no, lilyjazz no funciona.
#(set-global-staff-size 19)

\include "main.ily"
\include "music-sheet-paper.ily"

\paper {
  first-page-number = 2
}

\header {
  title = "In the midnight hour"
  instrument = "Trumpet, Tenor Sax & Trombone"
  composer = "Wilson Pickett"
  arranger = ""
  poet = "gabriel@sauros.es"
  tagline = ""
  meter = \jazzTempoMarkup "Fast R&B" c4 "140"
}

% Función musical global
global = {
  % Para pintar todos los números de compás debajo del pentagrama
  % \override Score.BarNumber #'break-visibility = #end-of-line-invisible
  % \override Score.BarNumber #'direction = #DOWN
  % \override Score.BarNumber #'extra-offset = #'(1.5 . 0)
  \compressFullBarRests

  \set Timing.beamExceptions = #'()
  \set Timing.baseMoment = #(ly:make-moment 1/4)
  \set Timing.beatStructure = #'(1 1 1 1) 
}

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
  % Oculta el primer pentagrama del markup
  \override Staff.VerticalAxisGroup #'remove-first = ##t

  % Indicación de tiempo
  % \once \override Score.RehearsalMark.self-alignment-X = #LEFT
  \override Score.RehearsalMark.extra-offset = #'(0 . 2)
  % \mark 

  \part "Intro"
  s1*4

  s1*4

  \part "Verse"
  s1*15
  s1*2

  \part "Verse"
  s1*15
  s1*2

  \part "Chorus"
  s1*7

  \note "Break"
  s1
  \note "Trumpet solo"
  s1*13

  s1*4

  \mark \markup { "Vocals " \underline "(Repeat ad.lib.)" }
  s1*4

  \part "Chorus"
  s1*8

  \part "Verse"
  s1*8
  s1*8

  \part "Chorus"
  s1*6
}

% Cifrados
scoreChords = \chords {
  % c1:m f2:m g2:m7

  s1*49
  b1:7

  e1 s s s
  e1 s s s
  b1:7 a:7 a:7 s
  e1 
}

% Música
trumpet = \transpose g e \relative c'' {
  \global
  \key g \major

  f2.~ \f f8 d~ -> 
  d2. r4 
  c2.~ c8 bf'~ -> \fp 
  bf1 \< 

  R1*4 \!

  \break
  \bar "||"
  R1*8

  d,2. \mf r4 
  c2. r4 
  r8 c8~ -> \fp c2.~ \< 
  c1

  R1 \! 
  r2 c8 b g g~ -> 
  g1 
  R1*2

  \break
  \bar "||"
  d'1-> \mf 
  r2 c8 b g g~ -> 
  g1 
  R 

  d'-> 
  r2 c8 b g g~ -> 
  g1 
  R

  \break
  d'2. r4 
  c2. r4 
  r8 c8~ -> \fp c2.~ \< 
  c1

  R1 \! 
  r2 c8 b g g~ -> 
  g1 
  R1*2

  \break
  \bar "||"
  d'2-> \f c8-. c4-- b8~ -> 
  b2 c8-. c4-- d8~ -> 
  d4 c8 a c4 a8 g 
  a2 c8-. c4-- d8~ ->

  \break
  d2 c8-. c4-- b8~ -> 
  b2 \times 2/3 { c4-- f,-- f-- } 
  g1->
  \improOn r2 r2 

  \bar "||"
  \break
  \repeat unfold 8 { r2 }
  \break
  \repeat unfold 8 { r2 }
  \break
  \repeat unfold 8 { r2 }
  \break
  \repeat unfold 2 { r2 }
  \improOff
  r2 c8 b g g~ -> 
  g1 
  R1*2

  \break
  \bar "[|:"
  R1*4
  \bar ":|]"

  \break
  d'2-> \f c8-. c4-- b8~ -> 
  b2 c8-. c4-- d8~ -> 
  d4 c8 a c4 a8 g 
  a2 c8-. c4-- d8~ ->

  \break
  d2 c8-. c4-- b8~ -> 
  b2 \times 2/3 { c4-- f,-- f-- }
  g1 -> 
  R1

  \bar "||"
  R1*8
  R1*8

  \break
  \bar "||"
  d'2-> \f c8-. c4-- b8~ -> 
  b2 c8-. c4-- d8~ -> 
  d4 c8 a c4 a8 g 
  a2 c8-. c4-- d8~ ->
  \break
  d2 c8-. c4-- b8~ -> 
  b2 \times 2/3 { c4-- f,-- f-- }

  \bar "||"
  g1~ -> 
  g2 \times 2/3 { c4-- f,-- f-- } 
  \break
  g1~ -> 
  g2 \times 2/3 { c4-- f,-- f-- } 
  g1~ -> 
  g2 \times 2/3 { c4-- f,-- f-- } 

  g2. \fermata r4
  g'1-> \fermata

  \bar "|."
}

tenorSax = \transpose g e \relative c'' {
  \global
  \key g \major

  a'2.~ \f a8 fs~ -> 
  fs2. r4 
  e2.~ e8 d~ -> \fp
  d1 \< 

  R1*4 \!

  R1*8

  fs2. \mf r4 
  e2. r4 
  r8 e8~ -> \fp e2.~ \< 
  e1

  R1 \! 
  r2 f8 e d d~ -> 
  d1 
  R1*2

  g1-> \mf 
  r2 f8 e d d~ -> 
  d1 
  R 

  g 
  r2 f8 e d d~ -> 
  d1 
  R

  fs?2. r4 
  e2. r4 
  r8 e8~ -> \fp e2.~ \< 
  e1

  R1 \! 
  r2 f8 e d d~ -> 
  d1 
  R1*2

  d2-> \f c8-. c4-- b8~ -> 
  b2 c8-. c4-- d8~ -> 
  d4 c8 a c4 a8 g 
  a2 c8-. c4-- d8~ ->

  d2 c8-. c4-- b8~ -> 
  b2 \times 2/3 { c4-- f,-- f-- } 
  g1->
  R1

  R1*13
  r2 f'8 e d d~ -> 
  d1 
  R1*2

  R1*4

  d2-> \f c8-. c4-- b8~ -> 
  b2 c8-. c4-- d8~ -> 
  d4 c8 a c4 a8 g 
  a2 c8-. c4-- d8~ ->

  d2 c8-. c4-- b8~ -> 
  b2 \times 2/3 { c4-- f,-- f-- }
  g1 -> 
  R1

  R1*8
  R1*8

  d'2-> \f c8-. c4-- b8~ -> 
  b2 c8-. c4-- d8~ -> 
  d4 c8 a c4 a8 g 
  a2 c8-. c4-- d8~ ->
  d2 c8-. c4-- b8~ -> 
  b2 \times 2/3 { c4-- f,-- f-- }

  g1~ -> 
  g2 \times 2/3 { c4-- f,-- f-- } 
  g1~ -> 
  g2 \times 2/3 { c4-- f,-- f-- } 
  g1~ -> 
  g2 \times 2/3 { c4-- f,-- f-- } 

  g2. \fermata r4
  d''1-> \fermata
}

trombone = \transpose g e \relative c' {
  \global
  \key g \major
  \clef bass

  f2.~ \f f8 d~ -> 
  d2. r4 
  c2.~ c8 bf~ -> \fp 
  bf1 \< 

  R1*4 \!

  R1*8

  d2. \mf r4
  c2. r4
  r8 b~ -> \fp b2.~ \<
  b1

  R1 \! 
  r2 c8 b g g~ -> 
  g1
  R1*2

  d'1-> \mf 
  r2 c8 b g g~ -> 
  g1
  R1

  d'
  r2 c8 b g g~ -> 
  g1
  R

  d'2. r4 
  c2. r4 
  r8 bf~ -> \fp bf2.~ \<
  bf1

  R1 \! 
  r2 c8 b g g~ -> 
  g1 
  R1*2

  d'2-> \f c8-. c4-- b8~ -> 
  b2 c8-. c4-- d8~ -> 
  d4 c8 a c4 a8 g 
  a2 c8-. c4-- d8~ ->

  d2 c8-. c4-- b8~ -> 
  b2 \times 2/3 { c4-- f,-- f-- } 
  g1->
  R1

  R1*13
  r2 c8 b g g~ -> 
  g1 
  R1*2

  R1*4

  d'2-> \f c8-. c4-- b8~ -> 
  b2 c8-. c4-- d8~ -> 
  d4 c8 a c4 a8 g 
  a2 c8-. c4-- d8~ ->

  d2 c8-. c4-- b8~ -> 
  b2 \times 2/3 { c4-- f,-- f-- }
  g1 -> 
  R1

  R1*8
  R1*8

  d'2-> \f c8-. c4-- b8~ -> 
  b2 c8-. c4-- d8~ -> 
  d4 c8 a c4 a8 g 
  a2 c8-. c4-- d8~ ->
  d2 c8-. c4-- b8~ -> 
  b2 \times 2/3 { c4-- f,-- f-- }

  g1~ -> 
  g2 \times 2/3 { c4-- f,-- f-- } 
  g1~ -> 
  g2 \times 2/3 { c4-- f,-- f-- } 
  g1~ -> 
  g2 \times 2/3 { c4-- f,-- f-- } 

  g2. \fermata r4

  g'1-> \fermata
}

% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
%~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\book {
  \paper { 
    % Descomentar para score
    % #(set-paper-size "a4" 'landscape)
    % indent = 20
    % systems-per-page = 2
  }

  \score {
    % Descomentar para midi
    % \midi { \tempo 4 = 110 }

    % NO USAR layout-set-staff-size O SE ROMPE lilyjazz!!!
    \layout { }

    <<
      \scoreMarkup
      \transpose bf c' \scoreChords
      \new StaffGroup <<
        \new Staff \with 
        { instrumentName = "Trpt."}
        { \transpose bf c' \trumpet } 

        \new Staff \with 
        { instrumentName = "T Sx." \RemoveEmptyStaves }
        { \transpose bf c' \tenorSax } 

        \new Staff \with 
        { instrumentName = "Trbn." \RemoveEmptyStaves } 
        { \trombone }
      >>
    >>
  }
}
