% Se debe incluir antes que lilyjazz. Si no, lilyjazz no funciona.
#(set-global-staff-size 20)

\include "main.ily"
\include "music-sheet-paper.ily"

\header {
  title = "Soul man"
  instrument = "Trumpet, Tenor Sax & Trombone"
  composer = "Sam & Dave"
  arranger = ""
  poet = "gabriel@sauros.es"
  tagline = ""
  meter = \jazzTempoMarkup "R&B" c4 "105"
}

\paper {
  first-page-number = 2
}



% Función musical global
global = {
  % Para pintar todos los números de compás debajo del pentagrama
  % \override Score.BarNumber #'break-visibility = #end-of-line-invisible
  % \override Score.BarNumber #'direction = #DOWN
  % \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

  \compressFullBarRests

  % Arregla el problema de tener el scoreMarkup añadido
  \revert MultiMeasureRest.staff-position
  \override Voice.Rest.staff-position = #0	

  \set Timing.beamExceptions = #'()
  \set Timing.baseMoment = #(ly:make-moment 1/4)
  \set Timing.beatStructure = #'(1 1 1 1) 

  \override ParenthesesItem.font-size = #0

  \override BendAfter.springs-and-rods = #ly:spanner::set-spacing-rods
  \override BendAfter.minimum-length = #3
}

% Elementos comunes de texto (marcas de ensayo, tempo, saltos de línea, etc)
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
  % Oculta el primer pentagrama del markup
  \override Staff.VerticalAxisGroup #'remove-first = ##t
  \override Score.RehearsalMark.self-alignment-X = #LEFT

  % Indicación de tiempo
  % \once \override Score.RehearsalMark.self-alignment-X = #LEFT
  \override Score.RehearsalMark.extra-offset = #'(0 . 2) 
  % \mark \jazzTempoMarkup "Easy Swing" c4 "112"

  s1*4
  s1*4

  \part "Verse"
  s1*8

  \part "Chorus"
  s1*4
  s1*4

  \part "Verse"
  s1*8
  \eolMark \markup \eyeglasses

  \part "Chorus"
  s1*4
  s1*4

  s1*5
  s1*4

  \mark \markup \line { "Chorus " \underline "(Repeat 4 xs)" }
  s1*2

  \note "Repeat ad.lib."
  s1*2

  \note "Repeat 8 xs"
}

% Cifrado de acordes
scoreChords = \chords {
  % c1:m f2:m g2:m7
}

trumpet = \relative c' {
  \global
  \key g \major

  R1*3
  r2 r8 \f d'16 e g e d e 

  \break
  g1~
  g2 r8 d8-. e16 d8-. g,16~->
  g1
  R1

  \bar "||"
  \break
  \repeat percent 4 {
    g1~\p
    g2\< r2\!
  }

  \pageBreak
  \bar "[|:"
  \repeat volta 2 {
    R1
    bf16 \f a8-. g16~-. g16 f bf a~-. a g f8~ f4
    R1*2

    \break
    R1
    bf16 a8-. g16~-. g16 f bf a~-. a g f8~ f4
    R1
    c'8.-> e16~-> e8 g-> d4-. r

    \break
    \repeat percent 3 { 
      r2 r4 r8 \f g-> 
      r2 r4 r8 g->
    }
    r2 r4 r8 g-> 
    R1
  }
  \bar ":|]"

  \break
  R1
  bf,16 \f a8-. g16~-. g16 f bf a~-. a g f8~ f4
  R1*2

  \break
  R1
  bf16 a8-. g16~-. g16 f bf a~-. a g f8~ f4
  R1
  c'8.-> e16~-> e8 g-> d4-. r

  \break
  ef,1(\mf
  f2) r2
  g1(\<\glissando
  g'1\f
  af2.) r4

  \bar "||"
  \break
  \key af \major
  R1*4

  \bar "[|:"
  \repeat volta 4 {
    af2.~ \f af8 af-.
    f8 ef~ ef4 r2
  }

  \bar ":|][|:"
  R1*2

  \break
  \bar ":|][|:"
  \repeat volta 8 {
    af2.~ \mf af8 af-.
  }
  \alternative {{
    f8 ef~ ef4 r2
  } {
    f8 ef~ ef4 r8 \f ef16 f af f ef f
  }}
  af4 -> r4\fermata af2\fermata
  \bar "|."
}

% Música
tenorSax = \relative c'' {
  \global
  \key g \major

  R1*3
  r2 r8 \f d16 e g e d e 

  g1~
  g2 r8 d8-. e16 d8-. g,16~->
  g1
  R1

  \repeat percent 4 {
    d'1~ \p
    d2\< r2\!
  }

  \repeat volta 2 {
    R1
    bf'16 \f a8-. g16~-. g16 f bf a~-. a g f8~ f4
    R1*2

    R1
    bf16 a8-. g16~-. g16 f bf a~-. a g f8~ f4
    R1
    e8.-> g16~-> g8 c-> a4-. r

    \repeat percent 3 { 
      r2 r4 r8 \f d-> 
      r2 r4 r8 d->
    }
    r2 r4 r8 d-> 
    R1
  }


  R1
  bf16 \f a8-. g16~-. g16 f bf a~-. a g f8~ f4
  R1*2

  R1
  bf16 a8-. g16~-. g16 f bf a~-. a g f8~ f4
  R1
  e8.-> g16~-> g8 c-> a4-. r

  bf,1( \mf
  d2) r2
  e1(\<\glissando
  c'1\f
  df2.) r4

  \key af \major
  R1*4

  \bar "[|:"
  \repeat volta 4 {
    af2.~ \f af8 af-.
    f8 ef~ ef4 r2
  }

  \bar ":|][|:"
  R1*2

  \bar ":|][|:"
  \repeat volta 8 {
    af2.~ \mf af8 af-.
  }
  \alternative {{
    f8 ef~ ef4 r2
  } {
    f8 ef~ ef4 r8 \f ef16 f af f ef f
  }}
  af4 -> r4\fermata c2\fermata
  \bar "|."
}

% Música
trombone = \relative c {
  \global
  \clef bass
  \key g \major

  R1*3
  r2 r8 \f d'16 e g e d e 

  g1~
  g2 r8 d8-. e16 d8-. g,16~->
  g1
  R1

  \repeat percent 4 {
    g1~\p
    g2\< r2\!
  }

  \repeat volta 2 {
    R1
    bf16 \f a8-. g16~-. g16 f bf a~-. a g f8~ f4
    R1*2

    R1
    bf16 a8-. g16~-. g16 f bf a~-. a g f8~ f4
    R1
    g8.-> c16~-> c8 e-> d4-. r

    \repeat percent 3 { 
      r2 r4 r8 \f g-> 
      r2 r4 r8 g->
    }
    r2 r4 r8 g-> 
    R1
  }


  R1
  bf,16 \f a8-. g16~-. g16 f bf a~-. a g f8~ f4
  R1*2

  R1
  bf16 a8-. g16~-. g16 f bf a~-. a g f8~ f4
  R1
  g8.-> c16~-> c8 e-> d4-. r

  g,1(\mf
  f2) r2
  g1(\<\glissando
  g'1\f
  af2.) r4

  \key af \major
  R1*4

  \bar "[|:"
  \repeat volta 4 {
    af,2.~ \f af8 af-.
    f8 ef~ ef4 r2
  }

  \bar ":|][|:"
  R1*2

  \bar ":|][|:"
  \repeat volta 8 {
    af2.~ \mf af8 af-.
  }
  \alternative {{
    f8 ef~ ef4 r2
  } {
    f8 ef~ ef4 r8 \f ef'16 f af f ef f
  }}
  af4 -> r4\fermata gf2\fermata
  \bar "|."
}

% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
\book {
  \paper { 
    % Descomentar para score
    % #(set-paper-size "a4" 'landscape)
    % indent = 20
    % systems-per-page = 2
  }

  \score {
    % Descomentar para midi
    % \midi { \tempo 4 = 110 }

    % ATENCIÓN
    % NO USAR layout-set-staff-size O SE ROMPE lilyjazz!!!
    \layout { }

    <<
      \scoreMarkup
      \transpose bf c' \scoreChords
      \new StaffGroup <<
        \new Staff \with 
        { instrumentName = "Trpt."}
        { \transpose bf c' \trumpet } 

        \new Staff \with 
        { instrumentName = "T Sx." }
        { \transpose bf c' \tenorSax } 

        \new Staff \with 
        { instrumentName = "Trbn." } 
        { \trombone }
      >>
    >>
  }
}



%{
  \book {
    \header { instrument = "" }
    \bookOutputName ""
    \score {
      <<
        \scoreMarkup
        \scoreChords
        \new Staff { \transpose bf c' \instrumentA }
      >>
    }
  }
  %}

