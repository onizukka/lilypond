% Se debe incluir antes que lilyjazz. Si no, lilyjazz no funciona.
#(set-global-staff-size 20)

\include "main.ily"
\include "music-sheet-paper.ily"

\paper {
  first-page-number = 2
}

\header {
  title = "Suspicious minds"
  instrument = "Trumpet, Tenor Sax & Trombone"
  composer = "Elvis Presley"
  arranger = ""
  poet = "gabriel@sauros.es"
  tagline = ""
  meter = \jazzTempoMarkup "Rock&Roll" c4 "140"
}

% Función musical global
global = {
  % Para pintar todos los números de compás debajo del pentagrama
  % \override Score.BarNumber #'break-visibility = #end-of-line-invisible
  % \override Score.BarNumber #'direction = #DOWN
  % \override Score.BarNumber #'extra-offset = #'(1.5 . 0)
  \compressFullBarRests

  \set Timing.beamExceptions = #'()
  \set Timing.baseMoment = #(ly:make-moment 1/4)
  \set Timing.beatStructure = #'(1 1 1 1) 
}


changeA = \markup \line {
  "("
  \hspace #-.5
  \score {
    \new Staff \with {
      fontSize = #-4
      \override StaffSymbol #'staff-space = #(magstep -4)
      \override StaffSymbol #'line-count = #0
      \override VerticalAxisGroup #'Y-extent = #'(0 . 0)
    }
    \relative c'' { \stemUp c4 }
    \layout {
      ragged-right= ##t
      indent = 0
      \context {
        \Staff
        \remove "Clef_engraver"
        \remove "Time_signature_engraver"
      }
    }
  }
  "="
  \score {
    \new Staff \with {
      fontSize = #-4
      \override StaffSymbol #'staff-space = #(magstep -4)
      \override StaffSymbol #'line-count = #0
      \override VerticalAxisGroup #'Y-extent = #'(0 . 0)
    }
    \relative c'' { \stemUp c8 }
    \layout {
      ragged-right= ##t
      indent = 0
      \context {
        \Staff
        \remove "Clef_engraver"
        \remove "Time_signature_engraver"
      }
    }
  }
  ") "
  \underline "slow feel"
}


changeB = \markup \line {
  "("
  \hspace #-.5
  \score {
    \new Staff \with {
      fontSize = #-4
      \override StaffSymbol #'staff-space = #(magstep -4)
      \override StaffSymbol #'line-count = #0
      \override VerticalAxisGroup #'Y-extent = #'(0 . 0)
    }
    \relative c'' { \stemUp c8 }
    \layout {
      ragged-right= ##t
      indent = 0
      \context {
        \Staff
        \remove "Clef_engraver"
        \remove "Time_signature_engraver"
      }
    }
  }
  "="
  \score {
    \new Staff \with {
      fontSize = #-4
      \override StaffSymbol #'staff-space = #(magstep -4)
      \override StaffSymbol #'line-count = #0
      \override VerticalAxisGroup #'Y-extent = #'(0 . 0)
    }
    \relative c'' { \stemUp c4 }
    \layout {
      ragged-right= ##t
      indent = 0
      \context {
        \Staff
        \remove "Clef_engraver"
        \remove "Time_signature_engraver"
      }
    }
  }
  ") "
  \underline "a tempo"
}

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
  % Oculta el primer pentagrama del markup
  \override Staff.VerticalAxisGroup #'remove-first = ##t

  % Indicación de tiempo
  % \once \override Score.RehearsalMark.self-alignment-X = #LEFT
  \override Score.RehearsalMark.extra-offset = #'(0 . 2) 
  % \mark 

  s1*4

  \part "Verse & Chorus"
  s1*24

  \part "Verse"
  s1*8
  s1*8

  \part "Chorus"
  s1*8

  \once \override Score.RehearsalMark.extra-offset = #'(0 . 0.5) 
  \mark \changeA
  s1*11*6/8

  \once \override Score.RehearsalMark.extra-offset = #'(0 . 0.5) 
  \mark \changeB
  s1

  \part "Verse"
  s1*16

  \mark \markup \line { "Verse " \underline "(Repeat ad.lib, Tacet after decresc, Play on cue)" }
  s1*4

  s1*2
  \once \override Score.RehearsalMark.extra-offset = #'(0 . 0) 
  \mark \markup \underline "(Tacet when decresc, Play on cue)"
  s1*2
  s1*5
}

% Cifrados
scoreChords = \chords {
  % c1:m f2:m g2:m7
}

% Música
trumpet = \relative c'' {
  \global
  \key g \major

  R1*4
  \bar "||"

  R1*24
  \bar "||"

  \break
  R1*6
  r4 c8\mf b e d~ d4
  r4 c8 b e d~ d4

  \break
  d,8-. \p d-. d4-. r2
  R1
  e8-. e-. e4-. r2
  R1

  fs1-> \<
  g2.-> r4 \!
  R1*2

  \break
  R1*1
  r2 r4 g16 a b c
  d1\mf \breathe
  e2\< fs

  g1\f
  fs2 r
  e1
  g2 fs
  \bar "||"

  \break
  \time 6/8
  R1*8*6/8
  R1*3*6/8

  \time 4/4
  d4->\f d-> d-> d->
  \bar "||"

  \break
  R1*8
  R1*8

  \break
  \bar "[|:"
  \repeat volta 2 {
    d,8-.\f d-. d4~-> d r4
    d8-. d-. d4~-> d r4
    e8-. e-. e4~-> e r4
    e8-. e-. e4~-> e r4
  }
  \alternative {
    {
      \break
      fs1->
      g2.-> r4
      r4 c8 b e d~ d4
      r4 c8 b e d~ d4
    }
    {
      \break
      fs,1-> \fermata \>
    }
  }

  R1 \fermataMarkup \!
  g1-> \fermata \>
  R1 \fermataMarkup \!
  g'1-> \fermata
  \bar "|."
}

tenorSax = \relative c'' {
  \global
  \key g \major

  R1*4

  R1*24

  R1*6
  r4 c8\mf b e d~ d4
  r4 c8 b e d~ d4

  b8-. \p b-. b4-. r2
  R1
  c8-. c-. c4-. r2
  R1

  d1-> \< 
  e2.-> r4 \!
  R1*2

  R1*1
  r2 r4 b16 c d e
  fs1\mf \breathe
  g2\< a

  b1\f
  b2 r
  g1
  a1

  \time 6/8
  R1*8*6/8

  R1*3*6/8
  \time 4/4
  d,4->\f d-> d-> d->

  R1*8
  R1*8

  \repeat volta 2 {
    b8-.\f b-. b4~-> b r4
    b8-. b-. b4~-> b r4
    c8-. c-. c4~-> c r4
    c8-. c-. c4~-> c r4
  }
  \alternative {
    {
      d1->
      e2.-> r4
      r4 c8 b e d~ d4
      r4 c8 b e d~ d4
    }
    {
      d1-> \fermata \>
    }
  }

  R1 \fermataMarkup \!
  e1-> \fermata \>
  R1 \fermataMarkup \!
  b'1-> \fermata
}

trombone = \relative c' {
  \global
  \clef bass
  \key g \major

  R1*4

  R1*24

  R1*6
  r4 c8\mf b e d~ d4
  r4 c8 b e d~ d4

  g,8-. \p g-. g4-. r2
  R1
  g8-. g-. g4-. r2
  R1

  a-> \<
  g2.-> r4 \!
  R1*2

  R1*2
  b1\mf \breathe
  c2\< d

  e1\f
  b2 r
  c1
  d1

  \time 6/8
  R1*8*6/8

  R1*3*6/8
  \time 4/4
  d4-> d-> d-> d->

  R1*8
  R1*8

  \repeat volta 2 {
    g,8-.\mf g-. g4~-> g r4
    g8-. g-. g4~-> g r4
    g8-. g-. g4~-> g r4
    g8-. g-. g4~-> g r4
  }
  \alternative {
    {
      a1->
      g2.-> r4
      r4 c8 b e d~ d4
      r4 c8 b e d~ d4
    }
    {
      a1-> \fermata \>
    }
  }

  R1 \fermataMarkup \!
  g-> \fermata \>
  R1 \fermataMarkup \!
  g'1-> \fermata
}

% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\book {
  \paper { 
    % Descomentar para score
    % #(set-paper-size "a4" 'landscape)
    % indent = 20
    % systems-per-page = 2
  }

  \score {
    % Descomentar para midi
    % \midi { \tempo 4 = 110 }

    % NO USAR layout-set-staff-size O SE ROMPE lilyjazz!!!
    \layout { }

    <<
      \scoreMarkup
      \transpose bf c' \scoreChords
      \new StaffGroup <<
        \new Staff \with 
        { instrumentName = "Trpt."}
        { \transpose bf c' \trumpet } 

        \new Staff \with 
        { instrumentName = "T Sx." \RemoveEmptyStaves }
        { \transpose bf c' \tenorSax } 

        \new Staff \with 
        { instrumentName = "Trbn." \RemoveEmptyStaves } 
        { \trombone }
      >>
    >>
  }
}
