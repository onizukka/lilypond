\version "2.18.2"

% Se debe incluir antes que lilyjazz. Si no, lilyjazz no funciona.
#(set-global-staff-size 20)

\include "main.ily"
\include "music-sheet-paper.ily"

\paper {
  first-page-number = 2
}

\header {
  title = "Sugarfoot"
  instrument = "Trumpet, Tenor Sax & Trombone"
  composer = "Black Joe Lewis & The Honeybears"
  arranger = ""
  poet = "gabriel@sauros.es"
  tagline = ""
  meter = \jazzTempoMarkup "Medium Funk" c4 "120"
}

% Función musical global
global = {
  % Para pintar todos los números de compás debajo del pentagrama
  % \override Score.BarNumber #'break-visibility = #end-of-line-invisible
  % \override Score.BarNumber #'direction = #DOWN
  % \override Score.BarNumber #'extra-offset = #'(1.5 . 0)
  \compressFullBarRests

  \set Timing.beamExceptions = #'()
  \set Timing.baseMoment = #(ly:make-moment 1/4)
  \set Timing.beatStructure = #'(1 1 1 1) 
}

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
  % Oculta el primer pentagrama del markup
  \override Staff.VerticalAxisGroup #'remove-first = ##t

  % Indicación de tiempo
  % \once \override Score.RehearsalMark.self-alignment-X = #LEFT
  \override Score.RehearsalMark.extra-offset = #'(-1 . 2) 
  \override Score.RehearsalMark.self-alignment-X = #RIGHT
  s1

  \mark \markup \tiny "\"Sugarfoot called me last night\""
  s1
  \mark \markup \tiny "\"Said his girl called him\""
  s1
  \once \override Score.RehearsalMark.break-visibility = #end-of-line-visible
  \mark \markup \tiny "\"He said can I come over\""
  s1
  \mark \markup \tiny "\"She said yeah\""
  s1*2
  s1*4

  \override Score.RehearsalMark.extra-offset = #'(0 . 2) 
  \override Score.RehearsalMark.self-alignment-X = #LEFT
  \mark \markup \line { "Verse " \underline "(Repeat 3 xs)" }
  s1*8
  s1*4

  \part "Chorus"
  s1*4

  \part "Bridge"
  s1*10

  \part "Chorus"
  s1*4

  \part "Verse"
  s1*8
  s1*4

  \part "Chorus"
  s1*8
}


% Cifrados
scoreChords = \chords {
  % c1:m f2:m g2:m7
}

% Música
trumpet = \relative c'' {
  \global

  s1
  g'4-^ \f s4 s2
  g4-^ s4 s2
  \break
  g4-^ s4 s2

  g4-^ r g-^ r
  g4-^ g4-^ g4-^ g4-^

  \break
  \repeat percent 2 {
    r2 f16 -> \f f-. r8 g16 -> g-. r8
    r2 f16 -> f-. r8 g16 -> g-. r8
  }

  \pageBreak
  \bar "[|:-||"
  \repeat volta 3 {
    R1*8
    R1*2
    \break
    d16 ( \mf e8-. ) d16 ( e4-. ) d16 ( e8-. ) d16 ( e4-. )
    d16 ( e8-. ) d16 ( e8-. ) d16 ( e-. ) r d16 ( e8-. ) e4-.

    \repeat percent 2 {
      r2 f16 -> \f f-. r8 g16 -> g-. r8
      r2 f16 -> f-. r8 g16 -> g-. r8
      \noBreak
    }
  }
  \bar ":|]"
  
  \break
  R1*4
  \pageBreak
  \bar "[|:"
  \repeat volta 2 {
    \repeat percent 2 {
      r4 g-^ \f r g-^
      r4 g-^ r g-^
    }
  }
  \bar ":|]"

  \break
  \bar "[|:"
  \repeat volta 2 {
    d16 ( \mf e8-. ) d16 ( e4-. ) d16 ( e8-. ) d16 ( e4-. )
    d16 ( e8-. ) d16 ( e8-. ) d16 ( e-. ) r d16 ( e8-. ) e4-.
  }
  \bar ":|]"

  \repeat percent 2 {
    r2 f16 -> \f f-. r8 g16 -> g-. r8
    r2 f16 -> f-. r8 g16 -> g-. r8
  }

  R1*8
  R1*2
  d16 ( \mf e8-. ) d16 ( e4-. ) d16 ( e8-. ) d16 ( e4-. )
  d16 ( e8-. ) d16 ( e8-. ) d16 ( e-. ) r d16 ( e8-. ) e4-.

  \repeat percent 2 {
    r2 f16 -> \f f-. r8 g16 -> g-. r8
    r2 f16 -> f-. r8 g16 -> g-. r8
  }
  \repeat percent 2 {
    r2 f16 -> \f f-. r8 g16 -> g-. r8
    r2 f16 -> f-. r8 g16 -> g-. r8
  }

  \bar "|."
}

tenorSax = \relative c'' {
  \global

  s1
  b'4-^ \f s4 s2
  b4-^ s4 s2
  b4-^ s4 s2

  b4-^ r b-^ r
  b4-^ b4-^ b4-^ b4-^

  \repeat percent 2 {
    r2 d,16 -> \f d-. r8 e16 -> e-. r8
    r2 d16 -> d-. r8 e16 -> e-. r8
  }

  \repeat volta 3 {
    R1*8
    R1*2
    f16 ( \mf g8-. ) f16 ( g4-. ) f16 ( g8-. ) f16 ( g4-. )
    f16 ( g8-. ) f16 ( g8-. ) f16 ( g-. ) r f16 ( g8-. ) g4-.

    \repeat percent 2 {
      r2 d16 -> \f d-. r8 e16 -> e-. r8
      r2 d16 -> d-. r8 e16 -> e-. r8
    }
  }
  
  R1*4
  \repeat volta 2 {
    \repeat percent 2 {
      r4 b'-^ \f r b-^
      r4 b-^ r b-^
    }
  }

  \repeat volta 2 {
    f16 ( \mf g8-. ) f16 ( g4-. ) f16 ( g8-. ) f16 ( g4-. )
    f16 ( g8-. ) f16 ( g8-. ) f16 ( g-. ) r f16 ( g8-. ) g4-.
  }

  \repeat percent 2 {
    r2 d16 -> \f d-. r8 e16 -> e-. r8
    r2 d16 -> d-. r8 e16 -> e-. r8
  }

  R1*8
  R1*2
  \break
  f16 ( \mf g8-. ) f16 ( g4-. ) f16 ( g8-. ) f16 ( g4-. )
  f16 ( g8-. ) f16 ( g8-. ) f16 ( g-. ) r f16 ( g8-. ) g4-.

  \repeat percent 2 {
    r2 d16 -> \f d-. r8 e16 -> e-. r8
    r2 d16 -> d-. r8 e16 -> e-. r8
  }
  \repeat percent 2 {
    r2 d16 -> \f d-. r8 e16 -> e-. r8
    r2 d16 -> d-. r8 e16 -> e-. r8
  }
}

trombone = \relative c' {
  \global
  \clef bass

  s1
  f,4-^ \f s4 s2
  f4-^ s4 s2
  f4-^ s4 s2

  f4-^ r f-^ r
  f4-^ f4-^ f4-^ f4-^

  \repeat percent 2 {
    g,2 \f -> ~ g4 ~ g8 r
    g2 -> ~ g4 ~ g8 r
  }

  \repeat volta 3 {
    R1*8
    R1*2
    bf'16 ( \mf c8-. ) bf16 ( c4-. ) bf16 ( c8-. ) bf16 ( c4-. )
    bf16 ( c8-. ) bf16 ( c8-. ) bf16 ( c-. ) r bf16 ( c8-. ) c4-.

    \repeat percent 2 {
      g,2 \f -> ~ g4 ~ g8 r
      g2 -> ~ g4 ~ g8 r
    }
  }
  
  R1*4
  \repeat volta 2 {
    \repeat percent 2 {
      r4 f'-^ \f r f-^
      r4 f-^ r f-^
    }
  }

  \repeat volta 2 {
    bf16 ( \mf c8-. ) bf16 ( c4-. ) bf16 ( c8-. ) bf16 ( c4-. )
    bf16 ( c8-. ) bf16 ( c8-. ) bf16 ( c-. ) r bf16 ( c8-. ) c4-.
  }

  \repeat percent 2 {
    g,2 \f -> ~ g4 ~ g8 r
    g2 -> ~ g4 ~ g8 r
  }

  \break
  R1*8
  R1*2
  bf'16 ( \mf c8-. ) bf16 ( c4-. ) bf16 ( c8-. ) bf16 ( c4-. )
  bf16 ( c8-. ) bf16 ( c8-. ) bf16 ( c-. ) r bf16 ( c8-. ) c4-.

  \break
  \repeat percent 2 {
    g,2 \f -> ~ g4 ~ g8 r
    g2 -> ~ g4 ~ g8 r
  }
  \repeat percent 2 {
    g2 \f -> ~ g4 ~ g8 r
    g2 -> ~ g4 ~ g8 r
  }
}

% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\book {
  \paper { 
    % Descomentar para score
    % #(set-paper-size "a4" 'landscape)
    % indent = 20
    % systems-per-page = 2
  }

  \score {
    % Descomentar para midi
    % \midi { \tempo 4 = 110 }

    % NO USAR layout-set-staff-size O SE ROMPE lilyjazz!!!
    \layout { }

    <<
      \scoreMarkup
      \transpose bf c' \scoreChords
      \new StaffGroup <<
        \new Staff \with 
        { instrumentName = "Trpt."}
        { \transpose bf c' \trumpet } 

        \new Staff \with 
        { instrumentName = "T Sx." \RemoveEmptyStaves }
        { \transpose bf c' \tenorSax } 

        \new Staff \with 
        { instrumentName = "Trbn." \RemoveEmptyStaves } 
        { \trombone }
      >>
    >>
  }
}
