% Se debe incluir antes que lilyjazz. Si no, lilyjazz no funciona.
#(set-global-staff-size 20)

\include "main.ily"
\include "music-sheet-paper.ily"

\header {
  title = "Hold on, I'm coming"
  instrument = "Trumpet, Tenor Sax & Trombone"
  composer = "Sam & Dave"
  arranger = ""
  poet = "gabriel@sauros.es"
  tagline = ""
  meter = \jazzTempoMarkup "R&B" c4 "90"
}

\paper {
    first-page-number = 2
}

% Función musical global
global = {
  % Para pintar todos los números de compás debajo del pentagrama
  % \override Score.BarNumber #'break-visibility = #end-of-line-invisible
  % \override Score.BarNumber #'direction = #DOWN
  % \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

  \compressFullBarRests
}

% Elementos comunes de texto (marcas de ensayo, tempo, saltos de línea, etc)
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
  % Oculta el primer pentagrama del markup
  \override Staff.VerticalAxisGroup #'remove-first = ##t

  \set Timing.beamExceptions = #'()
  \set Timing.baseMoment = #(ly:make-moment 1/4)
  \set Timing.beatStructure = #'(1 1 1 1) 

  % Indicación de tiempo
  % \once \override Score.RehearsalMark.self-alignment-X = #LEFT
  \override Score.RehearsalMark.extra-offset = #'(0 . 2) 
  % \mark 

  % s1*2
  % \once \override Score.RehearsalMark.X-offset = #5
  % \mark \markup \boxed "A"

  s1*4

  \part "Verse"
  s1*8
  \part "Chorus"
  s1*4

  \part "Verse"
  s1*2
  s1*2

  \part "Chorus"
  s1*4

  \part "Bridge"
  s1*12

  s1*4

  \note "Keyboard solo"
  s1*16

  \note "Guitar solo"
  s1*12

  s1*4

  \part "Verse"
  s1*2
  s1*2

  \mark \markup \line { "Chorus " \underline "(Repeat 6 xs, 3rd x: double feel)" }
  s1*4

  s1*2
}

% Cifrado de acordes
scoreChords = \chords {
  % c1:m f2:m g2:m7
}

trumpet = \relative c' {
  \global
  \key af \major

  \repeat percent 2 { af'16 \f gf8-. ef16~-. ef8 ef~-> ef16 ef gf8-. af4-. }
  \repeat percent 2 { af'16 gf8-. ef16~-. ef8 ef~-> ef16 ef gf8-. af4-. }

  \break
  \bar "||"
  R1*8

  \repeat percent 2 { af,16 \f gf8-. ef16~-. ef8 ef~-> ef16 ef gf8-. af4-. }
  \repeat percent 2 { af'16 gf8-. ef16~-. ef8 ef~-> ef16 ef gf8-. af4-. }

  \break
  \bar "[|:"
  \repeat volta 2 { 
    r4 \mp c,-. c-. c-.
    r4 c-. c-. c-.
  }

  \bar ":|][|:"
  \repeat volta 2 { 
    r4 cf-. cf-. cf-.
    r4 cf-. cf-. cf-.
  }

  \break
  \bar ":|][|:"
  \repeat volta 2 {
    \repeat percent 4 { af16 \mf gf8-. ef16~-. ef8 ef~-> ef16 ef gf8-. af4-. }
  }
  \bar ":|]"

  \break
  R1*8

  af1( \mf
  gf)\> \breathe
  cf(\!
  df2.)\< r4 \!

  \break
  \repeat percent 2 { af16 \f gf8-. ef16~-. ef8 ef~-> ef16 ef gf8-. af4-. }
  \repeat percent 2 { af'16 gf8-. ef16~-. ef8 ef~-> ef16 ef gf8-. af4-. }

  \bar "||"
  \break
  R1*16
  R1*12

  \break
  \repeat percent 2 { af,16 \f gf8-. ef16~-. ef8 ef~-> ef16 ef gf8-. af4-. }
  \repeat percent 2 { af'16 gf8-. ef16~-. ef8 ef~-> ef16 ef gf8-. af4-. }

  \break
  \bar "[|:"
  \repeat volta 2 { 
    r4 \mp c,-. c-. c-.
    r4 c-. c-. c-.
  }

  \bar ":|][|:"
  \repeat volta 2 { 
    r4 cf-. cf-. cf-.
    r4 cf-. cf-. cf-.
  }

  \bar ":|][|:"
  \repeat volta 6 {
    \repeat percent 2 { af16 \mf gf8-. ef16~-. ef8 ef~-> ef16 ef gf8-. af4-. }
  }
  \alternative {{
    \set Score.repeatCommands = #'((volta "1, 2, 3, 4, 5"))
    \repeat percent 2 { af16 gf8-. ef16~-. ef8 ef~-> ef16 ef gf8-. af4-. }
  } {
    \break
    af16 \< gf8-. ef16~-. ef8 ef~-> ef16 ef gf8-. af8.-. ef'16 \f
  }}
  af16 gf8-. ef16~-. ef df ef8~-> ef16 ef gf8-. af4->
  \bar "|."
}

% Música
tenorSax = \relative c'' {
  \global
  \key af \major

  \repeat percent 2 { af16 \f gf8-. ef16~-. ef8 ef~-> ef16 ef gf8-. af4-. }
  \repeat percent 2 { af'16 gf8-. ef16~-. ef8 ef~-> ef16 ef gf8-. af4-. }

  R1*8

  \repeat percent 2 { af,16 \f gf8-. ef16~-. ef8 ef~-> ef16 ef gf8-. af4-. }
  \repeat percent 2 { af'16 gf8-. ef16~-. ef8 ef~-> ef16 ef gf8-. af4-. }
  \repeat volta 2 { 
    r4 \mp ef-. ef-. ef-. 
    r4 ef-. ef-. ef-. 
  }
  \repeat volta 2 { 
    r4 f-. f-. f-.
    r4 f-. f-. f-.
  }

  \repeat volta 2 {
    \repeat percent 4 { af,16 \mf gf8-. ef16~-. ef8 ef~-> ef16 ef gf8-. af4-. }
  }

  R1*8

  ef'1( \mf
  df)\> \breathe
  gf(\!
  af2.)\< r4\!

  \repeat percent 2 { af,16 \f gf8-. ef16~-. ef8 ef~-> ef16 ef gf8-. af4-. }
  \repeat percent 2 { af'16 gf8-. ef16~-. ef8 ef~-> ef16 ef gf8-. af4-. }

  R1*16
  R1*12

  \repeat percent 2 { af,16 \f gf8-. ef16~-. ef8 ef~-> ef16 ef gf8-. af4-. }
  \repeat percent 2 { af'16 gf8-. ef16~-. ef8 ef~-> ef16 ef gf8-. af4-. }
  \repeat volta 2 { 
    r4 \mp ef-. ef-. ef-. 
    r4 ef-. ef-. ef-. 
  }
  \repeat volta 2 { 
    r4 f-. f-. f-.
    r4 f-. f-. f-.
  }

  \repeat volta 2 {
    \repeat percent 2 { af,16 \mf gf8-. ef16~-. ef8 ef~-> ef16 ef gf8-. af4-. }
  }
  \alternative {{
    \repeat percent 2 { af16 gf8-. ef16~-. ef8 ef~-> ef16 ef gf8-. af4-. }
  } {
    af16 \< gf8-. ef16~-. ef8 ef~-> ef16 ef gf8-. af8.-. ef'16 \f
  }}
  af16 gf8-. ef16~-. ef df ef8~-> ef16 ef gf8-. af4->
}

% Música
trombone = \relative c {
  \global
  \clef bass
  \key af \major
  \repeat percent 2 { af'16 \f gf8-. ef16~-. ef8 ef~-> ef16 ef gf8-. af4-. }
  \repeat percent 2 { af'16 gf8-. ef16~-. ef8 ef~-> ef16 ef gf8-. af4-. }

  R1*8

  \repeat percent 2 { af,16 \f gf8-. ef16~-. ef8 ef~-> ef16 ef gf8-. af4-. }
  \repeat percent 2 { af'16 gf8-. ef16~-. ef8 ef~-> ef16 ef gf8-. af4-. }
  \repeat volta 2 { 
    r4 \mp af,-. af-. af-. 
    r4 af-. af-. af-.
  }
  \repeat volta 2 { 
    r4 df-. df-. df-. 
    r4 df-. df-. df-. 
  }

  \repeat volta 2 {
    \repeat percent 4 { af16 \mf gf8-. ef16~-. ef8 ef~-> ef16 ef gf8-. af4-. }
  }

  R1*8

  c1( \mf 
  bf)\> \breathe
  ef(\!
  df2.)\< r4\!

  \repeat percent 2 { af16 \f gf8-. ef16~-. ef8 ef~-> ef16 ef gf8-. af4-. }
  \repeat percent 2 { af'16 gf8-. ef16~-. ef8 ef~-> ef16 ef gf8-. af4-. }

  R1*16
  R1*12

  \repeat percent 2 { af,16 \f gf8-. ef16~-. ef8 ef~-> ef16 ef gf8-. af4-. }
  \repeat percent 2 { af'16 gf8-. ef16~-. ef8 ef~-> ef16 ef gf8-. af4-. }
  \repeat volta 2 { 
    r4 \mp af,-. af-. af-. 
    r4 af-. af-. af-.
  }
  \repeat volta 2 {
    r4 df-. df-. df-. 
    r4 df-. df-. df-. 
  }

  \repeat volta 2 {
    \repeat percent 2 { af16 \mf gf8-. ef16~-. ef8 ef~-> ef16 ef gf8-. af4-. }
  }
  \alternative {{
    \repeat percent 2 { af16 gf8-. ef16~-. ef8 ef~-> ef16 ef gf8-. af4-. }
  } {
    af16 \< gf8-. ef16~-. ef8 ef~-> ef16 ef gf8-. af8.-. ef'16 \f
  }}
  af16 gf8-. ef16~-. ef df ef8~-> ef16 ef gf8-. af4->
}

% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
\book {
  \paper { 
    % Descomentar para score
    % #(set-paper-size "a4" 'landscape)
    % indent = 20
    % systems-per-page = 2
  }

  \score {
    % Descomentar para midi
    % \midi { \tempo 4 = 110 }

    % ATENCIÓN
    % NO USAR layout-set-staff-size O SE ROMPE lilyjazz!!!
    \layout { }

    <<
      \scoreMarkup
      \transpose bf c' \scoreChords
      \new StaffGroup <<
        \new Staff \with 
        { instrumentName = "Trpt."}
        { \transpose bf c' \trumpet } 

        \new Staff \with 
        { instrumentName = "T Sx." }
        { \transpose bf c' \tenorSax } 

        \new Staff \with 
        { instrumentName = "Trbn." } 
        { \trombone }
      >>
    >>
  }
}



%{
  \book {
    \header { instrument = "" }
    \bookOutputName ""
    \score {
      <<
        \scoreMarkup
        \scoreChords
        \new Staff { \transpose bf c' \instrumentA }
      >>
    }
  }
  %}

