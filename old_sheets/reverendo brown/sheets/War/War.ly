\version "2.18.2"
\language "english"

% Se debe incluir antes que lilyjazz. Si no, lilyjazz no funciona.
#(set-global-staff-size 19)

\include "main.ily"

\paper {
	indent = 10
}

\header {
	title = "War"
	instrument = "Trumpet, Tenor Sax & Trombone"
	composer = "Edwin Starr"
	arranger = "Transc: gabriel@sauros.es"
	poet = ""
	tagline = "Edition: gabriel@sauros.es"
}

% Función musical global
global = {
	% Para pintar todos los números de compás debajo del pentagrama
	% \override Score.BarNumber #'break-visibility = #end-of-line-invisible
	% \override Score.BarNumber #'direction = #DOWN
	% \override Score.BarNumber #'extra-offset = #'(1.5 . 0)
	\compressFullBarRests

	\set Timing.beamExceptions = #'()
	\set Timing.baseMoment = #(ly:make-moment 1/4)
	\set Timing.beatStructure = #'(1 1 1 1) 
}

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
	% Oculta el primer pentagrama del markup
	\override Staff.VerticalAxisGroup #'remove-first = ##t

	% Indicación de tiempo
	% \once \override Score.RehearsalMark.self-alignment-X = #LEFT
	% \once \override Score.RehearsalMark.extra-offset = #'(0 . 3) 
	% \mark \jazzTempoMarkup "Easy Swing" c4 "112"

	\mkup "(Last D.C. Repeat only 4xs)"
	\mkup "(Repeat 6xs)"
	s1*2
  \eolMark \toCoda

	\break
	s1*6
	\eolMark \markup \underline "(D.C 4 xs. Last time to Coda)"

	\break
	\mark \coda
}

% Cifrados
scoreChords = \chords {
	% c1:m f2:m g2:m7
}

% Música
trumpet = \relative c'' {
	\global
	\key e \minor

	\bar "[|:"
	\repeat volta 6 {
		r8 \f e-. e4-. r8 g-. g4-.
		r2 r8 g~-> g4
	}
	\bar ":|]"

	R1*2
	\bar "[|:"
	\repeat volta 2 {
		e1~\p\<
		e2 r\!
	}
	\bar ":|]"
	R1
	r8 e8-.\p r e~-> e2\<
	\bar "||"

	R1*8\! 
  R1
  r8 \f e-. e4-. r2
	\bar "|."
}

tenorSax = \relative c'' {
	\global
	\key e \minor

	\repeat volta 6 {
		r8 \f g-. g4-. r8 b-. b4-.
		r2 r8 b~-> b4
	}

	R1*2
	\repeat volta 2 {
		e1~\p\<
		e2 r\!
	}
	R1
	r8 e8-.\p r e~->e2\<

	R1*8\! 
  R1
  r8 \f e-. e4-. r2
	\bar "|."
}

trombone = \relative c' {
	\global
	\clef bass
	\key e \minor

	\repeat volta 6 {
		r8 \f e-. e4-. r8 g-. g4-.
		r2 r8 g~-> g4
	}

	R1*2
	\repeat volta 2 {
		e1~\p\<
		e2 r\!
	}
	R1
	r8 e8-.\p r e~->e2\<

	R1*8\! 
  R1
  r8 \f e-. e4-. r2
	\bar "|."
}

% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\book {
	\paper { 
		% Descomentar para score
		% #(set-paper-size "a4" 'landscape)
		% indent = 20
		% systems-per-page = 2
	}
	
	\score {
		% Descomentar para midi
		% \midi { \tempo 4 = 110 }
		
		% NO USAR layout-set-staff-size O SE ROMPE lilyjazz!!!
		\layout { }
		
		<<
			\scoreMarkup
			\transpose bf c' \scoreChords
			\new StaffGroup <<
				\new Staff \with 
				{ instrumentName = "Trpt."}
				{ \transpose bf c' \trumpet } 

				\new Staff \with 
				{ instrumentName = "T Sx." }
				{ \transpose bf c' \tenorSax } 

				\new Staff \with 
				{ instrumentName = "Trbn." } 
				{ \trombone }
			>>
		>>
	}
}
