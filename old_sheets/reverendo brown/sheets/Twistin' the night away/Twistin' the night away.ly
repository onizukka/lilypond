% Se debe incluir antes que lilyjazz. Si no, lilyjazz no funciona.
#(set-global-staff-size 20)

\include "main.ily"
\include "music-sheet-paper.ily"

\paper {
  first-page-number = 2
}

\header {
  title = "Twistin' the night away"
  instrument = "Trumpet, Tenor Sax & Trombone"
  composer = "Sam Cooke"
  arranger = ""
  poet = "gabriel@sauros.es"
  tagline = ""
  meter = \jazzTempoMarkup "Medium Swing" c4 "140"
}

% Función musical global
global = {
  % Para pintar todos los números de compás debajo del pentagrama
  % \override Score.BarNumber #'break-visibility = #end-of-line-invisible
  % \override Score.BarNumber #'direction = #DOWN
  % \override Score.BarNumber #'extra-offset = #'(1.5 . 0)
  \compressFullBarRests

  \set Timing.beamExceptions = #'()
  \set Timing.baseMoment = #(ly:make-moment 1/4)
  \set Timing.beatStructure = #'(1 1 1 1) 
}

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
  % Oculta el primer pentagrama del markup
  \override Staff.VerticalAxisGroup #'remove-first = ##t

  % Indicación de tiempo
  \override Score.RehearsalMark.extra-offset = #'(0 . 2)

  s4.

  \part "Intro"
  s1*4
  \part "Verse"
  s1*16
  \part "Chorus"
  s1*9
  \mkup "(Harp solo)"
  s1*16
  \part "Verse"
  s1*16
  \part "Chorus"
  s1*8
  \mkup "(Trombone solo)"
  s1*9
  \part "Chorus"
  s1*9
  s1*3
}

% Cifrados
scoreChords = \chords {
  % c1:m f2:m g2:m7
}

tromboneChords = \chords {
  % c1:m f2:m g2:m7

  s4.
  s1*4
  s1*16
  s1*9
  s1*16
  s1*16
  s1*8

  a1 s
  fs:m s
  d s
  e s
}

% Música
trumpet = \relative c'' {
  \global
  \key a \major

  \partial 4.
  r8 r4
  \bar "||"

  r4 \f a'8 e g a r e(~->
  e4 fs)-. e(-> fs)-.
  r4 a8 e g a r a-^
  \break
  \showStave R1

  \bar "[|:"
  \repeat volta 2 {
    R1*5
    \break
    R1*2
    r2 r4\mp e,,4-. 

    a4-- r r2
    r2 r4 gs4-. 
    fs4-- r r2
    \break
    r2 r4 e4-.

    d4-- r r2
    r2 r4 d4-.
    e4-- r r2  
    r2 r4 r8\mf cs

    \bar "||"
    r8 cs r4 r4 r8 cs
    \break
    r8 cs r4 r4 r8 e(~->
    e a, cs a cs d cs e)-.
    r2 r4 r8 d

    r8 d r4 r4 r8 d
    \break
    r8 d r4 r4 r8 fs
    r8 fs r8 fs e4-- cs8 cs-.

  } \alternative {{
    R1
    \bar ":|]"
  }{
    R1
  }}

  \break
  R1*15

  \pageBreak
  \showStave R1
  \bar "||"
  R1*7
  \break
  r2 r4\mp e4-. 

  a4-- r r2
  r2 r4 gs4-. 
  fs4-- r r2
  r2 r4 e4-.

  d4-- r r2
  r2 r4 d4-.
  e4-- r r2  
  \break
  r2 r4 r8\mf cs

  \bar "||"
  r8 cs r4 r4 r8 cs
  r8 cs r4 r4 r8 e(~->
  e a, cs a cs d cs e)-.
  \break
  r2 r4 r8 d

  r8 d r4 r4 r8 d
  r8 d r4 r4 r8 fs
  r8 fs r8 fs e4-- cs8 cs-.
  R1

  \break
  \bar "[|:"
  \repeat volta 2 {
    R1*7
  } \alternative {{
    R1
    \bar ":|]"
  }{
    \break
    r2 r4 r8 cs
  }}

  \bar "[|:"
  \repeat volta 2 {
    r8 cs r4 r4 r8 cs
    r8 cs r4 r4 r8 e(~->
    e a, cs a cs d cs e)-.
    \break
    r2 r4 r8 d

    r8 d r4 r4 r8 d
    r8 d r4 r4 r8 fs
    r8 fs r8 fs e4-- cs8 cs-.

  } \alternative {{
    r2 r4 r8 cs
    \bar ":|]"
  }{
    \break
    R1
  }}

  r4\f a''8 e g a r e(~->
  e4 fs)-. e(-> fs)-.
  r4 a8 e g a r a-^
  \bar "|."
}

tenorSax = \relative c'' {
  \global
  \key a \major

  \partial 4.
  r8 r4

  r4\f cs'8 a b cs r a(~->
  a4 a)-. a(-> a)-.
  r4 cs8 a b cs r cs-^
  r2 r4\mp  cs,4-. 

  \repeat volta 2 {
    fs4-- r r2
    r2 r4 cs4-. 
    a4-- r r2
    r2 r4 cs4-.

    b4-- r r2
    r2 r4 fs4-.
    gs4-- r r2  
    r2 r4 cs4-. 

    fs4-- r r2
    r2 r4 cs4-. 
    a4-- r r2
    r2 r4 cs4-.

    b4-- r r2
    r2 r4 fs4-.
    gs4-- r r2  
    r2 r4 r8\mf a

    r8 a r4 r4 r8 a
    r8 a r4 r4 r8 cs(~->
    cs fs, a fs a b a cs)-.
    r2 r4 r8 a

    r8 a r4 r4 r8 a
    r8 a r4 r4 r8 cs
    r8 b r8 cs b4-- a8 a-.

  } \alternative {{
    r2 r4 cs4-. 
  }{
    R1
  }}

  R1*15
  r2 r4\mp  cs4-. 

  fs4-- r r2
  r2 r4 cs4-. 
  a4-- r r2
  r2 r4 cs4-.

  b4-- r r2
  r2 r4 fs4-.
  gs4-- r r2  
  r2 r4 cs4-. 

  fs4-- r r2
  r2 r4 cs4-. 
  a4-- r r2
  r2 r4 cs4-.

  b4-- r r2
  r2 r4 fs4-.
  gs4-- r r2  
  r2 r4 r8\mf a

  r8 a r4 r4 r8 a
  r8 a r4 r4 r8 cs(~->
  cs fs, a fs a b a cs)-.
  r2 r4 r8 a

  r8 a r4 r4 r8 a
  r8 a r4 r4 r8 cs
  r8 b r8 cs b4-- a8 a-.
  R1

  \repeat volta 2 {
    R1*7
  } \alternative {{
    R1
  }{
    r2 r4 r8 a
  }}

  \repeat volta 2 {
    r8 a r4 r4 r8 a
    r8 a r4 r4 r8 cs(~->
    cs fs, a fs a b a cs)-.
    r2 r4 r8 a

    r8 a r4 r4 r8 a
    r8 a r4 r4 r8 cs
    r8 b r8 cs b4-- a8 a-.

  } \alternative {{
    r2 r4 r8 a
  }{
    R1
  }}

  r4\f cs'8 a b cs r a(~->
  a4 a)-. a(-> a)-.
  r4 cs8 a b cs r cs-^
}

trombone = \relative c' {
  \global
  \key a \major
  \clef bass

  \partial 4.
  r8 r4

  r4\f a'8 e g a r e(~->
  e4 fs)-. e(-> fs)-.
  r4 a8 e g a r a-^
  r2 r4 \mp e,4-. 

  \repeat volta 2 {
    a4-- r r2
    r2 r4 gs4-. 
    fs4-- r r2
    r2 r4 e4-.

    d4-- r r2
    r2 r4 d4-.
    e4-- r r2  
    r2 r4 e-.

    a4-- r r2
    r2 r4 gs4-. 
    fs4-- r r2
    r2 r4 e4-.

    d4-- r r2
    r2 r4 d4-.
    e4-- r r2  
    r4\mf fs4-- e-- cs8 a

    r4 r8 a~-> a4\bendAfter #-2 r
    r4 r8 a~-> a4\bendAfter #-2 r8 a'(~->
    a e fs e fs fs e a)-.
    r4 fs4-- e-- cs8 d

    r4 r8 d~-> d4\bendAfter #-2 r
    r4 r8 fs~-> fs4\bendAfter #-2 r8 e
    r8 e r8 fs gs4-- fs8 e-.

  } \alternative {{
    r2 r4 e4-. 
  }{
    R1
  }}

  R1*15
  r2 r4 \mp e4-. 

  a4-- r r2
  r2 r4 gs4-. 
  fs4-- r r2
  r2 r4 e4-.

  d4-- r r2
  r2 r4 d4-.
  e4-- r r2  
  r2 r4 e-.

  a4-- r r2
  r2 r4 gs4-. 
  fs4-- r r2
  r2 r4 e4-.

  d4-- r r2
  r2 r4 d4-.
  e4-- r r2  
  r4\mf fs4-- e-- cs8 a

  r4 r8 a~-> a4\bendAfter #-2 r
  r4 r8 a~-> a4\bendAfter #-2 r8 a'(~->
  a e fs e fs fs e a)-.
  r4 fs4-- e-- cs8 d

  r4 r8 d~-> d4\bendAfter #-2 r
  r4 r8 fs~-> fs4\bendAfter #-2 r8 e
  r8 e r8 fs gs4-- fs8 e-.
  R1

  \repeat volta 2 {
    \improOn
    \repeat unfold 14 { r2 }
  } \alternative {{
    r2 r2
  }{
    \improOff
    r4 fs4-- e-- cs8 a
  }}

  \repeat volta 2 {
    r4 r8 a~-> a4\bendAfter #-2 r
    r4 r8 a~-> a4\bendAfter #-2 r8 a'(~->
    a e fs e fs fs e a)-.
    r4 fs4-- e-- cs8 d

    r4 r8 d~-> d4\bendAfter #-2 r
    r4 r8 fs~-> fs4\bendAfter #-2 r8 e
    r8 e r8 fs gs4-- fs8 e-.

  } \alternative {{
    r4 fs4-- e-- cs8 a
  }{
    R1
  }}

  r4\f a''8 e g a r e(~->
  e4 fs)-. e(-> fs)-.
  r4 a8 e g a r a-^
}

% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\book {
  \paper { 
    % Descomentar para score
    % #(set-paper-size "a4" 'landscape)
    % indent = 20
    % systems-per-page = 2
  }

  \score {
    % Descomentar para midi
    % \midi { \tempo 4 = 110 }

    % NO USAR layout-set-staff-size O SE ROMPE lilyjazz!!!
    \layout { }

    <<
      \scoreMarkup
      \transpose bf c' \scoreChords
      \new StaffGroup <<
        \new Staff \with 
        { instrumentName = "Trpt." \RemoveEmptyStaves }
        { \transpose bf c' \trumpet } 

        \new Staff \with 
        { instrumentName = "T Sx." \RemoveEmptyStaves }
        { \transpose bf c' \tenorSax } 

        \tromboneChords
        \new Staff \with 
        { instrumentName = "Trbn." } 
        { \trombone }
      >>
    >>
  }
}
