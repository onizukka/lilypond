\version "2.18.2"

% Se debe incluir antes que lilyjazz. Si no, lilyjazz no funciona.
#(set-global-staff-size 20)

\include "main.ily"
\include "music-sheet-paper.ily"

\paper {
}

\header {
  title = "Treat her right"
  instrument = "Trumpet, Tenor Sax & Trombone"
  composer = "Roy Head & Gene Kurtz"
  arranger = ""
  poet = "gabriel@sauros.es"
  tagline = ""
  meter = \jazzTempoMarkup "Fast Soul" c4 "170"
}

% Función musical global
global = {
  % Para pintar todos los números de compás debajo del pentagrama
  % \override Score.BarNumber #'break-visibility = #end-of-line-invisible
  % \override Score.BarNumber #'direction = #DOWN
  % \override Score.BarNumber #'extra-offset = #'(1.5 . 0)
  \compressFullBarRests

  \set Timing.beamExceptions = #'()
  \set Timing.baseMoment = #(ly:make-moment 1/4)
  \set Timing.beatStructure = #'(1 1 1 1) 
}

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
  % Oculta el primer pentagrama del markup
  \override Staff.VerticalAxisGroup #'remove-first = ##t

  % Indicación de tiempo
  % \once \override Score.RehearsalMark.self-alignment-X = #LEFT
  \override Score.RehearsalMark.extra-offset = #'(0 . 2) 

  \note "Tacet 1st x"
  \part "Chorus"
  s1
  \note "Play both xs"
  s1*11

  \part "Verse"
  s1*12
  \part "Verse"
  s1*12
  \part "Verse"
  s1*12

  \part "Chorus"
  s1*12
  \note "Harp solo"
  s1*12

  \part "Verse"
  s1*12
  \part "Verse"
  s1*12
  \part "Chorus"
  s1*12
}


% Cifrados
scoreChords = \chords {
  % c1:m f2:m g2:m7
}

% Música
trumpet = \relative c'' {
  \global
  \key g \major

  \bar "[|:"
  \repeat volta 2 {
    \tiny
    b4-. \mf b8 g a a as b
    \normalsize
    \repeat percent 3 {
      b4-. b8 g a a as b
    }

    \break
    \repeat percent 2 {
      c4-. c8 g bf bf b c
    }
    \repeat percent 2 {
      b4-. b8 g a a as b
    }

    \break
    d4-. d8 a c c cs d
    c?4-. c8 g bf bf b c
    b4-. b8 g a a as as
    b4-. r r2
  }
  \bar ":|]"

  \break
  R1*12
  R1*12
  R1*11
  \pageBreak
  r2 r4 r8 \mf b

  \repeat percent 3 {
    d d c d r4 r8 b
  }
  \break
  d d c d r4 r8 ds

  e e d? e r4 r8 ds
  e e d? e r4 r8 b
  \break
  d d c d r4 r8 b
  d d c d r4 r8 d

  fs fs e fs r4 r8 ds
  \break
  e e d? e r4 r8 b
  d d c d r4 c4-.
  d4-. r r2

  \break
  R1*12
  R1*12
  R1*11

  \pageBreak
  r2 r4 r8 \mf b
  \bar "[|:"
  \repeat volta 2 {
    \repeat percent 3 {
      d d c d r4 r8 b
    }
    \break
    d d c d r4 r8 ds

    e e d? e r4 r8 ds
    e e d? e r4 r8 b
    \break
    d d c d r4 r8 b
    d d c d r4 r8 d

    fs fs e fs r4 r8 ds
    \break
    e e d? e r4 r8 b
    d d c d r4 c4-.

  } \alternative {{
    d4-. r r2
    r2 r4 r8 b
    \bar ":|]"
  }{
    d4-. r r2
  }}

  \bar "|."
}

tenorSax = \relative c'' {
  \global
  \key g \major

  \bar "[|:"
  \repeat volta 2 {
    \tiny
    g'4-. \mf g8 d f f fs g
    \normalsize
    \repeat percent 3 {
      g4-. g8 d f f fs g
    }
    \break
    \repeat percent 2 {
      g4-. g8 e g g fs? g
    }
    \repeat percent 2 {
      g4-. g8 d f f fs g
    }
    \break
    a4-. a8 fs a a gs a
    g4-. g8 e g g fs? g
    g4-. g8 d f f fs fs
    g4-. r r2
  }
  \bar ":|]"

  R1*12
  R1*12
  R1*11
  r2 r4 r8 \mf d

  \repeat percent 3 {
    g g f g r4 r8 d
  }
  g g f g r4 r8 fs

  g g f? g r4 r8 fs
  g g f? g r4 r8 d
  g g f g r4 r8 d
  g g f g r4 r8 g

  a a gs a r4 r8 fs
  g? g f? g r4 r8 d
  g g f g r4 d4-.
  g4-. r r2

  R1*12
  R1*12
  R1*11

  r2 r4 r8 \mf d
  \bar "[|:"
  \repeat volta 2 {
    \repeat percent 3 {
      g g f g r4 r8 d
    }
    g g f g r4 r8 fs

    g g f? g r4 r8 fs
    g g f? g r4 r8 d
    g g f g r4 r8 d
    g g f g r4 r8 g

    a a gs a r4 r8 fs
    g? g f? g r4 r8 d
    g g f g r4 f4-.

  } \alternative {{
    g4-. r r2
    r2 r4 r8 d
    \bar ":|]"
  }{
    g4-. r r2
  }}

  \bar "|."
}

trombone = \relative c' {
  \global
  \key g \major
  \clef bass

  \bar "[|:"
  \repeat volta 2 {
    \tiny
    g4-. \mf g8 d f f fs g
    \normalsize
    \repeat percent 3 {
      g4-. g8 d f f fs g
    }
    \break
    \repeat percent 2 {
      g4-. g8 e g g fs? g
    }
    \repeat percent 2 {
      g4-. g8 d f f fs g
    }
    \break
    a4-. a8 fs a a gs a
    g4-. g8 e g g fs? g
    g4-. g8 d f f fs fs
    g4-. r r2
  }
  \bar ":|]"

  R1*12
  R1*12
  R1*11
  r2 r4 r8 \mf g

  \repeat percent 3 {
    b b a b r4 r8 g
  }
  b b a b r4 r8 b

  c c bf c r4 r8 b
  c c bf c r4 r8 g
  b b a b r4 r8 g
  b b a b r4 r8 b

  d d cs d r4 r8 b
  c? c bf c r4 r8 g
  b b a b r4 a4-.
  b4-. r r2

  R1*12
  R1*12
  R1*11

  r2 r4 r8 \mf g
  \bar "[|:"
  \repeat volta 2 {
    \repeat percent 3 {
      b b a b r4 r8 g
    }
    b b a b r4 r8 b

    c c bf c r4 r8 b
    c c bf c r4 r8 g
    b b a b r4 r8 g
    b b a b r4 r8 b

    d d cs d r4 r8 b
    c? c bf c r4 r8 g
    b b a b r4 a4-.

  } \alternative {{
    b4-. r r2
    r2 r4 r8 g
    \bar ":|]"
  }{
    b4-. r r2
  }}

  \bar "|."
}

% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\book {
  \paper { 
    % Descomentar para score
    % #(set-paper-size "a4" 'landscape)
    % indent = 20
    % systems-per-page = 2
  }

  \score {
    % Descomentar para midi
    % \midi { \tempo 4 = 110 }

    % NO USAR layout-set-staff-size O SE ROMPE lilyjazz!!!
    \layout { }

    <<
      \scoreMarkup
      \transpose bf c' \scoreChords
      \new StaffGroup <<
        \new Staff \with 
        { instrumentName = "Trpt."}
        { \transpose bf c' \trumpet } 

        \new Staff \with 
        { instrumentName = "T Sx." \RemoveEmptyStaves }
        { \transpose bf c' \tenorSax } 

        \new Staff \with 
        { instrumentName = "Trbn." \RemoveEmptyStaves } 
        { \trombone }
      >>
    >>
  }
}
