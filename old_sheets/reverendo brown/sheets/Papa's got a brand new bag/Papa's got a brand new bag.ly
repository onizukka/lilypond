% Se debe incluir antes que lilyjazz. Si no, lilyjazz no funciona.
#(set-global-staff-size 20)

\include "main.ily"
\include "music-sheet-paper.ily"

\header {
  title = "Papa's got a brand new bag"
  instrument = "Trumpet, Tenor Sax & Trombone"
  composer = "James Brown"
  arranger = ""
  poet = "gabriel@sauros.es"
  tagline = ""
  meter =  \jazzTempoMarkup "Medium Funk" c4 "125"
}

\paper {
  first-page-number = 2
}

% Función musical global
global = {
  % Para pintar todos los números de compás debajo del pentagrama
  % \override Score.BarNumber #'break-visibility = #end-of-line-invisible
  % \override Score.BarNumber #'direction = #DOWN
  % \override Score.BarNumber #'extra-offset = #'(1.5 . 0)
  \compressFullBarRests

  \set Timing.beamExceptions = #'()
  \set Timing.baseMoment = #(ly:make-moment 1/4)
  \set Timing.beatStructure = #'(1 1 1 1) 
}

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
  % Oculta el primer pentagrama del markup
  \override Staff.VerticalAxisGroup #'remove-first = ##t
  \override Score.RehearsalMark.extra-offset = #'(0 . 2) 

  \part "Intro"
  s1*5/4

  \mark \markup \general-align #Y #DOWN {
    \column { \fontsize #-2 \musicglyph #"scripts.segno" }
    \column { "Verse" }
  }
  s1*12

  \part "Verse"
  s1*8
  \eolMark \toCoda

  s1*4

  s1*8

  \note "Tenor Sax solo ad.lib."
  s1*4
  \eolMark \dalSegnoAlCoda

  \mkup \coda
}

% Cifrados
tenorChords = \chords {

  s1*5/4

  s1*24

  s1*8

  e:9+
}

% Música
trumpet = \relative c'' {
  \global
  \key e \major

  \time 5/4
  fs2.-> \f ~ fs2

  \time 4/4

  \bar "[|:"
  \repeat volta 2 {
    \repeat percent 2 {
      e4-^ \mf r4 r4 cs16( b cs8
      b4-^) r4 r2
    }

    \break
    e4-^ r4 r4 d16( cs d8
    cs4-^) r4 r2
    e4-^ r4 r4 cs16( b cs8
    b4-^) r4 r2

    R1*3
    \break
    r4 fs'2.-> \f

    \bar "||"
    \repeat percent 2 {
      e4-^ \mf r4 r4 cs16( b cs8
      b4-^) r4 r2
    }

    \pageBreak
    e4-^ r4 r4 d16( cs d8
    cs4-^) r4 r2
    e4-^ r4 r4 cs16( b cs8
    b4-^) r4 r2

    \bar "||"
    R1*3
    \break
    r4 fs'2.-> \f

    \repeat percent 4 {
      r4 b,16( \p as b8-.) r2
      r4 b16( as b8-.) r2
    }
  }

  \break
  \bar ":|][|:"
  \repeat volta 2 {
    \repeat percent 2 {
      r4 b16( as b8-.) r2
      r4 b16( as b8-.) r2
    }
  }
  \bar ":|]"

  \break
  R1*3
  e4-^ \f r4 r2
  \bar "|."
}

tenorSax = \relative c'' {
  \global
  \key e \major

  \time 5/4
  ds2.-> \f ~ ds2

  \time 4/4
  \repeat volta 2 {
    \repeat percent 2 {
      b'4-^ \mf r4 r4 a16( g a8
      g4-^) r4 r2
    }

    g4-^ r4 r4 a16( g a8
    g4-^) r4 r2
    b4-^ r4 r4 a16( g a8
    g4-^) r4 r2

    R1*3
    r4 ds2.-> \f

    \repeat percent 2 {
      b'4-^ \mf r4 r4 a16( g a8
      g4-^) r4 r2
    }

    g4-^ r4 r4 a16( g a8
    g4-^) r4 r2
    b4-^ r4 r4 a16( g a8
    g4-^) r4 r2


    R1*3
    r4 ds2.-> \f

    \repeat percent 4 {
      r4 fs16( \p es fs8-.) r2
      r4 fs16( es fs8-.) r2
    }
  }

  \repeat volta 2 {
    \improOn
    \repeat unfold 16 { r4 }
    \improOff
  }

  R1*3
  b4-^ \f r4 r2
}

trombone = \relative c' {
  \global
  \clef bass
  \key e \major

  \time 5/4
  b2.-> \f ~ b2

  \time 4/4
  \repeat volta 2 {
    \repeat percent 2 {
      e4-^ \mf e,,-^ r4 e''16( d e8
      d4-^) r4 r2
    }

    a4-^ a,4-^ r4 fs''16( e fs8
    e4-^) r4 r2
    e4-^ e,,-^ r4 e''16( d e8
    d4-^) r4 r2

    R1*3
    r4 b2.-> \f

    \repeat percent 2 {
      e4-^ \mf e,,-^ r4 e''16( d e8
      d4-^) r4 r2
    }

    a4-^ a,4-^ r4 fs''16( e fs8
    e4-^) r4 r2
    e4-^ e,,-^ r4 e''16( d e8
    d4-^) r4 r2

    R1*3
    r4 b2.-> \f

    \repeat percent 4 {
      r4 d16( \p cs d8-.) r2
      r4 d16( cs d8-.) r2
    }
  }

  \repeat volta 2 {
    \repeat percent 2 {
      r4 d16( cs d8-.) r2
      r4 d16( cs d8-.) r2
    }
  }

  R1*3
  e4-^ \f r4 r2
}

% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\book {
  \paper { 
    % Descomentar para score
    % #(set-paper-size "a4" 'landscape)
    % indent = 20
    % systems-per-page = 4
  }

  \score {
    % Descomentar para midi
    % \midi { \tempo 4 = 110 }

    % NO USAR layout-set-staff-size O SE ROMPE lilyjazz!!!
    \layout { }

    <<
      \scoreMarkup
      % \transpose bf c' \scoreChords
      \new StaffGroup <<
        \new Staff \with 
        { instrumentName = "Trpt." }
        { \transpose bf c' \trumpet } 

        \transpose bf c' \tenorChords
        \new Staff \with 
        { instrumentName = "T Sx." }
        { \transpose bf c' \tenorSax } 

        \new Staff \with 
        { instrumentName = "Trbn." } 
        { \trombone }
      >>
    >>
  }
}
