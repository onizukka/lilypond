% Se debe incluir antes que lilyjazz. Si no, lilyjazz no funciona.
#(set-global-staff-size 20)

\include "main.ily"
\include "music-sheet-paper.ily"

\paper {
  page-count = 2
  systems-per-page = 3
  first-page-number = 2
}

\header {
  title = "It's your thing"
  instrument = "Trumpet, Tenor Sax & Trombone"
  composer = "Isley Brothers"
  arranger = "(Brother Strut lick)"
  poet = "gabriel@sauros.es"
  tagline = ""
  meter = \jazzTempoMarkup "Medium Funk" c4 "95"
}

% Función musical global
global = {
  % Para pintar todos los números de compás debajo del pentagrama
  % \override Score.BarNumber #'break-visibility = #end-of-line-invisible
  % \override Score.BarNumber #'direction = #DOWN
  % \override Score.BarNumber #'extra-offset = #'(1.5 . 0)
  \compressFullBarRests

  \set Timing.beamExceptions = #'()
  \set Timing.baseMoment = #(ly:make-moment 1/4)
  \set Timing.beatStructure = #'(1 1 1 1) 

  \key f \major
}

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
  % Oculta el primer pentagrama del markup
  \override Staff.VerticalAxisGroup #'remove-first = ##t

  % Indicación de tiempo
  % \once \override Score.RehearsalMark.self-alignment-X = #LEFT
  \override Score.RehearsalMark.extra-offset = #'(0 . 2) 
  % \mark \jazzTempoMarkup "Easy Swing" c4 "112"

  \part "Intro"
  s1*4

  \mark \markup \general-align #Y #DOWN {
    \column { \fontsize #-1 \musicglyph #"scripts.segno" }
    \column { "Verse" \vspace #.25 \underline "(Repeat 4 xs)" }
  }
  s1*3

  \mark \markup \line { "Chorus" \underline "(Repeat 7 xs)" }
  s1*2
  \eolMark \toCoda

  \mark \markup \line { "Verse " \underline "(Repeat 4 xs)" }
  s1*3

  \part "Chorus"
  \mark \markup \eyeglasses
  s1*4

  \eolMark \dalSegnoAlCoda
  \mark \markup \general-align #Y #DOWN {
    \column {\fontsize #-1 \musicglyph #"scripts.coda" }
    \column { "Verse" \vspace #.25 \underline "(Repeat 8 xs)" }
  }
  s1*2
}

% Cifrados
scoreChords = \chords {
  % c1:m f2:m g2:m7
}

% Música
trumpet = \relative c'' {
  \global

  R1*4

  \break
  \bar "[|:"
  \repeat volta 4 {
    r2 r8 \mf a16( gs a gs a8-^)
  }
  \alternative {
    {
      R1
      \bar ":|]"
    } {
      r4 r8. c16 d f8---^ af16---^ ~ af af( f8-^)
    }
  }

  \break
  \bar "[|:"
  \repeat volta 7 {
    a,4->\sfp\< r8\! f8->\sfp \<~ f4 ef16( f-^\!) r8
  }
  \bar ":|]"
  \times 2/3 {f8-.\mf\<[ a-. bf-.]} \times 2/3 {b-.[ c-. ef-.]} f4-^\! r

  \pageBreak
  \defineBarLine "[|:-||" #'("||" "[|:" "")
  \bar "[|:-||"
  \repeat volta 4 {
    r2 r8 \mf a,16( gs a gs a8-^)
  }
  \alternative {
    {
      R1
      \bar ":|]"
    } {
      r4 r8. c16 d f8---^ af16---^ ~ af af( f8-^)
    }
  }

  \break
  \bar "[|:"
  \repeat volta 2 {
    r4 c16(\f d) c( d) f2->
    r4 c16( d) c( d) f,2->
    r4 c'16( d) c( d) a'2->
    r4 bf16( c) bf( c) d4-> bf16 bf8-. af16---^
  }

  \break
  \bar ":|][|:"
  \repeat volta 8 {
    r2 r8 \mf a,16( gs a gs a8-^)
  } \alternative {{
    r4 r8. c16 d f8---^ af16---^ ~ af af( f8-^)
    \bar ":|]"
  }{
    r2 \times 2/3 {f,8-.\mf\<[ a-. bf-.]} \times 2/3 {b-.[ c-. ef-.]} 
  }}

  f4-^\! r r2
  \bar "|."
}

tenorSax = \relative c'' {
  \global

  R1*4

  \repeat volta 4 {
    r2 r8 \mf c'16( b c b c8-^)
  }
  \alternative {
    {
      r4 r8. c,16 ef f8---^ af16---^ ~ af af( f8-^)
    } {
      r4 r8. c16 ef f8---^ af16---^ ~ af af( f8-^)
    }
  }

  \repeat volta 7 {
    c4->\sfp\< r8\! f8->\sfp \<~ f4 ef16( f-^\!) r8
  }
  \times 2/3 {f,8-.\mf\<[ a-. bf-.]} \times 2/3 {b-.[ c-. ef-.]} f4-^\! r

  \repeat volta 4 {
    r2 r8 \mf c'16( b c b c8-^)
  }
  \alternative {
    {
      r4 r8. c,16 ef f8---^ af16---^ ~ af af( f8-^)
    } {
      r4 r8. c16 ef f8---^ af16---^ ~ af af( f8-^)
    }
  }

  \repeat volta 2 {
    r4 c16(\f d) c( d) f2->
    r4 c16( d) c( d) f,2->
    r4 c'16( d) c( d) a'2->
    r4 bf16( c) bf( c) d4-> bf16 bf8-. af16---^
  }

  \repeat volta 2 {
    r2 r8 \mf c16( b c b c8-^)
  }
  \alternative {
    {
      r4 r8. c,16 ef f8---^ af16---^ ~ af af( f8-^)
    } {
      r2 \times 2/3 {f,8-.\mf\<[ a-. bf-.]} \times 2/3 {b-.[ c-. ef-.]} 
    }
  }

  f4-^\! r r2
}

trombone = \relative c' {
  \global
  \clef bass

  R1*4

  \repeat volta 4 {
    r2 r8 \mf ef16( d ef d ef8-^)
  }
  \alternative {
    {
      r4 r8. c,16 ef f8---^ af16---^ ~ af af( f8-^)
    } {
      r4 r8. c16 ef f8---^ af16---^ ~ af af( f8-^)
    }
  }

  \repeat volta 7 {
    f,4->\sfp\< r8\! f'8->\sfp \<~ f4 ef16( f-^\!) r8
  }
  \times 2/3 {f8-.\mf\<[ a-. bf-.]} \times 2/3 {b-.[ c-. ef-.]} f4-^\! r

  \repeat volta 4 {
    r2 r8 \mf ef16( d ef d ef8-^)
  }
  \alternative {
    {
      r4 r8. c,16 ef f8---^ af16---^ ~ af af( f8-^)
    } {
      r4 r8. c16 ef f8---^ af16---^ ~ af af( f8-^)
    }
  }

  \repeat volta 2 {
    f,2-> r4 f16 f8-. f16
    f2-> r4 f16 f8-. f16
    f2-> r4 f16 f8-. f16
    f2-> r4 f16 f8-. f16---^
  }

  \repeat volta 2 {
    r2 r8 \mf ef''16( d ef d ef8-^)
  } \alternative {{
    r4 r8. c,16 ef f8---^ af16---^ ~ af af( f8-^)
  }{

    r2 \times 2/3 {f8-.\mf\<[ a-. bf-.]} \times 2/3 {b-.[ c-. ef-.]} 
  }}

  f4-^\! r r2
}

% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\book {
  \paper { 
    % Descomentar para score
    % #(set-paper-size "a4" 'landscape)
    % indent = 20
  }

  \score {
    % Descomentar para midi
    % \midi { \tempo 4 = 110 }

    % NO USAR layout-set-staff-size O SE ROMPE lilyjazz!!!
    \layout { }

    <<
      \scoreMarkup
      \transpose bf c' \scoreChords
      \new StaffGroup <<
        \new Staff \with 
        { instrumentName = "Trpt."}
        { \transpose bf c' \trumpet } 

        \new Staff \with 
        { instrumentName = "T Sx." \RemoveEmptyStaves }
        { \transpose bf c' \tenorSax } 

        \new Staff \with 
        { instrumentName = "Trbn." \RemoveEmptyStaves } 
        { \trombone }
      >>
    >>
  }
}
