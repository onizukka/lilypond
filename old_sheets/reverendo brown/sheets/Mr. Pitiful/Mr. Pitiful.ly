\version "2.18.2"
\language "english"

% Se debe incluir antes que lilyjazz. Si no, lilyjazz no funciona.
#(set-global-staff-size 19)

\include "main.ily"

\paper {
	indent = 10
}

\header {
	title = "Mr. Pitiful"
	instrument = "Trumpet, Tenor Sax & Trombone"
	composer = "Otis Reeding"
	arranger = "Transc: gabriel@sauros.es"
	poet = ""
	tagline = "Edition: gabriel@sauros.es"
}

% Función musical global
global = {
	% Para pintar todos los números de compás debajo del pentagrama
	% \override Score.BarNumber #'break-visibility = #end-of-line-invisible
	% \override Score.BarNumber #'direction = #DOWN
	% \override Score.BarNumber #'extra-offset = #'(1.5 . 0)
	\compressFullBarRests

	\set Timing.beamExceptions = #'()
	\set Timing.baseMoment = #(ly:make-moment 1/4)
	\set Timing.beatStructure = #'(1 1 1 1) 
}

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
	% Oculta el primer pentagrama del markup
	\override Staff.VerticalAxisGroup #'remove-first = ##t

	% Indicación de tiempo
	% \once \override Score.RehearsalMark.self-alignment-X = #LEFT
	% \once \override Score.RehearsalMark.extra-offset = #'(0 . 3) 
	% \mark \jazzTempoMarkup "Easy Swing" c4 "112"

	s2

	s1*20

	s1*28
	\mkup "(Repeat 3xs)"
}

% Cifrados
scoreChords = \chords {
	% c1:m f2:m g2:m7
}

% Música
trumpet = \relative c'' {
	\global
	\key c \major

	\partial 2
	e,8-.\f g4-> c8~->

	\bar "||"
	c1~ 
	c4 r a8-. ef4-> c8~-> 
	c1(
	c'4)-. r r2

	\bar "||"
	R1*15
	\break
	r2 r4 r8\mf b(

	\bar "[|:"
	\repeat volta 2 {
		c4)-. r r r8 c\bendAfter #-3
		r2 r4 r8 b(
		c4)-. r r r8 c\bendAfter #-3
		r2 r4 r8 b(

		c4)-. r r r8 c\bendAfter #-3
		r2 r4 r8 as(
		b4)-. r r r8 b\bendAfter #-3
		r2 r4 r8 b(

		c4)-. r r r8 c\bendAfter #-3
		r2 r4 r8 gs(
		\break
		a4)-. r r r8 a\bendAfter #-3
		r2 r4 r8 b(

		c4)-. r r r8 b\bendAfter #-3
		r2 r4 r8 b(
		c4)-. r r2
	}

	\alternative {
		{
			r8 g16( f g f g f \repeat tremolo 4 { g f }
			g4-> e4)-. r8 g16( f g f g f 
			g4-> e4)-. r8 g16( f g f g f 
			\repeat tremolo 4 { g f } \repeat tremolo 4 { g f) }

			g( f e8)~-> e4 r2
			R1*7
			\bar ":|]"
		}

		{
			\break
			r2\f e8-. g4-> c8~->
		}
	}

	\bar "[|:"
	\repeat volta 4 {
		c1~ 
		c4 r a8-. ef4-> c8~-> 
		c1~
		c4 r e8-. g4-> c8~->
	}
	\bar ":|]"

	\break
	c1~ 
	c4 r a8-. ef4-> c8~-> 
	c1(
	c'4)-. r r2
	\bar "||"
}

tenorSax = \relative c'' {
	\global
	\key c \major

	\partial 2
	c8-.\f e4-> g8~->

	g1~ 
	g4 r ef8-. c4-> g8~-> 
	g1(
	g'4)-. r r2

	R1*15
	r2 r4 r8\mf fs(

	\repeat volta 2 {
		g4)-. r r r8 g\bendAfter #-3
		r2 r4 r8 fs(
		g4)-. r r r8 g\bendAfter #-3
		r2 r4 r8 fs(

		g4)-. r r r8 g\bendAfter #-3
		r2 r4 r8 e(
		f?4)-. r r r8 f\bendAfter #-3
		r2 r4 r8 fs(

		g4)-. r r r8 e\bendAfter #-3
		r2 r4 r8 e(
		f4)-. r r r8 f\bendAfter #-3
		r2 r4 r8 fs(

		g4)-. r r r8 f?\bendAfter #-3
		r2 r4 r8 fs(
		g4)-. r r2
	}

	\alternative {
		{
			r8 e16( d e d e d \repeat tremolo 4 { e d }
			e4-> c4)-. r8 e16( d e d e d 
			e4-> c4)-. r8 e16( d e d e d 
			\repeat tremolo 4 { e d } \repeat tremolo 4 { e d) }

			e( d c8)~-> c4 r2
			R1*7
		}

		{
			r2\f c8-. e4-> g8~->
		}
	}

	\repeat volta 4 {
		g1~ 
		g4 r ef8-. c4-> g8~-> 
		g1~
		g4 r c8-. e?4-> g8~->
	}

	g1~ 
	g4 r ef8-. c4-> g8~-> 
	g1(
	g'4)-. r r2
}

trombone = \relative c' {
	\global
	\key c \major
	\clef bass

	\partial 2
	e,8-.\f g4-> c8~->

	c1~ 
	c4 r a8-. ef4-> c8~-> 
	c1(
	c'4)-. r r2

	R1*15
	r2 r4 r8\mf ds(

	\repeat volta 2 {
		e4)-. r r r8 e\bendAfter #-3
		r2 r4 r8 ds(
		e4)-. r r r8 e\bendAfter #-3
		r2 r4 r8 ds(

		e4)-. r r r8 e\bendAfter #-3
		r2 r4 r8 cs(
		d4)-. r r r8 g,\bendAfter #-3
		r2 r4 r8 ds'(

		e4)-. r r r8 bf\bendAfter #-3
		r2 r4 r8 d(
		ef4)-. r r r8 ef\bendAfter #-3
		r2 r4 r8 ds(

		e4)-. r r r8 d\bendAfter #-3
		r2 r4 r8 ds(
		e4)-. r r2
	}

	\alternative {
		{
			R1*12
		}

		{
			r2\f e,8-. g4-> c8~->
		}
	}

	\repeat volta 4 {
		c1~ 
		c4 r a8-. ef4-> c8~-> 
		c1~
		c4 r e8-. g4-> c8~->
	}

	c1~ 
	c4 r a8-. ef4-> c8~-> 
	c1(
	c'4)-. r r2
}

% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\book {
	\paper { 
		% Descomentar para score
		% #(set-paper-size "a4" 'landscape)
		% indent = 20
		% systems-per-page = 2
	}
	
	\score {
		% Descomentar para midi
		% \midi { \tempo 4 = 110 }
		
		% NO USAR layout-set-staff-size O SE ROMPE lilyjazz!!!
		\layout { }
		
		<<
			\scoreMarkup
			\transpose bf c' \scoreChords
			\new StaffGroup <<
				\new Staff \with 
				{ instrumentName = "Trpt."}
				{ \transpose bf c' \trumpet } 

				\new Staff \with 
				{ instrumentName = "T Sx." }
				{ \transpose bf c' \tenorSax } 

				\new Staff \with 
				{ instrumentName = "Trbn." } 
				{ \trombone }
			>>
		>>
	}
}
