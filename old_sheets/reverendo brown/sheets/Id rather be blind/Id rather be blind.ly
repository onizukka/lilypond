\version "2.18.2"
\language "english"

% Se debe incluir antes que lilyjazz. Si no, lilyjazz no funciona.
#(set-global-staff-size 19)

\include "main.ily"

\paper {
  indent = 10
}

\header {
  title = "I'd rather be blind"
  instrument = "Trumpet, Tenor Sax & Trombone"
  composer = "Etta James"
  arranger = "Transc: gabriel@sauros.es"
  poet = ""
  tagline = "Edition: gabriel@sauros.es"
}

% Función musical global
global = {
  % Para pintar todos los números de compás debajo del pentagrama
  % \override Score.BarNumber #'break-visibility = #end-of-line-invisible
  % \override Score.BarNumber #'direction = #DOWN
  % \override Score.BarNumber #'extra-offset = #'(1.5 . 0)
  \compressFullBarRests

  \set Timing.beamExceptions = #'()
  \set Timing.baseMoment = #(ly:make-moment 1/4)
  \set Timing.beatStructure = #'(1 1 1 1) 
}

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
  % Oculta el primer pentagrama del markup
  \override Staff.VerticalAxisGroup #'remove-first = ##t

  % Indicación de tiempo
  % \once \override Score.RehearsalMark.self-alignment-X = #LEFT
  % \once \override Score.RehearsalMark.extra-offset = #'(0 . 3) 
  % \mark \jazzTempoMarkup "Easy Swing" c4 "112"
  % Intro, Verse, Bridge, Build, Refrain

  \part "Intro (guitar alone)"
  s1*4*12/8

  \part "Intro (full band)"
  s1*4*12/8

  \part "Lead"
  s1*4*12/8
  s1*4*12/8
  s1*4*12/8

  s1*4*12/8
  s1*4*12/8
  s1*4*12/8

  s1*4*12/8
  s1*4*12/8
  s1*4*12/8
  s1*4*12/8

  s1*4*12/8
  s1*4*12/8

  \mkup "(Repeat ad.lib.)"
  s1*5*12/8

  s1*12/8
  \once \override Score.RehearsalMark.extra-offset = #'(45 . 0) 
  \mkup "(On cue)"
  s1*12/8
  s1*4*12/8
}

% Cifrados
scoreChords = \chords {
  % c1:m f2:m g2:m7
}

% Música
trumpet = \relative c'' {
  \global
  \key a \major
  \time 12/8

  %verse
  R1*4*12/8
  R1*4*12/8
  \bar "||"

  %verse
  R1*4*12/8
  R1*4*12/8
  R1*4*12/8

  \break
  R1*2*12/8
  a2. \p ~ a4. fs4. \<
  e2. \mf ~ e4. \> r4. \!

  \break
  R1*4*12/8
  a1. \p \<
  a2. \mf ~ a4. r
  a2. ~ a4. fs
  e2. \> ~ e4. r \!

  \break
  R1*2*12/8
  a2. \p ~ a4. fs4. \<
  e2. \> ~ e4. r \!

  \break
  R1*4*12/8
  R1*4*12/8
  R1*4*12/8
  r2. a4.-> \bendAfter #-5 r4.
  R1*3*12/8

  \break
  a1. \p \<
  a2. \mf ~ a4. r
  a2. ~ a4. fs
  e2. \> ~ e4. r \!

  \break
  \bar "[|:"
  \repeat volta 2 {
    R1*3*12/8
  }
  \alternative {
    { \set Score.repeatCommands = #(list (list 'volta "ad.lib."))
      R1*12/8 }
    { \set Score.repeatCommands = #'((volta #f) (volta "last x"))
      R1*12/8 }
  }
  \bar ":|]"

  \break
  a1. \p \<
  a2. \mf \fermata r4. \fermata r8 \f \times 2/3 {e8[ fs16]} \times 2/3 {a8[ fs16]}

  \break
  a2. ~ a4. r8 \times 2/3 {e8[ fs16]} \times 2/3 {a8[ fs16]}
  b2. ~ b4. r8 fs \times 2/3 {a8[ fs16]}
  b2. ~ b4. cs8-> a-> fs-> 
  a1. \fermata
  \bar "||"
}

tenorSax = \relative c' {
  \global
  \key a \major
  \time 12/8

  %verse
  R1*4*12/8
  R1*4*12/8

  %verse
  R1*4*12/8
  R1*4*12/8
  R1*4*12/8

  R1*12/8
  r2. r4. r8 \p fs a 
  b2. ~ b4. e,4. \<
  a2. \mf ~ a4. \> r4. \!

  R1*4*12/8
  a1. \p \<
  b2. \mf ~ b4. r
  b2. ~ b4. e,
  a2. \> ~ a4. r \!

  R1*12/8
  r2. r4. r8 \p fs a 
  b2. ~ b4. e,4. \<
  a2. \mf ~ a4. \> r4. \!

  R1*4*12/8
  R1*4*12/8
  R1*4*12/8

  r2. a4.-> \f \bendAfter #-5 r4.
  R1*2*12/8
  r2. r4. r8 \p \times 2/3 {e8[ fs16]} \times 2/3 {a8[ fs16]}

  a1. \<
  b2. \mf ~ b4. r
  b2. ~ b4. e,
  a2. \> ~ a4. r \!

  \repeat volta 2 {
    R1*3*12/8
  }
  \alternative {
    { R1*12/8 }
    { r2. r4. r8 \p \times 2/3 {e8[ fs16]} \times 2/3 {a8[ fs16]} }
  }

  a1. \p \<
  b2. \mf \fermata r4. \fermata r8 \f \times 2/3 {e8[ fs16]} \times 2/3 {a8[ fs16]}

  a2. ~ a4. r8 \times 2/3 {e8[ fs16]} \times 2/3 {a8[ fs16]}
  b2. ~ b4. r8 fs \times 2/3 {a8[ fs16]}
  b2. ~ b4. cs8-> a-> fs-> 
  a1. \fermata
}

trombone = \relative c' {
  \global
  \clef bass
  \key a \major
  \time 12/8

  %verse
  R1*4*12/8
  R1*4*12/8

  %verse
  R1*4*12/8
  R1*4*12/8
  R1*4*12/8

  R1*2*12/8
  d2. \p ~ d4. d4. \<
  cs2. \mf ~ cs4. \> r4. \!

  R1*4*12/8
  cs1. \p \<
  d2. \mf ~ d4. r
  d2. ~ d4. d
  cs2. \> ~ cs4. r \!

  R1*2*12/8
  d2. \p ~ d4. d4. \<
  cs2. \mf ~ cs4. \> r4. \!

  R1*4*12/8
  R1*4*12/8
  R1*4*12/8
  r2. cs4.-> \bendAfter #-5 r4.
  R1*3*12/8

  cs1. \p \<
  d2. \mf ~ d4. r
  d2. ~ d4. d
  cs2. \> ~ cs4. r \!

  \repeat volta 2 {
    R1*3*12/8
  }
  \alternative {
    { R1*12/8 }
    { R1*12/8 }
  }

  cs1. \p \<
  d2. \mf \fermata r4. \fermata r8 \f \times 2/3 {e,8[ fs16]} \times 2/3 {a8[ fs16]}

  a2. ~ a4. r8 \times 2/3 {e8[ fs16]} \times 2/3 {a8[ fs16]}
  b2. ~ b4. r8 fs \times 2/3 {a8[ fs16]}
  b2. ~ b4. cs8-> a-> fs-> 
  cs'1. \fermata
}

% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\book {
  \paper { 
    % Descomentar para score
    % #(set-paper-size "a4" 'landscape)
    % indent = 20
    % systems-per-page = 2
  }

  \score {
    % Descomentar para midi
    % \midi { \tempo 4 = 110 }

    % NO USAR layout-set-staff-size O SE ROMPE lilyjazz!!!
    \layout { }

    <<
      \scoreMarkup
      \transpose bf c' \scoreChords
      \new StaffGroup <<
        \new Staff \with 
        { instrumentName = "Trpt." }
        { \transpose bf c' \trumpet } 

        \new Staff \with 
        { instrumentName = "T Sx." \RemoveEmptyStaves }
        { \transpose bf c' \tenorSax } 

        \new Staff \with 
        { instrumentName = "Trbn." \RemoveEmptyStaves } 
        { \trombone }
      >>
    >>
  }
}
