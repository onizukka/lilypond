\version "2.18.2"
\language "english"

% Se debe incluir antes que lilyjazz. Si no, lilyjazz no funciona.
#(set-global-staff-size 20)

\include "main.ily"
\include "music-sheet-paper.ily"

\paper {
  indent = 10
}

\header {
  title = "Bring it on home to me"
  instrument = "Trumpet, Tenor Sax & Trombone"
  composer = "Otis reeding"
  arranger = ""
  poet = "gabriel@sauros.es"
  tagline = ""
  meter = \jazzTempoMarkup "Slow soul" c4. "70"
}

% Función musical global
global = {
  % Para pintar todos los números de compás debajo del pentagrama
  % \override Score.BarNumber #'break-visibility = #end-of-line-invisible
  % \override Score.BarNumber #'direction = #DOWN
  % \override Score.BarNumber #'extra-offset = #'(1.5 . 0)
  \compressEmptyMeasures

  \set Timing.beamExceptions = #'()
  \set Timing.baseMoment = #(ly:make-moment 1/4)
  \set Timing.beatStructure = #'(1 1 1 1) 
}

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
  % Oculta el primer pentagrama del markup
  \override Staff.VerticalAxisGroup #'remove-first = ##t

  % Indicación de tiempo
  % \once \override Score.RehearsalMark.self-alignment-X = #LEFT
  % \once \override Score.RehearsalMark.extra-offset = #'(0 . 3) 
  s1.

  \bar "||"
  s1.*4

  \part "Verse"
  \bar "||"
  s1.*8

  \part "Verse"
  \bar "||"
  s1.*8

  \break
  \part "Verse"
  \bar "||"
  s1.*8

  \break
  \note "Guitar solo"
  \bar "||"
  s1.*8

  \break
  \note "Harp. solo, play as backgrounds"
  \bar "||"
  s1.*4
  \break
  s1.*4

  \break
  \part "Verse"
  \bar "||"
  s1.*8

  \part "Verse"
  \bar "||"
  s1.*8

  \break
  \part "Verse"
  \bar "||"
  s1.*4
  \break
  s1.*4
  \break
  s1.*3
  \break
  s1.*3

  \bar "|."
}


% Cifrados
scoreChords = \chords {
  % c1:m f2:m g2:m7
}

% Música
trumpet = \relative c' {
  \global
  \key a \major
  \time 12/8

  r2. r4. e8 \mf fs a 
  cs2. ~ cs4. ~ cs8 cs a 
  b2. \> r4. \! e,8 \mf fs a
  cs2. ~ cs4. ~ cs8 a fs 
  e2. \> r2. \!

  R1*8*12/8
  R1*8*12/8

  r2. a4 \mf -- -> a8 -^ r4.
  r2. gs8(-> a gs8 -> ~ gs) e fs
  a2. b2. -> \<
  r4 \f a8( a8 a8 b a8 ) -^ r4 r4.

  r4. a4.-^ \mf r4 gs8-^ r4.
  fs2. ( e2. ) \>
  r8 \mf e fs a cs4 -^ r8 e, fs a c4 -^
  r8 e, fs cs'?( a) fs e2. \>

  R1*8*12/8 \!

  r4. cs4. \p cs4-- cs8-. r4.
  r4. b4. b4-- b8-. r4.
  cs2. fs4-- a8-. r4.
  r4. r4 a,8 c( a) b a-. r4

  a'2.( \p gs
  fs2. gs) \>
  R1. \!
  r4 a,8 c( a) b-> ~ b4. \> r4. \!

  R1*8*12/8
  R1*8*12/8

  r2. a'4 \mf -- -> a8 -^ r4.
  r2. gs8(-> a gs8 -> ~ gs) e fs
  a2. b2. -> \<
  r4 \f a8( a8 a8 b a8) -^ r4 r4.

  r4. a4.-^ \mf r4 gs8-^ r4.
  fs2. ( e2. ) \>
  r8 \mf e fs a cs4 -^ r8 e, fs a c4 -^
  r8 e, fs a cs?4 -^ r8 e, fs a fs c' -^

  r8 e, fs a cs?4 -^ r8 e, fs a c4 -^
  r8 e, fs a cs?4 -^ r8 e, fs a fs c'
  a2. \> r2. \!
  R1.

  r8 e' fs a fs c' a g e a,4.-^
  a'1. \fermata
}

tenorSax = \relative c'' {
  \global
  \key a \major
  \time 12/8

  r2. r4. e8 \mf fs a 
  e2. ~ e4. ~ e8 cs a 
  d2. \> r4. \! e8 \mf fs d
  e2. ~ e4. ~ e8 a, cs
  d2. \> r2. \!

  R1*8*12/8
  R1*8*12/8

  r2. e4 \mf -- -> e8 -^ r4.
  r2. e4 -- -> e8 -> ~ e cs d
  fs2. g2. -> \<
  r4 \f fs8( fs8 fs8 g fs8 ) -^ r4 r4.

  r4. fs4.-^ \mf r4 e8-^ r4.
  c2. ( d2. ) \>
  r8 \mf e, fs a cs4 -^ r8 e, fs a c4 -^
  r8 e, fs cs'?( a) fs e2. \>

  R1*8*12/8 \!

  r4. fs4. \p fs4-- fs8-. r4.
  r4. gs4. gs4-- gs8-. r4.
  g2. cs4-- b8-. r4.
  r4. r4 a8 c( a) b a-. r4

  fs'2.( \p e
  c2. d) \>
  R1. \!
  r4 a,8 c( a) b-> ~ b4. \> r4. \!

  R1*8*12/8
  R1*8*12/8

  r2. e'4 \mf -- -> e8 -^ r4.
  r2. e4 -- -> e8 -> ~ e cs d
  fs2. g2. -> \<
  r4 \f fs8( fs8 fs8 g fs8 ) -^ r4 r4.

  r4. fs4.-^ \mf r4 e8-^ r4.
  c2. ( d2. ) \>
  r8 \mf e, fs a cs4 -^ r8 e, fs a c4 -^
  r8 e, fs a cs?4 -^ r8 e, fs a fs c' -^

  r8 e, fs a cs?4 -^ r8 e, fs a c4 -^
  r8 e, fs a cs?4 -^ r8 e, fs a fs c'
  a2. \> r2. \!
  R1.

  r8 e' fs a fs c' b a gs fs4.-^
  cs'?1. \fermata
}

trombone = \relative c {
  \global
  \clef bass
  \key a \major
  \time 12/8

  r2. r4. e8 \mf fs a 
  cs2. ~ cs4. ~ cs8 cs a 
  b2. \> r4. \! e,8 \mf fs a
  cs2. ~ cs4. ~ cs8 a fs 
  e2. \> r2. \!

  R1*8*12/8
  R1*8*12/8

  r2. cs'4 \mf -- -> cs8 -^ r4.
  r2. b4 -- -> b8 -> ~ b e, fs
  cs'2. e2. -> \<
  r4 \f d8( d8 d8 d d8) -^ r4 r4.

  r4. cs4.-^ \mf r4 b8-^ r4.
  a2. ( gs2. ) \>
  r8 \mf e fs a cs4 -^ r8 e, fs a c4 -^
  r8 e, fs cs'?( a) fs e2. \>

  R1*8*12/8 \!

  r4. e4. \p e4-- e8-. r4.
  r4. fs4. fs4-- fs8-. r4.
  e2. g4-- a8-. r4.
  r4. r4 a8 c( a) b a-. r4

  cs?2.( \p b 
  a2. b) \>
  R1. \!
  r4 a,8 c( a) b-> ~ b4. \> r4. \!

  R1*8*12/8
  R1*8*12/8

  r2. cs'4 \mf -- -> cs8 -^ r4.
  r2. b4 -- -> b8 -> ~ b e, fs
  cs'2. e2. -> \<
  r4 \f d8( d8 d8 d d8) -^ r4 r4.

  r4. cs4.-^ \mf r4 b8-^ r4.
  a2. ( gs2. ) \>
  r8 \mf e fs a cs4 -^ r8 e, fs a c4 -^
  r8 e, fs a cs?4 -^ r8 e, fs a fs c' -^

  r8 e, fs a cs?4 -^ r8 e, fs a c4 -^
  r8 e, fs a cs?4 -^ r8 e, fs a fs c'
  a2. \> r2. \!
  R1.

  r8 e fs d a' fs ds b' c cs4.-^
  fs1. \fermata
}

% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\book {
  \paper { 
    % Descomentar para score
    % #(set-paper-size "a4" 'landscape)
    % indent = 20
    % systems-per-page = 2
  }

  \score {
    % Descomentar para midi
    % \midi { \tempo 4 = 110 }

    % NO USAR layout-set-staff-size O SE ROMPE lilyjazz!!!
    \layout { }

    <<
      \scoreMarkup
      \transpose bf c' \scoreChords
      \new StaffGroup <<
        \new Staff \with 
        { instrumentName = "Trpt."}
        { \transpose bf c' \trumpet } 

        \new Staff \with 
        { instrumentName = "T Sx." \RemoveEmptyStaves }
        { \transpose bf c' \tenorSax } 

        \new Staff \with 
        { instrumentName = "Trbn." \RemoveEmptyStaves } 
        { \trombone }
      >>
    >>
  }
}
