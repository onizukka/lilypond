% Se debe incluir antes que lilyjazz. Si no, lilyjazz no funciona.
#(set-global-staff-size 19)

\include "main.ily"
\include "music-sheet-paper.ily"

\paper {
  first-page-number = 2
}

\header {
  title = "Vehicle"
  instrument = "Trumpet, Tenor Sax & Trombone"
  composer = "The ides of march"
  arranger = ""
  poet = "gabriel@sauros.es"
  tagline = ""
  meter = \jazzTempoMarkup "Funk Rock" c4 "105"
}

% Función musical global
global = {
  % Para pintar todos los números de compás debajo del pentagrama
  % \override Score.BarNumber #'break-visibility = #end-of-line-invisible
  % \override Score.BarNumber #'direction = #DOWN
  % \override Score.BarNumber #'extra-offset = #'(1.5 . 0)
  \compressFullBarRests

  \set Timing.beamExceptions = #'()
  \set Timing.baseMoment = #(ly:make-moment 1/4)
  \set Timing.beatStructure = #'(1 1 1 1) 
}

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
  % Oculta el primer pentagrama del markup
  \override Staff.VerticalAxisGroup #'remove-first = ##t

  % Indicación de tiempo
  % \once \override Score.RehearsalMark.self-alignment-X = #LEFT
  \override Score.RehearsalMark.extra-offset = #'(0 . 2) 

  s1
  s2
  s1*2

  \part "Soli"
  s1*4

  \part "Verse"
  s1*8
  \part "Chorus"
  s1*3
  \part "Soli"
  s1*4

  \mkup "Guitar solo"
  s1*11

  \part "Soli"
  s1*4
  \part "Verse"
  s1*8
  \part "Chorus"
  s1*3

  \part "Soli"
  s1*4
  \part "Chorus"
  s1*3
}


% Cifrados
scoreChords = \chords {
  % c1:m f2:m g2:m7
}

% Música
trumpet = \relative c' {
  \global
  \key ef \minor

  r2 ef16 \mf gf8---. bf16---. r gf af8->~
  \time 2/4
  af2 \<

  \bar "||"
  \time 4/4
  ef'8---> \f r r4 r2
  R1

  ef,2 \mf ef16 gf8---. bf16---. r gf af8->~
  af2. r4
  bf2 bf16 df8---. f16---. r df ef8->~
  ef1 \>

  \break
  \bar "[|:"
  \repeat volta 2 {
    ef,4-^ \f r r2
    r2 r8 \mf bf'16 \( c df c bf f'->~
    f2.~ f16 af16-> \f \bendAfter #-3 \) r8
    r2 r8 \mf bf,16 \( c df c bf8

    gf'1 
    f2. \) r4
    \pageBreak
    ef1~
    ef2 \> d

    r4 \! ef8---> \< r r4 f8---> r 
    r4 gf8---> r r4 af4-> \(
    ef8---> \) \f r r4 r2

    \break
    ef,2 \mf ef16 gf8---. bf16---. r gf af8->~
    af2. r4
    bf2 bf16 df8---. f16---. r df ef8->~
    ef1 \>
  } 
  \bar ":|]"

  \break
  R1*4 \!

  \pageBreak
  gf1 \mf \(
  af2 r
  ef1 \glissando \<
  bf'2. \) \f r4

  r4 ef,8---> \mf \< r r4 f8---> r 
  r4 gf8---> r r4 af4-> \(
  ef8---> \) \f r r4 r2

  ef,16 \mf d ef d ef d ef d ef16 gf8---. bf16---. r gf af8->~
  af2. r4
  bf16 a bf a bf a bf a bf16 df8---. f16---. r df ef8->~
  ef1 \>

  \break
  \bar "||"
  ef,4-^ \f r r2
  r2 r8 \mf bf'16 \( c df c bf f'->~
  f2.~ f16 af16-> \f \bendAfter #-3 \) r8
  r2 r8 \mf bf,16 \( c df c bf8

  gf'1 
  f2. \) r4
  ef1~
  ef2 \> d

  r4 \! ef8---> \< r r4 f8---> r 
  r4 gf8---> r r4 af4-> \(
  ef8---> \) \f r r4 r2

  ef,16 \mf d ef d ef d ef d ef16 gf8---. bf16---. r gf af8->~
  af2. r4
  bf16 a bf a bf a bf a bf16 df8---. f16---. r df ef8->~
  ef1

  r4 ef8---> \< r r4 f8---> r 
  r4 gf8---> r r4 af4-> \(
  ef8---> \) \f r r4 r2 \fermata

  r2 ef,16 \mf gf8---. bf16---. r gf af8->~
  \time 2/4
  af2 \<

  \time 4/4
  ef'8---> \f r r4 r2
  \bar "|."
}

tenorSax = \relative c' {
  \global
  \key ef \minor

  r2 ef'16 \mf gf8---. bf16---. r gf af8->~
  \time 2/4
  af2 \<

  \bar "||"
  \time 4/4
  ef'8---> \f r r4 r2
  R1

  ef,2 \mf ef16 gf8---. bf16---. r gf af8->~
  af2. r4
  ef2 ef16 gf8---. bf16---. r gf af8->~
  af1 \>

  \repeat volta 2 {
    ef4-^ \f r r2
    r2 r8 \mf ef16 \( f gf f ef bf'->~
    bf2.~ bf16 af16-> \f \bendAfter #-3 \) r8
    r2 r8 \mf ef16 \( f gf f ef8

    bf'1 
    af2. \) r4
    gf1
    bf \>

    r4 \! gf8---> \< r r4 af8---> r 
    r4 bf8---> r r4 df4-> \(
    ef8---> \) \f r r4 r2

    ef,2 \mf ef16 gf8---. bf16---. r gf af8->~
    af2. r4
    ef2 ef16 gf8---. bf16---. r gf af8->~
    af1 \>
  } 

  R1*4 \!

  ef1 \mf \(
  f2 r
  ef1 ~ \<
  ef2 \f d4 \) r4

  r4 gf8---> \mf \< r r4 af8---> r 
  r4 bf8---> r r4 df4-> \(
  ef8---> \) \f r r4 r2

  ef,16 \mf d ef d ef d ef d ef16 gf8---. bf16---. r gf af8->~
  af2. r4
  ef16 d ef d ef d ef d ef16 gf8---. bf16---. r gf af8->~
  af1 \>

  ef4-^ \f r r2
  r2 r8 \mf ef16 \( f gf f ef bf'->~
  bf2.~ bf16 af16-> \f \bendAfter #-3 \) r8
  r2 r8 \mf ef16 \( f gf f ef8

  bf'1 
  af2. \) r4
  gf1 
  bf \>

  r4 \! gf8---> \< r r4 af8---> r 
  r4 bf8---> r r4 df4-> \(
  ef8---> \) \f r r4 r2

  ef,16 \mf d ef d ef d ef d ef16 gf8---. bf16---. r gf af8->~
  af2. r4
  ef16 d ef d ef d ef d ef16 gf8---. bf16---. r gf af8->~
  af1

  r4 gf8---> \< r r4 af8---> r 
  r4 bf8---> r r4 df4-> \(
  ef8---> \) \f r r4 r2 \fermata

  r2 ef,16 \mf gf8---. bf16---. r gf af8->~
  \time 2/4
  af2 \<

  \time 4/4
  ef'8---> \f r r4 r2
  \bar "|."
}

trombone = \relative c {
  \global
  \key ef \minor
  \clef bass

  r2 ef16 \mf gf8---. bf16---. r gf af8->~
  \time 2/4
  af2 \<

  \bar "||"
  \time 4/4
  ef'8---> \f r r4 r2
  R1

  ef,2 \mf ef16 gf8---. bf16---. r gf af8->~
  af2. r4
  ef2 ef16 gf8---. bf16---. r gf af8->~
  af1 \>

  \repeat volta 2 {
    ef4-^ \f r r2
    r2 r8 \mf ef16 \( f gf f ef bf'->~
    bf2.~ bf16 af16-> \f \bendAfter #-3 \) r8
    r2 r8 \mf ef16 \( f gf f ef8

    bf'1 
    af2. \) r4
    gf1
    bf \>

    r4 \! cf8---> \< r r4 df8---> r 
    r4 ef8---> r r4 f4-> \(
    ef8---> \) \f r r4 r2

    ef,2 \mf ef16 gf8---. bf16---. r gf af8->~
    af2. r4
    ef2 ef16 gf8---. bf16---. r gf af8->~
    af1 \>
  } 

  R1*4 \!

  af1 \mf \(
  bf2 r
  cf1  \<
  bf2. \f \) r4

  r4 \! cf8---> \< r r4 df8---> r 
  r4 ef8---> r r4 f4-> \(
  ef8---> \) \f r r4 r2

  ef,16 \mf d ef d ef d ef d ef16 gf8---. bf16---. r gf af8->~
  af2. r4
  ef16 d ef d ef d ef d ef16 gf8---. bf16---. r gf af8->~
  af1 \>

  ef4-^ \f r r2
  r2 r8 \mf ef16 \( f gf f ef bf'->~
  bf2.~ bf16 af16-> \f \bendAfter #-3 \) r8
  r2 r8 \mf ef16 \( f gf f ef8

  bf'1 
  af2. \) r4
  gf1 
  bf \>

  r4 \! cf8---> \< r r4 df8---> r 
  r4 ef8---> r r4 f4-> \(
  ef8---> \) \f r r4 r2

  ef,16 \mf d ef d ef d ef d ef16 gf8---. bf16---. r gf af8->~
  af2. r4
  ef16 d ef d ef d ef d ef16 gf8---. bf16---. r gf af8->~
  af1

  r4 \! cf8---> \< r r4 df8---> r 
  r4 ef8---> r r4 f4-> \(
  ef8---> \) \f r r4 r2 \fermata

  r2 ef,16 \mf gf8---. bf16---. r gf af8->~
  \time 2/4
  af2 \<

  \time 4/4
  ef'8---> \f r r4 r2
  \bar "|."
}

% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\book {
  \paper { 
    % Descomentar para score
    % #(set-paper-size "a4" 'landscape)
    % indent = 20
    % systems-per-page = 2
  }

  \score {
    % Descomentar para midi
    % \midi { \tempo 4 = 110 }

    % NO USAR layout-set-staff-size O SE ROMPE lilyjazz!!!
    \layout { }

    <<
      \scoreMarkup
      \transpose bf c' \scoreChords
      \new StaffGroup <<
        \new Staff \with 
        { instrumentName = "Trpt."}
        { \transpose bf c' \trumpet } 

        \new Staff \with 
        { instrumentName = "T Sx." \RemoveEmptyStaves }
        { \transpose bf c' \tenorSax } 

        \new Staff \with 
        { instrumentName = "Trbn." \RemoveEmptyStaves } 
        { \trombone }
      >>
    >>
  }
}
