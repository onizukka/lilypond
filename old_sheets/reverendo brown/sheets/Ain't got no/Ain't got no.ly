\version "2.18.2"
\language "english"

% Se debe incluir antes que lilyjazz. Si no, lilyjazz no funciona.
#(set-global-staff-size 20)

\include "main.ily"
\include "music-sheet-paper.ily"

\paper {
  indent = 10
}

\header {
  title = "Ain't got no - I got life"
  instrument = "Trumpet, Tenor Sax & Trombone"
  composer = "Nina Simone"
  arranger = ""
  poet = "gabriel@sauros.es"
  tagline = ""
  meter = \jazzTempoMarkup "Medium Rock" c4. "110"
}

% Función musical global
global = {
  % Para pintar todos los números de compás debajo del pentagrama
  % \override Score.BarNumber #'break-visibility = #end-of-line-invisible
  % \override Score.BarNumber #'direction = #DOWN
  % \override Score.BarNumber #'extra-offset = #'(1.5 . 0)
  \compressEmptyMeasures

  \set Timing.beamExceptions = #'()
  \set Timing.baseMoment = #(ly:make-moment 1/4)
  \set Timing.beatStructure = #'(1 1 1 1) 
}

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
  % Oculta el primer pentagrama del markup
  \override Staff.VerticalAxisGroup #'remove-first = ##t

  R1

  \mark "Intro"
  R1*8
  \pageBreak
  \mark "Verse"
  R1*10
  \mark "Bridge"
  R1*8
  \pageBreak
  \mark "Chorus"
  R1*15
  \pageBreak
  R1*4
  \mkup "(Harp. solo)"
  \mark "Verse"
  R1*10
  \mark "Bridge"
  R1*8
  \pageBreak
  \mark "Chorus"
  R1*15
  \pageBreak
  \mkup "(Repeat       Xs)"
}


% Cifrados
scoreChords = \chords {
  % c1:m f2:m g2:m7
}

% Música
trumpet = \relative c' {
  \global
  \key g \major

  g''16 \f g g g g g g g f8-. f4-> e8-.

  \bar "||"
  \repeat percent 3 {
    \parenthesize d4-. d,16 e g a-. ~ a g8.-> ~ g4
    r4 g16 a c d-. ~ d c8.-> ~ c4
  }
  r4 f16 f f e-. ~ e g8.-> ~ g4
  b2.-> r4

  \break
  \bar "[|:"
  \repeat volta 2 {
    R1*7
    r4 c,8. -> d16-. r2
    r4 f16 \f f f e-. ~ e g8.-> ~ g4
    b2.-> r4
  }
  \bar ":|]"

  \break
  c,2. \mf r4
  c4 d8 e-. ~ e d c4-.
  a2. r4
  a4 b8 c-. ~ c b a4-.

  \break
  c2. r4
  c4 d8 e-. ~ e d c4-.
  d1 \sfp \<
  fs4-^ \f r fs-^ r

  \break
  \bar "[|:"
  \repeat volta 2 {
    r4 g-^ \f r g-^
    r4 g-^ r g-^
    r4 g-^ r g-^
    \break
    fs2. \p \< r4 \!
    r4 c16 \f c c d-. ~ d c8.-> ~ c4
    r4 fs16 fs fs g-. ~ g fs8.-> ~ fs4
  }
  \bar ":|]"

  \break
  r4 g-^ r8 fs16 \mp \< fs fs fs \! r8
  r4 g-^ r8 fs16 \mp \< fs fs fs \! r8
  \break
  r4 g-^ r8 fs16 \mp \< fs fs fs \! r8
  r4 e-^ r8 fs16 \mp \< fs fs fs \! r8

  R1*5

  \break
  r4 d,16 \f e g a-. ~ a g8.-> ~ g4
  r4 g16 a c d-. ~ d c8.-> ~ c4
  r4 f16 f f e-. ~ e g8.-> ~ g4
  b2.-> r4

  \break
  R1*8
  r4 f16 \f f f e-. ~ e g8.-> ~ g4
  b2.-> r4

  \break
  c,2. \mf r4
  c4 d8 e-. ~ e d c4-.
  a2. r4
  a4 b8 c-. ~ c b a4-.

  \break
  c2. r4
  c4 d8 e-. ~ e d c4-.
  d1-> \f ~
  d2. ^"opt.8va" \! r4

  \break
  \bar "[|:"
  \repeat volta 2 {
    r4 g-^ \f r g-^
    r4 g-^ r g-^
    r4 g-^ r g-^
    \break
    fs2. \p \< r4 \!
    r4 c16 \f c c d-. ~ d c8.-> ~ c4
    r4 fs16 fs fs g-. ~ g fs8.-> ~ fs4
  }
  \bar ":|]"

  \break
  r4 g-^ r8 fs16 \mp \< fs fs fs \! r8
  r4 g-^ r8 fs16 \mp \< fs fs fs \! r8
  \break
  r4 g-^ r8 fs16 \mp \< fs fs fs \! r8
  r4 e-^ r8 fs16 \mp \< fs fs fs \! r8

  R1*5

  \break
  \bar "[|:"
  \repeat volta 2 {
    r4 d,16 \f e g a-. ~ a g8.-> ~ g4
  } \alternative {{
    r4 g16 a c d-. ~ d c8.-> ~ c4
    \bar ":|]"
  }{
    \break
    r4 f16 f f e-. ~ e g8.-> ~ g4
  }}
  b2.-> r4
  \bar "|."
}

tenorSax = \relative c'' {
  \global
  \key g \major

  g'16 \f g g g g g g g a8-. a4-> g8-.

  \bar "||"
  \repeat percent 3 {
    \parenthesize b4-. d,,16 e g a-. ~ a g8.-> ~ g4
    r4 g16 a c d-. ~ d c8.-> ~ c4
  }
  r4 f16 f f e-. ~ e g8.-> ~ g4
  g2.-> r4

  \break
  \bar "[|:"
  \repeat volta 2 {
    R1*7
    r4 c,8. -> d16-. r2
    r4 f16 \f f f e-. ~ e g8.-> ~ g4
    g2.-> r4
  }
  \bar ":|]"

  \break
  e2. \mf r4
  e4 f8 g-. ~ g f e4-.
  c2. r4
  c4 d8 e-. ~ e d c4-.

  \break
  e2. r4
  e4 f8 g-. ~ g f e4-.
  a1 \sfp \<
  d,4-^ \f r d-^ r

  \break
  \bar "[|:"
  \repeat volta 2 {
    r4 b'-^ \f r c-^
    r4 b-^ r c-^
    r4 b-^ r c-^
    \break
    d2. \p \< r4 \!
    r4 c,16 \f c c d-. ~ d c8.-> ~ c4
    r4 fs16 fs fs g-. ~ g fs8.-> ~ fs4
  }
  \bar ":|]"

  \break
  r4 e-^ r8 d16 \mp \< d d d \! r8
  r4 e-^ r8 d16 \mp \< d d d \! r8
  \break
  r4 e-^ r8 d16 \mp \< d d d \! r8
  r4 c-^ r8 d16 \mp \< d d d \! r8

  R1*5

  \break
  r4 d,16 \f e g a-. ~ a g8.-> ~ g4
  r4 g16 a c d-. ~ d c8.-> ~ c4
  r4 f16 f f e-. ~ e g8.-> ~ g4
  g2.-> r4

  \break
  R1*8
  r4 f16 \f f f e-. ~ e g8.-> ~ g4
  g2.-> r4

  \break
  e2. \mf r4
  e4 f8 g-. ~ g f e4-.
  c2. r4
  c4 d8 e-. ~ e d c4-.

  \break
  e2. r4
  e4 f8 g-. ~ g f e4-.
  a1-> \f ~
  a2. \!  r4

  \break
  \bar "[|:"
  \repeat volta 2 {
    r4 g-^ \f r g-^
    r4 g-^ r g-^
    r4 g-^ r g-^
    \break
    fs2. \p \< r4 \!
    r4 c16 \f c c d-. ~ d c8.-> ~ c4
    r4 fs16 fs fs g-. ~ g fs8.-> ~ fs4
  }
  \bar ":|]"

  \break
  r4 e-^ r8 d16 \mp \< d d d \! r8
  r4 e-^ r8 d16 \mp \< d d d \! r8
  \break
  r4 e-^ r8 d16 \mp \< d d d \! r8
  r4 c-^ r8 d16 \mp \< d d d \! r8

  R1*5

  \break
  \bar "[|:"
  \repeat volta 2 {
    r4 d,16 \f e g a-. ~ a g8.-> ~ g4
  } \alternative {{
    r4 g16 a c d-. ~ d c8.-> ~ c4
    \bar ":|]"
  }{
    r4 f16 f f e-. ~ e g8.-> ~ g4
  }}
  g2.-> r4
  \bar "|."
}

trombone = \relative c {
  \global
  \clef bass
  \key g \major

  g'16 \f g g g g g g g c,8-. c4-> d8-.

  \bar "||"
  \repeat percent 3 {
    \parenthesize g4-. d16 e g a-. ~ a g8.-> ~ g4
    r4 g16 a c d-. ~ d c8.-> ~ c4
  }
  r4 f16 f f e-. ~ e g8.-> ~ g4
  g2.-> r4

  \break
  \bar "[|:"
  \repeat volta 2 {
    e,8-. e,-. r4 r2
    g'8-. g,-. r4 r2
    e'8-. e,-. r4 r2
    \break
    g'8-. g,-. r4 r2

    d''8-. d,-. r4 r2
    b'8-. b,-. r4 r2
    \break
    e8-. e,-. r4 r2
    c'4-^ c'8. -> d16-. r2

    r4 f16 \f f f e-. ~ e g8.-> ~ g4
    g2.-> r4
  }
  \bar ":|]"

  \break
  R1*6
  e1 \sfp \<
  d4-^ \f r d-^ r

  \break
  \bar "[|:"
  \repeat volta 2 {
    r4 d-^ \f r e-^
    r4 d-^ r e-^
    r4 d-^ r e-^
    \break
    b2. \p \< r4 \!
    r4 c16 \f c c d-. ~ d c8.-> ~ c4
    r4 fs16 fs fs g-. ~ g fs8.-> ~ fs4
  }
  \bar ":|]"

  \break
  e,4-- b'-^ b,8-- b'16 \mp \< b b b \! r8
  e,4-- b'-^ b,8-- b'16 \mp \< b b b \! r8
  \break
  e,4-- b'-^ b,8-- b'16 \mp \< b b b \! r8
  a,4-- a'-^ d,8-- d'16 \mp \< d d d \! r8

  R1*5

  \break
  r4 d,16 \f e g a-. ~ a g8.-> ~ g4
  r4 g16 a c d-. ~ d c8.-> ~ c4
  r4 f16 f f e-. ~ e g8.-> ~ g4
  g2.-> r4

  \break
  R1*8
  r4 f16 \f f f e-. ~ e g8.-> ~ g4
  g2.-> r4

  \break
  R1*6
  d1 \f ~
  d2. r4

  \break
  \bar "[|:"
  \repeat volta 2 {
    r4 d-^ \f r e-^
    r4 d-^ r e-^
    r4 d-^ r e-^
    \break
    b2. \p \< r4 \!
    r4 c16 \f c c d-. ~ d c8.-> ~ c4
    r4 fs16 fs fs g-. ~ g fs8.-> ~ fs4
  }
  \bar ":|]"

  \break
  e,4-- b'-^ b,8-- b'16 \mp \< b b b \! r8
  e,4-- b'-^ b,8-- b'16 \mp \< b b b \! r8
  \break
  e,4-- b'-^ b,8-- b'16 \mp \< b b b \! r8
  a,4-- a'-^ d,8-- d'16 \mp \< d d d \! r8

  R1*5

  \break
  \bar "[|:"
  \repeat volta 2 {
    r4 d,16 \f e g a-. ~ a g8.-> ~ g4
  } \alternative {{
    r4 g16 a c d-. ~ d c8.-> ~ c4
    \bar ":|]"
  }{
    r4 f16 f f e-. ~ e g8.-> ~ g4
  }}
  g2.-> r4
  \bar "|."
}

% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\book {
  \paper { 
    % Descomentar para score
    % #(set-paper-size "a4" 'landscape)
    % indent = 20
    % systems-per-page = 2
  }

  \score {
    % Descomentar para midi
    % \midi { \tempo 4 = 110 }

    % NO USAR layout-set-staff-size O SE ROMPE lilyjazz!!!
    \layout { }

    <<
      \scoreMarkup
      \transpose bf c' \scoreChords
      \new StaffGroup <<
        \new Staff \with 
        { instrumentName = "Trpt." }
        { \transpose bf c' \trumpet } 

        \new Staff \with 
        { instrumentName = "T Sx." }
        { \transpose bf c' \tenorSax } 

        \new Staff \with 
        { instrumentName = "Trbn." } 
        { \trombone }
      >>
    >>
  }
}
