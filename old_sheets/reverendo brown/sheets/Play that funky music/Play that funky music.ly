% Se debe incluir antes que lilyjazz. Si no, lilyjazz no funciona.
#(set-global-staff-size 20)

\include "main.ily"
\include "music-sheet-paper.ily"

\paper {
  first-page-number = 2
}

\header {
  title = "Play that funky music"
  instrument = "Trumpet, Tenor Sax & Trombone"
  composer = "Wild Cherry"
  arranger = ""
  poet = "gabriel@sauros.es"
  tagline = ""
  meter = \jazzTempoMarkup "Medium Funk" c4 "100"
}

% Función musical global
global = {
  % Para pintar todos los números de compás debajo del pentagrama
  % \override Score.BarNumber #'break-visibility = #end-of-line-invisible
  % \override Score.BarNumber #'direction = #DOWN
  % \override Score.BarNumber #'extra-offset = #'(1.5 . 0)
  \compressFullBarRests

  \set Timing.beamExceptions = #'()
  \set Timing.baseMoment = #(ly:make-moment 1/4)
  \set Timing.beatStructure = #'(1 1 1 1) 
}

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
  % Oculta el primer pentagrama del markup
  \override Staff.VerticalAxisGroup #'remove-first = ##t

  % Indicación de tiempo
  % \once \override Score.RehearsalMark.self-alignment-X = #LEFT
  \override Score.RehearsalMark.extra-offset = #'(0 . 2) 

  \part "Intro"
  s1*4
  s1*4

  \part "Verse"
  s1*16
  s1*4

  \part "Chorus"
  s1*12

  \mkup "(Guitar solo)"
  s1*8

  \part "Verse"
  s1*16
  s1*4

  \part "Chorus"
  s1*12

  \mkup "(Harp solo)"
  s1*8

  \mkup "(Chorus)"
  s1*13
}


% Cifrados
scoreChords = \chords {
  % c1:m f2:m g2:m7
}

% Música
trumpet = \relative c'' {
  \global
  \key e \major

  R1*4
  R1*4

  \break
  \bar "[|:-||"
  \repeat volta 2 {
    \parenthesize e4-^ r r2
    R1*2
    r2 r4 cs8-. \mf c-.

    b16 b-. r8 r4 r2
    \break
    R1*2
    r2 r4 cs8-. c-.

    b16 b-. r8 r4 r2
    R1*2
    \break
    r2 r4 cs8-. c-.

    b16 b-. r8 r4 r2
    R1*2
    r4 e8-. d-. e4-. r4

    R1*4
    \break
    R1*8
    R1*3
    r2 r4 r8 d8->( \f
  }
  \bar ":|]"

  \break
  e4-^) r r2
  R1*7

  \bar "||"
  R1*3
  r2 r4 cs8-. \mf c-.

  b16 b-. r8 r4 r2
  \break
  R1*2
  r2 r4 cs8-. c-.

  b16 b-. r8 r4 r2
  R1*2
  \break
  r2 r4 cs8-. c-.

  b16 b-. r8 r4 r2
  R1*2
  r4 e8-. d-. e4-. r4

  R1*4
  \break
  R1*8
  R1*3
  r2 r4 r8 d8->( \f

  \bar "||"
  e4-^) r r2
  R1*2
  r2 r4 cs8-. \mf c-.

  b16 b-. r8 r4 r2
  R1*2
  r2 r4 e8-. ef-.

  \bar "||"
  \key g \minor
  d?16 d-. r8 r4 r2
  R1*2
  r2 r4 e8-. ef-.

  d16 d-. r8 r4 r2
  R1*2
  r2 r4 fs8-. f-.

  \bar "||"
  \key a \minor
  e?16 e-. r8 r4 r2
  R1*2
  r2 r4 fs8-. f-.

  e16 e-. r8 r4 r2
  R1*2
  r2 r4 r8 g,(

  a1) \fermata
  \bar "|."
}

tenorSax = \relative c'' {
  \global
  \key e \major

  R1*8

  \repeat volta 2 {
    \parenthesize b'4-^ r r2
    R1*2
    r4 e,,8-. \mf d-. e4-. a'8-. af-.

    g16 g-. r8 r4 r2
    R1*2
    r4 e,8-. d-. e4-. a'8-. af-.

    g16 g-. r8 r4 r2
    R1*2
    r4 e,8-. d-. e4-. a'8-. af-.

    g16 g-. r8 r4 r2
    R1*2
    r4 b8-. a-. b4-. r4

    R1*4
    R1*7
    r2 r4 r8 \f d,,->(

    e4-^) r r2
    r2 r4 r8 \f d->(
    e4-^) r r2
    r2 r4 r8 a'8->(
  }

  b4-^) r r2
  R1*7

  R1*3
  r4 e,,8-. \mf d-. e4-. a'8-. af-.

  g16 g-. r8 r4 r2
  R1*2
  r4 e,8-. d-. e4-. a'8-. af-.

  g16 g-. r8 r4 r2
  R1*2
  r4 e,8-. d-. e4-. a'8-. af-.

  g16 g-. r8 r4 r2
  R1*2
  r4 b8-. a-. b4-. r4

  R1*4
  R1*7
  r2 r4 r8 \f d,,->(

  e4-^) r r2
  r2 r4 r8 d->(
  e4-^) r r2
  r2 r4 r8 a'8->(

  b4-^) r r2
  R1*2
  r4 e,,8-. \mf d-. e4-. a'8-. af-.

  g16 g-. r8 r4 r2
  R1*2

  r4 e,8-. d-. e4-. c''8-. b-.
  \key g \minor
  bf?16 bf-. r8 r4 r2
  \break
  R1*2
  r4 g,8-. f-. g4-. c'8-. b-.

  bf?16 bf-. r8 r4 r2
  R1*2
  r4 g,8-. f-. g4-. d''8-. df-.

  \key a \minor
  c16 c-. r8 r4 r2
  R1*2
  r4 a,8-. g-. a4-. d'8-. df-.

  c16 c-. r8 r4 r2
  R1*2
  r2 r4 r8 g,(

  a1) \fermata
}

trombone = \relative c' {
  \global
  \key e \major
  \clef bass

  R1*8

  \repeat volta 2 {
    \parenthesize e4-^ r r2
    R1*2
    r4 e,,8-. \mf d-. e4-. e''8-. ef-.

    d16 d-. r8 r4 r2
    R1*2
    r4 e,,8-. d-. e4-. e''8-. ef-.

    d16 d-. r8 r4 r2
    R1*2
    r4 e,,8-. d-. e4-. e''8-. ef-.

    d16 d-. r8 r4 r2
    R1*2
    r4 e8-. d-. e4-. r4

    R1*4
    R1*8
    R1*3
    r2 r4 r8 d8->( \f
  }

  e4-^) r r2
  R1*7

  R1*3
  r4 e,,8-. \mf d-. e4-. e''8-. ef-.

  d16 d-. r8 r4 r2
  R1*2
  r4 e,,8-. d-. e4-. e''8-. ef-.

  d16 d-. r8 r4 r2
  R1*2
  r4 e,,8-. d-. e4-. e''8-. ef-.

  d16 d-. r8 r4 r2
  R1*2
  r4 e8-. d-. e4-. r4

  R1*4
  R1*8
  R1*3
  r2 r4 r8 d8->( \f

  e4-^) r r2
  R1*2
  r4 e,,8-. \mf d-. e4-. e''8-. ef-.

  d16 d-. r8 r4 r2
  R1*2
  r4 e,,8-. d-. e4-. g''8-. fs-.

  \key g \minor
  f?16 f-. r8 r4 r2
  R1*2
  r4 g,,8-. f-. g4-. g''8-. fs-.

  f?16 f-. r8 r4 r2
  R1*2
  r4 g,,8-. f-. g4-. a''8-. af-.

  \key a \minor
  g16 g-. r8 r4 r2
  R1*2
  r4 a,,8-. g-. a4-. a''8-. af-.

  g16 g-. r8 r4 r2
  R1*2
  r2 r4 r8 g,(

  a1) \fermata
}

% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\book {
  \paper { 
    % Descomentar para score
    % #(set-paper-size "a4" 'landscape)
    % indent = 20
    % systems-per-page = 2
  }

  \score {
    % Descomentar para midi
    % \midi { \tempo 4 = 110 }

    % NO USAR layout-set-staff-size O SE ROMPE lilyjazz!!!
    \layout { }

    <<
      \scoreMarkup
      \transpose bf c' \scoreChords
      \new StaffGroup <<
        \new Staff \with 
        { instrumentName = "Trpt."}
        { \transpose bf c' \trumpet } 

        \new Staff \with 
        { instrumentName = "T Sx." }
        { \transpose bf c' \tenorSax } 

        \new Staff \with 
        { instrumentName = "Trbn." } 
        { \trombone }
      >>
    >>
  }
}
