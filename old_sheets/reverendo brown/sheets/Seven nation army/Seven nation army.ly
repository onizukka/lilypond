\version "2.18.2"
\language "english"

% Se debe incluir antes que lilyjazz. Si no, lilyjazz no funciona.
#(set-global-staff-size 20)

\include "main.ily"
\include "music-sheet-paper.ily"

\paper {
  first-page-number = 2
  footnote-separator-markup = ""
}

\header {
  title = "Seven nation army"
  instrument = "Trumpet, Tenor Sax & Trombone"
  composer = "The White Stripes"
  arranger = "(Ben l'Oncle Soul)"
  poet = "gabriel@sauros.es"
  tagline = ""
  meter =  \jazzTempoMarkup "R&B" c4 "110"
}

% Función musical global
global = {
  % Para pintar todos los números de compás debajo del pentagrama
  % \override Score.BarNumber #'break-visibility = #end-of-line-invisible
  % \override Score.BarNumber #'direction = #DOWN
  % \override Score.BarNumber #'extra-offset = #'(1.5 . 0)
  \compressFullBarRests

  \set Timing.beamExceptions = #'()
  \set Timing.baseMoment = #(ly:make-moment 1/4)
  \set Timing.beatStructure = #'(1 1 1 1) 
}

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
  % Oculta el primer pentagrama del markup
  \override Staff.VerticalAxisGroup #'remove-first = ##t

  % Indicación de tiempo
  % \once \override Score.RehearsalMark.self-alignment-X = #LEFT
  \override Score.RehearsalMark.extra-offset = #'(0 . 2) 

  \part "Intro"
  s1*4
  \part "Verse"
  s1*16
  s1*2
  \part "Chorus"
  s1*4
  s1*2
  s1*4

  \part "Verse"
  s1*16
  s1*2
  \part "Chorus"
  s1*4
  \note "Keyboard solo"
  s1*16

  \part "Verse"
  s1*8
  s1*4
  s1*2
  \part "Chorus"
  s1*4

  s1*2
  s1*3
}

% Cifrados
scoreChords = \chords {
  % c1:m f2:m g2:m7
}

nextPageRests = \markup \fill-line { 
  \null
  \small \line {
    \score {
      \new Staff \with {
        fontSize = #-2
        \override StaffSymbol #'staff-space = #(magstep -4)
      }
      \relative c'' { 
        \once \override Score.RehearsalMark.extra-offset = #'(15 . -1.5) 
        \mark \markup \scale #'(1 . -1) \rotate #0 \curvedArrow #0 #0 #0
        \compressFullBarRests R1*16 | R1*8
        \bar ""
      }
      \layout {
        indent = 0
        \context {
          \Staff
          \remove "Clef_engraver"
          \remove "Time_signature_engraver"
        }
      }
    }
    \hspace #9
  }
}

% Música
trumpet = \relative c'' {
  \global
  \key e \minor

  R1*4

  \bar "||"
  R1*16

  \break
  r8 \f g16 ( fs g8 ) b16 ( as b8 ) d16 ( cs d4 )
  r8 a16 ( gs a8 ) cs16 ( c cs8 ) e16 ( ds e4 )

  \break
  \bar "[|:"
  \repeat volta 2 {
    r8 \mf e, -. g -. b \bendAfter #-5 r8 e, -. g -. e -.
    \hiddenGrace e b'4-- \bendBefore \hiddenGrace b b4-- \bendBefore a8 -. b -- r8 g -- 
    \break
    r8 \hiddenGrace g e8-- \bendBefore ~ e4 ~ e2
    R1
  }
  \bar ":|]"

  \pageBreak
  r8 \f g16 ( fs g8 ) b16 ( as b8 ) d16 ( cs d4 )
  r8 a16 ( gs a8 ) cs16 ( c cs8 ) e16 ( ds e4 )

  R1*4

  \break
  \bar "||"
  R1*16

  r8 \f g,16 ( fs g8 ) b16 ( as b8 ) d16 ( cs d4 )
  r8 a16 ( gs a8 ) cs16 ( c cs8 ) e16 ( ds e4 )

  \break
  \bar "[|:"
  \repeat volta 2 {
    r8 \mf e, -. g -. b \bendAfter #-5 r8 e, -. g -. e -.
    \hiddenGrace e16 b'4-- \bendBefore b4-- \bendBefore a8 -. b -- r8 g -- 
    \break
    r8 \hiddenGrace g e-- \bendBefore ~ e4 ~ \footnote "" #'(0 . 0) \nextPageRests e2 
    R1
  }
  \bar ":|]"

  \pageBreak
  R1*16
  \bar "||"

  R1*8

  \break
  \bar "[|:"
  \repeat volta 2 {
    g4 \mf r8 g8 -. b8. -- g16 -- ~ g8 fs -. 
    e4 r8 e -. ds4 r4
    \break
    g4 r8 g8 -. b8.--  g16 -- ~ g8 fs -. 
    e8.--  fs16 -- ~ fs8 e -. ds4 r4
  }
  \bar ":|]"

  \break
  r8 \f g16 ( fs g8 ) b16 ( as b8 ) d16 ( cs d4 )
  r8 a16 ( gs a8 ) cs16 ( c cs8 ) e16 ( ds e4 )

  \break
  \bar "[|:"
  \repeat volta 2 {
    r8 \mf e, -. g -. b \bendAfter #-5 r8 e, -. g -. e -.
    \hiddenGrace e b'4-- \bendBefore \hiddenGrace b b4-- \bendBefore a8 -. b -- r8 g -- 
    \break
    r8 \hiddenGrace g e-- \bendBefore ~ e4 ~ e2
    R1
  }
  \bar ":|]"

  \break
  r8 \f g16 ( fs g8 ) b16 ( as b8 ) d16 ( cs d4 )
  r8 a16 ( gs a8 ) cs16 ( c cs8 ) e16 ( ds e4 )

  \break
  e1 \p ~ e ~ e ~ e ~ e \fermata

  \bar "|."
}

tenorSax = \relative c'' {
  \global
  \key e \minor

  R1*4

  R1*16

  d1 \sfp \<
  cs \sfp \<

  \repeat volta 2 {
    r8 \mf b -. e -. g \bendAfter #-5 r8 b, -. e -. b -.
    g'4-- \bendBefore g4-- \bendBefore fs8 -. g -- r8 e -- 
    r8 b-- \bendBefore ~ b4 ~ b2
    R1
  }

  d1 \sfp \<
  cs \sfp \<

  R1*4 \!

  R1*16

  d1 \sfp \<
  cs \sfp \<

  \repeat volta 2 {
    r8 \mf b -. e -. g \bendAfter #-5 r8 b, -. e -. b -.
    g'4-- \bendBefore g4-- \bendBefore fs8 -. g -- r8 e -- 
    r8 b-- \bendBefore ~ b4 ~ b2
    R1
  }

  R1*16

  R1*8

  \repeat volta 2 {
    e4 \mf r8 e8 -. g8. -- e16 -- ~ e8 d -. 
    c4 r8 c -. b4 r4
    e4 r8 e8 -. g8.--  e16 -- ~ e8 d -. 
    c8.--  d16 -- ~ d8 c -. b4 r4
  }

  d1 \sfp \<
  cs \sfp \<

  \repeat volta 2 {
    r8 \mf b -. e -. g \bendAfter #-5 r8 b, -. e -. b -.
    g'4-- \bendBefore g4-- \bendBefore fs8 -. g -- r8 e -- 
    r8 b-- \bendBefore ~ b4 ~ b2
    R1
  }

  d1 \sfp \<
  cs \sfp \<

  e1 \p ~ e ~ e ~ e ~ e \fermata
}

trombone = \relative c' {
  \global
  \clef bass
  \key e \minor

  R1*4

  R1*16

  g1 \sfp \<
  a \sfp \<

  \repeat volta 2 {
    r8 \mf e -. g -. b \bendAfter #-5 r8 e, -. g -. e -.
    b'4-- \bendBefore b4-- \bendBefore a8 -. b -- r8 g -- 
    r8 e-- \bendBefore ~ e4 ~ e2
    R1
  }

  g1 \sfp \<
  a \sfp \<

  R1*4 \!

  R1*16

  g1 \sfp \<
  a \sfp \<

  \repeat volta 2 {
    r8 \mf e -. g -. b \bendAfter #-5 r8 e, -. g -. e -.
    b'4-- \bendBefore b4-- \bendBefore a8 -. b -- r8 g -- 
    r8 e-- \bendBefore ~ e4 ~ e2
    R1
  }

  R1*16

  R1*8

  \repeat volta 2 {
    b'4 \mf r8 b8 -. e8. -- b16 -- ~ b8 a -. 
    g4 r8 g -. fs4 r4
    b4 r8 b8 -. e8. -- b16 -- ~ b8 a -. 
    g8.--  a16 -- ~ a8 g -. fs4 r4
  }

  g1 \sfp \<
  a \sfp \<

  \repeat volta 2 {
    r8 \mf e -. g -. b \bendAfter #-5 r8 e, -. g -. e -.
    b'4-- \bendBefore b4-- \bendBefore a8 -. b -- r8 g -- 
    r8 e-- \bendBefore ~ e4 ~ e2
    R1
  }

  g1 \sfp \<
  a \sfp \<

  e1 \p ~ e ~ e ~ e ~ e \fermata
}

% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\book {

  \paper { 
    % Descomentar para score
    % #(set-paper-size "a4" 'landscape)
    % indent = 20
    % systems-per-page = 2
  }

  \score {
    % Descomentar para midi
    % \midi { \tempo 4 = 110 }

    % NO USAR layout-set-staff-size O SE ROMPE lilyjazz!!!
    \layout { }

    <<
      \scoreMarkup
      \transpose bf c' \scoreChords
      \new StaffGroup <<
        \new Staff \with 
        { instrumentName = "Trpt."}
        { \transpose bf c' \trumpet } 

        \new Staff \with 
        { instrumentName = "T Sx." \RemoveEmptyStaves }
        { \transpose bf c' \tenorSax } 

        \new Staff \with 
        { instrumentName = "Trbn." \RemoveEmptyStaves } 
        { \trombone }
      >>
    >>
  }
}
