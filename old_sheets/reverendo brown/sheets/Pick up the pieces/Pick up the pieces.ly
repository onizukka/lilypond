\version "2.18.2"
\language "english"

% Se debe incluir antes que lilyjazz. Si no, lilyjazz no funciona.
#(set-global-staff-size 19)

\include "main.ily"

\header {
	title = "Pick up the pieces"
	instrument = "Trumpet, Tenor Sax & Trombone"
	composer = "Average White Band"
	arranger = "Transc: music@gpalaciosm.com"
	poet = ""
	tagline = "Edition: music@gpalaciosm.com"
}

\paper {
	
	% distancia entre la parte superior de la página y el título
	top-markup-spacing.basic-distance = 5

	% distancia entre el título y el primer sistema
	markup-system-spacing.basic-distance = 20

	% distancia entre el texto y el primer sistema 
	top-system-spacing.basic-distance = 15

	system-system-spacing.basic-distance = 5

	% márgen inferior
	bottom-margin = 15
	
	% intentado del primer sistema
	indent = 10
	 
	% número de páginas
	% page-count = 1

	ragged-last-bottom = ##f 

	oddHeaderMarkup = \markup {
		\on-the-fly #not-first-page \fill-line {
			\fromproperty #'page:page-number-string
			\fromproperty #'header:title
			\fromproperty #'header:instrument
		}
	}

	evenHeaderMarkup = \markup {
		\on-the-fly #not-first-page \fill-line {
			\fromproperty #'header:instrument
			\fromproperty #'header:title
			\fromproperty #'page:page-number-string
		}
	}
}



% Función musical global
global = {
	% Para pintar todos los números de compás debajo del pentagrama
	% \override Score.BarNumber #'break-visibility = #end-of-line-invisible
	% \override Score.BarNumber #'direction = #DOWN
	% \override Score.BarNumber #'extra-offset = #'(1.5 . 0)

	\compressFullBarRests

	% Arregla el problema de tener el scoreMarkup añadido
	\revert MultiMeasureRest.staff-position
	\override Voice.Rest.staff-position = #0	

	\set Timing.beamExceptions = #'()
	\set Timing.baseMoment = #(ly:make-moment 1/4)
	\set Timing.beatStructure = #'(1 1 1 1) 
	
	\override ParenthesesItem.font-size = #0

	\override BendAfter.springs-and-rods = #ly:spanner::set-spacing-rods
  	\override BendAfter.minimum-length = #3
}

% Elementos comunes de texto (marcas de ensayo, tempo, saltos de línea, etc)
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
	% Oculta el primer pentagrama del markup
	\override Staff.VerticalAxisGroup #'remove-first = ##t
	\override Score.RehearsalMark.self-alignment-X = #LEFT

	% Indicación de tiempo
	% \once \override Score.RehearsalMark.self-alignment-X = #LEFT
	% \once \override Score.RehearsalMark.extra-offset = #'(0 . 3) 
	% \mark \jazzTempoMarkup "Easy Swing" c4 "112"

	% s1*2
	% \once \override Score.RehearsalMark.X-offset = #5
	% \mark \markup \boxed "A"

	%{ 
	\mark \markup {
		\column { \vspace #-.4 \fontsize #1 \musicglyph #"scripts.coda"  }
	    \hspace #.5
	    \column { "text" }
	} 
	%}
	s1*8

	\mkup "(Repeat 2xs)"
	s1*8

	\break
	s1*12

	\break
	\mkup "(Repeat 2xs)"
	s1*8

	\break
	s1*12

	\break
	s1*6

	\break
	s1*8

	\break
	\mkup "(Repeat 2xs)"
	\mark \segno
	s1*8

	\break
	s1*8

	\break
	\mkup "(No repetition 1st x, Repeat 3xs on segno)"
	s1*6

	\break
	s1*4
	\once \override Score.RehearsalMark.self-alignment-X = #RIGHT
	\mark \toCoda
	\bar "||"
	s1*1
	\mark \markup "(4)"
	s1*2
	\mark \markup "(6)"
	s1*2
	\mark \markup "(8)"
	s1

	\break
	\mkup "(Tenor Sax solo)"
	s1*16
	\mkup "(Repeat 4xs)"
	s1*3
	\mkup "(Tacet last x)"
	s1
	\eolMark \toSegno
	
	\break
	\mark \coda
	s1*4
}

% Cifrado de acordes
scoreChords = \chords {

}

tenorChords = \chords {
	% c1:m f2:m g2:m7

	s1*94

	bf1*16:7
	f1*4:7
}

trumpet = \relative c' {
  	\global
  	\key f \minor
	
	R1*8

	\bar ":|][|:"
	\repeat volta 2 {
		af8 c g' f r4 ef8 bf
		d c r4 bf'8 ef, f af~->
		af4 f-. ef8 f r af~->
		af4 f-. ef8 f r4

		R1*4
	}
	\bar ":|]"

	af8 c, g' f r4 af4--
	bf8 af r4 bf8 ef, f af~->
	af4 f-. ef8 f r af~->
	af4 f-. ef8 f r4

	R1*8

	\bar "[|:"
	\repeat volta 2 {
		af,8 c g' f r4 ef8 bf
		d c r4 bf'8 ef, f af~->
		af4 f-. ef8 f r af~->
		af4 f-. ef8 f r4

		R1*4
	}
	\bar ":|]"

	af8 c, g' f r4 af4--
	bf8 af r4 bf8 ef, f af~->
	af4 f-. ef8 f r af~->
	af4 f-. ef8 f r4

	R1*8

	bf8 af bf af f4-. bf8 af
	bf af ef4 -. r2
	bf'8 af bf af f4-. bf8 af
	bf af ef4 -. r2

	bf'8 af bf af f4-. bf8 af
	bf af c4 -. r2
	bf8 af bf af f4-. bf8 af
	bf af ef4 -. ef'2~->\<

	ef1~ 
	ef1

	R1*4\!

	\bar "[|:"
	\repeat volta 2 {
		af,,8 c g' f r4 ef8 bf
		d c r4 bf'8 ef, f af~->
		af4 f-. ef8 f r af~->
		af4 f-. ef8 f r4

		R1*4
	}
	\bar ":|]"

	af8 c, g' f r4 af4--
	bf8 af r4 bf8 ef, f af~->
	af4 f-. ef8 f r af~->
	af4 f-. ef8 f r4

	R1*4

	\bar "[|:"
	\repeat volta 2 {
		bf8 af bf af f4-. bf8 af
		bf af ef4 -. r2
		bf'8 af bf af f4-. bf8 af
		bf af ef4 -. r2
	}
	\bar ":|]"

	bf'8 af bf af f4-. bf8 af
	bf af c4 -. r2
	bf8 af bf af f4-. bf8 af
	bf af ef4 -. ef'2~->

	ef1~ 
	ef1~
	ef1~ 
	ef1~

	ef1~\<
	ef1~
	ef1~ 
	ef1

	R1*16\!

	\bar "[|:"
	\repeat volta 4 {
		c8 c r4 r4 c8 c
		R1
		c8 c r4 r4 c8 c
		r8 c-. r af c8 c d4-.
	}

	\bar ":|]"

	af,8 c g' f r4 ef8 bf
	d c r4 bf'8 ef, f af~->
	af4 f-. ef8 f r af~->
	af4 f-. r8 f' r4
	\bar "|."
}

% Música
tenorSax = \relative c'' {
  	\global
  	\key f \minor
	
	R1*8

	\bar ":|][|:"
	\repeat volta 2 {
		af8 c g' f r4 ef8 bf
		d c r4 bf'8 ef, f af~->
		af4 f-. ef8 f r af~->
		af4 f-. ef8 f r4

		R1*4
	}
	\bar ":|]"

	af8 c, g' f r4 af8 af
	bf8 af r4 bf8 ef, f af~->
	af4 f-. ef8 f r af~->
	af4 f-. ef8 f r4

	R1*8

	\bar "[|:"
	\repeat volta 2 {
		af,8 c g' f r4 ef8 bf
		d c r4 bf'8 ef, f af~->
		af4 f-. ef8 f r af~->
		af4 f-. ef8 f r4

		R1*4
	}
	\bar ":|]"

	af8 c, g' f r4 af8 af
	bf8 af r4 bf8 ef, f af~->
	af4 f-. ef8 f r af~->
	af4 f-. ef8 f r4

	R1*8

	bf8 af bf af f4-. bf8 af
	bf af ef4 -. r2
	bf'8 af bf af f4-. bf8 af
	bf af ef4 -. r2

	bf'8 af bf af f4-. bf8 af
	bf af c4 -. r2
	bf8 af bf af f4-. bf8 af
	bf af ef4 -. c'2~->\<

	c1~ 
	c1

	R1*4\!

	\bar "[|:"
	\repeat volta 2 {
		af,8 c g' f r4 ef8 bf
		d c r4 bf'8 ef, f af~->
		af4 f-. ef8 f r af~->
		af4 f-. ef8 f r4

		R1*4
	}
	\bar ":|]"

	af8 c, g' f r4 af8 af
	bf8 af r4 bf8 ef, f af~->
	af4 f-. ef8 f r af~->
	af4 f-. ef8 f r4

	R1*4

	bf8 af bf af f4-. bf8 af
	bf af ef4 -. r2
	bf'8 af bf af f4-. bf8 af
	bf af ef4 -. r2

	bf'8 af bf af f4-. bf8 af
	bf af c4 -. r2
	bf8 af bf af f4-. bf8 af
	bf af ef4 -. c'2~->

	c1~ 
	c1~
	c1~ 
	c1~

	c1~\<
	c1~
	c1~ 
	c1

	\once \override MultiMeasureRest.transparent = ##t
	R1*16\!

	\bar "[|:"
	\repeat volta 4 {
		\impro 16
	}
	\bar ":|]"

	af,8 c g' f r4 ef8 bf
	d c r4 bf'8 ef, f af~->
	af4 f-. ef8 f r af~->
	af4 f-. ef8 c' r4
	\bar "|."
}

% Música
trombone = \relative c {
  	\global
  	\clef bass
  	\key f \minor

	R1*8

	\bar ":|][|:"
	\repeat volta 2 {
		af'8 c g' f r4 ef8 bf
		d c r4 bf'8 ef, f ef~->
		ef4 d-. c8 d r ef~->
		ef4 d-. c8 d r4

		R1*4
	}
	\bar ":|]"

	af'8 c, g' f r4 ef8 d
	ef8 d r4 bf'8 ef, f ef~->
	ef4 d-. c8 d r ef~->
	ef4 d-. c8 d r4

	R1*8

	\bar "[|:"
	\repeat volta 2 {
		af8 c g' f r4 ef8 bf
		d c r4 bf'8 ef, f ef~->
		ef4 d-. c8 d r ef~->
		ef4 d-. c8 d r4

		R1*4
	}
	\bar ":|]"

	af'8 c, g' f r4 ef8 d
	ef8 d r4 bf'8 ef, f ef~->
	ef4 d-. c8 d r ef~->
	ef4 d-. c8 d r4

	R1*8

	bf8 af bf af f4-. bf8 af
	bf af ef4 -. r2
	bf'8 af bf af f4-. bf8 af
	bf af ef4 -. r2

	bf'8 af bf af f4-. bf8 af
	bf af c4 -. r2
	bf8 af bf af f4-. bf8 af
	bf af ef4 -. ef'2~->

	ef1~ 
	ef1

	R1*4

	\bar "[|:"
	\repeat volta 2 {
		af,8 c g' f r4 ef8 bf
		d c r4 bf'8 ef, f ef~->
		ef4 d-. c8 d r ef~->
		ef4 d-. c8 d r4

		R1*4
	}
	\bar ":|]"

	af'8 c, g' f r4 ef8 d
	ef8 d r4 bf'8 ef, f ef~->
	ef4 d-. c8 d r ef~->
	ef4 d-. c8 d r4

	R1*4

	bf8 af bf af f4-. bf8 af
	bf af ef4 -. r2
	bf'8 af bf af f4-. bf8 af
	bf af ef4 -. r2

	bf'8 af bf af f4-. bf8 af
	bf af c4 -. r2
	bf8 af bf af f4-. bf8 af
	bf af ef4 -. c'2~->

	c1~ 
	c1~
	c1~ 
	c1~

	c1~ \<
	c1~
	c1~ 
	c1

	R1*16\!

	\bar "[|:"
	\repeat volta 2{
		af8 af r4 r4 af8 af
		R1
		af8 ef r4 r4 af8 af
		r8 af-. r f af8 af bf4-.
	}
	\bar ":|]"

	af,8 c g' f r4 ef8 bf
	d c r4 bf'8 ef, f ef~->
	ef4 d-. c8 d r ef~->
	ef4 d-. c8 f r4
	\bar "|."
}

% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
\book {
	\paper { 
		% Descomentar para score
		% #(set-paper-size "a4" 'landscape)
		% indent = 20
		% systems-per-page = 2
	}
	
	\score {
		% Descomentar para midi
		% \midi { \tempo 4 = 110 }
		
		% ATENCIÓN
		% NO USAR layout-set-staff-size O SE ROMPE lilyjazz!!!
		\layout { }
		
		<<
			\scoreMarkup
			\scoreChords
			\new StaffGroup <<
				\new Staff \with 
				{ instrumentName = "Trpt."}
				{ \transpose bf c' \trumpet } 

				\transpose bf c' \tenorChords
				\new Staff \with 
				{ instrumentName = "T Sx." }
				{ \transpose bf c' \tenorSax } 

				\new Staff \with 
				{ instrumentName = "Trbn." } 
				{ \trombone }
			>>
		>>
	}
}



%{
\book {
	\header { instrument = "" }
	\bookOutputName ""
	\score {
		<<
			\scoreMarkup
			\scoreChords
			\new Staff { \transpose bf c' \instrumentA }
		>>
	}
}
%}

