% Se debe incluir antes que lilyjazz. Si no, lilyjazz no funciona.
#(set-global-staff-size 20)

\include "main.ily"
\include "music-sheet-paper.ily"

\paper {
  first-page-number = 2
}

\header {
  title = "Proud Mary"
  instrument = "Trumpet, Tenor Sax & Trombone"
  composer = "Tina Turner"
  arranger = ""
  poet = "gabriel@sauros.es"
  tagline = ""
  meter = \jazzTempoMarkup "R&B" c4 "150"
}

% Función musical global
global = {
  % Para pintar todos los números de compás debajo del pentagrama
  % \override Score.BarNumber #'break-visibility = #end-of-line-invisible
  % \override Score.BarNumber #'direction = #DOWN
  % \override Score.BarNumber #'extra-offset = #'(1.5 . 0)
  \compressFullBarRests

  \set Timing.beamExceptions = #'()
  \set Timing.baseMoment = #(ly:make-moment 1/4)
  \set Timing.beatStructure = #'(1 1 1 1) 
}

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
  % Oculta el primer pentagrama del markup
  \override Staff.VerticalAxisGroup #'remove-first = ##t

  % Indicación de tiempo
  % \once \override Score.RehearsalMark.self-alignment-X = #LEFT
  \override Score.RehearsalMark.extra-offset = #'(0 . 2)
  % \mark \jazzTempoMarkup "Easy Swing" c4 "112"

  \part "Slow"
  s1*4

  \part "Intro"
  s1*8

  \part "Verse"
  s1*12

  \part "Chorus"
  s1*23

  \part "Verse"
  s1*12

  \mark \markup \general-align #Y #DOWN {
    \column { \fontsize #-1 \musicglyph #"scripts.segno" }
    \column { 
      "Chorus"
      \underline "(Repeat 4 xs)" 
    }
  }
  s1*8

  s1*2
  \once \override Score.RehearsalMark.self-alignment-X = #RIGHT
  \mark \toCoda
  s1*2
  s1*4
  s1*4
  \eolMark \toSegno

  \break
  \mark \coda
  s1*3
}

% Cifrados
scoreChords = \chords {
  % c1:m f2:m g2:m7
}

% Música
trumpet = \relative c'' {
  \global
  \key d \major

  \once \override MultiMeasureRestNumber.transparent = ##t
  R1*4
  \bar "||"

  r2\f b,8 d d b
  d4-. r8 b'~-> b a es fs
  d4-. r b8 d d b
  d4-. r8 b'~-> b a es fs

  \break
  d4-. r b8 d d b
  d4-. r8 b'~-> b a es fs
  d4-. r8 b'~-> b a es fs
  d4-. r4 r2

  \break
  \bar "[|:"
  \repeat volta 2 {
    \parenthesize d4-._\markup \normalsize "(only 2nd x)" r4 r2
    \repeat percent 3 {
      r2 c8-.\p b-. r a~->
      a1
    }
    R1

    \break
    cs'1\mf~
    cs2. ~ cs8 d~->
    d2.~ d8 d~->
    d2.\< r4\!

    r4 r8\f d-> r2
    r4 r8 d-> r2
    \break
    r2 r4 r8 fs-.
    r fs e d e4-- d-.

    r4 r8 d-> r2
    r4 r8 d-> r2
    r2 r4 r8 fs-.
    r fs e d e4-- d-.

    \break
    R1*2
    d2.\mf\< r4\!

    c8--\f c8-. r a-. r a-. r4
    c8-- c8-. r a-. r a-. r4
    \break
    c8 c8-. r a-. r g-. r f-.
    r8 f-. r d f4(-> g4-.)
  }
  \alternative {{
    r2 b,8 d d b
    d4-. r8 b'~-> b a es fs
    \break
    d4-. r b8 d d b
    d4-. r8 b'~-> b a es fs 
    \bar ":|]" 
  }{
    \once \override MultiMeasureRestNumber.transparent = ##t
    R1*4^\markup \underline \fontsize #2 "(Ad.lib.)"
    \bar "||"
  }}

  R1*8

  \break
  cs'1\p~
  cs2. ~ cs8 d~->
  d2.~ d8 d~->
  d2.\< r4\mf

  \bar "[|:"
  \repeat volta 4 {
    r4_"cres. poco a poco ..." _"forte on segno" r8 d-> r2
    r4 r8 d-> r2
    \break
    r2 r4 r8 fs-.
  }
  \alternative {{
    r8 fs e d e4-- d-. 
    \bar ":|]" 
  }{
    r8 fs e d e4-- d-.
  }}

  R1*2
  d2.\< r4\!
  \break
  c8--\f c8-. r a-. r a-. r4
  c8-- c8-. r a-. r a-. r4
  \bar "||"
  c8 c8-. r a-. r g-. r f-.
  r8 f-. r d f4(-> g4-.)

  r2 b,8 \f d d b
  d4-. r8 b'~-> b a es fs
  d4-. r b8 d d b
  d4-. r8 b'~-> b a es fs

  d4-. r b8 d d b
  d4-. r8 b'~-> b a es fs
  d4-. r8 b'~-> b a es fs
  d4-. r4 r2
  \bar "||"

  c'8 c8-. r a-. r g-. r f-.
  r8 f-. r d f4-> g4\fermata

  d'1\fermata

  \bar "|."
}

tenorSax = \relative c'' {
  \global
  \key d \major

  \once \override MultiMeasureRestNumber.transparent = ##t
  R1*4

  r2\f b8 d d b
  d4-. r8 b'~-> b a es fs
  d4-. r b8 d d b
  d4-. r8 b'~-> b a es fs

  d4-. r b8 d d b
  d4-. r8 b'~-> b a es fs
  d4-. r8 b'~-> b a es fs
  d4-. r4 r2

  \repeat volta 2 {
    \parenthesize d4-._\markup \normalsize "(only 2nd x)" r4 r2
    \repeat percent 3 {
      r2 a8-.\p g-. r fs~->
      fs1
    }
    R1

    a'1\mf~
    a2. ~ a8 b~->
    b2.~ b8 b~->
    b2.\< r4\!

    r4 r8\f a-> r2
    r4 r8 a-> r2
    r2 r4 r8 fs-.
    r fs e d e4-- d-.

    r4 r8 a'-> r2
    r4 r8 a-> r2
    r2 r4 r8 fs-.
    r fs e d e4-- d-.

    R1*2
    a'2.\mf\< r4\!

    c8--\f c8-. r a-. r a-. r4
    c8-- c8-. r a-. r a-. r4
    c8 c8-. r a-. r g-. r f-.
    r8 f-. r d f4(-> g4-.)
  }
  \alternative {{
    r2 b,8 d d b
    d4-. r8 b'~-> b a es fs
    d4-. r b8 d d b
    d4-. r8 b'~-> b a es fs 
  }{
    \once \override MultiMeasureRestNumber.transparent = ##t
    R1*4
  }}

  R1*8
  a1\p~
  a2. ~ a8 b~->
  b2.~ b8 b~->
  b2.\< r4\mf

  \repeat volta 4 {
    r4_"cres. poco a poco ..."_"forte on segno" r8 a-> r2
    r4 r8 a-> r2
    r2 r4 r8 fs-.
  }
  \alternative {{
    r8 fs e d e4-- d-. 
  }{
    r8 fs e d e4-- d-.
  }}

  R1*2
  a'2.\< r4\!
  c8--\f c8-. r a-. r a-. r4
  c8-- c8-. r a-. r a-. r4
  c8 c8-. r a-. r g-. r f-.
  r8 f-. r d f4(-> g4-.)

  r2 b,8 \f d d b
  d4-. r8 b'~-> b a es fs
  d4-. r b8 d d b
  d4-. r8 b'~-> b a es fs

  d4-. r b8 d d b
  d4-. r8 b'~-> b a es fs
  d4-. r8 b'~-> b a es fs
  d4-. r4 r2

  c'8 c8-. r a-. r g-. r f-.
  r8 f-. r d f4-> g4\fermata

  a1\fermata
}

trombone = \relative c' {
  \global
  \clef bass
  \key d \major

  \once \override MultiMeasureRestNumber.transparent = ##t
  R1*4

  r2\f b8 d d b
  d4-. r8 b~-> b a es fs
  d4-. r b'8 d d b
  d4-. r8 b~-> b a es fs

  d4-. r b'8 d d b
  d4-. r8 b~-> b a es fs
  d4-. r8 b'~-> b a es fs
  d4-. r4 r2

  \repeat volta 2 {
    \parenthesize d4-. r4 r2
    \repeat percent 3 {
      r2 c8-.\p c-. r d~->
      d1
    }
    R1

    e'1\mf~
    e2. ~ e8 fs~->
    fs2.~ fs8 g~->
    g2.\< r4\!

    r4 r8\f d-> r2
    r4 r8 d-> r2
    r2 r4 r8 fs-.
    r fs e d e4-- d-.

    r4 r8 d-> r2
    r4 r8 d-> r2
    r2 r4 r8 fs-.
    r fs e d e4-- d8\breathe fs-.\p

    r fs e d e4-- d4~
    d1~
    d2.\mf\< r4\!

    c8--\f c8-. r a-. r a-. r4
    c8-- c8-. r a-. r a-. r4
    c8 c8-. r a-. r g-. r f-.
    r8 f-. r d f4(-> g4-.)
  }
  \alternative {{
    r2 b8 d d b
    d4-. r8 b~-> b a es fs
    d4-. r b'8 d d b
    d4-. r8 b~-> b a es fs 
  }{
    \once \override MultiMeasureRestNumber.transparent = ##t
    R1*4
  }}

  R1*8
  e'1\p~
  e2. ~ e8 fs~->
  fs2.~ fs8 g~->
  g2.\< r4\mf

  \repeat volta 4 {
    r4 r8 d-> r2
    r4 r8 d-> r2
    r2 r4 r8 fs-.
  }
  \alternative {{
    r8 fs e d e4-- d-. 
  }{
    r8 fs e d e4-- d8\breathe fs-.\p
  }}

  r fs e d e4-- d4~
  d1~
  d2.\< r4\!
  c8--\f c8-. r a-. r a-. r4
  c8-- c8-. r a-. r a-. r4
  c8 c8-. r a-. r g-. r f-.
  r8 f-. r d f4(-> g4-.)

  r2 b8 \f d d b
  d4-. r8 b~-> b a es fs
  d4-. r b'8 d d b
  d4-. r8 b~-> b a es fs

  d4-. r b'8 d d b
  d4-. r8 b~-> b a es fs
  d4-. r8 b'~-> b a es fs
  d4-. r4 r2

  c'8 c8-. r a-. r g-. r f-.
  r8 f-. r d f4-> g4\fermata

  d'1\fermata
}

% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\book {
  \paper { 
    % Descomentar para score
    % #(set-paper-size "a4" 'landscape)
    % indent = 20
    % systems-per-page = 2
  }

  \score {
    % Descomentar para midi
    % \midi { \tempo 4 = 110 }

    % NO USAR layout-set-staff-size O SE ROMPE lilyjazz!!!
    \layout { }

    <<
      \scoreMarkup
      \transpose bf c' \scoreChords
      \new StaffGroup <<
        \new Staff \with 
        { instrumentName = "Trpt."}
        { \transpose bf c' \trumpet } 

        \new Staff \with 
        { instrumentName = "T Sx." \RemoveEmptyStaves }
        { \transpose bf c' \tenorSax } 

        \new Staff \with 
        { instrumentName = "Trbn." \RemoveEmptyStaves } 
        { \trombone }
      >>
    >>
  }
}
