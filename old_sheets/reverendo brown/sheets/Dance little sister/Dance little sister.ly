\version "2.18.2"

% Se debe incluir antes que lilyjazz. Si no, lilyjazz no funciona.
#(set-global-staff-size 20)

\include "main.ily"
\include "music-sheet-paper.ily"

\paper {
  first-page-number = 2
}

\header {
  title = "Dance little sister"
  instrument = "Trumpet, Tenor Sax & Trombone"
  composer = "Terence Trent d'Arby"
  arranger = ""
  poet = "gabriel@sauros.es"
  tagline = ""
  meter = \jazzTempoMarkup "Hard Funk" c4 "105"
}

% Función musical global
global = {
  % Para pintar todos los números de compás debajo del pentagrama
  % \override Score.BarNumber #'break-visibility = #end-of-line-invisible
  % \override Score.BarNumber #'direction = #DOWN
  % \override Score.BarNumber #'extra-offset = #'(1.5 . 0)
  \compressFullBarRests

  \set Timing.beamExceptions = #'()
  \set Timing.baseMoment = #(ly:make-moment 1/4)
  \set Timing.beatStructure = #'(1 1 1 1) 
}

% Elementos comunes de texto (marcas de ensayo, tempo, etc)
scoreMarkup = \new Staff \with { \RemoveEmptyStaves } {
  % Oculta el primer pentagrama del markup
  \override Staff.VerticalAxisGroup #'remove-first = ##t

  % Indicación de tiempo
  % \once \override Score.RehearsalMark.self-alignment-X = #LEFT
  \override Score.RehearsalMark.extra-offset = #'(0 . 2) 

  s1

  s1*8

  \note "Tenor only, answering voice"
  s1*8

  \part "Verse"
  s1*8
  \note "Tenor lead"
  s1*8

  \mark \markup \line { "Chorus" \underline "(Tenor only)" }
  s1*8

  \note "Tenor only, answering voice"
  s1*8

  \part "Verse"
  s1*8

  \note "Tenor lead"
  s1*7

  \note "Unis"
  s1
  \part "Chorus"
  s1*4

  \note "Tenor solo"
  s1*8

  \mark \markup \line { "Chorus" \underline "(Tenor only)" }
  s1*7

  \note "Unis"
  s1
  \part "Chorus"
  s1*8

  \note "Tenor only, answering voice"
  s1*8
}

% Cifrados
scoreChords = \chords {
  s1*69

  d1:m7 
  s2 g:7/d
  d1:m7 
  s2 g:7/d
  d1:m7 
  s2 g:7/d
  d1:m7 
}

% Música
trumpet = \relative c'' {
  \global
  \key d \minor

  R1

  \bar "||"
  R1*8

  \bar "||"
  \break
  R1*8

  \bar "||"
  \break
  d,1 ~ \pp
  d2 \breathe c
  d1 ~ 
  d2 r

  d1 ~ 
  d2 \breathe c
  \break
  d1 ~ 
  d2 r

  f1 ~ \pp
  f2 \breathe e
  f1 ~ 
  f2 r

  \break
  f1 ~ 
  f2 \breathe e
  f1
  R1

  \break
  R1*4
  \break
  R1*4

  \bar "||"
  \break
  R1*4
  \break
  R1*4

  \bar "||"
  \break
  d1 ~ \pp
  d2 \breathe c
  d1 ~ 
  d2 r

  d1 ~ 
  d2 \breathe c
  \pageBreak
  d1 ~ 
  d2 r

  f1 ~ \pp
  f2 \breathe e
  f1 ~ 
  f2 r

  \break
  f1 ~ 
  f2 \breathe e
  f1 
  r4 r8. \f d'16-. f-. g-. a-. g ~ -> g8 f-^

  \break
  \bar "[|:"
  \repeat volta 2 {
    r2 r8. c16 ~ -> c c d8-^
    r2 r16 g8-. g16 ~ -> g g a8-^
    r2 r8. c,16 ~ -> c c d8-^
    r2 a'16( c) c c ~ -> c8 d-^
  }
  \bar ":|]"

  \break 
  R1*4
  \break 
  R1*4
  \break 
  R1*4
  \pageBreak
  R1*3

  r4 r8. \f d,16-. f-. g-. a-. g ~ -> g8 f-^

  \break
  \bar "[|:"
  \repeat volta 2 {
    r2 r8. c16 ~ -> c c d8-^
    r2 r16 g8-. g16 ~ -> g g a8-^
    r2 r8. c,16 ~ -> c c d8-^
    r2 a'16( c) c c ~ -> c8 d-^

    \break
    r2 r8. c,16 ~ -> c c d8-^
    r2 r16 g8-. g16 ~ -> g g a8-^
    r2 r8. c,16 ~ -> c c d8-^
    r4 r8. d16-. f-. g-. a-. g ~ -> g8 f-^
  }
  \bar ":|]"

  \break
  R1*4
  \break
  R1*4
  \bar "|."
}

tenorSax = \relative c'' {
  \global
  \key d \minor

  R1

  R1*8

  r2 r16 \mf a8( c16 ~ c d8 e16 ~
  e2) r
  r2 r8. a,16( g8 c16 a ~
  a2) r

  r2 r16 a8( c16 ~ c d8 e16 ~
  e4 f16 e8 d16 ~ d c a8) r4
  r2 r8. a16( g g c a ~
  a2) r

  R1*7
  r2 r8 \mf d16( cs c4)

  a1 ~
  a2 \breathe g2
  a1 ~
  a2 r4 d4 \glissando
  a1 ~
  a2 \breathe g2
  a1
  r4 r8. \f d16-. f-. g-. a-. g ~ -> g8 f-^

  r2 r8. c16 ~ -> c c d8-^
  r2 r16 g8-. g16 ~ -> g g a8-^
  r2 r8. c,16 ~ -> c c d8-^
  r2 g16( a) c c ~ -> c8 d-^

  r2 r8. c,16 ~ -> c c d8-^
  r2 r16 g8-. g16 ~ -> g g a8-^
  r2 r8. c,16 ~ -> c c d8-^
  r2 g16( a) c c ~ -> c8 d-^

  r2 r16 \mf a,8( c16 ~ c d8 e16 ~
  e2) r
  r2 r8. a,16( g8 c16 a ~
  a2) r

  r2 r16 a8( c16 ~ c d8 e16 ~
  e4 f16 e8 d16 ~ d c a8) r4
  r2 r8. a16( g g c a ~
  a2) r

  R1*7
  r2 r8 \mf d16( cs c4)

  a1 ~
  a2 \breathe g2
  a1 ~
  a2 r4 d4 \glissando
  a1 ~
  a2 \breathe g2
  a1
  r4 r8. \f d16-. f-. g-. a-. g ~ -> g8 f-^

  \repeat volta 2 {
    r2 r8. c16 ~ -> c c d8-^
    r2 r16 g8-. g16 ~ -> g g a8-^
    r2 r8. c,16 ~ -> c c d8-^
    r2 a'16( c) c c ~ -> c8 d-^
  }

  \impro 28

  r4 r8. \f d,16-. f-. g-. a-. g ~ -> g8 f-^

  r2 r8. c16 ~ -> c c d8-^
  r2 r16 g8-. g16 ~ -> g g a8-^
  r2 r8. c,16 ~ -> c c d8-^
  r2 g16( a) c c ~ -> c8 d-^

  r2 r8. c,16 ~ -> c c d8-^
  r2 r16 g8-. g16 ~ -> g g a8-^
  r2 r8. c,16 ~ -> c c d8-^
  r4 r8. \f d16-. f-. g-. a-. g ~ -> g8 f-^

  \repeat volta 2 {
    r2 r8. c16 ~ -> c c d8-^
    r2 r16 g8-. g16 ~ -> g g a8-^
    r2 r8. c,16 ~ -> c c d8-^
    r2 a'16( c) c c ~ -> c8 d-^

    r2 r8. c,16 ~ -> c c d8-^
    r2 r16 g8-. g16 ~ -> g g a8-^
    r2 r8. c,16 ~ -> c c d8-^
    r4 r8. d16-. f-. g-. a-. g ~ -> g8 f-^
  }

  r2 r16 \mf a,8( c16 ~ c d8 e16 ~
  e2) r
  r2 r8. a,16( g8 c16 a ~
  a2) r

  r2 r16 a8( c16 ~ c d8 e16 ~
  e4 f16 e8 d16 ~ d c a8) r4
  r2 r8. a16( g g c a ~
  a2) r
}

trombone = \relative c' {
  \global
  \key d \minor
  \clef bass

  R1

  R1*8

  R1*8

  d,1 ~ \pp
  d2 \breathe c
  d1 ~ 
  d2 r

  d1 ~ 
  d2 \breathe c
  d1 ~ 
  d2 r

  d1 ~ \pp
  d2 \breathe c
  d1 ~ 
  d2 r

  d1 ~ 
  d2 \breathe c
  d1
  R1

  R1*8

  R1*8

  d1 ~ \pp
  d2 \breathe c
  d1 ~ 
  d2 r

  d1 ~ 
  d2 \breathe c
  d1 ~ 
  d2 r

  d1 ~ \pp
  d2 \breathe c
  d1 ~ 
  d2 r

  d1 ~ 
  d2 \breathe c
  d1
  r4 r8. \f d16-. f-. g-. a-. g ~ -> g8 f-^

  \repeat volta 2 {
    r2 r8. c16 ~ -> c c d8-^
    r2 r16 g8-. g16 ~ -> g g a8-^
    r2 r8. c,16 ~ -> c c d8-^
    r2 a'16( c) c c ~ -> c8 d-^
  }

  R1*8
  R1*7

  r4 r8. \f d,16-. f-. g-. a-. g ~ -> g8 f-^

  \repeat volta 2 {
    r2 r8. c16 ~ -> c c d8-^
    r2 r16 g8-. g16 ~ -> g g a8-^
    r2 r8. c,16 ~ -> c c d8-^
    r2 a'16( c) c c ~ -> c8 d-^

    r2 r8. c,16 ~ -> c c d8-^
    r2 r16 g8-. g16 ~ -> g g a8-^
    r2 r8. c,16 ~ -> c c d8-^
    r4 r8. d16-. f-. g-. a-. g ~ -> g8 f-^
  }

  R1*8
}

% Impresión de la partitura
% (Sólo se imprime el primer \book que encuentra Lilypond!! ojo al incluir archivos)
% ~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.<>.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~.:.~
\book {
  \paper { 
    % Descomentar para score
    % #(set-paper-size "a4" 'landscape)
    % indent = 20
    % systems-per-page = 2
  }

  \score {
    % Descomentar para midi
    % \midi { \tempo 4 = 110 }

    % NO USAR layout-set-staff-size O SE ROMPE lilyjazz!!!
    \layout { }

    <<
      \scoreMarkup
      \transpose bf c' \scoreChords
      \new StaffGroup <<
        \new Staff \with 
        { instrumentName = "Trpt." \RemoveEmptyStaves }
        { \transpose bf c' \trumpet } 

        \new Staff \with 
        { instrumentName = "T Sx."}
        { \transpose bf c' \tenorSax } 

        \new Staff \with 
        { instrumentName = "Trbn." \RemoveEmptyStaves } 
        { \trombone }
      >>
    >>
  }
}
