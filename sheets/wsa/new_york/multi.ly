\version "2.24.1"
\language "english"


\include "main.ily"
% \include "fonts/lilyjazz.ily"

\header {
  title = "New York State of Mind"
  instrument = ""
  composer = "Alicia Keys"
  arranger = "Arr: Gabriel Palacios"
  poet = ""
  tagline = ""
}

marks = {
  R2.

  \bar "||"
  R2.
}


harmony = \chords {
  c
}

lead = \relative c' {
  \key f \major
  \time 4/4
  
  c'1 ~
  c2 ~ c8 bf a g ~
  g8 a ~ a2. ~
  a2 r2

  c1 ~
  c2 ~ c8 bf a g ~
  g8 a ~ a2. ~
  a2 r2

  r4 c,8 c16 c ~ c a8 g16 ~ g8 c16 c
  c8 c16 c ~ c a8 g16 r8. g16 g8 f
  bf8 a ~ a2.
  R2.

  r4 c8 c16 c ~ c a8 g16 ~ g8 c16 c
  c8 c16 c ~ c a8 g16 ~ g g8 g16 g8 f
  r8 bf8 a2.
  R2.

  r4 f'8 e16 f ~ f e8 c16 ~ c8 f16 f
  f8 f16 f ~ f e8 c16 ~ c c8 c16 ~ c a8.
  r8 d c4 \acciaccatura {d16 c} bf4 ~ bf8  c16 d
  R2.

  r4 f8 e16 f ~ f e8 c16 ~ c8 f16 f
  f8 f16 f ~ f e8 c16 ~ c c8 c16 ~ c a8.
  r8 d c4 \acciaccatura {d16 c} bf4 ~ bf8  c16 d
  R2.

  r8 e16 e e e8 e16 ~ e e8 e16 ~ e e e e
  e8 e16 e ~ e e e8 r16 e8 e16 e8 a, 

  r8 g' ~ g a ~ a4 r
  g8 a a a a a g c ~
  c8 a8 r a a a g c ~
  c8 a g r f g f g ~

  g8 a ~ a2 r4
  r8 f f f f g f c' ~
  c a r f f g f c' ~
  c a g r f f f g ~

  g a r g ~ g a r g ~
  g a ~ a2 r4

  r4 f'8 e16 f ~ f e8 c16 ~ c8 f16 f
  f8 f16 f ~ f e8 c16 ~ c c8 c16 ~ c a8.
  r8 d c4 \acciaccatura {d16 c} bf4 ~ bf8  c16 d
  R2.

  r4 f8 e16 f ~ f e8 c16 ~ c8 f16 f
  f8 f16 f ~ f e8 c16 ~ c c8 c16 ~ c a8.
  r8 d c4 \acciaccatura {d16 c} bf4 ~ bf8  c16 d
  R2.
}

% Saxophones

altoI = \relative c' {
  \key f \major
  \time 3/4
  
  d4 e f
  
  g4 a2 ~
  a2 r4
  g4 a2 ~
  a2 r4

  c4 bf a
  g4 a2 ~
  a2 r4
  c4 bf a

  g4 a2 ~
  a2 r4
  g4 a2 ~
  a2 r4

  r4 f f
  f g f 
  c'2. ~
  c4 r4 bf
  \time 4/4
  c2 r8 f, f e

  f1
}

altoII = \relative c' {
  \key f \major
  \time 3/4

  b4 c cs
  
  d4 f2 ~
  f2 r4
  c4 f2 ~
  f2 r4

  c4 bf a
}

tenorI = \relative c' {
  \key f \major
  \time 3/4

  a4 a af
  
  g2. ~
  g2 r4
  g2. ~
  g2 r4
}

tenorII = \relative c' {
  \key f \major
  \time 3/4

  f,4 f ef
  
  f2. ~
  f2 r4
  f2. ~
  f2 r4
}

baritone = \relative c' {
  \key f \major
  \time 3/4

  d,4 c b
  
  bf?2. ~
  bf2 r4
  a2. ~
  a2 r4

  g2. ~
  g2.
}

% Trumpets

trumpetI = \relative c' {
  \key f \major
  \time 3/4

  R2.
}

trumpetII = \relative c' {
  \key f \major
  \time 3/4

  R2.
}

trumpetIII = \relative c' {
  \key f \major
  \time 3/4

  R2.
}

trumpetIV = \relative c' {
  \key f \major
  \time 3/4

  R2.
}

% Trombones

tromboneI = \relative c' {
  \key f \major
  \time 3/4

  R2.

  r2 c,4
  c'2. ~
  c4 r c,
  c'2. ~

  c2 r4
}

tromboneII = \relative c' {
  \key f \major
  \time 3/4

  R2.

  r2 c,4
  c'2. ~
  c4 r c,
  c'2. ~

  c2 r4
}

tromboneIII = \relative c' {
  \key f \major
  \time 3/4

  R2.

  r2 c,4
  c'2. ~
  c4 r c,
  c'2. ~

  c2 r4
}

tromboneIV = \relative c' {
  \key f \major
  \time 3/4

  R2.

  r2 c,4
  c'2. ~
  c4 r c,
  c'2. ~

  c2 r4
}

% Rhythm section

pianoR = \relative c' {
  \key f \major
  \time 3/4

  R2.
}

pianoL = \relative c' {
  \key f \major
  \time 3/4

  R2.
}

guitar = \relative c' {
  \key f \major
  \time 3/4

  R2.
}

bass = \relative c' {
  \key f \major
  \time 3/4

  R2.
}

drumpart = \relative c' {
  \key f \major
  \time 3/4

  R2.
}

full = <<
  % \new Staff \with { instrumentName = "Lead" } \lead
  \new Staff \with { \RemoveAllEmptyStaves } { \marks }
  \new StaffGroup <<
    \new Staff \with { instrumentName = "Alto sax I" }   \transpose ef c' \altoI
    \new Staff \with { instrumentName = "Alto sax II" }  \transpose ef c' \altoII
    \new Staff \with { instrumentName = "Tenor sax I" }  \transpose bf c'' \tenorI
    \new Staff \with { instrumentName = "Tenor sax II" } \transpose bf c'' \tenorII
    \new Staff \with { instrumentName = "Baritone sax" } \transpose ef c'' \baritone
  >>
  \new StaffGroup <<
    \new Staff \with { instrumentName = "Trumpet I" }   \transpose bf c' \trumpetI
    \new Staff \with { instrumentName = "Trumpet II" }  \transpose bf c' \trumpetII
    \new Staff \with { instrumentName = "Trumpet III" } \transpose ef c' \trumpetIII
    \new Staff \with { instrumentName = "Trumpet IV" }  \transpose ef c' \trumpetIV
  >>
  \new StaffGroup <<
    \new Staff \with { instrumentName = "Trombone I" }   \transpose bf c'' \tromboneI
    \new Staff \with { instrumentName = "Trombone II" }  \transpose bf c'' \tromboneII
    \new Staff \with { instrumentName = "Trombone III" } \transpose bf c'' \tromboneIII
    \new Staff \with { instrumentName = "Trombone IV" }  \transpose ef c'' \tromboneIV
  >>
  \new StaffGroup <<
    \new Staff \with { instrumentName = "Guitar" } \guitar
    \new PianoStaff \with { instrumentName = "Piano" } << 
      \new Staff \pianoR
      \new Staff \pianoL
    >>
    \new Staff \with { instrumentName = "Bass" } \bass
    \new DrumStaff \with { instrumentName = "Drums" } \drumpart
  >>
>>

full_midi = <<
  % \new Staff \with { instrumentName = "Lead" } \lead
  \new Staff \with { \RemoveAllEmptyStaves } { \marks }
  \new StaffGroup <<
    \new Staff \with { instrumentName = "Alto sax I" midiInstrument = "alto sax" }       \altoI
    \new Staff \with { instrumentName = "Alto sax II" midiInstrument = "alto sax" }      \altoII
    \new Staff \with { instrumentName = "Tenor sax I" midiInstrument = "tenor sax" }     \tenorI
    \new Staff \with { instrumentName = "Tenor sax II" midiInstrument = "tenor sax" }    \tenorII
    \new Staff \with { instrumentName = "Baritone sax" midiInstrument = "baritone sax" } \baritone
  >>
  \new StaffGroup <<
    \new Staff \with { instrumentName = "Trumpet I" midiInstrument = "trumpet" }   \trumpetI
    \new Staff \with { instrumentName = "Trumpet II" midiInstrument = "trumpet" }  \trumpetII
    \new Staff \with { instrumentName = "Trumpet III" midiInstrument = "trumpet" } \trumpetIII
    \new Staff \with { instrumentName = "Trumpet IV" midiInstrument = "trumpet" }  \trumpetIV
  >>
  \new StaffGroup <<
    \new Staff \with { instrumentName = "Trombone I" midiInstrument = "trombone" }   \tromboneI
    \new Staff \with { instrumentName = "Trombone II" midiInstrument = "trombone" }  \tromboneII
    \new Staff \with { instrumentName = "Trombone III" midiInstrument = "trombone" } \tromboneIII
    \new Staff \with { instrumentName = "Trombone IV" midiInstrument = "trombone" }  \tromboneIV
  >>
  \new StaffGroup <<
    \new Staff \with { instrumentName = "Guitar" } \guitar
    \new PianoStaff \with { instrumentName = "Piano" } << 
      \new Staff \pianoR
      \new Staff \pianoL
    >>
    \new Staff \with { instrumentName = "Bass" } \bass
    \new DrumStaff \with { instrumentName = "Drums" } \drumpart
  >>
>>

% sheet = <<
%   \harmony
%   \new Staff \with { \RemoveAllEmptyStaves } { \marks }

%   \new StaffGroup <<
%     \new Staff \with 
%     { instrumentName = "Eb" }
%     { \transpose ef c' \partEb } 

%     \new Staff \with 
%     { instrumentName = "Bb" }
%     { \transpose bf c'' \partBb } 
%   >>
% >>

#(set-global-staff-size 12)
\book {
  \paper {
    #(set-paper-size "a4" 'landscape)
    #(define fonts
      (set-global-fonts
      #:music "lilyjazz"
      #:brace "lilyjazz"
      #:roman "lilyjazz-text"
      #:sans "lilyjazz-chord"
      #:factor (/ staff-height pt 20)
    ))
  }
  \score { 
    \full
  }
}

\book {
  \paper {}
  \score { 
    \full_midi
    \midi { \tempo 4 = 100 }
  }
}
