\version "2.24.1"
\language "english"

#(set-global-staff-size 20)

\include "main.ily"
\include "fonts/lilyjazz.ily"

\header {
  title = "Alcohol"
  instrument = ""
  composer = ""
  arranger = ""
  poet = ""
  tagline = ""
}

marks = {
  \tweak #'padding #10
  \tempo \markup {
    "Shuffle"
    \hspace #1
    \rhythm { 8[ 8] } = \rhythm { \tuplet 3/2 { 4 8 } }
  }

  s4

  \bar "[|:"
  s1*16
  \bar ":|]"
}


harmony = \chords {
  s4

  g1 s g d
  c2 d g e:m
  c2 d g s

  g1 s d g
  g1 s g2/d d g1
}

% Música
lead = \relative c' {
  \voiceOne
  \overrideTimeSignatureSettings 4/4 1/4 1,1,1,1 #'()
  \key g \major
  \time 4/4
  
  \partial 4
  \stemDown
  b'4

  \repeat volta 2 {
    g2. b4
    g2. b4
    g4 b g b
    a2 r8 a a b

    c4 c r8 c b a
    b4 g r8 g g b
    a4 d, fs a
    g2 r4 \xNotesOn \stemUp g8 g

    g4 \magnifyMusic 0.65 { g8 g g4 } g8 g8
    g4 \magnifyMusic 0.65 { g8 g g4 } \xNotesOff g8 b8
    a4. d,8 fs d fs a
    g2 r4 \xNotesOn g8 g

    g4 \magnifyMusic 0.65 { g8 g g4 } g8 g8
    g4 \magnifyMusic 0.65 { g8 g g4 } \xNotesOff c8 c8
    b4. d,8 fs d fs a
    g2 r4 \stemDown b4
  }
}

back = \relative c' \magnifyMusic 0.8 {
  \voiceTwo
  \overrideTimeSignatureSettings 4/4 1/4 1,1,1,1 #'()
  \key g \major
  \time 4/4
  
  \partial 4
  \stemUp
  d'4

  \repeat volta 2 {
    b2. d4
    b2. d4
    b4 d b d
    c2 \once \normalsize r8 c c d

    e4 e \once \normalsize r8 e d c
    d4 b \once \normalsize r8 b b d
    c4 fs, a c
    b2 r4 s4

    s1*3
    s2 r4 s4

    s1*3
    s2 r4 d4
  }
}

lyric = \lyricmode {
  Al -- cohol, al -- cohol
  al -- cohol, al -- cohol, al -- cohol,
  he -- mos ve -- ni -- do a_em -- bo -- rra -- char -- nos,
  el re -- sul ta -- do nos da_i -- gual.

  A -- llá va (a -- llá va), a -- llá va (a -- llá va),
  << 
    { a -- llá va -- mos to -- dos a triun -- far. }
    \new Lyrics { a -- llá va la pe -- ña a_a -- ni -- mar. }
  >>

  Y -- si no (y -- si no), y -- si no (y -- si no),
  << 
    { en los ba -- res va -- mos a_ol -- vi dar. }
    \new Lyrics { nos la pe -- la va -- mos a_a -- rra -- sar. }
  >>

  Al
}


sheet = <<
  \harmony
  \new Staff \with { \RemoveAllEmptyStaves } { \marks }

  \new Staff <<
    \new Voice { \lead } \addlyrics { \lyric }
    \new Voice { \back }
  >>
>>

\book {
  \bookOutputName "Alcohol"
  \score { \sheet }
}

\book {
  \bookOutputName "Alcohol - Eb"
  \score { \transpose ef c' \sheet }
}

\book {
  \bookOutputName "Alcohol - Bb"
  \score { \transpose bf c' \sheet }
}
