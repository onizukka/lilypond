\version "2.24.1"
\language "english"

#(set-global-staff-size 20)

\include "main.ily"
\include "fonts/lilyjazz.ily"

\header {
  title = "Bares, qué lugares"
  instrument = ""
  composer = ""
  arranger = ""
  poet = ""
  tagline = ""
}

marks = {
  s1

  \bar "[|:"
  \textMark \segno
  s1
  \bar "||"
  s1*2
  \break
  s1*2
  
  s1*2
  \break
  s1*2

  s1*2
  \break
  s1*2

  s1*3
  \bar ":|]"
  \textEndMark \alCoda
  s1
  \bar "||"

  s1*4
  \break
  s1*4
  \bar "||"
  \break

  s1*4
  \break
  s1*3
  \break
  \textEndMark \dalSegnoAlCoda
  \bar "||"
  \textMark \coda
  s1
  s1*4
  \break
  s1*4
  \break
  s1*4
  \bar "|."
}


harmony = \chords {
  a1:m s

  a1:m s b:7 e
  a1:m s b:7 e
  d1:m a:m b:7 e
  d1:m a2:m b4:7 e
  a1:m s

  a1 s b:m7 e
  a1 s b:m7 e
  d1:m a:m b:7 e
  d1:m a2:m b4:7 e
  a1:m a1:m

  d1:m a:m b:7 e
  d1:m a2:m b4:7 e
  a1 a2:m b4:7 e
  a1:m s b2:7 e
  a1:m
}

% Música
lead = \relative c' {
  \voiceOne
  \overrideTimeSignatureSettings 4/4 1/4 1,1,1,1 #'()
  \key a \minor
  \time 4/4

  R1

  \repeat volta 2 {
    r2 r4 r8 e

    a2. c4
    f4. e8 d c b a 
    b2. ~ b8 a
    b8 c4 b8 r4 r8 e,

    a2. c4
    f4. e8 d c b a 
    b2. ~ b8 a
    b8 c4 b8 r2

    f'4 d ~ d8 e4 d8
    e4 c2 r8 c
    b4 b b8 c d c
    b2. r4

    f'4 d ~ d8 e4 d8
    e4 d8 c b4 a8 gs
    a1
  }

  r2 r4 r8 e

  \key a \major
  a2. cs4
  fs4. e8 d cs b a 
  b2. ~ b8 a
  b8 cs4 b8 r4 r8 e,

  a2. cs4
  fs4. e8 d cs b a 
  b2. ~ b8 a
  b8 cs4 b8 r2

  \key a \minor
  f'4 d ~ d8 e4 d8
  e4 c2 r8 c
  b4 b b8 c d c
  b2. r4

  f'4 d ~ d8 e4 d8
  e4 d8 c b4 a8 gs
  a1
  R1

  f'4 d ~ d8 e4 d8
  e4 c2 r8 c
  b4 b b8 c d c
  b2. r4

  f'4 d ~ d8 e4 d8
  e4 d8 c b4 a8 gs
  a2 r8 e'4 d8
  e4 d8 c b4 a8 gs

  a2 r8 e'4 d8
  e2 d4 c
  b2 a4 gs
  a1
}

back = \relative c' \magnifyMusic 0.8 {
  \voiceTwo
  \overrideTimeSignatureSettings 4/4 1/4 1,1,1,1 #'()
  \key a \minor
  \time 4/4
  
  R1

  \repeat volta 2 {
    r2 r4 r8 e

    c2. e4
    a4. c8 b a e c
    ds2. ~ ds8 fs
    gs8 a4 gs8 r4 r8 e

    c2. e4
    a4. c8 b a e c
    ds2. ~ ds8 fs
    gs8 a4 gs8 r2

    r8 gs4 a8 ~ a b4 a8 ~
    a8 gs4 a8 ~ a b4 a8 ~
    a8 gs4 a8 ~ a b4 a8
    gs2. r4

    r8 gs4 a8 ~ a b4 a8 ~
    a8 gs4 a8 ~ a b4 gs8
    s1
  }

  r2 r4 r8 e

  \key a \major
  cs2. e4
  a4. cs8 b a e cs 
  d2. ~ d8 fs
  gs8 a4 gs8 r4 r8 e

  cs2. e4
  a4. cs8 b a e cs 
  d2. ~ d8 fs
  gs8 a4 gs8 r2

  \key a \minor
  r8 gs4 a8 ~ a b4 a8 ~
  a8 gs4 a8 ~ a b4 a8 ~
  a8 gs4 a8 ~ a b4 a8
  gs2. r4

  r8 gs4 a8 ~ a b4 a8 ~
  a8 gs4 a8 ~ a b4 gs8
  s1
  R1

  r8 gs4 a8 ~ a b4 a8 ~
  a8 gs4 a8 ~ a b4 a8 ~
  a8 gs4 a8 ~ a b4 a8
  gs2. r4

  r8 gs4 a8 ~ a b4 a8 ~
  a8 gs4 a8 ~ a b4 gs8
  a2 r8 c4 b8
  c4 b8 a gs4 fs8 e

  a2 r8 c4 b8
  c2 b4 a
  gs2 fs4 e
  a,1
}

lyric_a = \lyricmode {

  << { 
    A -- mor la no -- che_ha si -- do lar -- ga_y lle -- na de_e -- mo -- ción
    Pe -- ro_a -- ma -- ne -- ce y_a -- pe -- te -- ce_es -- tar jun -- tos los dos
    Ba -- res, qué lu -- ga -- res, tan gra -- tos pa -- ra con -- ver -- sar
    No_hay co -- mo_el ca -- lor del a -- mor en un bar
  } \new Lyrics {
    A -- mor no_he sa -- bi -- do_en -- con -- trar el mo -- men -- to jus -- to
    Pues con el frí -- o de la no -- che no_es -- ta -- ba_a__a -- gus -- to
    Mo -- zo, pon -- ga_un tro -- zo de ba -- yo -- ne -- sa_y un ca -- fé
    Que_a la se -- ño -- ri -- ta la_in -- vi -- ta mon -- sieur
  } \new Lyrics {
    A -- mor aun -- que_a_es -- tas ho -- ras ya no_es toy muy en -- te -- ro
    Al fin lle -- gó_el mo -- men -- to de de -- cir -- lo te quie -- ro
    Po -- llo, o -- tro bo -- llo no me ten -- ga que le -- van -- tar
    No_hay co -- mo_el ca -- lor del a -- mor en un bar
  } >>

  Y dos a -- lon -- dras nos ob -- ser -- van sin gran in -- te -- rés
  El ca -- ma -- re -- ro_es -- tá le -- yen -- do_el As con á -- vi -- dez
  Ba -- res, qué lu -- ga -- res, tan gra -- tos pa -- ra con -- ver -- sar
  No_hay co -- mo_el ca -- lor del a -- mor en un bar

  Je -- fe, no se que -- je y sir -- va_o tra co -- pi -- ta más
  No_hay co -- mo_el ca -- lor del a -- mor en un bar
  El ca -- lor del a -- mor en un bar
  El ca -- lor del a -- mor en un bar
}

lyric_b = \lyricmode {
}


sheet = <<
  \harmony
  \new Staff \with { \RemoveAllEmptyStaves } { \marks }

  \new Staff <<
    \new Voice { \lead } 
      \addlyrics { \lyric_a }
      \addlyrics { \lyric_b }
    \new Voice { \back }
  >>
>>

\book {
  \bookOutputName "Bares, qué lugares"
  \score { \sheet }
}

\book {
  \bookOutputName "Bares, qué lugares - Eb"
  \score { \transpose ef c' \sheet }
}

\book {
  \bookOutputName "Bares, qué lugares - Bb"
  \score { \transpose bf c' \sheet }
}
