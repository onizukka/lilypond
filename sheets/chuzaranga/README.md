# Chuzaranga setlists

## Coros

- [x] A por ellos
- [x] Alcohol
- [x] Amapola
- [ ] Quizás
- [ ] Tírate de la moto
- [ ] Era una sandía
- [ ] Mari Carmen
- [ ] I will survive
- [ ] **Seven nation army**

## Folklore español

- [ ] Potpourri castellano
- [ ] Mariano (El provinciano)
- [ ] Desde santurce a Bilbao
- [ ] El gato montés
- [ ] Paquito el chocolatero
- [ ] **Puente de aranda**
- [ ] **Que si que**
- [ ] **Redoble, redoble**
- [ ] **Muñeiras**

## Populares

- [ ] Cafetal
- [x] Bares qué lugares
- [ ] Fiesta
- [ ] Los managers
- [ ] Yellow submarine
- [ ] Tengo un tractor amarillo
- [ ] **Mala vida**
- [ ] **Tras la barra del bar**
- [ ] **Soldadito marinero**
- [ ] **Los coches chocones**

## Punk/Rock/Ska

- [ ] Chus
- [ ] Lola
- [ ] Madness
- [ ] Que se vayan
- [ ] Sarri sarri
- [ ] Si nos dejais
- [ ] Desobediencia
- [ ] Wipe out
- [ ] **Fuel & Fire**
- [ ] **Solo vivir**
- [ ] **The final countdown**

## Marcha (Samba & co)

- [ ] Gimme hope Johanna
- [ ] When the saints go marching in
- [ ] Yo te quiero dar
- [ ] Lambada
- [ ] Tequila
- [ ] **Matador**
- [ ] **Carnaval carnaval**
- [ ] **Samba de Janeiro**
- [ ] **Maria Caipirinha**


## Italianas

- [ ] Bella ciao
- [ ] Licor café
- [ ] Tarantella
- [ ] Tu vuo fa l'americano
- [ ] **O sole mio**
- [ ] **Volare**
- [ ] **Amarcord**
- [ ] **La fogaraccia (Amarcord)**
- [ ] **La manine di primavera**

## Internacional

- [ ] Danza Húngara
- [ ] Tetris
- [ ] Kalinka
- [ ] Potpourri mexicanas (Adelita, Allá en el rancho grande, Cielito lindo, La cucaracha, La raspa)
- [ ] El Rey
- [ ] Bubamara
- [ ] Misirlou
- [ ] **La bamba**
- [ ] **Hava Nagila**

## Swing/Niños

- [ ] Príncipe azul
- [ ] Bob esponja
- [ ] Quiero ser como tu (Libro de la selva)
- [ ] **Monsters, Inc.**
- [ ] **Hay un amigo en mí**
- [ ] **Bajo del mar**
- [ ] **Hakuna matata**
- [ ] **Busca lo más vital**
- [ ] **Frozen**
- [ ] **Sing, sing, sing**
