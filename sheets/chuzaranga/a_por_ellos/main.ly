\version "2.24.1"
\language "english"

#(set-global-staff-size 20)

\include "main.ily"
\include "fonts/lilyjazz.ily"

\header {
  title = "A por ellos"
  instrument = ""
  composer = ""
  arranger = ""
  poet = ""
  tagline = ""
}

marks = {
  \tweak #'padding #10
  \tempo \markup {
    "Shuffle"
    \hspace #1
    \rhythm { 8[ 8] } = \rhythm { \tuplet 3/2 { 4 8 } }
  }

  s4

  \bar "[|:"
  s1*8
  \bar ":|]"
  s1

  \bar "|."
}


harmony = \chords {
  s4

  g1 c:6 d:7 g
  g2 g:7 c1 g2/d d:7 g d:7
  g1
}

% Música
lead = \relative c' {
  \voiceOne
  \key g \major
  \time 4/4
  
  \partial 4
  g'8 a

  \repeat volta 2 {
    b4 g2 b4
    a2 r4 fs8 g
    a4 fs2 a4
    g2 r4 g8 a

    b4 g2 b4
    c2 r4 c8 c
    b4 g a fs
  }
  \alternative {{ 
    g2 r4 g8 a
  }{ 
    g2 r2
  }}
}

back = \magnifyMusic 0.8 \relative c' {
  \voiceTwo
  \key g \major
  \time 4/4
  
  \partial 4
  e8 fs

  \repeat volta 2 {
    g4 d2 g4
    e2 r4 d8 e
    fs4 d2 c4
    b2 r4 b8 d

    g2 f
    e2 r4 g8 a
    g4 d fs d
  }
  \alternative {{
    g,2 r4 e'8 fs
  }{ 
    g,2 r2
  }}
}

chorus = \lyricmode {
  A por e -- llos o -- ee
  A por e -- llos o -- ee
  A por e -- llos o -- ee
  A por e -- llos e -- o -- ee
  A por ee
}

sheet = <<
  \harmony
  \new Staff \with { \RemoveAllEmptyStaves } { \marks }

  \new Staff <<
    \new Voice { \lead } \addlyrics { \chorus }
    \new Voice { \back } 
  >>
>>

\book {
  \bookOutputName "A por ellos"
  \score { \sheet }
}

\book {
  \bookOutputName "A por ellos - EB"
  \score { \transpose ef c' \sheet }
}

\book {
  \bookOutputName "A por ellos - Bb"
  \score { \transpose bf c'' \sheet }
}
