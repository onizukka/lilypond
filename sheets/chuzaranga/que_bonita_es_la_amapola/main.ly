\version "2.24.1"
\language "english"

#(set-global-staff-size 20)

\include "main.ily"
\include "fonts/lilyjazz.ily"

\header {
  title = "Qué bonita es la amapola"
  instrument = ""
  composer = ""
  arranger = ""
  poet = ""
  tagline = ""
}

marks = {
  s1
  \bar "[|:"
  s1

  \break
  s1*3
  \break
  s1*4
  \break
  s1*4
  \break
  s1*4
  \break
  s1*3
  \break
  s1*3
  \break
  s1*2
  \bar ":|]"
  s1*2
  \bar "|."
}


harmony = \chords {
  g1:m s

  d1:7 s g:m s
  d1:7 s g:m g:7
  c1:m f bf ef
  a:dim d:7 g:m g:7
  c1:m f bf ef
  a:dim d:7 

  g:m
  g:m s
}

% Música
lead = \relative c' {
  \voiceOne
  \overrideTimeSignatureSettings 4/4 1/4 1,1,1,1 #'()
  \key g \minor
  \time 4/4

  g'4 r8 fs g fs g fs 

  \repeat volta 2 {
    g r8 bf bf bf bf bf a

    c8 a4 g8 fs2
    r8 c'4 c8 c c c bf
    d8 bf4 a8 g2
    r8 bf4 bf8 bf bf bf a

    c8 a4 g8 fs2
    r8 a4 bf8 c a bf c
    ef d ~ d2. 
    r8 d4 d8 g f ef d

    f8 ef ~ ef2.
    r8 c4 c8 f ef d c
    ef d ~ d2.
    r8 bf4 bf8 ef d c bf

    d8 c ~ c2.
    r8 d4 ef8 d c bf c
    ef d ~ d2.
    r8 d4 d8 g f ef d

    f8 ef ~ ef2.
    r8 c4 c8 f ef d c
    ef d ~ d2.
    r8 bf4 bf8 ef d c bf

    d8 c ~ c2.
    r8 d4 ef8 d c bf a
  }
  \alternative {{
    a8 g r fs g fs g fs 
  }{
    g4 r8 fs g fs g fs 
    g1
  }}
}

back = \relative c' \magnifyMusic 0.8 {
  \voiceTwo
  \overrideTimeSignatureSettings 4/4 1/4 1,1,1,1 #'()
  \key g \minor
  \time 4/4
  
  g'4 r8 fs g fs g fs

  \repeat volta 2 {
    g r8 g g g g g fs

    a8 fs4 ef8 d2
    r8 a'4 a8 a a a g
    bf8 g4 ef8 d2
    r8 g4 g8 g g g fs

    a8 fs4 ef8 d2
    r8 fs4 g8 a fs g a
    c bf4. g4 a 
    b4. b8 b g a b

    d8 c ~ c4 c bf 
    a4. a8 a f g a
    c bf ~ bf4 bf a
    g4. g8 g ef f g

    bf8 a ~ a4 a g
    fs4. c'8 bf a g a
    c bf ~ bf4 g a 
    b4. b8 b g a b

    d8 c ~ c4 c bf 
    a4. a8 a f g a
    c bf ~ bf4 bf a
    g4. g8 g ef f g

    bf8 a ~ a4 a g
    fs4. c'8 bf a g fs
  }
  \alternative {{
    fs8 g r fs g fs g fs 
  }{
    g4 r8 fs g fs g fs 
    s1
  }}
}

lyric_a = \lyricmode {
  \repeat unfold 7 { \skip 1 }

  Qué bo -- ni -- ta_es la_a -- ma -- po -- la_ay ay ay
  Cuan -- do na -- ce_en los tri -- ga -- les ay ay
  Más bo -- ni -- ta_es -- tá mi ni -- ña_ay ay ay
  Cuan -- do_a la ven -- ta -- na sa -- le

  Que me_han di -- cho que te di -- ga
  Que_en la e -- ra no se jo -- de
  Que pa' e -- so_es -- tán las ca -- mas
  que tie -- nen bue -- nos col -- cho -- nes

  Que me_han di -- cho que te di -- ga
  Que_en la e -- ra no se jo -- de
  Por -- que_hay car -- dos bo -- rri -- que -- ros
  Que me pin -- chan los co -- jo -- nes
}

lyric_b = \lyricmode {
  \repeat unfold 7 { \skip 1 }

  Aho -- ra si que_es -- ta -- mos bue -- nos ay ay
  Tú pre -- ña -- da_y yo_en la car -- cel ay ay
  Tú no tie -- nes quién te me -- ta_ay ay ay
  Yo no ten -- go quién me sa -- que

  Y_el pa -- trón de la cha -- ran -- ga
  Es -- tá lo -- co de con -- ten -- to
  Por -- que tie -- ne_u -- na cha -- va -- la
  Que le lim -- pia_el ins -- tru -- men -- to

  Y_el pa -- trón de la cha -- ran -- ga
  Es -- tá lo -- co de con -- ten -- to
  Por -- que tie -- ne_u -- na cha -- va -- la
  Que le lim -- pia_el ins -- tru -- men -- to
}


sheet = <<
  \harmony
  \new Staff \with { \RemoveAllEmptyStaves } { \marks }

  \new Staff <<
    \new Voice { \lead } 
      \addlyrics { \lyric_a }
      \addlyrics { \lyric_b }
    \new Voice { \back }
  >>
>>

\book {
  \bookOutputName "Qué bonita es la amapola"
  \score { \sheet }
}

\book {
  \bookOutputName "Qué bonita es la amapola - Eb"
  \score { \transpose ef c' \sheet }
}

\book {
  \bookOutputName "Qué bonita es la amapola - Bb"
  \score { \transpose bf c' \sheet }
}
