\version "2.24.1"
\language "english"

#(set-global-staff-size 20)

\include "main.ily"
\include "fonts/lilyjazz.ily"

\header {
  title = "Las tortugas ninja"
  instrument = "Vocals"
  composer = ""
  arranger = ""
  poet = ""
  tagline = ""
}

marks = {
  s1*4
  \break
  s1*4
  \break
  s1*4
  \break
  s1*4
  \break
  s1*4
  \break
  s1*4
  \break
  s1*4
  \break
  s1*4
  \break
  s1*4
  \break
  s1*4
  \break
  s1*4
  \bar "||"
}


harmony = \chords {
}

drummer_lyrics = \lyricmode {
  Teen -- age mu -- tant nin -- ja tur -- tles
  Teen -- age mu -- tant nin -- ja tur -- tles
  Teen -- age mu -- tant nin -- ja tur -- tles
  Tu -- rtle pow -- er!

  Teen -- age mu -- tant nin -- ja tur -- tles
  Teen -- age mu -- tant nin -- ja tur -- tles
  cut him no slack

  Teen -- age mu -- tant nin -- ja tur -- tles
  Teen -- age mu -- tant nin -- ja tur -- tles

  Teen -- age mu -- tant nin -- ja tur -- tles
  Teen -- age mu -- tant nin -- ja tur -- tles
  par -- ty dude

  Teen -- age mu -- tant nin -- ja tur -- tles
  Teen -- age mu -- tant nin -- ja tur -- tles
  Teen -- age mu -- tant nin -- ja tur -- tles
  Tu -- rtle pow -- er!

}

bass_lyrics = \lyricmode {
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _

  _ _ _ _ _ _ _ _
  and they're gre -- en _ _ _ _ _ _
  _ _ _ _

  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _

  _ _ _ _ _ _ _ _
  does ma -- chi -- nes _ _ _ _ _ _
  _ _ _
}

guitar_lyrics = \lyricmode {
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  He -- roes in a half -- shell
  _ _ _ _

  They're the world's most fear -- some fight -- ing te -- _ am
  They -- 're he -- roes in a half -- shell _ _ _ _
  When the e -- vil Shred -- der at -- tacks
  These tur -- tle boys don't _ _ _ _

  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _

  Splin -- ter taught them to be nin -- ja te -- _ ens
  Le -- o -- nar -- do leads, Do -- na -- tel -- lo _ _ _ _
  Ra -- pha -- el is cool but rude
  Mi -- chael -- an -- ge -- lo is a _ _ _

  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  He -- roes in a half -- shell
  _ _ _ _
}

drummer = \relative c' {
  \key c \major
  \time 4/4

  c4 a' g a 
  c, a'8 g ~ g4 a
  ef c' bf c
  ef, c'8 bf ~ bf4 c

  f, f' ef f
  af, f'8 ef ~ ef4 f
  R1
  r4 c8 c c c r4

  R1*2
  c,4 a' g a 
  c, a'8 g ~ g4 a

  R1*2
  c,4 a' g a 
  c, a'8 g ~ g4 a

  R1*3
  bf8 bf bf4 bf r

  c,4 a' g a 
  c, a'8 g ~ g4 a
  c,4 a' g a 
  c, a'8 g ~ g4 a

  R1*2
  c,4 a' g a 
  c, a'8 g ~ g4 a

  R1*2
  c,4 a' g a 
  c, a'8 g ~ g4 a

  R1*3
  bf4 bf bf r

  c,4 a' g a 
  c, a'8 g ~ g4 a
  ef c' bf c
  ef, c'8 bf ~ bf4 c

  f, f' ef f
  af, f'8 ef ~ ef4 f
  R1
  r4 c8 c c c r4
}

bass = \relative c' {
  \key c \major
  \time 4/4

  c4 a' g a 
  c, a'8 g ~ g4 a
  ef c' bf c
  ef, c'8 bf ~ bf4 c

  f, f' ef f
  af, f'8 ef ~ ef4 f
  R1
  r4 c8 c c c r4

  R1*2
  c,4 a' g a 
  c, a'8 g ~ g4 a

  R1
  r2 ef'4 ef
  \appoggiatura c16 d4 c g a 
  c, a'8 g ~ g4 a

  R1*3
  bf8 bf bf4 bf r

  c,4 a' g a 
  c, a'8 g ~ g4 a
  c,4 a' g a 
  c, a'8 g ~ g4 a

  R1*2
  c,4 a' g a 
  c, a'8 g ~ g4 a

  R1
  r4 r8 ef'8 ~ ef4 d
  \appoggiatura c16 d4 c g a 
  c, a'8 g ~ g4 a

  R1*3
  bf4 bf bf4 r

  c,4 a' g a 
  c, a'8 g ~ g4 a
  ef c' bf c
  ef, c'8 bf ~ bf4 c

  f, f' ef f
  af, f'8 ef ~ ef4 f
  R1
  r4 c8 c c c r4
}

guitar = \relative c' {
  \key c \major
  \time 4/4

  e'4 e e e 
  e e8 e ~ e4 e
  ef ef ef ef
  ef ef8 ef ~ ef4 ef

  f f ef f
  f f8 ef ~ ef4 f
  c8 c c c bf4 c
  r4 g'8 g g g r4

  f4 ef8 f ~ f4 ef
  f ef g g 
  f8 ef c4 r2
  r2 r4 f8 ef

  f8 ef r4 f ef8 f ~
  f8 ef r4 g4 g
  \appoggiatura e16 f4 e4 r2
  r2 r4 f8 ef 

  f4 ef f8 ef f4
  g4 r r ef
  f8 ef4 f8 r4 ef4
  g8 g g4 f r

  e4 e e e 
  e e8 e ~ e4 e
  e4 e e e 
  e e8 e ~ e4 e

  f8 ef4 f8 ~ f8 ef8 r4
  f ef g g 
  f8 ef c4 r2
  r2 r4 f8 ef

  f8 ef4 f8 r4 f8 ef
  f8 ef4 g8 ~ g4 f
  \appoggiatura e16 f4 e4 r2
  r2 r4 f8 ef 

  f8 ef4 f8 r4 ef 
  g4 r r f8 ef
  f4 ef8 f8 r4 f8 ef
  g4 g4 f4 r

  e4 e e e 
  e e8 e ~ e4 e
  ef ef ef ef
  ef ef8 ef ~ ef4 ef

  f f ef f
  f f8 ef ~ ef4 f
  c8 c c c bf4 c
  r4 g'8 g g g r4
}

keytar = \relative c' {
  \key c \major
  \time 4/4

  g''4 g g g 
  g g8 g ~ g4 g
  g g g g
  g g8 g ~ g4 g

  a a a a
  af af8 af ~ af4 af
  c,8 c c c bf4 c
  r4 c'8 c c c r4

  R1*4

  R1
  r2 bf4 bf
  \appoggiatura g16 a4 g r2
  R1

  R1*3
  bf8 bf bf4 bf r

  g4 g g g
  g g8 g ~ g4 g
  g4 g g g
  g g8 g ~ g4 g

  R1*4

  R1
  r4 r8 bf ~ bf4 a
  \appoggiatura g16 a4 g r2
  R1

  R1*3
  bf4 bf4 bf4 r

  g4 g g g 
  g g8 g ~ g4 g
  g g g g
  g g8 g ~ g4 g

  a a a a
  af af8 af ~ af4 af
  c,8 c c c bf4 c
  r4 c'8 c c c r4
}

sheet = <<
  \harmony
  \new Staff \with { \RemoveAllEmptyStaves } { \marks }
  \compressEmptyMeasures

  \new Staff { \keytar }
  \new Staff { \guitar } \addlyrics { \guitar_lyrics }
  \new Staff { \bass } \addlyrics { \bass_lyrics }
  \new Staff { \drummer } \addlyrics { \drummer_lyrics }
>>

\book {
  \score { \sheet }
}
