\version "2.24.1"
\language "english"


\include "main.ily"

\header {
  title = "Smile"
  subtitle = "(from Tony BENNET Duets Album with Barbra STREISAND)"
  composer = ""
  arranger = "Trans: Gabriel Palacios"
  tagline = ##f
}

marks = {
	s1*5
	s1*4
	
  \bar "||"
	\bmark "A"
	s1*4
	s1*4
	
  \bar "||"
	\bmark "B"
	s1*4
	s1*4
	s1*4
	
  \bar "||"
	\bmark "C"
	s1*4
	s1*4

  \bar "||"
	\bmark "D"
	s1*4
	s1*4
	s1*3
	
	\bar "|."
}


harmony = \chords {
  c
}

% Saxophones

altoI = \relative c'' {
	\key gf \major
  
	R1*8
	r2 b ( \pp \<
	
	\key ef \major
	
	\tag #'part \break
	bf?1 \mp
	c )
	d (
	ef2 c )
	
	\tag #'part \break
	bf1 (
	a2 \< \breathe b4 ^"Soli" c
	af? \mf bf? g af
	e2 ) f4 \> ( ^ "Div" g
	
	\tag #'part \break
	ef?1 \pp
	g2 ~ g8 ) af ( \< bf g
	af1 \mf 
	g4 af ~ af8 ) bf4 ( -> \> af8
	
	\tag #'part \break
	f4 \pp ) g ( ef2
	ef2 ) \! d4 ( \< ef
  f1 \mf \> ) 
	g2 \mp ( g -- )
	
	\tag #'part \break
	R1
	gf2 ( \mf ef4 f
	g?2 ) \times 2/3 { g4 ( \< af bf }
	b2 r8 d4 b8
	
	\key g \major
	
	\tag #'part \pageBreak
	a2. ) \> \f r4 \!
	g4 ( \pp \glissando c2. ) \>
	r8 \! e4. \mp -> ( ~ e2 ~
	e2 e \>
	
	\tag #'part \break
	d2. ) \p \> r4 \!
	r2 r8 fs ( \mf ds [ e ]
	g2 c,4 g'
	fs2 f4 gs,
	
	\tag #'part \break
	e'1 ) \>
	\ottava #0
	a,2 ( \pp \< b
	c4 \mf b bf2 )
	b?4 ( \> a g a
	
	\tag #'part \break
	d,2 ) \mp af'4 \mf ( g 
	fs2 ds
	e2. ) \> r4 \!
	R1 \fermata
	
	\tag #'part \break
	r4 d2. \pp \<
	r4 \! d2. \pp \<
	\once \override BreathingSign.text = \markup { \musicglyph #"scripts.caesura.curved" }
	\breathe
	d'1 \! \mp \fermata
}

altoII = \relative c'' {
	\key gf \major
	
	R1*8
	r2 g ( \pp \<
	
	\key ef \major
	
	\tag #'part \break
	g?1 \mp
	af )
	b (
	c2 af )
	
	\tag #'part \break
	g1 (
	gf2 a \mp \<
  g'2 \pp \! \breathe ef, \mp
	d2 ) f4 \> ( e
	
	\tag #'part \break
	ef?1 \pp
	ef2 ~ ef8 ) ef ( \< f ef
	gf1 \mf
	g?4 af ~ af8 ) bf4 ( -> \> af8
	
	\tag #'part \break
	f4 ) \pp g ( ef2 
	c2 ) b4 \< ( c
  c1 \mf \> )
	df2 \mp ( d ) --
	
	\tag #'part \break
	c1 ( \pp \<
	ef2. \mf ef4
	f4 ef ) \times 2/3 { ef ( \< f ef }
	e2 ef
	
	\key g \major
	
	\tag #'part \pageBreak
	d2. ) \> \f r4 \!
	e4 ( \pp \glissando a2. ) \>
	r8 \! df4. ( -> \mp ~ df2
	d?2 b4 \> c
	
	\tag #'part \break
	a2. ) \p \> r4 \!
	r2 r8 fs' ( \mf ds [ e ]
	g2 c,4 g'
	fs2 f4 gs,
	
	\tag #'part \break
	e'1 ) \>
	e,2 ( \pp \< fs? 
	g1 ) \mf
	ef ( \>
	
	\tag #'part \break
	b2 ) \mp ds \mf (
	d? cs
	c?2. \> ) r4 \!
	R1 \fermata
	
	\tag #'part \break
	r4 d2. \pp \<
	r4 \! c2. \pp \<
	\once \override BreathingSign.text = \markup { \musicglyph #"scripts.caesura.curved" }
	\breathe
	fs1 \! \mp \fermata
}

tenorI = \relative c' {
	\key gf \major
	
	R1*8
	r2 d ( \pp \<
	
	\key ef \major
	
	\tag #'part \break
	ef1 \mp
	g )
	af (
	af2 \breathe g4 gf )
	
	\tag #'part \break
	f1 (
	ef2 gf \mp \<
	ef'2 \pp \breathe c, \mp
	e2 ) df ( \>
	
	\tag #'part \break
	bf1 \pp
	c2 ~ c8 ) c ( \< ef c
	ef1 \mf 
	ef2 ~ ef8 ) ef4. ( -> \>
	
	\tag #'part \break
	d2 \pp g,2
	bf2 ) b4 \< ( c
  af1 \mf \> )
	a2 \mp ( b ) --
	
	\tag #'part \break
	g1 ( \pp \<
	cf2 \mf cf4 cf
	d4 c? ) c2 ( \<
	c1
	
	\key g \major
	
	\tag #'part \pageBreak
	b2. ) \> \f r4 \!
	c4 ( \pp \glissando g'2. ) \>
	r8 \! g4. ( -> \mp ~ g2
	b2 fs2 \>
	
	\tag #'part \break
	fs2. ~ \p fs8 ) b,8 -- -> \mf
	e2 ( -- -> ~ e8 ds fs e
	a1
	gs2 g8 f4. ->
	
	\tag #'part \break
	e1 ) \>
	c2 ( \pp \< d 
	ef1 ) \mf
	c1 ( \>
	
	\tag #'part \break
	a2 ) \mp b ( \mf
	a g
	a2. ) \> r4 \!
	R1 \fermata
	
	\tag #'part \break
	r4 a2. \pp \<
	r4 \! g2. \pp \<
	\once \override BreathingSign.text = \markup { \musicglyph #"scripts.caesura.curved" }
	\breathe
	d'1 \! \mp \fermata
}

tenorII = \relative c' {
	\key gf \major
	
	R1*8
	r2 b ( \pp \<
	
	\key ef \major
	
	\tag #'part \break
	d1 \mp
	ef )
	ef (
	g2 ef )
	
	\tag #'part \break
	f1 (
	ef2 gf \mp \<
	f2 \pp \! \breathe af, \mp
	bf ) c ( \>
	
	\tag #'part \break
	af1 \pp
	af2 ~ af8 ) c ( \< ef c
	cf1 \mf 
	cf2 ~ cf8 ) cf4. ( -> \>
	
	\tag #'part \break
	bf2 \pp b
	c2 ) a \< ( 
  g1 \mf \> )
	g2 \mp ( af ) --
	
	\tag #'part \break
	ef1 ( \pp \<
	bf'4 \mf af bf af
	bf2 ) af ( \<
	g2 fs
	
	\key g \major
	
	\tag #'part \pageBreak
	e2. ) \> \f r4 \!
	g4 ( \pp \glissando c2. ) \>
	r8 \! e4. ( -> \mp ~ e2
	g2 c,4 \> d 
	
	\tag #'part \break
	e2. ~ \p e8 ) b8 -- -> \mf
	e2 ( ~ -- -> e8 ds fs e ~ 
	e1
	d
	
	\tag #'part \break
	c1 ) \>
	g2 ( \pp \< a
	bf4 \mf b d c )
	g1 ( \>
	
	\tag #'part \break
	fs2 ) \mp g ( \mf
	fs1
	g2. ) \> r4 \!
	R1 \fermata
	
	\tag #'part \break
	r4 g2. \pp \<
	r4 \! g2. \pp \<
	\once \override BreathingSign.text = \markup { \musicglyph #"scripts.caesura.curved" }
	\breathe
	b1 \! \mp \fermata
}

baritone = \relative c {
	\key gf \major
	
	R1*8
	r2 bf \pp \< ~
	
	\key ef \major
	
	\tag #'part \break
	bf4. \! ef16 \mf ^"Soli (Baritone sax or Trombone IV)" ( f g8 d'4 bf8
	c4 f,2. ~
	f8 ) f ( gf a d b4 c16 d
	ef4 af,?2. )
	
	\tag #'part \break
	f'4 ( ef \times 2/3 { d f ef }
	d c ) ef2 ( \mp \< ^"Div"
	c \pp bf4 f
	a2 ) c4 ( \> bf
	
	\tag #'part \break
	f1 \pp
	g2 ~ g8 ) f4 \< ( g8
	bf4 \mf af gf2
	f2 ~ f8 ) f4. \> ~
	
	\tag #'part \break
	f8 \! f -- -> \mf g?4 -- -> f2 ( \pp
	g2 ) f ( \<
  ef1 ) \mf \>
	ef2 \mp ( d ) --
	
	\tag #'part \break

	c1 ( \pp \<
	af2 ) \mf f'4 ( ef
	d4 ef ) \times 2/3 { ef2 ( \< f4 }
	g2 fs
	
	\key g \major
	
	\tag #'part \pageBreak
	e2. ) \> \f r4 \!
	R1*3

	\tag #'part \break
  R1*4

	\tag #'part \break
	R1
	e2 ( \pp \< fs
	g1 ) \mf
	f2 ( \> f4 ef
	
	\tag #'part \break
	d2 ) \mp b ( \mf
	e, a
	d,2. ) \> r4 \!
	R1 \fermata
	
	\tag #'part \break
	r4 e'2. \pp \<
	r4 \! ef2. \pp \<
	\once \override BreathingSign.text = \markup { \musicglyph #"scripts.caesura.curved" }
	\breathe
	g,1 \! \mp\fermata
}

% Trombones

tromboneI = \relative c' {
	\key gf \major
  
	R1*9
	
	\key ef \major
	
	\tag #'part \break
	R1*4

	\tag #'part \break
	R1*4

	\tag #'part \break
	R1*4
	
	\tag #'part \break
	r4 bf ( \mp \< b ef
	d \mf c b c ~
	c1 ) \>
	R1 \!
	
	\tag #'part \break
	c1 ( \pp \<
	cf1 \mf
	bf2 ) \times 2/3 { g'4 \< af bf }
	b1 
	
	\key g \major
	
	\tag #'part \pageBreak
	a2. \> \f r4 \!
	R1*2
	r2 fs2 ( \mp
	
	\tag #'part \break
	e1
	cs )
	c? (
	cs2 b
	
	\tag #'part \break
	b1 ) \>
	g2 ( \pp \< a
	bf4 \mf b d c )
	g1 -- \>
	
	\tag #'part \break
	b2 \mp \> r2 \!
	R1*2
	R1 \fermata
	
	\tag #'part \break
	R1*2
	d'1 \! \mp \fermata
}

tromboneII = \relative c' {
	\key gf \major
  
	R1*9
	
	\key ef \major
	
	\tag #'part \break
	R1*4

	\tag #'part \break
	R1*4

	\tag #'part \break
	R1*4
	
	\tag #'part \break
	r4 bf ( \mp \< b ef
	d \mf c b c ~
	c1 ) \>
	R1 \!
	
	\tag #'part \break
	g1 ( \pp \<
	bf4 \mf af bf af
	f4 g ) ef'2 ( \<
	e2 ef
	
	\key g \major
	
	\tag #'part \pageBreak
	d2. ) \> \f r4 \!
	R1*2
	r2 fs2 ( \mp
	
	\tag #'part \break
	e1
	cs )
	c? (
	cs2 b
	
	\tag #'part \break
	b1 ) \>
	g2 ( \pp \< a
	bf4 \mf b d c )
	g1 -- \>
	
	\tag #'part \break
	g2 \mp \> r2 \!
	R1*2
	R1 \fermata
	
	\tag #'part \break
	R1*2
	a'1 \! \mp \fermata
}

tromboneIII = \relative c' {
	\key gf \major
  
	R1*9
	
	\key ef \major
	
	\tag #'part \break
	R1*4

	\tag #'part \break
	R1*4

	\tag #'part \break
	R1*4
	
	\tag #'part \break
	r4 bf ( \mp \< b ef
	d \mf c b c
	af1 ) \>
	R1 \!
	
	\tag #'part \break
	ef1 ( \pp \<
	ef2 \mf f4 ef
	d4 ef ) af2 ( \<
	c1
	
	\key g \major
	
	\tag #'part \pageBreak
	a?2. ) \> \f r4 \!
	R1*2
	r2 c4 ( \mp bf
	
	\tag #'part \break
	a1
	g1 ~
	g8 ) e ( \mf b' g e4. g8
	gs2 a4 gs
	
	\tag #'part \break
	g?1 ) \>
	e2 ( \pp \< fs
	g1 ) \mf
	f2 ( \> f4 ef
	
	\tag #'part \break
	fs2 ) \mp \> r2 \!
	R1*2
	R1 \fermata
	
	\tag #'part \break
	R1*2
	d'1 \! \mp \fermata
}

tromboneIV = \relative c {
	\key gf \major
  
	R1*9
	
	\key ef \major
	
	\tag #'part \break
	r4. \! ef16 \mf ^"Soli (Baritone sax or Trombone IV)" ( f g8 d'4 bf8
	c4 f,2. ~
	f8 ) f ( gf a d b4 c16 d
	ef4 af,?2. )
	
	\tag #'part \break
	f'4 ( ef \times 2/3 { d f ef }
	d c ) r2
  R1*2

	\tag #'part \break
	R1*4

	\tag #'part \break
	R1*4
	
	\tag #'part \break
	a,1 ( \pp \<
	af?2 ) \mf f'4 ( ef
	d4 ef ) \times 2/3 { ef2 ( \< f4 }
	g2 fs
	
	\key g \major
	
	\tag #'part \pageBreak
	e2. ) \> \f r4 \!
	R1*2
	r2 c'4 ( \mp bf
	
	\tag #'part \break
	a1
	g1 ~
	g8 ) e ( \mf b' g e4. g8
	gs2 a4 gs
	
	\tag #'part \break
	g?1 ) \>
	e2 ( \pp \< fs
	g1 ) \mf
	f2 ( \> f4 ef
	
	\tag #'part \break
	d2 ) \mp \> r2 \!
	R1*2
	R1 \fermata
	
	\tag #'part \break
	R1*2
	g,1 \! \mp \fermata
}

% Trumpets

trumpetI = \relative c' {
	\key gf \major
  
	R1*9
	
	\key ef \major
	
	\tag #'part \break
	R1*7
	r2 f4 ( \mf \> g
	
	\tag #'part \break
	af1 \pp
	g2 ~ g8 ) af ( \< c g'
	ef1 ) \mf 
	ef2 ~ ef8 ef4 ( -> f8
	
	\tag #'part \break
	d4 ) \> r4 \! r2
	R1
	r8 g4 \mp -> ef8 -> ~ ef c -. -- af4 \>
	R1 \!
	
	\tag #'part \break
	R1*2
	r2 \times 2/3 { g4 ( \mf \< af bf }
	b1 
	
	\key g \major
	
	\tag #'part \pageBreak
	d2. ) \> \f r4 \!
	c8 ( \mp d e2. ) \>
  r8 \! a4. \mp ~ a8 a g4 ~
	g2 e2

	\tag #'part \break
	d2. \p r4
	R1*3
	
	\tag #'part \break
	R1
	a2 ( \pp \< b
	c4 \mf b bf2 )
	b?4 ( \> a g a
	
	\tag #'part \break
	d,2 ) \mp \> r2 \!
	R1*2
	R1 \fermata
	
	\tag #'part \break
	R1*2
	d'1 \! \mp \fermata
}

trumpetII = \relative c' {
	\key gf \major
  
	R1*9
	
	\key ef \major
	
	\tag #'part \break
	R1*7
	r2 f4 ( \mf \> g
	
	\tag #'part \break
	af1 \pp
	g2 ~ g8 ) ef (\< af c
	cf1 ) \mf 
	cf2 ~ cf8 cf4 ( -> cf8
	
	\tag #'part \break
	bf?4 ) \> r4 \! r2
	R1
	r8 ef4 \mp -> c8 -> ~ c af -- -. f4 \>
	R1 \!
	
	\tag #'part \break
	R1*2 \!
	r2 \times 2/3 { g4 ( \mf \< af bf }
	b2 a2
	
	\key g \major
	
	\tag #'part \pageBreak
	a2. ) \> \f r4 \!
	c8 ( \mp d e2. ) \>
	R1*2 \!

	\tag #'part \break
  R1*4
	
	\tag #'part \break
	R1
	e,2 ( \pp \< fs
	g1 ) \mf 
	g1 ( \>
	
	\tag #'part \break
	d2 ) \mp \> r2 \!
	R1*2
	R1 \fermata

	\tag #'part \break
	R1*2
	a'1 \! \mp \fermata
}

trumpetIII = \relative c' {
	\key gf \major
  
	R1*9
	
	\key ef \major
	
	\tag #'part \break
	R1*8
	
	\tag #'part \break
	ef1 ( \pp
	ef2 ~ ef8 ) c ( \< f af
	af1 ) \mf 
	f2 ~ f8 f4 ( -> f8
	
	\tag #'part \break
	g4 ) \> r4 \! r2
	R1
	r8 c4 \mp -> af8 -> ~ af g -- -. ef4 \>
	R1 \!
	
	\tag #'part \break
	R1*2 \!
	r2 \times 2/3 { ef4 ( \mf \< f ef }
	g2 ef
	
	\key g \major
	
	\tag #'part \pageBreak
	d2. ) \> \f r4 \!
	g8 ( \mp a b2. ) \>
	R1*2 \!

	\tag #'part \break
  R1*4
	
	\tag #'part \break
	R1
	c,2 ( \pp \< d
	ef1 ) \mf
	ef1 ( \>
	
	\tag #'part \break
	d2 ) \mp \> r2 \!
	R1*2
	R1 \fermata
	
	\tag #'part \break
	R1*2
	fs1 \! \mp \fermata
}

trumpetIV = \relative c' {
	\key gf \major
  
	R1*9
	
	\key ef \major
	
	\tag #'part \break
	R1*8
	
	\tag #'part \break
	c1 ( \pp
	c2 ~ c8 ) af ( \< c g'
	gf1 ) \mf 
	ef2 ~ ef8 ef4 ( -> ef8
	
	\tag #'part \break
	f4 ) \> r4 \! r2
	R1
	r8 bf4 \mp -> g8 -> ~ g f -- -. ef4 \>
	R1 \!
	
	\tag #'part \break
	R1*2 \!
	r2 \times 2/3 { ef4 ( \mf \< f ef }
	g2 ef
	
	\key g \major
	
	\tag #'part \pageBreak
	d2. ) \> \f r4 \!
	g8 ( \mp a b2. ) \>
	R1*2 \!

	\tag #'part \break
  R1*4
	
	\tag #'part \break
	R1
	c,2 ( \pp \< d
	ef1 ) \mf
	ef1 ( \>
	
	\tag #'part \break
	d2 ) \mp \> r2 \!
	R1*2
	R1 \fermata
	
	\tag #'part \break
	R1*2
	d1 \! \mp \fermata
}

% Rhythm section

pianoROne = \relative c' {
  \voiceOne
  \override Voice.Rest.staff-position = #8

  \key fs \major
  
  <as' cs>2 \arpeggio r16 as8. ~ as4
  f2 r4 <ds as' ds>4
  \tag #'part \break
  r4 <es' gs> s2
  gs,8 ( fs <b ds>4 ) fss4. <ds fss ass ds>8 \arpeggio
  
  \tag #'part \break
  <ds gs b>4.  <as fs' cs'>8 <ds as' fs'>4 <fs gs as cs>8 <ds fs gs as>
  \set tieWaitForNote = ##t
  r4 \appoggiatura { a16 ~ b ~ cs ~ e ~ fs ~ a ~ cs ~ } <a, b cs e fs a cs e>4 as'4 ( cs )
  
  \tag #'part \break
  cs4 b es, as  
  \change Staff = "leftHand" r8 ds,, as' [ fs ] ds' b 
  \change Staff = "rightHand" << { fs'8 ds} \\ { gs,4 } >>
  
  \tag #'part \break
  bf'4 g' \afterGrace d2 { bf,16 ( cf df d e f g af bf cf df d e f g af bf cf df d e f g af}
    
  \key ef \major
  
  \override Voice.Rest.staff-position = #0
  \override Voice.MultiMeasureRest.staff-position = #2
  
  \tag #'part \break
  bf1 )
  r8 g, f'2.
  <af,, d f>1
  r8 af bf4 r2
  
  \tag #'part \break
  r2 f''8 ef r4
  r2 <b d f>4 r4
  R1*2
  
  \tag #'part \break
  <af c ef>4 r2.
  <af c>2 ~ <af c>8 af c g'
  ef1
  R1
  
  \tag #'part \break
  r2. <g, bf>8 <f af>
  <d f>2 r
  R1
  r2. <b d>4
  
  \tag #'part \break
  r8 g c g' c4 <d ef g>
  R1*2
  r2. \times 4/7 { fs,,16 ( g a bf ef g a }
  
  \key g \major
   
  \tag #'part \break
  <d, g b d>1 ) \arpeggio
  R1*3
  
  \tag #'part \break
  d'1
  R1*2
  r2. r8 d, ~
  
  \tag #'part \break
  d4 b' e,2
  r8 e d' [ c ] a fs e' d
  R1*2
  
  \tag #'part \break
  <d,, a' d>2 \arpeggio r2
  \once \override Voice.MultiMeasureRest.staff-position = #0
  R1*2
  
  <af' b f'>2 <a c ef fs> \fermata
  
  \tag #'part \break
  <c a'>4 <b g'>2 <b g'>4
  <c a'>8 <d b'>8 <b g'>2.
  <d fs a d>1 \arpeggio \fermata
}

pianoRTwo = \relative c' {
  \voiceTwo
  \override Voice.Rest.staff-position = #-9

  \key fs \major
  
  <ds>2 \arpeggio <b ds>16  ~ <b ds>8. ~ <b ds>4
  <a c>2 \arpeggio <fs b ds>2
  <ds' as' cs ds>2 r8 <gss, ds'>4. ~
  <cs ds>4 ~ <b ds> r8 <bs cs es>8 <fss ass dss>4
  
  \slurDown s2 r8 gs ( as16 fs gs fs ) \slurNeutral
  <fs a cs e>2 <cs' fs>
  
  r8 gs' fss [ fs ] <gs, cs>4 <css es>
  \once \override NoteColumn.ignore-collision = ##t
  <gs b ds gs>1 \arpeggio 
  <c ef g>4 <af' c ef> <d, g b>2
  
  s1*36
}

pianoLThree = \relative c, {
  \voiceThree
  
  \clef bass
  \key fs \major
  
  \override Voice.Rest.staff-position = #5
  
  <cs' as'>2. r4 
  <d fs>2 \arpeggio r4 gs
  r8 fs ~ <fs ds'>4 r8 es4. (
  <gs, fs'>2 ) r8 cs4.
  
  r16 ds16 ~ <ds b'>8 ~ <ds b'>4 ~ <ds b'>2
  cs2 d
  
  << { r8 ds' d4 } \\ { r8 as4. } >> <as, fs'>4 fs'
  b,1 \arpeggio
  ef8 af c4 af2
  
  s1*36
}

pianoLFour = \relative c, {
  \voiceFour
  
  \clef bass
  \key fs \major
  
  fs2. \arpeggio cs'4 ~
  cs cs <gs, cs>2
  as'2
  \appoggiatura { cs,8 } ds'2 -> ~ ds2 ds,2
  
  gs1
  <b, fs'>2 e
  
  fs4 as ds, gs
  cs,1 \arpeggio
  df2 bf'
    
  \key ef \major
  R1*20
  
  \key g \major
  
  R1*12
  
  \once \override Voice.MultiMeasureRest.staff-position = #2
  R1
  R1*2
  <d, d'>2 <d d'>
  
  c8 g' e' a d e a b
  c,,,8 ef' a d ef f g a
  
  <g,, d' b'>1 \arpeggio
}

guitar = \relative c' {
  % \key f \major
  % \time 3/4

  % R2.
}

bass = \relative c' {
  \clef bass
  \key gf \major
  
  R1*8
  r2 bf4 \p \< bf,
  
  \key ef \major
  
  \tag #'part \break
  ef8 \mf bf' ef4 r4. bf8
  ef,2. ef4
  ef2. r8 ef
  bf'2 bf4 af
  
  \tag #'part \break
  g2. r8 g
  gf2. r8 gf
  f2 f4 ~ \times 2/3 { f4 f8 } 
  bf,8 bf4 bf8 bf4 g'8 c,
  
  \tag #'part \break
  f2 ~ f8 c f c
  f2 f4 af
  af2. \times 2/3 { r8 af, af' } 
  df,2 df4 cf
  
  \tag #'part \break
  bf4 ~ \times 2/3 { bf8 bf' f } g4 g8 g,
  c4 ~ \times 2/3 { c4 ef8 } f4 ~ \times 2/3 { f8 f c }
  bf4. bf'8 bf4 bf8 bf,
  bf2 bf4 bf8 g 
  
  \tag #'part \break
  a2. \pp \< a4
  af?4 \mf ~ \times 2/3 { af8 af' ef } df4 cf
  bf4. bf'8 bf4 bf8 bf,
  d4 \< d'2 d,4
  
  \key g \major
  
  \tag #'part \break
  g4 \f \> g'2 d8 \mf d,
  g4 g'2 g8 d,
  g2 g'
  a,2 d,4 c
  
  \tag #'part \break
  b ~ \times 2/3 { b4 b8 } b4 b 
  bf? ~ bf8 bf'16 g bf,4. bf8
  a2. a4
  e'2 e4. e8
  
  \tag #'part \break
  a,2 ~ a8 a' e e
  a,2 b4. b8
  c2 g'4 c, 
  f2 f4 ef
  
  \tag #'part \break
  d2 \> r2 \!
  R1*2
  R1 \fermata
  
  \tag #'part \break
  R1*2
  g,1 \fermata
}

drumpart = \relative c' {
  % \key f \major
  % \time 3/4

  % R2.
}

full = <<
  % \new Staff \with { instrumentName = "Lead" } \lead
  \new Staff \with { \RemoveAllEmptyStaves } { \marks }
  \new StaffGroup <<
    \new Staff \with { instrumentName = "Alto sax I" }   \removeWithTag #'part \transpose ef c' \altoI
    \new Staff \with { instrumentName = "Alto sax II" }  \removeWithTag #'part \transpose ef c' \altoII
    \new Staff \with { instrumentName = "Tenor sax I" }  \removeWithTag #'part \transpose bf c'' \tenorI
    \new Staff \with { instrumentName = "Tenor sax II" } \removeWithTag #'part \transpose bf c'' \tenorII
    \new Staff \with { instrumentName = "Baritone sax" } \removeWithTag #'part \transpose ef c'' \baritone
  >>
  \new StaffGroup <<
    \new Staff \with { instrumentName = "Trombone I" }   \removeWithTag #'part \transpose bf c'' \tromboneI
    \new Staff \with { instrumentName = "Trombone II" }  \removeWithTag #'part \transpose bf c'' \tromboneII
    \new Staff \with { instrumentName = "Trombone III" } \removeWithTag #'part \transpose bf c'' \tromboneIII
    \new Staff \with { instrumentName = "Trombone IV" }  \removeWithTag #'part \transpose ef c'' \tromboneIV
  >>
  \new StaffGroup <<
    \new Staff \with { instrumentName = "Trumpet I" }   \removeWithTag #'part \transpose bf c' \trumpetI
    \new Staff \with { instrumentName = "Trumpet II" }  \removeWithTag #'part \transpose bf c' \trumpetII
    \new Staff \with { instrumentName = "Trumpet III" } \removeWithTag #'part \transpose ef c' \trumpetIII
    \new Staff \with { instrumentName = "Trumpet IV" }  \removeWithTag #'part \transpose ef c' \trumpetIV
  >>
  \new StaffGroup <<
    % \new Staff \with { instrumentName = "Guitar" } \guitar
    \new PianoStaff \with { instrumentName = "Piano" } << 
      \set PianoStaff.connectArpeggios = ##t
      \new Staff = "rightHand" <<
          \new Voice \removeWithTag #'part \pianoROne
          \new Voice \removeWithTag #'part \pianoRTwo
      >>
      \harmony
      \new Staff = "leftHand" <<
          \new Voice \removeWithTag #'part \pianoLThree
          \new Voice \removeWithTag #'part \pianoLFour
      >>
    >>
    \new Staff \with { instrumentName = "Bass" } \bass
    % \new DrumStaff \with { instrumentName = "Drums" } \drumpart
  >>
>>

full_midi = <<
  % \new Staff \with { instrumentName = "Lead" } \lead
  \new Staff \with { \RemoveAllEmptyStaves } { \marks }
  \new StaffGroup <<
    \new Staff \with { instrumentName = "Alto sax I" midiInstrument = "alto sax" }       \altoI
    \new Staff \with { instrumentName = "Alto sax II" midiInstrument = "alto sax" }      \altoII
    \new Staff \with { instrumentName = "Tenor sax I" midiInstrument = "tenor sax" }     \tenorI
    \new Staff \with { instrumentName = "Tenor sax II" midiInstrument = "tenor sax" }    \tenorII
    \new Staff \with { instrumentName = "Baritone sax" midiInstrument = "baritone sax" } \baritone
  >>
  \new StaffGroup <<
    \new Staff \with { instrumentName = "Trombone I" midiInstrument = "trombone" }   \tromboneI
    \new Staff \with { instrumentName = "Trombone II" midiInstrument = "trombone" }  \tromboneII
    \new Staff \with { instrumentName = "Trombone III" midiInstrument = "trombone" } \tromboneIII
    \new Staff \with { instrumentName = "Trombone IV" midiInstrument = "trombone" }  \tromboneIV
  >>
  \new StaffGroup <<
    \new Staff \with { instrumentName = "Trumpet I" midiInstrument = "trumpet" }   \trumpetI
    \new Staff \with { instrumentName = "Trumpet II" midiInstrument = "trumpet" }  \trumpetII
    \new Staff \with { instrumentName = "Trumpet III" midiInstrument = "trumpet" } \trumpetIII
    \new Staff \with { instrumentName = "Trumpet IV" midiInstrument = "trumpet" }  \trumpetIV
  >>
  \new StaffGroup <<
    % \harmony
    % \new Staff \with { instrumentName = "Guitar" } \guitar
    \new PianoStaff \with { instrumentName = "Piano" midiInstrument = "acoustic grand" } << 
      \set PianoStaff.connectArpeggios = ##t
      \new Staff = "rightHand" <<
          \new Voice \pianoROne
          \new Voice \pianoRTwo
      >>
      \new Staff = "leftHand" <<
          \new Voice \pianoLThree
          \new Voice \pianoLFour
      >>
    >>
    \new Staff \with { instrumentName = "Bass" midiInstrument = "pizzicato strings" } \bass
    % \new DrumStaff \with { instrumentName = "Drums" } \drumpart
  >>
>>

#(set-global-staff-size 13)
\include "fonts/lilyjazz.ily"

\book {
  \paper {
    #(set-paper-size "a4" 'landscape)
    % #(define fonts
    %   (set-global-fonts
    %   #:music "lilyjazz"
    %   #:brace "lilyjazz"
    %   #:roman "lilyjazz-text"
    %   #:sans "lilyjazz-chord"
    %   #:factor (/ staff-height pt 20)
    % ))
  }
  \score { 
    \full
  }
}

% \book {
%   \paper {}
%   \score { 
%     \full_midi
%     \midi { \tempo 4 = 60 }
%   }
% }

#(set-global-staff-size 20)
\include "fonts/lilyjazz.ily"

\book {
  \bookOutputName "Smile - Alto sax I"
  \header { instrument = "Alto sax I" }
  \score { <<
    \new Staff \with { \RemoveAllEmptyStaves } { \marks }
    \new Staff { \compressEmptyMeasures { \transpose ef c' \altoI } } 
  >> }
}

\book {
  \bookOutputName "Smile - Alto sax II"
  \header { instrument = "Alto sax II" }
  \score { <<
    \new Staff \with { \RemoveAllEmptyStaves } { \marks }
    \new Staff { \compressEmptyMeasures { \transpose ef c' \altoII } } 
  >> }
}

\book {
  \bookOutputName "Smile - Tenor sax I"
  \header { instrument = "Tenor sax I" }
  \score { <<
    \new Staff \with { \RemoveAllEmptyStaves } { \marks }
    \new Staff { \compressEmptyMeasures { \transpose bf c'' \tenorI } } 
  >> }
}

\book {
  \bookOutputName "Smile - Tenor sax II"
  \header { instrument = "Tenor sax II" }
  \score { <<
    \new Staff \with { \RemoveAllEmptyStaves } { \marks }
    \new Staff { \compressEmptyMeasures { \transpose bf c'' \tenorII } } 
  >> }
}

\book {
  \bookOutputName "Smile - Baritone sax"
  \header { instrument = "Baritone sax" }
  \score { <<
    \new Staff \with { \RemoveAllEmptyStaves } { \marks }
    \new Staff { \compressEmptyMeasures { \transpose ef c'' \baritone } } 
  >> }
}

\book {
  \bookOutputName "Smile - Trombone I"
  \header { instrument = "Trombone I" }
  \score { <<
    \new Staff \with { \RemoveAllEmptyStaves } { \marks }
    \new Staff { \compressEmptyMeasures { \transpose bf c'' \tromboneI } } 
  >> }
}

\book {
  \bookOutputName "Smile - Trombone II"
  \header { instrument = "Trombone II" }
  \score { <<
    \new Staff \with { \RemoveAllEmptyStaves } { \marks }
    \new Staff { \compressEmptyMeasures { \transpose bf c'' \tromboneII } } 
  >> }
}

\book {
  \bookOutputName "Smile - Trombone III"
  \header { instrument = "Trombone III" }
  \score { <<
    \new Staff \with { \RemoveAllEmptyStaves } { \marks }
    \new Staff { \compressEmptyMeasures { \transpose bf c'' \tromboneIII } } 
  >> }
}

\book {
  \bookOutputName "Smile - Trombone IV"
  \header { instrument = "Trombone IV" }
  \score { <<
    \new Staff \with { \RemoveAllEmptyStaves } { \marks }
    \new Staff { \compressEmptyMeasures { \transpose ef c'' \tromboneIV } } 
  >> }
}

\book {
  \bookOutputName "Smile - Trumpet I"
  \header { instrument = "Trumpet I" }
  \score { <<
    \new Staff \with { \RemoveAllEmptyStaves } { \marks }
    \new Staff { \compressEmptyMeasures { \transpose bf c' \trumpetI } } 
  >> }
}

\book {
  \bookOutputName "Smile - Trumpet II"
  \header { instrument = "Trumpet II" }
  \score { <<
    \new Staff \with { \RemoveAllEmptyStaves } { \marks }
    \new Staff { \compressEmptyMeasures { \transpose bf c' \trumpetII } } 
  >> }
}

\book {
  \bookOutputName "Smile - Trumpet III"
  \header { instrument = "Trumpet III" }
  \score { <<
    \new Staff \with { \RemoveAllEmptyStaves } { \marks }
    \new Staff { \compressEmptyMeasures { \transpose ef c' \trumpetIII } } 
  >> }
}

\book {
  \bookOutputName "Smile - Trumpet IV"
  \header { instrument = "Trumpet IV" }
  \score { <<
    \new Staff \with { \RemoveAllEmptyStaves } { \marks }
    \new Staff { \compressEmptyMeasures { \transpose ef c' \trumpetIV } } 
  >> }
}

\book {
  \bookOutputName "Smile - Piano"
  \header { instrument = "Piano" }
  \score { <<
    \new Staff \with { \RemoveAllEmptyStaves } { \marks }
    \new PianoStaff << 
      \set PianoStaff.connectArpeggios = ##t
      \new Staff = "rightHand" <<
          \new Voice \pianoROne
          \new Voice \pianoRTwo
      >>
      \new Staff = "leftHand" <<
          \new Voice \pianoLThree
          \new Voice \pianoLFour
      >>
    >>
  >> }
}

\book {
  \bookOutputName "Smile - Bass"
  \header { instrument = "Bass" }
  \score { <<
    \new Staff \with { \RemoveAllEmptyStaves } { \marks }
    \new Staff { \compressEmptyMeasures { \bass } } 
  >> }
}
