\version "2.24.1"
\language "english"

#(set-global-staff-size 20)

\include "main.ily"
\include "fonts/lilyjazz.ily"

\header {
  title = ""
  instrument = ""
  composer = ""
  arranger = ""
  poet = ""
  tagline = ""
}

marks = {
  \mark \markup "Score mark"
}


harmony = \chords {
  c
}

% Música
soprano = \relative c' {
  \key ef \major
  \time 4/4
  
  r2. r4 \times 2/3 { ef8 e f }

  gf8 af gf a ef \times 2/3 { c df d }
  ef8 f ef d c \times 2/3 { a bf b }
  c8 ef r f gf d ef e
  f cs d bf
}

alto = \relative c' {
  \key ef \major
  \time 4/4

  c'
}

tenor = \relative c' {
  \key ef \major
  \time 4/4

  c'
}

baritone = \relative c' {
  \key ef \major
  \time 4/4

  c'
}

sheet = <<
  \harmony
  \new Staff \with { \RemoveAllEmptyStaves } { \marks }

  \new StaffGroup <<
    \new Staff \with 
    { 
      midiInstrument = #"soprano sax" 
      instrumentName = "Soprano Sax"
    }
    { \transpose ef c' \soprano } 

    % \new Staff \with 
    % { instrumentName = "Bb" }
    % { \transpose bf c'' \partBb } 
  >>
>>

\book {
  \score { \sheet \midi { } }
}
